<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This is used to store national taxes based on directory and CMR account</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>CMR__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>CMR account to apply Tax amount</description>
        <externalId>false</externalId>
        <label>CMR</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>National Taxs</relationshipLabel>
        <relationshipName>National_Taxs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Directory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Apply tax based on this directory</description>
        <externalId>false</externalId>
        <label>Directory</label>
        <referenceTo>Directory__c</referenceTo>
        <relationshipLabel>National Taxs</relationshipLabel>
        <relationshipName>National_Taxs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Tax_Code__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Tax Code</label>
        <referenceTo>c2g__codaTaxCode__c</referenceTo>
        <relationshipLabel>National Taxes</relationshipLabel>
        <relationshipName>National_Taxes</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Tax__c</fullName>
        <description>to store tax percentage</description>
        <externalId>false</externalId>
        <label>Tax(%)</label>
        <precision>8</precision>
        <required>false</required>
        <scale>5</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <label>National Tax</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CMR__c</columns>
        <columns>Directory__c</columns>
        <columns>Tax__c</columns>
        <columns>Tax_Code__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>{000}</displayFormat>
        <label>National Tax Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>National Taxes</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>CMR_and_Directory_Validation</fullName>
        <active>true</active>
        <description>CMR and Directory should not be populated together. Either one should be populated.</description>
        <errorConditionFormula>AND(
 NOT( ISBLANK( CMR__c ) ) ,
 NOT( ISBLANK( Directory__c ) )
)</errorConditionFormula>
        <errorMessage>CMR and Directory should not be populated together. Either one should be populated.</errorMessage>
    </validationRules>
</CustomObject>
