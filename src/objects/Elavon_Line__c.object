<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>ACH_Return_Code__c</fullName>
        <externalId>false</externalId>
        <label>ACH Return Code</label>
        <length>4</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Error__c</fullName>
        <externalId>false</externalId>
        <formula>isblank(Account__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Error</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Elavon Lines</relationshipLabel>
        <relationshipName>Elavon_Lines</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Cash_Entry_ID_c__c</fullName>
        <externalId>false</externalId>
        <label>Cash_Entry_ID__c</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cash_Entry_Line_Value__c</fullName>
        <externalId>false</externalId>
        <formula>abs( Value__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Cash Entry Line Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Cash_Entry_Type__c</fullName>
        <externalId>false</externalId>
        <formula>if( and( Value__c &gt;0,text(Payment_Status__c) =&quot;SENT&quot;),&quot;Receipt&quot;,&quot;Refund&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Cash Entry Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Credit_Card_Type__c</fullName>
        <externalId>false</externalId>
        <label>Credit Card Type</label>
        <picklist>
            <picklistValues>
                <fullName>AMEX</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DISC</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VISA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MC</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Customer_ID__c</fullName>
        <externalId>false</externalId>
        <label>Customer_ID</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Date__c</fullName>
        <externalId>false</externalId>
        <formula>Elavon_Header__r.Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Dimension_1__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Dimension 1</label>
        <referenceTo>c2g__codaDimension1__c</referenceTo>
        <relationshipLabel>Elavon Lines</relationshipLabel>
        <relationshipName>Elavon_Lines</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Dimension_2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Dimension 2</label>
        <referenceTo>c2g__codaDimension2__c</referenceTo>
        <relationshipLabel>Elavon Lines</relationshipLabel>
        <relationshipName>Elavon_Lines</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Dimension_3__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Dimension 3</label>
        <referenceTo>c2g__codaDimension3__c</referenceTo>
        <relationshipLabel>Elavon Lines</relationshipLabel>
        <relationshipName>Elavon_Lines</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Dimension_4__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Dimension 4</label>
        <referenceTo>c2g__codaDimension4__c</referenceTo>
        <relationshipLabel>Elavon Lines</relationshipLabel>
        <relationshipName>Elavon_Lines</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Directory_Edition__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory Edition</label>
        <referenceTo>Directory_Edition__c</referenceTo>
        <relationshipLabel>Elavon Lines</relationshipLabel>
        <relationshipName>Elavon_Lines</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Directory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory</label>
        <referenceTo>Directory__c</referenceTo>
        <relationshipLabel>Elavon Lines</relationshipLabel>
        <relationshipName>Elavon_Lines</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Edition_Code__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Edition Code</label>
        <referenceTo>Directory_Edition__c</referenceTo>
        <relationshipLabel>Elavon Lines (Edition Code)</relationshipLabel>
        <relationshipName>Elavon_Lines1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Elavon_Batch__c</fullName>
        <externalId>false</externalId>
        <label>Elavon Batch</label>
        <referenceTo>Elavon_Batch__c</referenceTo>
        <relationshipLabel>Elavon Lines</relationshipLabel>
        <relationshipName>Elavon_Lines</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Elavon_Header__c</fullName>
        <externalId>false</externalId>
        <label>Elavon Header</label>
        <referenceTo>Elevon_Staging__c</referenceTo>
        <relationshipLabel>Elavon Lines</relationshipLabel>
        <relationshipName>Elavon_Lines</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Last_4__c</fullName>
        <externalId>false</externalId>
        <label>Last 4</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Line_Description__c</fullName>
        <externalId>false</externalId>
        <label>Line Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Order_Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Order Line Item</label>
        <referenceTo>Order_Line_Items__c</referenceTo>
        <relationshipLabel>Elavon Lines</relationshipLabel>
        <relationshipName>Elavon_Lines</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Payment_Reason__c</fullName>
        <externalId>false</externalId>
        <formula>if(Cash_Entry_Type__c = &quot;Refund&quot;,&quot;RFD - Refund&quot;,&quot;P - Payment&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Payment Reason</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Payment_Status__c</fullName>
        <externalId>false</externalId>
        <label>Payment Status</label>
        <picklist>
            <picklistValues>
                <fullName>SENT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RETN</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>REFN</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Payment_Type__c</fullName>
        <externalId>false</externalId>
        <label>Payment Type</label>
        <picklist>
            <picklistValues>
                <fullName>ACH</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CC</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Process__c</fullName>
        <externalId>false</externalId>
        <formula>Account_Error__c =false</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Process</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Reason_Description__c</fullName>
        <externalId>false</externalId>
        <label>Reason Description</label>
        <length>48</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Recurring_Reference_ID__c</fullName>
        <externalId>false</externalId>
        <label>Recurring Reference ID</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reference__c</fullName>
        <externalId>false</externalId>
        <label>Reference</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Return_Date__c</fullName>
        <externalId>false</externalId>
        <label>Return Date</label>
        <length>8</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_Confirmation_ID__c</fullName>
        <externalId>false</externalId>
        <label>Transaction Confirmation ID</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_Types__c</fullName>
        <externalId>false</externalId>
        <formula>If(AND(OR( TEXT(Payment_Status__c) =&quot;SENT&quot;, TEXT(Payment_Status__c) =&quot;REFN&quot;), Value__c &gt;0),&quot;P - Payment&quot;, 

CASE(text( Payment_Status__c ), 

&quot;RETN&quot;,&quot;NSF - Returned Check NSF&quot;, 
&quot;SENT&quot;,&quot;RFD - Refund&quot;,&quot;RFD - Refund&quot;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Transaction Types</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Value__c</fullName>
        <externalId>false</externalId>
        <label>Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Elavon Line</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Account__c</columns>
        <columns>Account_Error__c</columns>
        <columns>Cash_Entry_Line_Value__c</columns>
        <columns>Cash_Entry_Type__c</columns>
        <columns>Elavon_Batch__c</columns>
        <columns>Elavon_Header__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>ELA-{000000000}</displayFormat>
        <label>Elavon Line Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Elavon Lines</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
