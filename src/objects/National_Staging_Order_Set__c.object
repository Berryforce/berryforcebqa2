<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>NationalManualEntry_v44</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <comment>use national manual input</comment>
        <content>NationalManualEntry_v44</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Groups the National Staging Order Lines together by Transaction ID and Unique Version ID.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Approved_to_Process__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Approved to Process</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Auto_Number__c</fullName>
        <externalId>false</externalId>
        <label>Auto Number</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CMRClientCombo__c</fullName>
        <externalId>false</externalId>
        <formula>CMR_Number__c+Client_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CMRClientCombo</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CMR_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>CMR Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>National Staging Order Sets (CMR Name)</relationshipLabel>
        <relationshipName>National_Staging_Order_Sets1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CMR_Notice__c</fullName>
        <externalId>false</externalId>
        <formula>IF( CMR_Name__r.Non_Elite__c  =TRUE, IMAGE(&quot;/img/samples/flag_red.gif&quot;, &quot;Non-Elite&quot;, 32,32)&amp;&quot;Non-Elite CMR&quot;,
IMAGE(&quot;/img/samples/flag_green.gif&quot;, &quot;Elite CMR&quot;, 32,32)&amp;&quot;Elite CMR&quot;)&amp;BR()&amp;
IF( CMR_Name__r.Is_Active__c  =FALSE, IMAGE(&quot;/img/samples/flag_red.gif&quot;, &quot;Inactive CMR - No Orders Can Be Processed&quot;, 32,32)&amp;&quot;Inactive CMR - No Orders Can Be Processed&quot;,
IMAGE(&quot;/img/samples/flag_green.gif&quot;, &quot;Active CMR&quot;, 32,32)&amp;&quot;Active CMR&quot;)&amp;BR()&amp;
IF( CMR_Name__r.Bankruptcy__c  =TRUE, IMAGE(&quot;/img/samples/flag_red.gif&quot;, &quot;Bankrupt CMR - No Orders Can Be Processed&quot;, 32,32)&amp;&quot;Bankrupt CMR - No Orders Can Be Processed&quot;&amp;BR(),&quot;&quot;)&amp;
IF( CMR_Name__r.Delinquency_Indicator__c  =TRUE, IMAGE(&quot;/img/samples/flag_red.gif&quot;, &quot;Delinquent CMR - No Orders Can Be Processed&quot;, 32,32)&amp;&quot;Delinquent CMR - No Orders Can Be Processed&quot;&amp;BR(),&quot;&quot;)&amp; 
IF( CMR_Name__r.Manual_Delinquency_Indicator__c  =TRUE, IMAGE(&quot;/img/samples/flag_red.gif&quot;, &quot;Delinquent CMR - No Orders Can Be Processed&quot;, 32,32)&amp;&quot;Delinquent CMR - No Orders Can Be Processed&quot;&amp;BR(),&quot;&quot;)&amp; 
IF( ISPICKVAL(CMR_Name__r.c2g__CODACreditStatus__c, &quot;On Hold&quot;)  || ISPICKVAL(CMR_Name__r.c2g__CODACreditStatus__c, &quot;Not Approved&quot;) || ISPICKVAL(CMR_Name__r.c2g__CODACreditStatus__c,&quot;Terminated&quot;), IMAGE(&quot;/img/samples/flag_red.gif&quot;, &quot;Check CMR Credit Status - No Orders Can Be Processed&quot;, 32,32)&amp;&quot;Check CMR Credit Status - No Orders Can Be Processed&quot;&amp;BR(),&quot;&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CMR Notice</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CMR_Number__c</fullName>
        <externalId>false</externalId>
        <label>CMR #</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CORE_Migration_ID__c</fullName>
        <description>This is needed for migration of data from CORE</description>
        <externalId>false</externalId>
        <label>CORE Migration ID</label>
        <length>30</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ChildReadyForProcess__c</fullName>
        <externalId>false</externalId>
        <label>Child Ready For Process</label>
        <summaryFilterItems>
            <field>National_Staging_Line_Item__c.Ready_for_Processing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>National_Staging_Line_Item__c.UDAC__c</field>
            <operation>notEqual</operation>
            <value></value>
        </summaryFilterItems>
        <summaryForeignKey>National_Staging_Line_Item__c.National_Staging_Header__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Child_Total__c</fullName>
        <externalId>false</externalId>
        <label>Child Total</label>
        <summaryFilterItems>
            <field>National_Staging_Line_Item__c.UDAC__c</field>
            <operation>notEqual</operation>
            <value></value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>National_Staging_Line_Item__c.Standing_Order__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>National_Staging_Line_Item__c.National_Staging_Header__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Client_Name_Text__c</fullName>
        <externalId>false</externalId>
        <label>Client Name Text</label>
        <length>53</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Client Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>National Staging Order Sets</relationshipLabel>
        <relationshipName>National_Staging_Order_Sets</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Client_Number__c</fullName>
        <externalId>false</externalId>
        <label>Client #</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Date__c</fullName>
        <description>changed label from Seniority Date to Date</description>
        <externalId>false</externalId>
        <label>Seniority Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Directory_Edition_Number__c</fullName>
        <externalId>false</externalId>
        <label>Directory Edition #</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Edition__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory Edition</label>
        <referenceTo>Directory_Edition__c</referenceTo>
        <relationshipLabel>National Staging Order Sets</relationshipLabel>
        <relationshipName>National_Staging_Order_Sets</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Directory_Number__c</fullName>
        <externalId>false</externalId>
        <label>Directory #</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Version__c</fullName>
        <externalId>false</externalId>
        <label>Directory Version</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory</label>
        <referenceTo>Directory__c</referenceTo>
        <relationshipLabel>National Staging Order Sets</relationshipLabel>
        <relationshipName>National_Staging_Order_Sets</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>From_Number__c</fullName>
        <externalId>false</externalId>
        <label>From #</label>
        <length>4</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>From_Type__c</fullName>
        <externalId>false</externalId>
        <label>From Type</label>
        <length>1</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Header_Error_Description__c</fullName>
        <externalId>false</externalId>
        <label>Header Error Description</label>
        <length>5000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Informatica_Unique_ID__c</fullName>
        <description>Needed for SAC/RAC updates to/from ELITE</description>
        <externalId>false</externalId>
        <label>Informatica Unique ID</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IsMigratedXtrans__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>IsMigratedXtrans</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Active__c</fullName>
        <externalId>false</externalId>
        <formula>CMR_Name__r.Is_Active__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is Active</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Converted__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Converted to Opportunity and Order OR not</description>
        <externalId>false</externalId>
        <label>Is Converted?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Ready__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Ready?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Late_Order__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Late Order</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>NAT_Client_Id__c</fullName>
        <externalId>false</externalId>
        <label>NAT Client Id</label>
        <length>25</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>NAT__c</fullName>
        <externalId>false</externalId>
        <label>NAT</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>National_Credit_Status__c</fullName>
        <description>Copies the National Credit Status from CMR to National Standing Order -- this allows credit status to be displayed in related lists for efficiency in processing orders</description>
        <externalId>false</externalId>
        <formula>TEXT( CMR_Name__r.National_Credit_Status__c )</formula>
        <inlineHelpText>CMR&apos;s Credit Status as determined by the National Accounts Receivable Team</inlineHelpText>
        <label>National Credit Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Non_Elite_CMR__c</fullName>
        <externalId>false</externalId>
        <formula>IF( CMR_Name__r.Non_Elite__c =TRUE, &quot;Y&quot;, &quot;N&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Non-Elite CMR</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_of_Graphics__c</fullName>
        <externalId>false</externalId>
        <label>Number of Graphics</label>
        <summaryFilterItems>
            <field>National_Staging_Line_Item__c.National_Graphics_ID__c</field>
            <operation>notEqual</operation>
            <value></value>
        </summaryFilterItems>
        <summaryForeignKey>National_Staging_Line_Item__c.National_Staging_Header__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>National Staging Order Sets</relationshipLabel>
        <relationshipName>National_Staging_Order_Sets</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Override__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Override</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PreviousTransactionUniqueID__c</fullName>
        <externalId>false</externalId>
        <label>Previous TransactionUniqueID</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Publication_Code__c</fullName>
        <externalId>false</externalId>
        <label>Publication Code</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Publication_Company_Code_c__c</fullName>
        <externalId>false</externalId>
        <formula>Directory__r.Publication_Company_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Publication Company Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Publication_Company__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Publication Company</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>National Staging Order Sets (Publication Company)</relationshipLabel>
        <relationshipName>National_Staging_Order_Sets2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Publication_Date__c</fullName>
        <externalId>false</externalId>
        <label>Publication Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>RAC_Date__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Receipt acknowledged date from Elite - set by integration</inlineHelpText>
        <label>RAC Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Ready_for_Process__c</fullName>
        <externalId>false</externalId>
        <formula>IF(CMR_Name__r.Is_Active__c = TRUE &amp;&amp; CMR_Name__r.Bankruptcy__c =FALSE &amp;&amp; (NOT(ISPICKVAL(CMR_Name__r.c2g__CODACreditStatus__c, &quot;On Hold&quot;))) &amp;&amp; Is_Ready__c &amp;&amp; Child_Total__c != 0 &amp;&amp; Child_Total__c == ChildReadyForProcess__c, true, false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Ready for Process</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Reference_Date__c</fullName>
        <externalId>false</externalId>
        <label>Reference Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SAC_Date__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Date sent to Elite - set by integration</inlineHelpText>
        <label>SAC Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Standing_Order__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&quot;/apex/NationalStagingOrderSetDetail_V4?id=&quot;&amp; Id, Name )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Standing Order</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>State__c</fullName>
        <externalId>false</externalId>
        <label>State</label>
        <length>2</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TRANS_Code__c</fullName>
        <externalId>false</externalId>
        <label>TRANS Code</label>
        <length>1</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>To_Number__c</fullName>
        <externalId>false</externalId>
        <label>To #</label>
        <length>4</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>To_Type__c</fullName>
        <externalId>false</externalId>
        <label>To Type</label>
        <length>1</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_ID__c</fullName>
        <externalId>true</externalId>
        <label>Transaction ID</label>
        <length>150</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_Unique_ID__c</fullName>
        <externalId>false</externalId>
        <label>Transaction Unique ID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_Version_ID__c</fullName>
        <externalId>false</externalId>
        <label>Transaction Version ID</label>
        <length>21</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_Version__c</fullName>
        <externalId>false</externalId>
        <label>Transaction Version</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>National Staging Order Set</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>LAST_UPDATE</columns>
        <columns>Is_Ready__c</columns>
        <columns>Is_Converted__c</columns>
        <columns>Opportunity__c</columns>
        <columns>Directory_Edition__c</columns>
        <columns>TRANS_Code__c</columns>
        <columns>Client_Number__c</columns>
        <columns>CMR_Number__c</columns>
        <columns>Directory_Number__c</columns>
        <columns>Directory_Version__c</columns>
        <columns>OWNER.ALIAS</columns>
        <columns>CREATED_DATE</columns>
        <columns>Client_Name__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Processed_Orders</fullName>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <columns>CREATED_DATE</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>LAST_UPDATE</columns>
        <columns>Standing_Order__c</columns>
        <columns>TRANS_Code__c</columns>
        <columns>Directory_Number__c</columns>
        <columns>Directory_Edition__c</columns>
        <columns>Header_Error_Description__c</columns>
        <columns>CMR_Number__c</columns>
        <columns>Client_Number__c</columns>
        <columns>Client_Name__c</columns>
        <columns>Late_Order__c</columns>
        <columns>Ready_for_Process__c</columns>
        <columns>Is_Ready__c</columns>
        <columns>National_Credit_Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Is_Converted__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>CREATEDBY_USER</field>
            <operation>notContain</operation>
            <value>Data Migration</value>
        </filters>
        <filters>
            <field>Directory__c</field>
            <operation>contains</operation>
        </filters>
        <filters>
            <field>Directory_Edition__c</field>
            <operation>contains</operation>
        </filters>
        <filters>
            <field>TRANS_Code__c</field>
            <operation>notEqual</operation>
            <value>T</value>
        </filters>
        <filters>
            <field>TRANS_Code__c</field>
            <operation>notEqual</operation>
            <value>N</value>
        </filters>
        <label>All Processed Orders</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Unprocessed_Orders</fullName>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND 5 AND 6</booleanFilter>
        <columns>CREATED_DATE</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>NAME</columns>
        <columns>Standing_Order__c</columns>
        <columns>TRANS_Code__c</columns>
        <columns>Directory_Number__c</columns>
        <columns>Directory_Edition__c</columns>
        <columns>CMR_Number__c</columns>
        <columns>Client_Number__c</columns>
        <columns>Client_Name__c</columns>
        <columns>Header_Error_Description__c</columns>
        <columns>Late_Order__c</columns>
        <columns>Ready_for_Process__c</columns>
        <columns>Is_Ready__c</columns>
        <columns>National_Credit_Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Is_Converted__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>National_Credit_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </filters>
        <filters>
            <field>Override__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>CREATEDBY_USER</field>
            <operation>notContain</operation>
            <value>Data Migration</value>
        </filters>
        <filters>
            <field>TRANS_Code__c</field>
            <operation>notEqual</operation>
            <value>T</value>
        </filters>
        <filters>
            <field>TRANS_Code__c</field>
            <operation>notEqual</operation>
            <value>N</value>
        </filters>
        <label>All Unprocessed Orders</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Unprocessed_Orders_Krista</fullName>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <columns>CREATED_DATE</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>LAST_UPDATE</columns>
        <columns>Standing_Order__c</columns>
        <columns>TRANS_Code__c</columns>
        <columns>Directory_Number__c</columns>
        <columns>Directory_Edition__c</columns>
        <columns>Header_Error_Description__c</columns>
        <columns>CMR_Number__c</columns>
        <columns>Client_Number__c</columns>
        <columns>Client_Name__c</columns>
        <columns>Late_Order__c</columns>
        <columns>Ready_for_Process__c</columns>
        <columns>Is_Ready__c</columns>
        <columns>National_Credit_Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Is_Converted__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>National_Credit_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </filters>
        <filters>
            <field>Override__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>CREATEDBY_USER</field>
            <operation>notContain</operation>
            <value>Data Migration</value>
        </filters>
        <filters>
            <field>TRANS_Code__c</field>
            <operation>notEqual</operation>
            <value>T</value>
        </filters>
        <filters>
            <field>TRANS_Code__c</field>
            <operation>notEqual</operation>
            <value>N</value>
        </filters>
        <filters>
            <field>Directory_Number__c</field>
            <operation>equals</operation>
            <value>034414</value>
        </filters>
        <label>All Unprocessed Orders - Krista</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Completed_Transfers</fullName>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <columns>Standing_Order__c</columns>
        <columns>NAME</columns>
        <columns>TRANS_Code__c</columns>
        <columns>Is_Converted__c</columns>
        <columns>CMR_Number__c</columns>
        <columns>Client_Number__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>TRANS_Code__c</field>
            <operation>equals</operation>
            <value>T</value>
        </filters>
        <filters>
            <field>TRANS_Code__c</field>
            <operation>equals</operation>
            <value>N</value>
        </filters>
        <filters>
            <field>Is_Converted__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>Completed Transfers</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Data_Migration_X_Trans</fullName>
        <columns>NAME</columns>
        <columns>Standing_Order__c</columns>
        <columns>Directory_Edition__c</columns>
        <columns>IsMigratedXtrans__c</columns>
        <columns>Is_Ready__c</columns>
        <columns>Is_Converted__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>IsMigratedXtrans__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>Data Migration X Trans</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Needs_CMR_Credit_Approval</fullName>
        <booleanFilter>(1 AND 2 AND 3 AND 4) OR 5</booleanFilter>
        <columns>Standing_Order__c</columns>
        <columns>CREATED_DATE</columns>
        <columns>National_Credit_Status__c</columns>
        <columns>CMR_Name__c</columns>
        <columns>CMR_Number__c</columns>
        <columns>Client_Name__c</columns>
        <columns>Client_Number__c</columns>
        <columns>Directory_Edition_Number__c</columns>
        <columns>Approved_to_Process__c</columns>
        <columns>Override__c</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Is_Converted__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>National_Credit_Status__c</field>
            <operation>notEqual</operation>
            <value>Approved</value>
        </filters>
        <filters>
            <field>Override__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>CREATEDBY_USER</field>
            <operation>notEqual</operation>
            <value>Data Migration</value>
        </filters>
        <filters>
            <field>Is_Active__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>Needs CMR Credit Approval</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Transfer</fullName>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <columns>Standing_Order__c</columns>
        <columns>NAME</columns>
        <columns>Is_Converted__c</columns>
        <columns>TRANS_Code__c</columns>
        <columns>CMR_Number__c</columns>
        <columns>Client_Number__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>TRANS_Code__c</field>
            <operation>equals</operation>
            <value>T</value>
        </filters>
        <filters>
            <field>TRANS_Code__c</field>
            <operation>equals</operation>
            <value>N</value>
        </filters>
        <filters>
            <field>Is_Converted__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>Transfer</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>NSOS-{00000}</displayFormat>
        <label>National Staging Order Set Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>National Staging Order Sets</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Elite_RT</fullName>
        <active>true</active>
        <description>Elite record type</description>
        <label>Elite RT</label>
    </recordTypes>
    <recordTypes>
        <fullName>Elite_T_Transfer</fullName>
        <active>true</active>
        <description>For NSOS which have transaction type T</description>
        <label>Elite T Transfer</label>
    </recordTypes>
    <recordTypes>
        <fullName>Manual_NS_RT</fullName>
        <active>true</active>
        <description>Manual entry record</description>
        <label>Manual NS RT</label>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Client_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CMR_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Directory__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>LAST_UPDATE</customTabListAdditionalFields>
        <searchResultsAdditionalFields>CMR_Number__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Client_Number__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Client_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Directory_Number__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Directory__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Directory_Version__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>LAST_UPDATE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>National_Staging_Order_Set_List</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>National Staging Order Set List</masterLabel>
        <openType>noSidebar</openType>
        <page>NationalStagingOrderSetDetail_V4</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
