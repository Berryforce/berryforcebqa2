<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object used for informatica process. When user run Galley Parameter VF screen with Parameter new record will get created.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>GSJ_Base_URL__c</fullName>
        <externalId>false</externalId>
        <formula>LEFT($Api.Partner_Server_URL_260, FIND( &apos;/services&apos;, $Api.Partner_Server_URL_260))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Base URL</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GSJ_Business_Type__c</fullName>
        <description>Store customer type names</description>
        <externalId>false</externalId>
        <label>Business Type</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GSJ_Directory_Code__c</fullName>
        <description>Populate directory code</description>
        <externalId>false</externalId>
        <label>Directory Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GSJ_Directory_Edition_For_Informatica__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This is field is used to refer Directory edition values in Galley Staging record when directory pagination record doesn&apos;t have directory edition value.</description>
        <externalId>false</externalId>
        <label>Directory Edition For Informatica</label>
        <referenceTo>Directory_Edition__c</referenceTo>
        <relationshipLabel>Galley Staging Jobs</relationshipLabel>
        <relationshipName>Galley_Staging_Jobs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>GSJ_Directory_Edition_Ids__c</fullName>
        <externalId>false</externalId>
        <label>Directory Edition Ids</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>GSJ_Directory_Edition__c</fullName>
        <description>populate more than on directory edition Id</description>
        <externalId>false</externalId>
        <label>Directory Edition</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GSJ_Directory_Name__c</fullName>
        <externalId>false</externalId>
        <label>Directory Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GSJ_Directory_Section__c</fullName>
        <description>populate more than one Directory Section Id</description>
        <externalId>false</externalId>
        <label>Directory Section</label>
        <length>2000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>GSJ_Edition_Year__c</fullName>
        <description>Directory Edition Year</description>
        <externalId>false</externalId>
        <label>Edition Year</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GSJ_Galley_Header_Limit__c</fullName>
        <externalId>false</externalId>
        <label>Galley Header Limit</label>
        <picklist>
            <picklistValues>
                <fullName>10000</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>20000</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>30000</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>40000</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>50000</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>GSJ_Job_Status__c</fullName>
        <externalId>false</externalId>
        <label>Job Status</label>
        <picklist>
            <picklistValues>
                <fullName>Ready for Processing</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Process Started</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Process Failed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Process Completed</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>GSJ_Process_Completed_Date__c</fullName>
        <description>Informatica will populate this date once Galley Header and Child records are created.</description>
        <externalId>false</externalId>
        <label>Process Completed Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>GSJ_Processed_By_Informatica__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field updated by informatica once Galley Header and Child records are created.</description>
        <externalId>false</externalId>
        <label>Processed By Informatica</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>GSJ_Requested_User_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Requested User Name</label>
        <referenceTo>User</referenceTo>
        <relationshipLabel>Galley Staging Jobs</relationshipLabel>
        <relationshipName>Galley_Staging_Jobs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>GSJ_Telco__c</fullName>
        <description>Populate more than one Telco ID</description>
        <externalId>false</externalId>
        <label>Telco</label>
        <length>2000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>GSJ_Where_Condition__c</fullName>
        <description>While create a record, where condition should populated through code</description>
        <externalId>false</externalId>
        <label>Where Condition</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <label>Galley Staging Job</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>GSJ_Job_Status__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <columns>GSJ_Directory_Name__c</columns>
        <columns>GSJ_Directory_Section__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Galley Staging Job Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Galley Staging Jobs</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
