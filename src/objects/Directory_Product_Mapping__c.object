<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>this contains which directory/section/heading/product</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>DPM_Dir_Prod_External_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>This field is updated via a Workflow and concatenates the full 18 Character Salesforce record ids for Product and Directory to ensure uniqueness</description>
        <externalId>true</externalId>
        <inlineHelpText>This field is updated via a Workflow and concatenates the full 18 Character Salesforce record ids for Product and Directory to ensure uniqueness</inlineHelpText>
        <label>DPM Dir Prod External ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>DPM_External_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Concatenation of the Directory 18 Character SFDC ID and the Product 18 Character SFDC ID</description>
        <externalId>true</externalId>
        <inlineHelpText>Concatenation of the Directory 18 Character SFDC ID and the Product 18 Character SFDC ID</inlineHelpText>
        <label>DPM External ID</label>
        <length>36</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Directory_CASESAFEID__c</fullName>
        <externalId>false</externalId>
        <formula>CASESAFEID( Directory__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Directory CASESAFEID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Directory__r.Directory_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Directory Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory__c</fullName>
        <description>The Directory this Product is allowed to be sold into</description>
        <externalId>false</externalId>
        <inlineHelpText>The Directory this Product is allowed to be sold into</inlineHelpText>
        <label>Directory</label>
        <referenceTo>Directory__c</referenceTo>
        <relationshipLabel>Directory Product Mappings</relationshipLabel>
        <relationshipName>Directory_Product_Mappings</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Exclude_National_Upcharge__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Exclude National Upcharge</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Foreign_EAS_WP_Ban_Bill_Remaining__c</fullName>
        <externalId>false</externalId>
        <formula>CASE( Inventory_Tracking_Group__c,
&quot;WP Banners&quot;,  VALUE(Directory__r.WP_Banners_Remaining_in_Foreign_EAS__c)  , 
&quot;WP Billboards&quot;, VALUE(Directory__r.WP_Billboards_Remaining_in_Foreign_EAS__c), 
NULL)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Foreign EAS WP Ban/Bill Remaining</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Foreign_EAS_WP_Ban_Bill_Sold__c</fullName>
        <description>Amount of WP Banners or Billboards sold within an EAS Section that requires separate Inventory Tracking</description>
        <externalId>false</externalId>
        <inlineHelpText>Amount of WP Banners or Billboards sold within an EAS Section that requires separate Inventory Tracking</inlineHelpText>
        <label>Foreign EAS WP Ban/Bill Sold</label>
        <summaryFilterItems>
            <field>Product_Inventory__c.Foreign_EAS_WP_Ban_Bill_Inv_Tracking__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Product_Inventory__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Product_Inventory__c.Leader_Ad__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Product_Inventory__c.National_Ad__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>Product_Inventory__c.Directory_Product_Mapping__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>FullRate__c</fullName>
        <externalId>false</externalId>
        <label>Full Rate</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Grandfathered__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Grandfathered</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Inventory_Tracking_Group__c</fullName>
        <externalId>false</externalId>
        <label>Inventory Tracking Group</label>
        <picklist>
            <picklistValues>
                <fullName>Spine</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Spine - Half</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Front Cover</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Front Cover - Mini</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inside Front Cover</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inside Front Cover - Half</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Outside Back Cover</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Outside Back Cover - Half</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inside Back Cover</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inside Back Cover - Half</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Delivery RIDE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Tip On - 6 X 6</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Tip On - 4 X 4 / 4 X 2</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WP Banners</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WP Billboards</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>YP Leader Ad</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Local_Only__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Local Only</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Max_Quantity_Allowed__c</fullName>
        <externalId>false</externalId>
        <label>Max Quantity Allowed</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>National_Full_Rate__c</fullName>
        <externalId>false</externalId>
        <formula>ROUND(FullRate__c * IF(Directory__r.Months_for_National__c &lt; 1 ,12,Directory__r.Months_for_National__c) * (1 + IF( Exclude_National_Upcharge__c, 0, Directory__r.National_Rate_Up_for_Companion__c)) / 0.05, 0) * 0.05</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>National Full Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>National_Only__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>National Only</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Product2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product2</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Directory Product Mappings</relationshipLabel>
        <relationshipName>Directory_Product_Mappings</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product_CASESAFEID__c</fullName>
        <externalId>false</externalId>
        <formula>CASESAFEID(Product2__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product CASESAFEID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Code_UDAC__c</fullName>
        <externalId>false</externalId>
        <formula>Product2__r.ProductCode</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Code/UDAC</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quantity_Remaining__c</fullName>
        <externalId>false</externalId>
        <formula>VALUE(CASE( Inventory_Tracking_Group__c , 
&quot;Spine&quot;, IF( Directory__r.Spine_Halves_Sold__c &gt;=1, &quot;0&quot;, TEXT(Max_Quantity_Allowed__c - Directory__r.Spines_Sold__c))
,
&quot;Spine - Half&quot;, IF( Directory__r.Spines_Sold__c &gt;=1, &quot;0&quot;, TEXT(Max_Quantity_Allowed__c - Directory__r.Spine_Halves_Sold__c))
,
&quot;Front Cover&quot;, IF( Directory__r.Front_Cover_Minis_Sold__c &gt;= 1, &quot;0&quot;, TEXT(Max_Quantity_Allowed__c - Directory__r.Front_Covers_Sold__c))
,
&quot;Front Cover - Mini&quot;, IF( Directory__r.Front_Covers_Sold__c &gt;=1, &quot;0&quot;, TEXT(Max_Quantity_Allowed__c - Directory__r.Front_Cover_Minis_Sold__c))
,
&quot;Inside Front Cover&quot;, IF( Directory__r.Inside_Front_Cover_Halves_Sold__c &gt;=1, &quot;0&quot;, TEXT(Max_Quantity_Allowed__c - Directory__r.Inside_Front_Covers_Sold__c))
,
&quot;Inside Front Cover - Half&quot;, IF( Directory__r.Inside_Front_Covers_Sold__c &gt;=1, &quot;0&quot;, TEXT(Max_Quantity_Allowed__c - Directory__r.Inside_Front_Cover_Halves_Sold__c))
,
&quot;Inside Back Cover&quot;, IF( Directory__r.Inside_Back_Cover_Halves_Sold__c &gt;=1, &quot;0&quot;,  TEXT(Max_Quantity_Allowed__c - Directory__r.Inside_Back_Covers_Sold__c))
,
&quot;Inside Back Cover - Half&quot;, IF( Directory__r.Inside_Back_Covers_Sold__c &gt;=1, &quot;0&quot;,  TEXT(Max_Quantity_Allowed__c - Directory__r.Inside_Back_Cover_Halves_Sold__c))
,
&quot;Outside Back Cover&quot;, IF( Directory__r.Outside_Back_Cover_Halves_Sold__c &gt;=1, &quot;0&quot;,  TEXT(Max_Quantity_Allowed__c - Directory__r.Outside_Back_Covers_Sold__c))
,
&quot;Outside Back Cover - Half&quot;, IF( Directory__r.Outside_Back_Covers_Sold__c &gt;=1, &quot;0&quot;,  TEXT(Max_Quantity_Allowed__c - Directory__r.Outside_Back_Cover_Halves_Sold__c))
,
&quot;Delivery RIDE&quot;, TEXT(Max_Quantity_Allowed__c - Directory__r.Delivery_RIDE_Items_Sold__c)
,
&quot;Tip On - 6 X 6&quot;, IF( Directory__r.Tip_Ons_4_X_4_4_X_2_Sold__c &gt;= 1, &quot;0&quot;, TEXT(Max_Quantity_Allowed__c - Directory__r.Tip_Ons_6_X_6_Sold__c))
,
&quot;Tip On - 4 X 4 / 4 X 2&quot;, IF( Directory__r.Tip_Ons_6_X_6_Sold__c &gt;= 1, &quot;0&quot;, TEXT(Max_Quantity_Allowed__c - Directory__r.Tip_Ons_4_X_4_4_X_2_Sold__c))
,
&quot;WP Banners&quot;, Directory__r.WP_Banners_Remaining__c , 
&quot;WP Billboards&quot;, Directory__r.WP_Billboards_Remaining__c, 
 TEXT(Max_Quantity_Allowed__c - Quantity__c)))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Quantity Remaining</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <description>The quantity of the specific product sold per the specific Directory</description>
        <externalId>false</externalId>
        <inlineHelpText>The quantity of the specific product sold per the specific Directory</inlineHelpText>
        <label>Quantity Sold</label>
        <summaryFilterItems>
            <field>Product_Inventory__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Product_Inventory__c.Foreign_EAS_WP_Ban_Bill_Inv_Tracking__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Product_Inventory__c.Leader_Ad__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Product_Inventory__c.National_Ad__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>Product_Inventory__c.Directory_Product_Mapping__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Requires_Inventory_Tracking__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This Checkbox should be TRUE if this Product in this Directory requires Inventory Tracking</description>
        <externalId>false</externalId>
        <inlineHelpText>This Checkbox should be TRUE if this Product in this Directory requires Inventory Tracking</inlineHelpText>
        <label>Requires Inventory Tracking</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Round_Test__c</fullName>
        <externalId>false</externalId>
        <formula>VALUE( LEFT( TEXT( FullRate__c) , LEN( TEXT( FullRate__c)) - 1 ) )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Round Test</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>is_Active__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>is Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>strProductInventoryCombo__c</fullName>
        <description>Hidden field. Will reduce one soql query in orderline trigger while creating product inventory.</description>
        <externalId>false</externalId>
        <formula>Directory__c &amp; Product2__c</formula>
        <label>strProductInventoryCombo</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Directory Product Mapping</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Directory__c</columns>
        <columns>Directory_Code__c</columns>
        <columns>Product2__c</columns>
        <columns>Product_Code_UDAC__c</columns>
        <columns>FullRate__c</columns>
        <columns>National_Full_Rate__c</columns>
        <columns>is_Active__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>JC_Product_Mapping_by_Book</fullName>
        <columns>NAME</columns>
        <columns>Directory__c</columns>
        <columns>Directory_Code__c</columns>
        <columns>Product2__c</columns>
        <columns>Product_Code_UDAC__c</columns>
        <columns>FullRate__c</columns>
        <columns>National_Full_Rate__c</columns>
        <columns>is_Active__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Directory_Code__c</field>
            <operation>equals</operation>
            <value>016600</value>
        </filters>
        <label>JC Product Mapping by Book</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Directory Product Mapping Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Directory Product Mappings</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>is_Active__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Product2__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>is_Active__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product2__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATED_DATE</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>is_Active__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Product2__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATED_DATE</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>is_Active__c</searchFilterFields>
        <searchFilterFields>Product2__c</searchFilterFields>
        <searchResultsAdditionalFields>Product2__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATED_DATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>is_Active__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
