<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Canvass_ID_for_Dupe_Check</fullName>
        <field>Unique_Canvass_ID_Check__c</field>
        <formula>Name</formula>
        <name>Populate Canvass ID for Dupe Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Unique Canvass ID</fullName>
        <actions>
            <name>Populate_Canvass_ID_for_Dupe_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Canvass__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
