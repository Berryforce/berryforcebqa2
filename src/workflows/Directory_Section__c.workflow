<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Reset_Tear_Pages_Ready_for_Transmission</fullName>
        <field>Tear_Pages_Ready_For_Transmission__c</field>
        <literalValue>0</literalValue>
        <name>Reset Tear Pages Ready for Transmission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Dir_Sec_Toll_Free_Phrase</fullName>
        <description>Upon Creation, or if Toll Free Phrase on Directory Section is cleared, update the Directory Section&apos;s Toll Free Phrase to the value on Directory.</description>
        <field>Toll_Free_Phrase__c</field>
        <formula>Directory__r.Toll_Free_Phrase__c</formula>
        <name>Update Dir Sec Toll Free Phrase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Reset Tear Pages Ready for Transmission</fullName>
        <active>true</active>
        <description>Used to set Tear Pages Ready for Transmission to FALSE once the Future Order Start Date has been reached.</description>
        <formula>Tear_Pages_Ready_For_Transmission__c = TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reset_Tear_Pages_Ready_for_Transmission</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Directory_Section__c.BOTS_Edition_Future_Order_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Toll Free Phrase Assignment</fullName>
        <actions>
            <name>Update_Dir_Sec_Toll_Free_Phrase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Toll_Free_Phrase__c = NULL</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
