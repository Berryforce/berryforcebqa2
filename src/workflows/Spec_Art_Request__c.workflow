<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Spec_Art_Received_Date</fullName>
        <description>When the Spec Art Ad Status is set to In Progress, set the Date Graphics  Received.</description>
        <field>Date_Received__c</field>
        <formula>Now()</formula>
        <name>Populate Spec Art Received Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Telephone_Area_Code</fullName>
        <description>Populate the Telephone Area Code that is passed to Miles33.  The Advertised Telephone Number is broken down into Area Code and Telephone Number.</description>
        <field>Telephone_Areacode__c</field>
        <formula>MID(Advertised_Listed_Phone_Number__c, 2,3)</formula>
        <name>Populate Telephone Area Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Telephone_Number</fullName>
        <description>Populate the Telephone Number from the Advertised Listed Phone Number; field is passed to Miles33.</description>
        <field>Telephone_Number__c</field>
        <formula>TRIM(SUBSTITUTE(RIGHT( Advertised_Listed_Phone_Number__c ,8), &quot;-&quot;,&quot;&quot;))</formula>
        <name>Populate Telephone Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>SpecAd_Graphic_file_upload_to_Miles</fullName>
        <apiVersion>30.0</apiVersion>
        <description>Used to upload the SpecAd Graphic file to PT</description>
        <endpointUrl>https://app.informaticaondemand.com/saas/api/1/salesforceoutboundmessage/n4xEE5zdScVxkyeePD53NkT4cpaKFff5</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>kristen.maldonado@berrysfdc2.com</integrationUser>
        <name>SpecAd Graphic file upload to Miles</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Completed Spec Ad</fullName>
        <actions>
            <name>Spec_Art_Work_Completed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Spec_Art_Request__c.Miles_Status__c</field>
            <operation>contains</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>When the Graphic artist completes the Spec Ad, several work flow actions occur: Create a task for the Sales Rep to alert them the ad is complete; update the links to see the High/Low res images; populate the Date Graphics Completed date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Spec_Art_Work_Completed</fullName>
        <assignedToType>owner</assignedToType>
        <description>The Spec Request you submitted to PT is Completed and ready for review.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Spec Art Work Completed</subject>
    </tasks>
</Workflow>
