<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Unique_Lead_Assgn_Combo</fullName>
        <field>Unique_Lead_Assignment_Check_Field__c</field>
        <formula>strLeadAssgnCombo__c</formula>
        <name>Populate Unique Lead Assgn Combo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unique_Lead_Assignment_Check_Field</fullName>
        <field>Unique_Lead_Assignment_Check_Field__c</field>
        <formula>Name</formula>
        <name>Unique Lead Assignment Check Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Assignment_Name_Field</fullName>
        <field>Name</field>
        <formula>Concatenated_Lead_Assignment_Name__c</formula>
        <name>Update Lead Assignment Name Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Name Field</fullName>
        <actions>
            <name>Update_Lead_Assignment_Name_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead_Assignment_Rules__c.Area_Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate_Unique_Field_Check_Field</fullName>
        <actions>
            <name>Populate_Unique_Lead_Assgn_Combo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead_Assignment_Rules__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
