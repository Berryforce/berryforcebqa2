<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Leader_Ad_Checkbox_to_TRUE</fullName>
        <field>Leader_Ad__c</field>
        <literalValue>1</literalValue>
        <name>Set Leader Ad Checkbox to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Foreign_EAS_Inv_Track</fullName>
        <description>Used to set the Foreign EAS Inv Tracking checkbox</description>
        <field>Foreign_EAS_WP_Ban_Bill_Inv_Tracking__c</field>
        <literalValue>1</literalValue>
        <name>Update Foreign EAS Inv. Track</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Foreign EAS WP Ban%2FBill Inv%2E Tracking</fullName>
        <actions>
            <name>Update_Foreign_EAS_Inv_Track</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to set the Foreign EAS WP Ban/Bill Inv. Tracking checkbox on Product Inventory to True if the Directory Section a product is sold into is in a Directory Section that requires separate Inv. Tracking for WP Banners and Billboards</description>
        <formula>AND(Directory_Section__r.Foreign_EAS_WP_Ban_Bill_Inv_Tracking__c = TRUE, OR( ISPICKVAL(Order_Line_Item__r.Product2__r.Inventory_Tracking_Group__c, &quot;WP Banners&quot;), ISPICKVAL(Order_Line_Item__r.Product2__r.Inventory_Tracking_Group__c, &quot;WP Billboards&quot;) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UPDATE Leader Ad to TRUE</fullName>
        <actions>
            <name>Set_Leader_Ad_Checkbox_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Product Inventory record is a Leader Ad</description>
        <formula>AND( ISPICKVAL(Order_Line_Item__r.Product2__r.Print_Specialty_Product_Type__c, &quot;Leader Ad&quot;), ISCHANGED( Order_Line_Item__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
