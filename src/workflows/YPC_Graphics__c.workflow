<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>YPC_Graphics_Date_Completed</fullName>
        <description>Set the Date Graphics complete when the graphic artist completes the graphics.</description>
        <field>Date_Graphics_Complete__c</field>
        <formula>Now()</formula>
        <name>YPC Graphics Date Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>YPC_Graphics_Date_Received</fullName>
        <description>Set the Date Received when the graphics artist begins work on the graphics.</description>
        <field>Date_Graphics_Received__c</field>
        <formula>Now()</formula>
        <name>YPC Graphics Date Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>YPC_Graphics_Date_Submitted</fullName>
        <description>Set the Date Submitted when the Status is set to New (when the user clicks Submit to Graphics) which will set the YPC Graphics records status.</description>
        <field>Date_Submitted__c</field>
        <formula>Now()</formula>
        <name>YPC Graphics Date Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Completed YPC Graphics</fullName>
        <actions>
            <name>YPC_Graphics_Date_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>YPC_Graphics__c.Miles_Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>When the graphic artist completes the YPC Graphics, these work flow actions occur: Create a task for the Sales Rep to alert them the ad is complete; populate the Date Graphics Completed date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Received YPC Graphics</fullName>
        <actions>
            <name>YPC_Graphics_Date_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>YPC_Graphics__c.Miles_Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>When the graphic artist begins the YPC Graphics, set the Date Graphics Received date.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Submitted YPC Graphics</fullName>
        <actions>
            <name>YPC_Graphics_Date_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>YPC_Graphics__c.Miles_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>When the request is sent to the graphic artist (Ad Status=New when the user clicks on Submit to Graphics button on the parent DFF record) set the Date Graphics Submitted date.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
