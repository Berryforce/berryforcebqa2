<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Lead_Status_to_Unqualified</fullName>
        <description>When Lead Owner is changed to Unqualified Leads queue, change Lead Status to Unqualified</description>
        <field>Status</field>
        <literalValue>Unqualified</literalValue>
        <name>Change Lead Status to Unqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Country</fullName>
        <description>The default lead field &quot;country&quot; will be updated with the value of &quot;US&quot;</description>
        <field>Country</field>
        <formula>IF(Country = &quot;&quot;,&quot;US&quot;,
&quot;US&quot;
)</formula>
        <name>Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_First_Name</fullName>
        <description>Populate the First Name with the value &quot;Unknown&quot; at create</description>
        <field>FirstName</field>
        <formula>&quot;Unknown&quot;</formula>
        <name>Lead First Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Status_Changed_to_Open</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Lead Status Changed to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unqualified_Leads_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Unqualified_Leads</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Unqualified Leads Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Description on Lead Carries over onto Account</fullName>
        <active>false</active>
        <description>The description on the lead will carry over when converted onto the account description field.</description>
        <formula>AND(!ISBLANK(Description),!$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Country Field</fullName>
        <actions>
            <name>Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is to update the Lead &quot;Country&quot; field with the default value of US</description>
        <formula>AND( ISBLANK( Country ) , !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Description</fullName>
        <active>false</active>
        <formula>AND(!ISBLANK(Description),!$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead First Name</fullName>
        <actions>
            <name>Lead_First_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populate the First Name with the value &quot;Unknown&quot; at create</description>
        <formula>AND( ISBLANK( FirstName ) , !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Unqualified Leads</fullName>
        <actions>
            <name>Unqualified_Leads_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( ISPICKVAL(Status, &apos;Unqualified&apos;), !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Unqualified Leads Queue to Status</fullName>
        <actions>
            <name>Change_Lead_Status_to_Unqualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When changing owner to Unqualified Leads queue, status becomes Unqualified</description>
        <formula>AND( Owner:Queue.QueueName =&apos;Unqualified Leads&apos; , !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Unqualified Leads Reverse</fullName>
        <actions>
            <name>Lead_Status_Changed_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Owner:Queue.QueueName !=&apos;Unqualified Leads&apos;, !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
