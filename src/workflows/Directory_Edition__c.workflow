<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Capture_BOTS_Edition_SFDC_ID</fullName>
        <field>Current_BOTS_Edition_SFDC_ID__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Capture BOTS Edition SFDC ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_NI_Edition_SFDC_ID</fullName>
        <field>Current_NI_Edition_SFDC_ID__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Capture NI Edition SFDC ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fields_Changed_for_Telco_Report</fullName>
        <description>Populates a field on Directory Edition if one of the dates change that are sent to the Telco&apos;s in a VF email template with today&apos;s date so the business can filter a report to know which directory editions have changed to send the email.</description>
        <field>Fields_Changed_for_Telco_Report__c</field>
        <formula>TODAY()</formula>
        <name>Fields Changed for Telco Report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_BOTS_Future_Order_Start_Date</fullName>
        <field>BOTS_Future_Order_Start_Date__c</field>
        <formula>Future_Order_Start_Date__c</formula>
        <name>SET BOTS Future Order Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_BOTS_PUB_DATE</fullName>
        <field>BOTS_Edition_Publication_Date__c</field>
        <formula>Pub_Date__c</formula>
        <name>SET BOTS PUB DATE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_BOTS_Sales_Lockout_Date</fullName>
        <field>BOTS_Sales_Lockout_Date__c</field>
        <formula>Sales_Lockout__c</formula>
        <name>SET BOTS Sales Lockout Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_BOTS_Ship_Date</fullName>
        <field>BOTS_Ship_Date__c</field>
        <formula>Ship_Date__c</formula>
        <name>SET BOTS Ship Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_NI_PUB_DATE</fullName>
        <field>NI_Edition_Publication_Date__c</field>
        <formula>Pub_Date__c</formula>
        <name>SET NI PUB DATE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_SO_BOTS_Blackout_Begin_Date_on_Dir</fullName>
        <field>SO_BOTS_Blackout_Period_Begin_Date__c</field>
        <formula>Final_Service_Order_Due_to_Berry__c</formula>
        <name>SET SO BOTS Blackout Begin Date on Dir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_SO_BOTS_Blackout_End_Date_on_Dir</fullName>
        <field>SO_BOTS_Blackout_Period_End_Date__c</field>
        <formula>ALPHA_PAGES_DUE__c</formula>
        <name>SET SO BOTS Blackout End Date on Dir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_SO_NI_Blackout_Begin_Date_on_Dir</fullName>
        <field>SO_NI_Blackout_Period_Begin_Date__c</field>
        <formula>Final_Service_Order_Due_to_Berry__c</formula>
        <name>SET SO NI Blackout Begin Date on Dir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_SO_NI_Blackout_End_Date_on_Dir</fullName>
        <field>SO_NI_Blackout_Period_End_Date__c</field>
        <formula>ALPHA_PAGES_DUE__c</formula>
        <name>SET SO NI Blackout End Date on Dir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Directory__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DE_name</fullName>
        <field>Name</field>
        <formula>Directory__r.Name &amp; &quot; &quot; &amp; Edition_Code__c</formula>
        <name>Update DE name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Fields Changed for Telco Report</fullName>
        <actions>
            <name>Fields_Changed_for_Telco_Report</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates a field on Directory Edition if one of the dates change that are sent to the Telco&apos;s in a VF email template with today&apos;s date so the business can filter a report to know which directory editions have changed to send the email.</description>
        <formula>AND(NOT(ISNEW()), OR(ISCHANGED(EAS_CHANGE_CLOSE__c), ISCHANGED(DCR_Close__c), ISCHANGED(BOC__c), ISCHANGED(Final_Service_Order_Due_to_Berry__c), ISCHANGED(MANUSCRIPT_DUE_BERRY__c), ISCHANGED(Galleys_Due_Telco__c), ISCHANGED(Galleys_Due_Berry__c), ISCHANGED(MAIL_LABELS_BERRY__c), ISCHANGED(ALPHA_PROOFS_DUE__c), ISCHANGED(ALPHA_PROOFS_DUE_BERRY__c), ISCHANGED(CLASS_PROOFS_DUE__c), ISCHANGED(CLASS_PROOFS_DUE_BERRY__c), ISCHANGED(Ship_Date__c), ISCHANGED(EBD__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate BOTS Blackout Date on Directory</fullName>
        <actions>
            <name>SET_BOTS_PUB_DATE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SET_BOTS_Sales_Lockout_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SET_BOTS_Ship_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SET_SO_BOTS_Blackout_Begin_Date_on_Dir</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SET_SO_BOTS_Blackout_End_Date_on_Dir</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The Workflow used to populate the needed SO Blackout dates from the BOTS Edition onto the Directory record so the dates can be referenced via formula fields on SO Stage.</description>
        <formula>TEXT(Book_Status__c) = &quot;BOTS&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate NI Blackout Date on Directory</fullName>
        <actions>
            <name>SET_NI_PUB_DATE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SET_SO_NI_Blackout_Begin_Date_on_Dir</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SET_SO_NI_Blackout_End_Date_on_Dir</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The Workflow used to populate the needed SO Blackout dates from the NI Edition onto the Directory record so the dates can be referenced via formula fields on SO Stage.</description>
        <formula>TEXT(Book_Status__c) = &quot;NI&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate NI Edition SFDC ID on Directory</fullName>
        <actions>
            <name>Capture_NI_Edition_SFDC_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to populate the current NI Edition for a given Directory on the Directory record so it can be easily retrieved through formula fields for various uses</description>
        <formula>ISPICKVAL(Book_Status__c, &quot;NI&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Name</fullName>
        <actions>
            <name>Update_DE_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Directory_Edition__c.Pub_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
