<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Owner_Change</fullName>
        <description>Account Owner Change</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Account_Owner_Change</template>
    </alerts>
    <alerts>
        <fullName>Alert_Accounting_when_Accounts_Receivable_Control_field_changes_on_the_Account_o</fullName>
        <description>Alert Accounting when Accounts Receivable Control field changes on the Account object</description>
        <protected>false</protected>
        <recipients>
            <recipient>allison.gardner@berryforce.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GL_Account_Change_Accounts</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Account_Owner</fullName>
        <description>Once a Sales Rep deems an account as unworkable by clicking the Deactivate Account checkbox, the account changes owner to designated person to manage.</description>
        <field>OwnerId</field>
        <lookupValue>unassignedaccounts@berryforce.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Change Account Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Child_Accounts_Flmnt_Phn_Id_Update_FU</fullName>
        <field>TalusPhoneId__c</field>
        <formula>Parent.TalusPhoneId__c</formula>
        <name>Child Accounts Flmnt Phn Id Update FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Child_Accounts_Fulfillment_Id_Update_FU</fullName>
        <field>TalusAccountId__c</field>
        <formula>Parent.TalusAccountId__c</formula>
        <name>Child Accounts Fulfillment Id Update FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Country_Default</fullName>
        <field>BillingCountry</field>
        <formula>&quot;US&quot;</formula>
        <name>Country Default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dashboard_Indicator_to_True</fullName>
        <description>Update the Dashboard Indicator on the Account to True based on a time delay of the Dashboard Pending being set to True</description>
        <field>Dashboard_Account__c</field>
        <literalValue>1</literalValue>
        <name>Dashboard Indicator to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delinquency_Indicator_to_True</fullName>
        <description>For use when the Manual_Delinquency_Indicator__c is set to true and the Delinquency_Indicator__c is false.</description>
        <field>Delinquency_Indicator__c</field>
        <literalValue>1</literalValue>
        <name>Delinquency Indicator to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Disassociate</fullName>
        <description>Set the Disassociate flag to false 365 days after its been set to true</description>
        <field>Disassociated__c</field>
        <literalValue>0</literalValue>
        <name>Disassociate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Enterprise_Customer_Id_Clone_FU</fullName>
        <field>Enterprise_Cstmr_Id_Clone__c</field>
        <formula>Enterprise_Customer_ID__c</formula>
        <name>Enterprise Customer Id Clone FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FirstPartyHoldFalse</fullName>
        <description>Sets the manged 1st Party Hold field according to the unmanged First Party Hold</description>
        <field>ffps_bmatching__FirstPartyHold__c</field>
        <literalValue>0</literalValue>
        <name>FirstPartyHoldFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FirstPartyHoldTrue</fullName>
        <description>Sets the Managed 1stPartyHold field to true according to the unmanged field</description>
        <field>ffps_bmatching__FirstPartyHold__c</field>
        <literalValue>1</literalValue>
        <name>FirstPartyHoldTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Dashboard_Pending_Date</fullName>
        <field>Dashboard_Pending_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Dashboard Pending Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Telco_Settlement</fullName>
        <description>Set Telco Settlement checkbox as true on record creation if record type is Telco Partner.</description>
        <field>Is_Telco__c</field>
        <literalValue>1</literalValue>
        <name>Telco Settlement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X1stPartyHoldFalse</fullName>
        <field>ffps_bmatching__FirstPartyHold__c</field>
        <literalValue>0</literalValue>
        <name>1stPartyHoldFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X1stPartyHoldTrue</fullName>
        <field>ffps_bmatching__FirstPartyHold__c</field>
        <literalValue>1</literalValue>
        <name>1stPartyHoldTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>1stPartyHoldReset</fullName>
        <actions>
            <name>X1stPartyHoldFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.First_Party_Hold__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>Sets the FF managed field according to the value of the umanaged field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>1stPartyHoldSet</fullName>
        <actions>
            <name>X1stPartyHoldTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.First_Party_Hold__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>Sets the FF managed field according to the value of the umanaged field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Address Default</fullName>
        <actions>
            <name>Country_Default</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( ISNULL ( BillingCountry ), !$Setup.ValidationSkip__c.InActive__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Owner Change</fullName>
        <actions>
            <name>Account_Owner_Change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>To notify the new owner when the account owner is changed by the non-dedicated reassignment user</description>
        <formula>AND(ISCHANGED(OwnerId), OR($User.Alias &lt;&gt; &quot;kbo&quot;,!$Setup.ValidationSkip__c.InActive__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Child Accounts Fulfillment Id Update</fullName>
        <actions>
            <name>Child_Accounts_Flmnt_Phn_Id_Update_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Child_Accounts_Fulfillment_Id_Update_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND (5 OR 6)</booleanFilter>
        <criteriaItems>
            <field>Account.ParentId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Is_Child_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.TalusAccountId__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>notEqual</operation>
            <value>Data Migration</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>This workflow is to update Child Account Fulfillment Ids with Parent Account fulfillment values</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Dashboard Pending</fullName>
        <actions>
            <name>Set_Dashboard_Pending_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.Dashboard_Pending__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>When the Dashboard Pending is set to True by a trigger on the OLI, set the Dashboard Pending Date to Today and create a time delayed field update on the Dashboard Indicator.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Dashboard_Indicator_to_True</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deactivate Account - Owner Change</fullName>
        <actions>
            <name>Change_Account_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.Deactivate_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>Once a Sales Rep deems an account as unworkable by clicking the Deactivate Account checkbox, the account changes owner to designated person to manage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Delinquency Indicator to True</fullName>
        <actions>
            <name>Delinquency_Indicator_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Account.Manual_Delinquency_Indicator__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Delinquency_Indicator__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>For use when the Manual_Delinquency_Indicator__c is set to true and the Delinquency_Indicator__c is false.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Description on Account</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Description</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>The description on the lead will carry over when converted onto the account description field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Disassociated Account</fullName>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.Disassociated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>This workflow will set the Disassociated flag to false 12 months after it was set to ture</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Disassociate</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>365</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Enterprise Customer Id Clone</fullName>
        <actions>
            <name>Enterprise_Customer_Id_Clone_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.Enterprise_Customer_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FirstPartyHoldFalse</fullName>
        <actions>
            <name>FirstPartyHoldFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>X1stPartyHoldFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.First_Party_Hold__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>Set managed field according to unmanged field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FirstPartyHoldTrue</fullName>
        <actions>
            <name>FirstPartyHoldTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.First_Party_Hold__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>Set managed field according to unmanged field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>GL Account Change - Accounts</fullName>
        <actions>
            <name>Alert_Accounting_when_Accounts_Receivable_Control_field_changes_on_the_Account_o</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Alert Accounting when the Accounts Receivable Control field changes</description>
        <formula>AND( ISCHANGED( c2g__CODAAccountsReceivableControl__c ), !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Telco Settlement</fullName>
        <actions>
            <name>Telco_Settlement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(RecordType.DeveloperName == &apos;Telco_Partner&apos;, !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Tier Override</fullName>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.Tier_Override__c</field>
            <operation>notEqual</operation>
            <value>None</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>This is a workflow to manually override the STORM-provided Customer Tier</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
