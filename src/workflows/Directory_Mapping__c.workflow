<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Unique_Directory_Name_Check</fullName>
        <field>CS_Unique_Directory_Name_Check__c</field>
        <formula>Directory__r.Name &amp; Telco__r.Name</formula>
        <name>Unique Directory Name Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Directory_Mapping_Name</fullName>
        <field>Name</field>
        <formula>DM_Directory_Mapping_Name_f__c</formula>
        <name>Update Directory Mapping Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Directory_Mapping_Unique_Field</fullName>
        <field>Unique_Directory_Name_Check__c</field>
        <formula>DM_Directory_Mapping_Name_f__c</formula>
        <name>Update Directory Mapping Unique Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Directory Mapping Unique Check</fullName>
        <actions>
            <name>Update_Directory_Mapping_Unique_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Directory_Mapping__c.Canvass_Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Unique Directory Name Check</fullName>
        <actions>
            <name>Unique_Directory_Name_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1 = 1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Directory Mapping Name</fullName>
        <actions>
            <name>Update_Directory_Mapping_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This WF will update &quot;Name&quot; field if there any change on Canvas, Directory and Telco</description>
        <formula>IF(Name != DM_Directory_Mapping_Name_f__c, true, false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
