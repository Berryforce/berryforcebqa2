<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ABD_Backout_Disconnect_False</fullName>
        <description>When the ABD Pending Review Backout Batch is processed, set Disconnected via ABD and Disconnect to False, and set Disconnect Reason to -None.</description>
        <field>Disconnected__c</field>
        <literalValue>0</literalValue>
        <name>ABD Backout Disconnect False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ABD_Backout_Disconnect_Reaso</fullName>
        <description>When the ABD Pending Review Backout process is run, set Disconnected via ABD and Disconnect = False, set Disconnect Reason to -None-.</description>
        <field>Disconnect_Reason__c</field>
        <name>ABD Backout Disconnect Reaso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ABD_Backout_Disconnected_Via_ABD</fullName>
        <description>If the ABD Pending Backout Batch is run, set Disconnected via ABD and Disconnected= to False, and set Disconnected Reason to -None-.</description>
        <field>Disconnected_Via_ABD__c</field>
        <literalValue>0</literalValue>
        <name>ABD Backout Disconnected Via ABD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_LCN_Change_Date</fullName>
        <field>LCN_Change_Date__c</field>
        <formula>DATEVALUE( LastModifiedDate )</formula>
        <name>Capture LCN Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_LCN_Required</fullName>
        <field>Requires_LCN_Telco_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Check LCN Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Listing_Additional</fullName>
        <description>If the Listing Type = Additional then update the Record Type to Additional which will change the page layout</description>
        <field>RecordTypeId</field>
        <lookupValue>Additional_Listing</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Listing - Additional</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Listing_Main</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Main_Listing</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Listing - Main</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Listing_NOSO</fullName>
        <field>RecordTypeId</field>
        <lookupValue>NOSO</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Listing - NOSO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Normalize_set_to_Null</fullName>
        <field>Normalized_Last_Name_Business_Name__c</field>
        <name>Normalize set to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Normalized_Name_to_Null</fullName>
        <description>Setting Normalized Last name to Null</description>
        <field>Normalized_Last_Name_Business_Name__c</field>
        <name>Normalized Name to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Normalized_Street_to_Null</fullName>
        <description>Setting Normalized street Name to Null</description>
        <field>Normalized_Listing_Street_Name__c</field>
        <name>Normalized Street to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_CLEC_Provider_Name</fullName>
        <field>CLEC_Provider_Name__c</field>
        <formula>CLEC_Provider__r.Name</formula>
        <name>Populate CLEC Provider Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Telco_Provider_Name</fullName>
        <field>Telco_Provider_Name__c</field>
        <formula>Telco_Provider__r.Name</formula>
        <name>Populate Telco Provider Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_City</fullName>
        <field>Previous_City__c</field>
        <formula>PRIORVALUE(Listing_City__c)</formula>
        <name>Previous City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Last_Name_Business_Name</fullName>
        <field>Previous_Last_Name_Business_Name__c</field>
        <formula>PRIORVALUE( Name )</formula>
        <name>Previous Last Name/Business Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_PO_Box</fullName>
        <field>Previous_PO_Box__c</field>
        <formula>PRIORVALUE(  Listing_PO_Box__c )</formula>
        <name>Previous PO Box #</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Phone</fullName>
        <field>Previous_Phone__c</field>
        <formula>PRIORVALUE(Phone__c)</formula>
        <name>Previous Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Postal_Code</fullName>
        <field>Previous_Postal_Code__c</field>
        <formula>PRIORVALUE(Listing_Postal_Code__c)</formula>
        <name>Previous Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_State</fullName>
        <field>Previous_State__c</field>
        <formula>PRIORVALUE(Listing_State__c)</formula>
        <name>Previous State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Street_Name</fullName>
        <field>Previous_Street_Name__c</field>
        <formula>PRIORVALUE(Listing_Street__c)</formula>
        <name>Previous Street Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Street_Number</fullName>
        <field>Previous_Street_Number__c</field>
        <formula>PRIORVALUE(Listing_Street_Number__c)</formula>
        <name>Previous Street Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_Sort_As_Full</fullName>
        <field>Sort_As_Full__c</field>
        <formula>IF( Manual_Sort_As_Override__c != NULL, Manual_Sort_As_Override__c,
     IF( Normalized_Last_Name_Business_Name__c != NULL, Normalized_Last_Name_Business_Name__c, Name)
  )
 &amp;
 IF( First_Name__c  != NULL, 
     IF( Normalized_First_Name__c != NULL, &quot; &quot; &amp; Normalized_First_Name__c, &quot; &quot; &amp;  First_Name__c), 
     NULL)
 &amp;
 IF( Listing_Street_Number__c != NULL, &quot; &quot; &amp; Listing_Street_Number__c, NULL)
 &amp;
 IF( Listing_Street__c != NULL, &quot; &quot; &amp; Listing_Street__c, NULL)
 &amp;
 IF( Listing_City__c != NULL, &quot; &quot; &amp; Listing_City__c, NULL)
 &amp;
 IF( Phone_Unformatted__c != NULL, &quot; &quot; &amp; Phone_Unformatted__c, NULL)</formula>
        <name>SET Sort As Full</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Requires_LCN_Checkbox</fullName>
        <field>Requires_LCN_Telco_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Set Requires LCN Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Listing_Area_Code</fullName>
        <description>Updates the Phone Number&apos;s Area Code text field</description>
        <field>Area_Code_WF__c</field>
        <formula>LEFT(
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Phone__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 3)</formula>
        <name>Update Listing Area Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Listing_Exchange</fullName>
        <description>Used to populate the Listing Phone Number&apos;s Exchange</description>
        <field>Exchange_WF__c</field>
        <formula>MID( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Phone__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;),
4, 3)</formula>
        <name>Update Listing Exchange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Listing_Line</fullName>
        <description>Used to populate the Listing Phone Number&apos;s Line</description>
        <field>Line_WF__c</field>
        <formula>RIGHT( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Phone__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 4)</formula>
        <name>Update Listing Line</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ABD Pending Review Backout</fullName>
        <actions>
            <name>ABD_Backout_Disconnect_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ABD_Backout_Disconnect_Reaso</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ABD_Backout_Disconnected_Via_ABD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If the ABD Pending Review Backout is processed, the ABD Pending Review flag is changed from True to False, and for Disconnects, set the Disconnect via ABD = False, set Disconnect Reason to Null.</description>
        <formula>AND( ABD_Pending_Review__c = FALSE, Disconnected_Via_ABD__c = TRUE, Disconnected__c = TRUE,!$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Capture LCN Change Date</fullName>
        <actions>
            <name>Capture_LCN_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Listing__c.Requires_LCN_Telco_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>DEPRECATED - This field update is captured in other workflows that set the Requires LCN field.
When Required LCN Flag = TRUE, Capture the Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Listing - Additional</fullName>
        <actions>
            <name>Listing_Additional</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Listing__c.Listing_Type__c</field>
            <operation>equals</operation>
            <value>Additional</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>If the Listing Type = Additional then update the Record Type to Additional which will change the page layout</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Listing - Main</fullName>
        <actions>
            <name>Listing_Main</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Listing__c.Listing_Type__c</field>
            <operation>equals</operation>
            <value>Main</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>If the Listing Type = Main then update the Record Type to Main which will change the page layout</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Listing - NOSO</fullName>
        <actions>
            <name>Listing_NOSO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Listing__c.Listing_Type__c</field>
            <operation>equals</operation>
            <value>NOSO</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DevBatchUser_UserName</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notContain</operation>
            <value>$Label.DataMigr_UserName</value>
        </criteriaItems>
        <description>If the Listing Type = NOSO then update the Record Type to NOSO which will change the page layout</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Listing Area Code%2C Exchange%2C Line Update</fullName>
        <actions>
            <name>Update_Listing_Area_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Listing_Exchange</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Listing_Line</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to capture the Listing Phone Number&apos;s Area Code, Exchange, and Line into their respective fields</description>
        <formula>AND(OR( ISCHANGED( Phone__c ),     Area_Code_WF__c = NULL,     Exchange_WF__c = NULL,     Line_WF__c = NULL ), !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Normalized Name to Null</fullName>
        <actions>
            <name>Normalize_set_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When the Listing Name is changed manually setting Normalized name to Null</description>
        <formula>AND( NOT(ISNEW()), ISCHANGED(Name) ,NOT( ISBLANK( Normalized_Last_Name_Business_Name__c ) ),!$Setup.ValidationSkip__c.InActive__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Normalized Street  to Null</fullName>
        <actions>
            <name>Normalized_Name_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Normalized_Street_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When the Listing street is changed manually setting Normalized street Name to Null</description>
        <formula>AND(NOT(ISNEW()), ISCHANGED( Listing_Street__c ),OR(NOT( ISBLANK(Normalized_Listing_Street_Name__c) ),NOT( ISBLANK( Normalized_Last_Name_Business_Name__c ))), !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Telco %26 CLEC Names</fullName>
        <actions>
            <name>Populate_CLEC_Provider_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Telco_Provider_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(Telco_Provider_Name__c = NULL, CLEC_Provider_Name__c = NULL),!$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Previous Listing Information</fullName>
        <actions>
            <name>Capture_LCN_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_Last_Name_Business_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_PO_Box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_Street_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_Street_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Requires_LCN_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When listing address info or phone or disconnect are updated, or a listing is created, set previous info fields and set the Requires LCN checkbox to true and LCN Change Date to today if it is not the Integration User or Service Order Keyer profile.</description>
        <formula>AND(OR(ISNEW(),ISCHANGED( Listing_Street_Number__c ), ISCHANGED( Listing_Street__c ), ISCHANGED( Listing_City__c ), ISCHANGED(Listing_State__c), ISCHANGED(Listing_Postal_Code__c), ISCHANGED(LST_Last_Name_Business_Name__c), ISCHANGED(Phone__c), ISCHANGED(Listing_PO_Box__c), ISCHANGED(Disconnected__c)), OR($Profile.Name = &quot;Sales Rep&quot;, $Profile.Name = &quot;Customer Service&quot;, $Profile.Name = &quot;Customer Service Manager&quot;, $Profile.Name = &quot;Account Manager&quot;),  RecordTypeId  &lt;&gt;  $Label.TestListingNOSORT)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sort As FULL</fullName>
        <actions>
            <name>SET_Sort_As_Full</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Workflow is used to populate and update the Sort As FULL field so it can be correctly referenced by the OLIs that are related to the given Listing</description>
        <formula>AND(OR( Sort_As_Full__c = NULL,     ISCHANGED(Sort_As_Full__c),     ISCHANGED(LST_Last_Name_Business_Name__c),     ISCHANGED(First_Name__c),     ISCHANGED(Normalized_Last_Name_Business_Name__c),     ISCHANGED(Normalized_First_Name__c),     ISCHANGED(Manual_Sort_As_Override__c),     ISCHANGED(Listing_Street_Number__c),     ISCHANGED(Listing_Street__c),     ISCHANGED(Listing_City__c),     ISCHANGED(Phone__c) ), !$Setup.ValidationSkip__c.InActive__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
