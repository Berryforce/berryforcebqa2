<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Capture_Edition_Renewal_Date</fullName>
        <description>To capture the date an OLI is renewed into a new Directory Edition</description>
        <field>Directory_Edition_Renewal_Date__c</field>
        <formula>TODAY()</formula>
        <name>Capture Edition Renewal Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Previous_Edition</fullName>
        <description>Used to capture the name of the OLI&apos;s previous Directory Edition</description>
        <field>Previous_Directory_Edition__c</field>
        <formula>PRIORVALUE( Directory_Edition_Name__c)</formula>
        <name>Capture Previous Edition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_WP_Logo_Header</fullName>
        <field>WP_Logo_Header__c</field>
        <literalValue>1</literalValue>
        <name>Check WP Logo Header</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Effective_Date_Field_Update</fullName>
        <field>Effective_Date__c</field>
        <formula>TODAY()</formula>
        <name>Effective Date Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OLI_Rate_Change</fullName>
        <field>Rate_changed__c</field>
        <literalValue>1</literalValue>
        <name>OLI Rate Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_Caption_Anchor_SORT_AS</fullName>
        <description>Used to copy the Caption/Anchor Sort As FORMULA value into the usable text field for Pagination. VERY IMPORTANT</description>
        <field>Sort_As__c</field>
        <formula>SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(
Sort_As_FORMULA__c,
&quot;  &quot;, &quot; &quot;), &quot;   &quot;, &quot; &quot;), &quot;    &quot;, &quot; &quot;), &quot;     &quot;, &quot; &quot;), &quot;      &quot;, &quot; &quot;), &quot;       &quot;, &quot; &quot;), &quot;        &quot;, &quot; &quot;), &quot;         &quot;, &quot; &quot;)</formula>
        <name>SET Caption/Anchor SORT AS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Caption_Header_Record_ID</fullName>
        <field>Anchor_Caption_Header_Record_ID__c</field>
        <formula>IF( Anchor_Listing_Caption_Header__c != NULL,
CASESAFEID( Anchor_Listing_Caption_Header__r.Id ),
CASESAFEID( Scoped_Caption_Header__r.Id ))</formula>
        <name>Set Caption Header Record ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OLI_WP_Logo_Product_Checkbox</fullName>
        <field>WP_Logo_Product__c</field>
        <literalValue>1</literalValue>
        <name>Set OLI WP Logo Product Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Print_Product_Type</fullName>
        <field>Print_Product_Type__c</field>
        <formula>TEXT(Product2__r.Print_Product_Type__c)</formula>
        <name>Set Print Product Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reqs_Scoped_Suppress_Process_TRUE</fullName>
        <description>This Field Update will set the Requires Scoped Suppression Processing to TRUE</description>
        <field>Requires_Scoped_Suppression_Processing__c</field>
        <literalValue>1</literalValue>
        <name>Set Reqs Scoped Suppress Process TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture Print Product Type</fullName>
        <actions>
            <name>Set_Print_Product_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(Print_Product_Type__c = NULL, ISCHANGED( Product2__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Effective Date Update</fullName>
        <actions>
            <name>Effective_Date_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order_Line_Items__c.Effective_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Line_Items__c.Effective_Date__c</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rate Change Update</fullName>
        <actions>
            <name>OLI_Rate_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(UnitPrice__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Requires Suppression Processing Set</fullName>
        <actions>
            <name>Set_Reqs_Scoped_Suppress_Process_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Workflow is used to check the conditions needed for setting the Requires Scoped Suppression Processing flag.</description>
        <formula>AND(OR(TEXT(Product2__r.Print_Product_Type__c) = &quot;Listing&quot;, 
TEXT(Product2__r.Print_Product_Type__c) = &quot;In-Column&quot;), 
Listing__r.Number_of_Scoped_Listings__c &gt;= 1, 
Is_Child__c = FALSE, 
isCanceled__c = FALSE 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set OLI WP Logo Product Checkbox</fullName>
        <actions>
            <name>Set_OLI_WP_Logo_Product_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to check the WP Logo Product checkbox if an Order Line Item&apos;s Product is a WP Logo Product</description>
        <formula>OR( AND( ISCHANGED(Product2__c),          ISPICKVAL(Product2__r.Print_Specialty_Product_Type__c , &quot;WP Logo&quot;)        )     ,     ISPICKVAL(Product2__r.Print_Specialty_Product_Type__c , &quot;WP Logo&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set WP Logo Header Checkbox</fullName>
        <actions>
            <name>Check_WP_Logo_Header</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to check the WP Logo Header checkbox if an Order Line Item is related to a Caption Header with a WP Logo Product</description>
        <formula>OR( AND( ISCHANGED( Anchor_Listing_Caption_Header__c ),          Anchor_Listing_Caption_Header__r.WP_Logo_Product__c = TRUE )     ,     AND( ISCHANGED( Scoped_Caption_Header__c ),          Scoped_Caption_Header__r.WP_Logo_Product__c = TRUE) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Caption Header Record ID</fullName>
        <actions>
            <name>Set_Caption_Header_Record_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(ISCHANGED( Anchor_Listing_Caption_Header__c), ISCHANGED( Scoped_Caption_Header__c ),  Anchor_Caption_Header_Record_ID__c = NULL))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Caption%2FAnchor Sort As</fullName>
        <actions>
            <name>SET_Caption_Anchor_SORT_AS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(      ISNEW(),     Sort_As__c = NULL,     Sort_As__c != Sort_As_FORMULA__c,     ISCHANGED( Sort_As__c),     ISCHANGED( Listing__c),     ISCHANGED( Anchor_Listing_Caption_Header__c),           ISCHANGED( Scoped_Caption_Header__c),     ISCHANGED( Trademark_Finding_Line_OLI__c)   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Upon Edition Change</fullName>
        <actions>
            <name>Capture_Edition_Renewal_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Capture_Previous_Edition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(ISNEW()), ISCHANGED( Directory_Edition__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
