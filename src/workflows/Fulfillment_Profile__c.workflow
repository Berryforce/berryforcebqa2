<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Business_Url_FU</fullName>
        <field>business_url__c</field>
        <formula>&apos;http://&apos; + business_url__c</formula>
        <name>Business Url FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Business Url</fullName>
        <actions>
            <name>Business_Url_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Fulfillment_Profile__c.business_url__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Fulfillment_Profile__c.business_url__c</field>
            <operation>notContain</operation>
            <value>http</value>
        </criteriaItems>
        <description>To append business url field with &apos;http&apos; to the field value</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
