<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Caption_Help_Request_Completed</fullName>
        <description>Caption Help Request Completed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Pagination_Templates/Caption_Help_Request_Completed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Caption_Help_Request_to_Queue</fullName>
        <description>To assign newly created caption help requests to the Captions Queue</description>
        <field>OwnerId</field>
        <lookupValue>Caption_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Caption Help Request to Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Caption Help Request Completed</fullName>
        <actions>
            <name>Caption_Help_Request_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Caption_Help_Request__c.Request_Completed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>To notify the originator that their request has been completed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Caption Help Request to Queue</fullName>
        <actions>
            <name>Caption_Help_Request_to_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To assign newly created caption help requests to the Captions Queue</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
