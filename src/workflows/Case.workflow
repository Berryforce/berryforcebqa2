<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Close_Auto</fullName>
        <description>For case close auto notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Template/Case_Close</template>
    </alerts>
    <alerts>
        <fullName>Claim_Approved_Alert</fullName>
        <description>Claim Approved Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Template/Claim_Approved_Notification</template>
    </alerts>
    <alerts>
        <fullName>Dff_Fulfillment_Case_Email_Alert</fullName>
        <description>Dff_Fulfillment_Case_Email_Alert</description>
        <protected>false</protected>
        <recipients>
            <field>DFF_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TeamTrack_Case_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Manager_Rejected_Claim</fullName>
        <description>Manager Rejected Claim</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Template/Claim_Rejected_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_the_contact_whenever_the_case_is_opened_saved</fullName>
        <description>Notify the contact whenever the case is opened/saved</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Template/Case_Open</template>
    </alerts>
    <alerts>
        <fullName>Send_Case_Close_Email</fullName>
        <description>Send Case Close Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Template/Case_Close</template>
    </alerts>
    <alerts>
        <fullName>Send_Case_Open_Email</fullName>
        <description>Send Case Open Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Template/Case_Open</template>
    </alerts>
    <fieldUpdates>
        <fullName>CS_Non_Advertiser_to_Closed</fullName>
        <description>Changes the CS Non-Advertiser RT to Non-Advertiser Closed</description>
        <field>RecordTypeId</field>
        <lookupValue>CS_Non_Advertiser_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CS Non-Advertiser to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Closed_By_User</fullName>
        <description>To capture the user who last modified the Case to make the Case Closed</description>
        <field>Case_Closed_By__c</field>
        <formula>LastModifiedBy.FirstName &amp;&quot; &quot;&amp; LastModifiedBy.LastName</formula>
        <name>Capture Closed By User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Claim_RT_to_Claim_Closed_RT</fullName>
        <description>To update the Claim RT to Claim Closed RT upon closing a Claim</description>
        <field>RecordTypeId</field>
        <lookupValue>CS_Claim_Closed_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Claim RT to Claim Closed RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Inquiry_RT_to_Inquiry_Closed_RT</fullName>
        <description>Updating an Inquiry_RT to Inquiry_Closed_RT</description>
        <field>RecordTypeId</field>
        <lookupValue>CS_Inquiry_Closed_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case Inquiry RT to Inquiry Closed RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_to_CS_Internal_Berry_Team</fullName>
        <field>OwnerId</field>
        <lookupValue>CS_Internal_Berry_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner to CS Internal Berry Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Phone_Update_if_Null</fullName>
        <description>If Phone field (Main_Listed_Phone) is blank, set the field equal to Account.Phone for case on Account OR Lead_Name__r.Phone if case on Lead.</description>
        <field>Main_Listed_Phone_Number__c</field>
        <formula>IF(AccountId &lt;&gt; &quot;&quot;, Account.Phone,
IF(Lead_Name__c &lt;&gt; &quot;&quot;, Lead_Name__r.Phone,
  &quot;&quot;))</formula>
        <name>Case Phone Update if Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Claim_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CS_Claim_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Claim RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Inquiry_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CS_Inquiry_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Inquiry RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_RT_For_cancellation_close</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Cancellation_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change RT For cancellation close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type</fullName>
        <description>Change Record Type of Case when Case = Closed and Record Type = National Claim</description>
        <field>RecordTypeId</field>
        <lookupValue>National_Claim_Hide_Button</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Claim_RT_to_Claim_Closed_RT</fullName>
        <description>To change a Claim RT to a Claim Closed RT</description>
        <field>RecordTypeId</field>
        <lookupValue>CS_Claim_Closed_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Claim RT to Claim Closed RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fulfillment_Case_BerryDigitalSupport</fullName>
        <field>OwnerId</field>
        <lookupValue>Berry_Digital_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Fulfillment_Case_BerryDigitalSupport</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fulfillment_Dff_Owner_Email_FU</fullName>
        <field>DFF_Owner_Email__c</field>
        <formula>Digital_Product_Requirement__r.CreatedBy.Email</formula>
        <name>Fulfillment_Dff_Owner_Email_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>High_Priority</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Change Priority to High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Non_Cust_RT_to_Non_Cust_Closed_RT</fullName>
        <description>Updating the Record Type field from Non Customer RT to Non Customer Closed RT</description>
        <field>RecordTypeId</field>
        <lookupValue>CS_Non_Customer_Closed_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Non-Cust RT to Non-Cust Closed RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Overcomp_to_True</fullName>
        <field>Overcompensation_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Overcomp to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recd_From_Sales</fullName>
        <description>Received from = sales</description>
        <field>Received_From__c</field>
        <literalValue>Sales</literalValue>
        <name>Recd From = Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revenue_Assurance_Case</fullName>
        <field>Revenue_Assurance__c</field>
        <literalValue>1</literalValue>
        <name>Revenue Assurance Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ApprovedTrue</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Approved to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_Checkbox_true</fullName>
        <field>Zero_Cancel_Fee_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Approved Checkbox true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_True</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Approved to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_to_True</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Approved to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approvedto_True</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Approved to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Needs_Overcompensation_Approval_True</fullName>
        <field>Needs_Overcompensation_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Set Needs Overcompensation Approval True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Needs_SOA_Approval_to_TRUE</fullName>
        <field>Needs_SOA_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Set Needs SOA Approval to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_New_Cancellation_Case_to_True</fullName>
        <field>CheckNewlyCreatedCancellationCase__c</field>
        <literalValue>1</literalValue>
        <name>Set New Cancellation Case to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Overcomp_Approved_to_TRUE</fullName>
        <field>Overcompensation_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Overcomp Approved to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Rejected_Checkbox_true</fullName>
        <field>Zero_Cancel_Fee_Request_Rejected__c</field>
        <literalValue>1</literalValue>
        <name>Set Rejected Checkbox true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SOA_Approved_to_TRUE</fullName>
        <field>SOA_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set SOA Approved to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Set Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Set Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Telco_Approved_to_TRUE</fullName>
        <field>Telco_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Telco Approved to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Telco_Approved_t_True</fullName>
        <field>Telco_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Telco Approved to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_to_TRUE</fullName>
        <description>Update the Approved__c checkbox to TRUE once Approved</description>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update Approved to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Approved_to_FALSE</fullName>
        <field>Approved__c</field>
        <literalValue>0</literalValue>
        <name>Update Case Approved to FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Approved_to_TRUE</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update Case Approved to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Client_Tier_At_Case_Close</fullName>
        <description>The field update to pull in the Client Tier at the time the Case was closed</description>
        <field>Customer_Tier_At_Case_Close__c</field>
        <formula>TEXT(Account.Client_Tier__c)</formula>
        <name>Update Client Tier At Case Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Non_Customer_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CS_Non_Customer_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update the Non Customer RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Case Approved to True</fullName>
        <actions>
            <name>Set_Approved_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 AND 3 AND 5) AND (2 AND 4 AND 6)) OR (1 AND 3 AND 5)OR (1 AND 7)</booleanFilter>
        <criteriaItems>
            <field>Case.SOA_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_SOA_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Overcompensation_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_Overcompensation_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Telco_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_Telco_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_SOA_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Case Approved to TRUE</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Owner to CS Internal Berry Team</fullName>
        <actions>
            <name>Case_Owner_to_CS_Internal_Berry_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Category__c</field>
            <operation>equals</operation>
            <value>Internal Berry Team</value>
        </criteriaItems>
        <description>On case creation, change owner to CS Internal Berry Team queue when case type = Internal Berry Team</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Approved to False</fullName>
        <actions>
            <name>Update_Case_Approved_to_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 4) OR (2 AND 5) OR (3 AND 6)</booleanFilter>
        <criteriaItems>
            <field>Case.Needs_SOA_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_Overcompensation_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_Telco_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SOA_Approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Overcompensation_Approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Telco_Approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Approved to True</fullName>
        <actions>
            <name>Set_Approved_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 4 AND 6) OR (3 AND 2 AND 6) OR (5 AND 2 AND 4) OR (1 AND 3 AND 5) OR (1 AND 3 AND 6) OR (1 AND 5 AND 4) OR (3 AND 5 AND 2)</booleanFilter>
        <criteriaItems>
            <field>Case.SOA_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_SOA_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Overcompensation_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_Overcompensation_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Telco_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_Telco_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Case Approved to TRUE</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Case Closed By</fullName>
        <actions>
            <name>Capture_Closed_By_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Case is Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Claim RT to Closed Claim RT</fullName>
        <actions>
            <name>Case_Claim_RT_to_Claim_Closed_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CS Claim</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Upon closing a Claim RT Case, the workflow will update the Claim RT to a Closed Claim RT. Used for Page Layout reassignment.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Customer Tier At Case Close</fullName>
        <actions>
            <name>Update_Client_Tier_At_Case_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Category__c</field>
            <operation>equals</operation>
            <value>Claim,Inquiry</value>
        </criteriaItems>
        <description>To update the Customer Tier At Case Close field to pull in the value at the time of close.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Inquiry RT to Inquiry Closed RT</fullName>
        <actions>
            <name>Case_Inquiry_RT_to_Inquiry_Closed_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CS Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used to update an Inquiry RT Case to an Inquiry Closed RT when the case is Closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Non-AdvertiserRT to Non-Customer Closed RT</fullName>
        <actions>
            <name>CS_Non_Advertiser_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CS Non Advertiser</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Use to change a Non-Advertiser RT Case to a Non-AdvertiserClosed RT when closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Non-Customer RT to Non-Customer Closed RT</fullName>
        <actions>
            <name>Non_Cust_RT_to_Non_Cust_Closed_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CS Non Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Use to change a Non-Customer RT Case to a Non-Customer Closed RT when closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Telco Approval Notes is not null</fullName>
        <actions>
            <name>Update_Case_Approved_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Needs_Telco_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_SOA_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Needs_Overcompensation_Approval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Telco_Approval_Notes__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If Needs Telco Approval equal TRUE and Telco Approval Notes is not NULL, Case should be Approved</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Account%27s Telco</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Category__c</field>
            <operation>equals</operation>
            <value>Non-Claim,Claim,From Sales</value>
        </criteriaItems>
        <description>Used to populate the Account&apos;s Telco field on Case upon Case creation for Claims, Non Claims, and Sales Initiated Cases.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update Main Listed Phone</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CS Claim,CS Inquiry,CS Internal Berry Team</value>
        </criteriaItems>
        <description>Used to default/update the Main Listed Phone field on a Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change RT For closed cancel case</fullName>
        <actions>
            <name>Change_RT_For_cancellation_close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cancellation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change RT for Claim Category</fullName>
        <actions>
            <name>Change_Claim_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Category__c</field>
            <operation>equals</operation>
            <value>Claim</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change RT for Inquiry Category</fullName>
        <actions>
            <name>Change_Inquiry_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Category__c</field>
            <operation>equals</operation>
            <value>Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change RT for Non Customer Category</fullName>
        <actions>
            <name>Update_the_Non_Customer_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Case_Category__c</field>
            <operation>equals</operation>
            <value>Non-Customer</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Dff_Case_To_BerryDigitalSupport_Queue</fullName>
        <actions>
            <name>Fulfillment_Case_BerryDigitalSupport</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Fulfillment Case RT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Domain Issue,Vendor Fulfillment Issue,YPC Issue - Invalid Data,YPC Issue - Misc.,Video Issue,Cancellation Issue,YPC Issue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TeamTrack_tableIdItemId__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Workflow rule to assign Case to Berry Digital Support Queue, whenever a Fulfillment related Case is created other than TeamTrack user</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Dff_Fulfillment_Case_Email_Alert</fullName>
        <actions>
            <name>Dff_Fulfillment_Case_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR (1 AND 4 AND 5 AND 6 AND 7)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Fulfillment Case RT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TeamTrack_tableIdItemId__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Invalid DFF Data,Missing Info on DFF,YPC Issue - Limited Inventory,General Query,Website Issue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Category__c</field>
            <operation>equals</operation>
            <value>Query</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TeamTrack_tableIdItemId__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>Berry Digital Support</value>
        </criteriaItems>
        <description>Workflow to send an email alert to Dff Owner, when a Case is created from Dff by any user other than Team Track User and also whenever Team Track creates a Case in Salesforce for Dff Owner requesting for more information</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Dff_Owner_Email_on_Case</fullName>
        <actions>
            <name>Fulfillment_Dff_Owner_Email_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Fulfillment Case RT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TeamTrack_tableIdItemId__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Workflow will update &apos;Dff Owner Email&apos; field with Dff CreatedBy User Email Id</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Hide Initiate Claim Button</fullName>
        <actions>
            <name>Change_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>National Claim</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Hide Initiate Claim Button when Case = Closed, Record Type = National Claim</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Main Listed Number Update</fullName>
        <actions>
            <name>Case_Phone_Update_if_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Main_Listed_Phone_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Talus Case RT,Talus Fulfillment ICR RT,Billing Case,Bug,Enhancement,CORE Migrated Case,Email2Case,National Claim</value>
        </criteriaItems>
        <description>If Phone (Main_Listed_Number) is empty, update to Account.Phone or Lead.Phone as appropriate.0</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Needs Overcompensation to True</fullName>
        <actions>
            <name>Set_Needs_Overcompensation_Approval_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Total Monthly BCC is greater Max BCC please set the &quot;Needs Overcompensation Approval&quot; flag = True.</description>
        <formula>Total_Monthly_BCC__c &gt; Total_Max_BCC__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Needs SOA Approval to True</fullName>
        <actions>
            <name>Set_Needs_SOA_Approval_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (3 AND 4) OR ((5 AND 6) OR (7 AND 8))</booleanFilter>
        <criteriaItems>
            <field>Case.Sub_Credit__c</field>
            <operation>greaterOrEqual</operation>
            <value>150</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Customer Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Credit__c</field>
            <operation>greaterOrEqual</operation>
            <value>1000</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>Customer Service Manager/Supervisor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Total_Monthly_BCC__c</field>
            <operation>greaterOrEqual</operation>
            <value>450</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Customer Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Total_Monthly_BCC__c</field>
            <operation>greaterOrEqual</operation>
            <value>1200</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Customer Service Manager</value>
        </criteriaItems>
        <description>To set Needs SOA Approval to True based on thresholds</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New CS Claim Case Open</fullName>
        <actions>
            <name>New_CS_Claim_Case_Open</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CS Claim</value>
        </criteriaItems>
        <description>This workflow rule creates a task when a new CS Claim Case is open.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>RA Created Case</fullName>
        <actions>
            <name>Revenue_Assurance_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Owner:User.Profile.Name = &quot;Revenue Assurance&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Case Close Email</fullName>
        <actions>
            <name>Send_Case_Close_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Notify_Contact_On_Case_Closed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sends an email when the &quot;notify on case closed&quot; check box is TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Case Open Email</fullName>
        <actions>
            <name>Send_Case_Open_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CS Claim,CS Non Customer,CS Claim Closed,CS Inquiry Closed,CS Inquiry,CS Internal Berry Team,CS Non Customer Closed,Billing Case,CS Non Advertiser,CS P4P,National Claim</value>
        </criteriaItems>
        <description>Sends an email notification to the contact when an case has been created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Email Notification on case creation</fullName>
        <actions>
            <name>Notify_the_contact_whenever_the_case_is_opened_saved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Notify_Contact_on_Case_Open__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Case Recd From</fullName>
        <actions>
            <name>Recd_From_Sales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Sales Rep</value>
        </criteriaItems>
        <description>Set case received from to sales when case created by sales rep profile</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Telco Approval Notes is not null</fullName>
        <actions>
            <name>Telco_Approved_t_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Telco_Approval_Notes__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateCheckNewlyCreatedCancellationCase</fullName>
        <actions>
            <name>Set_New_Cancellation_Case_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CheckNewlyCreatedCancellationCase__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cancellation</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>New_CS_Claim_Case_Open</fullName>
        <assignedToType>accountOwner</assignedToType>
        <description>A new CS Claim Case has been opened on your account. Please review within 2 days.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New CS Claim Case Open</subject>
    </tasks>
</Workflow>
