/********************************************************************
Apex Class to submit iYP DFFs for Fulfillment upon Ad Status Complete
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 04/21/2015
$Id$
*********************************************************************/
Public Class SendMilesDFFTalus {

    @future(callout = true)
    public static void ypcDffAutoPost(Set < Id > dffs) {

        system.debug('************DFFs************' + dffs.size());

        //Get nigel endpoint url
        string url = Label.Talus_Nigel_Url;

        Digital_Product_Requirement__c updtDffrcd;

        List < Digital_Product_Requirement__c > allDffs = new List < Digital_Product_Requirement__c > ();
        Map < Id, String > mapDffs = new Map < Id, String > ();
        Map < String, String > fnlMapDffs = new Map < String, String > ();
        Map < String, String > fnlMapDffs1 = new Map < String, String > ();

        try {
            allDffs = CommonUtility.dffBySetIds(dffs);
        } catch (exception e) {
            ErrorLog.CreateErrorLog(e.getmessage(), null, 400, 'Exception Occured in DFF Auto Post');
        }

        if (allDffs.size() > 0) {

            Set < String > setpckskus = new Set < String > ();
            Set < String > setprdSkus = new Set < String > ();
            for (Digital_Product_Requirement__c d1: allDffs) {
                setpckskus.add(d1.Test_PackageId__c);
                setPrdSkus.add(d1.Test_ProductId__c);
            }
            Map < String, List < Product_Properties__c >> mappCdffByProdproperty = CommonUtility.pkgPrdtPrpts(setpckskus, setprdSkus);

            for (Digital_Product_Requirement__c d: allDffs) {

                updtDffrcd = d;

                //Instantiate JSON Generator method to form JSON data
                JSONGenerator jsonGen = JSON.createGenerator(true);
                String newJsonString;

                //Starting JSON Generator
                jsonGen.writeStartObject();

                //Setting start date for Talus to current date as they dont accept dates in past
                Datetime myDate = datetime.now();
                String newDate1 = myDate.format('yyyy-MM-dd', 'UTC');
                String newDate2 = String.valueof(System.Today().addDays(30000));

                jsonGen.writeStringField('sku', d.Test_PackageId__c);
                jsonGen.writeStringField('start_date', newDate1);
                jsonGen.writeStringField('end_date', newDate2);
                jsonGen.writeFieldName('products');
                jsonGen.writeStartArray();

                List < Product_Properties__c > NewPrpts = new List < Product_Properties__c > ();
                if (mappCdffByProdproperty.get(d.Test_ProductId__c) != null) {
                    NewPrpts = mappCdffByProdproperty.get(d.Test_ProductId__c);
                }

                //This will start json object for each product
                jsonGen.writeStartObject();
                jsonGen.writeStringField('code', String.valueof(d.id));
                jsonGen.writeStringField('sku', d.Test_ProductId__c);
                jsonGen.writeStringField('external_id', String.valueof(d.UDAC__c));
                if (String.isNotBlank(d.setup_notes__c)) {
                    jsonGen.writeStringField('setup_notes', String.valueof(d.setup_notes__c));
                } else(jsonGen.writeStringField('setup_notes', ''));

                String newFurl = url + d.Account__r.TalusAccountId__c + '/subscriptions/';

                if (NewPrpts.size() <= 0) {

                    jsonGen.writeFieldName('properties');
                    jsonGen.writeStartObject();

                    jsonGen.writeEndObject();

                } else if (NewPrpts.size() > 0) {

                    jsonGen.writeFieldName('properties');
                    jsonGen.writeStartObject();

                    for (Product_Properties__c newPrdPrpts: NewPrpts) {

                        String newPrptyName = newPrdPrpts.Name__c + '__c';

                        //System.debug('************ValufromDFF************' + newPrptyName + d.get(newPrptyName));

                        if (newPrdPrpts.Type__c == 'string') {

                            String newPrpty = String.valueof(d.get(newPrptyName));

                            //System.debug('************STRINGVAL************' + newPrpty);

                            if (String.isNotBlank(newPrpty)) {
                                jsonGen.writeStringField(newPrdPrpts.Name__c, newPrpty);
                            } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                        } else if (newPrdPrpts.Type__c == 'int') {

                            Integer newPrpty = Integer.valueof(d.get(newPrptyName));
                            if (newPrpty != null) {
                                jsonGen.writeNumberField(newPrdPrpts.Name__c, newPrpty);
                            } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                        } else if (newPrdPrpts.Type__c == 'jsonobjarray' || newPrdPrpts.Type__c == 'jsonobj') {

                            String desptn;
                            String descVal;
                            String newPrpty1;
                            String newPrpty2;
                            List < String > spltVls = new List < String > ();
                            String fldNms = CommonUtility.jsonObjFlds(newPrdPrpts.Name__c);

                            if (String.isNotBlank(fldNms)) {

                                jsonGen.writeFieldName(newPrdPrpts.Name__c);
                                jsonGen.writeStartArray();

                                List < String > lstMpVls = fldNms.split(';');
                                for (String iterator: lstMpVls) {

                                    spltVls = iterator.split('-');
                                    desptn = spltVls[0];
                                    descVal = spltVls[1];

                                    jsonGen.writeStartObject();
                                    newPrpty1 = String.valueof(d.get(desptn));
                                    newPrpty2 = String.valueof(d.get(descVal));

                                    If(String.isNotBlank(newPrpty1)) {
                                        jsonGen.writeStringField('description', newPrpty1);
                                    } else(jsonGen.writeStringField('description', ''));

                                    If(String.isNotBlank(newPrpty2)) {
                                        jsonGen.writeStringField('value', newPrpty2);
                                    } else(jsonGen.writeStringField('value', ''));

                                    jsonGen.writeEndObject();

                                }

                                jsonGen.writeEndArray();

                            } else {

                                if (newPrdPrpts.Name__c == 'graphic_pao') {

                                    jsonGen.writeFieldName(newPrdPrpts.Name__c);
                                    jsonGen.writeStartObject();

                                    if (d.Graphic_Pao_Url__c != null) {
                                        jsonGen.writeStringField('graphic_url', String.valueof(d.Graphic_Pao_Url__c));
                                    } else(jsonGen.WriteStringField('graphic_url', ''));

                                    jsonGen.writeEndObject();

                                }
                                if (newPrdPrpts.Name__c == 'coupons') {

                                    jsonGen.writeFieldName(newPrdPrpts.Name__c);
                                    jsonGen.writeStartArray();

                                    jsonGen.writeStartObject();
                                    if (d.coupon_description__c != null) {
                                        jsonGen.writeStringField('coupon_description', d.coupon_description__c);
                                    } else(jsonGen.writeStringField('coupon_description', ''));
                                    if (d.coupon_text_offer_1__c != null) {
                                        jsonGen.writeStringField('coupon_text_offer_1', d.coupon_text_offer_1__c);
                                    } else(jsonGen.writeStringField('coupon_text_offer_1', ''));
                                    if (d.coupon_text_offer_2__c != null) {
                                        jsonGen.writeStringField('coupon_text_offer_2', d.coupon_text_offer_2__c);
                                    } else(jsonGen.writeStringField('coupon_text_offer_2', ''));
                                    if (d.coupon_text_offer_3__c != null) {
                                        jsonGen.writeStringField('coupon_text_offer_3', d.coupon_text_offer_3__c);
                                    } else(jsonGen.writeStringField('coupon_text_offer_3', ''));
                                    jsonGen.writeEndObject();

                                    jsonGen.writeStartObject();
                                    if (d.coupon_url__c != null) {
                                        jsonGen.writeStringField('coupon_url', d.coupon_url__c);
                                    } else(jsonGen.writeStringField('coupon_url', ''));
                                    jsonGen.writeEndObject();

                                    jsonGen.writeEndArray();

                                }
                                if (newPrdPrpts.Name__c == 'graphic_banner') {

                                    jsonGen.writeFieldName(newPrdPrpts.Name__c);
                                    jsonGen.writeStartObject();

                                    if (d.Banner_Url__c != null) {
                                        jsonGen.writeStringField('graphic_url', String.valueof(d.Banner_Url__c));
                                    } else(jsonGen.WriteStringField('graphic_url', ''));
                                    if (d.Banner_Link__c != null) {
                                        jsonGen.WriteStringField('graphic_link', String.valueof(d.Banner_Link__c));
                                    } else(jsonGen.WriteStringField('graphic_link', ''));
                                    if (d.Banner_Dist_Url__c != null) {
                                        jsonGen.WriteStringField('graphic_dist_url', String.valueof(d.Banner_Dist_Url__c));
                                    } else(jsonGen.WriteStringField('graphic_dist_url', ''));

                                    jsonGen.writeEndObject();

                                }
                                if (newPrdPrpts.Name__c == 'graphic_logo') {

                                    jsonGen.writeFieldName(newPrdPrpts.Name__c);
                                    jsonGen.writeStartObject();
                                    if (d.RecordType.DeveloperName == 'iYP_Diamond') {
                                        //System.debug('************DiamondValues************' + d.Diamond_Graphic_Link__c + '------------' + d.Diamond_Graphic_URL__c + '------------' + d.Diamond_Graphic_Dist_URL__c);
                                        if (d.Diamond_Graphic_Link__c != null) {
                                            jsonGen.writeStringField('graphic_link', String.valueof(d.Diamond_Graphic_Link__c));
                                        } else(jsonGen.WriteStringField('graphic_link', ''));
                                        if (d.Diamond_Graphic_URL__c != null) {
                                            jsonGen.WriteStringField('graphic_url', String.valueof(d.Diamond_Graphic_URL__c));
                                        } else(jsonGen.WriteStringField('graphic_url', ''));
                                        if (d.Diamond_Graphic_Dist_URL__c != null) {
                                            jsonGen.WriteStringField('graphic_dist_url', String.valueof(d.Diamond_Graphic_Dist_URL__c));
                                        } else(jsonGen.WriteStringField('graphic_dist_url', ''));
                                    } else {
                                        if (d.Logo_Graphic_Link__c != null) {
                                            jsonGen.writeStringField('graphic_link', String.valueof(d.Logo_Graphic_Link__c));
                                        } else(jsonGen.WriteStringField('graphic_link', ''));
                                        if (d.Logo_Graphic_Url__c != null) {
                                            jsonGen.WriteStringField('graphic_url', String.valueof(d.Logo_Graphic_Url__c));
                                        } else(jsonGen.WriteStringField('graphic_url', ''));
                                        if (d.Logo_Graphic_Dist_Url__c != null) {
                                            jsonGen.WriteStringField('graphic_dist_url', String.valueof(d.Logo_Graphic_Dist_Url__c));
                                        } else(jsonGen.WriteStringField('graphic_dist_url', ''));
                                    }
                                    jsonGen.writeEndObject();

                                }

                            }

                        } else if (newPrdPrpts.Type__c == 'date') {

                            if (d.get(newPrptyName) != null) {
                                Date newPrpty = Date.valueof(d.get(newPrptyName));
                                Date myDateMy = date.newinstance(newPrpty.year(), newPrpty.month(), newPrpty.day());
                                jsonGen.writeStringField(newPrdPrpts.Name__c, String.valueof(myDateMy));
                            } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                        } else if (newPrdPrpts.Type__c == 'email') {

                            String newPrpty = String.valueof(d.get(newPrptyName));
                            if (String.isNotBlank(newPrpty)) {
                                jsonGen.writeStringField(newPrdPrpts.Name__c, newPrpty);
                            } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                        } else if (newPrdPrpts.Type__c == 'stringarray') {

                            String newPrpty = String.valueof(d.get(newPrptyName));
                            if (String.isNotBlank(newPrpty)) {
                                List < String > lst = new List < String > ();
                                lst = newPrpty.split(';');
                                jsonGen.writeObjectField(newPrdPrpts.Name__c, lst);
                            } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                        } else if (newPrdPrpts.Type__c == 'bool') {

                            Boolean newPrpty = boolean.valueof(d.get(newPrptyName));
                            jsonGen.writeBooleanField(newPrdPrpts.Name__c, newPrpty);

                        } else if (newPrdPrpts.Type__c == 'decimal') {

                            Integer newPrpty = Integer.valueof(d.get(newPrptyName));
                            if (newPrpty != null) {
                                jsonGen.writeNumberField(newPrdPrpts.Name__c, newPrpty);
                            } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));

                        } else if (newPrdPrpts.Type__c == 'hoursofoperation') {

                            String newPrpty = String.valueof(d.get(newPrptyName));

                            if (String.isNotBlank(newPrpty)) {
                                List < String > lst = new List < String > ();
                                lst = newPrpty.split(';');

                                //system.debug('************123452HoursOfOperation123452************' + lst.size());

                                if (newPrpty != null) {
                                    jsonGen.writeFieldName(newPrdPrpts.Name__c);
                                    jsonGen.writeStartArray();
                                    for (String s: lst) {
                                        if (s != 'null') {
                                            List < String > lst1 = s.split(',');
                                            jsonGen.writeStartArray();
                                            for (String ss: lst1) {
                                                jsonGen.writeString(ss);
                                            }
                                            jsonGen.writeEndArray();
                                        } else {
                                            jsonGen.writeNull();
                                        }
                                    }
                                    jsonGen.writeEndArray();
                                }
                            } else(jsonGen.writeStringField(newPrdPrpts.Name__c, ''));
                        }

                    }

                    jsonGen.writeEndObject();

                }
                jsonGen.writeEndObject();

                jsonGen.writeEndArray();

                //Ending JSON Generator 
                jsonGen.writeEndObject();

                //Retrieve JSON data in a string format, and make sure to use this method only once
                newJsonString = jsonGen.getAsString();

                system.debug('************JSON DATA FINAL************' + newJsonString);

                //Get the response string by making callout to Talus
                if (String.isNotBlank(newJsonString)) {

                    try {

                        HttpResponse newRes = CommonUtility.postUtil(newJsonString, newFurl, String.valueof('POST'));

                        String newResponsestring = newRes.getBody();

                        if (Test.isRunningTest()) {
                            newRes.setStatusCode(201);
                            newResponsestring = '{ "code":null, "created":"2013-03-05T15:26:44.313581", "deleted":null, "end_date":"02/28/2014", "external_id":null, "external_references": [ { "key":"subscription", "reference_id":"108147", "service":"Geneva" } ], "fulfillment_date":null, "id":453, "last_updated":"2013-03-05T15:26:44.312943", "products": [ { "code":null, "created":"2013-03-05T15:26:44", "deleted":null, "external_references": [ { "key":"subscription_product", "reference_id":"196083", "service":"Geneva" } ], "id":652, "last_updated":"2013-03-05T15:26:44", "package_product_id":18, "product_id":1, "properties": { "ClicksContracted":"12000" }, "status":"active" } ], "start_date":"04/01/2013", "status":"active" }';                                               
                        }

                        System.debug('************HTTPResponse************' + newRes + '************RESPONSESTING************' + newResponsestring);

                        if (newRes != null && newRes.getStatusCode() == 201) {

                            Map < String, Object > newResps = (Map < String, Object > ) JSON.deserializeUntyped(newResponsestring);

                            system.debug('************RESPONSESTINGId************' + newResps.get('id') + '************' + newResps.get('products'));

                            //Add all dffs into a Map for updating each DFF's "Talus Subscription Id" field 
                            mapDffs.put(updtDffrcd.id, string.valueof(newResps.get('id')));

                            //System.debug('************MapList************' + mapDffs);

                            List < object > lst = (List < Object > ) newResps.get('products');
                            integer i;
                            for (i = 0; i < lst.size(); i++) {

                                fnlMapDffs.put(String.valueof(((Map < String, Object > ) lst[i])
                                    .get('code')), String.valueof(((Map < String, Object > ) lst[i])
                                    .get('id')));

                                fnlMapDffs1.put(String.valueof(((Map < String, Object > ) lst[i])
                                    .get('code')), String.valueof(newResps.get('id')));

                                //System.debug('*****************%%%%%%%%****************************' + fnlMapDffs + '@@@@@@@' + fnlMapDffs1);
                                //System.debug('*******####$$$******$$$####*******' + ((Map < String, Object > ) lst[i]).get('code'));
                                //System.debug('*******####$$$******$$$####*******' + ((Map < String, Object > ) lst[i]).get('id'));
                                //System.debug('*******####$$$******$$$####*******' + fnlMapDffs);

                            }

                        } else {
                            ErrorLog.CreateErrorLog(newResponsestring, d.Id, 400, 'Fulfillment Validaiton Error in DFF Auto POST');
                        }

                    } catch (exception e) {
                        ErrorLog.CreateErrorLog(e.getmessage(), d.Id, 400, 'Failed in DFF Auto Post');
                    }

                }

            }

        }

        if (fnlMapDffs.size() > 0) {
            //Call external class to update Dff records with Talus returned Ids
            updtDffWithTalusId.updtDffTId(fnlMapDffs, fnlMapDffs1);
        }

    }

    @future(callout = true)
    public static void ypcDffAutoPatch(Set < Id > dffs) {

        system.debug('************DFFs************' + dffs.size());

        string url = Label.Talus_Nigel_Url;

        List < Digital_Product_Requirement__c > allDffs = new List < Digital_Product_Requirement__c > ();
        Map < Id, String > mapDffs = new Map < Id, String > ();
        Map < String, String > fnlMapDffs = new Map < String, String > ();
        Map < String, String > fnlMapDffs1 = new Map < String, String > ();
        List < Track_DFF_Update__c > trkDffs = new List < Track_DFF_Update__c > ();
        String trackInfo;

        try {
            allDffs = CommonUtility.dffBySetIds(dffs);
        } catch (exception e) {
            ErrorLog.CreateErrorLog(e.getmessage(), null, 400, 'Exception Occured in DFF Auto Patch');
        }

        for (Digital_Product_Requirement__c d: allDffs) {
            
            try{
            
            String fUrl = url + d.Account__r.TalusAccountId__c + '/subscriptions/' + d.Talus_Subscription_Id__c + '/subscription_products/' + d.Talus_DFF_Id__c + '/patch/';
            system.debug('************fUrl************' + fUrl);

            //instantiate JSON Generator method to form JSON data
            JSONGenerator jsonGen = JSON.createGenerator(true);
            String newJsonString;

            //starting JSON Generator
            jsonGen.writeStartObject();
            jsonGen.writeStringField('patch_notes', 'Graphics change');
            jsonGen.writeFieldName('patch_update');
            jsonGen.writeStartObject();

            jsonGen.writeFieldName('properties');
            jsonGen.writeStartObject();

            if (String.isNotBlank(d.Production_Notes_for_Banner_Graphic__c) && d.Fulfillment_Submit_Status__c == 'Complete') {
                system.debug('************ProductionNotesforBannerGraphic************');
                jsonGen.writeFieldName('graphic_banner');
                jsonGen.writeStartObject();

                if (d.Banner_Url__c != null) {
                    jsonGen.writeStringField('graphic_url', String.valueof(d.Banner_Url__c));
                } else(jsonGen.WriteStringField('graphic_url', ''));
                if (d.Banner_Link__c != null) {
                    jsonGen.WriteStringField('graphic_link', String.valueof(d.Banner_Link__c));
                } else(jsonGen.WriteStringField('graphic_link', ''));
                if (d.Banner_Dist_Url__c != null) {
                    jsonGen.WriteStringField('graphic_dist_url', String.valueof(d.Banner_Dist_Url__c));
                } else(jsonGen.WriteStringField('graphic_dist_url', ''));

                jsonGen.writeEndObject();
            }
            if (String.isNotBlank(d.Production_Notes_for_Logo__c) && d.Fulfillment_Submit_Status__c == 'Complete') {
                system.debug('************ProductionNotesforLogo************');
                jsonGen.writeFieldName('graphic_logo');
                jsonGen.writeStartObject();
                if (d.RecordType.DeveloperName == 'iYP_Diamond') {
                    if (d.Diamond_Graphic_Link__c != null) {
                        jsonGen.writeStringField('graphic_link', String.valueof(d.Diamond_Graphic_Link__c));
                    } else(jsonGen.WriteStringField('graphic_link', ''));
                    if (d.Diamond_Graphic_URL__c != null) {
                        jsonGen.WriteStringField('graphic_url', String.valueof(d.Diamond_Graphic_URL__c));
                    } else(jsonGen.WriteStringField('graphic_url', ''));
                    if (d.Diamond_Graphic_Dist_URL__c != null) {
                        jsonGen.WriteStringField('graphic_dist_url', String.valueof(d.Diamond_Graphic_Dist_URL__c));
                    } else(jsonGen.WriteStringField('graphic_dist_url', ''));
                } else {
                    if (d.Logo_Graphic_Link__c != null) {
                        jsonGen.writeStringField('graphic_link', String.valueof(d.Logo_Graphic_Link__c));
                    } else(jsonGen.WriteStringField('graphic_link', ''));
                    if (d.Logo_Graphic_Url__c != null) {
                        jsonGen.WriteStringField('graphic_url', String.valueof(d.Logo_Graphic_Url__c));
                    } else(jsonGen.WriteStringField('graphic_url', ''));
                    if (d.Logo_Graphic_Dist_Url__c != null) {
                        jsonGen.WriteStringField('graphic_dist_url', String.valueof(d.Logo_Graphic_Dist_Url__c));
                    } else(jsonGen.WriteStringField('graphic_dist_url', ''));
                }
                jsonGen.writeEndObject();
            }
            if (String.isNotBlank(d.Production_Notes_for_PrintAdOrderGraphic__c) && d.Fulfillment_Submit_Status__c == 'Complete') {
                system.debug('************ProductionNotesforPrintAdOrderGraphic************');
                jsonGen.writeFieldName('graphic_pao');
                jsonGen.writeStartObject();

                if (d.Graphic_Pao_Url__c != null) {
                    jsonGen.writeStringField('graphic_url', String.valueof(d.Graphic_Pao_Url__c));
                } else(jsonGen.WriteStringField('graphic_url', ''));

                jsonGen.writeEndObject();
            }

            jsonGen.writeEndObject();

            //ending JSON Generator
            jsonGen.writeEndObject();

            //retrieve formed JSON data in a string format, and make sure to use this method only once
            newJsonString = jsonGen.getAsString();
            trackInfo = newJsonString;

            system.debug('************PATCH JSON DATA FINAL************' + newJsonString);

            if (newJsonString != null) {

                HttpResponse newRes = CommonUtility.postUtil(newJsonString, fUrl, String.valueof('POST'));

                String newResponsestring = newRes.getBody();

                system.debug('************RESPONSE************' + newResponsestring + '----------' + newRes.getStatusCode() + '-----' + newRes.getStatus());

                String JSONContent = newResponsestring;

                JSONParser parser = JSON.createParser(JSONContent);

                parser.nextToken();

                parser.nextValue();

                String fieldName = parser.getCurrentName();

                String fieldValue = parser.getText();
                
                if (Test.isRunningTest()) {
                    newRes.setStatusCode(202);
                    newResponsestring = '{ "code":null, "created":"2013-03-05T15:26:44.313581", "deleted":null, "end_date":"02/28/2014", "external_id":null, "external_references": [ { "key":"subscription", "reference_id":"108147", "service":"Geneva" } ], "fulfillment_date":null, "id":453, "last_updated":"2013-03-05T15:26:44.312943", "products": [ { "code":null, "created":"2013-03-05T15:26:44", "deleted":null, "external_references": [ { "key":"subscription_product", "reference_id":"196083", "service":"Geneva" } ], "id":652, "last_updated":"2013-03-05T15:26:44", "package_product_id":18, "product_id":1, "properties": { "ClicksContracted":"12000" }, "status":"active" } ], "start_date":"04/01/2013", "status":"active" }';                                               
                }

                if (newRes != null && (newRes.getStatusCode() == 202 || newRes.getStatusCode() == 200)) {

                    trkDffs.add(new Track_DFF_Update__c(Data_Fulfillment_Form__c = d.Id, Sent_Date__c = system.today(), Status__c = newRes.getStatus(), Updated_Info__c = trackInfo));

                } else if (newRes != null && (newRes.getStatusCode() != 202 || newRes.getStatusCode() != 200)) {

                    system.debug('************PatchFailed************');

                }
            }
            
            }catch(exception e){
                ErrorLog.CreateErrorLog(e.getmessage(), d.Id, 400, 'Failed in DFF Auto Patch');
            }
        }
        
        //Insert all TrackDFFUpdate records
        if (trkDffs.size() > 0) {

            insert trkDffs;

        }
                
    }

}