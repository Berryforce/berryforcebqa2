@isTest(seeAllData=true)
public with sharing class InvokeTTSOAPService1Test {

    public static testMethod void invkTTService() {
        
        test.startTest();

        List<Case> caseLst = new List<Case>();
        
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c='12';
        insert a;

        Case c1 = CommonUtility.createCaseTest(a);
        c1.Subject='CaseFromSFDC';
        c1.Status='New';
        c1.TeamTrack_tableIdItemId__c='1234';
        caseLst.add(c1);
        
        Case c2 = CommonUtility.createCaseTest(a);
        c2.Resolution_Note__c='TestResolutionNotes';
        c2.Subject='CaseFromSFDC';
        c2.Status='New';
        caseLst.add(c2);
        
        insert caseLst;
        
        for(Case cs: CaseLst){

        ApexPages.StandardController controller = new ApexPages.StandardController(cs); 
        InvokeTTSOAPService1 calTT = new InvokeTTSOAPService1(controller);
        calTT.invokeService1();

        }
        
        test.stopTest();

    }
    
}