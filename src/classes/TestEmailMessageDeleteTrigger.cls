@IsTest
public class TestEmailMessageDeleteTrigger
{
    static testMethod void testDeleteEmailtrigger_1(){
    
      Case cs1 = new Case();
      insert cs1;
       
    
           EmailMessage EmMsg=new EmailMessage();
           
           EmMsg.FromAddress = 'test@theberrycompany.com';
           EmMsg.FromName = 'test user';
           EmMsg.Subject = 'testemail';
           EmMsg.ParentId = cs1.Id;
           insert EmMsg;
 
           // should succeed
           delete EmMsg;
        }

}