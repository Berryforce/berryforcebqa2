public with sharing class ListingSOQLMethods {
   
    public static Map<id,Listing__c> getNationalDisconnectedListing(Set<id> setListing){
        return new Map<id,Listing__c>([select id,Disconnected__c,RecordType.Name,account__c, account__r.recordtype.name from listing__c where id in :setListing]);
    }
    public static Map<Id,Listing__c> fetchListing(set<Id> setLstId){
        return new map<Id,Listing__c>([Select Id,Phone__c,Main_Listing__c,Service_order_stage_item__c,Disconnected__c,(select Id,Phone_Number__c,caption_header__c,caption_member__c,Disconnected__c from Reference_listings__r)from Listing__c where Id IN :setLstId]); 
    }
    public static list<Listing__c> getMatchedListings(set<string> setMatchedListingId, set<Id> setListRecordtypeId){
        return [select Id,Name,LST_Last_Name_Business_Name__c,Account__c,ABD_Pending_Review__c,Lst_StrListingMatch__c,Disconnected_Via_ABD__c,Bus_Res_Gov_Indicator__c,StrLstMatch__c,RecordTypeId,Phone__c,Listing_Street_Number__c,Listing_Street__c,Listing_City__c,Listing_State__c,Listing_Postal_Code__c,Listing_Country__c,
                          Disconnected__c,Disconnect_Reason__c,(select Id,Name,SL_Last_Name_Business_Name__c,Product_Code_UDAC__c,Section_Page_Type__c,listing__c,StrBOCDisconnect__c from Reference_Listings__r) from Listing__c where Lst_StrListingMatch__c IN : setMatchedListingId AND RecordTypeId IN : setListRecordtypeId Order By CreatedDate DESC];
    }
    public static list<Listing__c> fetchAdditionalListings(set<Id> setListingId){
        return [Select Id,Main_Listing__c,service_order_stage_item__c,(Select Id,Section_Page_Type__c from Reference_Listings__r where Section_Page_Type__c='YP')from listing__c where Id IN : setListingId];
    }
    public static Map<Id,Listing__c> fetchListingforSLDFFupdate(string listingId){
    	return new map<Id,Listing__c>([Select Id, Name,Bus_Res_Gov_Indicator__c,Cross_Reference_Text__c,Normalized_Designation__c,Disconnect_Reason__c,Disconnected__c,Effective_Date__c,Area_Code__c,Exchange__c,              
	  			First_Name__c,Honorary_Title__c,Lst_Last_Name_Business_Name__c,Left_Telephone_Phrase__c,Lineage_Title__c,Listing_Type__c,Manual_Sort_As_Override__c,Normalized_Last_Name_Business_Name__c,Lst_SL_DFF_Sync__c, 
	  			Normalized_First_Name__c,Normalized_Listing_Street_Name__c,Normalized_Listing_Street_Number__c,Normalized_Phone__c,Omit_Address_OAD__c,Phone_Override__c,Phone_Type__c,Phone__c,Service_Order__c, 
	  			Telco_Provider__c,Listing_Street_Number__c,Listing_Street__c,Listing_City__c,Listing_PO_Box__c,Listing_State__c,Listing_Postal_Code__c,Listing_Country__c,Right_Telephone_Phrase__c,Designation__c, 
	  			Telco_Sort_Order__c,Normalized_Honorary_Title__c,Normalized_Lineage_Title__c,Normalized_Secondary_Surname__c,Disconnected_Via_ABD__c,ABD_Pending_Review__c From listing__c where Id =:listingId AND Lst_SL_DFF_Sync__c=true]);
    }
    public static map<Id,Listing__c> fetchListingforSync(set<Id> setListingId){
    	return new map<Id,Listing__c>([Select Id, Name,Bus_Res_Gov_Indicator__c,Cross_Reference_Text__c,Normalized_Designation__c,Disconnect_Reason__c,Disconnected__c,Effective_Date__c,Area_Code__c,Exchange__c,              
	  			First_Name__c,Honorary_Title__c,Lst_Last_Name_Business_Name__c,Left_Telephone_Phrase__c,Lineage_Title__c,Listing_Type__c,Manual_Sort_As_Override__c,Normalized_Last_Name_Business_Name__c,Lst_SL_DFF_Sync__c, 
	  			Normalized_First_Name__c,Normalized_Listing_Street_Name__c,Normalized_Listing_Street_Number__c,Normalized_Phone__c,Omit_Address_OAD__c,Phone_Override__c,Phone_Type__c,Phone__c,Service_Order__c,CLEC_Provider__c, 
	  			Telco_Provider__c,Listing_Street_Number__c,Listing_Street__c,Listing_City__c,Listing_PO_Box__c,Listing_State__c,Listing_Postal_Code__c,Listing_Country__c,Right_Telephone_Phrase__c,Designation__c, 
	  			Telco_Sort_Order__c,Normalized_Honorary_Title__c,Normalized_Lineage_Title__c,Normalized_Secondary_Surname__c,Disconnected_Via_ABD__c,ABD_Pending_Review__c From listing__c where Id IN :setListingId]);
    }
}