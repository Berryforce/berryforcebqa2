/***********************************************************
Batch Class to send YPC Add-on patches to Fulfillment Vendor
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 09/21/2015
$Id$
************************************************************/
global class YpcAddonPatch implements Database.Batchable < sObject > , Database.AllowsCallouts {
    global String query;

    global YpcAddonPatch(String query) {

        this.query = query;

    }

    global Database.Querylocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext BC, List < Digital_Product_Requirement__c > scope) {

        List < Digital_Product_Requirement__c > lstDffs = scope;
        System.debug('************' + lstDffs.size());

        Set < String > cstlkItms = new Set < String > {
            'CSTLK', 'CST5', 'RBL', 'RB5', 'YPCP', 'YPC5', 'PVID', 'PVID5', 'VID1', 'VID15', 'VIDA', 'VIDA5'
        };
        Set < String > pointsItms = new Set < String > {
            'VCTLK', 'VCTL5', 'VRB', 'VRP5', 'VCP', 'VCP5', 'VVP', 'VP5', 'VAA', 'VA5', 'VRB5'
        };
        Set < String > ypcCpn = new Set < String > {
            'YPCP', 'YPC5'
        };
        Set < String > ypcCstlk = new Set < String > {
            'CSTLK', 'CST5'
        };

        String cstVals;
        String pntVals;
        String testVal;
        String trackInfo = '';
        String fUrl;
        System.debug('************You are Testing this************: ' + UserInfo.getFirstName());
        string url = Label.Talus_Nigel_Url;
        String cpnDesc;
        String cpnUrl;
        String CstlkTxt;
        String CstlkUrl;
        String txt1;
        String txt2;
        String txt3;

        List < Digital_Product_Requirement__c > updtDffs = new List < Digital_Product_Requirement__c > ();
        List < Track_DFF_Update__c > trkDffs = new List < Track_DFF_Update__c > ();

        Map < Digital_Product_Requirement__c, List < YPC_AddOn__c > > mpAddons = new Map < Digital_Product_Requirement__c, List < YPC_AddOn__c > > ();
        List < YPC_AddOn__c > ypcAdn = new List < YPC_AddOn__c > ();
        List < YPC_AddOn__c > delYpcAdn = new List < YPC_AddOn__c > ();
        Map < Id, YPC_AddOn__c > delYpdAdnMap = new Map < Id, YPC_AddOn__c > ();
        /*
        for (Digital_Product_Requirement__c d: scope) {
            for (YPC_AddOn__c ypc: d.YPC_AddOns__r) {
                ypcAdn.add(ypc);
            }
            mpAddons.put(d, ypcAdn);
        }
        */
        //System.debug('************Map Size************: ' + mpAddons.size() + '************Map KeySet************: ' + mpAddons.keyset() + '************Map Values************: ' + mpAddons.values());

        if (Test.isRunningTest()) {
            lstDffs = [SELECT Id, Name, udac__c, udac_points__c, add_ons__c, vid_add_on__c, coupon_description__c, coupon_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c, cslt_url__c, cslt_text__c, recordType.DeveloperName, Account__r.TalusAccountId__c, Talus_Subscription_Id__c, Talus_DFF_Id__c, OrderLineItemID__r.Effective_Date__c, (SELECT Id, Name, DFF__c, DFF_Id__c, Validation__c, Effective_Date__c, Action_Type__c, coupon_description__c, coupon_url__c, cslt_text__c, cslt_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c from YPC_AddOns__r) from Digital_Product_Requirement__c where Account__r.TalusAccountId__c != null and Talus_Subscription_Id__c != null and Id IN(SELECT DFF__c from YPC_AddOn__c)];
        }

        for (Digital_Product_Requirement__c dff: scope) {

            //List < YPC_AddOn__c > spVals = mpAddons.get(dff);
            List < YPC_AddOn__c > spVals = new List < YPC_AddOn__c > ();
            List < YPC_AddOn__c > spVals1 = dff.YPC_AddOns__r;
            if (spVals1.size() > 0) {
                for (YPC_AddOn__c iterator: spVals1) {
                    if (iterator.Effective_Date__c <= system.Today() && (!iterator.Validation__c)) {
                        spVals.add(iterator);
                    }
                }
            }

            cstVals = '';
            pntVals = '';
            if (spVals.size() > 0) {

                if (String.isBlank(dff.add_ons__c)) {
                    cstVals = '';
                } else(cstVals = dff.add_ons__c);

                if (String.isBlank(dff.udac_points__c)) {
                    pntVals = '';
                } else(pntVals = dff.udac_points__c);
                for (YPC_AddOn__c val: spVals) {
                    //System.debug('************' + val.Action_Type__c + '************' + val.Effective_Date__c + '************' + val.Name);
                    if (val.Action_Type__c == 'New' && val.Effective_Date__c <= system.Today() && (!val.Validation__c)) {
                        if (cstlkItms.contains(val.Name) && (!cstVals.contains(val.Name))) {
                            cstVals += ';' + val.Name;
                            if (ypcCpn.contains(val.Name)) {
                                cpnDesc = val.coupon_description__c;
                                dff.coupon_description__c = val.coupon_description__c;
                                cpnUrl = String.Valueof(val.coupon_url__c);
                                dff.coupon_url__c = String.Valueof(val.coupon_url__c);
                                txt1 = val.coupon_text_offer_1__c;
                                dff.coupon_text_offer_1__c = val.coupon_text_offer_1__c;
                                txt2 = val.coupon_text_offer_2__c;
                                dff.coupon_text_offer_2__c = val.coupon_text_offer_2__c;
                                txt3 = val.coupon_text_offer_3__c;
                                dff.coupon_text_offer_3__c = val.coupon_text_offer_3__c;

                                //System.debug('************CpnVals************' + cpnDesc + '************************' + cpnUrl);
                            }
                            if (ypcCstlk.contains(val.Name)) {
                                CstLkTxt = val.cslt_text__c;
                                dff.cslt_text__c = val.cslt_text__c;
                                CstLkUrl = val.cslt_url__c;
                                dff.cslt_url__c = val.cslt_url__c;
                                //System.debug('************CstlkVals************' + CstLkTxt + '************************' + CstLkUrl);
                            }
                        } else if (pointsItms.contains(val.Name) && (!pntVals.contains(val.Name))) {
                            pntVals += ';' + val.Name;
                        }
                    } else if (val.Action_Type__c == 'Cancel' && val.Effective_Date__c <= system.Today()) {
                        if (cstlkItms.contains(val.Name)) {
                            cstVals = cstVals.replace(val.Name, '');
                            if (val.Name == 'CSTLK' || val.Name == 'CST5') {
                                dff.cslt_url__c = '';
                                dff.cslt_text__c = '';
                            }
                            if (val.Name == 'YPCP' || val.Name == 'YPC5') {
                                dff.coupon_description__c = '';
                                dff.coupon_url__c = '';
                                dff.coupon_text_offer_1__c = '';
                                dff.coupon_text_offer_2__c = '';
                                dff.coupon_text_offer_3__c = '';
                            }
                            if (cstVals.contains('CSTLK') || cstVals.contains('CST5')) {
                                CstlkUrl = dff.cslt_url__c;
                                CstlkTxt = dff.cslt_text__c;
                            }
                            if (cstVals.contains('YPCP') || cstVals.contains('YPC5')) {
                                cpnDesc = dff.coupon_description__c;
                                cpnUrl = dff.coupon_url__c;
                                txt1 = dff.coupon_text_offer_1__c;
                                txt2 = dff.coupon_text_offer_2__c;
                                txt3 = dff.coupon_text_offer_3__c;
                            }
                        } else if (pointsItms.contains(val.Name)) {
                            pntVals = pntVals.replace(val.Name, '');
                        }
                    }
                }
                //System.debug('************CstLnkVals************' + cstVals + '************PntVals************' + pntVals);
                List < String > fnlVals1 = cstVals.split(';');
                cstVals = '';
                for (String s: fnlVals1) {
                    if (String.isNotBlank(s) && (!cstVals.Contains(s))) {
                        cstvals += s + ';';
                    }
                }

                List < String > fnlVals2 = pntVals.split(';');
                pntVals = '';
                for (String s: fnlVals2) {
                    if (String.isNotBlank(s) && (!pntVals.Contains(s))) {
                        pntVals += s + ';';
                    }
                }

                if (cstVals.endsWith(';')) {
                    cstVals = cstVals.removeEnd(';');
                }
                if (pntVals.endsWith(';')) {
                    pntVals = pntVals.removeEnd(';');
                }

                dff.add_ons__c = cstVals;
                dff.udac_points__c = pntVals;

                fUrl = url + dff.Account__r.TalusAccountId__c + '/subscriptions/' + dff.Talus_Subscription_Id__c + '/subscription_products/' + dff.Talus_DFF_Id__c + '/patch/';
                System.debug('************DFFId************' + dff.Id + '************Furl************' + fUrl + '************Add-ons:: ' + dff.add_ons__c + '************Add-ons String:: ' + cstvals + '************Udac Points:: ' + dff.udac_points__c + '************Udac Points String:: ' + pntVals);


                //Instantiate new JSON Generator for sending Patch call 
                JSONGenerator jsongenpatch = JSON.createGenerator(true);
                String jsonGenPtch;

                //Starting JSON Generator
                jsongenpatch.writeStartObject();
                jsongenpatch.writeStringField('patch_notes', 'SFDC wants to update below fields information');
                jsongenpatch.writeFieldName('patch_update');
                jsongenpatch.writeStartObject();
                jsongenpatch.writeStringField('external_id', String.valueof(dff.udac__c));
                jsongenpatch.writeFieldName('properties');
                jsongenpatch.writeStartObject();

                List < String > lst = new List < String > ();

                if (String.isNotBlank(dff.add_ons__c)) {
                    lst = dff.add_ons__c.split(';');
                    jsongenpatch.writeObjectField('add_ons', lst);
                } else {
                    jsongenpatch.writeStringField('add_ons', '');
                }

                for (String iterator: lst) {
                    if ((iterator == 'CSTLK') || (iterator == 'CST5')) {
                        if (String.isNotBlank(CstlkUrl)) {
                            jsongenpatch.writeStringField('cslt_url', CstlkUrl);
                        } else(jsongenpatch.writeStringField('cslt_url', ''));
                        if (String.isNotBlank(CstlkTxt)) {
                            jsongenpatch.writeStringField('cslt_text', CstlkTxt);
                        } else(jsongenpatch.writeStringField('cslt_text', ''));
                    }
                    if ((iterator == 'YPCP') || (iterator == 'YPC5')) {
                        jsongenpatch.writeFieldName('coupons');
                        jsongenpatch.writeStartArray();

                        jsongenpatch.writeStartObject();
                        if (String.isNotBlank(cpnDesc)) {
                            jsongenpatch.writeStringField('coupon_description', cpnDesc);
                        } else(jsongenpatch.writeStringField('coupon_description', ''));
                        if (String.isNotBlank(txt1)) {
                            jsongenpatch.writeStringField('coupon_text_offer_1', txt1);
                        } else(jsongenpatch.writeStringField('coupon_text_offer_1', ''));
                        if (String.isNotBlank(txt2)) {
                            jsongenpatch.writeStringField('coupon_text_offer_2', txt2);
                        } else(jsongenpatch.writeStringField('coupon_text_offer_2', ''));
                        if (String.isNotBlank(txt3)) {
                            jsongenpatch.writeStringField('coupon_text_offer_3', txt3);
                        } else(jsongenpatch.writeStringField('coupon_text_offer_3', ''));
                        jsongenpatch.writeEndObject();

                        jsongenpatch.writeStartObject();
                        if (String.isNotBlank(cpnUrl)) {
                            jsongenpatch.writeStringField('coupon_url', cpnUrl);
                        } else(jsongenpatch.writeStringField('coupon_url', ''));
                        jsongenpatch.writeEndObject();

                        jsongenpatch.writeEndArray();
                    }
                }

                lst.clear();

                if (String.isNotBlank(dff.udac_points__c)) {
                    lst = dff.udac_points__c.split(';');
                    jsongenpatch.writeObjectField('udac_points', lst);
                } else {
                    jsongenpatch.writeStringField('udac_points', '');
                }


                Set < String > vidItms = new Set < String > {
                    'PVID', 'PVID5', 'VID1', 'VID15', 'VIDA', 'VIDA5'
                };
                for (String iterator: lst) {
                    if (vidItms.Contains(iterator)) {
                        if (String.isNotBlank(dff.vid_add_on__c)) {
                            jsongenpatch.writeStringField('vid_add_on', dff.vid_add_on__c);
                        }
                    }
                }

                trackInfo = 'add_ons__c: ' + cstVals + '\n' + 'udac_points__c: ' + pntVals;

                jsongenpatch.writeEndObject();

                jsongenpatch.writeEndObject();

                //Endgin JSON Generator
                jsongenpatch.writeEndObject();

                jsongenpatch.close();

                jsonGenPtch = jsongenpatch.getAsString();

                System.debug('************PatchJSON************' + jsonGenPtch);

                try {

                    HttpResponse newRes = CommonUtility.postUtil(jsonGenPtch, fUrl, String.valueof('POST'));

                    String newResponsestring = newRes.getBody();

                    System.debug('************RESPONSE************' + newRes + newResponsestring + '************' + newRes.getStatusCode() + '************' + newRes.getStatus());

                    String JSONContent = newResponsestring;

                    //System.debug('************DFFValues************' + dff.coupon_description__c + dff.coupon_url__c + dff.cslt_text__c + dff.cslt_url__c);

                    if (Test.isRunningTest()) {
                        newRes.setStatusCode(202);
                    }
                    if (newRes.getStatusCode() == 202 || newRes.getStatusCode() == 200) {
                        updtDffs.add(dff);

                        delYpcAdn.addall(spVals);
                        trkDffs.add(new Track_DFF_Update__c(Data_Fulfillment_Form__c = dff.Id, Sent_Date__c = system.today(), Status__c = 'Applied', Updated_Info__c = trackInfo, Add_Ons__c = true));

                    } else {
                        ErrorLog.CreateErrorLog(newResponsestring, dff.Id, newRes.getStatusCode(), 'Error while submitting Add-on Udac changes from YPC Add-on Batch');
                    }
                } catch (exception e) {
                    ErrorLog.CreateErrorLog(e.getMessage(), dff.Id, NULL, 'Error while submitting Add-on Udac changes from YPC Add-on Batch');
                }

            }

        }

        if (updtDffs.size() > 0) {
            update updtDffs;
        }

        if (trkDffs.size() > 0) {
            insert trkDffs;
        }

        if (delYpcAdn.size() > 0) {
            for (YPC_AddOn__c adn: delYpcAdn) {
                delYpdAdnMap.put(adn.Id, adn);
            }
            delete delYpdAdnMap.values();
        }

    }

    global void finish(Database.BatchableContext BC) {

        List < Messaging.SingleEmailMessage > lstAdmEmails = new List < Messaging.SingleEmailMessage > ();
        List < String > admEmails = Label.Fulfillment_Batch_Notification_Emails.split(',');

        //Notify Admin on Job completion
        AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            from AsyncApexJob where Id = : BC.getJobId()
        ];

        for (String trgtObjId: admEmails) {
            lstAdmEmails.add(CommonUtility.emailUsingTargetObjectId(trgtObjId, 'Batch Job For YPC Add-on Patch has ' + a.Status, 'Batch Job has processed Total of ' + a.TotalJobItems + ' batches with ' + a.NumberOfErrors + ' failures'));
        }
        Messaging.sendEmail(lstAdmEmails);        

    }
}