@IsTest(SeeAllData=true)

public class TaskBDTriggerTest {
/**********************
Method to test the block of deletion of tasks


***********************/
    static testMethod void PrevenDeleteTaskTest (){
       // create a task 
      Task TestTask = new Task();
      TestTask.Subject='Unit Test Task';
      TestTask.Status='Not Started';
      TestTask.Priority='Normal';
      TestTask.Description='Unit Test of Task Deletion';
      TestTask.IsReminderSet=false;
      TestTask.IsRecurrence=false;
      insert TestTask;
      
        Test.startTest();
        //delete the task
     try {
            delete TestTask;
        } catch (DmlException e) {
            System.assert( e.getMessage().contains('Task Unit Test Task Cannot be Deleted'), e.getMessage() );      
            //Assert field 
    
           // System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION' , e.getDmlStatusCode(0) );
        } //catch 
    
        Test.stopTest();
    }
    
    
}