global class AccountValuesPopulationBatch implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String userId=Userinfo.getUserId();
        String objname='Account';
        CommonMethods.addUserIdToSkipTrigger(userId,objname);
        //String migrationUserID = '\''+System.Label.MigrationUserID+'\'';
        //String custAccountRecType=System.Label.TestAccountCustomerRT;
      /*  String SOQL = 'SELECT Id,Name,Highest_Spend_Canvass__c,(select id,Name,OS_Total_OLI_Value__c,Canvass__c from Order_Sets__r where OS_No_of_Digital_OLI__c!=0 or OS_No_of_Print_OLI__c!=0) from Account '+
                       'WHERE CreatedById =  '+ MigrationUserID+' and RecordTypeId=:custAccountRecType'; */
        String SOQL = 'SELECT Id,Name,Highest_Spend_Canvass__c,ACC_DM_isTriggerExecuted__c from Account';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accList) {
        try{
            String migrationUserID = System.Label.MigrationUserID;
            String custAccountRecType=System.Label.TestAccountCustomerRT;
            List<Account> accNewList=[SELECT Id,Name,Highest_Spend_Canvass__c,ACC_DM_isTriggerExecuted__c,(select id,Name,OS_Total_OLI_Value__c,Canvass__c from Order_Sets__r where OS_No_of_Digital_OLI__c!=0 or OS_No_of_Print_OLI__c!=0) from Account where id IN :accList and CreatedById=:migrationUserID and RecordTypeId=:custAccountRecType and ACC_DM_isTriggerExecuted__c=false];
            if(accNewList!=null && accNewList.size()>0){
                for(Account acc:accNewList){
                    //Highest Spend Canvass value population on Account
                    if(acc.Order_Sets__r!=null && acc.Order_Sets__r.size()>0){
                        Decimal highestTotal=0;
                        String reqdCanvassCode=null;
                        for(Order_Group__c ordSet:acc.Order_Sets__r){
                            if(ordSet.OS_Total_OLI_Value__c>highestTotal){
                                highestTotal=ordSet.OS_Total_OLI_Value__c;
                                reqdCanvassCode=ordSet.Canvass__c;
                            }
                        }
                        if(reqdCanvassCode!=null){
                            acc.Highest_Spend_Canvass__c=reqdCanvassCode;
                        }  
                    }
                    acc.ACC_DM_isTriggerExecuted__c=true;
                }  
                update accNewList;
            }
        }catch(Exception e){
            System.debug('The exception is'+e);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch update of migrated Account ');
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        String userId=Userinfo.getUserId();
        String objname='Account';
        CommonMethods.removeUserIdToSkipTrigger(userId,objname);
    }

}