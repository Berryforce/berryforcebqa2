global class LOBCleanUpScheduler implements Schedulable {
   public Interface LOBCleanUpSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('LOBCleanUpSchedulerSchedulerHndlr');
        if(targetType != null) {
            LOBCleanUpSchedulerInterface obj = (LOBCleanUpSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}