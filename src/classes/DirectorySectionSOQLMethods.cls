public with sharing class DirectorySectionSOQLMethods {
    public static List<Directory_Section__c> getDirectorySectionById(Set<Id> DSIds) {
        return [SELECT Id, Exposure_Count__c, Display_Ad_Count__c FROM Directory_Section__c WHERE Id IN : DSIds];
    }
    public static List<Directory_Section__c> getDirectorySectionByDirIdWithEAS(Set<Id> DirIds) {
        return [SELECT Id, Directory__c FROM Directory_Section__c WHERE Directory__c IN : DirIds];
    }
    
    public static Map<id,Directory_Section__c> getDirectorySectionByDirIdAndDSId(Set<Id> setDirSection, Id directory) {
        return new Map<id,Directory_Section__c>([select id from Directory_Section__c where id in :setDirSection and Directory__c =:directory]);
    }
    
    public static List<Directory_Section__c> getYPDirectorySectionsByDirIds(Set<Id> directoryIds) {
        return [SELECT Id, Directory__c FROM Directory_Section__c WHERE Section_Page_Type__c = : CommonMessages.ypSecPageType AND Directory__c IN:directoryIds];
    }
    
    public static List<Directory_Section__c> getDirectorySectionsByDirIds(Set<Id> directoryIds) {
        return [SELECT Id, Section_Page_Type__c, Tear_Pages_Ready_For_Transmission__c, Name, Send_Tear_Pages_To_Elite__c FROM Directory_Section__c WHERE 
        		Directory__c IN:directoryIds];
    }

    public static map<Id, Directory_Section__c> getWPirectorySectionsByIds(Set<Id> setDirSecId) {
        return new map<Id,Directory_section__c>([SELECT Id, Directory__c,Section_Page_Type__c  FROM Directory_Section__c WHERE Section_Page_Type__c = 'WP' AND Id IN:setDirSecId]);
    }
}