global class MonthlyCronCreationBatch Implements Database.Batchable <sObject> {
	Date nxtBillDate;
	String mediaType = '';
	Boolean bFlag = false;
	
	global MonthlyCronCreationBatch(Date nxtBillDate, String mediaType) {
		this.nxtBillDate = nxtBillDate;
		this.mediaType = mediaType;
	}
	
	global MonthlyCronCreationBatch(Date nxtBillDate, String mediaType, boolean bFlag) {
		this.nxtBillDate = nxtBillDate;
		this.mediaType = mediaType;
		this.bFlag = bFlag;
	}
	
    global Database.queryLocator start(Database.BatchableContext bc) {
    	String strRT = System.Label.TestOLIRTLocal;
    	String strPrintMediaType = CommonMessages.printMediaType;
        String SOQL = 'SELECT Id, Order_Group__c, Next_Billing_Date__c, Media_Type__c, Is_P4P__c, P4P_Billing__c, Telco_Invoice_Date__c FROM Order_Line_Items__c ';
        
        if(String.isNotBlank(mediaType) && !bFlag) {
        	if(mediaType == CommonMessages.MCFMPMediaType) {
        		SOQL += 'WHERE Telco_Invoice_Date__c =:nxtBillDate AND Talus_Go_Live_Date__c = Null AND Media_Type__c =:strPrintMediaType ';
        	} else {
	        	SOQL += 'WHERE Next_Billing_Date__c =: nxtBillDate AND Talus_Go_Live_Date__c != Null ';	  
	        	if(mediaType == CommonMessages.P4P || mediaType == CommonMessages.MCP4PPrintCancelMediaType) {
					SOQL += 'AND Is_P4P__c = true AND P4P_Billing__c = true ';
				} else {
		        	SOQL += 'AND Media_Type__c =: mediaType ';
				}
        	}
        	if(mediaType == CommonMessages.MCP4PPrintCancelMediaType) {
        		SOQL += 'AND P4P_Current_Billing_Clicks_Leads__c > 0 AND IsCanceled__c = true ';
        	} else {
        		SOQL += 'AND IsCanceled__c = false ';
        	}
        	SOQL += 'AND RecordTypeId =: strRT ';
			
        } else {
        	if(mediaType == CommonMessages.MCFMPMediaType) {
				SOQL += 'WHERE Telco_Invoice_Date__c > TODAY AND Telco_Invoice_Date__c <=: nxtBillDate AND RecordTypeID =:strRT AND isCanceled__c = FALSE AND Talus_Go_Live_Date__c = Null AND Media_Type__c =:strPrintMediaType  ';
        	}
        	else {
        		SOQL += 'WHERE Next_Billing_Date__c > TODAY AND Next_Billing_Date__c <=: nxtBillDate AND RecordTypeID =:strRT AND isCanceled__c = FALSE AND Talus_Go_Live_Date__c != Null ';
        	}
        }
        SOQL += 'ORDER BY Order_Group__c, Media_Type__c';
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<Order_Line_Items__c> listOLI) {
    	List<Monthly_Cron__c> listMonthlyCron = new List<Monthly_Cron__c>();
    	Map<String, Map<Id, List<Order_Line_Items__c>>> mapMediaTypeOrderIdOLIList = new Map<String, Map<Id, List<Order_Line_Items__c>>>();
    	
    	for(Order_Line_Items__c OLI : listOLI) {    		
    		if(!mapMediaTypeOrderIdOLIList.containsKey(OLI.Media_Type__c)) {
    			mapMediaTypeOrderIdOLIList.put(OLI.Media_Type__c, new Map<Id, List<Order_Line_Items__c>>());
    		}
    		if(!mapMediaTypeOrderIdOLIList.get(OLI.Media_Type__c).containsKey(OLI.Order_Group__c)) {
    			mapMediaTypeOrderIdOLIList.get(OLI.Media_Type__c).put(OLI.Order_Group__c, new List<Order_Line_Items__c>());
    		}
    		mapMediaTypeOrderIdOLIList.get(OLI.Media_Type__c).get(OLI.Order_Group__c).add(OLI);
    	}
    	
    	for(String mediaType : mapMediaTypeOrderIdOLIList.keySet()) {
    		for(Id orderId : mapMediaTypeOrderIdOLIList.get(mediaType).keySet()) {
    			listMonthlyCron.add(createMonthlyCron(mapMediaTypeOrderIdOLIList.get(mediaType).get(orderId).get(0)));
    		}
    	}
    	insert listMonthlyCron;
    }

    global void finish(Database.BatchableContext bc) {
    	if(!bFlag) {
	    	if(String.isNotBlank(mediaType)) {
	    		if(mediaType == CommonMessages.printMediaType) {
	    			PrintMonthlyBilling obj = new PrintMonthlyBilling(nxtBillDate, mediaType);
	    			Database.executeBatch(obj, 20);
	    		} else if(mediaType == CommonMessages.digitalMediaType) {
	    			DigitalMonthlyBilling obj = new DigitalMonthlyBilling(nxtBillDate, mediaType);
	    			Database.executeBatch(obj, 20);
	    		} else if(mediaType == CommonMessages.P4P) {
	    			P4PProcessBatch obj = new P4PProcessBatch(nxtBillDate, mediaType);
	    			Database.executeBatch(obj, 20);
	    		} else if(mediaType == CommonMessages.MCFMPMediaType) {
	    			PrintFirstMonthBilling obj = new PrintFirstMonthBilling(nxtBillDate, mediaType);
	    			Database.executeBatch(obj, 20);
	    		} else if(mediaType == CommonMessages.MCP4PPrintCancelMediaType) {
	    			P4PPrintCanceledMonthlyBatch obj = new P4PPrintCanceledMonthlyBatch(nxtBillDate, mediaType);
	    			Database.executeBatch(obj, 20);
	    		}
	    	}
    	}
    }
    
    Monthly_Cron__c createMonthlyCron(Order_Line_Items__c OLI) {
    	Monthly_Cron__c monthlyCron = new Monthly_Cron__c(MC_Order_Set_Id__c = OLI.Order_Group__c);
    	if(mediaType == CommonMessages.MCFMPMediaType) {
    		monthlyCron.MC_OLI_Media_Type__c = CommonMessages.MCFMPMediaType;
    		monthlyCron.MC_OLI_Next_Billing_Date__c = nxtBillDate;
    	} else if(mediaType == CommonMessages.MCP4PPrintCancelMediaType) {    		
    		monthlyCron.MC_OLI_Media_Type__c = CommonMessages.MCP4PPrintCancelMediaType;
    		monthlyCron.MC_OLI_Next_Billing_Date__c = nxtBillDate;
    	} else { 	
    		if(OLI.Is_P4P__c && OLI.P4P_Billing__c) {
				monthlyCron.MC_OLI_Media_Type__c = CommonMessages.P4P;
			} else {
				monthlyCron.MC_OLI_Media_Type__c = OLI.Media_Type__c;    
			}	
			monthlyCron.MC_OLI_Next_Billing_Date__c = OLI.Next_Billing_Date__c;	
    	}					  
		return monthlyCron;    							  
    }
}