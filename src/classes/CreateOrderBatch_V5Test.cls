@isTest
public class CreateOrderBatch_V5Test{
    static testMethod void CreateOrderBatch_V5Test01() {
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;
        
        //Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        //insert objDirEd1;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = 'SEO';
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        lstProduct.add(newProduct);
        }
        insert lstProduct;
        List<pricebookentry> lstinsertPBE = new List<pricebookentry>();
        //pricebook2 objPB = TestMethodsUtility.createpricebook();
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        for(Product2 iterator : lstProduct) {
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
        lstinsertPBE.add(pbe);
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDPM.Product2__c = iterator.Id;
        }
        insert lstinsertPBE;
        insert lstDPM;
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        
        List<Opportunity> lstOpp = new List<Opportunity>();
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        
        newOpportunity.modifyid__c = '12345';
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        newOpportunity.CORE_Migration_ID__c  = '12345';
        newOpportunity.NotProcessed__c=false;
        newOpportunity.isLocked__c=false;
        newOpportunity.StageName='Closed Won';
        insert newOpportunity;
        lstOpp.add(newOpportunity);
         ID pricebookId;
       
          list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
        OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
        objOLI.PricebookEntryId = iterator.Id;
        objOLI.Billing_Duration__c = 12;
        objOLI.Directory__c = objDir.Id;
        objOLI.Directory_Edition__c = objDirEd.Id;
        objOLI.Full_Rate__c = 30.00;
        objOLI.UnitPrice = 30.00;
        objOLI.Package_ID__c = '123456';
        objOLI.Billing_Partner__c = objOLI.Id;
        objOLI.OpportunityId = newOpportunity.Id;
        objOLI.Directory_Heading__c = objDH.Id;
        objOLI.Directory_Section__c = objDS.Id;
        lstOLI.add(objOLI);
        mapPBE.put(iterator.Id, iterator);
        }
        
        newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        
        newOpportunity.modifyid__c = null;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        newOpportunity.CORE_Migration_ID__c  = '23456';
        newOpportunity.NotProcessed__c=false;
        newOpportunity.isLocked__c=false;
        newOpportunity.StageName='Closed Won';
        insert newOpportunity;
        lstOpp.add(newOpportunity);

        for(PricebookEntry iterator : lstPBE) {
        OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
        objOLI.PricebookEntryId = iterator.Id;
        objOLI.Billing_Duration__c = 12;
        objOLI.Directory__c = objDir.Id;
        objOLI.Directory_Edition__c = objDirEd.Id;
        objOLI.Full_Rate__c = 30.00;
        objOLI.UnitPrice = 30.00;
        objOLI.Package_ID__c = '123456';
        objOLI.Billing_Partner__c = objOLI.Id;
        objOLI.OpportunityId = newOpportunity.Id;
        objOLI.Directory_Heading__c = objDH.Id;
        objOLI.Directory_Section__c = objDS.Id;
        lstOLI.add(objOLI);
        mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;
        
         
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Test.startTest();
        Order_Line_Items__c newOrLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        newOrLI.Parent_ID__c='pkg1';
        newOrLI.Parent_ID_of_Addon__c='addpkg1';
        newOrLI.Product2__c =lstProduct[0].id;
        newOrLI.Media_Type__c = 'Digital';
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        lstOrderLI.add(newOrLI);
        if(lstOrderLI.size()>0)
        insert lstOrderLI;
        
        DMOpportunityBatch__c objDMOpp = new DMOpportunityBatch__c(name='1');
        insert objDMOpp;
        
        CreateOrderBatch_V5 objNI = new CreateOrderBatch_V5(objDMOpp);
        new CreateOrderBatch_V5(null, objDMOpp);
        Database.executeBatch(objNI);
        objNI.execute(null, lstOpp);
    }
}