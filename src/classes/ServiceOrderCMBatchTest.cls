@isTest
private Class ServiceOrderCMBatchTest {
    static testmethod void soCMNewDuplicatelogic() {
        Database.BatchableContext bc;
        Recordtype rttelcoId = TestMethodsUtility.getRecordType('Telco__c','Telco Provider');
        Account objAcc = TestMethodsUtility.generateTelcoAccount();
        insert objAcc;
        Telco__c objTelco = TestMethodsUtility.generateTelco(objAcc.Id);
        objTelco.RecordTypeId = rttelcoId.id;
        insert objTelco;
        Directory__c objDir = TestMethodsUtility.createDirectory();
        Directory_Section__c objDirSec = TestMethodsUtility.createDirectorySection(objDir);
        Recordtype rtId = TestMethodsUtility.getRecordType('Service_order_stage__c','Annual Listings');
        Lead_Assignment_Rules__c objLAR = new Lead_Assignment_Rules__c(Name='Test LAR',Area_code__c=170,Exchange__c=444,Directory__c=objDir.id,Directory_Section__c=objDirSec.id);
        insert objLAR;
        listing__c objList = TestMethodsUtility.generateMainListing();
        insert objList;
        List<service_order_stage__c> objSOLst = new List<service_order_stage__c>();
        service_order_stage__c objSO = new service_order_stage__c(Name = 'Test SONew',SOS_LastName_BusinessName__c='Test SONew',Action__c = 'New',Phone__c = '(178)444-1398',First_Name__c = 'First Name',Listing_City__c='Texas',Listing_State__c='TX',Disconnected__c = false,Bus_Res_Gov_Indicator__c = 'Business',BOID__c = 1234,Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false,caption_header__c=true,Associated_to_listing__c= objList.Id);
        insert objSO;
        service_order_stage__c objSOCM = new service_order_stage__c(Name = 'Test SONew',SOS_LastName_BusinessName__c='Test SONew',Action__c = 'New',Phone__c = '(178)444-1398',First_Name__c = 'First Name',Listing_City__c='Texas',Listing_State__c='TX',Disconnected__c = false,Bus_Res_Gov_Indicator__c = 'Business',BOID__c = 1234,Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false,caption_member__c=true,under_caption__c= objSO.Id,SOS_First_Caption_Member__c='1',OTN__c='1',SO_OAD__c='1');
        insert objSOCM;
        service_order_stage__c objSOCM1 = new service_order_stage__c(Name = 'Test SONew',SOS_LastName_BusinessName__c='Test SONew',Action__c = 'New',Phone__c = '(178)444-1398',First_Name__c = 'First Name',Listing_City__c='Texas',Listing_State__c='TX',Disconnected__c = false,Bus_Res_Gov_Indicator__c = 'Business',BOID__c = 1234,Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false,caption_member__c=true,under_caption__c= objSO.Id,SOS_First_Caption_Member__c='1',OTN__c='1',SO_OAD__c='1');
        insert objSOCM1;
        set<Id> setSOId = new set<Id>();
        setSOId.add(objSOCM.Id);
        setSOId.add(objSOCM1.Id);
        objSOLst = [SELECT strDLcombo__c,strSOSCombination__c,StrBOCDisconnect__c,SOS_LastName_BusinessName__c,StrSOLstMatch__c,Action__c,Associated_to_Dir_Listing__c,Associated_to_Listing__c,BEX__c,BOID__c,Bus_Res_Gov_Indicator__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,CLEC_Provider__c,Cross_Reference_Indicator__c,Cross_Reference_Text__c,Data_Feed_Type__c,Data_Feed_Type_2__c,Designation__c,Directory_Heading__c,Directory_Section__c,Directory_Section__r.Section_Page_Type__c,Directory__c,Disconnected__c,
                    Disconnect_Reason__c,Effective_Date__c,Exchange__c,Fallout__c,First_Name__c,Honorary_Title__c,Id,Indent_Level__c,Indent_Order__c,Initial_Match_on_DL__c,Initial_Match_on_Listing__c,Is_Lead__c,Lead_Account_Source__c,Lead_Owner__c,Lead_Record_Type__c,Lead_Source__c,Lead_Status__c,Left_Telephone_Phrase__c,Lineage_Title__c,Line__c,Listing_City__c,Listing_Country__c,Listing_Postal_Code__c,Listing_PO_Box__c,Listing_State__c,Listing_Street_Number__c,Listing_Street__c,Listing_Type__c,
                    Manual_Sort_As_Override__c,Name,Normalized_Designation__c,Normalized_First_Name__c,Normalized_Honorary_Title__c,Normalized_Last_Name_Business_Name__c,Normalized_Lineage_Title__c,Normalized_Listing_City__c,Normalized_Listing_Postal_Code__c,Normalized_Listing_PO_Box__c,Normalized_Listing_Street_Name__c,Normalized_Listing_Street_Number__c,Normalized_Phone__c,Normalized_Secondary_Surname__c,Omit_Address_OAD__c,Phone_Override_Indicator__c,Phone_Override__c,Phone_Type__c,Phone__c,
                    Primary_Canvass__c,RecordTypeId,Region__c,Right_Telephone_Phrase__c,Secondary_Surname__c,Service_Order__c,Telco_Provider__c,Under_Caption__c,
                    Under_caption__r.Directory_Heading__c,Under_caption__r.Directory__c,Under_Caption__r.Associated_to_Dir_Listing__c,Under_Sub_Caption__c,
                    Under_Sub_Caption__r.Associated_to_Dir_Listing__c,Under_caption__r.Directory_Section__c,Under_Caption__r.Fallout__c,
                    Under_Caption__r.Associated_to_Listing__c,Under_caption__r.Caption_Member__c,Year__c, Batch_ID__c,
                    Scheduled_Process_Complete__c,Createddate,OUT_LAST_NAME_BUSINESS_NAME__c,OUT_FIRST_NAME__c,OUT_PHONE_NUMBER__c,OUT_STREET_NUMBER__c,
                    OUT_STREET_NAME__c,OUT_PO_BOX__c,OUT_CITY__c,OUT_STATE__c,OUT_POSTAL_CODE__c,Telco_Sort_Order__c,Do_Not_Create_YP__c,Omit_Phone_OTN__c,
                    SO_OAD__c,OTN__c,SOS_First_Caption_Member__c FROM Service_Order_Stage__c where Id IN: setSOId];
        
        Test.StartTest();
            ServiceOrdercaptionMemberBatchController objSOBatch = new ServiceOrdercaptionMemberBatchController(objDir.id,null);
            objSOBatch.start(bc);
            objSOBatch.execute(bc,objSOLst);
        Test.StopTest();
   }
   static testmethod void TestServiceOrderNewMatchingCMBatchController(){
            Database.BatchableContext bc;
            Recordtype rttelcoId = TestMethodsUtility.getRecordType('Telco__c','Telco Provider');
            Recordtype rtId = TestMethodsUtility.getRecordType('Service_order_stage__c','Annual Listings');
            Account objAcc = TestMethodsUtility.generateTelcoAccount();
            insert objAcc;
            Telco__c objTelco = TestMethodsUtility.generateTelco(objAcc.Id);
            objTelco.RecordTypeId = rttelcoId.id;
            insert objTelco;
            Directory__c objDir = TestMethodsUtility.createDirectory();
            Directory_Section__c objDirSec = TestMethodsUtility.createDirectorySection(objDir);
            Directory_Heading__c objDirH = TestMethodsUtility.createDirectoryHeading();
            listing__c Objlst = TestMethodsUtility.generateListing();
            insert Objlst;
            directory_listing__c objDirLst = new directory_listing__c(Name='TestMatchso',SL_Last_Name_Business_Name__c='TestMatchso',Phone_number__c='(444)222-1111',Directory__c=objDir.Id,Directory_section__c=objDirSec.Id,Caption_member__c=true,listing__c=Objlst.Id);
            insert objDirLst;
            List<service_order_stage__c> objSOLst = new List<service_order_stage__c>();
            service_order_stage__c objSO = new service_order_stage__c(Name = 'TestMatchso',SOS_LastName_BusinessName__c='TestMatchso',Action__c = 'New',Phone__c = '(444)222-1111',Bus_Res_Gov_Indicator__c = 'Business',Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Directory_Heading__c=objDirH.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false,caption_member__c=true);
            insert objSO;
            objSOLst = [SELECT strDLcombo__c,strSOSCombination__c,SOS_LastName_BusinessName__c,StrBOCDisconnect__c,StrSOLstMatch__c,Action__c,Associated_to_Dir_Listing__c,Associated_to_Listing__c,BEX__c,BOID__c,Bus_Res_Gov_Indicator__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,CLEC_Provider__c,Cross_Reference_Indicator__c,Cross_Reference_Text__c,Data_Feed_Type__c,Data_Feed_Type_2__c,Designation__c,Directory_Heading__c,Directory_Section__c,Directory_Section__r.Section_Page_Type__c,Directory__c,Disconnected__c,
                    Disconnect_Reason__c,Effective_Date__c,Exchange__c,Fallout__c,First_Name__c,Honorary_Title__c,Id,Indent_Level__c,Indent_Order__c,Initial_Match_on_DL__c,Initial_Match_on_Listing__c,Is_Lead__c,Lead_Account_Source__c,Lead_Owner__c,Lead_Record_Type__c,Lead_Source__c,Lead_Status__c,Left_Telephone_Phrase__c,Lineage_Title__c,Line__c,Listing_City__c,Listing_Country__c,Listing_Postal_Code__c,Listing_PO_Box__c,Listing_State__c,Listing_Street_Number__c,Listing_Street__c,Listing_Type__c,
                    Manual_Sort_As_Override__c,Name,Normalized_Designation__c,Normalized_First_Name__c,Normalized_Honorary_Title__c,Normalized_Last_Name_Business_Name__c,Normalized_Lineage_Title__c,Normalized_Listing_City__c,Normalized_Listing_Postal_Code__c,Normalized_Listing_PO_Box__c,Normalized_Listing_Street_Name__c,Normalized_Listing_Street_Number__c,Normalized_Phone__c,Normalized_Secondary_Surname__c,Omit_Address_OAD__c,Phone_Override_Indicator__c,Phone_Override__c,Phone_Type__c,Phone__c,
                    Primary_Canvass__c,RecordTypeId,Region__c,Right_Telephone_Phrase__c,Secondary_Surname__c,Service_Order__c,Telco_Provider__c, Batch_ID__c,
                    Under_Caption__c,Under_caption__r.Directory_Heading__c,Under_caption__r.Directory__c,Under_Caption__r.Associated_to_Dir_Listing__c,Under_Sub_Caption__c,Under_Sub_Caption__r.Associated_to_Dir_Listing__c,Under_caption__r.Directory_Section__c,Under_Caption__r.Fallout__c,Under_Caption__r.Associated_to_Listing__c,Under_caption__r.Caption_Member__c,Year__c,
                    Scheduled_Process_Complete__c,Createddate,OUT_LAST_NAME_BUSINESS_NAME__c,OUT_FIRST_NAME__c,OUT_PHONE_NUMBER__c,OUT_STREET_NUMBER__c,OUT_STREET_NAME__c,OUT_PO_BOX__c,OUT_CITY__c,OUT_STATE__c,OUT_POSTAL_CODE__c,Telco_Sort_Order__c,Do_Not_Create_YP__c,Omit_Phone_OTN__c,SO_OAD__c,OTN__c FROM Service_Order_Stage__c where Id =: objSO.Id];
        
            
            Test.StartTest();
                ServiceOrdercaptionMemberBatchController objSOCMBatch1 = new ServiceOrdercaptionMemberBatchController(objDir.id,null);
                objSOCMBatch1.start(bc);
                objSOCMBatch1.execute(bc,objSOLst);
            Test.StopTest();   
   }
   
   
  
}