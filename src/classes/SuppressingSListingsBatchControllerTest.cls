@isTest
public class SuppressingSListingsBatchControllerTest
{
    static testmethod void myunitTest()
    {
        Test.startTest();
        Account objAccount = new Account();
        objAccount.Name = 'Test Account 1';
        objAccount.Phone = '(456)789-3456';
        objAccount.Open_claim__c = false;
       
        objAccount.Delinquency_Indicator__c= false;
        objAccount.Letter_Renewal_Sequence__c = '1';
        insert objAccount;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account2';
        objAccount1.Phone = '(456)789-3478';
        objAccount1.Open_claim__c = false;

        objAccount1.Delinquency_Indicator__c=false;
        objAccount1.Letter_Renewal_Sequence__c = '1';
        insert objAccount1;
        
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = objAccount.Id;
        insert objContact;
        
        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = objAccount.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=objAccount.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        insert objOpportunity;
        
        Directory__c objDir = Testmethodsutility.createDirectory();
        Directory_Section__c dsection=Testmethodsutility.createDirectorySection(objDir);
        Directory_Heading__c heading=Testmethodsutility.createDirectoryHeading();
      /*  Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        insert objDirE;
      */  
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';

         
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print Graphic';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        
        insert objProd1;
        //Listing__c  listing = Testmethodsutility.createListing('Additional Listing');
        
        Listing__c  objMainListing = Testmethodsutility.createListing('Main Listing');
        Listing__c  listing = Testmethodsutility.generateListing('Additional Listing');
        listing.Main_Listing__c = objMainListing.id;
        insert listing;        
        //telco__c TCP = [SELECT Id from Telco__c LIMIT 1]; 
            
      
        //Directory_Listing__c DL = Testmethodsutility.createDirectoryListing('Address Override RT',listing);
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id);
        insert objOrderGroup ;
            Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Media_Type__c='Print',Requires_Scoped_Suppression_Processing__c = TRUE,Scoped_Suppression_Processing_Complete__c = FALSE,Listing__c=listing.id,Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,UnitPrice__c=100,Directory__c=objdir.id,Directory_Section__c=dsection.id);
            insert objOrderLineItem;

      //   Directory_Listing__c dl1 =new Directory_Listing__c(Listing__c =listing .id,Name = 'testDListing', Telco_Provider__c= TCP.Id,Directory__c=objdir.id);
       //  insert dl1;
       //  Directory_Listing__c dl2 =new Directory_Listing__c(Listing__c =listing .id,Name = 'testDListing1', Telco_Provider__c= TCP.Id,Directory__c=objdir.id);
       //  insert dl2;   
         Directory_Listing__c dl1 =new Directory_Listing__c(Listing__c =listing .id,Name = 'testDListing', Directory__c=objdir.id,Directory_Section__c=dsection.id);
         insert dl1;
         Directory_Listing__c dl2 =new Directory_Listing__c(Listing__c =listing .id,Name = 'testDListing1', Directory__c=objdir.id,Directory_Section__c=dsection.id);
         insert dl2;                        
            System.debug('$$$$$oliLst '+objOrderLineItem);
            System.debug('$$$$$oliLst '+objOrderLineItem);
            
            Order_Line_Items__c oli=[select id, strSuppressionCheck__c from Order_Line_Items__c where Id= :objOrderLineItem.id ];
            Directory_Listing__c  d=[select id, strSuppressionCheck__c from Directory_Listing__c  where Id= :dl1.id ];
            Directory_Listing__c  d1=[select id, strSuppressionCheck__c from Directory_Listing__c  where Id= :dl2.id ];
            database.executebatch(new  SuppressingScopedListingsBatchController(),2000);
            Test.stopTest();
        SuppressingScopedListingsBatchController sh1 = new SuppressingScopedListingsBatchController();
        String sch = '0 0 23 * * ?'; system.schedule('SuppressingScopedListing', sch, sh1);  
        String SOQL = 'select id,CreatedDate, Listing__c,strSuppressionCheck__c  from Order_Line_Items__c where Requires_Scoped_Suppression_Processing__c = TRUE AND Scoped_Suppression_Processing_Complete__c = FALSE AND Media_Type__c=\'Print\' order by createdDate ASC' ;              
        SuppressingScopedListingsBatchController sh2 = new SuppressingScopedListingsBatchController(SOQL);
    }
}