/***********************************************************
Apex Class to do Start Fulfillment for Manual and Yodle Dffs
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 04/02/2015
$Id$
************************************************************/
public with sharing class StartManualFlmntController {

    //getters, setters
    public String dffId {
        get;
        set;
    }
    public String prflId {
        get;
        set;
    }
    public String prflNm {
        get;
        set;
    }

    public Digital_Product_Requirement__c dff;
    public Order_Line_Items__c ol;
    public Modification_Order_Line_Item__c mOL;
    public Id usrId;

    String gnralRcdTyp = Label.ManualRecordTypes;
    String yodleRcdTyp = Label.YodleRecordTypes;

    public StartManualFlmntController(ApexPages.StandardController controller) {

        dffId = controller.getId();
        Id prflId = userInfo.getProfileId();
        usrId = userInfo.getUserId();
        Profile prfl = [SELECT Name from Profile where id = : prflId];
        prflNm = prfl.Name;

    }

    public void stFlmnt() {

        if (prflNm != 'ICR') {

            CommonUtility.msgWarning(CommonUtility.noPrflAccess);

        } else {

        dff = [SELECT Id, Name, OrderLineItemID__c, ModificationOrderLineItem__c, Submit_To_Yodle__c, Submit_Yodle_Cancel__c, Submit_Yodle_Update__c, RecordType.DeveloperName, CreatedById,
            OwnerId, Fulfillment_Submit_status__c, RecordTypeId, FSD_Status__c, Final_Status__c, OrderLineItemID__r.Id, OrderLineItemID__r.Name, OrderLineItemID__r.Status__c, OrderLineItemID__r.Talus_Go_Live_Date__c, OrderLineItemID__r.Talus_Cancel_Date__c, 
            ModificationOrderLineItem__r.Id, ModificationOrderLineItem__r.Name, ModificationOrderLineItem__r.Digital_Product_Requirement__c, ModificationOrderLineItem__r.Status__c, ModificationOrderLineItem__r.Talus_Go_Live_Date__c
            from Digital_Product_Requirement__c where id = : dffId
        ];
        
        System.debug('************dff************' + dff);
        
            if (dff.OrderLineItemID__c != null) {

                //System.debug('************OrderLineItem************' + dff.OrderLineItemID__r);
                
                if (dff.OwnerId != usrId) {

                    dff.OwnerId = usrId;
                    dff.FSD_Status__c = 'Complete';
                    dff.Final_Status__c = 'In Progress';

                    CommonUtility.msgConfirm(CommonUtility.sAssignedDff);

                } else {

                    CommonUtility.msgInfo(CommonUtility.dffAlrdyAssnd);

                }

                if (dff.OrderLineItemID__r.Status__c == 'New') {

                    dff.OrderLineItemID__r.Status__c = 'In Progress';
                    ol = dff.OrderLineItemID__r;
                    update ol;

                }

            } else {

                //System.debug('************ModifedOrderLineItem************' + dff.ModificationOrderLineItem__r);
                
                if (dff.OwnerId != usrId) {

                    dff.OwnerId = usrId;
                    dff.FSD_Status__c = 'Complete';
                    dff.Final_Status__c = 'In Progress';

                    CommonUtility.msgConfirm(CommonUtility.sAssignedDff);

                } else {

                    CommonUtility.msgInfo(CommonUtility.dffAlrdyAssnd);

                }

                if (dff.ModificationOrderLineItem__r.Status__c == 'New') {

                    dff.ModificationOrderLineItem__r.Status__c = 'In Progress';
                    mOL = dff.ModificationOrderLineItem__r;
                    update mOL;

                }

            }
            
            update dff;

        }

    }

}