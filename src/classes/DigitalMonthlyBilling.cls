global class DigitalMonthlyBilling implements Database.Batchable<sObject>, Database.Stateful {
    
	String mediaType;
    global Date dtBD;
    global Set<Id> setOrderSetIds = new Set<Id>();
    
    global DigitalMonthlyBilling(Date dtBillingDate){
       dtBD = dtBillingDate;
       System.debug('Testingg dtBillingDate '+dtBillingDate);
    }
    
    global DigitalMonthlyBilling(Date dtBillingDate, String mediaType){
       dtBD = dtBillingDate;
       this.mediaType = mediaType;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, MC_Order_Set_Id__c FROM Monthly_Cron__c WHERE MC_OLI_Media_Type__c =: mediaType AND MC_OLI_Next_Billing_Date__c =: dtBD';
        return Database.getQueryLocator(query);
    }  
    
	global void execute(Database.BatchableContext bc, List<Monthly_Cron__c> listMonthlyCron) {
		if(!Test.isRunningTest()) {
			Set<Id> orderSetIds = new Set<Id>();
			
			for(Monthly_Cron__c MC : listMonthlyCron) {
				orderSetIds.add(MC.MC_Order_Set_Id__c);
			}
			
			List<Order_Line_Items__c> listOLI = new List<Order_Line_Items__c>();
        	String strRT = System.Label.TestOLIRTLocal;
			
			listOLI =  [SELECT Account__c, Id, Name, Order__c, Order__r.Account__c, Canvass__c, Description__c, 
					   Discount__c,Fulfilled_on__c, FulfillmentDate__c, ListPrice__c, Opportunity__c, Opportunity__r.Name, Product2__c, Product2__r.RGU__c,
					   ProductCode__c, Quantity__c, Sent_to_fulfillment_on__c, Billing_Contact__c, Directory_Edition__c, Directory__c,
					   Billing_End_Date__c, Billing_Frequency__c, Billing_Partner__c, Billing_Start_Date__c,Telco__c,Telco_Invoice_Date__c, Print_First_Bill_Date__c,
					   Digital_Product_Requirement__c,Payment_Method__c, UnitPrice__c, Payments_Remaining__c, Successful_Payments__c, Payment_Duration__c, 
					   Service_Start_Date__c, Service_End_Date__c, IsCanceled__c, Talus_Go_Live_Date__c, 
					   Talus_Fulfillment_Date__c,  Current_Daily_Prorate__c, Quote_Signed_Date__c, Digital_Product_Requirement__r.business_phone_number_office__c, 
					   Quote_signing_method__c, Total_Prorated_Days_for_contract__c, Cutomer_Cancel_Date__c, Talus_Cancel_Date__c, 
					   Order_Group__c, Billing_Close_Date__c, Order_Anniversary_Start_Date__c,Last_Billing_Date__c, Next_Billing_Date__c, Contract_End_Date__c, 
					   Line_Status__c, Continious_Billing__c, Prorate_Stored_Value__c, Cancellation__c, Total_Prorate__c, 
					   Product2__r.Name, Product2__r.Family, Prorate_Credit__c, Prorate_Credit_Days__c, Current_Billing_Period_Days__c, Order_Line_Total__c, 
					   BillingChangeNoofDays__c, Billing_Change_Prorate_Credit__c, BillingChangeProrateCreditDays__c, BillingChangesPayment__c, 
					   Order_Billing_Date_Changed__c, OriginalBillingDate__c, P4P_Price_Per_Click_Lead__c, P4P_Current_Billing_Clicks_Leads__c, 
					   P4P_Billing__c, Is_P4P__c, P4P_Current_Months_Clicks_Leads__c, Billing_Partner_Account__c, Linvio_Payment_Method_Id__c, Account__r.BillingCity, 
					   Account__r.BillingCountry, Account__r.BillingPostalCode, Account__r.BillingState, Account__r.BillingStreet, Billing_Contact__r.Name, 
					   Billing_Contact__r.Paperless__c, Billing_Contact__r.Phone, Order_Line_Items__c.Product_Type__c, Order_Line_Items__c.Canvass__r.Name, 
					   Order_Line_Items__c.Canvass__r.Canvass_Code__c,Order_Line_Items__c.CMR_Name__c,Order_Line_Items__c.Directory_Code__c,Order_Line_Items__c.Edition_Code__c, 
					   Billing_Contact__r.MailingCity, Billing_Contact__r.MailingCountry, Billing_Contact__r.MailingPostalCode, Billing_Contact__r.MailingState, 
					   Billing_Contact__r.MailingStreet, Media_Type__c, Package_ID_External__c, 
					   (Select Id, Name, c2g__CreditNote__c, c2g__NetValue__c, c2g__UnitPrice__c, Order_Line_Item__c, Sales_Invoice_Line_Item__c, 
					   c2g__CreditNote__r.c2g__DueDate__c, c2g__CreditNote__r.c2g__CreditNoteTotal__c, c2g__CreditNote__r.c2g__CreditNoteStatus__c, 
					   c2g__CreditNote__r.c2g__CreditNoteDate__c,Payment__c  From Sales_Credit_Note_Line_Items__r where Is_Billed__c=false AND c2g__CreditNote__r.c2g__CreditNoteStatus__c = 'In Progress' 
					   order by c2g__CreditNote__r.c2g__DueDate__c) 
					   FROM Order_Line_Items__c 
					   WHERE IsCanceled__c = false 
					   AND Next_Billing_Date__c =: dtBD 
					   AND Talus_Go_Live_Date__c != Null 
					   AND ((Successful_Payments__c != null 
					   AND Payments_Remaining__c != null 
					   AND Payments_Remaining__c >= 1) 
					   OR Continious_Billing__c = true) 
					   AND RecordTypeId =: strRT 
					   AND Media_Type__c = 'Digital' AND Order_Group__c IN: orderSetIds];
			
			if(listOLI.size() > 0) {
				CommonRecurHandlerController_V1.BerryBillingCycle(listOLI);    
				for(Order_Line_Items__c OLI : listOLI) {
	        		setOrderSetIds.add(OLI.Order_Group__c);
	        	} 		
			}
		}       
	}
    
    global void finish(Database.BatchableContext bc) {
         AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
         /*String[] toAddresses = new String[] {a.CreatedBy.Email};
         toAddresses.addAll(System.Label.LocalBillingEmails.split(';'));*/
         Set<String> recipientIds = new Set<String>();
         recipientIds.addAll(User_Ids_For_Email__c.getInstance('LocalBilling').User_Ids__c.split(';'));
         recipientIds.add(UserInfo.getUserId());
         if(a.NumberOfErrors > 0) {
            futureCreateErrorLog.createErrorRecordBatch(a.ExtendedStatus, 'Digital Monthly Billing batch process status :'  + a.Status+'. The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + setOrderSetIds.size() +
            ' batches with '+ a.NumberOfErrors + ' failures.', 'Digital Monthly Billing batch process');
            for(String str : recipientIds) {
	            CommonEmailUtils.sendHTMLEmailForTargetObject(str, 'Digital Monthly Billing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + setOrderSetIds.size() +
	            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.');
            }
         }else if(a.JobItemsProcessed != a.TotalJobItems) {
            futureCreateErrorLog.createErrorRecordBatch('Either the batch job was aborted or Job failed to process due to internal error', 'Digital Monthly Billing batch process status : ' + a.Status+'. The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + setOrderSetIds.size() +
                ' batches with '+ a.NumberOfErrors + ' failures.', 'Digital Monthly Billing batch process');
            
            for(String str : recipientIds) {
	            CommonEmailUtils.sendHTMLEmailForTargetObject(str, 'Digital Monthly Billing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + setOrderSetIds.size() +
            	' batches with '+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.');
            }
            
        }
        else {
            for(String str : recipientIds) {
	            CommonEmailUtils.sendHTMLEmailForTargetObject(str, 'Digital Monthly Billing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + setOrderSetIds.size() +
            	' batches with '+ a.NumberOfErrors + ' failures.');
            }
        }
        
    }
}