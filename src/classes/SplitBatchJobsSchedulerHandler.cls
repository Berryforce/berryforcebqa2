public class SplitBatchJobsSchedulerHandler implements SplitBatchJobsScheduler.SplitBatchJobsSchedulerInterface {
	public void execute(SchedulableContext sc) {
        List<SplitBatchJobs__c> listSplitJobs = [SELECT Id,Name FROM SplitBatchJobs__c];
		if(listSplitJobs.size()> 0) {
        	delete listSplitJobs;
        }
        //Added by sathish
        list<StatementSplitBatch__c> lstStatementJob = [Select Id from StatementSplitBatch__c];
        if(lstStatementJob.size() > 0) {
        	delete lstStatementJob;
        }
    }    

}