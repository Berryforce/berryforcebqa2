/************************************
Created By: Reddy(pyellanki2@csc.com)
Updated Date: 10/02/2014
$Id$
*************************************/
public Class TrackDFFHandlerController {
    public static void onAfterUpdate(List<Track_DFF_Update__c> lstTDFF, map<Id, Track_DFF_Update__c> oldMap){
    Set<Id> DffIds = new Set<Id>();
    Set<Id> mDffIds = new Set<Id>();
        
        for(Track_DFF_Update__c iterator: lstTDFF){
            if(iterator.Status__c == 'Fulfilled' && iterator.Add_Ons__c == true){
                
                DffIds.add(iterator.Data_Fulfillment_Form__c);
                
            }
            if(iterator.Status__c == 'Fulfilled' && iterator.Updated_Info__c.contains('effective_date')){
                mDffIds.add(iterator.Data_Fulfillment_Form__c);
            } 
        }
        
        if(DffIds.size()>0){
            updtOLIDates(DffIds);        
        }
        if(mDffIds.size()>0){
            updtMOLI(mDffIds);
        }            
        
    }

    public static void onAfterInsert(List<Track_DFF_Update__c> lstTDFF){
    Set<Id> mDffIds = new Set<Id>();
        
        for(Track_DFF_Update__c iterator: lstTDFF){
            if(iterator.Status__c == 'Applied' && iterator.Updated_Info__c.contains('effective_date')){

                mDffIds.add(iterator.Data_Fulfillment_Form__c);

            }
        }

        if(mDffIds.size()>0){
            updtMOLI(mDffIds);
        }

    }
    
    //Method to update Add-On OLI's status 
    public static void updtOLIDates(Set<Id> dffId){
    List<Order_Line_Items__c> olLst = new List<Order_Line_Items__c>();
        
        for(Order_Line_Items__c iterator:[SELECT Id, Name, isCanceled__c, Status__c, Talus_Go_Live_Date__c from Order_Line_Items__c where Digital_Product_Requirement__r.Data_Fulfillment_Form__c IN: dffId and Product2__r.Product_or_Addon__c = 'Add-on']){
            
            //System.debug('************OLILIST************' + iterator);
            
            if(iterator.isCanceled__c == true && iterator.Status__c == 'Fulfilled' && iterator.Talus_Go_Live_Date__c != null){
                
                iterator.Status__c = 'Cancelled';
                iterator.Talus_Cancel_Date__c = system.Today();
                olLst.add(iterator);
                
            } else if(iterator.isCanceled__c != true && iterator.Status__c == null && iterator.Talus_Go_Live_Date__c == null){
            
                iterator.Status__c = 'Fulfilled';
                iterator.Talus_Go_Live_Date__c = system.Today();
                olLst.add(iterator);
            
            }          
        }
        
        //System.debug('************OLILIST_TO_UPDATE************' + olLst.size());
        
        if(olLst.size()>0){
            update olLst;
        }
    }

    //Method to update MOLI with Patch completion date
    public static void updtMOLI(set<Id> dffId){
    List<Modification_Order_Line_Item__c> mOlLst = new List<Modification_Order_Line_Item__c>();
    
        for(Modification_Order_Line_Item__c mOL: [SELECT Id, Name, Patch_Complete_Date__c from Modification_Order_Line_Item__c where Digital_Product_Requirement__c != null and Digital_Product_Requirement__c IN: dffId]){
            
            mOL.Patch_Complete_Date__c = system.Today();
            mOlLst.add(mOL);

        }

        System.debug('************MOLILIST_TO_UPDATE************' + mOlLst.size());

        if(mOlLst.size()>0){
            update mOlLst;
        }    

    }

}