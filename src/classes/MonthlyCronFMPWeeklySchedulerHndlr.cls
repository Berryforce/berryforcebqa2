global class MonthlyCronFMPWeeklySchedulerHndlr implements MonthlyCronFMPWeeklyScheduler.MonthlyCronFMPWeeklySchedulerInterface{
   global void execute(SchedulableContext sc) {
        MonthlyCronCreationBatch obj = new MonthlyCronCreationBatch(system.today() + 7, 'FMP', true);
        Database.executeBatch(obj, 2000);
    }   
}