public class DirectoryEditionSOQLMethods {  
    public static list<Directory_Edition__c> getCurrentDEByDirectory(set<Id> setId) {
        return [Select ID, Directory__c,Edition_Code__c,name,Pub_Date__c,Bill_Prep__c,New_Print_Bill_Date__c,ALPHA_PAGES_DUE__c,CLASS_PAGES_DUE__c,Final_Service_Order_Due_to_Berry__c,Year__c from Directory_Edition__c where Directory__c IN:setId and Book_Status__c = 'NI'];
    }
    
    public static list<Directory_Edition__c> getDirEditionByDirIds(set<Id> directoryIds){
        return [SELECT Id, Edition_Code__c, Directory__c, Book_Status__c, Name FROM Directory_Edition__c WHERE Directory__c IN : directoryIds];
    }
    
    public static list<Directory_Edition__c> getDirEditionByLSACode(set<String> setLSACode){
        return [SELECT Id, Name, LSA_Directory_Version__c, Book_Status__c, Directory__c , Directory__r.Publication_Company__c, Directory__r.Publication_Company_Code__c, Directory__r.directory_code__c, Pub_Date__c, Directory__r.State__c  FROM Directory_Edition__c WHERE LSA_Directory_Version__c IN:setLSACode];
    }
    
    public static list<Directory_Edition__c> getDirEditionByLSACodeDirNo(String strLSACode, String strDirNo){
        return [SELECT Id, Name,Directory_Edition_Code__c, LSA_Directory_Version__c, Book_Status__c, Directory__c , Directory__r.Publication_Company__c, Directory__r.Publication_Company_Code__c, Directory__r.directory_code__c, Pub_Date__c, Directory__r.State__c  FROM Directory_Edition__c WHERE LSA_Directory_Version__c = :strLSACode and Directory_Code__c = :strDirNo];
    }
    
    public static list<Directory_Edition__c> getBOTSDEByDirectory(set<Id> setId) {
        return [Select ID, Directory__c,Pub_Date__c,Bill_Prep__c,New_Print_Bill_Date__c,Edition_code__c from Directory_Edition__c where Directory__c IN:setId and Book_Status__c = 'BOTS'];
    }
    
    public static list<Directory_Edition__c> getDirEdByDirPubCodeAndEdCode(String pubCode, String editionCode) {
        return [SELECT ID, Directory__c, Directory__r.Name, Directory__r.Directory_Code__c, Name FROM Directory_Edition__c  
                WHERE Directory__r.Publication_Company_Code__c = : pubCode AND Edition_Code__c = : editionCode];
    }
    
    public static list<Directory_Edition__c> getDEByIds(set<Id> DEIds) {
        return [SELECT ID, NAME, Directory__r.Publication_Company__c, Directory__c, Directory_Code__c, Edition_Code__c, Year__c, Book_Status__c, Directory__r.Name 
               FROM Directory_Edition__c WHERE Id IN:DEIds];
    }
    
    public static list<Directory_Edition__c> getDirEditionByLSACodeDirCode(set<String> setLSACode, set<String> setDirCode){
        return [SELECT Id, LSA_Directory_Version__c, Directory_Code__c FROM Directory_Edition__c WHERE LSA_Directory_Version__c IN :setLSACode AND 
                Directory_Code__c IN :setDirCode];
    }
    
    public static map<Id, Directory_Edition__c> getDEMapByIds(set<Id> DEIds) {
        return new map<Id, Directory_Edition__c>([SELECT ID, Directory__r.Publication_Company_Code__c FROM Directory_Edition__c WHERE Id IN:DEIds]);
    }    
}