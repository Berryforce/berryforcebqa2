public class NationalBillingStatusHandler {

	public static void onBeforeInsert(List<National_Billing_Status__c> listNBS) {
		populateSendRem(listNBS);
	}
	
	public static void onBeforeInsert(List<National_Billing_Status__c> listNBS, Map<Id, National_Billing_Status__c> oldMap) {
		populateSendRem(listNBS);
	}
	
	private static void populateSendRem(List<National_Billing_Status__c> listNBS) {
		for(National_Billing_Status__c NBS : listNBS) {
			if(NBS.Remittance_Transfer_Completed_Date__c != null) {
				NBS.RSAP_Send_Remittance_Started__c = true;
			} else {
				NBS.RSAP_Send_Remittance_Started__c = false;
			}
		}
	}
}