@isTest(SeeAllData=True)
public class NationalStagingOrderSetSOQLMethodsTest{
@IsTest static void NationalStagingOrderSetSOQLMethodsTest() {
        Directory__c dir = TestMethodsUtility.generateDirectory();
        dir.Months_for_National__c = 12;
        insert dir;
        Set<String> stringTransUniqueId = new Set<String>();
        Set<Id> setNSOSId = new Set<Id>();
        Set<String> setCMRNumbers = new Set<String>();
        Set<String> setClientNumbers = new Set<String>();
        stringTransUniqueId.add('1234567890123456789012345678901234567890');
        National_Staging_Order_Set__c newNSOS = TestMethodsUtility.generateNationalStagingOrderSet('Elite RT');
        //newNSOS.PreviousTransactionUniqueID__c = '1234567890123456789012345678901234567890';
        newNSOS.Directory__c = dir.Id;
        insert newNSOS;
        National_Staging_Line_Item__c newNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(newNSOS);
        newNSLI.Ready_for_Processing__c = true;
        insert newNSLI;
        setNSOSId.add(newNSOS.Id);
        setCMRNumbers.add(newNSOS.CMR_Number__c);
        setClientNumbers.add(newNSOS.Client_Number__c);
        //System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNationalStagingOrderSetListBytransUniqueIds(stringTransUniqueId));
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNationalStagingOrderSetById(newNSOS.Id));
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getSortedNationalStagingOSWithLineItem());
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNationalStagingStandingOrders(newNSOS.Id));
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNationalStagingOrderSetWithOpportunity());
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNationalStagingOrderSetWithOpportunity(setNSOSId));
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNationalReferenceRecordByNationalOrderId(setNSOSId));
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNSOSByCMRAndClientNumberForTransI(setCMRNumbers,setClientNumbers));
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNSOSByDirIdPubCodeEditionCodeWithoutActionO(String.valueof(newNSOS.Publication_Company_Code_c__c),String.valueof(newNSOS.Directory_Edition__r.Edition_Code__c),newNSOS.Directory__r.Id));
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNSLITXNHStandingOrders(newNSOS.Id,'N'));
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNSLITXNHStandingOrders(newNSOS.Id,'I'));
        System.assertNotEquals(null, NationalStagingOrderSetSOQLMethods.getNationalStagingLITXNHStandingOrders(newNSOS.Id));
        
    }
    }