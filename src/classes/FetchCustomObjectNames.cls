public class FetchCustomObjectNames {
    public list<wrapper> lsyWrapper {get;set;}
    public void onload() {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
		lsyWrapper = new list<wrapper>();
        for (String objectName : gd.keySet()){
            Schema.SObjectType showObjectName = gd.get(objectName);
            if(showObjectName.getDescribe().isCustom()) {
                String  APIObjectName   =   showObjectName.getDescribe().getLocalName();
                String  APIObjectName1 = showObjectName.getDescribe().getName();
                String  APIObjectName2 = showObjectName.getDescribe().getLabel();
                system.debug(APIObjectName + ' --> ' + APIObjectName1 +' --> '+APIObjectName2);
                lsyWrapper.add(new wrapper(APIObjectName2, APIObjectName));
            }
        }
    }
    
    public class wrapper {
        public String strName {get;set;}
        public String API {get;set;}
        public wrapper(String strName, String API) {
            this.API = API;
            this.strName = strName;
        }
    }
}