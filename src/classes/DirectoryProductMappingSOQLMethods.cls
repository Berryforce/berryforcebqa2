public with sharing class DirectoryProductMappingSOQLMethods {
    public static List<Directory_Product_Mapping__c> getDirProdMapByProdId(set<Id> prodIds){
        return [SELECT Id, Directory__c, Product2__c FROM Directory_Product_Mapping__c WHERE Product2__c IN : prodIds];
    }
    
    public static List<Directory_Product_Mapping__c> getDirProdMapByDirCode(String dirCode){
        return [SELECT Id, Quantity_Remaining__c, Quantity__c, Product2__r.Name, Product2__r.ProductCode 
                FROM Directory_Product_Mapping__c WHERE
                Requires_Inventory_Tracking__c = TRUE AND Inventory_Tracking_Group__c != 'YP Leader Ad' AND Directory__r.Directory_Code__c =: dirCode];
    }
    
    public static List<Directory_Product_Mapping__c> getDirProdMapByProdIdAndDirId(set<Id> prodIds, set<Id> dirIds){
        return [SELECT Id, Directory__c, Product2__c, Quantity_Remaining__c,FullRate__c, 
                Foreign_EAS_WP_Ban_Bill_Remaining__c, Requires_Inventory_Tracking__c, strProductInventoryCombo__c
                FROM Directory_Product_Mapping__c WHERE Directory__c IN : dirIds AND Product2__c IN : prodIds];
    }
    
    public static List<Directory_Product_Mapping__c> getDirProdMapByComboValue(set<String> strDIRPROD2){
        return [SELECT Id, Directory__c, Product2__c, Quantity_Remaining__c,FullRate__c, DPM_Dir_Prod_External_ID__c, 
                Foreign_EAS_WP_Ban_Bill_Remaining__c, Requires_Inventory_Tracking__c, strProductInventoryCombo__c
                FROM Directory_Product_Mapping__c WHERE DPM_Dir_Prod_External_ID__c IN: strDIRPROD2];
    }
    
    public static List<Directory_Product_Mapping__c> getDirProdMapByUdacAndDirId(set<string> setStrUdac, set<Id> setDirIds){
        return [SELECT Id, Directory__c, Product2__c, Product_Code_UDAC__c
                FROM Directory_Product_Mapping__c WHERE Directory__c IN : setDirIds AND Product_Code_UDAC__c IN : setStrUdac];
    }
    
    public static list<Directory_Product_Mapping__c> fetchDPMByDirectoryId(set<Id> setDirID) {
        return [Select Product2__r.LOY__c, Product2__r.AllowX70Discount__c, Product2__r.AllowX65Discount__c, Product2__r.AllowX60Discount__c, 
        Product2__r.AllowX55Discount__c, Product2__r.AllowX50Discount__c, Product2__r.AllowX45Discount__c, Product2__r.AllowX40Discount__c, 
        Product2__r.AllowX35Discount__c, Product2__r.AllowX30Discount__c, Product2__r.AllowX25Discount__c, Product2__r.AllowX15Discount__c, 
        Product2__r.IsActive, Product2__r.ProductCode, Product2__r.Name, Product2__c, Name, Id, National_Full_Rate__c, FullRate__c, 
        Directory__r.Publication_Company_Code__c, Directory__r.Publication_Company__c, Directory__r.Publication_Company_Name__c, 
        Directory__r.Directory_Code__c, Directory__r.Name, Directory__c From Directory_Product_Mapping__c 
        where Directory__c IN:setDirID order by Product2__r.ProductCode, Directory__c];
    }
    
    public static map<id,Directory_Product_Mapping__c> getDirProdMapByDirIdAndProducts(set<id> setProduct, Id idDir){
        return new map<id,Directory_Product_Mapping__c>([select id,Product2__c from Directory_Product_Mapping__c where Directory__c = :idDir and Product2__c in :setProduct]);
    }
}