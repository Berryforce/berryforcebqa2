public with sharing class PrintLOBReport_v1 {
public String[] selectedPartners{get;set;}
public String datename{get; set;}        
public String startdatename{get; set;}
public String enddatename{get; set;}
transient public List<Order_Line_Items__c> OrderLineItem{get;set;}
public string emailaddr{get;set;}
public boolean showexport{get;set;}
public boolean showback{get;set;}
public String content{get;set;}
public boolean exporthide {get;set;}
transient public List<FinalWrapper> finalList{get;set;}
transient public List<BillSummaryWrapper> display_list_1{get;set;}
transient public List<UDACWrapper>  display_list_2{get;set;}
public boolean ShowReport{get;set;}
public boolean ShowError{get;set;}
public String DirectoryID{get;set;}
public String DirectoryName{get;set;}
public String DirectoryCode{get;set;}
public String TelcoName{get;set;}
transient public List<Order_Line_Items__c> OLIs{get;set;}
transient public List<c2g__codaInvoiceLineItem__c> ListILI{get;set;}
public Double OrderTotal{get;set;}
public Decimal MonthlyOrderTotal{get;set;}
public Integer InvoiceTotal{get;set;}
public Decimal UDACMonthlyOrderTotal{get;set;}
public Decimal UDACOrderTotal{get;set;}
public Integer UDACItemCount{get;set;}
public Date PublicationDate{get;set;}
public String contenttype{get;set;}
public String DirectoryEdition{get;set;}
transient public map<string,BillSummaryWrapper>BillingsummeryMap{get;set;}
transient public Map<Id,List<Order_Line_Items__c>> MapAccountOLI;
transient public Map<id,Map<String,Map<String,Map<String,Map<String,List<Order_Line_Items__c>>>>>> MapAccBPBFOLI;
public PrintLOBReport_v1(){
    BillingsummeryMap=new map<string,BillSummaryWrapper>();
    
    if(Apexpages.currentpage().getparameters().get('BillingPartner')!=null){
        selectedPartners=String.valueof(Apexpages.currentpage().getparameters().get('BillingPartner')).split(',');
    }
    DirectoryID=Apexpages.currentpage().getparameters().get('directoryId');
    DirectoryEdition=Apexpages.currentpage().getparameters().get('Edition');
    showreport=true;
    if(DirectoryID!=null && DirectoryEdition!=null)
    {
        if(selectedPartners!=null)
        {
           doGenerateCSV_email(selectedPartners,DirectoryID,DirectoryEdition);
        }
        else
        {
          doGenerateCSV_email(null,DirectoryID,DirectoryEdition);
        }
    }
    else
    {
        ShowError=true;
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: No Record Found. Kindly go back & refine your search filter');
        ApexPages.addMessage(myMsg);
    }
}

public pageReference  ExportReport(){
  if(Apexpages.currentpage().getparameters().get('BillingPartner')!=null){
        selectedPartners=String.valueof(Apexpages.currentpage().getparameters().get('BillingPartner')).split(',');
    }
    DirectoryID=Apexpages.currentpage().getparameters().get('directoryId');
    DirectoryEdition=Apexpages.currentpage().getparameters().get('Edition');
    showreport=true;
    if(DirectoryID!=null && DirectoryEdition!=null)
    {
        if(selectedPartners!=null)
        {
           doGenerateCSV_email(selectedPartners,DirectoryID,DirectoryEdition);
        }
        else
        {
          doGenerateCSV_email(null,DirectoryID,DirectoryEdition);
        }
    }
  PageReference pg=new PageReference('/apex/PrintLOBReportExport');
  return pg;
}
public void doGenerateCSV_email(String[] selectedPartners,String Direcotryid,String DirectoryEdition)
{
//Map<Id,List<Order_Line_Items__c>> 
MapAccountOLI=new Map<Id,List<Order_Line_Items__c>>();
List<Order_Line_Items__c> ListOLI=new List<Order_Line_Items__c>();
finalList=new List<FinalWrapper>();
display_list_1=new List<BillSummaryWrapper>();
display_list_2=new List<UDACWrapper>();
Set<ID> DirectoryIDs=new Set<ID>();
Set<Id> OLIIDS=new Set<ID>();
Set<ID> AccountIds=new Set<ID>();
BillingsummeryMap=new map<string,BillSummaryWrapper>();
    
RecordType  objRT = CommonMethods.getRecordTypeDetailsByName('Order_Line_Items__c','National Order Line Item');
List<Directory_Edition__c> dirMap=[Select id,Telco__r.name,Pub_Date__c,Directory_Code__c,Directory__c,Directory__r.Name from Directory_Edition__c where id=:DirectoryEdition];
if(dirMap.size()>0)
{
    DirectoryCode=dirMap[0].Directory_Code__c;
    DirectoryName=dirMap[0].Directory__r.Name;
    PublicationDate=dirMap[0].Pub_Date__c;
}

if(selectedPartners!=null)
{
    if(selectedPartners.size()>0)
    {
       
        ListOLI=[Select Account__r.Primary_Contact__r.Phone,Listing__r.Phone__c, Account__r.X3l_External_ID__c, Account__r.Parent_3L_External_ID__c,id,Digital_Product_Requirement__r.Advertised_Phone_Number__c,UnitPrice__c,Billing_Contact__r.Phone,Account__c,Account__r.Account_Number__c,Account__r.Parent.Account_Number__c,Account__r.Phone,Account__r.Name,Telco__r.Name,Listing_Name__c,Listing__r.Name,Directory__c,Billing_Partner__c,Billing_Frequency__c,RecordtypeId,Order_Line_Total__c,Account__r.Parent.Name,(Select id,Order_Line_Item__r.UnitPrice__c,c2g__NetValue__c from Sales_Invoice_Line_Items__r) from Order_Line_Items__c where RecordtypeId !=: objRT.Id AND isCanceled__c=false AND Product2__r.Media_Type__c='Print' AND Billing_Partner__c in :selectedPartners AND Directory__c=: Direcotryid AND Directory_Edition__c=:DirectoryEdition AND Billing_Partner__c != null Order By Billing_Partner__c,Account__r.Name];
    }
    else
    {
        ListOLI=[Select Account__r.Primary_Contact__r.Phone,Listing__r.Phone__c, Account__r.X3l_External_ID__c, Account__r.Parent_3L_External_ID__c, id,Digital_Product_Requirement__r.Advertised_Phone_Number__c,UnitPrice__c,Billing_Contact__r.Phone,Account__c,Account__r.Account_Number__c,Account__r.Parent.Account_Number__c,Account__r.Phone,Account__r.Name,Telco__r.Name,Listing_Name__c,Listing__r.Name,Directory__c,Billing_Partner__c,Billing_Frequency__c,RecordtypeId,Order_Line_Total__c,Account__r.Parent.Name,(Select id,Order_Line_Item__r.UnitPrice__c,c2g__NetValue__c from Sales_Invoice_Line_Items__r) from Order_Line_Items__c where RecordtypeId !=: objRT.Id AND isCanceled__c=false AND Product2__r.Media_Type__c='Print' AND Directory__c=: Direcotryid AND Directory_Edition__c=:DirectoryEdition AND Billing_Partner__c != null Order By Billing_Partner__c,Account__r.Name];
    }
}
else
{
    ListOLI=[Select Account__r.Primary_Contact__r.Phone,Listing__r.Phone__c, Account__r.X3l_External_ID__c, Account__r.Parent_3L_External_ID__c, id,Digital_Product_Requirement__r.Advertised_Phone_Number__c,UnitPrice__c,Billing_Contact__r.Phone,Account__c,Account__r.Account_Number__c,Account__r.Name,Account__r.Parent.Account_Number__c,Account__r.Phone,Telco__r.Name,Listing_Name__c,Listing__r.Name,Directory__c,Billing_Partner__c,Billing_Frequency__c,RecordtypeId,Order_Line_Total__c,Account__r.Parent.Name,(Select id,Order_Line_Item__r.UnitPrice__c,c2g__NetValue__c from Sales_Invoice_Line_Items__r) from Order_Line_Items__c where RecordtypeId !=: objRT.Id AND isCanceled__c=false AND Product2__r.Media_Type__c='Print' AND Directory__c=: Direcotryid AND Directory_Edition__c=:DirectoryEdition AND Billing_Partner__c != null Order By Billing_Partner__c,Account__r.Name];
}
//Map<id,Map<String,Map<String,Map<String,List<Order_Line_Items__c>>>>> 
MapAccBPBFOLI = new Map<id,Map<String,Map<String,Map<String,Map<String,List<Order_Line_Items__c>>>>>>();
for(Order_Line_Items__c  OLIIterator: ListOLI)
{
    DirectoryIDs.add(OLIIterator.Directory__c);  
    OLIIDS.add(OLIIterator.id);
    AccountIds.add(OLIIterator.Account__c);
    String AdvPhone=OLIIterator.Digital_Product_Requirement__r.Advertised_Phone_Number__c;
    String ListingName=String.valueof(OLIIterator.Listing__r.Name+OLIIterator.Listing__r.Phone__c).replaceAll('\\s+',''); 
    
    if(MapAccBPBFOLI.containskey(OLIIterator.Account__c))
    {
         if(MapAccBPBFOLI.get(OLIIterator.Account__c).containskey(OLIIterator.Billing_Partner__c))
         {
            if(MapAccBPBFOLI.get(OLIIterator.Account__c).get(OLIIterator.Billing_Partner__c).containskey(OLIIterator.Billing_Frequency__c))
            {
                 if(MapAccBPBFOLI.get(OLIIterator.Account__c).get(OLIIterator.Billing_Partner__c).get(OLIIterator.Billing_Frequency__c).containskey(AdvPhone))
                 {
                      if(MapAccBPBFOLI.get(OLIIterator.Account__c).get(OLIIterator.Billing_Partner__c).get(OLIIterator.Billing_Frequency__c).get(AdvPhone).containskey(ListingName)){
                        MapAccBPBFOLI.get(OLIIterator.Account__c).get(OLIIterator.Billing_Partner__c).get(OLIIterator.Billing_Frequency__c).get(AdvPhone).get(ListingName).add(OLIIterator);
                      }
                      else
                      {
                        MapAccBPBFOLI.get(OLIIterator.Account__c).get(OLIIterator.Billing_Partner__c).get(OLIIterator.Billing_Frequency__c).get(AdvPhone).put(ListingName,new List<Order_Line_Items__c>{OLIIterator});
                      }
                      
                 }
                 else
                 {
                      Map<String,List<Order_Line_Items__c>> tempListingOLIMap=new Map<String,List<Order_Line_Items__c>> {ListingName=>new list<Order_Line_Items__c>{OLIIterator}};
                      MapAccBPBFOLI.get(OLIIterator.Account__c).get(OLIIterator.Billing_Partner__c).get(OLIIterator.Billing_Frequency__c).put(AdvPhone,tempListingOLIMap);
                 }
                
                // MapAccBPBFOLI.get(OLIIterator.Account__c).get(OLIIterator.Billing_Partner__c).get(OLIIterator.Billing_Frequency__c).add(OLIIterator);
            }
            else
            {
                 Map<String,List<Order_Line_Items__c>> tempListingOLIMap=new Map<String,List<Order_Line_Items__c>> {ListingName=>new list<Order_Line_Items__c>{OLIIterator}};
                 Map<String,Map<String,List<Order_Line_Items__c>>> tempPhoneOLIMap=new Map<String,Map<String,List<Order_Line_Items__c>>>{AdvPhone=>tempListingOLIMap};
                 MapAccBPBFOLI.get(OLIIterator.Account__c).get(OLIIterator.Billing_Partner__c).put(OLIIterator.Billing_Frequency__c,tempPhoneOLIMap);
            }
         }
         else
         {
           Map<String,List<Order_Line_Items__c>> tempListingOLIMap=new Map<String,List<Order_Line_Items__c>> {ListingName=>new list<Order_Line_Items__c>{OLIIterator}};
           Map<String,Map<String,List<Order_Line_Items__c>>> tempPOLIMap=new Map<String,Map<String,List<Order_Line_Items__c>>>{AdvPhone=>tempListingOLIMap};
           Map<string,Map<String,Map<String,List<Order_Line_Items__c>>>> tempOLIMap=new Map<string,Map<String,Map<String,List<Order_Line_Items__c>>>>{OLIIterator.Billing_Frequency__c=>tempPOLIMap};
           MapAccBPBFOLI.get(OLIIterator.Account__c).put(OLIIterator.Billing_Partner__c,tempOLIMap);
         }
    }
    else
    {
      Map<String,List<Order_Line_Items__c>> tempListingOLIMap=new Map<String,List<Order_Line_Items__c>> {ListingName=>new list<Order_Line_Items__c>{OLIIterator}};
      Map<String,Map<String,List<Order_Line_Items__c>>> tempPOLIMap=new Map<String,Map<String,List<Order_Line_Items__c>>>{AdvPhone=>tempListingOLIMap};
      Map<string,Map<String,Map<String,List<Order_Line_Items__c>>>> tempOLIMap=new Map<string,Map<String,Map<String,List<Order_Line_Items__c>>>>{OLIIterator.Billing_Frequency__c=>tempPOLIMap};
      Map<String, Map<String,Map<String,Map<String,List<Order_Line_Items__c>>>>> OLIMapList=new Map<String, Map<String,Map<String,Map<String,List<Order_Line_Items__c>>>>>();
      OLIMapList.put(OLIIterator.Billing_Partner__c,tempOLIMap);
      MapAccBPBFOLI.put(OLIIterator.Account__c,OLIMapList);
    }
    
}

if(MapAccBPBFOLI.size()>0)
{
   for(Id AccIterator : MapAccBPBFOLI.keyset())
   {
       for(String BPIterator : MapAccBPBFOLI.get(AccIterator).keyset())
       {
          for(String BFIterator : MapAccBPBFOLI.get(AccIterator).get(BPIterator).keyset())
          {
             for(String PhoneIterator : MapAccBPBFOLI.get(AccIterator).get(BPIterator).get(BFIterator).keyset())
             {
                 for(String ListingNameIterator : MapAccBPBFOLI.get(AccIterator).get(BPIterator).get(BFIterator).get(PhoneIterator).keyset())
                 {
                     FinalWrapper FW=new FinalWrapper();
                     Double MonthlyDirectoryTotal=0.0;
                     for(Order_Line_Items__c OliIterator : MapAccBPBFOLI.get(AccIterator).get(BPIterator).get(BFIterator).get(PhoneIterator).get(ListingNameIterator))
                     {
                            MonthlyDirectoryTotal =MonthlyDirectoryTotal+OliIterator.UnitPrice__c;
                            FW.InvoiceRecipientName=OliIterator.Account__r.Name;
                            FW.BTN=OliIterator.Account__r.Primary_Contact__r.Phone;
                            FW.EnterpriseId=OliIterator.Account__r.Account_Number__c;
                            FW.PI ='';
                            FW.BillingFrequency=BFIterator;
                            FW.BillingPartner=BPIterator;
                            FW.ListedPhone=OliIterator.Listing__r.Phone__c;
                            FW.BookLength='12';
                            FW.BilledBy=OliIterator.Billing_Partner__c;
                            FW.Operator='';
                            FW.ListingName=OliIterator.Listing__r.Name;
                            FW.ParentAccountName=OliIterator.Account__r.Parent.Name;
                            FW.ParentAccountNum=OliIterator.Account__r.Parent.Account_Number__c;
                            FW.X3lExternal = OliIterator.Account__r.X3l_External_ID__c;
                            FW.Parent2lExternal = OliIterator.Account__r.Parent_3L_External_ID__c;
                     }
                     FW.MonthlyDirectoryTotal = MonthlyDirectoryTotal;
                      if(FW.MonthlyDirectoryTotal>0) //for omitting 0 rollup
                        finallist.add(FW);
                  }
           }  
            
           
             // finallist.add(new FinalWrapper(CustAccName,RecortTypeName,InvoieLI.Order_Line_Item__r.Billing_Contact__r.Phone,InvoieLI.Order_Line_Item__r.Account__r.Account_Number__c,TotalMonthlyAmount,null,InvoieLI.Order_Line_Item__r.Billing_Frequency__c,InvoieLI.Order_Line_Item__r.Billing_Partner__c,InvoieLI.c2g__Invoice__r.c2g__Account__r.Phone,'12',InvoieLI.Order_Line_Item__r.Billing_Partner__c,'null',InvoieLI.Order_Line_Item__r.Listing__r.Name,CustAccName,String.valueof(InvoieLI.Order_Line_Item__r.Account__r.Parent.Name),String.valueof(InvoieLI.c2g__Invoice__r.Transaction_Type__c)));
            
          }
       }
       
   }
}
finallist.sort();
if(finallist.size()>0)
{
  showback=true;
ShowReport=true;
ShowError=false;
  map<String,Double> mapBPAmount=new map<String,Double>();
  for(AggregateResult agr:[SELECT Billing_Partner__c BillingPartner,SUM(UnitPrice__c) MonthlyTotal from Order_Line_Items__c where Id in :ListOLI Group By Billing_Partner__c])
  {
     mapBPAmount.put(String.Valueof(agr.get('BillingPartner')),Double.Valueof(agr.get('MonthlyTotal')));
  }
  
  for(FinalWrapper frm: finallist)
  {
      if(!BillingsummeryMap.containskey(frm.BillingPartner))
      {
          BillingsummeryMap.put(frm.BillingPartner,new BillSummaryWrapper(frm.BillingPartner,mapBPAmount.get(frm.BillingPartner),'',1));
      }
      else if(BillingsummeryMap.containskey(frm.BillingPartner))
      {
            BillingsummeryMap.get(frm.BillingPartner).CountInvoice=BillingsummeryMap.get(frm.BillingPartner).CountInvoice+1;
      }
      
  }
  if(BillingsummeryMap.size()>0)
  {
       MonthlyOrderTotal=0.00;
       InvoiceTotal=0;
      for(BillSummaryWrapper bswrp: BillingsummeryMap.values())
      {
          if(bswrp.MonthlyBilling != null) {
            MonthlyOrderTotal=MonthlyOrderTotal+Decimal.valueof(bswrp.MonthlyBilling);
          }
          InvoiceTotal=InvoiceTotal+bswrp.CountInvoice;
      }
  }
}
else
{
showback=true;
ShowReport=false;
ShowError=true;
ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: No Record Found. Kindly go back & refine your search filter');
ApexPages.addMessage(myMsg);
}

if(OLIIDS.size()>0)
{
// For UDAC Summary

for(AggregateResult agr1:[SELECT Billing_Partner__c BillingPartner,Recordtype.Name RecordType,Product2__r.ProductCode UDAC,SUM(Order_Line_Total__c) OrderTotal,SUM(UnitPrice__c) MonthlyPayment,Count(ID) TotalItems FROM Order_Line_Items__c  where id in :ListOLI Group By Recordtype.Name,Product2__r.ProductCode,Billing_Partner__c])
{
      Double TotalMonthlyAmount=0.0;
      Double TotalAmount=0.0;
      String BillingPartner=null;
      String UDAC=null;
      Integer ItemCount=0;
      if(agr1.get('MonthlyPayment')!=null)
      {
      TotalMonthlyAmount=Double.valueof(agr1.get('MonthlyPayment'));
      }
      if(agr1.get('BillingPartner') != null)
      {
      BillingPartner=String.valueof(agr1.get('BillingPartner'));
      }
      if(agr1.get('UDAC') !=null)
      {
      UDAC=String.valueof(agr1.get('UDAC'));
      }
      if(agr1.get('TotalItems')!=null)
      {
      ItemCount=Integer.valueof(agr1.get('TotalItems'));
      }
      if(agr1.get('RecordType') != null)
      {
         if(agr1.get('RecordType') == 'National Order Line Item')
         {
          BillingPartner='National Billed';
         }
         else
         {
         BillingPartner=String.Valueof(agr1.get('BillingPartner'));
         }
      }
      if(TotalMonthlyAmount>0)
      display_list_2.add(new UDACWrapper(BillingPartner,UDAC,TotalMonthlyAmount,TotalAmount,ItemCount));
}
}

if(display_list_2.size()>0)
{
   UDACOrderTotal=0.00;
   UDACMonthlyOrderTotal=0.00;
   UDACItemCount=0;
    for(UDACWrapper u:display_list_2)
    {
      UDACOrderTotal=UDACOrderTotal+Decimal.valueof(u.TotalBilling);
      UDACMonthlyOrderTotal=UDACMonthlyOrderTotal+Decimal.valueof(u.MonthlyBilling);
      UDACItemCount=UDACItemCount+u.ItemCount;
    }
}
else
{
ShowReport=false;
ShowError=true;
showback=true;
ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: No Record Found. Kindly go back & refine your search filter');
ApexPages.addMessage(myMsg);
}
}

public PageReference cancel()
{
 PageReference pg=new PageReference('/apex/TelcoLOBReport_v1');
 pg.setredirect(true);
 return pg;
 
}
public class BillSummaryWrapper
{
        
        public String  Billing_Partner{get; set;} 
        public Decimal MBilling{get;set;}
        public Double MonthlyBilling{get;set;}
        public String PI{get;set;}
        public Integer CountInvoice{get;set;}
        public BillSummaryWrapper(String  Billing_Partner,Double MonthlyBilling,String PI,Integer CountInvoice) 
        {
           
           this.Billing_Partner=Billing_Partner;
           this.MonthlyBilling=MonthlyBilling;
           if(MonthlyBilling == null) {
                this.MBilling = 0.00;            
           } else {
                this.MBilling = Decimal.valueof(MonthlyBilling).setscale(2);
           }
           this.PI=PI;
           this.CountInvoice=CountInvoice;
        }
}

public class UDACWrapper{
        
        public String  Billing_Partner{get; set;} 
        public String UDAC{get; set;} 
        public Double MonthlyBilling{get;set;}
        public Double TotalBilling{get;set;}
        public Integer ItemCount {get;set;}
        public Decimal TBilling{get;set;}
        public Decimal MBilling{get;set;}
        public UDACWrapper(String  Billing_Partner,String UDAC,Double MonthlyBilling,Double TotalBilling,Integer ItemCount) 
        {
           
           this.Billing_Partner=Billing_Partner;
           this.TotalBilling=TotalBilling;
           this.TBilling=Decimal.valueof(TotalBilling).setscale(2);
           
         //  this.TotalBilling=ItemCount*MonthlyBilling;
           this.MonthlyBilling=MonthlyBilling;
           this.MBilling=Decimal.valueof(MonthlyBilling).setscale(2);
           
           this.UDAC=UDAC;
           this.ItemCount=ItemCount;
          
        }
    }
      
   
public class FinalWrapper implements Comparable{
        public String InvoiceRecipientName{get; set;}
        public String BTN{get; set;} 
        public Double MonthlyDirectoryTotal{get; set;}
        public Decimal MDTotal{get;set;}
        public String PI{get; set;}  
        public String BillingFrequency{get; set;} 
        public String BillingPartner{get;set;}
        public String ListedPhone{get; set;} 
        public String BookLength{get;set;}
        public String BilledBy{get;set;} 
        public String Operator{get;set;} 
        public String ListingName{get;set;} 
        public String EnterpriseId {get;set;}
        public String ParentAccountName{get;set;}
        public String ParentAccountNum{get;set;}
        public String X3lExternal {get;set;}
        public String Parent2lExternal {get;set;}
        public FinalWrapper(){}
        public FinalWrapper(String InvoiceRecipientName,String BTN,String EnterpriseId,Double MonthlyDirectoryTotal,String PI,String BillingFrequency,String BillingPartner,String ListedPhone,String BookLength,String BilledBy,String Operator,String ListingName,String ParentAccountName,String ParentAccountNum)
        {
            this.X3lExternal = X3lExternal;
            this.Parent2lExternal = Parent2lExternal;
            this.InvoiceRecipientName=InvoiceRecipientName;
            this.BTN=BTN;
            this.EnterpriseId =EnterpriseId ;
            this.MonthlyDirectoryTotal=MonthlyDirectoryTotal;
            this.MDTotal=Decimal.valueof(MonthlyDirectoryTotal).setscale(2);
            this.PI=PI;  
            this.BillingFrequency=BillingFrequency;
            this.BillingPartner=BillingPartner;
            this.ListedPhone=ListedPhone;
            this.BookLength=BookLength;
            this.BilledBy=BilledBy;
            this.Operator=Operator;
            this.ListingName=ListingName;
            this.ParentAccountName=ParentAccountName;
            this.ParentAccountNum=ParentAccountNum;
         }
         public Integer compareTo(Object ObjToCompare) {
           
                FinalWrapper jobsWrapper = (FinalWrapper)ObjToCompare;
                if (BillingPartner == jobsWrapper.BillingPartner) {
                        if (InvoiceRecipientName == jobsWrapper.InvoiceRecipientName) return 0;
                        else if (InvoiceRecipientName > jobsWrapper.InvoiceRecipientName) return 1;
                        else
                        return -1;
                }
                if (BillingPartner > jobsWrapper.BillingPartner){
                    return 1;
                }
                return -1; 
         }    
}
}