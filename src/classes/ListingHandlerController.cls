public with sharing class ListingHandlerController {
    public static boolean bflag=true;
    
    public static void onBeforeInsert(list<listing__c> objLst){
      AdditonalListingLeadAccountPopulate(objLst,null);
    }
    public static void onBeforeUpdate(list<listing__c> objLst,map<ID, listing__c> mapOldLst) {
      if(!system.isBatch()) {
            ListingDisconnectValidation(objLst,mapOldLst);
       }
       AdditonalListingLeadAccountPopulate(objLst,mapOldLst); 
       
     }
    
    public static void onAfterInsert(list<listing__c> objLst) {
        if(!system.isBatch() && !test.isrunningtest()) {
            ListingafterInsertUpdateLogic(objLst,null,null);
        } 
    }
    
    public static void onAfterUpdate(list<listing__c> objLst, map<ID, listing__c> mapOldLst, map<Id,listing__c> mapNewLst) {
        //if(!system.isBatch()){
            if(!CommonVariables.LeadListingAdrRecursive) {
                ListingafterInsertUpdateLogic(objLst,mapOldLst,mapNewLst);
            } 
        //}
    }
    
    public static void ListingDisconnectValidation(list<listing__c> lstList,map<id,listing__c> lstOldmap){
       Set<Id> setLstId = new Set<Id>();
       map<Id,listing__c> mapObjLst = new map<Id,Listing__c>();
       map<Id,list<Directory_listing__c>> mapObjDLst = new map<Id,list<Directory_listing__c>>();
       list<directory_listing__c> lstDir = new list<directory_listing__c>();
       set<Id> setSLCHId = new set<Id>();
       set<Id> setDLDisconId = new set<Id>();
       
       for(Listing__c lis : lstList) {
            Listing__c OldListing = lstOldmap.get(lis.Id);
            if(lis.Disconnected__c != OldListing.Disconnected__c && lis.Disconnected__c == true ) {
                setLstId.add(lis.Id);
            }
            if(lis.Disconnected__c != OldListing.Disconnected__c && lis.Disconnected__c == false ) {
                lis.Disconnect_Reason__c = null;
            }
        }
        if(setLstId.size()>0){
            mapObjLst = ListingSOQLMethods.fetchListing(setLstId);
        }
        if(mapObjLst.size()>0){
           for(Listing__c objList : lstList){
               if(mapObjLst.get(objList.Id) != null){
                   for(directory_listing__c objDirList : mapObjLst.get(objList.Id).Reference_Listings__r){
                        if(objDirList.Caption_Header__c == true) {
                            setSLCHId.add(objDirList.Id);
                        }
                   }
               }
           }
        }
        if(setSLCHId.size()> 0){
            lstDir = DirectoryListingSOQLMethods.getDlsbyUndercaption(setSLCHId);
        }
        if(lstDir.size()>0){
            for(directory_listing__c objDirLst : lstDir){
                if(!mapObjDLst.containskey(objDirLst.Under_caption__c)){
                    mapObjDLst.put(objDirLst.Under_caption__c, new list<directory_listing__c>());
                }
                mapObjDLst.get(objDirLst.Under_caption__c).add(objDirLst);
            }
        }
        if(mapObjLst.size()>0){
           for(Listing__c objLst : lstList){
               if(mapObjLst.get(objLst.Id) != null){
                   for(directory_listing__c objDirLst : mapObjLst.get(objLst.Id).Reference_Listings__r){
                        if(objDirLst.Caption_header__c == true){
                            if(mapObjDLst.get(objDirLst.Id) != null){
                                for(directory_listing__c objDirLstChild : mapObjDLst.get(objDirLst.Id)){
                                    if(objDirLstChild.Disconnected__c == false){
                                        setDLDisconId.add(objLst.Id);
                                    }
                                }
                            }
                        }
                        else{
                             if(objDirLst.Caption_member__c == true && objDirLst.Disconnected__c == false){
                                setDLDisconId.add(objLst.Id);
                             }
                        }
                        if(setDLDisconId.contains(objLst.Id)){
                           objLst.addError('You must establish a new Caption Header record, and link the Caption Members to the new Caption Header using the Under Caption field.');
                       }
                   }
               }
           }
        }
    }
    
    public static void ListingafterInsertUpdateLogic(list<listing__c> objLst, map<ID, listing__c> mapOldLst,map<Id,listing__c> mapNewLst) {
       
        set<Id> setLstId = new set<Id>();
        set<Id> setDFFLstId = new set<Id>();
        map<Id,Lead> mapobjLdLst = new map<Id,Lead>();
        map<Id,Account> mapobjActLst = new map<Id,Account>();
        map<String,Listing__c> mapactldLst = new map<String,Listing__c>();
        map<Id,list<listing__c>> mapListing = new map<Id,list<listing__c>>();
        //map<string,listing__c> mapLeadExist = new map<string,listing__c>();
        Boolean skipTrigger=CommonMethods.skipTriggerLogic(CommonMessages.dffObjectName);
        system.debug('Skip boolen'+skipTrigger);
        Boolean skipSLDFFSync = false;
        Boolean bflagLeadUpdate = false;
        for(listing__c iterator : objLst){
            if(mapOldLst != null) {
                if(bflag) {
                listing__c oldLst = mapOldLst.get(iterator.Id);
                if(iterator.Bus_Res_Gov_Indicator__c != oldLst.Bus_Res_Gov_Indicator__c  ||  iterator.Cross_Reference_Text__c != oldLst.Cross_Reference_Text__c || iterator.Normalized_Designation__c != oldLst.Normalized_Designation__c || iterator.Disconnect_Reason__c != oldLst.Disconnect_Reason__c || iterator.Disconnected__c != oldLst.Disconnected__c || iterator.Effective_Date__c != oldLst.Effective_Date__c ||
                    iterator.Honorary_Title__c != oldLst.Honorary_Title__c || (iterator.Lst_Last_Name_Business_Name__c != null && !iterator.Lst_Last_Name_Business_Name__c.equals(oldLst.Lst_Last_Name_Business_Name__c)) || iterator.Left_Telephone_Phrase__c != oldLst.Left_Telephone_Phrase__c || iterator.Lineage_Title__c != oldLst.Lineage_Title__c || (iterator.First_Name__c != null && !iterator.First_Name__c.equals(oldLst.First_Name__c)) || iterator.First_Name__c != oldLst.First_Name__c ||
                    iterator.Normalized_Last_Name_Business_Name__c != oldLst.Normalized_Last_Name_Business_Name__c || iterator.Normalized_First_Name__c != oldLst.Normalized_First_Name__c || iterator.Normalized_Listing_Street_Name__c != oldLst.Normalized_Listing_Street_Name__c || iterator.Listing_Type__c != oldLst.Listing_Type__c || iterator.Manual_Sort_As_Override__c != oldLst.Manual_Sort_As_Override__c || 
                    iterator.Normalized_Listing_Street_Number__c != oldLst.Normalized_Listing_Street_Number__c || iterator.Normalized_Phone__c != oldLst.Normalized_Phone__c || iterator.Omit_Address_OAD__c != oldLst.Omit_Address_OAD__c || iterator.Phone_Override__c != oldLst.Phone_Override__c || iterator.Phone_Type__c != oldLst.Phone_Type__c || iterator.Phone__c != oldLst.Phone__c || 
                    iterator.Service_Order__c != oldLst.Service_Order__c || iterator.Telco_Provider__c != oldLst.Telco_Provider__c || (iterator.Listing_street_number__c != null && !iterator.Listing_street_number__c.equals(oldLst.Listing_street_number__c)) || (iterator.Listing_Street__c != null && !iterator.Listing_Street__c.equals(oldLst.Listing_Street__c)) || (iterator.Listing_City__c != null && !iterator.Listing_City__c.equals(oldLst.Listing_City__c)) || (iterator.Listing_PO_Box__c != null && !iterator.Listing_PO_Box__c.equals(oldLst.Listing_PO_Box__c)) || (iterator.Listing_State__c != null && !iterator.Listing_State__c.equals(oldLst.Listing_State__c)) || 
                    iterator.Listing_Postal_Code__c != oldLst.Listing_Postal_Code__c || iterator.Listing_Country__c != oldLst.Listing_Country__c || iterator.Right_Telephone_Phrase__c != oldLst.Right_Telephone_Phrase__c || iterator.Designation__c != oldLst.Designation__c || iterator.Telco_Sort_Order__c != oldLst.Telco_Sort_Order__c || iterator.Normalized_Honorary_Title__c != oldLst.Normalized_Honorary_Title__c || 
                    iterator.Listing_street_number__c != oldLst.Listing_street_number__c || iterator.Listing_Street__c != oldLst.Listing_Street__c || iterator.Listing_PO_Box__c != oldLst.Listing_PO_Box__c || iterator.Listing_State__c != oldLst.Listing_State__c || iterator.Listing_City__c != oldLst.Listing_City__c || iterator.Lst_Last_Name_Business_Name__c != oldLst.Lst_Last_Name_Business_Name__c ||
                    iterator.Normalized_Lineage_Title__c != oldLst.Normalized_Lineage_Title__c || iterator.Normalized_Secondary_Surname__c != oldLst.Normalized_Secondary_Surname__c || iterator.Disconnected_Via_ABD__c != oldLst.Disconnected_Via_ABD__c || iterator.ABD_Pending_Review__c != oldLst.ABD_Pending_Review__c) {
                    setLstId.add(iterator.Id);
                }
                if(iterator.Listing_Street_Number__c != oldLst.Listing_Street_Number__c  ||  iterator.Listing_Street__c != oldLst.Listing_Street__c || iterator.Listing_PO_Box__c != oldLst.Listing_PO_Box__c || iterator.Listing_City__c != oldLst.Listing_City__c || iterator.Listing_State__c != oldLst.Listing_State__c ||
                   (iterator.Listing_street_number__c != null && !iterator.Listing_street_number__c.equals(oldLst.Listing_street_number__c)) || (iterator.Listing_Street__c != null && !iterator.Listing_Street__c.equals(oldLst.Listing_Street__c)) || (iterator.Listing_City__c != null && !iterator.Listing_City__c.equals(oldLst.Listing_City__c)) || 
                   (iterator.Listing_PO_Box__c != null && !iterator.Listing_PO_Box__c.equals(oldLst.Listing_PO_Box__c)) || (iterator.Listing_State__c != null && !iterator.Listing_State__c.equals(oldLst.Listing_State__c)) ||
                   iterator.Phone__c != oldLst.Phone__c  ||  iterator.Area_Code__c != oldLst.Area_Code__c || iterator.Exchange__c != oldLst.Exchange__c){
                    setDFFLstId.add(iterator.Id);   
                }
                //Adding the condition for skipping Listing scoped listing and DFF sync process
                if(iterator.Lst_SL_DFF_Sync__c != oldLst.Lst_SL_DFF_Sync__c && iterator.Lst_SL_DFF_Sync__c == true){
                    skipSLDFFSync = true;
                }
                if(iterator.Account__c != null){
                    if(oldLst.Telco_Provider_Account_ID__c != iterator.Telco_Provider_Account_ID__c && iterator.Telco_Provider_Account_ID__c != null){
                        Account objAct = new Account(Id = iterator.Account__c);
                        objAct.Telco_Partner__c = iterator.Telco_Provider_Account_ID__c;
                        if(!mapobjActLst.containsKey(objAct.Id)){
                            mapobjActLst.put(objAct.Id,objAct);
                        }
                    }
                }
                else if(iterator.Lead__c != Null) {
                    Lead objLd = new Lead(Id = iterator.Lead__c);
                    System.debug('*******Lead Record *******'+objLd);
                    if(iterator.Lead_Is_Converted__c == false) {
                        if(oldLst.Phone__c != iterator.Phone__c){
                            objLd.Phone = iterator.Phone__c;
                            bflagLeadUpdate = true;
                        }
                        if(oldLst.Listing_Street__c != iterator.Listing_Street__c){             
                            objLd.Street = iterator.Listing_Street__c;
                            bflagLeadUpdate = true;
                        }
                        if(oldLst.Listing_City__c != iterator.Listing_City__c){
                            objLd.City = iterator.Listing_City__c;
                            bflagLeadUpdate = true;
                        }
                        if(oldLst.Listing_State__c != iterator.Listing_State__c){
                            objLd.State = iterator.Listing_State__c;
                            bflagLeadUpdate = true;
                        }
                        if(oldLst.Listing_Postal_Code__c != iterator.Listing_Postal_Code__c){
                            objLd.PostalCode = iterator.Listing_Postal_Code__c;
                            bflagLeadUpdate = true;
                        }
                        if(oldLst.Listing_Country__c != iterator.Listing_Country__c){
                            objLd.Country = iterator.Listing_Country__c;
                            bflagLeadUpdate = true;
                        }
                        if(iterator.Telco_Provider_Account_ID__c != null && oldLst.Telco_Provider_Account_ID__c != iterator.Telco_Provider_Account_ID__c){
                            objLd.Telco_Partner_Account__c = iterator.Telco_Provider_Account_ID__c;
                            bflagLeadUpdate = true;
                        }
                        if(string.isNotBlank(iterator.LST_Last_Name_Business_Name__c) && !iterator.Lst_Last_Name_Business_Name__c.equals(oldLst.Lst_Last_Name_Business_Name__c)){
                            if(iterator.Lst_Last_Name_Business_Name__c.length()>80){
                                objLd.LastName = iterator.LST_Last_Name_Business_Name__c.Left(80);
                            }
                            else{
                                objLd.LastName = iterator.LST_Last_Name_Business_Name__c;
                            }
                            objLd.Company =  iterator.LST_Last_Name_Business_Name__c;
                            bflagLeadUpdate = true;
                        }
                        if(bflagLeadUpdate){
                            if(!mapobjLdLst.containsKey(objLd.Id)){
                                mapobjLdLst.put(objLd.Id,objLd);
                            }
                        }
                    }
                }
                }
            }
            /*else {
                if(iterator.Listing_Type__c == 'Main' && iterator.Account__c == null && string.isBlank(iterator.Lead__c) && iterator.Bus_Res_Gov_Indicator__c == 'Business'){
                    if(iterator.Phone__c != null){
                        mapLeadExist.put(iterator.Phone__c,iterator);
                    }
                }
            }*/                
        }
        /*if(mapLeadExist.size()> 0){
            listingLeadpopulate(mapLeadExist);
        }
        if(!skipSLDFFSync && setLstId.size()>0) {
            if(!system.isbatch()){
                if(!skipTrigger){
                    ListingToScopedListingSync.syncListingToScopedListing(setLstId,mapNewLst);
                }
                else {
                    ListingToScopedListingSync.syncNormalizedListingToScopedListing(setLstId,mapNewLst);
                }
            }
        }*/
        if(!skipSLDFFSync){
            if(!system.isbatch()){
                if(!skipTrigger && setLstId.size()>0){
                    ListingToScopedListingSync.syncListingToScopedListing(setLstId,mapNewLst);
                }
            }
            if(!skipTrigger && setDFFLstId.size()>0){
                ListingstoDFFPopulatevalues(setDFFLstId,mapNewLst);
            }
        }
        if(mapobjLdLst.size()> 0 && !system.isBatch()){
            CommonVariables.LeadListingAdrRecursive=true;
            update mapobjLdLst.values();                   
        }
        if(mapobjActLst.size()>0){
            update mapobjActLst.values();
        }
    }
    
   public static void AdditonalListingLeadAccountPopulate(list<listing__c> objLst,map<ID, listing__c> mapOldList) {
        list<listing__c> AddnLstupdate = new list<listing__c>();
        map<string,listing__c> mapListing = new map<string,listing__c>();
        for(listing__c iteratorLst : objLst) {
            if(mapOldList != null){
                listing__c iteratorOldLst = mapOldList.get(iteratorLst.Id);
                if(iteratorLst.Listing_type__c == 'Additional') {
                    if((iteratorLst.Main_Listing__c != iteratorOldLst.Main_Listing__c) && iteratorLst.Main_Listing__c != null) {
                        if(iteratorLst.Lst_AddlAccount__c != null) {
                            iteratorLst.Account__c = iteratorLst.Lst_AddlAccount__c;
                        }
                        else if(iteratorLst.Lst_AddlLead__c != null) {
                            iteratorLst.Lead__c = iteratorLst.Lst_AddlLead__c;
                        }
                    }
                }
                
                //Added by Mythili - for Sort_As field optimization
                if(iteratorLst.Phone__c != iteratorOldLst.Phone__c && String.isNotBlank(iteratorLst.Phone__c)) {
                    iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Phone__c.remove('(');
                    iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Listing_Phone_Unformatted_Substitute__c.remove(')');
                    iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Listing_Phone_Unformatted_Substitute__c.remove('-');
                    iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Listing_Phone_Unformatted_Substitute__c.remove(' ');
                }
                else if(String.isBlank(iteratorLst.Phone__c)) {
                    iteratorLst.Listing_Phone_Unformatted_Substitute__c=null;
                }
                
                                    
                //Added by Sathish - Truncate 80 char from Newly created field to populate on Standard Name field
                if(string.isNotBlank(iteratorLst.Lst_Last_Name_Business_Name__c) && (iteratorLst.Lst_Last_Name_Business_Name__c != null && !iteratorLst.Lst_Last_Name_Business_Name__c.equals(iteratorOldLst.Lst_Last_Name_Business_Name__c))) {
                    //iteratorLst.Name = iteratorLst.Lst_Last_Name_Business_Name__c.left(80);
                    if(iteratorLst.Lst_Last_Name_Business_Name__c.length() > 80) {
                        iteratorLst.Name = iteratorLst.Lst_Last_Name_Business_Name__c.Left(80);
                    }
                    else{
                        iteratorLst.Name = iteratorLst.Lst_Last_Name_Business_Name__c;
                    }
                }
                
                //Commented out by sathish - ATP-4320
                /*if(iteratorOldLst.Name != iteratorLst.Name && iteratorLst.Normalized_Last_Name_Business_Name__c != null) {
                    iteratorLst.Normalized_Last_Name_Business_Name__c='Normalization Pending';
                }
                if(iteratorOldLst.Listing_Street__c  != iteratorLst.Listing_Street__c && iteratorLst.Normalized_Listing_Street_Name__c != null && iteratorLst.Normalized_Last_Name_Business_Name__c != null) {
                    iteratorLst.Normalized_Last_Name_Business_Name__c='Normalization Pending';
                    iteratorLst.Normalized_Listing_Street_Name__c='';
                }*/
                //Added by sathish - ATP-4320
                //if(iteratorLst.Normalized_Last_Name_Business_Name__c != null) {
                    boolean bFlagNorm = false;
                    if(iteratorOldLst.Name != iteratorLst.Name) {
                        bFlagNorm = true;
                    }
                    if(iteratorOldLst.LST_Last_Name_Business_Name__c != iteratorLst.LST_Last_Name_Business_Name__c){
                        bFlagNorm = true;
                    }
                    if(iteratorOldLst.Listing_Street__c  != iteratorLst.Listing_Street__c) {
                        iteratorLst.Normalized_Listing_Street_Name__c='';
                        bFlagNorm = true;
                    }
                    if(iteratorOldLst.Listing_Street_Number__c  != iteratorLst.Listing_Street_Number__c) {
                        iteratorLst.Normalized_Listing_Street_Number__c = '';
                        bFlagNorm = true;
                    }
                    if(iteratorOldLst.Phone__c != iteratorLst.Phone__c) {
                        iteratorLst.Normalized_Phone__c = '';
                        bFlagNorm = true;
                    }
                    if(bFlagNorm) {
                        iteratorLst.Normalized_Last_Name_Business_Name__c='Normalization Pending';
                    }
                //}
                
                if(iteratorLst.ABD_Pending_Review__c == false && iteratorLst.Disconnected_Via_ABD__c == true && iteratorLst.Disconnected__c == true) {
                    iteratorLst.Disconnected__c = false;
                    iteratorLst.Disconnect_reason__c = '';
                    iteratorLst.Disconnected_Via_ABD__c = false;
                }
                if(iteratorOldLst.Listing_Type__c  != iteratorLst.Listing_Type__c && iteratorLst.Listing_Type__c == 'Main'){
                    iteratorLst.RecordtypeId = Label.TestListingMainRT;
                }
                if(iteratorOldLst.Listing_Type__c  != iteratorLst.Listing_Type__c && iteratorLst.Listing_Type__c == 'Additional'){
                    iteratorLst.RecordtypeId = Label.TestListingAdditionalRT;
                }
                if(iteratorOldLst.Listing_Type__c  != iteratorLst.Listing_Type__c && iteratorLst.Listing_Type__c == 'NOSO'){
                    iteratorLst.RecordtypeId = Label.TestListingNOSORT;
                }
                if(iteratorOldLst.Lst_Last_Name_Business_Name__c != iteratorLst.Lst_Last_Name_Business_Name__c || iteratorOldLst.First_Name__c != iteratorLst.First_Name__c || iteratorOldLst.Phone__c != iteratorLst.Phone__c || iteratorOldLst.Listing_Street_Number__c != iteratorLst.Listing_Street_Number__c || iteratorOldLst.Listing_PO_Box__c != iteratorLst.Listing_PO_Box__c || iteratorOldLst.Listing_Street__c != iteratorLst.Listing_Street__c) {
                    iteratorLst.Lst_StrListingMatch__c=iteratorLst.Lst_Last_Name_Business_Name__c+''+iteratorLst.First_Name__c+''+iteratorLst.Phone__c+''+iteratorLst.Listing_Street_Number__c+''+iteratorLst.Listing_Street__c+''+iteratorLst.Listing_PO_Box__c;
                }
                
                //Added by Mythili for ticket ATP-4502
                //if(iteratorOldLst.Normalized_Last_Name_Business_Name__c != iteratorLst.Normalized_Last_Name_Business_Name__c && string.isNotBlank(iteratorLst.Normalized_Last_Name_Business_Name__c)){
                if(string.isNotBlank(iteratorLst.Normalized_Last_Name_Business_Name__c)){
                    iteratorLst.LST_NormalisedLastName_LEFT_1_Char__c=iteratorLst.Normalized_Last_Name_Business_Name__c.Left(1);
                }
                else if(string.isBlank(iteratorLst.Normalized_Last_Name_Business_Name__c)){
                    iteratorLst.LST_NormalisedLastName_LEFT_1_Char__c=null;
                }
            }  
            else {
                //Added by Mythili - for Sort_As field optimization
                if(String.isNotBlank(iteratorLst.Phone__c)) {
                    iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Phone__c.remove('(');
                    iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Listing_Phone_Unformatted_Substitute__c.remove(')');
                    iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Listing_Phone_Unformatted_Substitute__c.remove('-');
                    iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Listing_Phone_Unformatted_Substitute__c.remove(' ');
                }
                
                //Added by Sathish - Truncate 80 char from Newly created field to populate on Standard Name field
                if(string.isNotBlank(iteratorLst.Lst_Last_Name_Business_Name__c)){
                    //iteratorLst.Name = iteratorLst.Lst_Last_Name_Business_Name__c.left(80);
                    if(iteratorLst.Lst_Last_Name_Business_Name__c.length()>80){
                        iteratorLst.Name = iteratorLst.Lst_Last_Name_Business_Name__c.Left(80);
                    }
                    else{
                        iteratorLst.Name = iteratorLst.Lst_Last_Name_Business_Name__c;
                    }
                }
                if(iteratorLst.Listing_type__c == 'Additional' && iteratorLst.Main_Listing__c != null) {
                    if(iteratorLst.Lst_AddlAccount__c != null){
                        iteratorLst.Account__c = iteratorLst.Lst_AddlAccount__c;
                    }
                    else if(iteratorLst.Lst_AddlLead__c != null){
                        iteratorLst.Lead__c = iteratorLst.Lst_AddlLead__c;
                    }
                }
                if(iteratorLst.Listing_Type__c == 'Main'){
                    iteratorLst.RecordtypeId = Label.TestListingMainRT;
                }
                if(iteratorLst.Listing_Type__c == 'Additional'){
                    iteratorLst.RecordtypeId = Label.TestListingAdditionalRT;
                }
                if(iteratorLst.Listing_Type__c == 'NOSO'){
                    iteratorLst.RecordtypeId = Label.TestListingNOSORT;
                }
                iteratorLst.Lst_StrListingMatch__c=iteratorLst.Lst_Last_Name_Business_Name__c+''+iteratorLst.First_Name__c+''+iteratorLst.Phone__c+''+iteratorLst.Listing_Street_Number__c+''+iteratorLst.Listing_Street__c+''+iteratorLst.Listing_PO_Box__c;
            
               //for ticket ATP-4502 by Mythili
               if(string.isNotBlank(iteratorLst.Normalized_Last_Name_Business_Name__c)) {
                    iteratorLst.LST_NormalisedLastName_LEFT_1_Char__c=iteratorLst.Normalized_Last_Name_Business_Name__c.Left(1);
               }
               
               //added by sat - atp-4502
               if(iteratorLst.Listing_Type__c == 'Main' && iteratorLst.Account__c == null && string.isBlank(iteratorLst.Lead__c) && iteratorLst.Bus_Res_Gov_Indicator__c == 'Business'){
                   if(iteratorLst.Phone__c != null){
                        mapListing.put(iteratorLst.Phone__c,iteratorLst);
                   }
               }
                
            }
        }
        if(mapListing.size()> 0){
            listingLeadpopulate(mapListing,objLst);
        }
    }
    
     public static void listingLeadpopulate(map<string,Listing__c> mapLeadActExist,list<listing__c> objLst){ 
        map<Id,listing__c> mapListingLeadAccountUpdate = new map<Id,listing__c>();
        map<string,Account> mapActExist = new map<string,Account>();
        map<string,Lead> mapLeadExist = new map<string,Lead>();
        set<Id> setAccountRTId = new set<Id>();
        setAccountRTId.add(Label.TestAccountCustomerRT);
        setAccountRTId.add(Label.TestAccountNationalRT);
        //Existing Accounts
        if(mapLeadActExist.size()>0){
            list<Account> actLst = [select Id, Name,Phone,ParentId,Is_Child_Account__c from Account where Phone IN :mapLeadActExist.keyset() and recordtypeId IN : setAccountRTId];
            if(actLst.size()> 0){
                for(Account acct :  actLst){
                    if(!mapActExist.containskey(acct.Phone)){
                        mapActExist.put(acct.Phone,acct);
                        mapLeadActExist.remove(acct.Phone);
                    }
                }
            }
        }
        //Existing Leads 
       if(mapLeadActExist.size()>0){
            list<lead> ldLst = [select Id,Name, Phone from Lead where Phone IN : mapLeadActExist.keyset()];
            if(ldLst.size()>0) {
                for(Lead ld : ldLst) {
                    if(!mapLeadExist.containskey(ld.Phone)){
                        mapLeadExist.put(ld.Phone,ld);
                        mapLeadActExist.remove(ld.Phone);
                    }   
                }
            }
        }
        list<Lead> leadLstInsert = new list<Lead>();
        if(mapLeadActExist.size()>0) {
            system.debug('New Lead Insertion Process');
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= Label.LeadAssignmentRuleId;
            for(Listing__c objLstng :mapLeadActExist.values()){
                Lead objLead = new Lead();
                if(string.isNotBlank(objLstng.LST_Last_Name_Business_Name__c)){
                    if(objLstng.Lst_Last_Name_Business_Name__c.length()>80){
                        objLead.LastName = objLstng.LST_Last_Name_Business_Name__c.Left(80);
                    }
                    else{
                        objLead.LastName = objLstng.LST_Last_Name_Business_Name__c;
                    }
                }
                objLead.FirstName = objLstng.First_Name__c;
                objLead.Company =  objLstng.LST_Last_Name_Business_Name__c;
                objLead.Phone = objLstng.Phone__c;
                objLead.Street = objLstng.Listing_Street__c;
                objLead.City = objLstng.Listing_City__c;
                objLead.State = objLstng.Listing_State__c;
                objLead.PostalCode = objLstng.Listing_Postal_Code__c;
                objLead.Country = objLstng.Listing_Country__c;
                objLead.Lead_Account_Source__c = objLstng.Lead_Account_Source__c;
                objLead.Status = 'Unqualified';
                objLead.Create_Listing__c = 'No';
                objLead.Primary_Canvass__c = objLstng.Primary_Canvass__c;
                objLead.setOptions(dmlOpts);
                if(objLstng.Telco_Provider_Account_ID__c != null){
                    objLead.Telco_Partner_Account__c = objLstng.Telco_Provider_Account_ID__c;
                }
               // objLead.RecordtypeId = CommonMethods.getRedordTypeIdByName('Service Order Lead','Lead');
                leadLstInsert.add(objLead);
            }
            if(leadLstInsert.size()>0){
                insert leadLstInsert;
            }
            for(Lead objLd :leadLstInsert ){
                if(!mapLeadExist.containskey(objLd.Phone)){
                   mapLeadExist.put(objLd.Phone,objLd);
                }  
            }
        }
        
        //Loop through the new listing record to assign account or lead
        for(listing__c iteratorLst : objLst){
            if(iteratorLst.Phone__c != null){
                if(mapActExist.get(iteratorLst.Phone__c) != null){
                	if(mapActExist.get(iteratorLst.Phone__c).Is_Child_Account__c == true){
                		iteratorLst.Account__c = mapActExist.get(iteratorLst.Phone__c).ParentId;
                	}
                	else {
                    	iteratorLst.Account__c = mapActExist.get(iteratorLst.Phone__c).Id;
                	}
                }
                else if(mapLeadExist.get(iteratorLst.Phone__c) != null){
                    iteratorLst.Lead__c = mapLeadExist.get(iteratorLst.Phone__c).Id;
                }
            }
        }
        
    }
    
    public static void ListingstoDFFPopulatevalues(set<Id> setLstId,map<Id,Listing__c> mapListing){
        list<order_line_items__c> lstOLI = new list<order_line_items__c>();
        list<Digital_Product_Requirement__c> lstDFF = new list<Digital_Product_Requirement__c>();
        map<Id,Id> mapOLIListing = new map<Id,Id>();
        set<string> setDFFRTId = new set<string>();
        setDFFRTId.addall(System.Label.DFFRTPrintListingNOContent.split(','));
        if(setLstId.size()> 0) {
            lstOLI=[Select Id,Digital_Product_Requirement__c,listing__c,Media_type__c,(select Id,OrderLineItemID__r.Listing__c from Digital_Product_Requirements__r where RecordtypeId IN :setDFFRTId) from Order_line_items__c where Listing__c IN :setLstId and Media_type__c=:CommonMessages.printMediaType];
            if(lstOLI.size()> 0) {
                for(Order_line_items__c iteratorOLI :lstOLI){
                    if(iteratorOLI.Digital_Product_Requirements__r.size()>0){
                        for(Digital_Product_Requirement__c iteratorDFF : iteratorOLI.Digital_Product_Requirements__r){
                            if(iteratorDFF.OrderLineItemID__r.Listing__c != null){
                                if(mapListing.get(iteratorDFF.OrderLineItemID__r.Listing__c) != null){
                                    listing__c objListing = mapListing.get(iteratorDFF.OrderLineItemID__r.Listing__c);
                                    iteratorDFF.Caption_Street_Number__c = objListing.Listing_Street_Number__c;
                                    iteratorDFF.Caption_Street_Name__c = objListing.Listing_Street__c;
                                    iteratorDFF.Caption_PO_Box__c = objListing.Listing_PO_Box__c;
                                    iteratorDFF.Caption_City__c = objListing.Listing_City__c;
                                    iteratorDFF.Caption_State__c = objListing.Listing_State__c;
                                    iteratorDFF.Caption_Phone_Number__c = objListing.Phone__c;
                                    iteratorDFF.Caption_Phone_Area_Code__c = objListing.Area_Code__c;
                                    iteratorDFF.Caption_Phone_Exchange__c = objListing.Exchange__c;
                                    lstDFF.add(iteratorDFF);
                                }
                            }
                        }
                    }
                }
            }
        }
        if(lstDFF.size()> 0){
            update lstDFF;
        }
    }
}