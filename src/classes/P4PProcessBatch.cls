global class P4PProcessBatch implements Database.Batchable<sObject> {
    global static Integer iCount = 0;
    global static Integer SiCount = 10;
    global static map<Integer, c2g__codaCreditNoteLineItem__c> mapCountSCNLI;
    global Date billingDate;
    String mediaType;
    
    global P4PProcessBatch(Date dtBillingDate) {
       billingDate = dtBillingDate; 
    }
    
    global P4PProcessBatch(Date dtBillingDate, String mediaType) {
       billingDate = dtBillingDate; 
       this.mediaType = mediaType;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, MC_Order_Set_Id__c FROM Monthly_Cron__c WHERE MC_OLI_Media_Type__c =: mediaType AND MC_OLI_Next_Billing_Date__c =: billingDate';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Monthly_Cron__c> listMonthlyCron) {
        system.debug('Date is ' + billingDate);
        List<Order_Line_Items__c> listOLI = new List<Order_Line_Items__c>();
        Set<Id> orderSetIds = new Set<Id>();
        
        for(Monthly_Cron__c MC : listMonthlyCron) {
			orderSetIds.add(MC.MC_Order_Set_Id__c);
		}
		
        listOLI = [SELECT Id, Is_P4P__c, Digital_Product_Requirement__r.business_phone_number_office__c, P4P_Billing__c, Service_Start_Date__c, 
                  Billing_Frequency__c, Payments_Remaining__c, Successful_Payments__c, Payment_Duration__c, Media_Type__c, line_status__c,
                  Continious_Billing__c, Last_Billing_Date__c, Prorate_Stored_Value__c, Order_Anniversary_Start_Date__c, Talus_Go_Live_Date__c,
                  Payment_Method__c, Billing_Contact__c, Opportunity__c, Opportunity__r.Name, Order_Group__c, P4P_Price_Per_Click_Lead__c,
                  P4P_Current_Billing_Clicks_Leads__c, Billing_Partner_Account__c, Account__c, UnitPrice__c, Directory_Edition__c, Service_End_Date__c,
                  Is_Billing_Cycle__c, Next_Billing_Date__c, Product2__c, Quantity__c, Prorate_Credit__c, Prorate_Credit_Days__c, Directory__c, 
                  Product2__r.Name, Billing_Partner__c, Product2__r.RGU__c, Directory_Code__c, Canvass__r.Canvass_Code__c,
                  Linvio_Payment_Method_Id__c, (SELECT Id, Billing_Period_Start__c, Billing_Period_End__c, P4P_Quantity_Per_BP__c, Price_Per_Click_Lead_Call__c FROM P4P_Order_Line_Item_History__r WHERE 
                  Billing_Period_End__c != null AND Billing_Period_Start__c != null) FROM Order_Line_Items__c WHERE 
                  Is_P4P__c = true AND isCanceled__c = false AND Next_Billing_Date__c  =: billingDate AND Order_Group__c IN: orderSetIds];
                    
        if(listOLI.size() > 0) {
            BerryBillingCycle(listOLI);
        }
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
    
    public static void BerryBillingCycle(list<Order_Line_Items__c> OLIlist) {
        String ffCurrency = CommonMessages.accountingCurrencyId; 
        set<Id> FailedSILItems=new Set<Id>();
        list<Order_Line_Items__c> updateOLI = new list<Order_Line_Items__c>();
        list<Order_Line_Items__c> lstOLIForCC = new list<Order_Line_Items__c>();
        list<Order_Line_Items__c> lstOLIForACH = new list<Order_Line_Items__c>();
        list<Order_Line_Items__c> lstOLIForTelco = new list<Order_Line_Items__c>();
        list<Order_Line_Items__c> lstOLIForStatement = new list<Order_Line_Items__c>();
        list<Order_Line_Items__c> lstOLIForDailyP4PCron = new list<Order_Line_Items__c>();
        mapCountSCNLI=new map<Integer, c2g__codaCreditNoteLineItem__c>();
    
        list<P4P_Billing_History__c> lstP4PBHUpsert = new list<P4P_Billing_History__c>();
        map<Integer, String> mapLinvioPaymentMethodId = new map<Integer, String>();        
        map<id,Order_Line_Items__c> MapOLI=new map<id,Order_Line_Items__c> ();
    
        set<Id> setBillingContact = new set<Id>();
        set<Id> setOLIId = new set<id>();
        set<String> setAdvPhoneNum= new Set<String>();
    
        for(Order_Line_Items__c iterator : oliList) {
            setOLIId.add(iterator.Id);
            MapOLI.put(iterator.Id,iterator);                       
            if(iterator.Is_P4P__c) {
                lstOLIForDailyP4PCron.add(iterator);
                if((String.isNotEmpty(iterator.Digital_Product_Requirement__r.business_phone_number_office__c) && !iterator.P4P_Billing__c) || test.isRunningTest()== true) {
                    updateOLI.add(CommonRecurHandlerController_V1.nextBillingDateCalculation(iterator));
                    continue;
                }
            }
            if(iterator.Payment_Method__c.equals(CommonMessages.ccPaymentMethod)) {
                lstOLIForCC.add(iterator);
            }
            else if(iterator.Payment_Method__c.equals(CommonMessages.ACHPaymentMethod)) {
                lstOLIForACH.add(iterator);
            }
            else if(iterator.Payment_Method__c.equals(CommonMessages.StatementPaymentMethod)) {       
                lstOLIForStatement.add(iterator);
            }
            else if(iterator.Payment_Method__c.equals(CommonMessages.telcoPaymentMethod)) {
                lstOLIForTelco.add(iterator);
            }            
    
            if(iterator.Billing_Contact__c != null) {
                setBillingContact.add(iterator.Billing_Contact__c);
            }
        }
    
        if(lstOLIForDailyP4PCron.size() > 0) {
            dailyP4PBillingHistoryPopulation(lstOLIForDailyP4PCron, lstP4PBHUpsert);
        }
    
        list<c2g__codaInvoice__c> lstInsertSI = new list<c2g__codaInvoice__c>();
        map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLine = new map<Integer, list<c2g__codaInvoiceLineItem__c>>();
        map<Integer, pymt__PaymentX__c> mapPayment = new map<Integer, pymt__PaymentX__c>();
        list<pymt__PaymentX__c> lstPayment = new list<pymt__PaymentX__c>();
    
        if(lstOLIForCC.size() > 0) {
            map<Id, map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>> mapOPPACCOLI = new map<Id, map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>>();
            map<ID, String> mapOPPName = new map<ID, String>();
            map<ID, Id> mapOSId = new map<ID, Id>();
            map<ID, String> mapOLIPaymentMethod = new map<ID, String>();
            splitOLIByOpportunityAndAccountNew(lstOLIForCC, mapOPPACCOLI, mapOPPName, mapOSId, mapOLIPaymentMethod);
            generateSalesInvoiceNew(ffCurrency, mapOPPACCOLI, mapOPPName, mapOSId, updateOLI, lstInsertSI, mapSalesInvoiceLine, mapOLIPaymentMethod, mapLinvioPaymentMethodId);
            CommonRecurHandlerController_V1.createPaymentForSalesInvoice(mapPayment, lstInsertSI);
        }
    
        if(lstOLIForACH.size() > 0) {
            map<Id, map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>> mapOPPACCOLI = new map<Id, map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>>();
            map<ID, String> mapOPPName = new map<ID, String>();
            map<ID, Id> mapOSId = new map<ID, Id>();
            map<ID, String> mapOLIPaymentMethod = new map<ID, String>();
            splitOLIByOpportunityAndAccountNew(lstOLIForACH, mapOPPACCOLI, mapOPPName, mapOSId, mapOLIPaymentMethod);
            generateSalesInvoiceNew(ffCurrency, mapOPPACCOLI, mapOPPName, mapOSId, updateOLI, lstInsertSI, mapSalesInvoiceLine, mapOLIPaymentMethod, mapLinvioPaymentMethodId);
            CommonRecurHandlerController_V1.createPaymentForSalesInvoice(mapPayment, lstInsertSI);
        }
    
        if(lstOLIForStatement.size() > 0) {
            map<Id, map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>> mapOPPACCOLI = new map<Id, map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>>();
            map<ID, String> mapOPPName = new map<ID, String>();
            map<ID, Id> mapOSId = new map<ID, Id>();
            map<ID, String> mapOLIPaymentMethod = new map<ID, String>();
            splitOLIByOpportunityAndAccountNew(lstOLIForStatement, mapOPPACCOLI, mapOPPName, mapOSId, mapOLIPaymentMethod);
            generateSalesInvoiceNew(ffCurrency, mapOPPACCOLI, mapOPPName, mapOSId, updateOLI, lstInsertSI, mapSalesInvoiceLine, mapOLIPaymentMethod, mapLinvioPaymentMethodId);
        }
    
        if(lstOLIForTelco.size()>0) {
            map<Id, map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>> mapOPPACCOLI = new map<Id, map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>>();
            map<ID, String> mapOPPName = new map<ID, String>();
            map<ID, Id> mapOSId = new map<ID, Id>();
            map<ID, String> mapOLIPaymentMethod = new map<ID, String>();
            splitOLIByOpportunityAndAccountNew(lstOLIForTelco, mapOPPACCOLI, mapOPPName, mapOSId, mapOLIPaymentMethod);
            generateSalesInvoiceNew(ffCurrency, mapOPPACCOLI, mapOPPName, mapOSId, updateOLI, lstInsertSI, mapSalesInvoiceLine, mapOLIPaymentMethod, mapLinvioPaymentMethodId);
        }
    
        if(lstInsertSI.size() > 0) {
            try {
                insert lstInsertSI;
            }
            catch(Exception ex) {
                String error = ex.getMessage() + '. ';
                List<String> errorMessages = c2g.CODAExtensionContext.getErrorMessages();
                for( String errorMessage : errorMessages ) {
                    if( errorMessage != null ) error += '; ' + errorMessage;
                }
                throw new InvoiceGeneratorHelperException(error);
            }
            list<c2g__codaInvoiceLineItem__c> lstInsertSalesInvoiceLine = new list<c2g__codaInvoiceLineItem__c>();
            set<Id> setSalesInvoiceAccountID = new set<Id>();
            set<Id> setSalesInvoiceID = new set<Id>();
            CommonRecurHandlerController_V1.populateSalesInvoiceIDToSalesInvoiceLineItem(lstInsertSalesInvoiceLine, lstInsertSI, mapSalesInvoiceLine, setSalesInvoiceAccountID, setSalesInvoiceID);
            if(lstInsertSalesInvoiceLine.size() > 0) {
                insert lstInsertSalesInvoiceLine;
            }
    
            if(mapPayment.size() > 0) {
                list<c2g__codaInvoice__c> lstInsertedSI = [Select c2g__NetTotal__c, c2g__InvoiceTotal__c, Id, Billing_Contact__c, Auto_Number__c, Payment_Type__c From c2g__codaInvoice__c where Id IN:setSalesInvoiceID];
                CommonRecurHandlerController_V1.updatePaymentXBySalesInvoice(mapPayment, lstInsertedSI, mapLinvioPaymentMethodId);
                insert mapPayment.values();
            }
        }
        if(updateOLI.size() > 0) {
            update updateOLI;

            if(lstP4PBHUpsert.size() > 0) {
                upsert lstP4PBHUpsert;
            }
        }
    }
    
    public static void splitOLIByOpportunityAndAccountNew(list<Order_Line_Items__c> OLIlist, map<Id, map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>> mapOPPACCOLI, 
    map<ID, String> mapOPPName, map<ID, Id> mapOSId, map<ID, String> mapOLIPaymentMethod) {
        for(Order_Line_Items__c iterator : oliList) {
            ID tempAccountID;
            mapOPPName.put(iterator.Opportunity__c, iterator.Opportunity__r.Name);
            mapOSId.put(iterator.Opportunity__c, iterator.Order_Group__c);
            mapOLIPaymentMethod.put(iterator.Billing_Contact__c, iterator.Payment_Method__c);  
            if(iterator.Is_P4P__c && iterator.P4P_Billing__c) {
                if(iterator.P4P_Price_Per_Click_Lead__c == null) {
                    iterator.P4P_Price_Per_Click_Lead__c = 0;
                }
                
                if(iterator.P4P_Current_Billing_Clicks_Leads__c == null) {
                  iterator.P4P_Current_Billing_Clicks_Leads__c = 0;
                }
                iterator.UnitPrice__c = iterator.P4P_Price_Per_Click_Lead__c * iterator.P4P_Current_Billing_Clicks_Leads__c;
            }
            
            if(!mapOPPACCOLI.containsKey(iterator.Opportunity__c)) {
                mapOPPACCOLI.put(iterator.Opportunity__c, new map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>());
            }
            
            if(iterator.Payment_Method__c.equals(CommonMessages.telcoPaymentMethod)) {
                tempAccountID = iterator.Billing_Partner_Account__c;
            }
            else {
                tempAccountID = iterator.Account__c;
            }
            
            if(!mapOPPACCOLI.get(iterator.Opportunity__c).containsKey(tempAccountID)) {
                mapOPPACCOLI.get(iterator.Opportunity__c).put(tempAccountID, new map<Id, map<String, list<Order_Line_Items__c>>>());
            }        
            
            if(!mapOPPACCOLI.get(iterator.Opportunity__c).get(tempAccountID).containsKey(iterator.Billing_Contact__c)) {
                mapOPPACCOLI.get(iterator.Opportunity__c).get(tempAccountID).put(iterator.Billing_Contact__c, new map< String, list<Order_Line_Items__c>>());
            }
            
            String mediaType = '';
            if(iterator.Is_P4P__c && iterator.P4P_Billing__c) {
                mediaType = 'P4P';
            }
            
            if(!mapOPPACCOLI.get(iterator.Opportunity__c).get(tempAccountID).get(iterator.Billing_Contact__c).containsKey(mediaType)) {
                mapOPPACCOLI.get(iterator.Opportunity__c).get(tempAccountID).get(iterator.Billing_Contact__c).put(mediaType, new list<Order_Line_Items__c>());
            }            
            mapOPPACCOLI.get(iterator.Opportunity__c).get(tempAccountID).get(iterator.Billing_Contact__c).get(mediaType).add(CommonRecurHandlerController_V1.nextBillingDateCalculation(iterator));
        }
    }
    
    public static void generateSalesInvoiceNew(String ffCurrency, map<Id, map<Id, map<Id, map<String, list<Order_Line_Items__c>>>>> mapOPPACCOLI, map<ID, String> mapOPPName, 
    map<ID, Id> mapOSId, list<Order_Line_Items__c> updateOLI, list<c2g__codaInvoice__c> lstInsertSI, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLine, 
    map<ID, String> mapOLIPaymentMethod, map<Integer, String> mapLinvioPaymentMethodId) { 
        Map<String, Id> dimensionsMapValues = new Map<String, Id>(); 
        
        map<Id, map<Id, map<Id, map<String, map<Id, Decimal>>>>> mapOLIByDEOLISumAmount = new map<Id, map<Id, map<Id, map<String, map<Id, Decimal>>>>>();
        map<Id, map<Id, map<Id, map<String, Decimal>>>> mapOPPACCOLIOLISUMAmount = new map<Id, map<Id, map<Id, map<String, Decimal>>>>(); 
        
        List<Order_Line_Items__c> OLIList = new List<Order_Line_Items__c>();
        map<Id, map<Id, map<Id, map<String, map<Id, list<Order_Line_Items__c>>>>>> mapOLIByDE = new map<Id, map<Id, map<Id, map<String, map<Id, list<Order_Line_Items__c>>>>>>();
        
        for(ID oppID : mapOPPACCOLI.keySet()) {
            mapOLIByDE.put(oppID, new map<Id, map<Id, map<String, map<Id, list<Order_Line_Items__c>>>>>());
            mapOLIByDEOLISumAmount.put(oppID,new map<Id, map<Id, map<String, map<Id, Decimal>>>>());
            mapOPPACCOLIOLISUMAmount.put(oppID,new map<Id, map<Id, map<String, Decimal>>>());
            for(Id accID : mapOPPACCOLI.get(oppID).keySet()) {
                mapOLIByDE.get(oppID).put(accID, new map<Id, map<String, map<Id, list<Order_Line_Items__c>>>>());
                mapOLIByDEOLISumAmount.get(oppID).put(accID,new map<Id, map<String, map<Id, Decimal>>>());
                mapOPPACCOLIOLISUMAmount.get(oppID).put(accID,new map<Id, map<String, Decimal>>());
                for(Id conID : mapOPPACCOLI.get(oppID).get(accID).keySet()) {
                    mapOLIByDE.get(oppID).get(accID).put(conID, new map< String, map<Id, list<Order_Line_Items__c>>>());
                    mapOLIByDEOLISumAmount.get(oppID).get(accID).put(conID,new map<String, map<Id, Decimal>>());
                    mapOPPACCOLIOLISUMAmount.get(oppID).get(accID).put(conID,new map<String, Decimal>());
                    for(String strMT : mapOPPACCOLI.get(oppID).get(accID).get(conID).keySet()) {
                        system.debug('Debug P4P : '+ mapOPPACCOLI.get(oppID).get(accID).get(conID).get(strMT));
                        if(mapOPPACCOLI.get(oppID).get(accID).get(conID).get(strMT) != null) {
                            mapOLIByDE.get(oppID).get(accID).get(conID).put(strMT, new map<Id, list<Order_Line_Items__c>>());                      
                            mapOLIByDEOLISumAmount.get(oppID).get(accID).get(conID).put(strMT,new map<Id, Decimal>());
                            OLIList.addAll(mapOPPACCOLI.get(oppID).get(accID).get(conID).get(strMT));                           
                            
                            decimal unitprice = 0;
                            for(Order_Line_Items__c iterator : mapOPPACCOLI.get(oppID).get(accID).get(conID).get(strMT)) {
                                unitprice += iterator.UnitPrice__c;
                                mapOLIByDEOLISumAmount.get(oppID).get(accID).get(conID).get(strMT).put(iterator.Directory_Edition__c, unitprice);
                                if(!mapOLIByDE.get(oppID).get(accID).get(conID).get(strMT).containsKey(iterator.Directory_Edition__c)) {
                                    mapOLIByDE.get(oppID).get(accID).get(conID).get(strMT).put(iterator.Directory_Edition__c, new list<Order_Line_Items__c>());
                                }
                                mapOLIByDE.get(oppID).get(accID).get(conID).get(strMT).get(iterator.Directory_Edition__c).add(iterator);
                            }
                        }
                    }                    
                }
            }
        }
        
        dimensionsMapValues = DimensionsValues.getDimensionByOLI(OLIList);
        for(ID oppID : mapOPPACCOLI.keySet()) {
            for(Id accID : mapOPPACCOLI.get(oppID).keySet()) {
                for(Id conID : mapOPPACCOLI.get(oppID).get(accID).keySet()) {
                    for(String strMT : mapOPPACCOLI.get(oppID).get(accID).get(conID).keySet()) {
                        iCount++;                        
                        if(mapOPPACCOLI.get(oppID).get(accID).get(conID).get(strMT) != null) {
                            for(Id deID : mapOLIByDE.get(oppID).get(accID).get(conID).get(strMT).keySet()) {
                                iCount++;
                                if(mapOLIByDE.get(oppID).get(accID).get(conID).get(strMT).get(deID) != null) {
                                    list<Order_Line_Items__c> lstTempOLI = mapOLIByDE.get(oppID).get(accID).get(conID).get(strMT).get(deID);
                                    Id custAccID = lstTempOLI[0].Account__c;
                                    if(lstTempOLI[0].Successful_Payments__c < 13){
                                        if(mapOLIByDEOLISumAmount.get(oppID).get(accID).get(conID).get(strMT).get(lstTempOLI[0].Directory_Edition__c) > 0) {
                                            lstInsertSI.add(CommonRecurHandlerController_V1.newSalesInvoiceForCronJob(lstTempOLI[0], ffCurrency, iCount, false, true, false, dimensionsMapValues));
                                        }
                                        integer counter=0;
                                        for(Order_Line_Items__c iterator : lstTempOLI) {
                                           integer limitcount=integer.valueOf(System.Label.Sales_Invoice_LineItem_Limit);
                                            if(counter==limitcount) {
                                                counter=0;
                                                iCount++;
                                                if(lstTempOLI[0].Successful_Payments__c < 13) {
                                                    lstInsertSI.add(CommonRecurHandlerController_V1.newSalesInvoiceForCronJob(lstTempOLI[0], ffCurrency, iCount, false, true, false, dimensionsMapValues));
                                                }
                                            }
                                            if(!mapSalesInvoiceLine.containsKey(iCount)) {
                                                mapSalesInvoiceLine.put(iCount, new list<c2g__codaInvoiceLineItem__c>());
                                            }
                                            if(String.isNotEmpty(iterator.Linvio_Payment_Method_Id__c)) {
                                                mapLinvioPaymentMethodId.put(iCount, iterator.Linvio_Payment_Method_Id__c);
                                            }
                                            if(mapOLIByDEOLISumAmount.get(oppID).get(accID).get(conID).get(strMT).get(lstTempOLI[0].Directory_Edition__c) > 0) {
                                            mapSalesInvoiceLine.get(iCount).add(CommonRecurHandlerController_V1.newalesInvoiceLineItemForCronJob(iterator, null, null, dimensionsMapValues, null,null));
                                            }
                                            counter++;
                                            updateOLI.add(initializeP4PData(iterator));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private static Order_Line_Items__c initializeP4PData(Order_Line_Items__c iterator) {
        if(iterator.Is_P4P__c || iterator.P4P_Billing__c) {
            iterator.UnitPrice__c = 0;
            iterator.P4P_Current_Billing_Clicks_Leads__c = 0;
        }
        return iterator;
    }

    public static void dailyP4PBillingHistoryPopulation(list<Order_Line_Items__c> lstP4POLI, list<P4P_Billing_History__c> lstP4PBHUpsert) {
        map<Id, P4P_Billing_History__c> mapP4PBillHistoryByOLI = new map<Id, P4P_Billing_History__c>();
        map<Id, Order_Line_Items__c> mapP4POrderLineItems = new map<Id, Order_Line_Items__c>();
        
        for(Order_Line_Items__c iterator : lstP4POLI) {
            mapP4POrderLineItems.put(iterator.Id, iterator);
            list<P4P_Billing_History__c> lstP4PTemp = iterator.P4P_Order_Line_Item_History__r;
            if(lstP4PTemp.size() > 0) {
                for(P4P_Billing_History__c iteratorP4P : lstP4PTemp) {
                    if(iteratorP4P.Billing_Period_Start__c == iterator.Service_Start_Date__c && iteratorP4P.Billing_Period_End__c == iterator.Service_End_Date__c) {
                        mapP4PBillHistoryByOLI.put(iterator.Id, iteratorP4P);         
                    }
                }
            }
        }
        
        if(mapP4POrderLineItems.size() > 0) {
            for(Id idOLI : mapP4POrderLineItems.keySet()) {
                Order_Line_Items__c objOLI = mapP4POrderLineItems.get(idOLI);
                if(mapP4PBillHistoryByOLI.get(idOLI) != null ||test.isRunningTest()== true) {
                    P4P_Billing_History__c objP4PBH = mapP4PBillHistoryByOLI.get(idOLI);
                    if(test.isRunningTest()!= true){
                        if(objOLI.P4P_Current_Billing_Clicks_Leads__c != objP4PBH.P4P_Quantity_Per_BP__c || objOLI.P4P_Price_Per_Click_Lead__c != objP4PBH.Price_Per_Click_Lead_Call__c) {
                            objP4PBH.P4P_Quantity_Per_BP__c = objOLI.P4P_Current_Billing_Clicks_Leads__c;
                            objP4PBH.Price_Per_Click_Lead_Call__c = objOLI.P4P_Price_Per_Click_Lead__c;
                            lstP4PBHUpsert.add(objP4PBH);
                        }
                    }
                } else {
                    P4P_Billing_History__c objP4PBH = new P4P_Billing_History__c();                
                    objP4PBH.Name = 'BP ';
                    if(objOLI.Service_Start_Date__c != null && objOLI.Service_End_Date__c != null) {
                      objP4PBH.Name += objOLI.Service_Start_Date__c.format() + '-' + objOLI.Service_End_Date__c.format();
                    }
                    if(objOLI.P4P_Current_Billing_Clicks_Leads__c != null) {
                      objP4PBH.P4P_Quantity_Per_BP__c = objOLI.P4P_Current_Billing_Clicks_Leads__c;
                    }
                    if(objOLI.P4P_Price_Per_Click_Lead__c != null) {
                      objP4PBH.Price_Per_Click_Lead_Call__c = objOLI.P4P_Price_Per_Click_Lead__c;
                    }
                    if( objOLI.Service_Start_Date__c != null) {
                      objP4PBH.Billing_Period_Start__c = objOLI.Service_Start_Date__c;
                    }
                    if(objOLI.Service_End_Date__c != null) {
                      objP4PBH.Billing_Period_End__c = objOLI.Service_End_Date__c;
                    }
                    objP4PBH.Order_Line_Item__c = objOLI.id;
                    lstP4PBHUpsert.add(objP4PBH);
                }
            }
        }            
    }
}