public with sharing class MediaTraxHTTPCalloutHandlerController_V1 {
    
    public static DOM.Document getCalloutRespose(String endPointURl, DOM.Document objDoc, String headerSOAPAction, String calloutState, String contentType) {
        try
        {
            HttpRequest req = new HttpRequest();
            req.setMethod(calloutState);
            req.setEndpoint(endPointURl);
            req.setHeader('Content-Type', contentType);
            Http http = new Http();
            HttpResponse res;
            req.setHeader('SOAPAction', headerSOAPAction);
            System.debug('Testingg check objDoc '+objDoc);
            req.setBodyDocument(objDoc);
            req.setTimeOut(50000);
            System.debug('Testingg check req '+req);
            System.debug('Testingg check req.getBodyDocument() '+req.getBodyDocument());
            System.debug('Testingg check req.getBody() '+req.getBody());
            System.debug('Testingg check objDoc '+objDoc);
            System.debug('Testingg check objDoc.toXmlString() '+objDoc.toXmlString());
            System.debug('Testingg check req.toString() '+req.toString());
            if(!Test.isRunningTest()){
                res = http.send(req);
            }
            else {
                res = new HttpResponse();
                res.setHeader('Content-Type', 'application/xml');
                if(headerSOAPAction == 'AddTrackingNumber') {
                    res.setBody('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><Response xmlns="https://ct.mediatrax.com/api/"><Return xsi:type="ns1:Document"xmlns:ns1="http://xml.apache.org/xml-soap"><AddTrackingNumber xmlns="http://calltraks.dev.local/api/"><action>AddTrackingNumber</action><trackingnumberid>93</trackingnumberid><trackingnumber>12345</trackingnumber><total>1</total></AddTrackingNumber></Return></Response></soapenv:Body></soapenv:Envelope>');
                }
                else if(headerSOAPAction == 'AddTrackingNumberError') {
                    res.setBody('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><Response xmlns="https://ct.mediatrax.com/api/"><Return xsi:type="ns1:Document"xmlns:ns1="http://xml.apache.org/xml-soap"><AddTrackingNumber xmlns="http://calltraks.dev.local/api/"><action>AddTrackingNumber</action><errorResponse xmlns="https://ct.mediatrax.com/api/"><message xmlns="https://ct.mediatrax.com/api">NPA has no rate centers</message></errorResponse><trackingnumber>12345</trackingnumber><total>1</total></AddTrackingNumber></Return></Response></soapenv:Body></soapenv:Envelope>');
                }
                else {
                    res.setBody('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><Response><Return><GetHeadings><action>GetHeadings</action><headings><heading><headingid>93</headingid><headingname>TestName</headingname></heading></headings><total>1</total></GetHeadings></Return></Response><Response xmlns="https://ct.mediatrax.com/api/"><Return xsi:type="ns1:Document"xmlns:ns1="http://xml.apache.org/xml-soap"><GetDirectories xmlns="http://calltraks.dev.local/api/"><directories><directory><directorynumber>101753</directorynumber><directoryid>1974</directoryid></directory></directories><total>2</total></GetDirectories></Return></Response><Response xmlns="https://ct.mediatrax.com/api/"><Return xsi:type="ns1:Document"xmlns:ns1="http://xml.apache.org/xml-soap"><AddTrackingNumber xmlns="http://calltraks.dev.local/api/"><action>AddTrackingNumber</action><trackingnumberid>93</trackingnumberid><trackingnumber>12345</trackingnumber><total>1</total></AddTrackingNumber></Return></Response></soapenv:Body></soapenv:Envelope>');
                }
                res.setStatusCode(200);
            }
            //system.debug('Testingg Status Code : '+ res.getStatusCode());
            //system.debug('Testingg getBodyDocument : '+ res.getBodyDocument());
            
            if(res.getStatusCode() == 200) {
                system.debug('Testingg XML : '+ res.getBody());
                return res.getBodyDocument();
            }
            else{
            	if(headerSOAPAction == 'AddTrackingNumber') {
            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception occurred while contacting external system. Please try after some time or contact administrator if error persist.'));
            	}
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception occurred while contacting external system. Please try after some time or contact administrator if error persist.'));
                futureCreateErrorLog.MediaTraxStatusBatch('MediaTrax returned error status code',res.getStatusCode(),res.getBody(), 'MediaTrax');
            }
            //system.debug('Testingg XML response : '+ res.getBody());
            return null;
        }catch(CalloutException objExp){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception occured while contacting external system. Please try after some time or contact administrator if error persist.'));
            throw new CustomException('Error Type : '+objExp.getTypename()+' Error Message : '+objExp.getMessage(),objExp.getStackTraceString());
        }catch(CustomException objExp){
            throw objExp;
        }catch(Exception objExp){
            //System.debug('Exception occured in MediaTraxHTTPCalloutHandlerController_V1 class in method getCalloutRespose - Message '+objExp.getMessage());
            throw new CustomException('Error Type : '+objExp.getTypename()+' Error Message : '+objExp.getMessage(),objExp.getStackTraceString());
        }
    }
    
    public static DOM.Document generateDOMObject(MediaTrax_API_Configuration__c objMedia, Map<String,String> mapfields) {
        try
        {
            String strApiURL = objMedia.API_URL__c;
            DOM.Document doc = new DOM.Document();
            dom.XmlNode envelope = doc.createRootElement('Envelope', objMedia.SOAP_URL__c, 'soapenv'); 
            envelope.setNamespace('api', strApiURL);
            dom.XmlNode body = envelope.addChildElement('Body', objMedia.SOAP_URL__c, null);
            dom.XmlNode body1 = body.addChildElement(objMedia.Header_SOAPAction__c, strApiURL, 'api');
            body1.addChildElement('authtoken', strApiURL, null).addTextNode(objMedia.AuthToken__c);
            body1.addChildElement('username', strApiURL, null).addTextNode(objMedia.UserName__c);
            body1.addChildElement('password', strApiURL, null).addTextNode(objMedia.Password__c);
            
            if(String.isNotblank(objMedia.Return_Fields__c)) {
                body1.addChildElement('fields', strApiURL, null).addTextNode(objMedia.Return_Fields__c);
            }
                
            if(mapfields != null && objMedia.Fields_Mapping__c != null){
                List<String> lstFields = objMedia.Fields_Mapping__c.split('\\,');
                for(String strField : lstFields){
                    if(mapfields.containsKey(strField)){
                        body1.addChildElement(strField, strApiURL, null).addTextNode(mapfields.get(strField));
                        //XmlCData.addCDataNodes(xml)
                    }
                    /*else{
                        body1.addChildElement(strField, strApiURL, null).addTextNode('');
                    } */
                }
            }
            system.debug('Testingg doc.toXmlString()'+doc.toXmlString());
            return doc;
        }catch(Exception objExp){
            
            System.debug('Exception occured in MediaTraxHTTPCalloutHandlerController_V1 class in method getCalloutRespose - Message '+objExp.getMessage());
            throw new CustomException('Error Type : '+objExp.getTypename()+' Error Message : '+objExp.getMessage(),objExp.getStackTraceString());
        }
    }
    
    public static DOM.Document mediaTraxCall(MediaTrax_API_Configuration__c objMedia, Map<String,String> mapfields){
                                                    
        try{
            DOM.Document objDoc = generateDOMObject(objMedia, mapfields);
            Dom.Document doc2 = getCalloutRespose(objMedia.Endpoint_URL__c, objDoc, objMedia.Header_SOAPAction__c, objMedia.Callout_Method__c, objMedia.Content_Type__c);       
            return doc2;
        }catch(CustomException objExp){
            throw(objExp);
        }catch(Exception objExp){
            throw new CustomException('Error Type : '+objExp.getTypename()+' Error Message : '+objExp.getMessage(),objExp.getStackTraceString());
        }
    }
    
    public static map<String, dom.XmlNode> parseDirectoryXML(DOM.Document objDoc, String soapNS, String xsi, String strReturn, String strResponse) {
        map<String, dom.XmlNode> mapNodeResult = new map<String, dom.XmlNode>();
        try
        {
            
            if(objDoc != null) {
                Dom.XMLNode address = objDoc.getRootElement();
                System.debug('Testingg 111 address.getChildElement '+address.getChildElement('Body', soapNS));
                System.debug('Testingg 111 '+address.getChildElement('Body', soapNS).getChildElement(strResponse, xsi));
                System.debug('Testingg 111 '+address.getChildElement('Body', soapNS).getChildElement(strResponse, xsi).getChildElement(strReturn, xsi));
                dom.XmlNode header = address.getChildElement('Body', soapNS).getChildElement(strResponse, xsi).getChildElement(strReturn, xsi);
                Boolean bFlag = false;
                for(dom.XmlNode iterator: header.getChildElements()) {
                    for(dom.XmlNode iterator1: iterator.getChildElements()) {
                        if(iterator1.getName() == 'errorResponse'){
                            mapNodeResult.put('Error', iterator1);
                            bFlag = true;
                        }
                    }
                    if(!bFlag) {
                        mapNodeResult.put('result', iterator);
                    }                   
                }            
            }
            return mapNodeResult;
        }catch(Exception objExp){
            
//            System.debug('Exception occured in MediaTraxHTTPCalloutHandlerController_V1 class in method parseDirectoryXML1 - Message '+objExp.getMessage());
            throw new CustomException('Error Type : '+objExp.getTypename()+' Error Message : '+objExp.getMessage(),objExp.getStackTraceString());
        }
    }
    
    public static DOM.Document TrackingInMediaTraxCall(Digital_Product_Requirement__c objDFF, Canvass__c objCanvass, MediaTrax_API_Configuration__c objConf) {
        try
        {
           system.debug('----for test method--------'+objDFF+'----'+objCanvass+'---'+objConf);
            DOM.Document objDoc = generateDOMObjectForTracking(objDFF, objCanvass, objConf);
            Dom.Document doc2 = getCalloutRespose(objConf.Endpoint_URL__c, objDoc, objConf.Header_SOAPAction__c, objConf.Callout_Method__c, objConf.Content_Type__c);
            return doc2;
        }catch(CustomException objExp){
            throw(objExp);
        }catch(Exception objExp){
            throw new CustomException('Error Type : '+objExp.getTypename()+' Error Message : '+objExp.getMessage(),objExp.getStackTraceString());
        }
    }
    
    public static DOM.Document generateDOMObjectForTracking(Digital_Product_Requirement__c objDFF, Canvass__c objCanvass, MediaTrax_API_Configuration__c objConf) {
        try
        {
            DOM.Document doc = new DOM.Document();
            dom.XmlNode envelope = doc.createRootElement('Envelope', objConf.SOAP_URL__c, 'soapenv'); 
            envelope.setNamespace('api', objConf.API_URL__c);
            dom.XmlNode body = envelope.addChildElement('Body', objConf.SOAP_URL__c, null);
            dom.XmlNode body1 = body.addChildElement(objConf.Header_SOAPAction__c, objConf.API_URL__c, 'api');
            body1.addChildElement('authtoken', objConf.API_URL__c, null).addTextNode(objConf.AuthToken__c);
            body1.addChildElement('username', objConf.API_URL__c, null).addTextNode(objConf.UserName__c);
            body1.addChildElement('password', objConf.API_URL__c, null).addTextNode(objConf.Password__c);
            body1.addChildElement('contractId', objConf.API_URL__c, null).addTextNode(objDFF.OrderLineItemID__r.Name.replace('OL-',''));
            body1.addChildElement('clientId', objConf.API_URL__c, null).addTextNode(objDFF.Account__r.Media_Trax_Client_ID__c);
            body1.addChildElement('clientLocation', objConf.API_URL__c, null).addTextNode(objDFF.Account__r.BillingCity);
            body1.addChildElement('stateCode', objConf.API_URL__c, null).addTextNode(objDFF.Account__r.BillingState);
            body1.addChildElement('countryCode', objConf.API_URL__c, null).addTextNode(objDFF.Account__r.BillingCountry);
            
            Pattern nonWordChar = Pattern.compile('[()-]');
            body1.addChildElement('targetNumber', objConf.API_URL__c, null).addTextNode(nonWordChar.matcher(objDFF.business_phone_number_office__c).replaceAll('').replace(' ',''));
            //body1.addChildElement('targetNumber', objConf.API_URL__c, null).addTextNode(objDFF.business_phone_number_office__c.replace('-','').replace('(','').replace(') ',''));
            body1.addChildElement('requestedAreaCode', objConf.API_URL__c, null).addTextNode(objDFF.P4P_Requested_Area_Code__c);
            body1.addChildElement('requestedPrefix', objConf.API_URL__c, null).addTextNode(objDFF.P4P_Requested_Prefix__c);
            
            body1.addChildElement('publishedNumber', objConf.API_URL__c, null).addTextNode('');
            System.debug('Testingg http objDFF.OrderLineItemID__r.Directory__r.Media_Trax_Directory_ID__c '+objDFF.OrderLineItemID__r.Directory__r.Media_Trax_Directory_ID__c);
            body1.addChildElement('directoryId', objConf.API_URL__c, null).addTextNode(objDFF.OrderLineItemID__r.Directory__r.Media_Trax_Directory_ID__c);
            System.debug('Testingg http objDFF.OrderLineItemID__r.Directory_Heading__r.Media_Trax_Heading_ID__c '+objDFF.OrderLineItemID__r.Directory_Heading__r.Media_Trax_Heading_ID__c);
            body1.addChildElement('headingId', objConf.API_URL__c, null).addTextNode(objDFF.OrderLineItemID__r.Directory_Heading__r.Media_Trax_Heading_ID__c);
            
            String strMonth = '';
			if(System.Today().Month() <= 9) {
				strMonth = '0'+ string.valueOf(System.Today().month());
			}
			else {
				strMonth = string.valueOf(System.Today().Month());
			}
			
			String strDay = '';
			if(System.Today().day() <= 9) {
				strDay = '0'+ string.valueOf(System.Today().day());
			}
			else {
				strDay = string.valueOf(System.Today().day());
			}
            
            
            body1.addChildElement('trackingNumberStarDate', objConf.API_URL__c, null).addTextNode(System.Today().Year() +'-'+System.Today().month()+'-'+System.Today().day());
            //body1.addChildElement('trackingNumberStarDate', objConf.API_URL__c, null).addTextNode(System.Today().Year() +'-'+strMonth+'-'+strDay);
            Date dtTrackingEndDate=System.today().addMonths(13);
            //System.debug('Testingg http objDFF.OrderLineItemID__r.Last_Billing_Date__c.Year() +'-'+objDFF.OrderLineItemID__r.Last_Billing_Date__c.month()+'-'+objDFF.OrderLineItemID__r.Last_Billing_Date__c.day() '+objDFF.OrderLineItemID__r.Last_Billing_Date__c.Year() +'-'+objDFF.OrderLineItemID__r.Last_Billing_Date__c.month()+'-'+objDFF.OrderLineItemID__r.Last_Billing_Date__c.day());
            
            strMonth = '';
			if(dtTrackingEndDate.Month() <= 9) {
				strMonth = '0'+ string.valueOf(dtTrackingEndDate.month());
			}
			else {
				strMonth = string.valueOf(dtTrackingEndDate.Month());
			}
			
			strDay = '';
			if(dtTrackingEndDate.day() <= 9) {
				strDay = '0'+ string.valueOf(dtTrackingEndDate.day());
			}
			else {
				strDay = string.valueOf(dtTrackingEndDate.day());
			}
            //body1.addChildElement('trackingNumberEndDate', objConf.API_URL__c, null).addTextNode(dtTrackingEndDate.Year() +'-'+strMonth+'-'+strDay);
            body1.addChildElement('trackingNumberEndDate', objConf.API_URL__c, null).addTextNode(dtTrackingEndDate.Year() +'-'+dtTrackingEndDate.month()+'-'+dtTrackingEndDate.day());
            System.debug('Testingg http objDFF.UDAC__c '+objDFF.UDAC__c);
            body1.addChildElement('udac', objConf.API_URL__c, null).addTextNode(objDFF.UDAC__c);
            
            body1.addChildElement('adCode', objConf.API_URL__c, null).addTextNode('');
            String strP4pNumberType = objDFF.P4P_Number_Type__c;
            if(strP4pNumberType == '800 Number') {
            	strP4pNumberType = '800Number';
            }
            else if(strP4pNumberType == 'Toll Free') {
            	strP4pNumberType = 'TollFree';
            }
            body1.addChildElement('numberType', objConf.API_URL__c, null).addTextNode(strP4pNumberType);
            
            body1.addChildElement('costPerCall', objConf.API_URL__c, null).addTextNode(String.valueOf('true'));
            body1.addChildElement('costPerCallPrice', objConf.API_URL__c, null).addTextNode(String.valueOf(objDFF.OrderLineItemID__r.P4P_Price_Per_Click_Lead__c));
            body1.addChildElement('costPerCallTime', objConf.API_URL__c, null).addTextNode(objCanvass.P4P_Call_Time__c);
            body1.addChildElement('costPerCallThreshold', objConf.API_URL__c, null).addTextNode(objCanvass.P4P_Threshold__c);
            body1.addChildElement('costPerCallFrequency', objConf.API_URL__c, null).addTextNode(objCanvass.P4P_Frequency__c);
            body1.addChildElement('callRecording', objConf.API_URL__c, null).addTextNode((objDFF.Call_Recording__c ? 'true' : 'false'));
            
            body1.addChildElement('callTransfer', objConf.API_URL__c, null).addTextNode('false');
            body1.addChildElement('voiceMail', objConf.API_URL__c, null).addTextNode('false');
            body1.addChildElement('mailMaster', objConf.API_URL__c, null).addTextNode('false');
            body1.addChildElement('whisperMode', objConf.API_URL__c, null).addTextNode('false');
            body1.addChildElement('callEmployeeCode', objConf.API_URL__c, null).addTextNode('false');
            return doc;
        } catch(Exception objExp) {
			if(!Test.isRunningTest()) {
            	throw new CustomException('Error Type : '+objExp.getTypename()+' Error Message : '+objExp.getMessage(),objExp.getStackTraceString());
			} else {
				return null;
			}
        }
    }
    
    public static DOM.Document TrackingInMediaTraxCallDeActivate(String strTrackNo, Date dtCanceldate, MediaTrax_API_Configuration__c objConf) {
        try{
            DOM.Document objDoc = generateDOMObjectForTrackingDeacivate(strTrackNo, dtCanceldate, objConf);
            //System.debug('Testingg objDoc in HeadingInMediaTraxCall '+objDoc);
            Dom.Document doc2 = getCalloutRespose(objConf.Endpoint_URL__c, objDoc, objConf.Header_SOAPAction__c, objConf.Callout_Method__c, objConf.Content_Type__c);
            //System.debug('Testingg doc2 in HeadingInMediaTraxCall '+doc2);  
            return doc2;
        }catch(CustomException objExp){
            throw objExp;
        }catch(Exception objExp){
            System.debug('Exception occured in MediaTraxHTTPCalloutHandlerController_V1 class in method generateDOMObjectForTracking - Message '+objExp.getMessage()+objExp.getStackTraceString());
            throw new CustomException('Error Type : '+objExp.getTypename()+' Error Message : '+objExp.getMessage(),objExp.getStackTraceString());
        }
    }
    
    public static DOM.Document generateDOMObjectForTrackingDeacivate(String strTrackNo, Date dtCanceldate, MediaTrax_API_Configuration__c objConf) {
        try{
            DOM.Document doc = new DOM.Document();
            dom.XmlNode envelope = doc.createRootElement('Envelope', objConf.SOAP_URL__c, 'soapenv'); 
            envelope.setNamespace('api', objConf.API_URL__c);
            dom.XmlNode body = envelope.addChildElement('Body', objConf.SOAP_URL__c, null);
            dom.XmlNode body1 = body.addChildElement(objConf.Header_SOAPAction__c, objConf.API_URL__c, 'api');
            body1.addChildElement('authtoken', objConf.API_URL__c, null).addTextNode(objConf.AuthToken__c);
            body1.addChildElement('username', objConf.API_URL__c, null).addTextNode(objConf.UserName__c);
            body1.addChildElement('password', objConf.API_URL__c, null).addTextNode(objConf.Password__c);
            body1.addChildElement('trackingNumberId', objConf.API_URL__c, null).addTextNode(strTrackNo);        
            body1.addChildElement('deactivationDate', objConf.API_URL__c, null).addTextNode(dtCanceldate.Year() +'-'+dtCanceldate.month()+'-'+dtCanceldate.day());
            system.debug('Testing Deactivate : '+ doc.toXmlString());
            return doc;
        }catch(Exception objExp){
            System.debug('Exception occured in MediaTraxHTTPCalloutHandlerController_V1 class in method generateDOMObjectForTrackingDeacivate - Message '+objExp.getMessage()+objExp.getStackTraceString());
            throw new CustomException('Error Type : '+objExp.getTypename()+' Error Message : '+objExp.getMessage(),objExp.getStackTraceString());
        }
    }
}