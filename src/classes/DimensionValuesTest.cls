@isTest(seeAllData=true)
public with sharing class DimensionValuesTest {
    static testMethod void dimtest(){
         Test.startTest();
        c2g__codaDimension1__c dimension1 = TestMethodsUtility.createDimension1('unittest', 'unittest');
        //c2g__codaDimension2__c dimension2 = TestMethodsUtility.createDimension2(new Product2(Name = 'test', RGU__c = 'Print'));
        //c2g__codaDimension3__c dimension3 = TestMethodsUtility.createDimension3(new Order_Line_Items__c(Billing_Partner__c = CommonMessages.berryTelcoName));
        c2g__codaDimension4__c dimension4 = TestMethodsUtility.createDimension4();
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = CommonMessages.printRGU;
        insert newProduct;
            
        List<Order_Line_Items__c> OLIList = new List<Order_Line_Items__c>();
        Order_Line_Items__c OLI = new Order_Line_Items__c(Product2__c = newProduct.Id, Is_P4P__c = true);
        OLIList.add(OLI);
        
        map<String, set<String>> dimValues = DimensionsValues.getDimensionValuesByOLI(OLIList); 
        Map<String, Id> dimRepCodIdMap = DimensionsValues.getDimensionByOLI(OLIList);
        Set<String> testset = new Set<String>{'unittest', 'unittest'};
        DimensionsValues.dimension1Values(testset);
        Map<String, Id> dimensionsMapValues  = DimensionsValues.dimension2Values(testset);
       // OLI.CMR_Name__c='unit test';
       // OLI.Billing_Partner__c = CommonMessages.BerryForDimension;
        //DimensionsValues.assignDimensions(OLI, new c2g__codaCreditNote__c(), new c2g__codaCreditNoteLineItem__c(), new c2g__codaInvoiceLineItem__c(), testset);
        Test.stopTest();
        
    }
}