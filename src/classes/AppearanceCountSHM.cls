public class AppearanceCountSHM{
    public static void IncreaseDecreaseAppearancecount(list<Directory_listing__c> lstDL, map<Id, Directory_listing__c> oldMap) {
        if(CommonVariables.AppearanceLogic) {
           
            map<String,list<Directory_listing__c>> mapIncreaseStrobjDL = new map<String,list<Directory_listing__c>>();
            map<String,list<Directory_listing__c>> mapDecreaseStrobjDL = new map<String,list<Directory_listing__c>>();
            
            //set<String> setComboValue = new set<String>();
            set<Id> setDirectorySectionId = new set<Id>();
            set<Id> setDirectoryHeadingId = new set<Id>();
            system.debug('********DL List size*********'+lstDL.size());
            for(Directory_listing__c objDL : lstDL) {
                if(objDL.Section_Page_type__c == 'YP' && objDL.Directory_section__c != null && objDL.Directory_Heading__c != null) {
                    boolean bFlag = true;
                    if(oldMap != null) {
                        bFlag = false;
                        Directory_listing__c objOldDL = oldMap.get(objDL.Id);
                        if(objOldDL.Directory_section__c != objDL.Directory_section__c || objOldDL.Directory_Heading__c != objDL.Directory_Heading__c) {
                            bFlag = true;
                            String objDLDirSHStrOld;
                            if(objOldDL.Directory_section__c != null && objOldDL.Directory_Heading__c != null){
                                objDLDirSHStrOld = String.valueOf(objOldDL.Directory_section__c).substring(0, 15) + String.valueOf(objOldDL.Directory_Heading__c).substring(0, 15);
                            }
                            //setDecreaseAppearances.add(objDLDirSHStrOld);
                            if(!mapDecreaseStrobjDL.containsKey(objDLDirSHStrOld)){
                                mapDecreaseStrobjDL.put(objDLDirSHStrOld, new list<Directory_listing__c>());
                            }
                            mapDecreaseStrobjDL.get(objDLDirSHStrOld).add(objDL);
                            //setComboValue.add(objDLDirSHStrOld);
                            setDirectorySectionId.add(objOldDL.Directory_section__c);
                            setDirectoryHeadingId.add(objOldDL.Directory_Heading__c);
                        }
                    }
                    if(bFlag) {
                        string objDLDirSHStr = String.valueOf(objDL.Directory_section__c).substring(0, 15) + String.valueOf(objDL.Directory_Heading__c).substring(0, 15);                     
                        system.debug('********String combination********'+objDLDirSHStr);
                        //setComboValue.add(objDLDirSHStr);
                        setDirectorySectionId.add(objDL.Directory_section__c);
                        setDirectoryHeadingId.add(objDL.Directory_Heading__c);
                        //setIncreaseAppearances.add(objDLDirSHStr);
                        if(!mapIncreaseStrobjDL.containsKey(objDLDirSHStr)){
                            mapIncreaseStrobjDL.put(objDLDirSHStr, new list<Directory_listing__c>());
                        }
                        mapIncreaseStrobjDL.get(objDLDirSHStr).add(objDL);
                    
                    }
                }
                    
                
            }
            system.debug('********Increase value map**********'+mapIncreaseStrobjDL.size());
            system.debug('********decrease value map**********'+mapDecreaseStrobjDL.size());
            map<string,Section_Heading_Mapping__c> mapSHM = new map<string,Section_Heading_Mapping__c>();
            if(setDirectorySectionId.size() > 0 && setDirectoryHeadingId.size() > 0 ) {
                for(Section_Heading_Mapping__c objSHM : SectionHeadingMappingSOQLMethods.getSecHeadMapByDirSecDirHeadIds(setDirectorySectionId,setDirectoryHeadingId)) {
                    system.debug('********String combination SHM********'+objSHM.strBundleAppearenceCombo__c);
                    if(!mapSHM.containsKey(objSHM.strBundleAppearenceCombo__c)) {
                        mapSHM.put(objSHM.strBundleAppearenceCombo__c,objSHM);
                    }
                }
                
                if(mapSHM.size() > 0) {
                    list<Section_Heading_Mapping__c> objSHMUpdateLst = new list<Section_Heading_Mapping__c>();
                    for(string objStr : mapSHM.keyset()) {
                        if(mapIncreaseStrobjDL.get(objStr) != null) {
                            Section_Heading_Mapping__c objSecHmInc = mapSHM.get(objStr);
                            for(integer i=1;i<=mapIncreaseStrobjDL.get(objStr).size();i++){
                            system.debug('********Increase value map count**********'+i);
                                //Section_Heading_Mapping__c objSecHmInc = mapSHM.get(objStr);
                                if(objSecHmInc.Appearance_Count__c == null || objSecHmInc.Appearance_Count__c == 0){
                                    objSecHmInc.Appearance_Count__c = 1;
                                }
                                else {
                                    objSecHmInc.Appearance_Count__c = objSecHmInc.Appearance_Count__c + 1;
                                }
                                if(objSecHmInc.Pagination_Appearance_Count__c == null || objSecHmInc.Pagination_Appearance_Count__c == 0){
                                    objSecHmInc.Pagination_Appearance_Count__c = 1;
                                }
                                else {
                                    objSecHmInc.Pagination_Appearance_Count__c = objSecHmInc.Pagination_Appearance_Count__c + 1;
                                }
                                //objSHMUpdateLst.add(objSecHmInc);
                            }
                            objSHMUpdateLst.add(objSecHmInc);
                        }
                        else if(mapDecreaseStrobjDL.get(objStr) != null) {
                            Section_Heading_Mapping__c objSecHmDec = mapSHM.get(objStr);
                            for(integer i=1;i<=mapDecreaseStrobjDL.get(objStr).size();i++){
                            system.debug('********decrease value map count**********'+i);
                                //Section_Heading_Mapping__c objSecHmDec = mapSHM.get(objStr);
                                if(objSecHmDec.Appearance_Count__c == 0 || objSecHmDec.Appearance_Count__c == null ){
                                    objSecHmDec.Appearance_Count__c = 0;
                                }
                                else {
                                    objSecHmDec.Appearance_Count__c = objSecHmDec.Appearance_Count__c - 1;
                                }
                                if(objSecHmDec.Pagination_Appearance_Count__c == 0 || objSecHmDec.Pagination_Appearance_Count__c == null){
                                    objSecHmDec.Pagination_Appearance_Count__c = 0;
                                }
                                else {
                                    objSecHmDec.Pagination_Appearance_Count__c = objSecHmDec.Pagination_Appearance_Count__c - 1;
                                }
                                //objSHMUpdateLst.add(objSecHmDec);
                            }
                            objSHMUpdateLst.add(objSecHmDec);
                        }
                    }
                    system.debug('********SHM records list**********'+objSHMUpdateLst);
                    if(objSHMUpdateLst.size() > 0) {
                        update objSHMUpdateLst;
                        CommonVariables.AppearanceLogic = false;
                    }
                }
            }
        }
    }
}