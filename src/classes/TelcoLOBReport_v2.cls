public with sharing class TelcoLOBReport_v2 {
public string[] partners;
public string entity;
public string directories;
public string directoriesEdition;
public String selectedtelco{get;set;}
public String selectedentity{get;set;}
public String selecteddirectories{get;set;}
public String selecteddirectoriesEdition{get;set;}
public String selectedReporttype {get;set;}
public boolean disablebtn{get;set;}
String Reporttype;
public String nameFile { get; set; }
public String startdatename{get; set;}
public String enddatename{get; set;}
public String prepdatename{get; set;}
public boolean showexport{get;set;}
public String telcoProduct1{get;set;}
public Boolean IsIncludeNational1{get;set;}
public Boolean IsIncludeCredit1{get;set;}
public Boolean IsIncludeNational2{get;set;}
public Boolean IsIncludeCredit2{get;set;}
transient  public List<Order_Line_Items__c> OLIs{get;set;}
transient  public Map<ID,c2g__codaCreditNote__c > SI_CreditNoteMap;
public boolean DEshow{get;set;}
transient  public List<DigitalInvoiceWrapper> DigitalWrap;
transient  public List<TotalBillingWrapper> TotalBillWrap{get;set;}
transient  public List<DigitalBPSectionWrapper> DBPWrap{get;set;}
transient  public boolean showresults{get;set;}
transient  public Double GrandTotalBilling{get;set;}
transient  public Double GrandTotalSCNBilling{get;set;}
transient  public Integer TotalBillingItems{get;set;}
transient  public Integer TotalSCNBillingItems{get;set;}
public Date billdatename{get;set;}
public TelcoLOBReport_v2()
{
   showexport=false;
   OLIs=new List<Order_Line_Items__c>();
   DEshow=true;
   showresults=false;
   disablebtn=true;
}
public void populateEdition()
{
  getdirectoryEdition();
  DEshow=false;
}
public List<SelectOption> getdirectoryEdition() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('0','--None--'));
            for(Directory_Edition__c tl:[Select id,Name from Directory_Edition__c where Directory__c=:this.selecteddirectories Order By Name ASC])
            {
             options.add(new SelectOption(tl.id,tl.Name));
            }
            
            return options;
        }
       public String getdirectoriesEdition() {
           return directoriesEdition;
        }  
    public void setdirectoriesEdition(String directoriesEdition) {
            this.directoriesEdition= directoriesEdition;
            this.selecteddirectoriesEdition=directoriesEdition;
           
        }
public List<SelectOption> getbillingpartners(){
            List<SelectOption> options = new List<SelectOption>();
            if(selecteddirectories !=null && selecteddirectoriesEdition!=null)
            {
                for(AggregateResult agr : [Select Billing_Partner__c BillingPartner from Order_Line_Items__c where Directory__c=:selecteddirectories  AND Directory_Edition__c=:selecteddirectoriesEdition Group By Billing_Partner__c  Order By Billing_Partner__c ASC]){
                    
                    if(agr.get('BillingPartner')!=null)
                    options.add(new SelectOption((String)agr.get('BillingPartner'),(String)agr.get('BillingPartner')));
                }
            }
            return options;
}
public List<SelectOption> getbillingEntity() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('0','--None--'));
            Schema.DescribeFieldResult BillingEntityResult = Canvass__c.Billing_Entity__c.getDescribe();
            List<Schema.PicklistEntry> BillingEntityList = BillingEntityResult.getPicklistValues();
            for(Schema.PicklistEntry tl: BillingEntityList)
            {
             options.add(new SelectOption(tl.getValue(),tl.getValue()));
            }
            
            return options;
        }
        public String getentity() {
           return entity;
        }
            
        public void setentity(String entity) {
            this.selectedentity=entity;
        }
public void getTelcoDate()
{
     
     system.debug('------------GET TELCODATE --LINE 100---');
     
     billdatename=null;
     if(selectedentity !=null)
     {
         if(Reporttype=='Digital')
         {
             List<Digital_Telco_Scheduler__c> telcoList=new List<Digital_Telco_Scheduler__c>();
             system.debug('---selectedentity-----'+selectedentity);
             telcoList=[SELECT Billing_Entity__c,Id,Invoice_From_Date__c,Bill_Prep_Date__c,Invoice_To_Date__c,Name,OwnerId,Sent_Telco_File__c,
             			Telco_Bill_Date__c,Telco_Receives_EFile__c,Telco__c,XML_Output_Total_Amount__c FROM Digital_Telco_Scheduler__c 
             			where Billing_Entity__c=:selectedentity AND Invoice_From_Date__c!=null AND Invoice_To_Date__c!=null Limit 1];
             if(telcoList.size()>0)
             {
                 Digital_Telco_Scheduler__c tlcConfig = telcoList[0];
                 billdatename=tlcConfig.Telco_Bill_Date__c;
                 startdatename=String.valueof(tlcConfig.Invoice_From_Date__c.format());
                 enddatename=String.valueof(tlcConfig.Invoice_To_Date__c.format());
                 selectedtelco=tlcConfig.Telco__c;
                 prepdatename=String.valueof(tlcConfig.Bill_Prep_Date__c.format());
                 disablebtn=false;
                 showresults=false;
             }
             else
             {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'No record found in Digital Telco Scheduler for selected telco');
                 ApexPages.addMessage(myMsg);
                 billdatename=null;
                 startdatename='';
                 enddatename='';
                 prepdatename='';
                 showresults=false;
                 //disablebtn=true;
             }
          } 
     }
     
}       
public List<SelectOption> getdirectory() {
           
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('--None--','--None--'));
        for(Directory__c tl:[Select id,Name from Directory__c Order By Name ASC])
        {
        options.add(new SelectOption(tl.id,tl.Name));
        }
        
        return options; 
   }
        
    public String getdirectories() {
       return directories;
    }
        
    public void setdirectories(String directories) {
        this.directories= directories;
        this.selecteddirectories=directories;
       
    }
        
    public List<SelectOption> getReporttypeOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('0','--None--'));
        options.add(new SelectOption('Digital','Digital'));
         options.add(new SelectOption('Print','Print'));
        return options;
    }           
    public String[] getpartners() {
       return partners;
    }
        
    public void setpartners(String[] partners) 
    {
        this.partners= partners;
    }
    
     public String getReporttype() {
     return Reporttype;
    }
    
     public void Chk() 
     {
        if(Reporttype=='Print')
        {
          IsIncludeCredit2=false;
          showresults=false;
          disablebtn=false;
          showexport=false;
        }
        if(Reporttype=='Digital')
        {
          IsIncludeNational2=false;
          disablebtn=false;
          
        }
        
     }
     public void setReporttype(String Reporttype) {
        
        this.Reporttype= Reporttype;
        this.selectedReporttype=Reporttype;
        
    }
    
    public PageReference generateReport() {   
        showexport=true;
        if(selectedReporttype=='Print') {
            if(partners.size()>0) {
            String slist='';
            for (String s: partners) {
                slist += s +',';
            }
            slist = slist.substring (0, slist.length() -1);
            PageReference pg=new PageReference('/apex/PrintLOBReport_v1?BillingPartner='+slist+'&directoryId='+selecteddirectories+'&IsNational='+IsIncludeNational2+'&Edition='+selecteddirectoriesEdition);
            pg.setRedirect(true);
            return pg;
            }
            else {
                PageReference pg=new PageReference('/apex/PrintLOBReport_v1?directoryId='+selecteddirectories+'&IsNational='+IsIncludeNational2+'&Edition='+selecteddirectoriesEdition);
                pg.setRedirect(true);
                return pg;
            }
        }
        else if(selectedReporttype=='Digital') {
            showresults=true;
            generateDigitalReport(selectedentity,startdatename,enddatename);
            // generateDigitalReportWithSalesCredit(selectedentity,startdatename,enddatename);
            return null;
        }
        else {
            showresults=false;
            showexport=false;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'No Records Found. Refine your filter to view results.');
            ApexPages.addMessage(myMsg);
            return null;
        }
    }
    
public void generateDigitalfromURL()
{
  Reporttype='Digital';
  showresults=true;
  showexport=true;
  entity=Apexpages.currentpage().getparameters().get('en');
  startdatename=Apexpages.currentpage().getparameters().get('sdate');
  enddatename=Apexpages.currentpage().getparameters().get('edate');
  generateDigitalReport(entity,startdatename,enddatename);
 // generateDigitalReportWithSalesCredit(entity,startdatename,enddatename);
}

public void generateDigitalReport(String selectedentity,String startdatename,String enddatename)
{
    set<id> InvoiceIds=new Set<Id>();
    DigitalWrap=new List<DigitalInvoiceWrapper>();
    TotalBillWrap=new List<TotalBillingWrapper>();
    DBPWrap=new List<DigitalBPSectionWrapper>();
    map<String,List<c2g__codaInvoiceLineItem__c>> mapStrCompareInvoices=new map<String,List<c2g__codaInvoiceLineItem__c>>();
    SI_CreditNoteMap=new Map<ID,c2g__codaCreditNote__c> ();
    map<String, list<DigitalInvoiceWrapper>> mapDIWs= new map<String, list<DigitalInvoiceWrapper>>();
    map<String,list<DigitalInvoiceWrapper>> mapBPInvoiceWrap =new map<String,list<DigitalInvoiceWrapper>>();
    //get all the records from the codaInvoiceLineItem based upon the entity type and  and dates
     //************ find whether all the fields needed or not
    for(c2g__codaInvoiceLineItem__c InvoiceIterator: [SELECT c2g__NetValue__c,Order_Line_Item__r.Account__c,Order_Line_Item__r.Account__r.X3l_External_ID__c, 
    Order_Line_Item__r.Account__r.Parent_3L_External_ID__c, Order_Line_Item__r.Account__r.Account_Number__c,Order_Line_Item__r.Account__r.Parent.Name,
    Order_Line_Item__r.Account__r.Parent.Account_Number__c,Order_Line_Item__r.Account__r.Name,c2g__Invoice__r.Name,Order_Line_Item__r.Account__r.Phone,
    c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,Order_Line_Item__r.Listing__r.Name,
        c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,Order_Line_Item__r.Billing_Contact__r.Phone,Order_Line_Item__r.UnitPrice__c,
        Order_Line_Item__r.Order_Line_Total__c,Order_Line_Item__r.name,c2g__Invoice__r.Customer_Name__r.Name,c2g__Invoice__r.c2g__Account__c,
        c2g__Invoice__r.c2g__Account__r.Name,c2g__Invoice__r.c2g__Account__r.Berry_ID__c,  c2g__Invoice__r.c2g__Account__r.Phone,
        c2g__Invoice__r.c2g__Account__r.Billing_Phone__c,Order_Line_Item__r.Service_Start_Date__c,Order_Line_Item__r.Product2__r.Name,
        Order_Line_Item__r.Service_End_Date__c,Order_Line_Item__r.Talus_Go_Live_Date__c,Order_Line_Item__r.ProductCode__c,Order_Line_Item__r.Product2__c,
        Order_Line_Item__r.Canvass__r.Name,Order_Line_Item__r.Listing_Name__c,Order_Line_Item__r.Directory__c,
        Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.Billing_Frequency__c,Order_Line_Item__r.Line_Number__c,Order_Line_Item__r.Telco__r.Name,
        Order_Line_Item__r.Product2__r.ProductCode,Order_Line_Item__r.Payment_Method__c,Order_Line_Item__r.RecordtypeId,c2g__Invoice__c,
        Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c,Order_Line_Item__r.Digital_Product_Requirement__r.Advertised_Phone_Number__c FROM c2g__codaInvoiceLineItem__c 
        where c2g__Invoice__r.c2g__InvoiceDate__c >=: Date.parse(startdatename)  AND c2g__Invoice__r.c2g__InvoiceDate__c <=: Date.parse(enddatename) AND 
        Order_Line_Item__r.Product2__r.Media_Type__c != 'Print' AND Order_Line_Item__r.Canvass__r.Billing_Entity__c = :selectedentity Order By 
        Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.Account__r.Name])
         {
            String strCompare='';
            string ParentAccNum='';
            string btn='';
            string bp='';
            string cnvName='';
            string invDt='';
            string bf='';
            string ttype='';
            string ExtId='';
            string ParentExtId='';
            //check whether any of the field are there or not
            /*
            Account number
            Parent Account Number
            Billing contact Phone
            Billing Partner
            Order line item canvass name
            invoice date
            Billing frequency
            trancastion type
            Ecxternal ID
            Parnet 3l externalId
            
            so for the corresponding parameter the invoice is added here
            */
            /*
            commented by Bona
            
            if(InvoiceIterator.Order_Line_Item__r.Account__r.Account_Number__c != null)  
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.Account_Number__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Account__r.Parent.Account_Number__c != null)  
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.Parent.Account_Number__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Billing_Contact__r.Phone != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Billing_Contact__r.Phone).trim();
            if(InvoiceIterator.Order_Line_Item__r.Billing_Partner__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Billing_Partner__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Canvass__r.Name != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Canvass__r.Name).trim();
            if(InvoiceIterator.c2g__Invoice__r.c2g__InvoiceDate__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.c2g__Invoice__r.c2g__InvoiceDate__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Billing_Frequency__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Billing_Frequency__c).trim();
            if(InvoiceIterator.c2g__Invoice__r.Transaction_Type__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.c2g__Invoice__r.Transaction_Type__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Account__r.X3l_External_ID__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.X3l_External_ID__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Account__r.Parent_3L_External_ID__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.Parent_3L_External_ID__c).trim();*/
          
           
          /*  if(InvoiceIterator.Order_Line_Item__r.Account__r.Parent.Account_Number__c!=null)  ParentAccNum=String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.Parent.Account_Number__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Billing_Contact__r.Phone!=null) btn=String.valueof(InvoiceIterator.Order_Line_Item__r.Billing_Contact__r.Phone).trim();
            if(InvoiceIterator.Order_Line_Item__r.Billing_Partner__c!=null) bp=String.valueof(InvoiceIterator.Order_Line_Item__r.Billing_Partner__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Canvass__r.Name!=null) cnvName=String.valueof(InvoiceIterator.Order_Line_Item__r.Canvass__r.Name).trim();
            if(InvoiceIterator.c2g__Invoice__r.c2g__InvoiceDate__c!=null) invDt=String.valueof(InvoiceIterator.c2g__Invoice__r.c2g__InvoiceDate__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Billing_Frequency__c!=null) bf=String.valueof(InvoiceIterator.Order_Line_Item__r.Billing_Frequency__c).trim();
            if(InvoiceIterator.c2g__Invoice__r.Transaction_Type__c!=null) ttype=String.valueof(InvoiceIterator.c2g__Invoice__r.Transaction_Type__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Account__r.X3l_External_ID__c!=null) ExtId=String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.X3l_External_ID__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Account__r.Parent_3L_External_ID__c!=null) ParentExtId=String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.Parent_3L_External_ID__c).trim();
          */  
            InvoiceIds.add(InvoiceIterator.c2g__Invoice__c);
            
           // strCompare= ParentAccNum.tolowercase()+btn.tolowercase()+bp.tolowercase()+cnvName.tolowercase()+invDt.tolowercase()+bf.tolowercase()+ttype.tolowercase()+ExtId.tolowercase()+ParentExtId.tolowercase();
            if(!mapStrCompareInvoices.containskey(InvoiceIterator.Order_Line_Item__r.Billing_Partner__c)){
                 mapStrCompareInvoices.put(InvoiceIterator.Order_Line_Item__r.Billing_Partner__c,new list<c2g__codaInvoiceLineItem__c>());
            }
            mapStrCompareInvoices.get(InvoiceIterator.Order_Line_Item__r.Billing_Partner__c).add(InvoiceIterator);
      }
     //get all the invoice details 
     system.debug('-----mapstrCompareInvoices------'+mapStrCompareInvoices.keyset());
    System.debug('InvoiceIds#######'+InvoiceIds.size());
    for(c2g__codaCreditNote__c cr:[Select c2g__Invoice__c,Transaction_Type__c, Name,c2g__NetTotal__c from c2g__codaCreditNote__c where c2g__Invoice__c in : InvoiceIds]) {
            SI_CreditNoteMap.put(cr.c2g__Invoice__c,cr);
    }
    
    if(mapStrCompareInvoices.size()>0) {
    System.debug('########mapStrCompareInvoices###'+mapStrCompareInvoices);
        for(String strIterator : mapStrCompareInvoices.keyset()) {
            Double BillingAmount=0.0;
            String CanvasName=null;
            String AccountName=null;
            String ListingName=null;
            String TelcoName=null;
            String  AccountNumber=null;
            String  ParentAccount=null;
            String ParentAccountNum=null;
            String  BillingPhone=null;
            String  ListedPhone=null;
            String  Billingpartner=null;
            String  Canvass=null;
            String  InvoiceDate=null;
            String  BillingFrequency=null;
            String  ServiceStartDate=null;
            String  ServiceEndDate=null;
            String  SalesInvoice=null;
            String  TransactionType=null;
            String  X3lExternal = null;
            String  Parent2lExternal = null;
            Id InvoiceId=null;
            
            //get all the invoice of the correspong parameter either the account number/invoice number so on
            for(c2g__codaInvoiceLineItem__c InvoiceIterator : mapStrCompareInvoices.get(strIterator)) {
                InvoiceId = InvoiceIterator.c2g__Invoice__c;
                BillingAmount = BillingAmount+InvoiceIterator.c2g__NetValue__c;
                if(InvoiceIterator.Order_Line_Item__r.Canvass__r.Name != null) {
                    CanvasName=InvoiceIterator.Order_Line_Item__r.Canvass__r.Name.escapeCSV();
                }
                if(InvoiceIterator.Order_Line_Item__r.Account__r.Name!=null) {
                    AccountName=InvoiceIterator.Order_Line_Item__r.Account__r.Name.escapeCSV();
                }
                if(InvoiceIterator.Order_Line_Item__r.Telco__r.Name!=null) {
                    TelcoName=InvoiceIterator.Order_Line_Item__r.Telco__r.Name.escapeCSV();
                } 
                if(InvoiceIterator.Order_Line_Item__r.Listing__r.Name!=null) {
                    ListingName=InvoiceIterator.Order_Line_Item__r.Listing__r.Name.escapeCSV();
                }
                BillingFrequency = InvoiceIterator.Order_Line_Item__r.Billing_Frequency__c;
                InvoiceDate = String.valueof(InvoiceIterator.c2g__Invoice__r.c2g__InvoiceDate__c);
                AccountNumber = InvoiceIterator.Order_Line_Item__r.Account__r.Account_Number__c;
                ParentAccount = InvoiceIterator.Order_Line_Item__r.Account__r.Parent.Name;
                ParentAccountNum = InvoiceIterator.Order_Line_Item__r.Account__r.Parent.Account_Number__c;
                BillingPhone = InvoiceIterator.Order_Line_Item__r.Billing_Contact__r.Phone;
                ListedPhone = InvoiceIterator.Order_Line_Item__r.Digital_Product_Requirement__r.Advertised_Phone_Number__c;
                Billingpartner = InvoiceIterator.Order_Line_Item__r.Billing_Partner__c;
                ServiceStartDate = String.valueof(InvoiceIterator.Order_Line_Item__r.Service_Start_Date__c);
                ServiceEndDate = String.valueof(InvoiceIterator.Order_Line_Item__r.Service_End_Date__c);
                SalesInvoice = InvoiceIterator.c2g__Invoice__r.Name;
                TransactionType = InvoiceIterator.c2g__Invoice__r.Transaction_Type__c;                            
                X3lExternal = InvoiceIterator.Order_Line_Item__r.Account__r.X3l_External_ID__c;
                Parent2lExternal = InvoiceIterator.Order_Line_Item__r.Account__r.Parent_3L_External_ID__c;
            }
            
            //get a wrapper for that class
            if(BillingAmount>0)
            DigitalWrap.add(new DigitalInvoiceWrapper(InvoiceId,BillingAmount,AccountName,AccountNumber,ParentAccount,ParentAccountNum,BillingPhone,ListedPhone,BillingPartner,CanvasName,InvoiceDate, BillingFrequency,TelcoName,SalesInvoice, TransactionType,ServiceStartDate,ServiceEndDate,false, X3lExternal, Parent2lExternal,ListingName)); 
        }
       System.debug('@@@@@DigitalWrap@@@@@@@@@'+DigitalWrap); 
       //based on the billing partner ,again invoicemap is created
        if(DigitalWrap.size()>0) {
           for(DigitalInvoiceWrapper DIW: DigitalWrap) {
               if(!mapBPInvoiceWrap.containskey(DIW.BillingPartner)) 
               {
                    mapBPInvoiceWrap.put(DIW.BillingPartner, new List<DigitalInvoiceWrapper>());
               }
               mapBPInvoiceWrap.get(DIW.BillingPartner).add(DIW);               
           }
           System.debug('################'+mapBPInvoiceWrap);
           
           for(String strIterator : mapBPInvoiceWrap.keyset()) {
               for(DigitalInvoiceWrapper  Iterator : mapBPInvoiceWrap.get(strIterator)) {
                    if(SI_CreditNoteMap.containskey(Iterator.InvoiceId)) {
                           Double SCNAmount=0.00;
                           if(SI_CreditNoteMap.get(Iterator.InvoiceId).c2g__NetTotal__c>0) {
                                SCNAmount=0-SI_CreditNoteMap.get(Iterator.InvoiceId).c2g__NetTotal__c;
                           }
                           else {
                                SCNAmount=SI_CreditNoteMap.get(Iterator.InvoiceId).c2g__NetTotal__c;
                           }
                        DigitalInvoiceWrapper diw = new DigitalInvoiceWrapper();
                        diw.InvoiceId=Iterator.InvoiceId;
                        diw.BillingAmount=SCNAmount;
                        diw.AccountName=Iterator.AccountName;
                        diw.AccountNum=Iterator.AccountNum;
                        diw.ParentAccountName=Iterator.ParentAccountName;
                        diw.ParentAccountNum=Iterator.ParentAccountNum;
                        diw.BillingPhone=Iterator.BillingPhone;
                        diw.ListedPhone=Iterator.ListedPhone;
                        diw.BillingPartner=Iterator.BillingPartner;
                        diw.CanvasName=Iterator.CanvasName;
                        diw.InvoiceDate=Iterator.InvoiceDate;
                        diw.BillingFrequency=Iterator.BillingFrequency;
                        diw.TelcoName=Iterator.TelcoName;
                        diw.SalesInvoiceNumber= Iterator.SalesInvoiceNumber;
                        diw.TransactionType=SI_CreditNoteMap.get(Iterator.InvoiceId).Transaction_Type__c;
                        diw.ServiceStartDate=Iterator.ServiceStartDate;
                        diw.ServiceEndDate=Iterator.ServiceEndDate;
                        diw.ListingName=Iterator.ListingName;
                        diw.IsSCN=true;
                        mapBPInvoiceWrap.get(strIterator).add(diw);
                    }
               }
           }
           
           if(mapBPInvoiceWrap.size()>0) {
                System.debug('##############'+mapBPInvoiceWrap.keySet());
                for(String strIterator : mapBPInvoiceWrap.keySet()) {
                     if(!mapDIWs.containsKey(strIterator)) {
                            mapDIWs.put(strIterator, new list<DigitalInvoiceWrapper>());
                     }
                     mapDIWs.get(strIterator).addAll(mapBPInvoiceWrap.get(strIterator));
                     
                }
                
                if(mapDIWs.size()>0) {
                    for(String BPs : mapDIWs.keySet()) {
                       DBPWrap.add(new DigitalBPSectionWrapper(BPs,mapDIWs.get(BPs)));
                    }
                    
                    for(DigitalBPSectionWrapper BPSection : DBPWrap) {
                        Integer CountBillItems=0;
                        Integer CountSCNBillItems=0;
                        for(DigitalInvoiceWrapper CNW : BPSection.DIWs)
                        {
                            if(!CNW.IsSCN)
                            {
                                CountBillItems++;
                            }
                            else
                            {
                                CountSCNBillItems++;
                            }
                        }
                        TotalBillWrap.add(new TotalBillingWrapper(BPSection.BillingPartner,CountBillItems,CountSCNBillItems,BPSection.TotalBilling,BPSection.TotalSCNBilling));  
                    }
                    GrandTotalBilling=0;
                    GrandTotalSCNBilling=0;
                    TotalBillingItems=0;
                    TotalSCNBillingItems=0;
                    for( TotalBillingWrapper tbw : TotalBillWrap)
                    {
                        GrandTotalBilling=GrandTotalBilling+tbw.TotalBilling;
                        GrandTotalSCNBilling=GrandTotalSCNBilling+tbw.TotalSCNBilling;
                        TotalBillingItems=TotalBillingItems+tbw.Items;
                        TotalSCNBillingItems=TotalSCNBillingItems+tbw.SCNItems ;
                    }
                    TotalBillWrap.add(new TotalBillingWrapper('Total',TotalBillingItems,TotalSCNBillingItems,GrandTotalBilling,GrandTotalSCNBilling)); 
                    showexport=true;
                }
            }
          
       }
     }
     else
    {
       showresults=false;
       showexport=false;
       ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'No Records Found. Refine your filter to view results.');
       ApexPages.addMessage(myMsg);
    }
}


public class DigitalInvoiceWrapper implements Comparable
    {
        public Double BillingAmount{get; set;}
        public String AccountName{get; set;}
        public String AccountNum{get;set;}
        public String ParentAccountName{get; set;}
        public String ParentAccountNum{get;set;}
        public String BillingPhone{get; set;}
        public String ListingName{get;set;}  
        public String ListedPhone{get; set;} 
        public String BillingPartner{get;set;}
        public String CanvasName{get;set;}
        public String InvoiceDate{get;set;}
        public String BillingFrequency{get; set;}
        public String TelcoName{get;set;}
        public String SalesInvoiceNumber{get;set;} 
        public String TransactionType{get;set;}
        public String ServiceStartDate{get;set;}
        public String ServiceEndDate{get;set;}
        public Id InvoiceId{get;set;}
        public Boolean IsSCN{get;set;}
        public String X3lExternal {get;set;}
        public String Parent2lExternal {get;set;}
        public DigitalInvoiceWrapper(){}
        public DigitalInvoiceWrapper(Id InvoiceId,Double BillingAmount,String AccountName,String AccountNum,String ParentAccount,String ParentAccountNum,String BillingPhone,String ListedPhone,String BillingPartner,String CanvasName,String InvoiceDate, String BillingFrequency,String TelcoName,String SalesInvoice, String TransactionType,String ServiceStartDate,String ServiceEndDate,boolean IsSCN, String X3lExternal, String Parent2lExternal,String ListingName)
        {
                this.InvoiceId=InvoiceId;
                this.BillingAmount=BillingAmount;
                this.AccountName=AccountName;
                this.AccountNum=AccountNum;
                this.ParentAccountName=ParentAccount;
                this.ParentAccountNum=ParentAccountNum;
                this.BillingPhone=BillingPhone;
                this.ListedPhone=ListedPhone;
                this.BillingPartner=BillingPartner;
                this.CanvasName=CanvasName;
                this.InvoiceDate=InvoiceDate;
                this.BillingFrequency=BillingFrequency;
                this.TelcoName=TelcoName;
                this.SalesInvoiceNumber= SalesInvoiceNumber;
                this.TransactionType=TransactionType;
                this.ServiceStartDate=ServiceStartDate;
                this.ServiceEndDate=ServiceEndDate;
                this.IsSCN=IsSCN;
                this.X3lExternal = X3lExternal;
                this.Parent2lExternal = Parent2lExternal;
                this.ListingName=ListingName;
        }
        
        public Integer compareTo(Object ObjToCompare) {           
                DigitalInvoiceWrapper jobsWrapper = (DigitalInvoiceWrapper)ObjToCompare;
                if (BillingPartner == jobsWrapper.BillingPartner) {
                        if (AccountName == jobsWrapper.AccountName) return 0;
                        else if (AccountName > jobsWrapper.AccountName) return 1;
                        else 
                        return -1;
                }
                if (BillingPartner > jobsWrapper.BillingPartner){
                   return 1;
                }
                return -1; 
         }
    }
    
    public class DigitalBPSectionWrapper {
        public String BillingPartner{get;set;}
        public List<DigitalInvoiceWrapper> DIWs{get;set;}
        public Double TotalBilling{get;set;}
        public Double TotalSCNBilling{get;set;}
        public Decimal TBilling{get;set;}
        public Decimal TSCNBilling{get;set;}
        public DigitalBPSectionWrapper(String BillingPartner,List<DigitalInvoiceWrapper> DIWs) {
            this.BillingPartner=BillingPartner;
            this.DIWs=DIWs;
            DIWs.sort();
            TotalBilling=0.00;
            TotalSCNBilling=0.00;
            for(DigitalInvoiceWrapper CNW: DIWs) {
                if(!CNW.IsSCN)
                {
                TotalBilling=TotalBilling+CNW.BillingAmount;

                }
                if(CNW.IsSCN)
                {
                TotalSCNBilling=TotalSCNBilling+CNW.BillingAmount;
                }
            }
            this.TotalBilling=TotalBilling;
            if(TotalBilling!=null) {
                this.TBilling=Decimal.valueof(TotalBilling).setscale(2);
            }
            else {
                this.TBilling=0.00;
            }
            if(TotalSCNBilling!=null) {
                TSCNBilling=Decimal.valueof(TotalSCNBilling).setscale(2);
            }
            else {
                TSCNBilling=0.00;
            }
        }
    }   
     public class TotalBillingWrapper {
       
        public String BillingPartner{get;set;}
        Public Integer Items{get;set;}
        public Double TotalBilling{get;set;}
        public Decimal TBilling{get;set;}
        public Double TotalSCNBilling{get;set;}
        public Decimal TSCNBilling{get;set;}
        public Integer SCNItems {get;set;}
        
        public TotalBillingWrapper(String BillingPartner,Integer Items,Integer SCNItems,Double TotalBilling,Double TotalSCNBilling)
        {
          
          this.BillingPartner=BillingPartner;
          this.Items=Items;
          this.SCNItems=SCNItems;
          this.TotalBilling=TotalBilling;
          this.TotalSCNBilling=TotalSCNBilling;
          if(TotalBilling!=null) {
            this.TBilling=Decimal.valueof(TotalBilling).setscale(2);
          }
          else {
            this.TBilling=0.00;
          }
          if(TotalSCNBilling!=null) {	
            this.TSCNBilling=Decimal.valueof(TotalSCNBilling).setscale(2);
          }
          else {
            this.TSCNBilling=0.00;
          }      
        }    
}                 
}