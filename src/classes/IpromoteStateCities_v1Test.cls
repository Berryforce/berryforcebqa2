@IsTest
public class IpromoteStateCities_v1Test{
    static testmethod void IpromoteStateCitiesTest(){
        
        Account newAccount= TestMethodsUtility.generateCustomerAccount();
        insert newAccount;     
        Contact newContact= TestMethodsUtility.createContact(newAccount);       
        Opportunity newOpportunity= TestMethodsUtility.createOpportunity('new', newAccount);
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOG = TestMethodsUtility.createOrderSet(newAccount,newOrder,newOpportunity);
        Order_Line_Items__c newOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOG);
        insert newOLI;
       
        RecordType rtId = CommonMethods.getRecordTypeDetailsByName('Digital_Product_Requirement__c','Online Display');
        list<Digital_Product_Requirement__c> objDFFLst = new list<Digital_Product_Requirement__c>();
        
        Digital_Product_Requirement__c objDFF = new Digital_Product_Requirement__c();
        objDFF.Account__c = newAccount.Id;
        objDFF.URN_number__c = 4;
        objDFF.OrderLineItemID__c = newOLI.Id;
        objDFF.URN_text__c = '12';
        objDFF.Forenames__c = 'Test 1';
        objDFF.Surname__c = 'Test SurName 2';
        //objDFF.cust_area_code__c = '9998881114';
        objDFF.RecordTypeId = rtId.Id;
        objDFF.audience_scope__c = 'City';
        objDFF.audience_targets__c = 'Albany';
        objDFFLst.add(objDFF);
        insert objDFFLst;
        
        Apexpages.currentPage().getParameters().put('dffid',objDFFLst[0].Id);
        
        string  strIpVal;
        strIpVal= objDFFLst[0].audience_targets__c;
        
        I_Promote_Audience_Targeting__c objIpAT = new I_Promote_Audience_Targeting__c();
        objIpAT.State__c = 'TX';
        objIpAT.Scope__c =  objDFFLst[0].audience_scope__c; 
        objIpAT.Cities__c = 'Barstow';
        objIpAT.Is_Checked__c = true;
        
        insert objIpAT;
        
        Test.startTest();
        IpromoteStatesandCities_V1 objIp = new   IpromoteStatesandCities_V1();
       
        objIP.CountTotalRecords = 0;
        objIp.onLoad();
        objIp.selectedState ='none';
        //objIp.userInputSearch ='Barstow';
        objIp.changeAudienceScope();
        //objIp.totalRecordCount();
        objIp.searchData();
        objIp.addValues();
        objIp.removeSelected();
        objIp.finish();
        objIp.getDisableAddButton();
        objIp.getDisablePrevious();
        objIp.getDisableNext();
        //objIp.Beginning();
        //objIp.Previous();
        objIp.Next();
        
        Test.stopTest();
    }
}