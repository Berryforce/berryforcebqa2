public with sharing class TelcoReversalPostingController {
    
    public InputParameters invoiceSet1{get;set;}
    public InputParameters invoiceSet2{get;set;}
    public InputParameters invoiceSet3{get;set;}
    public InputParameters invoiceSet4{get;set;}
    public InputParameters creditSet1{get;set;}
    public InputParameters creditSet2{get;set;}
    public InputParameters creditSet3{get;set;}  
    public InputParameters creditSet4{get;set;}
    public Boolean showSI{get;set;}
    public Boolean showSCN{get;set;}
    public set<Id> sinBatchIdSet{get;set;}
    public set<id> scnBatchIdSet{get;set;}
    public String strSINPostBatchStatus{get;set;}
    public Boolean SINPostBatchPoller{get;set;}
    public String strSCNPostBatchStatus{get;set;}
    public Boolean SCNPostBatchPoller{get;set;}

    //constructor
    public TelcoReversalPostingController(){
        invoiceSet1=new InputParameters();
        invoiceSet1.batchSize=80;
        invoiceSet2=new InputParameters();
        invoiceSet2.batchSize=80;
        invoiceSet3=new InputParameters();
        invoiceSet3.batchSize=80;
        invoiceSet4=new InputParameters();
        invoiceSet4.batchSize=80;
        creditSet1=new InputParameters();
        creditSet1.batchSize=10;
        creditSet2=new InputParameters();
        creditSet2.batchSize=10;
        creditSet3=new InputParameters();
        creditSet3.batchSize=10;
        creditSet4=new InputParameters();
        creditSet4.batchSize=10;  
        showSI=false;
        showSCN=false;
    }
    
    //Saves Telco Reversal Sales Inovice settings
    public void saveSIPosting(){
        List<SplitBatchJobs__c> splitBatchList=new List<SplitBatchJobs__c>();
        SplitBatchJobs__c spliBatchObj=null;
        String type='Telco Reversal Sales Invoice';
        if(invoiceSet1.startNum!=null && invoiceSet1.endNum!=null && invoiceSet1.startNum!=0 && invoiceSet1.endNum!=0){
            spliBatchObj=CommonMethods.createSplitBatchJob(invoiceSet1.startNum,invoiceSet1.endNum,CommonMethods.returnSettingsName(type,'1'),invoiceSet1.batchSize,null,null,type);
            splitBatchList.add(spliBatchObj);   
        }
        if(invoiceSet2.startNum!=null &&  invoiceSet2.endNum!=null && invoiceSet2.startNum!=0 && invoiceSet2.endNum!=0){
            spliBatchObj=CommonMethods.createSplitBatchJob(invoiceSet2.startNum,invoiceSet2.endNum,CommonMethods.returnSettingsName(type,'2'),invoiceSet2.batchSize,null,null,type);
            splitBatchList.add(spliBatchObj);   
        }
        if(invoiceSet3.startNum!=null && invoiceSet3.endNum!=null && invoiceSet3.startNum!=0 && invoiceSet3.endNum!=0){
            spliBatchObj=CommonMethods.createSplitBatchJob(invoiceSet3.startNum,invoiceSet3.endNum,CommonMethods.returnSettingsName(type,'3'),invoiceSet3.batchSize,null,null,type);
            splitBatchList.add(spliBatchObj);   
        }
        if(invoiceSet4.startNum!=null && invoiceSet4.endNum!=null && invoiceSet4.startNum!=0 && invoiceSet4.endNum!=0){
            spliBatchObj=CommonMethods.createSplitBatchJob(invoiceSet4.startNum,invoiceSet4.endNum,CommonMethods.returnSettingsName(type,'4'),invoiceSet4.batchSize,null,null,type);
            splitBatchList.add(spliBatchObj);   
        }
        
        //Save SplitBatchJobs__c list in custom setting
        if(splitBatchList!=null && splitBatchList.size()>0){  
            upsert splitBatchList Name;
            showSI=true;
        }
    }
    
    //Saves Telco Reversal Sales Credit Note settings
    public void saveSCNPosting(){
        List<SplitBatchJobs__c> splitBatchList=new List<SplitBatchJobs__c>();
        SplitBatchJobs__c spliBatchObj=null;
        String type='Telco Reversal Sales Credit Note';
        if(creditSet1.startNum!=null && creditSet1.endNum!=null && creditSet1.startNum!=0 && creditSet1.endNum!=0){
            spliBatchObj=CommonMethods.createSplitBatchJob(creditSet1.startNum,creditSet1.endNum,CommonMethods.returnSettingsName(type,'1'),creditSet1.batchSize,null,null,type);
            splitBatchList.add(spliBatchObj);   
        }
        if(creditSet2.startNum!=null &&  creditSet2.endNum!=null && creditSet2.startNum!=0 && creditSet2.endNum!=0){
            spliBatchObj=CommonMethods.createSplitBatchJob(creditSet2.startNum,creditSet2.endNum,CommonMethods.returnSettingsName(type,'2'),creditSet2.batchSize,null,null,type);
            splitBatchList.add(spliBatchObj);   
        }
        if(creditSet3.startNum!=null && creditSet3.endNum!=null && creditSet3.startNum!=0 && creditSet3.endNum!=0){
            spliBatchObj=CommonMethods.createSplitBatchJob(creditSet3.startNum,creditSet3.endNum,CommonMethods.returnSettingsName(type,'3'),creditSet3.batchSize,null,null,type);
            splitBatchList.add(spliBatchObj);   
        }
        if(creditSet4.startNum!=null && creditSet4.endNum!=null && creditSet4.startNum!=0 && creditSet4.endNum!=0){
            spliBatchObj=CommonMethods.createSplitBatchJob(creditSet4.startNum,creditSet4.endNum,CommonMethods.returnSettingsName(type,'4'),creditSet4.batchSize,null,null,type);
            splitBatchList.add(spliBatchObj);   
        }
        
        //Save SplitBatchJobs__c list in custom setting
        if(splitBatchList!=null && splitBatchList.size()>0){  
            upsert splitBatchList Name;  
            showSCN=true;
        }
    }
    
    //Posting SI batches to Financial Force
    public void postTelcoRevSIBatch(){
        sinBatchIdSet = new set<Id>();
        String postSINBatchId;
        String type='Telco Reversal Sales Invoice';
        if(invoiceSet1.startNum!=null && invoiceSet1.endNum!=null && invoiceSet1.startNum!=0 && invoiceSet1.endNum!=0){
            if(invoiceSet1.batchSize!=null && invoiceSet1.batchSize!=0){
                postSINBatchId=Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch(type,'post',invoiceSet1.startNum,invoiceSet1.endNum),invoiceSet1.batchSize);
            }else{
                postSINBatchId=Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch(type,'post',invoiceSet1.startNum,invoiceSet1.endNum),1);
            }
            sinBatchIdSet.add(postSINBatchId);
        }
        if(invoiceSet2.startNum!=null && invoiceSet2.endNum!=null && invoiceSet2.startNum!=0 && invoiceSet2.endNum!=0){
            if(invoiceSet2.batchSize!=null && invoiceSet2.batchSize!=0){
                postSINBatchId=Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch(type,'post',invoiceSet2.startNum,invoiceSet2.endNum),invoiceSet2.batchSize);
            }else{
                postSINBatchId=Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch(type,'post',invoiceSet2.startNum,invoiceSet2.endNum),1);
            }
            sinBatchIdSet.add(postSINBatchId);
        }
        if(invoiceSet3.startNum!=null && invoiceSet3.endNum!=null && invoiceSet3.startNum!=0 && invoiceSet3.endNum!=0){
            if(invoiceSet3.batchSize!=null && invoiceSet3.batchSize!=0){
                postSINBatchId=Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch(type,'post',invoiceSet3.startNum,invoiceSet3.endNum),invoiceSet3.batchSize);
            }else{
                postSINBatchId=Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch(type,'post',invoiceSet3.startNum,invoiceSet3.endNum),1);
            }
            sinBatchIdSet.add(postSINBatchId);
        }
        if(invoiceSet4.startNum!=null && invoiceSet4.endNum!=null && invoiceSet4.startNum!=0 && invoiceSet4.endNum!=0){
            if(invoiceSet4.batchSize!=null && invoiceSet4.batchSize!=0){
                postSINBatchId=Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch(type,'post',invoiceSet4.startNum,invoiceSet4.endNum),invoiceSet4.batchSize);
            }else{
                postSINBatchId=Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch(type,'post',invoiceSet4.startNum,invoiceSet4.endNum),1);
            }
            sinBatchIdSet.add(postSINBatchId);
        }
        strSINPostBatchStatus ='Status of Telco Reversal Sales Invoice Posting Batch : Queued';
        SINPostBatchPoller = true;  
    }
    
    //Posting SCN Batches to Financial Force
    public void postTelcoRevSCNBatch(){
        scnBatchIdSet = new set<Id>();
        String postSCNBatchId;
        String type='Telco Reversal Sales Credit Note';
        if(creditSet1.startNum!=null && creditSet1.endNum!=null && creditSet1.startNum!=0 && creditSet1.endNum!=0){
            if(creditSet1.batchSize!=null && creditSet1.batchSize!=0){
                postSCNBatchId=Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(type,'post',creditSet1.startNum,creditSet1.endNum),creditSet1.batchSize);
            }else{
                postSCNBatchId=Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(type,'post',creditSet1.startNum,creditSet1.endNum),1);
            }
            scnBatchIdSet.add(postSCNBatchId);
        }
        if(creditSet2.startNum!=null && creditSet2.endNum!=null && creditSet2.startNum!=0 && creditSet2.endNum!=0){
            if(creditSet2.batchSize!=null && creditSet2.batchSize!=0){
                postSCNBatchId=Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(type,'post',creditSet2.startNum,creditSet2.endNum),creditSet2.batchSize);
            }else{
                postSCNBatchId=Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(type,'post',creditSet2.startNum,creditSet2.endNum),1);
            }
            scnBatchIdSet.add(postSCNBatchId);
        }
        if(creditSet3.startNum!=null && creditSet3.endNum!=null && creditSet3.startNum!=0 && creditSet3.endNum!=0){
            if(creditSet3.batchSize!=null && creditSet3.batchSize!=0){
                postSCNBatchId=Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(type,'post',creditSet3.startNum,creditSet3.endNum),creditSet3.batchSize);
            }else{
                postSCNBatchId=Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(type,'post',creditSet3.startNum,creditSet3.endNum),1);
            }
            scnBatchIdSet.add(postSCNBatchId);
        }
        if(creditSet4.startNum!=null && creditSet4.endNum!=null && creditSet4.startNum!=0 && creditSet4.endNum!=0){
            if(creditSet4.batchSize!=null && creditSet4.batchSize!=0){
                postSCNBatchId=Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(type,'post',creditSet4.startNum,creditSet4.endNum),creditSet4.batchSize);
            }else{
                postSCNBatchId=Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(type,'post',creditSet4.startNum,creditSet4.endNum),1);
            }
            scnBatchIdSet.add(postSCNBatchId);
        }
        strSCNPostBatchStatus ='Status of Telco Reversal Sales Credit Note Posting Batch : Queued';
        SCNPostBatchPoller = true;  
    }
    
    public void SINPostJobStatus() {
                
        boolean bStatus = LocalBillingCommonMethods.apexBatchJobStatus(sinBatchIdSet);
        if(bStatus) {
            strSINPostBatchStatus= 'Status of Telco Reversal Sales Invoice Posting Batch : Completed';
            SINPostBatchPoller = false;
        }
        else {
            strSINPostBatchStatus= 'Status of Telco Reversal Sales Invoice Posting Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
    }
    
    public void SCNPostJobStatus(){
        boolean bStatus = LocalBillingCommonMethods.apexBatchJobStatus(scnBatchIdSet);
        if(bStatus) {
            strSCNPostBatchStatus= 'Status of Telco Reversal Sales Credit Note Posting Batch : Completed';
            SCNPostBatchPoller = false;
        }
        else {
            strSCNPostBatchStatus= 'Status of Telco Reversal Sales Credit Note Posting Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
    }
    
    //Wrapper class for input parameters
    public class InputParameters{
        public Integer startNum{get;set;}
        public Integer endNum{get;set;}
        public Integer batchSize{get;set;}

    }
}