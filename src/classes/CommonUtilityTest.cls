@isTest
public class CommonUtilityTest{

    public static testmethod void cmmnUtlty(){
        
    String jsnFld = 'additional_emails';
    Date dt = System.today();
    Set<String> pkgId = new Set<String>{'PKG_IYP001_METAL'};
    Set<String> prdtId = new Set<String>{'IYP0001_METAL'};
    
    Test.StartTest();
    
    CommonUtility.jsonObjFlds(jsnFld);
    CommonUtility.talusDffRT();
    CommonUtility.milesDffRT();
    CommonUtility.yodleDffRT();
    CommonUtility.manualDffRT();
    CommonUtility.lookupFields();
    CommonUtility.utcDate(dt);
    CommonUtility.pkgPrdtPrpts(pkgId, prdtId);
    CommonUtility.dffReadyToSubmit();
    CommonUtility.dffReqHrs();
    CommonUtility.mockGetRspns();
    CommonUtility.dffQueryString();
    CommonUtility.fpQueryString();
    CommonUtility.msgConfirm('TestConfirm');
    CommonUtility.msgInfo('TestInfo');
    CommonUtility.msgWarning('TestWarning');
    CommonUtility.msgError('TestError');
    CommonUtility.msgFatal('TestFatal');
    
    
    Test.StopTest();
    
    }

}