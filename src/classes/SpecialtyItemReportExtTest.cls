@isTest
private class SpecialtyItemReportExtTest{
    static testMethod void test(){
        Test.starttest();
        Canvass__c newCanvass = TestMethodsUtility.createCanvass();
        Account newAccount = TestMethodsUtility.generateAccount('customer', false);
        newAccount.Primary_Canvass__c = newCanvass.Id;
        insert newAccount;
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Pricebook2 newPriceBook = TestMethodsUtility.createpricebook();
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        newProduct.Print_Product_Type__c = CommonMessages.Specialty;
        newProduct.Product_Type__c = 'Print';
        insert newProduct;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Directory__c newDirectory = TestMethodsUtility.createDirectory();
        Directory_Mapping__c newDirectoryMapping = TestMethodsUtility.createDirectoryMapping();
        Directory_Edition__c newDirectoryEdition = TestMethodsUtility.generateDirectoryEdition(newDirectory.Id, newAccount.Primary_Canvass__c, newTelco.Id, newDirectoryMapping.Id);
        newDirectoryEdition.Pub_Date__c = System.Today();
        insert newDirectoryEdition;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        newOrLI.Directory_Edition__c = newDirectoryEdition.Id;      
        newOrLI.Product2__c = newProduct.Id;
        newOrLI.Directory__c = newDirectory.Id;
        insert newOrLI;  
        Line_Item_History__c LIH = TestMethodsUtility.generateLineItemHistory(newOrLI.Id, newDirectoryEdition.Id);
        LIH.UnitPrice__c = 34;
        insert LIH;
        LIH = [SELECT Directory_Edition__r.Year__c, UnitPrice__c, Directory_Edition__c FROM Line_Item_History__c
               WHERE Id = : LIH.Id];
        newOrLI = [Select Product2__c, Product2__r.Print_Specialty_Product_Type__c, Product2__r.ProductCode, Account__r.Name, 
                  Account__r.Account_Number__c, Product2__r.Media_Type__c,
                  Directory_Edition__r.Name, Account__r.Phone, Name, UnitPrice__c, UDAC__c, Directory_Edition__r.Year__c, Id, Directory__c                   
                  From Order_Line_Items__c WHERE Id = : newOrLI.Id];
        SpecialtyItemReportExt SIRE = new SpecialtyItemReportExt(new ApexPages.StandardController(newDirectoryEdition));
        SIRE.exportAsExcel();
        ApexPages.currentPage().getParameters().put('DEId', newDirectoryEdition.Id);        
        SpecialtyItemReportExt SIRE1 = new SpecialtyItemReportExt();
        Test.stoptest();
    }
}