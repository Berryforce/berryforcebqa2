global class ApplyCityService{
    webservice static void communitysectionMethod(string CSAId) {
        list<Community_Section_Abbreviation__c> CSAlst = new list<Community_Section_Abbreviation__c>();
        set<string> setDS = new set<string>();
        set<string> setCity = new set<string>();
        map<string,Community_Section_Abbreviation__c> mapstrCSA = new map<string,Community_Section_Abbreviation__c>();
        CSAlst = [Select Id,Name,Community__c,Community_Abbreviation__c,Community_Name__c,Directory_Section__c,
                  State__c,Suppress_Abbreviation_Rule__c from Community_Section_Abbreviation__c where Id =: CSAId];
        if(CSAlst.size()>0) {
            for(Community_Section_Abbreviation__c iteratorCSA : CSAlst) {
                if(iteratorCSA.Directory_Section__c != null && iteratorCSA.Community_Name__c != null) {
                    mapstrCSA.put(iteratorCSA.Directory_Section__c+iteratorCSA.Community_Name__c,iteratorCSA);
                }
                if(iteratorCSA.Directory_Section__c != null){
                    setDS.add(iteratorCSA.Directory_Section__c);   
                }
                if(iteratorCSA.Community_Name__c != null){
                    setCity.add(iteratorCSA.Community_Name__c);   
                }
            }
        }
        if(setDS.size()> 0) {
            ApplyCityServiceBatchClass apcsBatch= new ApplyCityServiceBatchClass(setDS,setCity,mapstrCSA);
            database.executebatch(apcsBatch);
        }
    }
}