public with sharing class P4PTransactionHandlerController {
    public static void populateCurrentMonthAndPeriodClicks(List<P4P_Transaction__c> listP4Ptrans) {
        Set<Id> OLIIds = new Set<Id>();
        List<Order_Line_Items__c> OLIList = new List<Order_Line_Items__c>();
        Map<Id, List<P4P_Transaction__c>> mapOLIIdP4PTrans = new Map<Id, List<P4P_Transaction__c>>(); 
        Map<Integer, Map<Id, List<P4P_Transaction__c>>> mapOLIMonthmapOLIIdP4PTrans = new Map<Integer, Map<Id, List<P4P_Transaction__c>>>();
        
        for(P4P_Transaction__c p4pTrans : listP4Ptrans) {
            OLIIds.add(p4pTrans.Order_Line_Item__c);
            
            if(!mapOLIIdP4PTrans.containsKey(p4pTrans.Order_Line_Item__c)) {
                mapOLIIdP4PTrans.put(p4pTrans.Order_Line_Item__c, new List<P4P_Transaction__c>());
            }
            mapOLIIdP4PTrans.get(p4pTrans.Order_Line_Item__c).add(p4pTrans);
            
            if(!mapOLIMonthmapOLIIdP4PTrans.containsKey(p4pTrans.Date_of_Click_Call_Lead__c.month())) {
                mapOLIMonthmapOLIIdP4PTrans.put(p4pTrans.Date_of_Click_Call_Lead__c.month(), new Map<Id, List<P4P_Transaction__c>>());
            }
            if(!mapOLIMonthmapOLIIdP4PTrans.get(p4pTrans.Date_of_Click_Call_Lead__c.month()).containsKey(p4pTrans.Order_Line_Item__c)) {
                mapOLIMonthmapOLIIdP4PTrans.get(p4pTrans.Date_of_Click_Call_Lead__c.month()).put(p4pTrans.Order_Line_Item__c, new List<P4P_Transaction__c>());
            }
            mapOLIMonthmapOLIIdP4PTrans.get(p4pTrans.Date_of_Click_Call_Lead__c.month()).get(p4pTrans.Order_Line_Item__c).add(p4pTrans);
        }
        
        if(OLIIds.size() > 0) {
            OLIList = OrderLineItemSOQLMethods.getOLIByIDForP4P(OLIIds);
            
            if(OLIList.size() > 0) {
                for(Order_Line_Items__c OLI : OLIList) {
                    if(OLI.P4P_Current_Billing_Clicks_Leads__c == null)
                        OLI.P4P_Current_Billing_Clicks_Leads__c = 0;
                    OLI.P4P_Current_Billing_Clicks_Leads__c += mapOLIIdP4PTrans.get(OLI.Id).size();
                    
                    if(OLI.Current_Month_for_P4P__c == null || OLI.Current_Month_for_P4P__c != system.today().month()) {
                        OLI.P4P_Current_Months_Clicks_Leads__c = mapOLIIdP4PTrans.get(OLI.Id).size();
                        OLI.Current_Month_for_P4P__c = system.today().month();
                    } else if(mapOLIMonthmapOLIIdP4PTrans.containsKey(Integer.valueOf(OLI.Current_Month_for_P4P__c))) {
                        OLI.P4P_Current_Months_Clicks_Leads__c +=  mapOLIMonthmapOLIIdP4PTrans.get(Integer.valueOf(OLI.Current_Month_for_P4P__c)).get(OLI.Id).size();
                    }
                }
                
                update OLIList;
            }
        }
    }
}