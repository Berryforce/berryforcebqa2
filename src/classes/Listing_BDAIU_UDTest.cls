//DB_Custom_Listing_c Trigger Test class
@isTest(SeeAllData=True)
public class Listing_BDAIU_UDTest{
    static testmethod void dbtest(){
        Test.starttest();
        Account objAccount = TestMethodsUtility.generateAccount('customer');
        insert objAccount;
        Account telcoAccount = TestMethodsUtility.generateAccount('telco');
        insert telcoAccount;
        
        Telco__c objTelco = TestMethodsUtility.createTelco(telcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Contact objContact = TestMethodsUtility.createContact(objAccount);
        insert objContact;
        
        Opportunity objOpportunity = TestMethodsUtility.createOpportunity(objAccount, objContact);
        insert objOpportunity;
        
        Directory__c objDir=TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c=objTelco.Id;
        insert objDir;
        Directory_Section__c objDS = TestMethodsUtility.generateDirectorySection(objDir);
        Directory_Heading__c dirHeading = Testmethodsutility.generateDirectoryHeading(); 
        dirHeading.Directory_Heading_Name__c=Testmethodsutility.generateRandomString(100);
        insert dirHeading;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        insert objProd;
        
        map<Id,listing__c> mapNewLst = new map<Id,listing__c>();
        Listing__c objMListing = TestMethodsUtility.generateListing('Main Listing');
        objMListing.Account__c = objAccount.Id;
        objMListing.Primary_Canvass__c = objAccount.Primary_Canvass__c;
        objMListing.Phone__c = '(999)999-9898';
        insert objMListing;
        mapNewLst.put(objMListing.id,objMListing);
        
        Listing__c objListing = TestMethodsUtility.generateListing('Additional Listing');
        objListing.Account__c = objAccount.Id;
        objListing.Primary_Canvass__c = objAccount.Primary_Canvass__c;
        objListing.Main_Listing__c = objMListing.id;
        insert objListing;
        
        Directory_Listing__c objDListing = TestMethodsUtility.generateNonOverrideRT();
        objDListing.Listing__c=objMListing.Id;
        objDListing.Follows_Listing_Record2__c = 'true';
        objDListing.Phone_Number__c = '(999)999-9898';
        objDListing.Directory__c= objDir.id;
        objDListing.Directory_Section__c = objDS.id;
        objDListing.Directory_Heading__c = dirHeading.id;
        objDListing.Telco_Provider__c=objTelco.Id;
        insert objDListing;
        
        Division__c objDiv = TestMethodsUtility.createDivision();
       
        List<OpportunityLineItem> LstOppLI = TestMethodsUtility.selectOpportunityLineItem(objOpportunity);
        insert LstOppLI;
             
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;
        
        Order_Group__c objOrderGroup = new Order_Group__c (Name = 'Unit Testing', selected__c = true, Order_Account__c = objAccount.ID, Opportunity__c = objOpportunity.ID);                                            
        insert objOrderGroup ;
        
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id);
        objOrderLineItem.Listing__c = objListing.Id;
        insert objOrderLineItem ;
        
        objMListing.Phone__c = '(999)999-9099';
        update objMListing;
        
        ListingToScopedListingSync.syncListingToScopedListing(new Set<Id> {objListing.Id},mapNewLst);
       
        
        Test.stoptest();
    }
}