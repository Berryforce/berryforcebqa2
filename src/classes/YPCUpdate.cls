global class  YPCUpdate implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        set<String> strStatus = new set<String>{'Complete'};
        String query = 'Select Id, Name, YPC_Graphics_URL__c, LoResCopy__c, DFF__c  From YPC_Graphics__c where Miles_Status__c IN:strStatus order by DFF__c';
        return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext bc, List<YPC_Graphics__c> dirListingList) {
         update dirListingList;
     }
     
     global void finish(Database.BatchableContext bc) {
         
     }
}