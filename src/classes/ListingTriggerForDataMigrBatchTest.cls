@isTest
public class ListingTriggerForDataMigrBatchTest {
    static testmethod void test_ListingTriggerForDataMigrBatch() {
        Canvass__c canvass=TestMethodsUtility.createCanvass();
        Lead newLead=new Lead();
        newLead.lastname='Sam';
        newLead.Status='Open';
        newLead.LeadSource='Other';
        newLead.Phone='1234567890';
        newLead.Company='ABC Ltd';
        newLead.Primary_Canvass__c=canvass.Id;
        insert newLead;
        Account newTelcoAccount = TestMethodsUtility.generateTelcoAccount();
        insert newTelcoAccount;
        Telco__c newTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = newTelco.Id;
        insert objDir;*/
        Directory__c objDir=TestMethodsUtility.createDirectory();  
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Directory_Heading__c dirHeading = Testmethodsutility.generateDirectoryHeading(); 
        dirHeading.Directory_Heading_Name__c=Testmethodsutility.generateRandomString(100);
        insert dirHeading;
        Directory_Heading__c dirHeading1 = Testmethodsutility.generateDirectoryHeading(); 
        dirHeading1.Directory_Heading_Name__c=Testmethodsutility.generateRandomString(101);
        insert dirHeading1;
        Listing__c  listing = Testmethodsutility.generateListing('Main Listing');
        listing.Lead__c=newLead.Id;
        listing.Account__c=newTelcoAccount.Id;
        listing.Telco_Provider__c=newTelco.Id;
        insert listing;
        Database.executeBatch(new ListingTriggerForDataMigrBatch('where id!=null'));
        
    }

}