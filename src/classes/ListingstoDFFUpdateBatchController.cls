global class ListingstoDFFUpdateBatchController implements database.batchable<sObject>{
	global string lstngId;
	global ListingstoDFFUpdateBatchController(string lstId){
		lstngId=lstId;
	}
    global database.QueryLocator start(database.batchablecontext bc){
    	set<string> setDFFRTId = new set<string>();
        setDFFRTId.addall(System.Label.DFFRTPrintListingNOContent.split(','));
        string Print = CommonMessages.printMediaType;
    	String strQuery = 'Select Id,Digital_Product_Requirement__c,listing__c,Media_type__c,(select Id,OrderLineItemID__r.Listing__c from Digital_Product_Requirements__r where RecordtypeId IN :setDFFRTId) from Order_line_items__c where Listing__c = :lstngId and Media_type__c=:Print';
    	system.debug('oli Query'+strQuery);
    	return database.getQueryLocator(strQuery);
    }
    global void execute(database.batchablecontext bc,List<Order_line_items__c> lstOLI){
    	system.debug('OLI list:'+lstOLI.size());
    	list<Digital_Product_Requirement__c> lstDFF = new list<Digital_Product_Requirement__c>();
    	map<Id,Listing__c> mapListing = new map<Id,Listing__c>();
    	if(string.isNotBlank(lstngId)){
    		mapListing = ListingSOQLMethods.fetchListingforSLDFFupdate(lstngId);
    	}
    	if(lstOLI.size()>0) {
    		for(Order_line_items__c iteratorOLI :lstOLI){
                if(iteratorOLI.Digital_Product_Requirements__r.size()>0){
                    for(Digital_Product_Requirement__c iteratorDFF : iteratorOLI.Digital_Product_Requirements__r){
                        if(iteratorDFF.OrderLineItemID__r.Listing__c != null){
                            if(mapListing.get(iteratorDFF.OrderLineItemID__r.Listing__c) != null){
                                listing__c objListing = mapListing.get(iteratorDFF.OrderLineItemID__r.Listing__c);
                                iteratorDFF.Caption_Street_Number__c = objListing.Listing_Street_Number__c;
                                iteratorDFF.Caption_Street_Name__c = objListing.Listing_Street__c;
                                iteratorDFF.Caption_PO_Box__c = objListing.Listing_PO_Box__c;
                                iteratorDFF.Caption_City__c = objListing.Listing_City__c;
                                iteratorDFF.Caption_State__c = objListing.Listing_State__c;
                                iteratorDFF.Caption_Phone_Number__c = objListing.Phone__c;
                                iteratorDFF.Caption_Phone_Area_Code__c = objListing.Area_Code__c;
                                iteratorDFF.Caption_Phone_Exchange__c = objListing.Exchange__c;
                                lstDFF.add(iteratorDFF);
                            }
                        }
                    }
                }
        	}
        	system.debug('Listing DFF record'+lstDFF.size());
        	if(lstDFF.size()>0){
    			update lstDFF;
    		}
    	}
    	
    }
    global void finish(Database.BatchableContext bc){
    	listing__c objListing = [Select Id,Lst_SL_DFF_Sync__c from Listing__c where Id=:lstngId Limit 1];
    	objListing.Lst_SL_DFF_Sync__c = false;
    	system.debug('Listing record'+objListing.id);
    	update objListing;
    	String strErrorMessage = '';
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        //String[] toAddresses = new String[] {a.CreatedBy.Email};
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Listing to DFF sync is '+ a.Status, 'The Batch Apex Job processed ' + a.JobItemsProcessed +
          ' batches with '+ a.NumberOfErrors +' failures.');
    }
}