public with sharing class OrderLineItemSOQLMethods {
        
   public static map<Id, Order_Line_Items__c> getOrderLineItemByAccountID(set<Id> accountID) {
        map<Id, Order_Line_Items__c> mapOrderLineItem = new map<Id,Order_Line_Items__c>([Select Name, Order_Anniversary_Start_Date__c,UnitPrice__c, Quantity__c, ProductCode__c, Product2__r.IsBundle__c,  Product2__r.Family, Product2__r.RGU__c,
                                                        Product2__r.IsActive, Product2__r.Description, Product2__r.ProductCode, Product2__r.Name, Product2__r.Id, Product2__c, 
                                                          Payment_Duration__c, Order__c, ListPrice__c, IsCanceled__c, Id, Discount__c, Canvass__c, Billing_Contact__c,  
                                                        Opportunity__r.OwnerId, Opportunity__c, Product_Type__c
                                                        From Order_Line_Items__c where IsCanceled__c = false and Order__r.Account__c IN :accountID order by Product2__c]);
        return mapOrderLineItem;
    }
    
    public static list<Order_Line_Items__c> getOpportunityByAcctIdsforCS(set<Id> accountIDs){
        return [Select Name, Id, Order_Anniversary_Start_Date__c,Account__c, Canvass__c, Canvass__r.Name From Order_Line_Items__c WHERE Account__c IN : accountIDs];
    }
    public static map<Id, Order_Line_Items__c> getOrderLineItemandMoliByID(set<Id> setOrdLineID) {
        return new map<Id, Order_Line_Items__c>([SELECT Id,Order_Anniversary_Start_Date__c,Billing_Close_Date__c,Billing_Contact__c,Billing_End_Date__c,Billing_Frequency__c,Billing_Partner_Account__c,Billing_Partner__c,
                                                Billing_Start_Date__c,Cancelation_Fee__c,Cancellation__c,Canvass__c,Category__c,checked__c,Continious_Billing__c,Current_Billing_Period_Days2__c,
                                                Current_Billing_Period_Days__c,Current_Daily_Prorate__c,Current_Rate__c,Cutomer_Cancel_Date__c,Date_of_Transfer__c,Days_B_W__c,Description__c,Directory__c,Directory_Edition__c,
                                                Directory_Edition__r.Sales_Start__c,Discount__c,Effective_Date__c,Media_Type__c,RecordTypeId,Is_Handled__c,Anchor_Listing_Caption_Header__c,Trademark_Finding_Line_OLI__c,
                                                Fulfilled_on__c,Geo_Code__c,Geo_Type__c,Is_Billing_Cycle__c,Is_P4P__c,Last_successful_settlement__c,Line_Number__c,Listing__c,ListPrice__c,Scoped_Caption_Header__c,
                                                UnitPrice__c,Name,Next_Billing_Date__c,Opportunity__c,Order_Billing_Date_Changed__c,Order__c,OriginalBillingDate__c,Package_ID__c,
                                                Parent_ID__c,Parent_Line_Item__c,Payments_Remaining__c,Payment_Duration__c,Payment_Method__c,Previous_End_Date__c,Previous_Start_Date__c,Product2__c,ProductCode__c,Product_Type__c,
                                                Prorate_Credit_Days__c,Prorate_Credit__c,Prorate_Stored_Value__c,Quote_Signed_Date__c,Quote_signing_method__c,Reason_for_Transfer__c,Scope__c, Directory_Section__c,Sent_to_fulfillment_on__c,
                                                Service_End_Date__c,Service_Start_Date__c,Order_Line_Total__c,Statement_Suppression__c,Status__c,Subscription_ID__c,Successful_Payments__c,Talus_Cancel_Date__c,Directory_Edition__r.Book_Status__c,
                                                Directory_Edition__r.Bill_Prep__c,Directory_Edition__r.New_Print_Bill_Date__c,Order_Group__c,Account__c,Directory_Heading__c, Directory_Heading__r.Name, Directory_Heading__r.code__c, Quantity__c,
                                                Line_Status__c,Last_Billing_Date__c,Directory__r.Pub_Date__c,Directory_Edition__r.Pub_Date__c,Seniority_Date__c,
                                                Talus_DFF_Id__c,Talus_Fulfillment_Date__c,Talus_Go_Live_Date__c,Telco_Invoice_Date__c,Telco__c,Total_Prorate__c,User_who_initiated_the_transfer__c,Vendor_Product_Key__c,Directory_Edition__r.Directory__c, 
                                                (SELECT Id, Name, Billing_Contact__c,Billing_End_Date__c, Billing_Frequency__c, Billing_Partner__c, Billing_Start_Date__c, Cancellation__c, 
                                                Canvass__c, Digital_Product_Requirement__c, Discount__c, Description__c, ListPrice__c, UnitPrice__c,Opportunity__c, 
                                                Order_Line_Item__c, Order__c,Media_Type__c FROM Modification_Order_Line_Items__r)
                                                FROM Order_Line_Items__c 
                                                where ID IN :setOrdLineID]);
    }
    public static map<Id, Order_Line_Items__c> getOrderLineItemByID(set<Id> setOLIID) {
        return new map<Id, Order_Line_Items__c>([Select Opportunity__r.Name,OLI_Original_Package_ID_External__c, Billing_Partner_Account__c, Order__c, Order_Group__c, Opportunity__c, Opportunity_line_Item_id__c, 
                Linvio_Payment_Method_Id__c,Last_Billing_Date__c, BillingTransfered__c, Order_Line_Total__c, Payments_Remaining__c, UnitPrice__c, Talus_Go_Live_Date__c, 
                Product2__r.Family, Product2__r.RGU__c, Payment_Duration__c, Quantity__c, Prorate_Credit_Days__c, BillingTransferedInfo__c, Name, 
                Prorate_Credit__c, Canvass__r.Canvass_Code__c, Directory_Code__c, Edition_Code__c, P4P_Current_Billing_Clicks_Leads__c, Telco__c, 
                Is_P4P__c, P4P_Billing__c, Product2__r.Name, Service_End_Date__c, Service_Start_Date__c, Directory__c, Directory_Edition__c,
                Next_Billing_Date__c, Billing_Frequency__c, Transferred_from__c, Transferred_To__c, Date_of_Transfer__c, Billing_Partner_Transferred__c, 
                Number_of_Months_Transferred__c, Billing_Partner_Change__c, Billing_Partner__c, Payment_Method__c,CMR_Name__c, Billing_Contact__c, Account__c, 
                Package_ID_External__c, Parent_ID__c, Parent_ID_of_Addon__c, ProductCode__c, Media_Type__c, Package_ID__c, Directory_Heading__r.name,
                Directory_Edition__r.name, Directory_Edition__r.Book_Status__c, Status__c, Reason_for_Transfer__c, Order_Anniversary_Start_Date__c, 
                Continious_Billing__c,Canvass__r.Billing_Entity__c, Line_status__c, Id,Successful_Payments__c, Expiration_Date__c,Canvass__c
                FROM Order_Line_Items__c WHERE ID IN :setOLIID]);
    }
    
    public static list<Order_Line_Items__c> getOLIByNationalOrderSet(set<ID> setOSID) {
        return [Select Order_Anniversary_Start_Date__c,Transaction_ID__c, Line_Number__c, ListPrice__c, Date__c, Directory_Code__c, From_C__c, To_P__c, Trans__c, To__c, Publication_Company__c, Publication_Code__c, SPINS__c, RecordTypeId, Product2__c, 
                Order__c, Order_Group__c, NAT__c, NAT_Client_ID__c, From__c, Directory__c, Description__c, DAT__c, Client_Number__c, 
                CMR_Number__c, CMR_Name__c, BAS__c, Advertising_Data__c, Account__c, unitprice__c, 
                Directory_Edition__c, Directory_Edition__r.Name, Trans_Version__c, Publication_Date__c, UDAC__c, Directory__r.Name, account__r.Name, CMR_Name__r.Name, 
                (Select DAT__c, Order_Line_Item__c, Line_Number__c, Advertising_Data__c, BAS__c, SPINS__c, UDAC__c From National_Child_Lines__r)   
                From Order_Line_Items__c where Order_Group__c IN :setOSID];
    }
    
    public static list<Order_Line_Items__c> getOLIByNationalOrderSet_V3(String pubCode, String edtnCode, Id dirId) {
        return [Select Page_Number__c, Order_Anniversary_Start_Date__c,Transaction_ID__c, From_C__c, To_P__c, Trans__c, To__c, Publication_Company__c, Publication_Company__r.Name, 
                RecordTypeId, Product2__c, Order_Group__r.InvoiceGenerationStatus__c, Publication_Code__c, SPINS__c, ListPrice__c, Product2__r.Family, 
                Order__c, Order_Group__c, Order_Group__r.IsActive__c, Order_Group__r.National_Staging_Order_Set__c, NAT__c, NAT_Client_ID__c, From__c, 
                Directory__c, Directory__r.Name, Directory__r.State__c, Directory_Code__c, Description__c, DAT__c, Client_Number__c, Product2__r.RGU__c, 
                Account__r.Name, CMR_Number__c, CMR_Name__c, CMR_Name__r.Name, CMR_Name__r.BillingState, BAS__c, Advertising_Data__c, Account__c,unitprice__c,
                Directory_Edition__c, Directory_Edition__r.Name, Directory_Edition__r.Life__c, Trans_Version__c, Publication_Date__c, UDAC__c, Line_Number__c             
                FROM Order_Line_Items__c WHERE Action__c != 'O' AND Publication_Code__c = :pubCode AND Edition_Code__c = :edtnCode
                AND Directory__c = :dirId];
    }
    
    public static list<Order_Line_Items__c> getOLIByNationalOrderSetWithoutInv(String pubCode, String edtnCode, Id dirId) {
        return [Select Page_Number__c, Order_Anniversary_Start_Date__c,Transaction_ID__c, From_C__c, To_P__c, Trans__c, To__c, Publication_Company__c, Publication_Company__r.Name, 
                RecordTypeId, Product2__c, Order_Group__r.InvoiceGenerationStatus__c, Publication_Code__c, SPINS__c, ListPrice__c, Product2__r.Family, 
                Order__c, Order_Group__c, Order_Group__r.IsActive__c, Order_Group__r.National_Staging_Order_Set__c, NAT__c, NAT_Client_ID__c, From__c, 
                Directory__c, Directory__r.Name, Directory__r.State__c, Directory_Code__c, Description__c, DAT__c, Client_Number__c, Product2__r.RGU__c, 
                Account__r.Name, CMR_Number__c, CMR_Name__c, CMR_Name__r.Name, CMR_Name__r.BillingState, BAS__c, Advertising_Data__c, Account__c,unitprice__c,
                Directory_Edition__c, Directory_Edition__r.Name, Directory_Edition__r.Life__c, Trans_Version__c, Publication_Date__c, UDAC__c, Line_Number__c,             
                National_Staging_Line_Item__r.BAS__c, National_Staging_Line_Item__r.Spins__c, Directory_Edition__r.National_Billing_Date__c
                FROM Order_Line_Items__c WHERE Action__c != 'O' AND Publication_Code__c = :pubCode AND Edition_Code__c = :edtnCode
                AND Directory__c = :dirId AND Order_Group__r.InvoiceGenerationStatus__c = false];
    }
    
    public static list<Order_Line_Items__c> getOLIForNationalDFF(set<Id> setOLIIDs) {
       return [Select National_Staging_Line_Item__r.SPINS__c,Order_Anniversary_Start_Date__c,Anchor_Listing_Caption_Header__c,strOLICombo__c ,Cutomer_Cancel_Date__c ,Product2__r.Is_Anchor__c,Product2__r.Print_Product_Type__c,Product_Type__c, Opportunity__c,Trademark_Finding_Line_OLI__c,National_Staging_Line_Item__r.Parent_ID__r.Is_Caption_Header__c,
                Opportunity__r.AccountId, Opportunity__r.Owner.Name,National_Staging_Line_Item__r.Is_Caption_Header__c, OL_Print_Specialty_Product_Type__c,
                National_Staging_Line_Item__r.Caption_Display_Text__c,National_Staging_Line_Item__r.Caption_Phone_Number__c,National_Staging_Line_Item__r.Caption_Indent_Level__c,
                National_Staging_Line_Item__r.Caption_Indent_Order__c,National_Staging_Line_Item__r.Is_Caption_Member__c,National_Staging_Line_Item__r.DAT__c,
                Opportunity__r.Account.account_number__c, Opportunity__r.Account.Phone, Digital_Product_Requirement__c,
                Product2__r.RGU__c, Opportunity_line_Item_id__c, ID,Product2__r.Trademark_Product__c, Opportunity__r.Owner.CommunityNickname,
                Product2__r.ProductCode,Product2__r.Family, Opportunity__r.Owner.Email,Opportunity__r.Owner.FirstName,Opportunity__r.Owner.LastName,directory__c,Directory_Section__c,
                Opportunity__r.Account.Account_Manager_Name__c, Opportunity__r.Account.Account_Manager_Email__c,
                Edition_Code__c,Directory_Code__c,Directory_Section__r.Name,Directory_Section__r.Section_Code__c,Directory_Section__r.Section_Page_Type__c,Directory_Edition__c,
                Directory_Heading__c, Product2__r.Name,Listing_Name__c,Listing__r.Listing_Street_Number__c,Listing__r.Listing_Street__c,
                Listing__r.Listing_PO_Box__c,Listing__r.Listing_City__c,Listing__r.Listing_Postal_Code__c,Listing__r.Phone__c,
                Listing__r.Area_Code__c,Listing__r.Exchange__c,Directory_Heading__r.code__c,Directory_Heading__r.Name,CMR_Name__r.Phone, Product2__c, Opportunity__r.Account_Manager__c
                from Order_Line_Items__c where Id IN:setOLIIDs];
    }
    
   
    public static list<Order_Line_Items__c> getOLIByID(set<ID> setOLIId) {
        return [Select Id, Account__c, Order_Anniversary_Start_Date__c,Directory_Heading__r.Directory_Heading_Name__c, Account__r.phone,Account__r.BillingState,
                Account__r.BillingPostalCode,Account__r.BillingStreet,Account__r.BillingCity,OL_Print_Specialty_Product_Type__c,Anchor_Listing_Caption_Header__c,Trademark_Finding_Line_OLI__c,
                Package_ID_External__c, Opportunity__r.StageName, Addon_Type__c, Billing_Contact__c, Billing_Contact__r.Name, Billing_Contact__r.Phone, Directory_Section__r.Section_Page_Type__c, P4P_Tracking_Number_ID__c,
                Billing_End_Date__c, Billing_Frequency__c,Opportunity__r.Billing_Contact__r.Email,Opportunity__r.Billing_Contact__r.HasOptedOutOfEmail, Billing_Partner__c, Anchor_Ad__c, Display_Ad__c,
                Billing_Start_Date__c, Canvass__c, Category__c, Continious_Billing__c, Opportunity__r.Billing_Contact__c,RecordTypeId, Core_Opportunity_Line_ID__c,Scoped_Caption_Header__c, 
                Description__c, Directory__c, Directory_Edition__c, Discount__c, Distribution_Area__c, Product2__r.Name, Product2__r.Inventory_Tracking_Group__c, Migration_Directory_Heading1__c, 
                Migration_Directory_Heading2__c, Migration_Directory_Heading3__c, Migration_Directory_Heading4__c, Product_Is_IBUN_Bundle_Product__c, Product2__r.Print_Product_Type__c,
                Effective_Date__c, FulfillmentDate__c, Geo_Code__c, Geo_Type__c, Is_Caption__c, Directory_Section__r.Section_Code__c,Media_Type__c,Cutomer_Cancel_Date__c, Product2__r.Is_Anchor__c,  
                Is_Child__c, Is_P4P__c, Line_Status__c, Linvio_Payment_Method_Id__c, Directory_Heading__c, Directory_Heading__r.Name, Directory_Heading__r.code__c,  
                Locality__c, UnitPrice__c, Name, Seniority_Date__c, Directory_Section__r.Columns__c,ListPrice__c,Last_Billing_Date__c,Account__r.OwnerId,Account__r.Owner.Email,Account__r.Owner.Manager.Email,
                Opportunity__c, Opportunity_line_item_id__c,Directory__r.Branch_Office__c, Order__c, Order_Group__c, Order_Line_Total__c, P4P_Billing__c,Is_Handled__c, 
                P4P_Price_Per_Click_Lead__c, Package_ID__c, Parent_ID__c, Parent_ID_of_Addon__c, Parent_Line_Item__c, Payment_Duration__c, Payment_Method__c,isCanceled__c,
                Payments_Remaining__c, Pricing_Program__c, Print_First_Bill_Date__c,Package_Name__c, Product_Type__c, Product2__c, Product2__r.Vendor__c, ProductCode__c, Quantity__c, 
                Quote_signing_method__c, Scope__c, Directory_Section__c, Statement_Suppression__c, Status__c, Product2__r.Family, Product2__r.RGU__c, Product2__r.Is_IBUN_Bundle_Product__c,
                Directory_Section__r.Name,Directory__r.Pub_Date__c, Section_Page_Type__c, strOLICombo__c, Product_Inventory_Tracking_Group__c, Opportunity_Close_Date__c,Listing__c,
                Account__r.Name,Account__r.Owner.Name,Directory_Edition__r.Name,Directory__r.Name,UDAC__c,Successful_Payments__c, Talus_Go_Live_Date__c, Telco_Invoice_Date__c, Directory_Code__c, 
                Edition_Code__c, Canvass__r.Canvass_Code__c,Product2__r.Product_Type__c, Listing__r.Phone__c, Listing__r.Area_Code__c, Opportunity__r.Name, Billing_Partner_Account__c, 
                Listing__r.Name, Listing__r.Listing_City__c, Listing__r.Listing_Country__c, Listing__r.Listing_PO_Box__c, Listing__r.Listing_Postal_Code__c, Listing__r.Listing_State__c, 
                Listing__r.Listing_Street__c, Listing__r.Listing_Street_Number__c,Directory_Edition__r.Book_Status__c,Directory_Edition__r.Pub_Date__c,Directory_Edition__r.Directory__c,
                Product2__r.Print_Specialty_Product_Type__c, Opportunity__r.Owner.Email, Opportunity__r.Owner.FirstName, Opportunity__r.Owner.LastName,
                Product2__r.ProductCode, Original_Order_Line_Item__c, Original_Order_Line_Item__r.Digital_Product_Requirement__r.business_phone_number_office__c,
                Original_Order_Line_Item__r.Digital_Product_Requirement__r.P4P_Forwarded_Number__c,Telco__c,Service_End_Date__c,Service_Start_Date__c,Next_Billing_Date__c,Product2__r.Description
                from Order_Line_Items__c where Id IN:setOLIId];
    }
    
    
    
     public static list<Order_Line_Items__c> getOLIbyNSLI(set<ID> setNSLIId) {
        return [Select UDAC__c, Listing__c,Directory_Heading__c, Order_Anniversary_Start_Date__c,Seniority_Date__c,National_Staging_Line_Item__c, Digital_Product_Requirement__c, Id, Name, Opportunity__c, Order_Group__c, Order_Group__r.Name, Order__c, Order__r.Account__c, Product2__r.Family, Product2__r.RGU__c, Opportunity_line_Item_id__c,
        Digital_Product_Requirement__r.National_Graphics_ID__c, Listing__r.Phone__c 
                from Order_Line_Items__c where National_Staging_Line_Item__c IN:setNSLIId];
    }
    
    public static list<Order_Line_Items__c> getOLIForSalesInvoicebyIDs(set<ID> setOLIId) {
        return [Select Payment_Duration__c, Order_Anniversary_Start_Date__c,Billing_Frequency__c, Product2__r.Name, Media_Type__c, 
                Order_Line_Total__c, ID, Name, Quantity__c, UnitPrice__c,
                Prorate_Credit__c, Prorate_Credit_Days__c, Opportunity__c, Order__c, Print_First_Bill_Date__c, Telco__c, 
                Billing_Partner_Account__c, Payment_Method__c, Account__c,P4P_Current_Billing_Clicks_Leads__c,Billing_Contact__c, Opportunity__r.Name, 
                Order_Group__c, Digital_Product_Requirement__r.business_phone_number_office__c, 
                Talus_Go_Live_Date__c, Directory__c, Directory_Edition__c, Payments_Remaining__c, Successful_Payments__c,LastModifiedDate,
                Product_Type__c, Canvass__r.Canvass_Code__c, CMR_Name__c, Billing_Partner__c, P4P_Billing__c, Is_P4P__c, Directory_Code__c, Edition_Code__c, 
                Canvass__c, P4P_Price_Per_Click_Lead__c,  
                Service_End_Date__c, Service_Start_Date__c, Next_Billing_Date__c, Billing_Close_Date__c, Product2__r.Family, Product2__r.RGU__c, Package_Id__c
                from Order_Line_Items__c where Id IN:setOLIId Order by LastModifiedDate DESC];
    }
    
    public static list<Order_Line_Items__c> getOLIForBillingTransfer(set<Id> oliID) 
    {
        return [Select Last_Billing_Date__c,OLI_Original_Package_ID_External__c,Order_Anniversary_Start_Date__c,Opportunity__r.Name, Billing_Partner_Account__c, Order__c, Order_Group__c, Opportunity__c, 
                Opportunity_line_Item_id__c, Linvio_Payment_Method_Id__c, BillingTransfered__c, Quantity__c, Prorate_Credit_Days__c, Media_Type__c,
                Order_Line_Total__c, Payments_Remaining__c, UnitPrice__c, Talus_Go_Live_Date__c, Product2__r.Family, Product2__r.RGU__c, Payment_Duration__c,                 
                Prorate_Credit__c, Canvass__r.Billing_Entity__c,Canvass__r.Canvass_Code__c, Directory_Code__c, Edition_Code__c, P4P_Current_Billing_Clicks_Leads__c, Is_P4P__c,  
                Service_End_Date__c, Service_Start_Date__c, Directory__c, Directory_Edition__c,Next_Billing_Date__c, P4P_Billing__c, Product2__r.Name,
                Billing_Frequency__c, Transferred_from__c, Transferred_To__c, Date_of_Transfer__c, Billing_Partner_Transferred__c, 
                Number_of_Months_Transferred__c, Billing_Partner_Change__c, Expiration_Date__c, Telco__c, 
                Billing_Partner__c, Payment_Method__c,CMR_Name__c, Billing_Contact__c, Account__c, BillingTransferedInfo__c, Name, Id,Successful_Payments__c
                from Order_Line_Items__c where Id IN:oliID];
    }
    
    public static list<Order_Line_Items__c> getOLIByCavassAndAcct(Id canvassId, Id acctId) {
        return [Select Last_Billing_Date__c,OLI_Original_Package_ID_External__c,Opportunity__r.Name, Billing_Partner_Account__c, Order__c, Order_Group__c, Opportunity__c, Opportunity_line_Item_id__c, 
                Linvio_Payment_Method_Id__c, BillingTransfered__c, Order_Line_Total__c, Payments_Remaining__c, UnitPrice__c, Talus_Go_Live_Date__c, 
                Product2__r.Family, Product2__r.RGU__c, Payment_Duration__c, Quantity__c, Prorate_Credit_Days__c, BillingTransferedInfo__c, Name, 
                Prorate_Credit__c, Canvass__r.Canvass_Code__c, Directory_Code__c,Canvass__r.Billing_Entity__c, Edition_Code__c, P4P_Current_Billing_Clicks_Leads__c, Telco__c, 
                Is_P4P__c, P4P_Billing__c, Product2__r.Name, Service_End_Date__c, Service_Start_Date__c, Directory__c, Directory_Edition__c,
                Next_Billing_Date__c, Billing_Frequency__c, Transferred_from__c, Transferred_To__c, Date_of_Transfer__c, Billing_Partner_Transferred__c, 
                Number_of_Months_Transferred__c, Billing_Partner_Change__c, Billing_Partner__c, Payment_Method__c,CMR_Name__c, Billing_Contact__c, Account__c, 
                Package_ID_External__c, Parent_ID__c, Parent_ID_of_Addon__c, ProductCode__c, Media_Type__c, Package_ID__c, Directory_Heading__r.name,
                Directory_Edition__r.name, Directory_Edition__r.Book_Status__c, Status__c, Reason_for_Transfer__c, Order_Anniversary_Start_Date__c, 
                Continious_Billing__c, Line_status__c, Id,Successful_Payments__c, Expiration_Date__c,(select id,OLI_BT_Order_Line_Item__c,Service_Start_Date__c,Next_Billing_Date__c,Service_End_Date__c,Successful_Payments__c,Payments_Remaining__c,Billing_Frequency_Change__c,Frequency_Change_Type__c,last_Billing_Date__c,createddate from  BillingTransfer_OLI_Histories__r order by createddate DESC) 
                FROM Order_Line_Items__c WHERE Canvass__c=:canvassId AND Account__c=:acctId AND isCanceled__c=False];
    }
   
    
    public static list<Order_Line_Items__c> getOLIForSalesInvoicebyPackageIDs(set<string> setOLIPkgId) {
        return [Select Order_Anniversary_Start_Date__c,Billing_Frequency__c, Product2__r.Name, Order_Line_Total__c, ID, Name, Quantity__c, UnitPrice__c, Prorate_Credit__c, Prorate_Credit_Days__c, Opportunity__c, 
                Billing_Partner_Account__c, Payment_Method__c, Package_Id__c,Account__c,P4P_Current_Billing_Clicks_Leads__c,Billing_Contact__c, Opportunity__r.Name, Order_Group__c, Talus_Go_Live_Date__c, 
                Product_Type__c, Canvass__r.Canvass_Code__c, CMR_Name__c, Billing_Partner__c, P4P_Billing__c, Is_P4P__c, Directory_Code__c, Edition_Code__c, Canvass__c, 
                Service_End_Date__c, Service_Start_Date__c, Next_Billing_Date__c, Billing_Close_Date__c, Product2__r.Family, Product2__r.RGU__c
                from Order_Line_Items__c where Package_Id__c IN:setOLIPkgId];
    }
    
    
    
    public static list<Order_Line_Items__c> getOrderLineItemByComboValueForSeniorityDate(set<String> setStringCombo) {
        return [SELECT Order_Anniversary_Start_Date__c, Account__c, Directory_Heading__c, Product2__c, Directory__c, Seniority_Date__c,strSeniorityDateCombo__c FROM Order_Line_Items__c WHERE strSeniorityDateCombo__c IN :setStringCombo AND IsCanceled__c != true and Seniority_Date__c != null];
    }
    
    public static list<Order_Line_Items__c> getOLIByTransUniqueCombo(String strDirCode, String edtnCode, String strCMRNo, String strClientNo) {
        return [Select Order_Anniversary_Start_Date__c,Order_Group__c, Order_Group__r.name   
                From Order_Line_Items__c WHERE Client_Number__c = :strClientNo AND Edition_Code__c = :edtnCode
                AND Directory_Code__c = :strDirCode AND CMR_Number__c = :strCMRNo];
    }
    
    public static List<Order_Line_Items__c> getOLIFByDirEditionIds(set<Id> setDirEditionIds) {
        return [SELECT Order_Anniversary_Start_Date__c,Directory_Edition__c, Id, IsCanceled__c FROM Order_Line_Items__c WHERE Directory_Edition__c In : setDirEditionIds];
    }
    
    public static list<Order_Line_Items__c> getOLIByIDForP4P(set<ID> setOLIId) {
        return [SELECT Id, Order_Anniversary_Start_Date__c,Current_Month_for_P4P__c, P4P_Current_Billing_Clicks_Leads__c, P4P_Current_Months_Clicks_Leads__c
                FROM Order_Line_Items__c WHERE Id IN:setOLIId AND Is_P4P__c = true];
    }
    
    public static list<Order_Line_Items__c> getOLIByListingIds(set<ID> listingIds) {
        return [SELECT Id, Order_Anniversary_Start_Date__c,Sort_As__c, Sort_As_FORMULA__c FROM Order_Line_Items__c WHERE Listing__c IN:listingIds];
    }
    public static Order_Line_Items__c getOliById(String oliId){
        return [select id,Order_Anniversary_Start_Date__c,Package_ID_External__c,Parent_ID__c,Parent_ID_of_Addon__c from Order_Line_Items__c where id=:oliId];
    }
    
    public static list<Order_Line_Items__c> getOLIByIdsForTradMarkFindLine(set<ID> OLIIds) {
        return [SELECT Id, Order_Anniversary_Start_Date__c,Sort_As__c, Sort_As_FORMULA__c FROM Order_Line_Items__c WHERE ID IN:OLIIds OR Trademark_Finding_Line_OLI__c IN:OLIIds];
    }
    
    public static list<Order_Line_Items__c> getOrderLineItemForSeniorityDate(set<Id> acctIds, set<Id> dirIds, set<Id> dirHeadIds, set<Id> prodIds) {
        return [SELECT Order_Anniversary_Start_Date__c, Account__c, Directory_Heading__c, Product2__c, Directory__c, Seniority_Date__c,
                strSeniorityDateCombo__c FROM Order_Line_Items__c WHERE IsCanceled__c = false AND Seniority_Date__c != null AND Account__c IN: acctIds 
                AND Directory__c IN: dirIds AND Directory_Heading__c IN: dirHeadIds AND Product2__c IN: prodIds AND Media_Type__c =:CommonMessages.printMediaType 
                AND IsCanceled__c = false];
    }
    public static list<Order_Line_Items__c> getOrderLineItemByAccountIDs(set<Id> accountIds) {
        return [SELECT Id,Name, Order__c, Order__r.Account__c, Canvass__c, Description__c , Directory_Code__c, Last_Billing_Date__c, Next_Billing_Date__c,
                Directory__c, Directory_Edition__c,Discount__c,Fulfilled_on__c, FulfillmentDate__c, ListPrice__c, P4P_Current_Billing_Clicks_Leads__c, 
                Opportunity__c, Opportunity__r.Name, Product2__c, Product2__r.Family, Product2__r.RGU__c, Media_Type__c, P4P_Price_Per_Click_Lead__c,
                ProductCode__c, Quantity__c, Sent_to_fulfillment_on__c, UnitPrice__c, Billing_Contact__c, BillingChangesPayment__c, Account__c,
                Edition_Code__c, Order_Anniversary_Start_Date__c,Billing_End_Date__c, Billing_Frequency__c, Total_Billing_Amount_Transfered__c,
                Billing_Partner__c, Billing_Start_Date__c, Telco_Invoice_Date__c, Print_First_Bill_Date__c, OriginalBillingDate__c, 
                Digital_Product_Requirement__c,Payment_Method__c, Payments_Remaining__c, Successful_Payments__c, Payment_Duration__c,Service_Start_Date__c,
                Service_End_Date__c, IsCanceled__c, Talus_Go_Live_Date__c, Talus_Fulfillment_Date__c,Current_Daily_Prorate__c,Quote_Signed_Date__c,
                Quote_signing_method__c, Total_Prorated_Days_for_contract__c, Cutomer_Cancel_Date__c, Talus_Cancel_Date__c, Order_Group__c,
                Billing_Close_Date__c, P4P_Billing__c, Is_P4P__c, P4P_Current_Months_Clicks_Leads__c,Billing_Partner_Account__c, Order_Billing_Date_Changed__c,
                Contract_End_Date__c, Line_Status__c,Continious_Billing__c, Prorate_Stored_Value__c, Cancellation__c, Total_Prorate__c,
                Product2__r.Name, Product2__r.Product_Type__c,Prorate_Credit__c, Prorate_Credit_Days__c, Current_Billing_Period_Days__c,Order_Line_Total__c,
                BillingChangeNoofDays__c, CMR_Name__c, Billing_Change_Prorate_Credit__c, BillingChangeProrateCreditDays__c                      
                FROM Order_Line_Items__c WHERE Account__c IN : accountIds] ;
    }
    public static List<Order_Line_Items__c> getOLIForSpecialtyProductByDirEditionId(Id dirEditionId) {
        return [Select Order_Anniversary_Start_Date__c,Product2__c, Product2__r.Print_Specialty_Product_Type__c, Product2__r.ProductCode, Account__r.Name, Account__r.Account_Number__c, 
                Directory_Edition__r.Name, Account__r.Phone, Name, UnitPrice__c, UDAC__c, Action_Code__c, Digital_Product_Requirement__r.Order_URN__c, Directory_Edition__r.Year__c, Id, Directory__c, 
                (SELECT Directory_Edition__r.Year__c, UnitPrice__c, Directory_Edition__c FROM Line_Item_Histories__r ORDER BY CreatedDate DESC)                
                From Order_Line_Items__c WHERE Product2__r.Print_Product_Type__c = : CommonMessages.Specialty AND 
                Product2__r.Media_Type__c = : CommonMessages.oliPrintProductType AND Directory_Edition__c = : dirEditionId];
    }
    
    
    public static list<Order_Line_Items__c> getOrderLineItemForSeniorityDateCombo(set<String> setSeniorityDataCombo) {
        return [SELECT Order_Anniversary_Start_Date__c, Account__c, Directory_Heading__c, Product2__c, Directory__c, Seniority_Date__c,
                strSeniorityDateCombo__c, OLI_Seniority_Date_Combo__c FROM Order_Line_Items__c WHERE IsCanceled__c = false AND Seniority_Date__c != null AND OLI_Seniority_Date_Combo__c IN:setSeniorityDataCombo AND Media_Type__c =:CommonMessages.printMediaType 
                AND IsCanceled__c = false];
    }
    
     public static list<Order_Line_Items__c> getOrderLineItemByAccountIDOrderByOrder(set<Id> orderGroupID) {
        return [Select Name, Order_Anniversary_Start_Date__c,UnitPrice__c, Quantity__c, ProductCode__c, Product2__r.IsBundle__c,  Product2__r.Family, 
                Product2__r.IsActive, Product2__r.Description, Product2__r.ProductCode, Product2__r.Name, Product2__r.Id, Product2__c, Payment_Method__c, 
                Payment_Duration__c, Order__c, ListPrice__c, IsCanceled__c, Id, Discount__c, Canvass__c, Billing_Contact__c, 
                Opportunity__r.OwnerId, Opportunity__c, Billing_Frequency__c, Billing_Partner__c, CMR_Name__c, Canvass__r.Canvass_Code__c,
                Opportunity__r.Account_Primary_Canvass__c, Account__c, Order_Group__c, BillingTransfered__c, BillingTransferedInfo__c, Payments_Remaining__c, Product_Type__c,Directory_Code__c, Product2__r.RGU__c, Is_P4P__c,P4P_Billing__c
                From Order_Line_Items__c where Order_Group__c IN :orderGroupID  
                order by Order__c];   
    }
     public static map<Id, Order_Line_Items__c> getOrderLineItemByAccount(set<Id> accountId) {
        return new map<Id, Order_Line_Items__c>([Select Order_Anniversary_Start_Date__c,Cutomer_Cancel_Date__c, Cancellation__c, IsCanceled__c, ProductCode__c, Product2__r.Name, Product2__c, Order__r.CreatedDate, Order__r.Name, Order__c, Opportunity__r.isLocked__c, Opportunity__r.Name, Opportunity__c, UnitPrice__c, ListPrice__c, Product2__r.Family, Product2__r.RGU__c,Id, FulfillmentDate__c,  Payment_Duration__c, Product_Type__c From Order_Line_Items__c where IsCanceled__c = false and Order__r.Account__c IN :accountID order by Order__r.CreatedDate, Product2__r.Name]);
    }
    
    public static list<Order_Line_Items__c> getOLINotHandledinModification(string objDEId,string UserId){
      return [Select Id,Name,Account__r.BillingStreet,Account__r.BillingCity,Account__r.BillingState,Account__r.BillingPostalCode,Cutomer_Cancel_Date__c,Account__r.Delinquency_Indicator__c,Account__r.Open_Claim__c,Account__r.Phone,Product_Type__c,
          Product2__r.Print_Product_Type__c,Billing_Contact__c,Billing_Contact__r.Name,UnitPrice__c,Order_Line_Total__c,Effective_Date__c,Is_P4P__c,Is_Handled__c,Last_Billing_Date__c,Billing_Contact__r.Email,Directory_Edition__c,Discount__c,Directory_Edition__r.Name,
          Listing__r.Phone__c,Listing__r.Name,Directory__r.Name,Directory_Edition__r.Letter_Renewal_Stage_1__c,Directory_Edition__r.Letter_Renewal_Stage_2__c,Account__c,Account__r.Name,Account__r.Letter_Renewal_Sequence__c,Product2__r.Name,
          Order_Anniversary_Start_Date__c,Opportunity__c,Media_Type__c  from Order_Line_Items__c 
          where Directory_Edition__c = : objDEId AND Account__r.Letter_Renewal_Sequence__c != null AND Account__r.OwnerId =:UserId AND Media_Type__c='Print' AND Is_Handled__c=false];
    }
    public static list<Order_Line_Items__c> getOLIbyunsuppressedSL(set<Id> setOLIId){
        return [Select Id,name,Scoped_Suppression_Processing_Complete__c from Order_Line_Items__c where Id IN : setOLIId];
    }
   
}