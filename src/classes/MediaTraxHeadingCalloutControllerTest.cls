@isTest
public class MediaTraxHeadingCalloutControllerTest{
   /* public static testmethod void MediaTraxHeadingCalloutControllerTest(){
        //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());

        Digital_Product_Requirement__c objDFF = TestMethodsUtility.generateDataFulfillmentForm();
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.name='Test123';
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.generateDirectoryHeading();
        objDH.name='Test123';
        insert objDH;
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Line_Items__c objOLI = new Order_Line_Items__c(Quote_Signed_Date__c = System.Today()+1, 
                                 Order_Anniversary_Start_Date__c = System.Today()-5,   Billing_Frequency__c = CommonMessages.singlePayment, 
                                 Billing_Partner__c = 'THE BERRY COMPANY', Talus_Go_Live_Date__c = System.Today()+30, Payment_Duration__c = 10,
                                 Account__c = newAccount.Id,Order__c = newOrder .Id, 
                                 Billing_Contact__c = newContact.Id, Canvass__c = newAccount.Primary_Canvass__c);
        
        objDFF.OrderLineItemID__c = objOLI.id;
        insert objDFF;
        List<Digital_Product_Requirement__c> lstDFF = [select OrderLineItemID__r.Name, Account__r.Media_Trax_Client_ID__c, Account__r.BillingCity, Account__r.BillingState, Account__r.BillingCountry, business_phone_number_office__c, P4P_Requested_Area_Code__c, P4P_Requested_Prefix__c, OrderLineItemID__r.Directory__r.Media_Trax_Directory_ID__c, OrderLineItemID__r.Directory_Heading__r.Media_Trax_Heading_ID__c, UDAC__c, P4P_Number_Type__c, OrderLineItemID__r.P4P_Price_Per_Click_Lead__c, Call_Recording__c from Digital_Product_Requirement__c where id = :objDFF.id];
        Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        List<MediaTrax_API_Configuration__c> lstMedia = new List<MediaTrax_API_Configuration__c>();
        MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='GetAuthToken',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/authentication.cfc',HEADER_SOAPACTION__C='GetAuthentication',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='',OPERATION__C='');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='UpdateHeadings',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/headings.cfc',HEADER_SOAPACTION__C='UpdateHeadings',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='headingId,headingName',OPERATION__C='Update');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='UpdateDirectories',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/directories.cfc',HEADER_SOAPACTION__C='UpdateDirectories',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='directoryIds,publisherIds,directoryNames,directoryNumbers,directoryNumber2s,telcoRecoNumbers,timeZoneIds',OPERATION__C='Update');
        lstMedia.add(objMedia);

        
        objMedia = new MediaTrax_API_Configuration__c(NAME='UpdateClients',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/clients.cfc',HEADER_SOAPACTION__C='UpdateClients',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='clientIdList,clientNameList,clientNumberList,address1List,address2List,cityList,stateCodeList,zipCodeList,countryCodeList,languageList,phoneList,faxList,isYellowPageList,priceOverrideList,callRecordingList,mailMasterList,callTransferList,voiceMailList,whisperModeList,costPerCallList,callEmployeeCodeList,maxAdSpendTypeList,maxAdSpendList',OPERATION__C='Update');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='GetHeadings',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/headings.cfc',HEADER_SOAPACTION__C='GetHeadings',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='headingId,headingName',PREDICATEFIELD__C='headingName',PREDICATEOPERATOR__C='IN',PREDICATEVALUE__C='',FIELDS_MAPPING__C='predicateField,predicateOperator,predicateValue',OPERATION__C='Get');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='AddHeadings',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/headings.cfc',HEADER_SOAPACTION__C='AddHeadings',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='headingNames',OPERATION__C='Add');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='GetDirectories',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/directories.cfc',HEADER_SOAPACTION__C='GetDirectories',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='directoryNumber2,telcoRecoNumber,publisherId,DirectoryID,DirectoryName,DirectoryNumber,TimeZoneID,TimeZone',PREDICATEFIELD__C='DirectoryNumber',PREDICATEOPERATOR__C='IN',PREDICATEVALUE__C='',FIELDS_MAPPING__C='predicateField,predicateOperator,predicateValue',OPERATION__C='Get');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='GetClients',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/clients.cfc',HEADER_SOAPACTION__C='GetClients',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='ClientID,ClientName,ClientNumber',PREDICATEFIELD__C='ClientNumber',PREDICATEOPERATOR__C='IN',PREDICATEVALUE__C='',FIELDS_MAPPING__C='predicateField,predicateOperator,predicateValue',OPERATION__C='Get');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='AddDirectories',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/directories.cfc',HEADER_SOAPACTION__C='AddDirectories',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='publisherIds,directoryNames,directoryNumbers,directoryNumber2s,telcoRecoNumbers,timeZoneIds',OPERATION__C='');     
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='AddClients',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/clients.cfc',HEADER_SOAPACTION__C='AddClients',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='clientNameList,clientNumberList,address1List,address2List,cityList,stateCodeList,zipCodeList,countryCodeList,languageList,phoneList,faxList,isYellowPageList,priceOverrideList,callRecordingList,mailMasterList,callTransferList,voiceMailList,whisperModeList,costPerCallList,callEmployeeCodeList,maxAdSpendTypeList,maxAdSpendList',OPERATION__C='');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='DeactivateTrackingNumber',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/trackingNumbers.cfc',HEADER_SOAPACTION__C='DeactivateTrackingNumber',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='Test123',OPERATION__C='');
        lstMedia.add(objMedia);
        insert lstMedia;
        
        
        /*MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='DeactivateTrackingNumber',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/trackingNumbers.cfc',HEADER_SOAPACTION__C='DeactivateTrackingNumber',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='DeactivateTrackingNumberResponse',RETURN_METHOD__C='DeactivateTrackingNumberReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='Test123',OPERATION__C='');
        insert objMedia;
        objMedia = new MediaTrax_API_Configuration__c(NAME='GetAuthToken',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/trackingNumbers.cfc',HEADER_SOAPACTION__C='DeactivateTrackingNumber',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='DeactivateTrackingNumberResponse',RETURN_METHOD__C='DeactivateTrackingNumberReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='Test123',OPERATION__C='');
        insert objMedia;*/
        //System.debug('Testingg debug lstDFF[0].OrderLineItemID__r.Name '+lstDFF[0].OrderLineItemID__r.Name);
        
        
        /*
        MediaTraxHeadingCalloutController_V1 objCtrller = new MediaTraxHeadingCalloutController_V1();
        
        map<String,String> mapData = new map<String,String>();
        mapData.put('Test123','123');
        List<Directory_Heading__c> lstDH = new List<Directory_Heading__c>();
        lstDH.add(objDH);
        lstDH.add(TestMethodsUtility.createDirectoryHeading());
        List<Directory__c> lstDir = new List<Directory__c>();
        lstDir.add(objDir);
        lstDir.add(TestMethodsUtility.createDirectory());
        Account objAcc = TestMethodsUtility.generateAccount('customer');
        objAcc.name='Test123';
        insert objAcc;
        lstAccount.add(objAcc);
        map<String,string> mpDataNull;
        
        lstAccount = [select Account_Number__c, Media_Trax_Client_ID__c, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Phone, Fax, P4P_Spending_Cap_Type__c, P4P_Spending_Cap__c from account where id in :lstAccount];        
        
        //Test.startTest();
        objCtrller.ProcessMediaTraxForGetAddHeading();
        objCtrller.ProcessMediaTraxForGetAddDirectory();
        objCtrller.ProcessMediaTraxForGetAddClient();
        objCtrller.ProcessMediaTraxForAddHeading();
        objCtrller.ProcessMediaTraxForAddDirectory();
        objCtrller.ProcessMediaTraxForAddClient();
        objCtrller.ProcessMediaTraxForHeadingUpdate();
        objCtrller.ProcessMediaTraxForDirectoryUpdate();
        objCtrller.ProcessMediaTraxForClientUpdate();
        objCtrller.AuthendationSutureCallout();
        
        objCtrller.ProcessMediaTraxForGetAddClientScope(lstAccount);
        objCtrller.ProcessMediaTraxForAddClientScope(lstAccount);
        objCtrller.getMediaTraxAPIConfig(new Set<String>{'DeactivateTrackingNumber'});
        objCtrller.ProcessMediaTraxForGetAddDirectoryScope(new List<Directory__c>{objDir});
        objCtrller.ProcessMediaTraxForAddDirectoryScope(new List<Directory__c>{objDir});
        
        
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());
        objCtrller.addMediaTraxHeading(mapData,lstDH);
        
        Test.stopTest();
    }*/
    /*public static testmethod void MediaTraxHeadingCalloutControllerTest01(){
        //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());
        
        Digital_Product_Requirement__c objDFF = TestMethodsUtility.generateDataFulfillmentForm();
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.name='Test123';
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.generateDirectoryHeading();
        objDH.name='Test123';
        insert objDH;
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Line_Items__c objOLI = new Order_Line_Items__c(Quote_Signed_Date__c = System.Today()+1, 
                                 Order_Anniversary_Start_Date__c = System.Today()-5,   Billing_Frequency__c = CommonMessages.singlePayment, 
                                 Billing_Partner__c = 'THE BERRY COMPANY', Talus_Go_Live_Date__c = System.Today()+30, Payment_Duration__c = 10,
                                 Account__c = newAccount.Id,Order__c = newOrder .Id, 
                                 Billing_Contact__c = newContact.Id, Canvass__c = newAccount.Primary_Canvass__c);
        
        objDFF.OrderLineItemID__c = objOLI.id;
        insert objDFF;
        List<Digital_Product_Requirement__c> lstDFF = [select OrderLineItemID__r.Name, Account__r.Media_Trax_Client_ID__c, Account__r.BillingCity, Account__r.BillingState, Account__r.BillingCountry, business_phone_number_office__c, P4P_Requested_Area_Code__c, P4P_Requested_Prefix__c, OrderLineItemID__r.Directory__r.Media_Trax_Directory_ID__c, OrderLineItemID__r.Directory_Heading__r.Media_Trax_Heading_ID__c, UDAC__c, P4P_Number_Type__c, OrderLineItemID__r.P4P_Price_Per_Click_Lead__c, Call_Recording__c from Digital_Product_Requirement__c where id = :objDFF.id];
        Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        List<MediaTrax_API_Configuration__c> lstMedia = new List<MediaTrax_API_Configuration__c>();
        MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='GetAuthToken',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/authentication.cfc',HEADER_SOAPACTION__C='GetAuthentication',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='',OPERATION__C='');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='UpdateHeadings',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/headings.cfc',HEADER_SOAPACTION__C='UpdateHeadings',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='headingId,headingName',OPERATION__C='Update');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='UpdateDirectories',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/directories.cfc',HEADER_SOAPACTION__C='UpdateDirectories',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='directoryIds,publisherIds,directoryNames,directoryNumbers,directoryNumber2s,telcoRecoNumbers,timeZoneIds',OPERATION__C='Update');
        lstMedia.add(objMedia);

        
        objMedia = new MediaTrax_API_Configuration__c(NAME='UpdateClients',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/clients.cfc',HEADER_SOAPACTION__C='UpdateClients',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='clientIdList,clientNameList,clientNumberList,address1List,address2List,cityList,stateCodeList,zipCodeList,countryCodeList,languageList,phoneList,faxList,isYellowPageList,priceOverrideList,callRecordingList,mailMasterList,callTransferList,voiceMailList,whisperModeList,costPerCallList,callEmployeeCodeList,maxAdSpendTypeList,maxAdSpendList',OPERATION__C='Update');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='GetHeadings',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/headings.cfc',HEADER_SOAPACTION__C='GetHeadings',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='headingId,headingName',PREDICATEFIELD__C='headingName',PREDICATEOPERATOR__C='IN',PREDICATEVALUE__C='',FIELDS_MAPPING__C='predicateField,predicateOperator,predicateValue',OPERATION__C='Get');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='AddHeadings',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/headings.cfc',HEADER_SOAPACTION__C='AddHeadings',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='headingNames',OPERATION__C='Add');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='GetDirectories',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/directories.cfc',HEADER_SOAPACTION__C='GetDirectories',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='directoryNumber2,telcoRecoNumber,publisherId,DirectoryID,DirectoryName,DirectoryNumber,TimeZoneID,TimeZone',PREDICATEFIELD__C='DirectoryNumber',PREDICATEOPERATOR__C='IN',PREDICATEVALUE__C='',FIELDS_MAPPING__C='predicateField,predicateOperator,predicateValue',OPERATION__C='Get');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='GetClients',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/clients.cfc',HEADER_SOAPACTION__C='GetClients',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='ClientID,ClientName,ClientNumber',PREDICATEFIELD__C='ClientNumber',PREDICATEOPERATOR__C='IN',PREDICATEVALUE__C='',FIELDS_MAPPING__C='predicateField,predicateOperator,predicateValue',OPERATION__C='Get');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='AddDirectories',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/directories.cfc',HEADER_SOAPACTION__C='AddDirectories',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='publisherIds,directoryNames,directoryNumbers,directoryNumber2s,telcoRecoNumbers,timeZoneIds',OPERATION__C='');     
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='AddClients',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/clients.cfc',HEADER_SOAPACTION__C='AddClients',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='clientNameList,clientNumberList,address1List,address2List,cityList,stateCodeList,zipCodeList,countryCodeList,languageList,phoneList,faxList,isYellowPageList,priceOverrideList,callRecordingList,mailMasterList,callTransferList,voiceMailList,whisperModeList,costPerCallList,callEmployeeCodeList,maxAdSpendTypeList,maxAdSpendList',OPERATION__C='');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='DeactivateTrackingNumber',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/trackingNumbers.cfc',HEADER_SOAPACTION__C='DeactivateTrackingNumber',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='Test123',OPERATION__C='');
        lstMedia.add(objMedia);
        insert lstMedia;
        
        
        /*MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='DeactivateTrackingNumber',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/trackingNumbers.cfc',HEADER_SOAPACTION__C='DeactivateTrackingNumber',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='DeactivateTrackingNumberResponse',RETURN_METHOD__C='DeactivateTrackingNumberReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='Test123',OPERATION__C='');
        insert objMedia;
        objMedia = new MediaTrax_API_Configuration__c(NAME='GetAuthToken',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/trackingNumbers.cfc',HEADER_SOAPACTION__C='DeactivateTrackingNumber',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='DeactivateTrackingNumberResponse',RETURN_METHOD__C='DeactivateTrackingNumberReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='Test123',OPERATION__C='');
        insert objMedia;*/
        //commented here
       /* System.debug('Testingg debug lstDFF[0].OrderLineItemID__r.Name '+lstDFF[0].OrderLineItemID__r.Name);
        
        
        
        MediaTraxHeadingCalloutController_V1 objCtrller = new MediaTraxHeadingCalloutController_V1();
        
        map<String,String> mapData = new map<String,String>();
        mapData.put('Test123','123');
        List<Directory_Heading__c> lstDH = new List<Directory_Heading__c>();
        lstDH.add(objDH);
        lstDH.add(TestMethodsUtility.createDirectoryHeading());
        List<Directory__c> lstDir = new List<Directory__c>();
        lstDir.add(objDir);
        lstDir.add(TestMethodsUtility.createDirectory());
        Account objAcc = TestMethodsUtility.generateAccount('customer');
        objAcc.name='Test123';
        insert objAcc;
        
        lstAccount.add(objAcc);
        
        lstAccount = [select Account_Number__c, Media_Trax_Client_ID__c, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Phone, Fax, P4P_Spending_Cap_Type__c, P4P_Spending_Cap__c from account where id in :lstAccount];
        
        map<String,string> mpDataNull = new map<String,string>();
        
        
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());
        
        objCtrller.addMediaTraxHeading(null,lstDH);
                
        //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());
        objCtrller.addMediaTraxDirectory(null,lstDir);
        objCtrller.addMediaTraxDirectory(mapData,lstDir);
        
        objCtrller.addMediaTraxClient(mpDataNull,lstAccount);
        objCtrller.addMediaTraxClient(mapData,lstAccount);
        
        objCtrller.replaceSpecialCharacters('Test123');
        
        objCtrller.ProcessMediaTraxForHeadingUpdateScope(lstDH);
        objCtrller.ProcessMediaTraxForDirectoryUpdateScope(lstDir);
        
        objCtrller.ProcessMediaTraxForClientUpdateScope(lstAccount);
        Test.stopTest();
    }
    */
}