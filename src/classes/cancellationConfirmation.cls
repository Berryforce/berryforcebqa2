public class cancellationConfirmation {
public String CaseId{get;set;}
public List<Order_Line_Items__c> OliItemForPrint {get;set;}
public List<Order_Line_Items__c> OliItemForDIgital {get;set;}
public string orderName {get;set;}
public string telePhone{get;set;}
public string Fax{get;set;}
public string Address{get;set;}
public string city{get;set;}
public string state{get;set;}
public string zipcode{get;set;}
public string pagebody{get;set;}
public string caseIdFromUrl;
public string TelcoName;
    public cancellationConfirmation()
    {
        OliItemForPrint=new List<Order_Line_Items__c>();
        OliItemForDIgital=new List<Order_Line_Items__c>();
            caseIdFromUrl=ApexPages.currentPage().getParameters().get('caseid');
            if(caseIdFromUrl!=null)
                getCaseObj1();
       // CanceledOLIData();
     // CaseId='500K0000007xA8G';
     //  getCaseObj1();
    }

    public Case getCaseObj1()
    {
        if(caseIdFromUrl!=null)
            CaseId=caseIdFromUrl;
        OliItemForPrint=new List<Order_Line_Items__c>();
        OliItemForDIgital=new List<Order_Line_Items__c>();
        Case CaseObj=[Select Id,owner.name, (SELECT BillingChangeNoofDays__c,Directory__r.Telco_Provider__r.Name,BillingChangeProrateCreditDays__c,BillingChangesPayment__c,Billing_Change_Prorate_Credit__c,Billing_Close_Date__c,Billing_Contact__c,Billing_End_Date__c,Billing_Frequency__c,Billing_Partner_Account__c,Billing_Partner__c,Billing_Start_Date__c,Cancelation_Fee__c,Cancellation__c,Canvass__c,checked__c,Continious_Billing__c,CreatedById,CreatedDate,Current_Billing_Period_Days__c,Current_Daily_Prorate__c,Current_Rate__c,Cutomer_Cancel_Date__c,Date_of_Transfer__c,Days_B_W__c,Description__c,Digital_Product_Requirement__c,Directory__c,Discount__c,Effective_Date__c,Fulfilled_on__c,FulfillmentDate__c,Directory_Heading__c, Directory_Heading__r.Directory_Heading_Name__c, Directory_Heading__r.Name,Id,isCanceled__c,IsDeleted,Is_Billing_Cycle__c,LastActivityDate,LastModifiedById,LastModifiedDate,Last_successful_settlement__c,Line_Status__c,ListPrice__c,Name,Next_Billing_Date__c,Opportunity__c,Order_Anniversary_Start_Date__c,Order_Billing_Date_Changed__c,Order_Group__c,Selected_Cancel_Date__c,Order__c,Order__r.name,OriginalBillingDate__c,Package_ID__c,Payments_Remaining__c,Payment_Duration__c,Payment_Method__c,Product2__c,ProductCode__c,Product_Type__c,Prorate_Credit_Days__c,                                  Prorate_Credit__c,Prorate_Stored_Value__c,Quantity__c,Quote_Signed_Date__c,Quote_signing_method__c,Reason_for_Transfer__c,Scope__c, Directory_Section__c,Sent_to_fulfillment_on__c,Service_End_Date__c,Service_Start_Date__c,Statement_Suppression__c,Status__c,Subscription_ID__c,Successful_Payments__c,SystemModstamp,Talus_Cancel_Date__c,Talus_Fulfillment_Date__c,Talus_Go_Live_Date__c,Talus_OrderLineItem__c,Telco__c,Total_Prorated_Days_for_contract__c,Total_Prorate__c,UnitPrice__c,User_who_initiated_the_transfer__c,Zero_Cancel_Fee_Approved__c,zero_Cancel_Fee_Requested__c,Media_Type__c,Directory__r.Division__r.Phone_Number__c,Directory__r.Division__r.Fax__c,Directory__r.Division__r.Address__c,Directory__r.Division__r.City__c,Directory__r.Division__r.State__c,Directory__r.Division__r.Zip_Code__c,Directory_Edition__c,Directory__r.Name,Directory_Edition__r.name FROM Order_Line_Items__r),contact.Name,contact.MailingStreet,contact.MailingCity,contact.MailingState,contact.MailingPostalCode,contact.Phone  FROM Case  where id=:CaseId];
        for(Order_Line_Items__c OrdrLi:CaseObj.Order_Line_Items__r)
        {
            if(OrdrLi.Media_Type__c=='Print')
            {
               OliItemForPrint.add(OrdrLi);
              
            }
            else if(OrdrLi.Media_Type__c=='Digital')
            {
                 OliItemForDIgital.add(OrdrLi);
            }
               orderName = OrdrLi.Order__r.name;
 
        }
        if(CaseObj.Order_Line_Items__r.size()>0){         
            if(CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.Phone_Number__c!=null)
                telePhone=CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.Phone_Number__c;
            if(CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.Fax__c!=null)
                Fax=CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.Fax__c; 
            if(CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.Address__c!=null)
                Address=CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.Address__c;
            if(CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.City__c!=null)
                City=CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.City__c;  
            if(CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.State__c!=null)
                State=CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.State__c;
            if(CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.Zip_Code__c!=null)
                zipcode=CaseObj.Order_Line_Items__r[0].Directory__r.Division__r.Zip_Code__c; 
        }         
        return caseobj;
    }
    private string GetImageUrl (String documentName){  
        List<Document> doc = [select name from document where Name =:documentName limit 1];
        if(caseIdFromUrl!=null){
            if(doc.size()>0)
                return '/servlet/servlet.FileDownload?file='+doc[0].id;
            else
                return 'https://berry--bqa1--c.cs9.content.force.com/servlet/servlet.ImageServer?id=015G0000001ia4d&oid=00DK000000W4p6S&lastMod=1403542981000';               
        }
        else{
            if(doc.size()>0)
                return URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+doc[0].id+'&oid='+UserInfo.getOrganizationId();
            else
                return 'https://berry--bqa1--c.cs9.content.force.com/servlet/servlet.ImageServer?id=015G0000001ia4d&oid=00DK000000W4p6S&lastMod=1403542981000';       
        }        
    } 
    public string getLogoUrl()
    {
        string imageURL='';
        if(caseIdFromUrl!=null)
               CaseId=caseIdFromUrl;
                Case CaseObj=[Select Id,owner.name, (SELECT BillingChangeNoofDays__c,Directory__r.Telco_Provider__r.Name,BillingChangeProrateCreditDays__c,BillingChangesPayment__c,Billing_Change_Prorate_Credit__c,Billing_Close_Date__c,Billing_Contact__c,Billing_End_Date__c,Billing_Frequency__c,Billing_Partner_Account__c,Billing_Partner__c,Billing_Start_Date__c,Cancelation_Fee__c,Cancellation__c,Canvass__c,checked__c,Continious_Billing__c,CreatedById,CreatedDate,Current_Billing_Period_Days__c,Current_Daily_Prorate__c,Current_Rate__c,Cutomer_Cancel_Date__c,Date_of_Transfer__c,Days_B_W__c,Description__c,Digital_Product_Requirement__c,Directory__c,Discount__c,Effective_Date__c,Fulfilled_on__c,FulfillmentDate__c,Directory_Heading__c, Directory_Heading__r.Directory_Heading_Name__c, Directory_Heading__r.Name,Id,isCanceled__c,IsDeleted,Is_Billing_Cycle__c,LastActivityDate,LastModifiedById,LastModifiedDate,Last_successful_settlement__c,Line_Status__c,ListPrice__c,Name,Next_Billing_Date__c,Opportunity__c,Order_Anniversary_Start_Date__c,Order_Billing_Date_Changed__c,Order_Group__c,Selected_Cancel_Date__c,Order__c,Order__r.name,OriginalBillingDate__c,Package_ID__c,Payments_Remaining__c,Payment_Duration__c,Payment_Method__c,Product2__c,ProductCode__c,Product_Type__c,Prorate_Credit_Days__c,                                  Prorate_Credit__c,Prorate_Stored_Value__c,Quantity__c,Quote_Signed_Date__c,Quote_signing_method__c,Reason_for_Transfer__c,Scope__c, Directory_Section__c,Sent_to_fulfillment_on__c,Service_End_Date__c,Service_Start_Date__c,Statement_Suppression__c,Status__c,Subscription_ID__c,Successful_Payments__c,SystemModstamp,Talus_Cancel_Date__c,Talus_Fulfillment_Date__c,Talus_Go_Live_Date__c,Talus_OrderLineItem__c,Telco__c,Total_Prorated_Days_for_contract__c,Total_Prorate__c,UnitPrice__c,User_who_initiated_the_transfer__c,Zero_Cancel_Fee_Approved__c,zero_Cancel_Fee_Requested__c,Media_Type__c,Directory__r.Division__r.Phone_Number__c,Directory__r.Division__r.Fax__c,Directory__r.Division__r.Address__c,Directory__r.Division__r.City__c,Directory__r.Division__r.State__c,Directory__r.Division__r.Zip_Code__c,Directory_Edition__c,Directory__r.Name,Directory_Edition__r.name,Telco__r.Name FROM Order_Line_Items__r),contact.Name,contact.MailingStreet,contact.MailingCity,contact.MailingState,contact.MailingPostalCode,contact.Phone  FROM Case  where id=:CaseId];
        if(CaseObj.Order_Line_Items__r.size()>0){
            if(CaseObj.Order_Line_Items__r[0].Telco__c!=null)
                 TelcoName=CaseObj.Order_Line_Items__r[0].Telco__r.Name.toLowerCase();   
        }  
 
       if(TelcoName==null || TelcoName.contains('Windstream'.toLowerCase()))
            imageURL=GetImageUrl('Berry_Logo_Telco'); 
        else if(TelcoName.contains('CenturyLink Telephone'.toLowerCase()))
            imageURL=GetImageUrl('CenturyLink');
        else if(TelcoName.contains('Hawaiian Telcom'.toLowerCase()))
            imageURL=GetImageUrl('HawaiianTelcom');
        else if(TelcoName.contains('Frontier Communications'.toLowerCase()))
             imageURL=GetImageUrl('frontier');
        else if(TelcoName.contains('Cincy Media'.toLowerCase()))
             imageURL=GetImageUrl('CincyMedia');
        else
            imageURL=GetImageUrl('Berry_Logo_Telco');
        System.debug('$$$$$ '+imageURL);       
        return imageURL;
    }       
    
     
}