public class BillingFirstMonthPrintHandlerCtl {

    public static void processTelcoInvoiceAndFirstPrintBillDateBilling(list<Order_Line_Items__c> OLIlist) {
        map<Id,map<Id, list<Order_Line_Items__c>>> mapOrderSetOLIByDE = new  map<Id,map<Id, list<Order_Line_Items__c>>>();
        map<Id, list<Order_Line_Items__c>> mapOLIByDE = new map<Id, list<Order_Line_Items__c>>();
        set<Id> setOLIIds = new set<Id>(); 
        for(Order_Line_Items__c iterator : OLIlist) {
            setOLIIds.add(iterator.Id);
            if(iterator.Order_Anniversary_Start_Date__c != null && iterator.Print_First_Bill_Date__c != null) {
                Integer month = iterator.Print_First_Bill_Date__c.month();
                Integer year = iterator.Print_First_Bill_Date__c.year();
                Integer monthDays = Date.daysInMonth(year, month);
                Integer orderDay = iterator.Order_Anniversary_Start_Date__c.day();
                if(monthDays < orderDay) {
                    iterator.Talus_Go_Live_Date__c = date.newInstance(year, month, monthDays);
                }
                else {
                    iterator.Talus_Go_Live_Date__c = date.newInstance(year, month, orderDay);
                }
            }
            else {
                iterator.Talus_Go_Live_Date__c = iterator.Print_First_Bill_Date__c;
            }
        }
        update OLIlist;
        OLIlist = OrderLineItemSOQLMethods.getOLIForSalesInvoicebyIDs(setOLIIds);
        //CommonRecurHandlerController_V1.BerryBillingCycle(OLIlist);
        
        Map<String, Id> dimensionsMapValues = DimensionsValues.getDimensionByOLI(OLIlist);
        list<c2g__codaInvoice__c> lstInsertSI = new list<c2g__codaInvoice__c>();
        map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLine = new map<Integer, list<c2g__codaInvoiceLineItem__c>>();
        integer iCountForFM = 0;
        set<Id> setOrderSet = new set<Id>();
         //Updated By Ankit for Zero Dollar Invoice
        map<Id, map<Id, double>> mapOLIByDEOLISumAmount = new map<Id, map<Id,double>>();
        for(Order_Line_Items__c iterator : OLIlist) {
            //Updated By Ankit for Zero Dollar Invoice
            if(!mapOLIByDEOLISumAmount.containsKey(iterator.Order_Group__c)){
                map<Id, double> tempmapOLIByDE = new map<Id,double>{iterator.Directory_Edition__c=>iterator.UnitPrice__c};
                mapOLIByDEOLISumAmount.put(iterator.Order_Group__c,tempmapOLIByDE);
            }
            else{
               if(!mapOLIByDEOLISumAmount.get(iterator.Order_Group__c).containskey(iterator.Directory_Edition__c)){
                   mapOLIByDEOLISumAmount.get(iterator.Order_Group__c).put(iterator.Directory_Edition__c,iterator.UnitPrice__c);
               }
               else{
                  mapOLIByDEOLISumAmount.get(iterator.Order_Group__c).put(iterator.Directory_Edition__c,mapOLIByDEOLISumAmount.get(iterator.Order_Group__c).get(iterator.Directory_Edition__c)+iterator.UnitPrice__c);
               }
            }
            
            if(!mapOrderSetOLIByDE.containsKey(iterator.Order_Group__c)) {
                map<Id, list<Order_Line_Items__c>> tempmapOLIByDE = new map<Id,list<Order_Line_Items__c>>{iterator.Directory_Edition__c=>new list<Order_Line_Items__c>{iterator}};
                mapOrderSetOLIByDE.put(iterator.Order_Group__c,tempmapOLIByDE);
            }
            else {
                if(!mapOrderSetOLIByDE.get(iterator.Order_Group__c).containskey(iterator.Directory_Edition__c)) {
                    mapOrderSetOLIByDE.get(iterator.Order_Group__c).put(iterator.Directory_Edition__c,new list<Order_Line_Items__c>());
                }
                mapOrderSetOLIByDE.get(iterator.Order_Group__c).get(iterator.Directory_Edition__c).add(iterator);
            }
            setOrderSet.add(iterator.Order__c);       
        }
        
        updateOrderLineItemAnniversaryDate(setOrderSet);

        for(id OrderSetIterator : mapOrderSetOLIByDE.keyset()) {
            for(Id idDE : mapOrderSetOLIByDE.get(OrderSetIterator).keySet()) {
                iCountForFM++;
                list<Order_Line_Items__c> lstTempOLI = mapOrderSetOLIByDE.get(OrderSetIterator).get(idDE);
                 //Updated By Ankit for Zero Dollar Invoice
                if(mapOLIByDEOLISumAmount.get(OrderSetIterator).get(idDE)>0){
                    lstInsertSI.add(CommonRecurHandlerController_V1.newSalesInvoice(lstTempOLI[0], CommonMessages.accountingCurrencyId, iCountForFM, true, false, false, dimensionsMapValues));
                    integer counter=0;
                    integer limitcount=integer.valueOf(System.Label.Sales_Invoice_LineItem_Limit);
                    System.debug('#########limitcount '+limitcount);
                    for(Order_Line_Items__c iterator : lstTempOLI) {
                       System.debug('######### '+counter);
                        if(counter == limitcount) {
                            counter=0;
                            iCountForFM++;
                            lstInsertSI.add(CommonRecurHandlerController_V1.newSalesInvoice(lstTempOLI[0], CommonMessages.accountingCurrencyId, iCountForFM, true, false, false, dimensionsMapValues));
                        }
                        if(!mapSalesInvoiceLine.containsKey(iCountForFM)) {
                            mapSalesInvoiceLine.put(iCountForFM, new list<c2g__codaInvoiceLineItem__c>());
                        }
                        mapSalesInvoiceLine.get(iCountForFM).add(CommonRecurHandlerController_V1.newalesInvoiceLineItemForCronJob(iterator, null, null, dimensionsMapValues, null,null));
                        counter++;
                    }
              }
            }
        }
        if(lstInsertSI.size() > 0) {
            try {
                insert lstInsertSI;
            } catch(Exception ex) {
                String error = ex.getMessage() + '. ';
                system.debug('Error is ' + error);
                List<String> errorMessages = c2g.CODAExtensionContext.getErrorMessages();
                for( String errorMessage : errorMessages ) { 
                    if( errorMessage != null ) error += '; ' + errorMessage; 
                }
                throw new InvoiceGeneratorHelperException(error);
            }
            list<c2g__codaInvoiceLineItem__c> lstInsertSalesInvoiceLine = new list<c2g__codaInvoiceLineItem__c>();
            for(c2g__codaInvoice__c iterator : lstInsertSI) {
                if(mapSalesInvoiceLine.get(Integer.valueOf(iterator.Auto_Number__c)) != null) {
                    for(c2g__codaInvoiceLineItem__c iteratorChild : mapSalesInvoiceLine.get(Integer.valueOf(iterator.Auto_Number__c))) {
                        iteratorChild.c2g__Invoice__c = iterator.Id;
                        system.debug('Unite Price Value : '+ iteratorChild.c2g__UnitPrice__c);
                    }
                    lstInsertSalesInvoiceLine.addAll(mapSalesInvoiceLine.get(Integer.valueOf(iterator.Auto_Number__c)));
                }
            }

            if(lstInsertSalesInvoiceLine.size() > 0) {
                try {
                insert lstInsertSalesInvoiceLine;
                } catch(Exception ex) {
                String error = ex.getMessage() + '. ';
                system.debug('Error is ' + error);
                List<String> errorMessages = c2g.CODAExtensionContext.getErrorMessages();
                for( String errorMessage : errorMessages ) { 
                    if( errorMessage != null ) error += '; ' + errorMessage; 
                }
                throw new InvoiceGeneratorHelperException(error);
            }
            }
        }
    }
    
    private static void updateOrderLineItemAnniversaryDate(set<Id> OrdIds) {
        list<order_line_items__c> lstOLI = [SELECT Id,  Order_Anniversary_Start_Date__c, Order__r.Billing_Anniversary_Date__c 
                                                FROM Order_Line_Items__c 
                                                WHERE Order__c in :OrdIds AND Order_Anniversary_Start_Date__c = Null];
        if(lstOLI.size() > 0) {
            for(order_line_items__c iterator : lstOLI) {
                iterator.Order_Anniversary_Start_Date__c = iterator.Order__r.Billing_Anniversary_Date__c;
            }
            update lstOLI;
        }
    }
}