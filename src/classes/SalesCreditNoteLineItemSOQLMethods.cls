public with sharing class SalesCreditNoteLineItemSOQLMethods {
    
    
    public static list<c2g__codaCreditNoteLineItem__c> getSysGenSCNLineItemByOLI(set<Id> setOLIId) {
        return [SELECT Id,c2g__CreditNote__r.c2g__Account__c,c2g__CreditNote__r.c2g__CreditNoteStatus__c,c2g__CreditNote__r.c2g__PaymentStatus__c,c2g__CreditNote__r.c2g__CreditNoteDate__c,
                c2g__CreditNote__r.c2g__Period__c,Order_Line_Item__c,c2g__UnitPrice__c,c2g__Product__c,c2g__Quantity__c,c2g__LineDescription__c,c2g__CreditNote__r.Customer_Name__c,
                Order_Line_Item__r.Product2__r.RGU__c,Order_Line_Item__r.Directory_Code__c,Order_Line_Item__r.Canvass__r.Canvass_Code__c, 
                Order_Line_Item__r.CMR_Name__c,Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.P4P_Billing__c,Order_Line_Item__r.Is_P4P__c,
                c2g__CreditNote__c,c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c,c2g__Dimension4__c,c2g__LineNumber__c,c2g__NetValue__c,
                c2g__OwnerCompany__c,Directory_Edition__c,Directory__c,Edition_Code__c,Name,Order_Line_Item__r.Directory__c, c2g__CreditNote__r.c2g__DueDate__c,
                Order_Line_Item__r.Directory_Edition__c,OLI_Line_Number__c,OLI_Product_Name__c,Payment__c,Sales_Invoice_Line_Item__c,Transaction_Type__c,
                Billing_Frequency__c,c2g__TaxCode1__c,c2g__TaxCode2__c,c2g__TaxCode3__c,c2g__TaxValue1__c,c2g__TaxValue2__c,c2g__TaxValue3__c, c2g__CreditNote__r.c2g__InvoiceDate__c,c2g__CreditNote__r.Telco_Reversal__c  
                FROM c2g__codaCreditNoteLineItem__c WHERE Order_Line_Item__c IN :setOLIId AND c2g__CreditNote__r.Is_Manually_Created__c = false AND Offset__c= false AND c2g__CreditNote__r.Telco_Reversal__c=false];
    }
}