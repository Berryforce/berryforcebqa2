public class CancellationLetterController
{
    public string CustomerName{get;set;}
    public string CustomerAddress{get;set;}
    public string TelcoName{get;set;}
    public string FooterTelcoName{get;set;}
    public string telePhone{get;set;}
    public string Fax{get;set;}
    public string CaseOwner{get;set;}
    public date todayDate{get;set;}
    public Id CaseId{get;set;}
    public String htmlBody{get;set;}
    public CancellationLetterController()
    {
           if(ApexPages.currentPage()!=null)
                caseID = ApexPages.currentPage().getParameters().get('id');
            if(caseID!=null) 
                generateHtmlBody(caseID);
                 
    }
    public string generateHtmlBody(Id caseId)
    {
        telePhone='582.272.2950';
        Fax='800.326.2950';
        FooterTelcoName='';
        TelcoName='';
        Case c=[select id,owner.name,contact.lastname,contact.name,contact.firstname,contact.MailingStreet,contact.MailingCity,contact.MailingState,contact.MailingPostalCode,contact.MailingCountry,(select id,Telco__r.name,Directory__r.Telco_Provider__r.name ,Directory__r.Division__r.Phone_Number__c,Directory__r.Division__r.Fax__c from Order_Line_Items__r) from case where id=:caseId];

        todayDate=Date.today();
        CustomerName=(C.contact.Name!=null ?  C.contact.Name: '');
       
        CustomerAddress=(C.contact.MailingStreet!=null ?  C.contact.MailingStreet : '')+'<br/>'+(C.contact.MailingCity!=null ?  C.contact.MailingCity : '')+'<br/>'+(C.contact.MailingState!=null ?  C.contact.MailingState : '')+' '+(C.contact.MailingPostalCode!=null ?  C.contact.MailingPostalCode : '');
        if(c.Order_Line_Items__r.size()>0){
            if(C.Order_Line_Items__r[0].Telco__r.Name!=null)
            {
                TelcoName=(C.Order_Line_Items__r[0].Telco__r.Name!='Windstream' ?  c.Order_Line_Items__r[0].Telco__r.Name + ' and our official publisher The Berry Company, LLC ("Berry")' : 'our official publisher, The Berry Company, LLC ("Berry")');
                FooterTelcoName= (C.Order_Line_Items__r[0].Telco__r.Name!='Windstream' ?  c.Order_Line_Items__r[0].Telco__r.Name + ' and Berry' : 'Berry');
            }
            else
            {
                TelcoName='The Berry Company, LLC';
            }
          //  System.debug('@@@@@@@@@@@@@@@@@ '+C.Order_Line_Items__r[0].Directory__r.Division__r.Phone_Number__c);
           //  System.debug('@@@@@@@@@@@@@@@@@ '+C.Order_Line_Items__r[0].Directory__r.Division__r.Fax__C);
            if(C.Order_Line_Items__r[0].Directory__r.Division__r.Phone_Number__c!=null)
                    telePhone=C.Order_Line_Items__r[0].Directory__r.Division__r.Phone_Number__c;
            if(C.Order_Line_Items__r[0].Directory__r.Division__r.Fax__c!=null)
                    Fax=C.Order_Line_Items__r[0].Directory__r.Division__r.Fax__c;                    
            
        }
        else
        {
            TelcoName='The Berry Company, LLC';
        }
        CaseOwner=c.owner.name; 
        string htmlBody='<html><body style="font-family:Arial,Helvetica,sans-serif;font-size:10px"><div>';
        htmlBody=htmlBody+'<div style="color:orange;margin-top:60px">'+ Datetime.now().format('MMMM dd,yyyy')+'<br><br>'+CustomerName+'<br>'+CustomerAddress+'</div>';
        
        htmlBody=htmlBody+'<br></br><div>Dear <span style="color:orange">'+CustomerName+'</span>,<br/><br/>';
        htmlBody=htmlBody+'<p style="text-align:justify">As you requested, I am cancelling your advertising program with <span style="color:orange">'+
         telcoName+'</span>. All items for the above referred Advertising order will be cancelled in accordance with the applicable Terms and Conditions.</p>';
        htmlBody=htmlBody+'<p style="text-align:justify">Ensuring you reach your business goals is very important to us. I am hoping you�ll reconsider your decision and allow us to help market your business. Reaching Today�s Savvy consumer can sometimes be overwhelming. After all consumers search for local businesses in a variety of ways, including 74% use search engines , 60% use print Yellow Pages, 47% Internet Yellow Pages and 35% use social media*. By cancelling your advertising program with us, you run the risk of consumers not being able to find your business when they�re ready to buy.</p>';
        htmlBody=htmlBody+'<p style="text-align:justify">Should you choose to reconsider, or if you have any questions or concerns, please call me at <span style="color:orange">'+telePhone+'</span>, or you can fax any correspondence to me at <span style="color:orange">'+fax+'</span>. As your business continues to evolve and change, we hope you�ll look to <span style="color:orange">'+FooterTelcoName+'</span>'; 
        htmlBody=htmlBody+'for your future advertising needs. <br>Please visit our websites at <a href="http://www.TheBerryCompany.com" style="text-decoration:none;color:blue" target="_blank">www.TheBerryCompany.com </a> for our complete portfolio of solutions</p></div>';
        htmlBody=htmlBody+'<br/><br/><br/><div>Sincerly, <br><span style="color:orange">'+CaseOwner+'</span><br>Berry<br>Sales Representative</div>';
        htmlBody=htmlBody+'<br/><br/><br/><br/><div><img height="50" src="'+getLogoUrl()+'" width="150" class="CToWUd"></div>';    
        htmlBody=htmlBody+'</div></body></html>';       
        
        return htmlBody;
               
    }
    private string GetImageUrl (String documentName){  
        List<Document> doc = [select name from document where Name =:documentName limit 1];
            if(doc.size()>0)
                return URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+doc[0].id+'&oid='+UserInfo.getOrganizationId();
            else
                return 'https://berry--bqa1--c.cs9.content.force.com/servlet/servlet.ImageServer?id=015G0000001ia4d&oid=00DK000000W4p6S&lastMod=1403542981000';             
    }   
    
    Public string getLogoUrl(){
       string imageURL=''; 
       System.debug('####### '+telcoName);
       string tempTelcoName=telcoName;
       tempTelcoName=tempTelcoName.toLowercase();      
       if(tempTelcoName==null || tempTelcoName.contains('Windstream'.toLowerCase()))
            imageURL=GetImageUrl('Berry_Logo_Telco'); 
        else if(tempTelcoName.contains('CenturyLink Telephone'.toLowerCase()))
            imageURL=GetImageUrl('CenturyLink');
        else if(tempTelcoName.contains('Hawaiian Telcom'.toLowerCase()))
            imageURL=GetImageUrl('HawaiianTelcom');
        else if(tempTelcoName.contains('Frontier Communications'.toLowerCase()))
             imageURL=GetImageUrl('frontier');
        else if(tempTelcoName.contains('Cincy Media'.toLowerCase()))
             imageURL=GetImageUrl('CincyMedia');
        else
            imageURL=GetImageUrl('Berry_Logo_Telco');
        System.debug('####### '+imageURL);     
        return imageURL;
    }         
}