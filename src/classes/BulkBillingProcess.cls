global class BulkBillingProcess {
    webservice static void bulkBill(Id acctId, Id oldRecordTypeId) {
        List<Order_Line_Items__c> listPrintOLI = new List<Order_Line_Items__c>();
                set<Id> OLiIds=new Set<Id>();
        List<Order_Line_Items__c> listDigitalOLI = new List<Order_Line_Items__c>();
        List<Order_Line_Items__c> lstOLIUpdate = new List<Order_Line_Items__c>();
        List<Modification_Order_Line_Item__c> lstMOLIUpdate = new List<Modification_Order_Line_Item__c>();
        Set<Id> setAccIDsForDuplicate = new Set<Id>();
        Account acct = new Account(Id = acctId, RecordTypeId = oldRecordTypeId);
        Integer noOfMonths=0;
        Decimal multiplier = 0.0;                   
        List<Order_Group__c> lstOS = OrderGroupSOQLMethods.getOrderGroupWithOLIByAccountIds(new Set<Id> {acctId});
        System.debug('Testingg lstOS '+lstOS);
        if(lstOS != null && lstOS.size() > 0) {
            for(Order_Group__c objOG : lstOS) {
                for(Order_Line_Items__c objOLI : objOG.Order_Line_Items__r) {
                    if(objOLI.Media_Type__c != null && !objOLI.Billing_Frequency__c.equals(CommonMessages.singlePayment))  {
                        if(objOLI.Media_Type__c.equals(CommonMessages.oliPrintProductType)) {
                            //if(!objOLI.Billing_Frequency__c.equals(CommonMessages.singlePayment)) {
                                objOLI.Billing_Frequency__c = CommonMessages.singlePayment;
                            //}
                                                        OLiIds.add(objOLI.id);
                            listPrintOLI.add(objOLI);
                        }
                        else {
                            if(objOLI.isCanceled__c == false) {
                                objOLI.isCanceled__c = true;                                    
                                objOLI.Cutomer_Cancel_Date__c = system.today();
                                String cancellationreason = Label.Cancellation_Reason_for_Account_1_party_Bulk_Billing;

                                objOLI.Cancellation__c = cancellationreason;
                                objOLI.Cancelation_Fee__c = 0;
                                date ccd = date.valueOf(system.today());
                                date contractEndDate = date.valueOf(objOLI.Contract_End_Date__c);      
                               /* if(contractEndDate != null) {
                                    noOfMonths = ccd.MonthsBetween(contractEndDate);
                                }
                                System.debug('########### '+objOLI.Next_Billing_Date__c.day());
                                System.debug('########### '+ccd.day());
                                if(objOLI.Next_Billing_Date__c.day()>ccd.day())
                                    noOfMonths=noOfMonths+1; 
                                */
                                //If(!objOLI.Billing_Frequency__c.equals(CommonMessages.singlePayment)){
                                    if(objOLI.Payments_Remaining__c!=null)  
                                          noOfMonths=Integer.valueOf(objOLI.Payments_Remaining__c);
                                     if(noOfMonths>3)
                                         noOfMonths=3;
                                    if(objOLI.UnitPrice__c != Null){
                                            objOLI.Cancelation_Fee__c = objOLI.UnitPrice__c * noOfMonths;
                                    } 
                                    listDigitalOLI.add(objOLI);
                                //}
                            }
                            if(objOLI.Order_Group__r.Order_Account__c != null) {
                                if(objOLI.Order_Group__r.Order_Account__r.Digital_Product_Involuntary_Cancelled__c == false) {
                                    if(!setAccIDsForDuplicate.contains(objOLI.Order_Group__r.Order_Account__c)) {
                                        setAccIDsForDuplicate.add(objOLI.Order_Group__r.Order_Account__c);
                                        acct.Digital_Product_Involuntary_Cancelled__c = true;
                                    }
                                }
                            }
                        }
                        lstOLIUpdate.add(objOLI);
                    }
                }
                System.debug('Testingg objOG.Modification_Order_Line_Items__r '+objOG.Modification_Order_Line_Items__r);
                for(Modification_Order_Line_Item__c objMOLI : objOG.Modification_Order_Line_Items__r) {
                    objMOLI.Inactive__c = true;
                    lstMOLIUpdate.add(objMOLI);
                }
            }
            System.debug('Testingg lstMOLIUpdate '+lstMOLIUpdate);
            if(lstMOLIUpdate.size() > 0) {
                if(!Test.isRunningtest()) {
                    update lstMOLIUpdate;
                    System.debug('Testingg lstMOLIUpdate '+lstMOLIUpdate);
                }
            }
            
            if(lstOLIUpdate.size() > 0) {
                if(!Test.isRunningtest()) {
                    update lstOLIUpdate;
                }
            }
              
            if(listDigitalOLI.size()>0) {
                if(!Test.isRunningtest()) {
                 System.debug('############ '+listDigitalOLI.size());
                  System.debug('############ '+listDigitalOLI);
                    list<c2g__codaInvoice__c> invoiceList = SalesInvoiceHandlerController.createSalesInvoiceForCancel(listDigitalOLI);  
                }
            }            
            if(OLiIds.size()>0){
                            list<c2g__codaInvoiceLineItem__c> lstSILIForUpdate =new List<c2g__codaInvoiceLineItem__c>();
                                for(c2g__codaInvoiceLineItem__c iterator : SalesInvoiceLineItemSOQLMethods.getInvoiceLineItemByOLI(OLiIds))
                                {
                                    lstSILIForUpdate.add(new c2g__codaInvoiceLineItem__c(id=iterator.id,offset__c=true));
                                }
                                if(lstSILIForUpdate.size()>0)
                                        update lstSILIForUpdate;
                        }
            if(listPrintOLI.size() > 0) {
                if(!Test.isRunningtest()) {
                    System.debug('############ '+listPrintOLI.size());
                     System.debug('############ '+listPrintOLI);
                    CommonRecurHandlerController_V1.BerryBillingCycle(listPrintOLI);
                }
            }
        }               
        update acct;
       // database.executebatch(new SetCancelCheckBoxBatchController(), 50);
    }
    
    webservice static void cancelBulkBill(Id acctId, Id oldRecordTypeId) {
        Account acct = new Account(Id = acctId, RecordTypeId = oldRecordTypeId, Collection_Status__c = '', Bankruptcy__c = false);   
        update acct;
    }
}