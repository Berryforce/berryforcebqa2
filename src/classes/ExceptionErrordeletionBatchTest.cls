@isTest
private Class ExceptionErrordeletionBatchTest {
   static testmethod void TestExErrordeletionBatch() {
        Database.BatchableContext bc;
        list<Exception_Error__c> objExLst= new list<Exception_Error__c>();
        Exception_Error__c objExc = new Exception_Error__c(Exception_Message__c='Test Message');
        insert objExc;
        objExLst = [Select Id from Exception_Error__c where Id=:objExc.Id];
        Test.StartTest();
            ExceptionErrordeletionBatch objExBatch = new ExceptionErrordeletionBatch();
            objExBatch.start(bc);
            objExBatch.execute(bc,objExLst);
            objExBatch.finish(bc);
        Test.StopTest();
   }
   
   static testmethod void TestExErrordeletionSchBatch() {
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        String jobId = System.schedule('Exception Error Batch', CRON_EXP, new ExceptionDeletionschedulebatch() );   
        Test.stopTest();
    }


}