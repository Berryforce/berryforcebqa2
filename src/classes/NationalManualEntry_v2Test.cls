@IsTest(SeeAllData=true)
public class NationalManualEntry_v2Test {
       
       public static testMethod void natManualCommonLinesTest() {
            Test.StartTest();
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1234';
        CMRAcct.National_Credit_Status__c='Approved';
        CMRAcct.Is_Active__c=true;
        insert CMRAcct;
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='2345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS.CMR_Name__c = CMRAcct.id;
        NSOS.Client_Name__c = ClientAcct.id;
        NSOS.Client_Name_Text__c = ClientAcct.Name;
        NSOS.Client_Number__c = ClientAcct.Client_Number__c;
        NSOS.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS.Directory_Edition_Number__c = '4444';
        NSOS.Directory_Version__c = '1234';
        NSOS.Directory__c = dir.Id;
        NSOS.Directory_Number__c = dir.Directory_Code__c;
        NSOS.Is_Ready__c=false;
        insert NSOS;
        
        
        National_Staging_Line_Item__c NSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLI.Line_Number__c = '11122';
        NSLI.UDAC__c = prod.ProductCode;
        NSLI.National_Graphics_ID__c='GRAPH1234567';
        NSLI.Is_Changed__c=true;
        insert NSLI;
        
         Advice_Query__c adviceQuery = TestMethodsUtility.createAdviceQuery(NSLI);
        Advice_Query__c adviceQueryExist = TestMethodsUtility.createAdviceQuery(NSLI);
        ApexPages.CurrentPage().getParameters().put('id', NSOS.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI.Id);     
        ApexPages.StandardController controller = new ApexPages.StandardController(NSOS);
        NationalManualEntry_V2 obj = new NationalManualEntry_V2(controller);
        obj.advID = adviceQuery.Id;
       obj.onLoad();
        obj.fetchAdviceLines();
        obj.Cancel();
        obj.addLineItems();
        obj.save();        
       // obj.getNatList();
        obj.getToFromList();
       // obj.getTransList();
        obj.valueChanged();
       // obj.fetchClientbyNumber();
        //obj.fetchClientbyNumberOld();
       // obj.fetchCMRbyNumber();
       
       // obj.EnableEditSeniorityDate();
       
        obj.fetchDirectoryByCode();
        obj.fetchDEByLSANo();
        obj.productByUDAC();
        obj.addAdviceQuery();
        obj.redirectEditPage();
        obj.removeEntry();
        obj.createNewAccount(NSOS);
        obj.checkError(1);
        NSLI.Directory_Heading__c = objDH.Id;
        NSLI.National_Pricing__c = dirProdMap.Id;
        NSLI.Directory_Section__c = dirSec.Id;
        NSLI.Discount__c = 'Full Price';
        NSLI.Standing_Order__c = true;
        NSLI.Product2__c = prod.Id;
        
        update NSLI;
        //obj.onLoad();
        obj.processOLI();
        
        NSLI = new National_Staging_Line_Item__c();
        NSLI.Line_Number__c = '1122';
        NSLI.Standing_Order__c = true;
        NSLI.UDAC__c = 'SRL';
        
        obj.NatLinLstObj.add(NSLI);
        
        obj.save();
        
        
        Advice_Query__c objAQ = new Advice_Query__c();
        objAQ.Advice_Query__c = NSLI.id;
        objAQ.Process_Completed__c = false;
        objAQ.Frequent_Comments_Questions__c = 'Advice';
        objAQ.Advice_Query_Options__c = 'Empty Telephone Number';
        objAQ.Free_text__c = 'Testing';
        insert objAQ;
        obj.advID = objAQ.id;        
        obj.adviceQueryList= new List<Advice_Query__c>();
        obj.adviceQueryList.add(objAQ);
        obj.lstExisAdviceQuery.add(objAQ);
        obj.saveAdviceQuery();
        obj.editAdvice();
        
        //obj.nationalStagingLineItemId = NSLI.id;
        
        obj.deleteAdviceQuery();

        NSOS = new National_Staging_Order_Set__c();
        NSOS.Directory_Edition_Number__c = '4444';
        NSOS.Directory_Version__c = '1234';
        NSOS.Directory__c = dir.Id;
        NSOS.Directory_Number__c = dir.Directory_Code__c;    
        
        controller = new ApexPages.StandardController(NSOS);
        obj = new NationalManualEntry_V2(controller);
          obj.NatHdrObj = NSOS;
        obj.fetchDEByLSANo();
        
        //obj.fetchClientbyNumber();
       // obj.fetchCMRbyNumber();
       
        obj.save();
        Test.StopTest();
       }
       
       public static testMethod void natManualNegativeTest3(){
    
     Test.StartTest();
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1234';
        CMRAcct.Delinquency_Indicator__c=true;
        CMRAcct.Is_Active__c=true;
        insert CMRAcct;
        
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='2345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS.CMR_Name__c = CMRAcct.id;
        NSOS.Client_Name__c = null;
        NSOS.Client_Name_Text__c = ClientAcct.Name;
        NSOS.Client_Number__c = ClientAcct.Client_Number__c;
        NSOS.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS.Directory_Edition_Number__c = '4444';
        NSOS.Directory_Version__c = '1234';
        NSOS.Directory__c = dir.Id;
        NSOS.Directory_Number__c = dir.Directory_Code__c;
        NSOS.Late_Order__c=true;
        NSOS.Date__c=null;
        insert NSOS;
        
        National_Staging_Line_Item__c NSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLI.Line_Number__c = '11122';
        NSLI.UDAC__c = prod.ProductCode;
        insert NSLI;
        
        National_Staging_Line_Item__c NSLINegative = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLINegative.Line_Number__c = '1112';
        NSLINegative.UDAC__c = prod.ProductCode;
        NSLINegative.Directory_Section__c=null;
        NSLINegative.Directory_Heading__c=null;
        NSLINegative.National_Pricing__c=null;
        NSLINegative.Discount__c=null;
        insert NSLINegative;
        
        ApexPages.CurrentPage().getParameters().put('id', null);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLINegative.Id);     
        ApexPages.StandardController controller = new ApexPages.StandardController(NSOS);
        NationalManualEntry_V2 obj = new NationalManualEntry_V2(controller);
        // obj.fetchClientbyNumber();
        // obj.fetchCMRbyNumber();
        obj.processOLI();
        
        Test.StopTest();
  }
  
  
  
  public static testMethod void CoveringProcessOLI(){
    Test.StartTest();
        ID cmrAccRTID = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCMRRT, CommonMessages.accountObjectName);
        
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1234';
        CMRAcct.Delinquency_Indicator__c=false;
        CMRAcct.Is_Active__c=true;
        CMRAcct.National_Credit_Status__c='Approved';
        CMRAcct.RecordTypeId=cmrAccRTID;
        insert CMRAcct;
        
        Account CMRAcct1 = TestMethodsUtility.generateCMRAccount();
        CMRAcct1.accountnumber='1234';
        CMRAcct1.Delinquency_Indicator__c=false;
        CMRAcct1.Is_Active__c=true;
        CMRAcct1.National_Credit_Status__c='In Progress';
        CMRAcct1.RecordTypeId=cmrAccRTID;
        insert CMRAcct1;
        
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='2345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS.CMR_Name__c = CMRAcct1.id;
        NSOS.Client_Name__c = ClientAcct.id;
        NSOS.Client_Name_Text__c = ClientAcct.Name;
        NSOS.Client_Number__c = ClientAcct.Client_Number__c;
        NSOS.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS.Directory_Edition_Number__c = '4444';
        NSOS.Directory_Version__c = '1234';
        NSOS.Directory__c = dir.Id;
        NSOS.Directory_Number__c = dir.Directory_Code__c;
        NSOS.Late_Order__c=true;
        NSOS.Approved_to_Process__c=false;
        insert NSOS;
        
        National_Staging_Line_Item__c NSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLI.Line_Number__c = '11122';
        NSLI.UDAC__c = prod.ProductCode;
        insert NSLI;
        
        National_Staging_Line_Item__c NSLINegative = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLINegative.Line_Number__c = '1112';
        NSLINegative.UDAC__c = prod.ProductCode;
        NSLINegative.Directory_Section__c=null;
        NSLINegative.Directory_Heading__c=null;
        NSLINegative.National_Pricing__c=null;
        NSLINegative.Discount__c=null;
        insert NSLINegative;
        
        /*
        ApexPages.CurrentPage().getParameters().put('id', NSOS.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI.Id);     
        ApexPages.StandardController controller = new ApexPages.StandardController(NSOS);
        NationalManualEntry_V2 obj = new NationalManualEntry_V2(controller);
        obj.processOLI();
        
        NSOS.CMR_Name__c = null;
        update NSOS;
        
        obj.processOLI();
        */
        
        National_Staging_Order_Set__c NSOSNew = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOSNew.CMR_Name__c = CMRAcct.id;
        NSOSNew.Client_Name__c = ClientAcct.id;
        NSOSNew.Client_Name_Text__c = ClientAcct.Name;
        NSOSNew.Client_Number__c = ClientAcct.Client_Number__c;
        NSOSNew.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOSNew.Directory_Edition_Number__c = '4444';
        NSOSNew.Directory_Version__c = '1234';
        NSOSNew.Directory__c = dir.Id;
        NSOSNew.Directory_Number__c = dir.Directory_Code__c;
        NSOSNew.Late_Order__c=true;
        NSOSNew.Date__c=Date.today();
        NSOSNew.Is_Ready__c=false;
        insert NSOSNew;
        
        National_Staging_Line_Item__c NSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOSNew);
        NSLI1.Line_Number__c = '11122';
        NSLI1.UDAC__c = prod.ProductCode;
        insert NSLI1;
        
        National_Staging_Line_Item__c NSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOSNew);
        NSLI2.Line_Number__c = '1112';
        NSLI2.UDAC__c = prod.ProductCode;
        NSLI2.Directory_Section__c=null;
        NSLI2.Directory_Heading__c=null;
        NSLI2.National_Pricing__c=null;
        NSLI2.Discount__c=null;
        insert NSLI2;
        /*
       ApexPages.CurrentPage().getParameters().put('id', NSOSNew.Id);
       Apexpages.currentpage().getparameters().put('index', '0');   
       */
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI2.Id);     
        ApexPages.StandardController controller1 = new ApexPages.StandardController(NSOSNew);
        NationalManualEntry_V2 obj1 = new NationalManualEntry_V2(controller1);
      
         obj1.fetchCMRbyNumber();
      obj1.processOLI();
       obj1.save();
        obj1.fetchClientbyNumber();
        Test.StopTest();
  } 
  
 
   public static testMethod void CoveringProcessOLI2(){
    Test.StartTest();
        ID cmrAccRTID3 = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCMRRT, CommonMessages.accountObjectName);
        
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
       CMRAcct.accountnumber='1234';
       CMRAcct.Delinquency_Indicator__c=false;
       CMRAcct.Is_Active__c=true;
       CMRAcct.National_Credit_Status__c='Approved';
      CMRAcct.RecordTypeId=cmrAccRTID3;
        insert CMRAcct;
        
        Account CMRAcct1 = TestMethodsUtility.generateCMRAccount();
      CMRAcct1.accountnumber='1234';
       CMRAcct1.Delinquency_Indicator__c=false;
      CMRAcct1.Is_Active__c=true;
      CMRAcct1.National_Credit_Status__c='In Progress';
       CMRAcct1.RecordTypeId=cmrAccRTID3;
        insert CMRAcct1;
        
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
      ClientAcct.accountnumber='2345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        
        National_Staging_Order_Set__c NSOSNew = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOSNew.CMR_Name__c = CMRAcct.Id;
        NSOSNew.Client_Name__c = ClientAcct.Id;
        NSOSNew.Client_Name_Text__c = ClientAcct.Name;
        NSOSNew.Client_Number__c = ClientAcct.Client_Number__c;
        NSOSNew.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOSNew.Directory_Edition_Number__c = '4444';
        NSOSNew.Directory_Version__c = '1234';
        NSOSNew.Directory__c = dir.Id;
        NSOSNew.Directory_Number__c = dir.Directory_Code__c;
        NSOSNew.Late_Order__c=true;
        NSOSNew.Date__c=Date.today();
        NSOSNew.Is_Ready__c=false;
        insert NSOSNew;
         ApexPages.StandardController controller11 = new ApexPages.StandardController(NSOSNew);
        NationalManualEntry_V2 obj144 = new NationalManualEntry_V2(controller11);
        obj144.processOLI();
       
        
        
         National_Staging_Line_Item__c NSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOSNew);
        NSLI1.Line_Number__c = '11122';
        NSLI1.UDAC__c = prod.ProductCode;
        NSLI1.National_Graphics_ID__c='GRAPH1234567';
        NSLI1.Is_Changed__c=true;
        insert NSLI1;
        
        
      
        
     ApexPages.CurrentPage().getParameters().put('id', NSOSNew.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI1.Id);     
        ApexPages.StandardController controller1 = new ApexPages.StandardController(NSOSNew);
        NationalManualEntry_V2 obj1 = new NationalManualEntry_V2(controller1);
        
       obj1.fetchClientbyNumber();
        obj1.fetchCMRbyNumber();
        
       
     
        
        
        Test.StopTest();
  } 
  
  
  
   public static testMethod void natManualCommonLinesTestNeg2() {
            Test.StartTest();
              ID cmrAccRTID2 = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCMRRT, CommonMessages.accountObjectName);
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1784';
        CMRAcct.National_Credit_Status__c='Approved';
        CMRAcct.Is_Active__c=true;
        CMRAcct.RecordTypeId=cmrAccRTID2;
        insert CMRAcct;
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='1345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS23 = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS23.CMR_Name__c = CMRAcct.id;
        NSOS23.Client_Name__c = ClientAcct.id;
        NSOS23.Client_Name_Text__c = ClientAcct.Name;
        NSOS23.Client_Number__c =ClientAcct.Client_Number__c;
        NSOS23.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS23.Directory_Edition_Number__c = '4444';
        NSOS23.Directory_Version__c = '1234';
          NSOS23.Late_Order__c=false;
          NSOS23.Approved_to_process__c=false;
        NSOS23.Directory__c = dir.Id;
        NSOS23.Directory_Number__c = dir.Directory_Code__c;
        NSOS23.Is_Ready__c=true;
        
        insert NSOS23;
        
        
        National_Staging_Line_Item__c NSLI23 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS23);
        NSLI23.Line_Number__c = '11122';
        NSLI23.UDAC__c = prod.ProductCode;
        NSLI23.National_Graphics_ID__c='GRAPH1234567';
        NSLI23.Is_Changed__c=false;
         NSLI23.Directory_Heading__c = objDH.Id;
        NSLI23.National_Pricing__c = dirProdMap.Id;
        NSLI23.Directory_Section__c = dirSec.Id;
        NSLI23.Discount__c = 'Full Price';
        NSLI23.Standing_Order__c = true;
        NSLI23.Product2__c = prod.Id;
        
        insert NSLI23;
        
        National_Staging_Line_Item__c NSLI21 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS23);
        NSLI21.Line_Number__c = '1112';
        NSLI21.UDAC__c =null;
        NSLI21.Directory_Section__c=null;
        NSLI21.Directory_Heading__c=null;
        NSLI21.National_Pricing__c=null;
        NSLI21.Discount__c=null;
        
        
        
        insert NSLI21;
        
        
         Advice_Query__c adviceQuery = TestMethodsUtility.createAdviceQuery(NSLI23);
        Advice_Query__c adviceQueryExist = TestMethodsUtility.createAdviceQuery(NSLI23);
        ApexPages.CurrentPage().getParameters().put('id', NSOS23.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI23.Id);     
        ApexPages.StandardController controller = new ApexPages.StandardController(NSOS23);
        NationalManualEntry_V2 obj = new NationalManualEntry_V2(controller);
         obj.NatHdrObj = NSOS23;
         obj.advID = adviceQuery.Id;
       
         obj.processOLI();
         obj.save();
       
        Test.StopTest();
       }
       
        public static testMethod void natManualCommonLinesTestNeg4() {
            Test.StartTest();
              ID cmrAccRTID1 = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCMRRT, CommonMessages.accountObjectName);
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1784';
        CMRAcct.National_Credit_Status__c='Approved';
        CMRAcct.Is_Active__c=true;
        CMRAcct.RecordTypeId=cmrAccRTID1;
        insert CMRAcct;
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='1345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS24 = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS24.CMR_Name__c = CMRAcct.id;
        NSOS24.Client_Name__c = ClientAcct.id;
        NSOS24.Client_Name_Text__c = ClientAcct.Name;
        NSOS24.Client_Number__c =ClientAcct.Client_Number__c;
        NSOS24.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS24.Directory_Edition_Number__c = '4444';
        NSOS24.Directory_Version__c = '1234';
          NSOS24.Late_Order__c=false;
          NSOS24.Approved_to_process__c=false;
        NSOS24.Directory__c = dir.Id;
        NSOS24.Directory_Number__c = dir.Directory_Code__c;
        NSOS24.Is_Ready__c=true;
        
        insert NSOS24;
        
        
        National_Staging_Line_Item__c NSLI24 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS24);
        NSLI24.Line_Number__c = '11122';
        NSLI24.UDAC__c = prod.ProductCode;
        NSLI24.National_Graphics_ID__c='GRAPH1234567';
        NSLI24.Is_Changed__c=false;
         NSLI24.Directory_Section__c=null;
        NSLI24.Directory_Heading__c=null;
        NSLI24.National_Pricing__c=null;
        NSLI24.Discount__c=null;
        NSLI24.Standing_Order__c = true;
        NSLI24.Product2__c =null;
        
        insert NSLI24;
        
    
        
        
         Advice_Query__c adviceQuery = TestMethodsUtility.createAdviceQuery(NSLI24);
        Advice_Query__c adviceQueryExist = TestMethodsUtility.createAdviceQuery(NSLI24);
        ApexPages.CurrentPage().getParameters().put('id', NSOS24.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI24.Id);     
        ApexPages.StandardController controller = new ApexPages.StandardController(NSOS24);
        NationalManualEntry_V2 obj24 = new NationalManualEntry_V2(controller);
       obj24.NatHdrObj = NSOS24;
       
         obj24.processOLI();
         obj24.save();
       
        Test.StopTest();
       }
  
}