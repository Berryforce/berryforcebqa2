global class LOBJobCreationBatch Implements Database.Batchable <sObject> {	
	Id selectedDirId;
    Id selectedDirEdId; 
    String billingPartners = '';
    List<String> listSelBillingPartners = new List<String>();
    Boolean IsDMIGR = false;
    
    global LOBJobCreationBatch(Boolean IsDMIGR) {
    	this.IsDMIGR = IsDMIGR;
    }
    
	global LOBJobCreationBatch(Id selectedDirId, Id selectedDirEdId, List<String> listSelBillingPartners) {
		this.selectedDirId = selectedDirId;
		this.selectedDirEdId = selectedDirEdId;
        this.listSelBillingPartners = listSelBillingPartners;
		
		if(listSelBillingPartners.size() > 0) {
			for(String str : listSelBillingPartners) {
				billingPartners += str;
			}
		}
	}
	
    global Database.queryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id FROM Directory_Edition__c ';      
        if(IsDMIGR) {
        	set<String> setDEStatus = new set<String>{'BOTS'};
        	SOQL += 'WHERE Book_Status__c IN:setDEStatus';
        } else {
        	SOQL += 'LIMIT 1';
        }
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<Directory_Edition__c> listDirEdition) {
    	List<List_Of_Business_Job__c> listLOBJob = new List<List_Of_Business_Job__c>();
    	Set<Id> setOrderIds = new Set<Id>();
    	Set<Id> setTempOrderIds = new Set<Id>();
    	List<Order_Line_Items__c> listOLI;
        RecordType  objRT = CommonMethods.getRecordTypeDetailsByName('Order_Line_Items__c','National Order Line Item');  
        
        if(IsDMIGR) {
        	listOLI = [SELECT Listing__r.Phone__c, Account__r.X3l_External_Id__c, Account__r.Parent_3L_External_Id__c,Id,
                    Digital_Product_Requirement__r.business_phone_number_office__c,UnitPrice__c, Account__r.Account_Number__c,
                    Account__r.Parent.Account_Number__c, Telco__r.Name,Listing_Name__c,Listing__r.Name, Billing_Partner__c,Billing_Frequency__c, Canvass__c, 
                    Directory_Code__c, Order__c, Edition_Code__c, Media_Type__c, Telco_Invoice_Date__c, Service_End_Date__c, Service_Start_Date__c FROM 
                    Order_Line_Items__c WHERE RecordtypeId !=: objRT.Id AND Product2__r.Media_Type__c='Print' AND 
                    Directory_Edition__c =: listDirEdition AND Billing_Partner__c != null ORDER BY Billing_Partner__c, Account__r.Name];
        } else if(listSelBillingPartners.size() > 0) {        
            listOLI = [SELECT Listing__r.Phone__c, Account__r.X3l_External_Id__c, Account__r.Parent_3L_External_Id__c,Id,
                    Digital_Product_Requirement__r.business_phone_number_office__c,UnitPrice__c, Account__r.Account_Number__c,
                    Account__r.Parent.Account_Number__c, Telco__r.Name,Listing_Name__c,Listing__r.Name,
                    Billing_Partner__c,Billing_Frequency__c, Canvass__c, Directory_Code__c, Order__c,
                    Edition_Code__c, Media_Type__c, Telco_Invoice_Date__c, Service_End_Date__c, Service_Start_Date__c FROM Order_Line_Items__c WHERE 
                    RecordtypeId !=: objRT.Id AND isCanceled__c = false AND Product2__r.Media_Type__c='Print' AND Billing_Partner__c in : listSelBillingPartners 
                    AND Directory__c=: selectedDirId AND Directory_Edition__c =: selectedDirEdId AND Billing_Partner__c != null ORDER BY Billing_Partner__c, Account__r.Name];
        } else {
            listOLI = [SELECT Listing__r.Phone__c, Account__r.X3l_External_Id__c, Account__r.Parent_3L_External_Id__c, Id, 
                    Digital_Product_Requirement__r.business_phone_number_office__c,UnitPrice__c, Account__c,Account__r.Account_Number__c,
                    Account__r.Parent.Account_Number__c, Telco__r.Name,Listing_Name__c,Listing__r.Name, Billing_Partner__c,Billing_Frequency__c, Canvass__c, 
                    Directory_Code__c, Order__c, Edition_Code__c, Media_Type__c, Telco_Invoice_Date__c, Service_End_Date__c, Service_Start_Date__c FROM 
                    Order_Line_Items__c WHERE RecordtypeId !=: objRT.Id AND isCanceled__c = false AND Product2__r.Media_Type__c = 'Print' AND 
                    Directory__c =: selectedDirId AND Directory_Edition__c =: selectedDirEdId AND Billing_Partner__c != null ORDER BY Billing_Partner__c, Account__r.Name];
        }  
        if(listOLI.size() > 0) {
        	for(Order_Line_Items__c OLI : listOLI) {
        		setOrderIds.add(OLI.Order__c);
        	}
        	
        	for(Id orderId : setOrderIds) {
        		setTempOrderIds.add(orderId);
        		if(setTempOrderIds.size() == Integer.valueOf(Label.Order_Set_Ids_Size_for_LOB_Job_Batch)) {
        			String orderIds = '';
        			for(Id orderId1 : setTempOrderIds) {
		        		orderIds += orderId1 + ';';
		        	}
		        	listLOBJob.add(newLOBJob(billingPartners, selectedDirEdId, selectedDirId, orderIds.removeEnd(';')));
		        	setTempOrderIds = new Set<Id>();
        		}
        	}
        	if(setTempOrderIds.size() > 0) {
        		String orderIds = '';
    			for(Id orderId1 : setTempOrderIds) {
	        		orderIds += orderId1 + ';';
	        	}
	        	listLOBJob.add(newLOBJob(billingPartners, selectedDirEdId, selectedDirId, orderIds.removeEnd(';')));
        	}
        	insert listLOBJob;
        } 
    }

    global void finish(Database.BatchableContext bc) {
    	LOBReportGenerationFromJobBatch LOB;
    	if(IsDMIGR) {
    		LOB = new LOBReportGenerationFromJobBatch(true);
    	} else {
    		LOB = new LOBReportGenerationFromJobBatch(selectedDirId, selectedDirEdId);
    	}
    	Database.executeBatch(LOB, Integer.valueOf(Label.LOB_Report_Generation_From_Job_Batch_Size));
    }
    
    List_Of_Business_Job__c newLOBJob(String billPartners, Id dirEditionId, Id dirId, String orderIds) {
		List_Of_Business_Job__c LOBJob = new List_Of_Business_Job__c(LBJ_Billing_Partners__c = billPartners, LBJ_Directory_Edition_Id__c = dirEditionId, 
										LBJ_Directory_Id__c = dirId, LBJ_Order_Ids__c = orderIds);
		return LOBJob;
	} 
}