public class SalesInvoiceHandlerController {
    /***************************************/
    /* Mapping the invoice id to invoice line item based on Autonumber values from Sales invoice.
    /* INserting a sales invoice line item.
    /* This method used in createSalesInvoice method with same controller.
    /* Parameter1 : list of inserted sales invoice
    /* Parameter2 : map of invoice line item. Key is AutoNumber, Value is list of  sales invoice line item. 
    /***************************************/
    public static void insertSalesInvoiceLineItems(list<c2g__codaInvoice__c> lstSI, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSILI) {
        list<c2g__codaInvoiceLineItem__c> insertSILI = new list<c2g__codaInvoiceLineItem__c>();
        for(c2g__codaInvoice__c iterator : lstSI) {
            list<c2g__codaInvoiceLineItem__c> lstSalesInvoiceLineItem = mapSILI.get(Integer.valueOf(iterator.Auto_Number__c));
            for(c2g__codaInvoiceLineItem__c iteratorLI : lstSalesInvoiceLineItem) {
                if(iteratorLI.c2g__UnitPrice__c == Null){
                    iteratorLI.c2g__UnitPrice__c = 0.0;
                }
                iteratorLI.c2g__Invoice__c = iterator.Id;
                insertSILI.add(iteratorLI);
            }
        }
        
        if(insertSILI.size() > 0) {
            System.debug('@@@@@insertSILI '+insertSILI);
            insert insertSILI;
        }
    }
    
    public static list<c2g__codaInvoice__c> createSalesInvoiceByOLIWithBundle(list<Order_Line_Items__c> lstOLI) {
        Map<String, Id> dimensionsMapValues = new Map<String, Id>(); 
               
        dimensionsMapValues = DimensionsValues.getDimensionByOLI(lstOLI);
        system.debug('Dimensions Map Values are ' + dimensionsMapValues);
        
        map<String, list<Order_Line_Items__c>> mapOLIByBundle = new map<String, list<Order_Line_Items__c>>();
        map<String, Double> mapOLIByPkgIdSalesPrice = new map<String, Double>();
        list<c2g__codaInvoice__c> insertSalesInvoice = new list<c2g__codaInvoice__c>();
        map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItem = new map<Integer, list<c2g__codaInvoiceLineItem__c>>();
        Integer iCount = 0;
        for(Order_Line_Items__c iterator : lstOLI) {
        
            //Updated By Ankit for Zero Dollar Invoice
            if(!mapOLIByPkgIdSalesPrice.containskey(iterator.Package_Id__c)){
                mapOLIByPkgIdSalesPrice.put(iterator.Package_Id__c,iterator.UnitPrice__c); 
            }
            else{ 
                mapOLIByPkgIdSalesPrice.put(iterator.Package_Id__c,mapOLIByPkgIdSalesPrice.get(iterator.Package_Id__c)+iterator.UnitPrice__c);
            }
            
            if(!mapOLIByBundle.containsKey(iterator.Package_Id__c)) {
                mapOLIByBundle.put(iterator.Package_Id__c, new list<Order_Line_Items__c>());
            }
            mapOLIByBundle.get(iterator.Package_Id__c).add(iterator);
        }
        
        if(mapOLIByBundle.size() > 0) {
            for(String strPkg : mapOLIByBundle.keySet()) {
                iCount++;
                if(mapOLIByBundle.get(strPkg) != null) {
                    //Update By: Ankit for checking Zero Dollar Invoices
                    //Date: 4/2/2015
                    if(mapOLIByPkgIdSalesPrice.containskey(strPkg) && mapOLIByPkgIdSalesPrice.get(strPkg)>0){
                    //Updated by :Ankit Nigam
                    //Date: 29/04/2014
                    //Invoice Date Should be Next Billing Date for Digital Product
                    insertSalesInvoice.add(CommonRecurHandlerController_V1.newSalesInvoice(mapOLIByBundle.get(strPkg)[0], CommonMessages.accountingCurrencyId, iCount, false, false, true, dimensionsMapValues));
                    for(Order_Line_Items__c iterator : mapOLIByBundle.get(strPkg)) {
                        if(!mapSalesInvoiceLineItem.containsKey(iCount)) {
                            mapSalesInvoiceLineItem.put(iCount, new list<c2g__codaInvoiceLineItem__c>());
                        }
                        mapSalesInvoiceLineItem.get(iCount).add(CommonRecurHandlerController_V1.newalesInvoiceLineItemForCronJob(iterator, null, null, dimensionsMapValues, null,null));
                    }
                  }
               }
            }
            
            if(insertSalesInvoice.size() > 0) {                
                try { 
                    insert insertSalesInvoice;
                } 
                catch(Exception ex) {
                    String error = ex.getMessage() + '. ';
                    List<String> errorMessages = c2g.CODAExtensionContext.getErrorMessages();
                    for( String errorMessage : errorMessages ) { 
                        if( errorMessage != null ) error += '; ' + errorMessage; 
                    }
                    throw new InvoiceGeneratorHelperException(error);
                }
                insertSalesInvoiceLineItems(insertSalesInvoice, mapSalesInvoiceLineItem);
                return insertSalesInvoice;
            }
        }
        return null;
    }
    
    public static list<c2g__codaInvoice__c> createSalesInvoiceByOLI(list<Order_Line_Items__c> lstOLI, boolean bFlag) {
        Map<String, Id> dimensionsMapValues = new Map<String, Id>(); 
        
        dimensionsMapValues = DimensionsValues.getDimensionByOLI(lstOLI);
        system.debug('Dimensions Map Values are ' + dimensionsMapValues);
            
        list<c2g__codaInvoice__c> insertSalesInvoice = new list<c2g__codaInvoice__c>();
        map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItem = new map<Integer, list<c2g__codaInvoiceLineItem__c>>();
        Integer iCount = 0;
        for(Order_Line_Items__c iterator : lstOLI) {
            iCount++;
            String tempAccountID;
            if(iterator.Payment_Method__c.equals(CommonMessages.telcoPaymentMethod)) {
                tempAccountID = iterator.Billing_Partner_Account__c;
            }
            else {
                tempAccountID = iterator.Account__c;
            }
            //insertSalesInvoice.add(CommonRecurHandlerController.newSalesInvoice(iterator.Opportunity__c, tempAccountID, iterator.Billing_Contact__c, iterator.Opportunity__r.Name, iterator.Order_Group__c, CommonMessages.accountingCurrencyId, iCount, iterator.Payment_Method__c, iterator.Account__c));
            //Update By: Ankit for checking Zero Dollar Invoices
            //Date: 4/2/2015
            if(iterator.UnitPrice__c>0){
                insertSalesInvoice.add(CommonRecurHandlerController_V1.newSalesInvoice(iterator, CommonMessages.accountingCurrencyId, iCount, true, false, false, dimensionsMapValues));
                if(!mapSalesInvoiceLineItem.containsKey(iCount)) {
                    mapSalesInvoiceLineItem.put(iCount, new list<c2g__codaInvoiceLineItem__c>());
                }
                mapSalesInvoiceLineItem.get(iCount).add(CommonRecurHandlerController_V1.newalesInvoiceLineItemForCronJob(iterator, null, null, dimensionsMapValues, null,null));
            }
        }

        if(insertSalesInvoice.size() > 0) {
            for(c2g__codaInvoice__c ite : insertSalesInvoice) {
                system.debug('Stepp ' + ite.c2g__Account__c);
                system.debug('Stepp ' + ite.Customer_Name__c);
            }
            try { 
                insert insertSalesInvoice;
            } 
            catch(Exception ex) {
                String error = ex.getMessage() + '. ';
                List<String> errorMessages = c2g.CODAExtensionContext.getErrorMessages();
                for( String errorMessage : errorMessages ) { 
                    if( errorMessage != null ) error += '; ' + errorMessage; 
                }
                throw new InvoiceGeneratorHelperException(error);
            }
            insertSalesInvoiceLineItems(insertSalesInvoice, mapSalesInvoiceLineItem);
            return insertSalesInvoice;
        }
        return null;
    }
    /***** Added by vikas on 06/05/2014 for cration of sales invoice on canellation *********/
    public static list<c2g__codaInvoice__c> createSalesInvoiceForCancel (list<Order_Line_Items__c> lstOLI) { 
        map<String, list<Order_Line_Items__c>> mapOLIByBundle = new map<String, list<Order_Line_Items__c>>();
        list<c2g__codaInvoice__c> insertSalesInvoice = new list<c2g__codaInvoice__c>();
        map<Integer,List<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItem = new map<Integer,List<c2g__codaInvoiceLineItem__c>>();
        Integer iCount = 0;
        Map<String, Id> dimensionsMapValues = new Map<String, Id>();            
        dimensionsMapValues = DimensionsValues.getDimensionByOLI(lstOLI);
        map<String, Double> mapOLIByPkgIdSalesPrice = new map<String, Double>();
        List<Product2> productList=[select id from Product2 where name='Cancellation Fee' limit 1];
        Id ProductId;
        if(productList.size()>0)
           ProductId=productList[0].id;
        for(Order_Line_Items__c OLI :lstOLI)
        {
            if(OLI.media_type__c=='Digital')
            {
                If(!mapOLIByBundle.ContainsKey(OLI.Package_ID__c))
                {
                    mapOLIByBundle.put(OLI.Package_ID__c,new List<Order_Line_Items__c>{OLI});
                }

                else
                {
                     mapOLIByBundle.get(OLI.Package_ID__c).add(OLI);
                }
                
                //Updated By Ankit for Zero Dollar Invoice
                if(!mapOLIByPkgIdSalesPrice.containskey(OLI.Package_Id__c)){
                    mapOLIByPkgIdSalesPrice.put(OLI.Package_Id__c,OLI.UnitPrice__c); 
                }
                else{ 
                    mapOLIByPkgIdSalesPrice.put(OLI.Package_Id__c,mapOLIByPkgIdSalesPrice.get(OLI.Package_Id__c)+OLI.UnitPrice__c);
                }
            }
        }
        for(string s:mapOLIByBundle.keySet())
        {
            iCount++;
           //Update By: Ankit for checking Zero Dollar Invoices
           //Date: 4/2/2015
           if(mapOLIByPkgIdSalesPrice.containskey(s) && mapOLIByPkgIdSalesPrice.get(s)>0){
             insertSalesInvoice.add(newSalesInvoiceForCancel(mapOLIByBundle.get(s)[0], CommonMessages.accountingCurrencyId, iCount, false, false, true));                 
                for(Order_Line_Items__c tempOLI:mapOLIByBundle.get(s))
                {
                    if(!mapSalesInvoiceLineItem.containsKey(iCount)) 
                    {
                        mapSalesInvoiceLineItem.put(iCount, new list<c2g__codaInvoiceLineItem__c>());
                    }
                    mapSalesInvoiceLineItem.get(iCount).add(newalesInvoiceLineItemForCancel(tempOLI, null, null, dimensionsMapValues, null,null,ProductId));            
                }
            }

        }       
        if(insertSalesInvoice.size() > 0) {                
            try { 
                insert insertSalesInvoice;
            } 
            catch(Exception ex) {
                String error = ex.getMessage() + '. ';
                List<String> errorMessages = c2g.CODAExtensionContext.getErrorMessages();
                for( String errorMessage : errorMessages ) { 
                    if( errorMessage != null ) error += '; ' + errorMessage; 
                }
                throw new InvoiceGeneratorHelperException(error);
            }
            insertSalesInvoiceLineItemsForCancel(insertSalesInvoice, mapSalesInvoiceLineItem);
            return insertSalesInvoice;
        }
        return null;
    }
    public static c2g__codaInvoice__c newSalesInvoiceForCancel(Order_Line_Items__c iterator, String ffCurrency, Integer iCountTemp, boolean bFlagForInvoiceDate, boolean bFlagNextBilling, boolean bFladFirstDigitalInvoice) 
    {
        system.debug('********Service Start' + iterator.Service_Start_Date__c);
        system.debug('********Service End' + iterator.Service_End_Date__c);
        c2g__codaInvoice__c newInvoice = new c2g__codaInvoice__c(c2g__InvoiceDescription__c = iterator.Opportunity__r.Name, c2g__CustomerReference__c = iterator.Opportunity__c,
                            c2g__Opportunity__c = iterator.Opportunity__c,c2g__InvoiceDate__c=System.today(), c2g__DueDate__c = System.today().addMonths(1), Billing_Contact__c = iterator.Billing_Contact__c, 
                            c2g__InvoiceCurrency__c = ffCurrency,c2g__PrintedText1AllowEdit__c = false, c2g__PrintedText2AllowEdit__c = false, c2g__PrintedText3AllowEdit__c = false,
                            c2g__PrintedText4AllowEdit__c = false, c2g__PrintedText5AllowEdit__c = false, ffbilling__CopyAccountValues__c = true, 
                            Service_Start_Date__c = iterator.Service_Start_Date__c, 
                            ffbilling__DeriveDueDate__c = true, ffbilling__DerivePeriod__c = true, 
                            ffbilling__DeriveCurrency__c = true, Transaction_Type__c = 'F - Fee',
                            Service_End_Date__c = iterator.Service_End_Date__c, 
                            SI_Billing_Partner__c = iterator.Billing_Partner__c,
                            Total_Month_Invoice__c = 1,SI_Payment_Method__c=iterator.Payment_Method__c);
        
        if(iterator.Order_Group__c != null) {
            newInvoice.Order_Set__c = iterator.Order_Group__c;
        }
        if(iterator.Payment_Method__c.equals(CommonMessages.telcoPaymentMethod)) {
            newInvoice.c2g__Account__c = iterator.Billing_Partner_Account__c;
            newInvoice.Customer_Name__c = iterator.Account__c;
        }
        else {
            newInvoice.c2g__Account__c = iterator.Account__c;
            newInvoice.Customer_Name__c = iterator.Account__c;
        }
        
        if(String.isNotBlank(iterator.Payment_Method__c)) {
            if(iterator.Payment_Method__c.equals(CommonMessages.ccPaymentMethod)) {
                newInvoice.Payment_Type__c = CommonMessages.ccPaymentMethod;
            }
            else if(iterator.Payment_Method__c.equals(CommonMessages.ACHPaymentMethod)){
                newInvoice.Payment_Type__c = CommonMessages.ACHPaymentType;
            }
        }
        
        if(iCountTemp > 0) {
            newInvoice.Auto_Number__c = iCountTemp;
        }
        return newInvoice;
    }    
    public static c2g__codaInvoiceLineItem__c newalesInvoiceLineItemForCancel(Order_Line_Items__c iterator, Id salesInvoiceId, String productState, Map<String, Id> dimensionsMapValues, c2g__codaCreditNoteLineItem__c objSCLI,Integer SCNiCount,Id productId) 
    {
     
        c2g__codaInvoiceLineItem__c addSILI = new c2g__codaInvoiceLineItem__c(ffbilling__DeriveUnitPriceFromProduct__c = false, 
                ffbilling__UseProductInformation__c = false, ffbilling__CalculateIncomeSchedule__c = true, ffbilling__CalculateTaxValue1FromRate__c = false, 
                ffbilling__CalculateTaxValue2FromRate__c = false, ffbilling__CalculateTaxValue3FromRate__c = false, ffbilling__DeriveLineNumber__c = true, 
                ffbilling__DeriveTaxRate1FromCode__c = false, ffbilling__DeriveTaxRate2FromCode__c = false, ffbilling__DeriveTaxRate3FromCode__c = false, 
                ffbilling__InternalCalculateIS__c = false, ffbilling__SetTaxCode1ToDefault__c = false, ffbilling__SetTaxCode2ToDefault__c = false,                 
                ffbilling__SetTaxCode3ToDefault__c = false, c2g__TaxValue1__c = 0.0, c2g__Quantity__c = 1,Order_Line_Item__c = iterator.ID,c2g__Product__c=productId,Successful_Payments__c=iterator.Successful_Payments__c,Billing_Frequency__c=iterator.Billing_Frequency__c,Payments_Remaining__c=iterator.Payments_Remaining__c);
        
        if(iterator.Directory__c != null && iterator.Directory_Edition__c != null){        
            addSILI.Directory__c = iterator.Directory__c;
            addSILI.Directory_Edition__c = iterator.Directory_Edition__c;
        }

        //System.debug('*********** salesinvoice id is: '+salesInvoiceId);
        if(salesInvoiceId != null) 
        {
            addSILI.c2g__Invoice__c = salesInvoiceId;
        }
           System.debug('$$$$$OCancelation_Fee__c '+iterator.Cancelation_Fee__c); 
        addSILI.c2g__UnitPrice__c=iterator.Cancelation_Fee__c;
        /* Populating Dimensions */
        if(dimensionsMapValues.size()>0) {
            DimensionsValues.assignDimensions(iterator, null, null, addSILI, dimensionsMapValues);   
        }         
        return addSILI;
    }    
    public static void insertSalesInvoiceLineItemsForCancel(list<c2g__codaInvoice__c> lstSI, map<Integer, List<c2g__codaInvoiceLineItem__c>> mapSILI) {
        list<c2g__codaInvoiceLineItem__c> insertSILI = new list<c2g__codaInvoiceLineItem__c>();
        for(c2g__codaInvoice__c iterator : lstSI) 
        {
            System.debug('_____________ '+iterator);
            System.debug('_____________ '+mapSILI);
            c2g__codaInvoiceLineItem__c SalesInvoiceLineItem=new c2g__codaInvoiceLineItem__c();
            list<c2g__codaInvoiceLineItem__c> lstSalesInvoiceLineItem = mapSILI.get(Integer.valueOf(iterator.Auto_Number__c));
            for(c2g__codaInvoiceLineItem__c iteratorLI : lstSalesInvoiceLineItem) {
                iteratorLI.c2g__Invoice__c = iterator.Id;                    
                insertSILI.add(iteratorLI);
            }            
        }
        
        if(insertSILI.size() > 0) 
        {
            try { 
                insert insertSILI;
             } 
              catch(Exception ex) 
                    {
                         system.debug('**********  in catch an error occured');
                        String error = ex.getMessage() + '. ';
                        List<String> errorMessages = c2g.CODAExtensionContext.getErrorMessages();
                        for( String errorMessage : errorMessages ) 
                        { 
            
                          if( errorMessage != null ) error += '; ' + errorMessage; 
            
                        }
                        system.debug('**********  error is= '+error);
                        throw new InvoiceGeneratorHelperException(error);
                    }       
                       
        }
    }         
}