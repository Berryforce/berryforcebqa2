//CommonFunctions Test Class
@isTest(SeeAllData=True)
public class CommonFunctionsTest{
static testmethod void CFTest(){


//Data from utility
Account newAccount = TestMethodsUtility.createAccount('customer');
Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.createproduct();
        PricebookEntry newPriceBookEntry =  [Select Id, UseStandardPrice, UnitPrice, Product2Id, Pricebook2Id, IsActive from PricebookEntry where Product2Id = :newProduct.Id limit 1];
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        //setOppId.add(newOpportunity.Id);
        OpportunityLineItem newOLI = TestMethodsUtility.generateOpportunityLineItem();
        newOLI.PricebookEntryId = newPriceBookEntry.Id;
        newOLI.OpportunityId = newOpportunity.Id;
        newOLI.Transaction_ID__c = 'Test1';
        insert newOLI;
       // setTransId.add('Test1');
       // setOpppLineId.add(newOLI.id);
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
     //   setOrLIId.add(newOrLI.Id);
//End of data from utility

//declare all sets

case case1 = TestMethodsUtility.createcase('CS Claim', newAccount);
set<Id> caseID = new set<Id>();
caseID.add(case1.id);

set<String> caseSID = new set<string>();
caseSID.add('case1');
set<id> salesinvoiceid = new set<id>();
c2g__codaInvoice__c newSalesInvoice = TestMethodsUtility.createSalesInvoice(newAccount, newOpportunity);
set<Id> invoiceID = new set<id>();
invoiceID.add(newSalesInvoice.id);
set<Id> opportunityId = new set<id>();
opportunityId.add(newOpportunity.id);
c2g__codaCreditNote__c salescreditnote = TestMethodsUtility.createSalesCreditNote(newSalesInvoice,newAccount);
set<Id> cnID = new set<Id>();
cnID.add(salescreditnote.id);
pymt__PaymentX__c payment = TestMethodsUtility.createPaymentX(newSalesInvoice,newOpportunity);
set<String> setPymtStatus = new set<string>();
setPymtStatus.add('new');


Test.startTest();
//CommonFunctions.getRecordTypeNameIDByObjectName('Account');
//CommonFunctions.getRecordTypeDeveloperNameIDByObjectName('Account');
CommonFunctions.getAccountingCurrency();
//CommonFunctions.getProcessInstanceWorkitemByTargetObjectId(targetobj.id);
CommonFunctions.getQueueByName('queuename');
CommonFunctions.getPaymentDetailsBYOpportunity(opportunityId);
CommonFunctions.getSlaesInvoiceLineItem(invoiceID);
CommonFunctions.getPaymentsByInvoiceID(salesinvoiceid);
CommonFunctions.getCaseByID(caseID);
CommonFunctions.getCaseByID(caseSID);
CommonFunctions.getSalesCreditNoteTotalSumByCase(caseID);
CommonFunctions.getSalesCreditNoteById(cnID);
CommonFunctions.getSalesCreditNoteCount(caseID);
CommonFunctions.getDeclinedInvoicePaymentDynSOQL(setPymtStatus,'TODAY');
CommonFunctions.getDeclinedInvoicePayment(setPymtStatus);
Test.stopTest();

}


}