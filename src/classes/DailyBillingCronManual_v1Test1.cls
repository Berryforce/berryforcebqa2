@isTest
private class DailyBillingCronManual_v1Test1{

    static testMethod void testDailyBillingCronManual_v1 (){
        Product2 prdt = CommonUtility.createPrdtTest();
      prdt.Product_Type__c = 'iYP Bronze';

      Product2 prdt1 = CommonUtility.createPrdtTest();
      prdt1.Product_Type__c = 'VP5';
      prdt1.Product_or_Addon__c = 'Add-on';

      Account acct = TestMethodsUtility.createAccount('telco');
      Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
      Canvass__c c = TestMethodsUtility.createCanvass(telco);
        
      Account a = TestMethodsUtility.createAccount('cmr');
      //a.TalusAccountId__c = 'afdfd132323';
      //insert a;

      Contact cnt = CommonUtility.createContactTest(a);
      insert cnt;

      Opportunity oppty = CommonUtility.createOpptyTest(a);
      insert oppty;

      Order__c ord = CommonUtility.createOrderTest(a);
      insert ord;

      Order_Group__c ordGrp = CommonUtility.createOGTest(a, ord, oppty);    
      insert ordGrp;
      
      Directory__c dir = TestMethodsUtility.createDirectory();
      
      Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = c.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS-1');
      insert DE;

      Order_Line_Items__c OLI = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
      OLI.Product2__c = prdt.Id;
      OLI.Effective_Date__c = system.Today();
      OLI.Status__c = 'Fulfilled';
      OLI.isCanceled__c = true;
      OLI.Is_Handled__c=false;
      insert OLI;

      Order_Line_Items__c OLI1 = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
      OLI1.Product2__c = prdt1.Id;
       OLI1.Is_Handled__c=false;
      //OLI1.Effective_Date__c = system.Today();
      insert OLI1;      
        
       
        Order_Line_Items__c firstOLI = OLI;
         firstOLI.Is_Handled__c=false;
        firstOLI.Talus_Go_Live_Date__c = Null; //system.today();
        firstOLI.Telco_Invoice_Date__c = system.today();
        firstOLI.Directory_Edition__c = DE.Id;
        firstOLI.CMR_Name__c = a.Id;
        firstOLI.Action_Code__c = 'New';
        firstOLI.Media_Type__c = 'Print';
        //added by praveen
        firstOLI.Print_First_Bill_Date__c = system.today();
        firstOLI.Order_Anniversary_Start_Date__c = system.today();
        
        firstOLI.Billing_End_Date__c = system.today()+2;
        update firstOLI;
      
      
        
        Apexpages.Standardcontroller ctrl = new Apexpages.Standardcontroller(firstOLI);
        DailyBillingCronManual_v1 DBCMcls = new DailyBillingCronManual_v1(ctrl);
        
            
        DBCMcls.orderLI = firstOLI;
        
        Test.startTest();
        DBCMcls.saveMonthlyPrint();
        DBCMcls.calculateCurrentAndPendingSpend();
        DBCMcls.startSensitiveHeadingUpdateBatch();
        DBCMcls.initiateBoostProcess();
        DBCMcls.unitPriceBatch();
        Test.stopTest();
        
      
       
    }
    
  
}