public with sharing class DailyBillingCronManual_v1 {
    //getters and setters
    public Order_Line_Items__c orderLI {get;set;}
    public string fsodDate{get;set;}
    public Directory_Edition__c dirEdition {get;set;}
    public National_Staging_Order_Set__c objNSOS {get;set;}
    
    public DailyBillingCronManual_v1(ApexPages.StandardController controller) {
        orderLI = new Order_Line_Items__c();
        dirEdition = new Directory_Edition__c();
        objNSOS = new National_Staging_Order_Set__c();
    }
    
    public pageReference saveFirstMonth() {
        System.debug('Testingg orderLI.Billing_End_Date__c '+orderLI.Billing_End_Date__c);
        MonthlyCronCreationBatch  objPFMB = new MonthlyCronCreationBatch(orderLI.Billing_End_Date__c, CommonMessages.MCFMPMediaType);
        if(!Test.isRunningTest()) {
            database.executebatch(objPFMB,50);
        }
        return null;
    }
    
    public void P4PPrintCancelBath() {
        MonthlyCronCreationBatch  obj = new MonthlyCronCreationBatch(orderLI.Billing_End_Date__c, CommonMessages.MCP4PPrintCancelMediaType);
        if(!Test.isRunningTest()) {
            database.executebatch(obj,50);
        }
    }
    
    public void P4PDigitalCancelBath() {
        MonthlyCronCreationBatch  obj = new MonthlyCronCreationBatch(orderLI.Billing_End_Date__c, CommonMessages.MCP4PDigitalCancelMediaType);
        if(!Test.isRunningTest()) {
            database.executebatch(obj,50);
        }
    }
    
    public pageReference saveModifyOLI() {
        System.debug('Testingg orderLI.Billing_End_Date__c '+orderLI.Billing_End_Date__c);
        MOLIProcessforEffective objMP = new MOLIProcessforEffective(orderLI.Billing_End_Date__c);
        Integer intBatchSize = Integer.valueOf(label.MOLI_batch_job_size);
        if(!Test.isRunningTest()) {
            database.executebatch(objMP,intBatchSize);
        }
        return null;
    }
    
    public pageReference saveCoreMigratedModifyOLI() {
        System.debug('Testingg orderLI.Billing_End_Date__c '+orderLI.Billing_End_Date__c);
        MOLIProcessforEffective objMP = new MOLIProcessforEffective(orderLI.Billing_End_Date__c, ' and Core_Opportunity_Line_ID__c != null');
        Integer intBatchSize = Integer.valueOf(label.MOLI_Process_batch_size);
        if(!Test.isRunningTest()) {
            database.executebatch(objMP,intBatchSize);
        }
        return null;
    }
    
    public void startNationalRenewalCron() {
          System.debug('###########'+orderLI.Billing_End_Date__c);
        /* if(orderLI.Billing_End_Date__c != null) {
             List<Directory_Edition__c> DEList=new List<Directory_Edition__c>();
             DEList=[Select id,Name,Directory__c,(Select id from National_Staging_Order_Sets__r) from Directory_Edition__c where Future_Order_Start_Date__c=:orderLI.Billing_End_Date__c AND Book_Status__c ='BOTS'];
             System.debug('###########'+DEList);
             if(DEList.size()>0) NationalOrderSetRenewalHandler.RenewNationalOrder(DEList);
         }
        */
        NationalOrderSetRenewalCron  objDBM = new NationalOrderSetRenewalCron(orderLI.Billing_End_Date__c);
        if(!Test.isRunningTest()) {
            database.executebatch(objDBM,1);
        }
    }
    
    public pageReference saveMonthlyDigital() {
        System.debug('Testingg orderLI.Billing_End_Date__c '+orderLI.Billing_End_Date__c);
        MonthlyCronCreationBatch objDBM = new MonthlyCronCreationBatch(orderLI.Billing_End_Date__c, CommonMessages.digitalMediaType);
        if(!Test.isRunningTest()) {
            database.executebatch(objDBM, Integer.valueOf(Label.Monthly_Cron_Batch_Size));
        }
        return null;
    }
    
    public pageReference saveMonthlyPrint() {       
        //Added & commented by Manish jhingran for ATP-2708         
        System.debug('Testingg orderLI.Billing_End_Date__c '+orderLI.Billing_End_Date__c);
        MonthlyCronCreationBatch objPMB = new MonthlyCronCreationBatch(orderLI.Billing_End_Date__c, 'Print');
        if(!Test.isRunningTest()) {
            database.executebatch(objPMB, Integer.valueOf(Label.Monthly_Cron_Batch_Size));
        }
        return null;
    }
    
    public void calculateCurrentAndPendingSpend() {
        CurrentAndPendingSpendBatch obj = new CurrentAndPendingSpendBatch();
        /* Batch size has been set as 10 to avoid DML Governor Limit */
        if(!Test.isRunningTest()) {
            Database.executeBatch(obj, 10);
        }
    }
    
    public void startSensitiveHeadingUpdateBatch() {
        SensitiveHeadingUpdateBatch obj = new SensitiveHeadingUpdateBatch();
        if(!Test.isRunningTest()) {
            Database.executeBatch(obj, 5);
        }
    }
    
    public void initiateBoostProcess() {
        Date TodayDate = Date.today();
        string BookStatus = 'BOTS';
        set<Id> setOpportunityID = new set<Id>();
        String soql = 'Select Id,Book_Status__c,Final_Auto_Renew_Job__c,Directory__c from Directory_Edition__c where Book_Status__c = : BookStatus '+' AND Final_Auto_Renew_Job__c =: '+'TodayDate';
        List<Directory_edition__c> DirEditionLst = database.query(soql);
        set<Id> objDirId = new set<Id>();
        for(Directory_Edition__c objDE : DirEditionLst){
            objDirId.add(objDE.Directory__c);
        }
        system.debug('****Directory id****'+objDirId);
        map<Id, directory_edition__c> mapDEBOTS = new map<Id, directory_edition__c>([Select Id,Book_Status__c,Directory__c from directory_edition__c where Directory__c IN :objDirId AND Book_Status__c = 'BOTS-1']);
        if(mapDEBOTS.size() > 0) {
            list<Order_Line_Items__c> oliLst = [Select Id,Name,Opportunity__c,Directory_Edition__c,Is_Handled__c from Order_Line_Items__c where Directory_Edition__c IN :mapDEBOTS.keySet() AND Is_Handled__c = false];
            if(oliLst.size()>0) {
                for(Order_Line_Items__c iterator : oliLst) {
                    system.debug('****Optyy id****'+iterator.Opportunity__c);
                    setOpportunityID.add(iterator.Opportunity__c);
                }
            }
        }
        if(!Test.isRunningTest()) {
            database.executebatch(new BoostRenewalAccountsBatchController(setOpportunityID),2);
        }
    }
    
    public void unitPriceBatch() {
        system.debug('Due date is ' + dirEdition.National_Rates_Due__c);
        if(dirEdition.National_Rates_Due__c!=null){
       // if(orderLI.Sent_to_fulfillment_on__c != null) {
            UnitPriceUpdateBatch obj = new UnitPriceUpdateBatch(dirEdition.National_Rates_Due__c);
            if(!Test.isRunningTest()) {
                Database.executeBatch(obj);
            }
        }
    }
    
    public void sortAsPopulate() {
        //Commented by Ankit Nigam
        //Date: 23/4/2015
        //Issue was: Exception: Constructor not defined: [SortAsPopulationBatch].<Constructor>(String)
        //Reason: Bona have modified the SortAsPopulationBatch class and need to check that with her later
        
        //SortAsPopulationBatch obj = new SortAsPopulationBatch(' ');
        //Database.executeBatch(obj);
    }
    
    public void unlockNO() {
        UnlockNationaLockOrdersBatch obj = new UnlockNationaLockOrdersBatch();
        if(!Test.isRunningTest()) {
            Database.executeBatch(obj,10);
        }
    }
    
    public void automateNO(){
        NationalOrderAutomationProcessBatch  obj = new NationalOrderAutomationProcessBatch ();
        if(!Test.isRunningTest()) {
            Database.executeBatch(obj,1);
        }
    }
    
    public void callMultiScopeBatch(){
        MultiScopingBatch obj = new MultiScopingBatch('');
        if(!Test.isRunningTest()) {
            Database.executeBatch(obj,50);
        }
    }
    
    public void callP4PBatch() {
        MonthlyCronCreationBatch obj = new MonthlyCronCreationBatch(orderLI.Billing_End_Date__c, CommonMessages.P4P);
        if(!Test.isRunningTest()) {
            Database.executeBatch(obj, Integer.valueOf(Label.Monthly_Cron_Batch_Size));
        }
    }
    
    public void initiateCommisionReadyFlagBatch() {
       if(!Test.isRunningTest()) {
            database.executebatch(new  SetCommissionReadyBatchController(),2000);
       }
    }
    
    public void NationalOrderBackOutBatch() {
       if(objNSOS.Directory__c != null && objNSOS.Directory_Edition__c != null) {
        system.debug('Testing : '+objNSOS.Directory__c + '' + objNSOS.Directory_Edition__c);
            if(!Test.isRunningTest()) {
                database.executebatch(new NationalOrderBackOutBatch(objNSOS.Directory__c, objNSOS.Directory_Edition__c), 1);
            }
       }
    }
}