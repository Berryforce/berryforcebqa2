public with sharing class CaseCommentHandlerController {
	public static void onAfterInsert(List<CaseComment> listCaseComments) {
		syncWithJIRA(listCaseComments);
	}
	
	public static void onAfterUpdate(List<CaseComment> listCaseComments, Map<Id, CaseComment> oldMap) {
		syncWithJIRA(listCaseComments);
	}
	
	public static void syncWithJIRA(List<CaseComment> listCaseComments) {
    	Map<String, JIRAConnector__c> mapJIRAConnector = JIRAConnector__c.getAll();    
		Set<Id> parentCase = new Set<Id>();
	    for(CaseComment CC : listCaseComments) {
	        parentCase.add(CC.ParentId);
	    }
	    
	    Map<Id, Case> mapCase = new Map<Id, Case>([Select Id, RecordTypeId FROM Case WHERE Id IN: parentCase]);
	    
	    for(CaseComment CC : listCaseComments) {
	    	if(mapJIRAConnector.containsKey(mapCase.get(CC.ParentId).RecordTypeId)) {
				JIRAConnectorWebserviceCallout.synchronizeWithJIRAIssue(String.valueOf(CC.ParentId), String.valueOf(mapCase.get(CC.ParentId).RecordTypeId));
			}
	    }
	}
}