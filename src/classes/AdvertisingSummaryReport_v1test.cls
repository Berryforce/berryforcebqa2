@isTest
public class AdvertisingSummaryReport_v1test
{
    public static testMethod void testAdvertisingSummaryReport_v1 () 
    {
      PageReference pageRef = Page.AdvertisingSummaryReport_v1;
      Test.setCurrentPage(pageRef);
      Account ac = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(ac.Id);
        Canvass__c c = TestMethodsUtility.createCanvass(telco);
        //Canvass__c c=TestMethodsUtility.createCanvass();
       
        Account acc  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
       Account acc1  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account1', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
       Account acc2  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account2', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');   
      Account acc3  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account3', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
    Account acc4  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account4', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
    Account acc5  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account5', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
     
     
      insert acc; 
      insert acc1;
      insert acc2;
      insert acc3;
      insert acc4;
      insert acc5;  
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = acc.Id;
        insert objContact;

        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = acc.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=acc.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        objOpportunity.Account_Primary_Canvass__c=c.id;
        insert objOpportunity;

        /*Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.Directory_Code__c='DIR001';
        insert objDir;*/
        
        Directory__c objDir = TestMethodsUtility.createDirectory();
       

        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c=System.today().addMOnths(1);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE1';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS-4';
        objDirE1.Pub_Date__c=System.today().addMOnths(2);
        insert objDirE1;   
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Print';  
        objProd.RGU__c='Print';     
        insert  objProd;
              
        Order__c objOrder = new Order__c(Account__c=acc.id,Billing_Anniversary_Date__c=null);
        insert objOrder;

        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=acc.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id,selected__c=true);
        insert objOrderGroup ;

        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(4),media_Type__c='Print');
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc1.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=true,Directory_Edition__c = objDirE1.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Print');
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc2.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Print');
        
        Order_Line_Items__c objOrderLineItem3 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc3.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(4),media_Type__c='Print');
        Order_Line_Items__c objOrderLineItem4 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc4.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=true,Directory_Edition__c = objDirE1.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Print');
        Order_Line_Items__c objOrderLineItem5 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc5.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Print');       
        oliLst.add(objOrderLineItem);
        oliLst.add(objOrderLineItem1);
        oliLst.add(objOrderLineItem2);
        oliLst.add(objOrderLineItem3);
        oliLst.add(objOrderLineItem4);
        oliLst.add(objOrderLineItem5);      
        insert oliLst;
        Set<Id> oliIdSet=new Set<Id>();
        for(Order_Line_Items__c temp:oliLst)
        {   
            oliIdSet.add(temp.id);
        }       
      List<Order_Line_Items__c> OLIs=new List<Order_Line_Items__c>();
      List<String> AccountIds=new List<String>();
      for(AggregateResult agr:[Select Count(ID) OLICount,Account__r.Name AccountName,Account__c AccountId from Order_Line_Items__c where Id IN :oliIdSet AND Account__c!=null AND Product2__r.Product_type__c='Print' Group By Account__c,Account__r.Name])
      {
        if(Integer.valueof(agr.get('OLICount'))>5)
        {
        AccountIds.add(String.valueof(agr.get('AccountId')));
        }
      }
      //chris roberts error fix 
      //System.ListException: List index out of bounds: 0 Class.TestClassAdvertisingSummaryReport_v1.testAdvertisingSummaryReport_v1: line 18, column 1

      if(AccountIds.size()<1){
          Account AID= [SELECT ID from Account Limit 1];
          AccountIds.add(String.valueof(AID.Id)); 
      }
      acc=[Select id from Account where id=: AccountIds[0] Limit 1];
      ApexPages.StandardController Accctrl = new ApexPages.standardController(acc);
      AdvertisingSummaryReport_v1 asr=new AdvertisingSummaryReport_v1(Accctrl);
      ApexPages.currentPage().getParameters().put('Id',acc.id);
      ApexPages.currentPage().getParameters().put('ptype','print');
      asr.fetchOLI(acc.id);
      asr.FetchDetailedInfo();
      //negative test case
      Account acc_n=new Account();
      List<Order_Line_Items__c> OLIs_n=new List<Order_Line_Items__c>();
      List<String> AccountIds_n=new List<String>();
      for(AggregateResult agr:[Select Count(ID) OLICount,Account__r.Name AccountName,Account__c AccountId from Order_Line_Items__c where Id IN :oliIdSet AND Account__c!=null AND Product2__r.Product_type__c!='Print' Group By Account__c,Account__r.Name])
      {
        if(Integer.valueof(agr.get('OLICount'))>5)
        {
        AccountIds_n.add(String.valueof(agr.get('AccountId')));
        }
      }
      if(AccountIds_n.size()>0)
          acc_n=[Select id from Account where id=: AccountIds_n[0] Limit 1];
      ApexPages.StandardController Accctrl_n = new ApexPages.standardController(acc);
      AdvertisingSummaryReport_v1 asr_n=new AdvertisingSummaryReport_v1(Accctrl);
      ApexPages.currentPage().getParameters().put('Id',acc_n.id);
      ApexPages.currentPage().getParameters().put('ptype','digital');
      asr_n.FetchDetailedInfo();
    }
    
    public static testMethod void testAdvertisingSummaryReport_v11 () 
    {
      PageReference pageRef = Page.AdvertisingSummaryReport_v1;
      Test.setCurrentPage(pageRef);
        Account ac = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(ac.Id);
        Canvass__c c = TestMethodsUtility.createCanvass(telco);
        Account acc  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
        insert acc ; 
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = acc.Id;
        insert objContact;

        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = acc.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=acc.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        objOpportunity.Account_Primary_Canvass__c=c.id;
        insert objOpportunity;

       /* Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.Directory_Code__c='DIR001';
        insert objDir;*/
        
        Directory__c objDir = TestMethodsUtility.createDirectory();

        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='BOTS-2';
         objDirE.Pub_Date__c=System.today().addMOnths(3);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE1';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS-3';
         objDirE1.Pub_Date__c=System.today().addMOnths(4);
        insert objDirE1;   
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'SEO';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Digital';      
        insert  objProd;
              
        Order__c objOrder = new Order__c(Account__c=acc.id,Billing_Anniversary_Date__c=null);
        insert objOrder;

        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=acc.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id,selected__c=true);
        insert objOrderGroup ;

        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(4),media_Type__c='Digital',Canvass__c =c.id);
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=true,Directory_Edition__c = objDirE1.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Digital',Canvass__c =c.id);
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Digital',Canvass__c =c.id);
        
        Order_Line_Items__c objOrderLineItem3 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(4),media_Type__c='Digital',Canvass__c =c.id);
        Order_Line_Items__c objOrderLineItem4 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=true,Directory_Edition__c = objDirE1.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Digital',Canvass__c =c.id);
        Order_Line_Items__c objOrderLineItem5 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Digital',Canvass__c =c.id);       
        oliLst.add(objOrderLineItem);
        oliLst.add(objOrderLineItem1);
        oliLst.add(objOrderLineItem2);
        oliLst.add(objOrderLineItem3);
        oliLst.add(objOrderLineItem4);
        oliLst.add(objOrderLineItem5);      
        insert oliLst;
        Set<Id> oliIdSet=new Set<Id>();
        for(Order_Line_Items__c temp:oliLst)
        {   
            oliIdSet.add(temp.id);
        }       
      List<Order_Line_Items__c> OLIs=new List<Order_Line_Items__c>();
      List<String> AccountIds=new List<String>();
      for(AggregateResult agr:[Select Count(ID) OLICount,Account__r.Name AccountName,Account__c AccountId from Order_Line_Items__c where Id IN :oliIdSet AND Account__c!=null AND Product2__r.Product_type__c='Print' Group By Account__c,Account__r.Name])
      {
        if(Integer.valueof(agr.get('OLICount'))>5)
        {
        AccountIds.add(String.valueof(agr.get('AccountId')));
        }
      }
      //chris roberts error fix 
      //System.ListException: List index out of bounds: 0 Class.TestClassAdvertisingSummaryReport_v1.testAdvertisingSummaryReport_v1: line 18, column 1

      if(AccountIds.size()<1){
          Account AID= [SELECT ID from Account Limit 1];
          AccountIds.add(String.valueof(AID.Id)); 
      }
      acc=[Select id from Account where id=: AccountIds[0] Limit 1];
      ApexPages.StandardController Accctrl = new ApexPages.standardController(acc);
      AdvertisingSummaryReport_v1 asr=new AdvertisingSummaryReport_v1(Accctrl);
      ApexPages.currentPage().getParameters().put('Id',acc.id);
      ApexPages.currentPage().getParameters().put('ptype','print');
      asr.fetchOLI(acc.id);
      asr.FetchDetailedInfo();
      //negative test case
      Account acc_n=new Account();
      List<Order_Line_Items__c> OLIs_n=new List<Order_Line_Items__c>();
      List<String> AccountIds_n=new List<String>();
      for(AggregateResult agr:[Select Count(ID) OLICount,Account__r.Name AccountName,Account__c AccountId from Order_Line_Items__c where Id IN :oliIdSet AND Account__c!=null AND Product2__r.Product_type__c!='Print' Group By Account__c,Account__r.Name])
      {
        if(Integer.valueof(agr.get('OLICount'))>5)
        {
        AccountIds_n.add(String.valueof(agr.get('AccountId')));
        }
      }
      if(AccountIds_n.size()>0)
          acc_n=[Select id from Account where id=: AccountIds_n[0] Limit 1];
      ApexPages.StandardController Accctrl_n = new ApexPages.standardController(acc);
      AdvertisingSummaryReport_v1 asr_n=new AdvertisingSummaryReport_v1(Accctrl);
      ApexPages.currentPage().getParameters().put('Id',acc_n.id);
      ApexPages.currentPage().getParameters().put('ptype','digital');
      asr_n.FetchDetailedInfo();
    }
    
    public static testMethod void testAdvertisingSummaryReport_v3 () 
    {
      
        Account ac = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(ac.Id);
        Canvass__c c = TestMethodsUtility.createCanvass(telco);
       
        Account acc  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
       Account acc1  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account1', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
       Account acc2  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account2', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');   
      Account acc3  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account3', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
    Account acc4  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account4', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
    Account acc5  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account5', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
     
     
      insert acc; 
      insert acc1;
      insert acc2;
      insert acc3;
      insert acc4;
      insert acc5;  
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = acc.Id;
        insert objContact;

        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = acc.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=acc.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        objOpportunity.Account_Primary_Canvass__c=c.id;
        insert objOpportunity;

        /*Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.Directory_Code__c='DIR001';
        insert objDir;*/
        
        Directory__c objDir= TestMethodsUtility.createDirectory();

        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
          objDirE.Pub_Date__c=System.today().addMOnths(5);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE1';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS';
         objDirE1.Pub_Date__c=System.today().addMOnths(6);
        insert objDirE1;   
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Print';  
        objProd.RGU__c='Print';     
        insert  objProd;
        Order__c objOrder = new Order__c(Account__c=acc.id,Billing_Anniversary_Date__c=null);
        insert objOrder;

        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=acc.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id,selected__c=true);
        insert objOrderGroup ;

        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(4),media_Type__c='Print');
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Print');
        oliLst.add(objOrderLineItem1);
      
        insert oliLst;
        PageReference pageRef = Page.AdvertisingSummaryReport_v1;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('Id',acc.id);
      ApexPages.currentPage().getParameters().put('ptype','digital');
      ApexPages.StandardController Accctrl_n = new ApexPages.standardController(acc);
      AdvertisingSummaryReport_v1 asr_n=new AdvertisingSummaryReport_v1(Accctrl_n);
      
    }       
   public static testMethod void testAdvertisingSummaryReport_v4 () 
    {
      
      Account ac = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(ac.Id);
        Canvass__c c = TestMethodsUtility.createCanvass(telco);
       
        Account acc  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
       Account acc1  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account1', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
       Account acc2  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account2', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');   
      Account acc3  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account3', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
    Account acc4  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account4', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
    Account acc5  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account5', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
     
     
      insert acc; 
      insert acc1;
      insert acc2;
      insert acc3;
      insert acc4;
      insert acc5;  
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = acc.Id;
        insert objContact;

        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = acc.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=acc.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        objOpportunity.Account_Primary_Canvass__c=c.id;
        insert objOpportunity;

        /*Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.Directory_Code__c='DIR001';
        insert objDir;*/
        
        Directory__c objDir= TestMethodsUtility.createDirectory();

        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
          objDirE.Pub_Date__c=System.today().addMOnths(7);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE1';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS-1';
         objDirE1.Pub_Date__c=System.today().addMOnths(8);
        insert objDirE1;   
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Print';  
        objProd.RGU__c='Print';     
        insert  objProd;
        Order__c objOrder = new Order__c(Account__c=acc.id,Billing_Anniversary_Date__c=null);
        insert objOrder;

        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=acc.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id,selected__c=true);
        insert objOrderGroup ;

        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(4),media_Type__c='Print');
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Print');
        oliLst.add(objOrderLineItem1);
      
        insert oliLst;
        PageReference pageRef = Page.AdvertisingSummaryReport_v1;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('Id',acc.id);
      ApexPages.currentPage().getParameters().put('ptype','digital');
      ApexPages.StandardController Accctrl_n = new ApexPages.standardController(acc);
      AdvertisingSummaryReport_v1 asr_n=new AdvertisingSummaryReport_v1(Accctrl_n);
      
    }
   public static testMethod void testAdvertisingSummaryReport_v5 () 
    {
      
      Account ac = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(ac.Id);
        Canvass__c c = TestMethodsUtility.createCanvass(telco);
        //Canvass__c c=TestMethodsUtility.createCanvass();
       
        Account acc  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
       Account acc1  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account1', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
       Account acc2  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account2', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');   
      Account acc3  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account3', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
    Account acc4  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account4', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
    Account acc5  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account5', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
     
     
      insert acc; 
      insert acc1;
      insert acc2;
      insert acc3;
      insert acc4;
      insert acc5;  
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = acc.Id;
        insert objContact;

        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = acc.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=acc.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        objOpportunity.Account_Primary_Canvass__c=c.id;
        insert objOpportunity;

        /*Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.Directory_Code__c='DIR001';
        insert objDir;*/
        
        Directory__c objDir= TestMethodsUtility.createDirectory();

        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='BOTS-3';
          objDirE.Pub_Date__c=System.today().addMOnths(9);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE1';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS-4';
         objDirE1.Pub_Date__c=System.today().addMOnths(10);
        insert objDirE1;   
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Digital';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Digital';  
   
        insert  objProd;
        Order__c objOrder = new Order__c(Account__c=acc.id,Billing_Anniversary_Date__c=null);
        insert objOrder;

        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=acc.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id,selected__c=true);
        insert objOrderGroup ;

        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(4),media_Type__c='Print');
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Package_ID__c='pkg01',checked__c=true,Account__c=acc.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Cutomer_Cancel_Date__c = date.today().addDays(-2),Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Print');
        oliLst.add(objOrderLineItem1);
      
        insert oliLst;
        PageReference pageRef = Page.AdvertisingSummaryReport_v1;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('Id',acc.id);
      ApexPages.currentPage().getParameters().put('ptype','digital');
      ApexPages.StandardController Accctrl_n = new ApexPages.standardController(acc);
      AdvertisingSummaryReport_v1 asr_n=new AdvertisingSummaryReport_v1(Accctrl_n);
      
    }    
        
}