/*****************************************************
Controller Class to show YPC Add-on Record Validations
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 11/10/2014
$Id$
******************************************************/
public with sharing class YPCAddonValidationController {

    //Getters and setters
    public String dffId {
        get;
        set;
    }
    public Digital_Product_Requirement__c dff;
    public List < YPC_AddOn__c > ypcAddons = new List < YPC_AddOn__c > ();
    public Set < String > ypcAddns = new Set < String > {
        'YPCP', 'YPC5', 'CSTLK', 'CST5'
    };

    //Constructor
    public YPCAddonValidationController(ApexPages.StandardController controller) {

        dffId = controller.getId();

    }

    public pageReference addnVldtn() {

        dff = [SELECT Id, Name, OrderLineItemID__c, ModificationOrderLineItem__c, Submit_To_Yodle__c, Submit_Yodle_Cancel__c, Submit_Yodle_Update__c, RecordType.DeveloperName, CreatedById, Contact__c,
            OwnerId, Fulfillment_Submit_status__c, RecordTypeId, FSD_Status__c, Final_Status__c, OrderLineItemID__r.Id, OrderLineItemID__r.Name, OrderLineItemID__r.Status__c, OrderLineItemID__r.Talus_Go_Live_Date__c, OrderLineItemID__r.Talus_Cancel_Date__c,
            ModificationOrderLineItem__r.Id, ModificationOrderLineItem__r.Name, ModificationOrderLineItem__r.Digital_Product_Requirement__c, ModificationOrderLineItem__r.Status__c, ModificationOrderLineItem__r.Talus_Go_Live_Date__c, (SELECT Id, Name, Action_Type__c, DFF__c, DFF_ID__c, Effective_Date__c, coupon_description__c, coupon_url__c, cslt_text__c, cslt_url__c From YPC_AddOns__r)
            from Digital_Product_Requirement__c where id = : dffId
        ];

        ypcAddons = dff.YPC_AddOns__r;

        System.debug('************dff************' + dff + '************AddonList************' + ypcAddons.size());

        if (dff.YPC_AddOns__r.size() > 0) {
            for (YPC_AddOn__c adn: dff.YPC_AddOns__r) {
                if (adn.Action_Type__c == 'New') {
                    if ((adn.Name == 'YPCP' || adn.Name == 'YPC5') && (String.isBlank(adn.coupon_description__c) && String.isBlank(adn.coupon_url__c))) {
                        CommonUtility.msgWarning('Please make sure to input Coupon Description or Coupon URL on ' + adn.Name + ' AddOn Record');
                    }

                    if (adn.Name == 'CSTLK' || adn.Name == 'CST5') {
                        if (String.isBlank(adn.cslt_text__c)) {
                            CommonUtility.msgWarning('Please make sure to input Custom Link Text on ' + adn.Name + ' YPC AddOn Record');
                        }
                        if (String.isBlank(adn.cslt_url__c)) {
                            CommonUtility.msgWarning('Please make sure to input Custom Link URL on ' + adn.Name + ' YPC AddOn Record');
                        }
                    }

                    if ((adn.Name == 'YPCP' || adn.Name == 'YPC5') && (String.isNotBlank(adn.coupon_description__c) || String.isNotBlank(adn.coupon_url__c))) {
                        CommonUtility.msgInfo(CommonUtility.cpnInfoPrvd);
                    }
                    if ((adn.Name == 'CSTLK' || adn.Name == 'CST5') && (String.isNotBlank(adn.cslt_text__c) && String.isNotBlank(adn.cslt_url__c))) {
                        CommonUtility.msgInfo(CommonUtility.cstlkInfoPrvd);
                    }
                }
            }
        } else {
            CommonUtility.msgInfo(CommonUtility.emptyYpcAddns);
        }        
        return null;
    }
}