public with sharing class BerryCaresCertificateSOQLMethods {
	public static List<Berry_Cares_Certificate__c> fetchBCCByBCC(List<Berry_Cares_Certificate__c> BCCList) {
		return [SELECT Id, Branding__c, Certificate_Number__c, Date_of_Issue__c, BCC_Sub_Amount__c, Customer_ID__c, Account_Name__c, Primary_Telephone_Number__c,
		Client_Contact_Last_Name__c, Client_Contact_First_Name__c, CSR_First_Name__c, CSR_Alias__c, Client_Contact_Email__c, Case__c, Case__r.ContactId 
		FROM Berry_Cares_Certificate__c WHERE ID IN: BCCList];
	}
}