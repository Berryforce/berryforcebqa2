global class BillingSubsequentDigitalSchedulerHndlr implements BillingSubsequentDigitalScheduler.BillingSubsequentDigitalSchedulerInterface {
    global void execute(SchedulableContext sc) {
         if(!Test.isRunningTest()){
        Database.executeBatch(new DigitalMonthlyBilling(system.today(), CommonMessages.digitalMediaType), 20);
        }
    } 
}