@IsTest
public class Product2SOQLMethodsTest {    
    static testMethod void Product2SOQLMethodsTest () {
        Test.startTest();
        Product2 objProduct = TestMethodsUtility.createproduct(); 
    
        Product2SOQLMethods.getProductMapByID(new set<ID> {objProduct.ID});
        Product2SOQLMethods.getProductByUDAC(new set<String> {objProduct.ProductCode});
        Test.stopTest();
    }
}