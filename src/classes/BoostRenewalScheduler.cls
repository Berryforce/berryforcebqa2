global class BoostRenewalScheduler implements Schedulable {
   public Interface BoostRenewalSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('BoostRenewalSchedulerHndlr');
        if(targetType != null) {
            BoostRenewalSchedulerInterface obj = (BoostRenewalSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}