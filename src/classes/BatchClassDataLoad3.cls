global class BatchClassDataLoad3 implements Database.Batchable<sObject>{
    
    //global string filtercondition;
    
    global BatchClassDataLoad3(){
        //filtercondition = condition;
    }
    
     global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'select Id, (Select id, P4P_Price_Per_Click_Lead__c, Is_P4P__c, P4P_Billing__c from Order_Line_Items__r where Is_P4P__c = true and P4P_Price_Per_Click_Lead__c != null and P4P_Price_Per_Click_Lead__c > 0 ) from Order_Group__c';
        return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext bc, List<Order_Group__c> lstOS) {
        //Savepoint sp = Database.setSavepoint();
        Boolean bFlagP4PBilling = false;
        List<Order_Line_Items__c> lstOLIUpdt = new List<Order_Line_Items__c>();
        
        for(Order_Group__c objOS : lstOS){
            bFlagP4PBilling = false;
            for(Order_Line_Items__c objOLI : objOS.Order_Line_Items__r) {
                if(objOLI.Is_P4P__c && objOLI.P4P_Price_Per_Click_Lead__c != null && objOLI.P4P_Price_Per_Click_Lead__c > 0 && !bFlagP4PBilling) {
                    bFlagP4PBilling = true;
                    lstOLIUpdt.add(new Order_Line_Items__c(id=objOLI.id, P4P_Billing__c = true));
                }
            }
        }
        if(lstOLIUpdt != null && lstOLIUpdt.size() > 0) {
            update lstOLIUpdt;
        }
     } 
      
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'OLI batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception records for any other errors that might have occured while processing.');
    }
}


/*global class DataLoadBatchClass3 implements Database.Batchable<sObject>{
    
    global DataLoadBatchClass3(){
    }
    
     global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'Select id, Normalized_Last_Name_Business_Name__c From Listing__c where Normalized_Last_Name_Business_Name__c = null';
        return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext bc, List<Listing__c> lstListing) {
        Savepoint sp = Database.setSavepoint();
        
            if(lstListing!=null && lstListing.size()>0){
                
                //AppearanceCountSHM.IncreaseDecreaseAppearancecount(lstListing,null);
                List<Listing__c> lstListingUpdt = new List<Listing__c>();
                for(Listing__c objListing :lstListing){
                    objListing.Normalized_Last_Name_Business_Name__c = 'Normalization Pending';
                    lstListingUpdt.add(objListing);
                }
                if(lstListingUpdt.size() > 0) {
                    update lstListingUpdt;
                }
            }
        
     } 
      
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Listing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception records for any other errors that might have occured while processing.');
    }
}*/