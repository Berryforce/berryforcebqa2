@IsTest(SeeAllData=true)
private class ApplyCityServiceTestClass {
    static testMethod void ApplyCityServiceTest() {
        Canvass__c c = TestMethodsUtility.createCanvass();
        Account act = TestMethodsUtility.generateCustomerAccount();
        insert act;
        Telco__c telco = TestMethodsUtility.createTelco(act.Id);  
        Contact cnt = TestMethodsUtility.createContact(act);       
        Opportunity oppty = TestMethodsUtility.createOpportunity('new', act);              
        Order__c ord = TestMethodsUtility.createOrder(act.Id);        
        Order_Group__c og = TestMethodsUtility.createOrderSet(act, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(act, cnt, oppty, ord, og);
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Edition__c DE = TestMethodsUtility.createDirectoryEdition(dir.Id, c.id, telco.Id, null);
        Directory_Section__c dirSection = TestMethodsUtility.createDirectorySection(dir);
        Listing__c objList = TestMethodsUtility.generateMainListing();
        objList.Listing_City__c = 'Texas';
        insert objList;
        //list<Directory_Pagination__c> dpLst = new list<Directory_Pagination__c>();
        Directory_Pagination__c dirPagination = TestMethodsUtility.generateDirectoryPagination(dirSection.Id, oln.Id, DE.Id);
        dirPagination.DP_Listing__c= objList.id;
        dirPagination.Trademark_Sorting_NULL_Identifier__c=true;
        dirPagination.caption_member__c=false;
        dirPagination.City_Name_Section_Abbreviation__c = 'Testcity';
        insert dirPagination;
        Directory_Pagination__c dirPagination1 = TestMethodsUtility.generateDirectoryPagination(dirSection.Id, oln.Id, DE.Id);
        dirPagination1.DP_Listing__c = objList.id;
        dirPagination1.Trademark_Sorting_NULL_Identifier__c=true;
        dirPagination1.caption_member__c=false;
        dirPagination1.City_Name_Section_Suppressed__c = true;             
        insert dirPagination1;
        set<id> setdpId = new set<Id>();
        setdpId.add(dirPagination.Id);
        setdpId.add(dirPagination1.Id);
        List<Directory_pagination__c> dpLst = [Select Id,Name,City_Name_Section_Abbreviation__c,City_Name_Section_Suppressed__c,City_Name_Unedited__c,Directory_Section__c,Listing_City__c,Listing_City_Unformatted__c from Directory_Pagination__c where Id IN:setdpId];
        system.assertEquals('Texas',dpLst[0].Listing_City_Unformatted__c );
        Community__c objComm = new Community__c(Name='Texas',State__c='TX');
        insert objComm;
        set<Id> setCSAId = new set<Id>();
        set<string> setDSId = new set<string>{dirSection.Id};
        set<string> setCity = new set<string>{'Texas'};
        map<string,Community_Section_Abbreviation__c> mapstrCSA = new map<string,Community_Section_Abbreviation__c>();
        Community_Section_Abbreviation__c objCSA = new Community_Section_Abbreviation__c (Name='Test CSA1',Community__c=objComm.Id,Community_Abbreviation__c='Test',Directory_Section__c=dirSection.Id,Suppress_Abbreviation_Rule__c='Suppress');
        insert objCSA;
        setCSAId.add(objCSA.Id);
        Community_Section_Abbreviation__c objCSA1 = new Community_Section_Abbreviation__c (Name='Test CSA2',Community__c=objComm.Id,Community_Abbreviation__c='Test12',Directory_Section__c=dirSection.Id,Suppress_Abbreviation_Rule__c='Use Abbreviation');
        insert objCSA1;
        setCSAId.add(objCSA1.Id);
        Community_Section_Abbreviation__c objCSA2 = new Community_Section_Abbreviation__c (Name='Test CSA3',Community__c=objComm.Id,Community_Abbreviation__c='Test12',Directory_Section__c=dirSection.Id,Suppress_Abbreviation_Rule__c='Print in Full');
        insert objCSA2;
        setCSAId.add(objCSA2.Id);
        list<Community_Section_Abbreviation__c> CSALst = [Select Id,Name,Community__c,Community_Abbreviation__c,Community_Name__c,Directory_Section__c,
              State__c,Suppress_Abbreviation_Rule__c from Community_Section_Abbreviation__c where Id IN: setCSAId];
        if(CSALst.size()> 0) {
            mapstrCSA.put(CSALst[0].Directory_Section__c+CSALst[0].Community_Name__c,CSALst[0]);
            mapstrCSA.put(CSALst[1].Directory_Section__c+CSALst[1].Community_Name__c,CSALst[1]);
            mapstrCSA.put(CSALst[2].Directory_Section__c+CSALst[2].Community_Name__c,CSALst[2]);
        }
        Pagination_Job__c Pj = TestMethodsUtility.createPaginationJob(dirSection.Id,'2015');
        list<Pagination_Job__c> pjLst = new list<Pagination_Job__c>();
        pjLst.add(Pj);
        Database.BatchableContext bc;
        Test.StartTest();
        ApplyCityService.communitysectionMethod(objCSA.Id);
        ApplyCityServiceBatchClass apcsBatch= new ApplyCityServiceBatchClass(setDSId ,setCity ,mapstrCSA);
        ApplyCityServiceBatchClass apcsBatchPj= new ApplyCityServiceBatchClass(setDSId ,setCity ,mapstrCSA,null,pjLst);
        apcsBatch.start(bc);
        apcsBatch.execute(bc,dpLst);
        ApplyCityService.communitysectionMethod(objCSA1.Id);
        ApplyCityServiceBatchClass apcsBatch1= new ApplyCityServiceBatchClass(setDSId ,setCity ,mapstrCSA);
        apcsBatch1.start(bc);
        apcsBatch1.execute(bc,dpLst);
        ApplyCityService.communitysectionMethod(objCSA2.Id);
        ApplyCityServiceBatchClass apcsBatch2= new ApplyCityServiceBatchClass(setDSId ,setCity ,mapstrCSA);
        apcsBatch2.start(bc);
        apcsBatch2.execute(bc,dpLst);
        Test.StopTest();    
    }
}