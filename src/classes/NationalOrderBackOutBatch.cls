global class NationalOrderBackOutBatch implements Database.Batchable<sObject>, Database.Stateful {
	String StrDirCode {get;set;}
	String strEdition {get;set;}
	list<Opportunity> lstDeleteOpp = new list<Opportunity>();
	public NationalOrderBackOutBatch(String strDirCode, String strEdition) {
		this.StrDirCode = strDirCode;
		this.strEdition = strEdition;
	}
	
	global DataBase.QueryLocator Start(DataBase.BatchableContext BC) {
		set<String> setDir = new set<String>{StrDirCode};
		set<String> setDirEd = new set<String>{strEdition};
		String strQurty = 'Select ID, Transaction_Unique_ID__c, (Select Id From Order_Sets__r), (Select Id From Opportunities__r) From National_Staging_Order_Set__c where Directory__c IN:setDir and Directory_Edition__c IN:setDirEd';
		return Database.getQueryLocator(strQurty);
	}
	
	global void execute(Database.BatchableContext BC, List<National_Staging_Order_Set__c> lstSObj) {
		list<Order_Group__c> lstDeleteOS = new list<Order_Group__c>();
		set<Id> setOSID = new set<Id>();
		set<String> setTRansUniqueID = new set<String>();
		for(National_Staging_Order_Set__c iterator : lstSObj) {
			setTRansUniqueID.add(iterator.Transaction_Unique_ID__c);
			for(Order_Group__c iteratorOS : iterator.Order_Sets__r) {
				setOSID.add(iteratorOS.Id);
				lstDeleteOS.add(new Order_Group__c(ID = iteratorOS.Id));
			}
			
			for(Opportunity iteratorOPP : iterator.Opportunities__r) {
				lstDeleteOpp.add(new Opportunity(ID = iteratorOPP.Id));
			}
		}
		
		list<Digital_Product_Requirement__c> lstDFF = [Select Id from Digital_Product_Requirement__c where OrderLineItemID__r.Order_Group__c IN:setOSID];
		list<Directory_Pagination__c> lstDP = [Select Id from Directory_Pagination__c where Order_Line_Item__r.Order_Group__c IN:setOSID];
		
		list<National_History_Order_Set__c> lstNSHistory = [Select ID From National_History_Order_Set__c where Transaction_Unique_ID__c != null AND Transaction_Unique_ID__c IN:setTRansUniqueID];
		
		if(lstDP.size() > 0) {
			delete lstDP;
		}
		
		if(lstDFF.size() > 0) {
			delete lstDFF;
		}
		
		if(lstDFF.size() > 0) {
			delete lstDeleteOS;
		}
		
		if(lstNSHistory.size() > 0) {
			delete lstNSHistory;
		}
			
		if(lstSObj.size() > 0) {
			delete lstSObj;
		}
	}
	
	global void finish(Database.Batchablecontext BC) {
		if(lstDeleteOpp.size() > 0) {
			delete lstDeleteOpp;
		}
	  AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
      /*Set<String> emailaddrSet=new Set<String>();
      emailaddrSet.add(a.CreatedBy.Email);
      List<String> emailaddrLst=new List<String>();
      emailaddrLst.addall(emailaddrSet);*/
      String strErrorMessage = '';
      if(a.NumberOfErrors > 0){
          strErrorMessage = a.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'NationalOrderSet Deletion is ' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems +
          ' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'.');
	}
}