public with sharing class NationalRateCardController_v3 {
        Directory__c objDIR {get;set;}
        public NationalPriceClass objNationalNP {get;set;}
        public boolean bFlag {get;set;}
        public string strVersion {get;set;}
        public string rateLabel {get;set;}
        public string xlsFileLabel {get;set;}
        
        public list<GenerateNCSVFileClass> lstNCSV {get;set;}
        public list<GenerateLCSVFileClass> lstLCSV {get;set;}
    
        public NationalRateCardController_v3(Apexpages.StandardController controller) {
                objDIR = (Directory__c)controller.getRecord();                
        }
        
        public void onLoad() {
            String strType = Apexpages.currentPage().getParameters().get('type');
            rateLabel = 'Local Rate Card';
            xlsFileLabel = 'LocalPrice';
            bFlag = checkType(strType);
            populateNPDataToWrapperClass(DirectoryProductMappingSOQLMethods.fetchDPMByDirectoryId(new set<Id>{objDIR.Id}), strType);
        }
        
        public Pagereference changeType() {
            bFlag = checkType(objNationalNP.strType);
            populateNPDataToWrapperClass(DirectoryProductMappingSOQLMethods.fetchDPMByDirectoryId(new set<Id>{objDIR.Id}), objNationalNP.strType);
            return null;
        }
        
        private boolean checkType(String strType) {
            if(String.isNotEmpty(strType)) {
            	rateLabel = 'National Rate Card';
            	xlsFileLabel = 'NationalPrice';
                if(strType.equals('local')) {
                	rateLabel = 'Local Rate Card';
                	xlsFileLabel = 'LocalPrice';
                    return false;
                }
            }
            return true;
        }
        
        private void populateNPDataToWrapperClass(list<Directory_Product_Mapping__c> lstDNPM, String strType) {
                if(lstDNPM != null) {
            list<NationalPriceChildClass> lstTempNPC = new list<NationalPriceChildClass>();
            for(Directory_Product_Mapping__c iteratorDNPM : lstDNPM) {
                if(objNationalNP == null) {
                    objNationalNP = new NationalPriceClass();
                }
                if(objNationalNP.directory == null) { 
                    objNationalNP.directory = iteratorDNPM.Directory__c;
                    objNationalNP.directoryName = iteratorDNPM.Directory__r.Name;
                    objNationalNP.directoryVersion = iteratorDNPM.Directory__r.Directory_Code__c;
                    objNationalNP.directoryPUBCOName = iteratorDNPM.Directory__r.Publication_Company_Name__c;
                    objNationalNP.directoryPUBCOCode = iteratorDNPM.Directory__r.Publication_Company_Code__c;
                    objNationalNP.updateRate = 0;
                    objNationalNP.typeSelect.add(new SelectOption('local', 'Local'));
                    objNationalNP.typeSelect.add(new SelectOption('national', 'National'));
                    objNationalNP.strType = strType;
                }
                NationalPriceChildClass objNPC = new NationalPriceChildClass();
                objNPC.productId = iteratorDNPM.Product2__c;                     
                if(iteratorDNPM.Product2__r.LOY__c) {
                    objNPC.LOY = 'Free';
                }
                objNPC.UDAC = iteratorDNPM.Product2__r.ProductCode;
                objNPC.fullRate = (iteratorDNPM.FullRate__c == null ? 0.00 : iteratorDNPM.FullRate__c);
                objNPC.fullRate = objNPC.fullRate.setScale(2, RoundingMode.HALF_UP);
                objNPC.nationalFullRate = iteratorDNPM.National_Full_Rate__c;
                objNPC.nationalFullRate = objNPC.nationalFullRate.setScale(2, RoundingMode.HALF_UP);
                objNPC.dirNPMapping = iteratorDNPM.Id;
                if(strType.equalsIgnoreCase('local')) {
                        objNPC.allowFR = true;
                        objNPC.allowNFR = false;
                        popualteNationalChildRecordValues(iteratorDNPM, objNPC, objNPC.fullRate);               
                }
                else {
                        objNPC.allowFR = false;
                        objNPC.allowNFR = true;
                        popualteNationalChildRecordValues(iteratorDNPM, objNPC, objNPC.nationalFullRate);
                }
                lstTempNPC.add(objNPC);
            }
            if(lstTempNPC.size() > 0) {
                objNationalNP.lstChild = lstTempNPC;
            }
                }
        }
        
        private void popualteNationalChildRecordValues(Directory_Product_Mapping__c iteratorDNPM, NationalPriceChildClass objNPC, Decimal rates) {
            objNPC.allowX15 = iteratorDNPM.Product2__r.AllowX15Discount__c;
            if(iteratorDNPM.Product2__r.AllowX15Discount__c == true) {
                objNPC.X15 = calculatePercentage(rates, 85.00);
            }
            objNPC.allowX25 = iteratorDNPM.Product2__r.AllowX25Discount__c;
            if(iteratorDNPM.Product2__r.AllowX25Discount__c == true) {
                objNPC.X25 = calculatePercentage(rates, 75.00);
            }
            objNPC.allowX30 = iteratorDNPM.Product2__r.AllowX30Discount__c;
            if(iteratorDNPM.Product2__r.AllowX30Discount__c == true) {
                objNPC.X30 = calculatePercentage(rates, 70.00);
            }
            objNPC.allowX35 = iteratorDNPM.Product2__r.AllowX35Discount__c;
            if(iteratorDNPM.Product2__r.AllowX35Discount__c == true) {
                objNPC.X35 = calculatePercentage(rates, 65.00);
            }
            objNPC.allowX40 = iteratorDNPM.Product2__r.AllowX40Discount__c;
            if(iteratorDNPM.Product2__r.AllowX40Discount__c == true) {
                objNPC.X40 = calculatePercentage(rates, 60.00);
            }
            objNPC.allowX45 = iteratorDNPM.Product2__r.AllowX45Discount__c;
            if(iteratorDNPM.Product2__r.AllowX45Discount__c == true) {
                objNPC.X45 = calculatePercentage(rates, 55.00);
            }
            objNPC.allowX50 = iteratorDNPM.Product2__r.AllowX50Discount__c;
            if(iteratorDNPM.Product2__r.AllowX50Discount__c == true) {
                objNPC.X50 = calculatePercentage(rates, 50.00);
            }
            objNPC.allowX55 = iteratorDNPM.Product2__r.AllowX55Discount__c;
            if(iteratorDNPM.Product2__r.AllowX55Discount__c == true) {
                objNPC.X55 = calculatePercentage(rates, 45.00);
            }
            objNPC.allowX60 = iteratorDNPM.Product2__r.AllowX60Discount__c;
            if(iteratorDNPM.Product2__r.AllowX60Discount__c == true) {
                objNPC.X60 = calculatePercentage(rates, 40.00);
            }
            objNPC.allowX65 = iteratorDNPM.Product2__r.AllowX65Discount__c;
            if(iteratorDNPM.Product2__r.AllowX65Discount__c == true) {
                objNPC.X65 = calculatePercentage(rates, 35.00);
            }
            objNPC.allowX70 = iteratorDNPM.Product2__r.AllowX70Discount__c;
            if(iteratorDNPM.Product2__r.AllowX70Discount__c == true) {
                objNPC.X70 = calculatePercentage(rates, 30.00);
            }
        }
        
        private void popualteNationalChildRecordValues(NationalPriceChildClass objNPC, Decimal rates) {
            if(objNPC.allowX15 == true) {
                objNPC.X15 = calculatePercentage(rates, 85.00);
            }
            if(objNPC.allowX25 == true) {
                objNPC.X25 = calculatePercentage(rates, 75.00);
            }
            if(objNPC.allowX30 == true) {
                objNPC.X30 = calculatePercentage(rates, 70.00);
            }
            if(objNPC.allowX35 == true) {
                objNPC.X35 = calculatePercentage(rates, 65.00);
            }
            if(objNPC.allowX40 == true) {
                objNPC.X40 = calculatePercentage(rates, 60.00);
            }
            if(objNPC.allowX45 == true) {
                objNPC.X45 = calculatePercentage(rates, 55.00);
            }
            if(objNPC.allowX50 == true) {
                objNPC.X50 = calculatePercentage(rates, 50.00);
            }
            if(objNPC.allowX55 == true) {
                objNPC.X55 = calculatePercentage(rates, 45.00);
            }
            if(objNPC.allowX60 == true) {
                objNPC.X60 = calculatePercentage(rates, 40.00);
            }
            if(objNPC.allowX65 == true) {
                objNPC.X65 = calculatePercentage(rates, 35.00);
            }
            if(objNPC.allowX70 == true) {
                objNPC.X70 = calculatePercentage(rates, 30.00);
            }
        }
        
        public Pagereference doCalculaion() {
                try {
                        String strType = objNationalNP.strType;
                    if(objNationalNP.updateRate <= 0 || objNationalNP.updateRate == null) {
                        system.debug('****************Validate update rate : '+ objNationalNP.updateRate);
                        CommonMethods.addError('Please enter the update rate value');
                        return null;
                    }
                    
                    for(NationalPriceChildClass iteratorNPC : objNationalNP.lstChild) {
                        iteratorNPC.fullRate = fullRateRound(iteratorNPC.fullRate + ((iteratorNPC.fullRate * objNationalNP.updateRate) / 100));
                        popualteNationalChildRecordValues(iteratorNPC, iteratorNPC.fullRate);
                    }
                }
                catch(Exception ex) {
                        CommonMethods.addError(ex.getMessage());
                }
                return null;
        }
        
        public Pagereference doCalculaionPerProduct() {
                try {
                        String currentProdId = Apexpages.currentPage().getParameters().get('pId');
                        for(NationalPriceChildClass iteratorNPC : objNationalNP.lstChild) {
                                if(currentProdId == String.valueOf(iteratorNPC.productId)) {
                                        iteratorNPC.fullRate = fullRateRound(iteratorNPC.fullRate);
                                        system.debug('Testingg : '+ iteratorNPC.fullRate);
                                        popualteNationalChildRecordValues(iteratorNPC, iteratorNPC.fullRate);
                                }
                        }
                }
                catch(Exception ex) {
                        CommonMethods.addError(ex.getMessage());
                }
                return null;
        }
        
        public Pagereference cancel() {
            return new Pagereference('/'+objDIR.Id);
        }
        
        public Pagereference save() {
            Savepoint sp = Database.setSavepoint();
            try {
                list<Directory_Product_Mapping__c> lstUpdate = new list<Directory_Product_Mapping__c>();
                for(NationalPriceChildClass iteratorNPC : objNationalNP.lstChild) {
                    lstUpdate.add(new Directory_Product_Mapping__c(Id = iteratorNPC.dirNPMapping, FullRate__c = iteratorNPC.fullRate));
                }
                
                if(lstUpdate.size() > 0) {
                    update lstUpdate;
                }
            }
            catch(Exception ex) {
                CommonMethods.addError(ex.getMessage());
                Database.rollback(sp);
                return null;
            }
            return new Pagereference('/'+objDIR.Id);
        }
        
        public Pagereference exportXLS() {
            return Page.NationalRateCardPage_Export;
        }
        
        public Pagereference generateCSV() {
        lstNCSV = new list<GenerateNCSVFileClass>();
        lstLCSV = new list<GenerateLCSVFileClass>();
        for(NationalPriceChildClass iteratorNPC : objNationalNP.lstChild) {
            system.debug(objNationalNP.strType);
            if(objNationalNP.strType == 'national') {
                GenerateNCSVFileClass objCSVFile = new GenerateNCSVFileClass();
                objCSVFile.directory = objNationalNP.directory;
                objCSVFile.directoryName = objNationalNP.directoryName;
                objCSVFile.directoryVersion = objNationalNP.directoryVersion;
                objCSVFile.directoryPUBCOName = objNationalNP.directoryPUBCOName;
                objCSVFile.directoryPUBCOCode = objNationalNP.directoryPUBCOCode;
                objCSVFile.dirNPMapping = iteratorNPC.dirNPMapping;
                objCSVFile.productId = iteratorNPC.productId;
                objCSVFile.npType = objNationalNP.strType;
                objCSVFile.LOY = iteratorNPC.LOY;
                objCSVFile.UDAC = iteratorNPC.UDAC;
                objCSVFile.fullRate = iteratorNPC.nationalFullRate;
                objCSVFile.X15 = iteratorNPC.X15;
                objCSVFile.X25 = iteratorNPC.X25;
                objCSVFile.X30 = iteratorNPC.X30;
                objCSVFile.X35 = iteratorNPC.X35;
                objCSVFile.X40 = iteratorNPC.X40;
                objCSVFile.X45 = iteratorNPC.X45;
                objCSVFile.X50 = iteratorNPC.X50;
                objCSVFile.X55 = iteratorNPC.X55;
                objCSVFile.X60 = iteratorNPC.X60;
                objCSVFile.X65 = iteratorNPC.X65;
                lstNCSV.add(objCSVFile);                
            }
            else if(objNationalNP.strType == 'local') {
                GenerateLCSVFileClass objCSVFile = new GenerateLCSVFileClass();
                objCSVFile.directory = objNationalNP.directory;
                objCSVFile.directoryName = objNationalNP.directoryName;
                objCSVFile.directoryVersion = objNationalNP.directoryVersion;
                objCSVFile.directoryPUBCOName = objNationalNP.directoryPUBCOName;
                objCSVFile.directoryPUBCOCode = objNationalNP.directoryPUBCOCode;
                objCSVFile.dirNPMapping = iteratorNPC.dirNPMapping;
                objCSVFile.productId = iteratorNPC.productId;
                objCSVFile.npType = objNationalNP.strType;
                objCSVFile.UDAC = iteratorNPC.UDAC;
                objCSVFile.fullRate = iteratorNPC.fullRate;
                objCSVFile.X40 = iteratorNPC.X40;
                objCSVFile.X45 = iteratorNPC.X45;
                objCSVFile.X50 = iteratorNPC.X50;
                objCSVFile.X55 = iteratorNPC.X55;
                objCSVFile.X60 = iteratorNPC.X60;
                objCSVFile.X65 = iteratorNPC.X65;
                objCSVFile.X70 = iteratorNPC.X70;
                lstLCSV.add(objCSVFile);
                
            }
        }       
        if(lstLCSV.size() > 0) {
            return Page.NationalRateCardPage_LCSV;
        }
        else if(lstNCSV.size() > 0) {
            return Page.NationalRateCardPage_CSV;
        }       
        return null;
    }
        
        private Decimal calculatePercentage(Decimal fullRate, Decimal percentage) {
        system.debug('Full Rate : '+ fullRate);
        system.debug('percentage : '+ percentage);
        Decimal rate = (fullRate * percentage) / 100;
        system.debug('rate : '+ rate);
        rate = rate.setScale(2, RoundingMode.HALF_UP);
        system.debug('rate : '+ rate);
        return roundCents(rate);
    }
    
    private Decimal fullRateRound(Decimal fullRate) {
        fullRate = fullRate.setScale(2, RoundingMode.HALF_UP);
        system.debug('rate : '+ fullRate);
        return roundCents(fullRate);
    }
    
    private Decimal roundCents(Decimal rate) {
        String strFullRate = String.valueOf(rate);
        system.debug('Full Rate String : '+ strFullRate);
        String strAfterDecimal = strFullRate.substringAfterLast('.');
        system.debug('strAfterDecimal : '+ strAfterDecimal);
        String fiveCent = '12346789';
        if(fiveCent.contains(strAfterDecimal.right(1))) {
            if(strAfterDecimal.right(1) == '1') {
                rate = rate + 0.04;
            }
            else if(strAfterDecimal.right(1) == '2') {
                rate = rate + 0.03;
            }
            else if(strAfterDecimal.right(1) == '3') {
                rate = rate + 0.02;
            }
            else if(strAfterDecimal.right(1) == '4') {
                rate = rate + 0.01;
            }
            else if(strAfterDecimal.right(1) == '6') {
                rate = rate + 0.04;
            }
            else if(strAfterDecimal.right(1) == '7') {
                rate = rate + 0.03;
            }
            else if(strAfterDecimal.right(1) == '8') {
                rate = rate + 0.02;
            }
            else if(strAfterDecimal.right(1) == '9') {
                rate = rate + 0.01;
            }
        }
        return rate;
    }
        
        
        public class NationalPriceClass {
        public Id directory {get;set;}
        public String directoryName {get;set;}
        public String directoryVersion {get;set;}
        public String directoryPUBCOName {get;set;}
        public String directoryPUBCOCode {get;set;}
        public Decimal updateRate {get;set;}
        public list<SelectOption> typeSelect {get;set;}
        public String strType {get;set;}
        public list<NationalPriceChildClass> lstChild {get;set;}
        
        public NationalPriceClass() {
            lstChild = new list<NationalPriceChildClass>();
            typeSelect = new list<SelectOption>();          
        }
    }
    
    public class NationalPriceChildClass {
        public ID dirNPMapping {get;set;}
        public ID productId {get;set;}
        public String LOY {get;set;}
        public String UDAC {get;set;}
        public Decimal fullRate {get;set;}
        public boolean allowFR {get;set;}
        public Decimal nationalFullRate {get;set;}
        public boolean allowNFR {get;set;}
        public Decimal X15 {get;set;}
        public boolean allowX15 {get;set;}
        public Decimal X25 {get;set;}
        public boolean allowX25 {get;set;}
        public Decimal X30 {get;set;}
        public boolean allowX30 {get;set;}
        public Decimal X35 {get;set;}
        public boolean allowX35 {get;set;}
        public Decimal X40 {get;set;}
        public boolean allowX40 {get;set;}
        public Decimal X45 {get;set;}
        public boolean allowX45 {get;set;}
        public Decimal X50 {get;set;}
        public boolean allowX50 {get;set;}
        public Decimal X55 {get;set;}
        public boolean allowX55 {get;set;}
        public Decimal X60 {get;set;}
        public boolean allowX60 {get;set;}
        public Decimal X65 {get;set;}
        public boolean allowX65 {get;set;}
        public Decimal X70 {get;set;}
        public boolean allowX70 {get;set;}
        
        public NationalPriceChildClass() {
            
        }
    }
    
    public class GenerateNCSVFileClass {
        public Id directory {get;set;}
        public String directoryName {get;set;}
        public String directoryVersion {get;set;}
        public String directoryPUBCOName {get;set;}
        public String directoryPUBCOCode {get;set;}
        public Decimal updateRate {get;set;}
        public ID dirNPMapping {get;set;}
        public String npType {get;set;}
        public ID productId {get;set;}
        public String LOY {get;set;}
        public String UDAC {get;set;}
        public Decimal fullRate {get;set;}
        public Decimal X15 {get;set;}
        public Decimal X25 {get;set;}
        public Decimal X30 {get;set;}
        public Decimal X35 {get;set;}
        public Decimal X40 {get;set;}
        public Decimal X45 {get;set;}
        public Decimal X50 {get;set;}
        public Decimal X55 {get;set;}
        public Decimal X60 {get;set;}
        public Decimal X65 {get;set;}
    }
    
    public class GenerateLCSVFileClass {
        public Id directory {get;set;}
        public String directoryName {get;set;}
        public String directoryVersion {get;set;}
        public String directoryPUBCOName {get;set;}
        public String directoryPUBCOCode {get;set;}
        public Decimal updateRate {get;set;}
        public ID dirNPMapping {get;set;}
        public String npType {get;set;}
        public ID productId {get;set;}
        public String UDAC {get;set;}
        public Decimal fullRate {get;set;}
        public Decimal X40 {get;set;}
        public Decimal X45 {get;set;}
        public Decimal X50 {get;set;}
        public Decimal X55 {get;set;}
        public Decimal X60 {get;set;}
        public Decimal X65 {get;set;}
        public Decimal X70 {get;set;}
    }
}