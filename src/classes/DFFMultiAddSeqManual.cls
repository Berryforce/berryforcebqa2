public with sharing class DFFMultiAddSeqManual {
    public pagereference doRun() {
        database.executebatch(new DFFMultiAddSequenceLogic());
        return (new pagereference('/apex/DFFMultiAddSequenceManualPage').setredirect(true));
    }
}