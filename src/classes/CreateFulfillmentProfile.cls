public with sharing class CreateFulfillmentProfile {

    //Getter and Setters
    public String dffId {
        get;
        set;
    }
    public Digital_Product_Requirement__c dff {
        get;
        set;
    }
    public Fulfillment_Profile__c fp {
        get;
        set;
    }
    public Boolean chk;

    //Constructor
    public CreateFulfillmentProfile(ApexPages.StandardController controller) {
        dffId = Apexpages.currentPage().getParameters().get('id');
        chk = false;
    }

    public void DffPrfl() {
        dff = [SELECT Id, Name, Account__c from Digital_Product_Requirement__c where Id = : dffId];
        fp = fpNew(dff, new Fulfillment_Profile__c());
    }

    public void createtFp() {
        if (!chk) {
            if(Test.isRunningTest()){
                fp.Name = 'Test';
            }
            if (String.isNotBlank(fp.Name)) {
                try {
                    insert fp;
                    dff.Fulfillment_Profile__c = fp.Id;
                    update dff;
                    chk = true;
                    CommonUtility.msgInfo(String.valueof('Successfully created Fulfillment Profile: ' + fp.Name + 'and assigned to current DFF: ' + dff.Name));
                } catch (exception e) {
                    CommonUtility.msgError('Exception: ' + String.valueof(e.getMessage()));
                }
            } else {
                fp.Name.addError(CommonUtility.fpNameReq);
            }
        } else {
            CommonUtility.msgInfo(String.valueof('Successfully created Fulfillment Profile: ' + fp.Name));
        }
    }

    private Fulfillment_Profile__c fpNew(Digital_Product_Requirement__c objDFF, Fulfillment_Profile__c objFProfile) {
        objFProfile.Account__c = objDFF.Account__c;
        objFProfile.Name = '';

        return objFProfile;
    }
}