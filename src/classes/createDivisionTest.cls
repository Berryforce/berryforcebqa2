@isTest
public class createDivisionTest {
    public static testmethod void createDivisionTest(){
        
        Division__c objDiv = TestMethodsUtility.generateDivision();
        
        objDiv.name = '12345';
        //List<Division__c> lstDivision = new list<Division__c>();
        //lstDivision.add(objDiv);
        
        Division__c objDiv2 = TestMethodsUtility.generateDivision();
        
        objDiv2.name = '123';
        //insert objDiv2;
        //lstDivision.add(objDiv2);
        
        Group g1 = new Group(Name='12345', type='Queue');
        insert g1;
        //QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Division__c');
        //insert q1;
        
        ApexPages.StandardController objA = new ApexPages.StandardController(objDiv);
        //PageReference pg = new PageReference('/' + objDiv.Id);
        ApexPages.currentPage().getParameters().put('retURL', '/' + objDiv.Id);
        
        createDivision objASHC = new createDivision(objA);
        objASHC.objDivision = objDiv;
        objASHC.saveData();
        objASHC.cancel();
        objASHC.getFields();
        
        Profile p = TestMethodsUtility.getSysAdminProfile();
        User u = new User(alias = 'utest', email='unittest@unittest.com',
        emailencodingkey='UTF-8', lastname='Unit Test', 
        languagelocalekey='en_US',
        localesidkey='en_GB', profileid = p.Id,
        timezonesidkey='Europe/London', 
        username='unittest@unittest.com');
                 
        System.runAs(u)
        {
            // create leads etc here and continue test
            objASHC.objDivision = objDiv2;
            Test.starttest();
            objASHC.saveData();
            
            Test.stoptest();
        }
        
        
    }
}