@isTest(SeeAllData=true)
private class AccountManagementTest {
    private static Account account;
    private static Canvass__c objCanvass;
    
    static testmethod void testExistingAccountController() {
        createTestData();
        Test.startTest();
        Apexpages.Standardcontroller ctrl = new Apexpages.Standardcontroller(account);
        AccountManagement AM = new AccountManagement(ctrl);
        AM.onLoad();
        System.assertNotEquals(AM.lstAccountWrapper, null);
        AM.accountName = 'Test';
        AM.accountSearch();
        System.assertNotEquals(AM.lstAccountWrapper.size(), 0);
        AM.lstAccountWrapper[0].bFlag = true;       
        AM.convertExistingAccountToChildAccount();
        test.stopTest();
    }
    
    static testmethod void testNewAccountController() {
        createTestData();
        Test.startTest();
        Apexpages.Standardcontroller ctrl = new Apexpages.Standardcontroller(account);
        AccountManagement AM = new AccountManagement(ctrl);
        AM.onLoad();
        AM.createChildAccount();
        AM.getAccFields();
        AM.getMgrFields();
        AM.getAddFields();
        PageReference pageRef = Page.CreateChildAccount;
        Test.setCurrentPage(pageRef);
        AM.objChildAccount = TestMethodsUtility.createAccount('customer');
        AM.saveAccount();
        test.stopTest();
    }
    
    static testmethod void testNewAccountAndCreateContactController() {
        createTestData();
        Test.startTest();
        Apexpages.Standardcontroller ctrl = new Apexpages.Standardcontroller(account);
        AccountManagement AM = new AccountManagement(ctrl);
        AM.onLoad();
        AM.createChildAccount();
        AM.objChildAccount = TestMethodsUtility.createAccount('customer');
        AM.saveAccountandCreateContact();
        system.assertNotEquals(AM.objChildAccount.Id, null);
        test.stopTest();
    }
    
    static testmethod void testNewContactController() {
        createTestData();
        Test.startTest();
        Apexpages.Standardcontroller ctrl = new Apexpages.Standardcontroller(account);
        AccountManagement AM = new AccountManagement(ctrl);
        AM.onLoad();
        AM.createChildAccount();
        AM.objChildAccount = TestMethodsUtility.createAccount('customer');
        AM.saveAccountandCreateContact();
        AM.getConAddressFields();
        AM.getConInfoFields();
        AM.getConFlagFields();
        AM.getConAddFields();
        PageReference pageRef = Page.CreateContact;
        Test.setCurrentPage(pageRef);
        system.assertNotEquals(AM.objChildAccount.Id, null);
        AM.objContact = TestMethodsUtility.createContact(AM.objChildAccount.Id);
        AM.saveContact();
        system.assertNotEquals(AM.objContact.Id, null); 
        test.stopTest();
    }
    
    static testmethod void testCreateContactNewController() {
        createTestData();
        Test.startTest();
        Apexpages.Standardcontroller ctrl = new Apexpages.Standardcontroller(account);
        AccountManagement AM = new AccountManagement(ctrl);
        AM.onLoad();
        AM.createChildAccount();
        AM.objChildAccount = TestMethodsUtility.createAccount('customer');
        AM.saveAccountandCreateContact();
        system.assertNotEquals(AM.objChildAccount.Id, null);
        AM.objContact = TestMethodsUtility.createContact(AM.objChildAccount.Id);
        AM.saveContactAndNew();
        system.assertNotEquals(AM.objContact.Id, null);
        test.stopTest();
    }
    
    static testmethod void testAccountMgmtCancel() {
        createTestData();
        Test.startTest();
        Apexpages.Standardcontroller ctrl = new Apexpages.Standardcontroller(account);
        AccountManagement AM = new AccountManagement(ctrl);
        AM.onLoad();
        AM.cancel();
    }
    
    static testmethod void testNewAccountCancel() {
        createTestData();
        Test.startTest();
        Apexpages.Standardcontroller ctrl = new Apexpages.Standardcontroller(account);
        AccountManagement AM = new AccountManagement(ctrl);
        AM.onLoad();
        AM.createChildAccount();        
        AM.cancelChildAccount();
        test.stopTest();
    }
    
    static testmethod void testNewContactCancel() {
        createTestData();
        Test.startTest();
        Apexpages.Standardcontroller ctrl = new Apexpages.Standardcontroller(account);
        AccountManagement AM = new AccountManagement(ctrl);
        AM.onLoad();
        AM.saveAccountandCreateContact();       
        AM.cancelContact();
        test.stopTest();
    }
    
    static void createTestData() {
        account = TestMethodsUtility.generateAccount('customer');
        insert account;
        Account account1 = TestMethodsUtility.generateAccount('customer');
        insert account1;
        Contact objContact = TestMethodsUtility.createContact(account.Id);
    }
}