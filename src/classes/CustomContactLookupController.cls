public with sharing class CustomContactLookupController {
 
  public Account account {get;set;} // new account to create
  public Contact contact {get;set;}
  public List<Contact> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  public string choice {get;set;}
  public String acid;
  public String opId;
  public Boolean refreshPage {get; set;}  // refreshParentPage
    
  public CustomContactLookupController () {
    //account = new Account();
    // get the current search string
    refreshPage = false;
    acid = System.currentPageReference().getParameters().get('acid');
    opId = System.currentPageReference().getParameters().get('opID');
    
    system.debug('acid========'+acid+'===='+System.currentPageReference().getParameters().get('sId'));
    
    if(System.currentPageReference().getParameters().get('sId')!=null && System.currentPageReference().getParameters().get('sId')!='')
    {
        searchString = System.currentPageReference().getParameters().get('sId');
    }
     
    runSearch();  
  }
  
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
 
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    system.debug('searchString=====11111111====='+searchString);
    results = performSearch(searchString);               
  } 
 
  // run the search and return the records found. 
  private List<Contact> performSearch(string searchString) 
  {
     system.debug('searchString======='+searchString+'====opId==='+opId);
     String strSearch='%'+searchString+'%';
   system.debug('searchString======='+searchString+'==='+strSearch);
   
     Opportunity opt = new Opportunity ();
     list<Opportunity> lstOpt = new list<Opportunity>();
     lstOpt=[SELECT id,name,Account.Id from Opportunity where id=:opId];
    //Commented out by Sathish - JIRA ticket : APT-3809
    //String soql = 'select id, name,FirstName,LastName,Email from Contact WHERE Email !=NULL AND IsActive__c=True';
    
    //Added by Sathish - JIRA ticket : APT-3809
    String soql = 'select id, name,FirstName,LastName,Email from Contact where IsActive__c=True';
    if(searchString != '' && searchString != null)
      soql = soql +  ' AND name LIKE :strSearch ';
      
  if(lstOpt!=null && lstOpt.size()>0)
  {
    //opt=lstOpt[0];
    soql = soql +' AND AccountId=\''+lstOpt[0].AccountId+'\'';
  }
  
  
    soql = soql + ' limit 25';
    System.debug('soql==========='+soql);
    return database.query(soql); 
 
  }
 
  // save the new account record
  public PageReference saveAccount() {
    if(choice != null && choice != '') {
        Opportunity objOpportunity = new Opportunity(id = opId, Signing_Contact__c = choice);
        update objOpportunity;
    }
    else {
        // TO Do display message
        CommonMethods.addError('Please select contact.');
    }
    //system.debug('choice=========='+choice);
    
    /*Id QuoteId = System.currentPageReference().getParameters().get('id');
    if(QuoteId!=null && choice!=null && choice!='')
    {
      system.debug('QuoteId========='+QuoteId);
      BigMachines__Quote__c c = [SELECT Billing_Contact__c,Canvass_Code__c FROM BigMachines__Quote__c 
             WHERE id =: QuoteId];
    system.debug('c==========='+c);  
    c.Billing_Contact__c = choice;
    update c;
    refreshPage = true;
    system.debug('c===2222========'+c);
    }*/
    return null;
 }
 
  // used by the visualforce page to send the link to the right dom element
 /* public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
 */
}