global class DirectoryPaginationSortAsFieldUpdate  Implements Database.Batchable <sObject> {
    String whereCondition = '';
    string query;
    Galley_Staging_Job__c objGSJ;
    
    global DirectoryPaginationSortAsFieldUpdate(){
    
    }
    
    
    global DirectoryPaginationSortAsFieldUpdate(Galley_Staging_Job__c GSJ, String whereCondition) {
        objGSJ = new Galley_Staging_Job__c();
        this.objGSJ = GSJ;
        this.whereCondition = whereCondition;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        //query = 'SELECT ID, DP_Sort_As__c, Sort_As__c FROM Directory_Pagination__c WHERE ';
        query = 'SELECT ID, DP_Sort_As__c, Sort_As__c FROM Directory_Pagination__c';
        if(String.isNotBlank(whereCondition)) {
            query += ' where '+whereCondition;
        } else {
            //query +=  'DP_Sort_As_Checked__c = true';
        }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Directory_Pagination__c> listDP) {
    List<Directory_Pagination__c> lstUpdtDP = new List<Directory_Pagination__c>(); 
    try{
        if(listDP.size() > 0) {
            for(Directory_Pagination__c objDP : listDP) {
                if(objDP.Sort_As__c != null && objDP.DP_Sort_As__c != objDP.Sort_As__c) {
                    objDP.DP_Sort_As__c = objDP.Sort_As__c;
                    lstUpdtDP.add(objDP);
                }                  
            } 
            if(lstUpdtDP != null && lstUpdtDP.size() > 0) {
                update  lstUpdtDP;
            }
        }    
    }
    catch(CustomException objExp) {
        futureCreateErrorLog.createErrorRecordBatch('Error Type : '+objExp.strType+'. Error Message : '+objExp.getMessage(), objExp.strStackTrace, 'Directory Pagination SortAs FieldUpdate');
    }
    catch(Exception objExp) {
        futureCreateErrorLog.createErrorRecordBatch('Error Type : '+objExp.getTypename()+'. Error Message : '+objExp.getMessage(), objExp.getStackTraceString(), 'Directory Pagination SortAs FieldUpdate');
    }

    }
    global void finish(Database.BatchableContext bc) {
        if(objGSJ != null) {
            List<Community_Section_Abbreviation__c> CASlst = CommunitySectionAbbreviationSOQLMethods.fetchCommSecAbb();
            set<string> setDS = new set<string>();
            set<string> setCity = new set<string>();
            map<string,Community_Section_Abbreviation__c> mapstrCSA = new map<string,Community_Section_Abbreviation__c>();
            if(CASlst.size()>0) {
                for(Community_Section_Abbreviation__c iteratorCSA : CASlst) {
                    if(iteratorCSA.Directory_Section__c != null && iteratorCSA.Community_Name__c != null) {
                        mapstrCSA.put(iteratorCSA.Directory_Section__c+iteratorCSA.Community_Name__c,iteratorCSA);
                    }
                    if(iteratorCSA.Directory_Section__c != null){
                        setDS.add(iteratorCSA.Directory_Section__c);   
                    }
                    if(iteratorCSA.Community_Name__c != null){
                        setCity.add(iteratorCSA.Community_Name__c);   
                    }
                }
            }
            if(setDS.size()> 0) {
                ApplyCityServiceBatchClass apcsBatch= new ApplyCityServiceBatchClass(setDS,setCity,mapstrCSA, objGSJ, null);
                Database.executeBatch(apcsBatch);
            }
        } else {
            AsyncApexJob asyncBatch = [Select CreatedById, Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id =:BC.getJobId()];
            list<String> lstEmailAddress = new list<String>{asyncBatch.CreatedBy.Email};
            String strErrorMessage = '';
            if(asyncBatch.NumberOfErrors > 0){
                strErrorMessage = asyncBatch.ExtendedStatus;
            }
            CommonEmailUtils.sendHTMLEmailForTargetObject(asyncBatch.CreatedById, 'Directory Pagination SortAs FieldUpdate ' + asyncBatch.Status, 'The batch Apex job processed ' + asyncBatch.TotalJobItems + '. Please check Exception/Error tab see if any error.');
        }
    }

}