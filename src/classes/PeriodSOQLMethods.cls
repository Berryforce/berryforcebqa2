public with sharing class PeriodSOQLMethods {
	
	public static list<c2g__codaPeriod__c> getCurrentPeriodByName(set<String> setName) {
		return [Select id,Name from c2g__codaPeriod__c where Name IN:setName];
	}

}