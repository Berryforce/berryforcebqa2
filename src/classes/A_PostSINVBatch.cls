/**
 * This process is run in chain from A_CreateSINVBatch finish method
 */
 
/**
 * If you want to run this process after calling the single A_Create, run thse lines
 * Integer scopeSize = 1; //Modify the scope size per chunk
 * A_PostSINVBatch postSINV = new A_PostSINVBatch();
 * Database.execute(postSINV, scopeSize);
 */

public class A_PostSINVBatch implements Database.Batchable<sObject>
{
    private Boolean postAll;
    private List<Id> invoiceIds;
        
    public List<String> errorMessages;
    public List<String> successfulMessages;
    
    public A_PostSINVBatch()
    {
        postAll = true;
        errorMessages = new List<String>();
        successfulMessages = new List<String>();
    }
    
    public A_PostSINVBatch(List<String> errors, List<String> messages, List<Id> documentIds)
    {
        postAll = false;
        invoiceIds = documentIds;
        errorMessages = errors;
        successfulMessages = messages;
        
        if(errorMessages == null)
        {
            errorMessages = new List<String>();
        }
        
        if(successfulMessages == null)
        {
            successfulMessages = new List<String>();
        }
    }

    //If I want to post all documents use this constructor instead of the above one
    public A_PostSINVBatch(List<String> errors, List<String> messages)
    {
        postAll = true;
        errorMessages = errors;
        successfulMessages = messages;
        
        if(errorMessages == null)
        {
            errorMessages = new List<String>();
        }
        
        if(successfulMessages == null)
        {
            successfulMessages = new List<String>();
        }
    }

    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        String qry;
        String invStatus = 'In Progress';
        
        if(postAll)//If I want to post all documents use this constructor and comment above one
        {
            qry = 'Select Id From c2g__codaInvoice__c Where c2g__InvoiceStatus__c = :invStatus';
        }
        else
        {
            qry = 'Select Id From c2g__codaInvoice__c Where c2g__InvoiceStatus__c = :invStatus and Id In :invoiceIds';
        }

        return Database.getQueryLocator(qry);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.Savepoint sp = Database.setSavepoint();

        List<c2g.CODAAPICommon.Reference> refs = new List<c2g.CODAAPICommon.Reference>();

        try
        {
            for(c2g__codaInvoice__c so : (List<c2g__codaInvoice__c>)scope)
            {
                refs.add( c2g.CODAAPICommon.getRef( so.Id, null ) );
            }

            //Check with different APIs but leave the latest one.
            //c2g.CODAAPISalesInvoice_6_0.BulkPostInvoice( A_AppContext.getv6ApiContext(), refs );
            //c2g.CODAAPISalesInvoice_7_0.BulkPostInvoice( A_AppContext.getv7ApiContext(), refs );
            c2g.CODAAPISalesInvoice_8_0.BulkPostInvoice( A_AppContext.getv8ApiContext(), refs );
            
            if(refs.size() > 0)
            {
                successfulMessages.add(refs.size() + ' SINVs have been posted.');
            }
            else
            {
                successfulMessages.add('No SINV has been posted.');
            }
        }
        catch (Exception e)
        {
            Database.rollback(sp);

            errorMessages.add( 'Error during Bulk Create and Post Sales Invoices ' + scope + '; Exception: ' + e.getMessage() + '\n\n');
        }
    }

    public void finish(Database.BatchableContext BC)
    {
        AsyncApexJob batchJob = [
                                    Select
                                        Id,
                                        Status,
                                        NumberOfErrors,
                                        ExtendedStatus,
                                        JobItemsProcessed,
                                        TotalJobItems,
                                        CreatedBy.Email
                                    From
                                        AsyncApexJob
                                    Where
                                        Id = :BC.getJobId()
                                ];

        sendEmail( batchJob.CreatedBy.Email, errorMessages );
    }

    private void sendEmail( String address, List<String> errorMessages)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { address } );

        String body = 'Results of Bulk Create and Post Sales Invoices in chain:\n\n';

        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
            mail.setSubject( 'Bulk Create and Post Sales Invoices in chain completed with errors.');
        }
        else
        {
            body += 'No errors occurred. Find bellow some useful information. \n\n';
            
            if(successfulMessages.size() > 0)
            {
                for( String msg : successfulMessages )
                {
                    body += msg +'\n';
                }
            }

            body += '\n';
            mail.setSubject( 'Bulk Create and Post Sales Invoices in chain completed.');
        }

        mail.setPlainTextBody( body );

        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }
}