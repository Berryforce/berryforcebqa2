@isTest(SeeAllData=true)
private Class FFCreditNotePostBatchControllerTest
{

  static testMethod void TestFFCreditNotePostBatchController()
  {
  
     Account objacc = TestMethodsUtility.generateAccount();
     insert objacc;
     
     Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
     newOpportunity.AccountId = objacc.Id;
     insert newOpportunity;
     
     c2g__codaInvoice__c objSalesInvoice= TestMethodsUtility.createSalesInvoice(objacc,newOpportunity);
     
     objSalesInvoice.Customer_Name__c= objacc.Id;
     
     c2g__codaCreditNote__c objCreditNote = TestMethodsUtility.generateSalesCreditNote(objSalesInvoice,objacc);
     
     List<c2g__codaCreditNote__c> objlstCreditNote = new List<c2g__codaCreditNote__c>();
     objlstCreditNote.add(objCreditNote);
     insert objlstCreditNote;
     
     Set<Id>  ids = new Set<Id>();
     ids.add(objlstCreditNote[0].id);
     
     Test.startTest();
     FFCreditNotePostBatchController objFFCreditNote = new FFCreditNotePostBatchController(ids);
     Id id = Database.executebatch(objFFCreditNote);
     
     Test.StopTest();
  
  }


}