@IsTest
public class MediaTraxAuthTokenSchedulerTest{
  static testMethod void MediaTraxAuthTokenSchedulerTest() {
    
    String CRON_EXP = '0 0 0 ? * MON-FRI';
    
    // Schedule the test job
    Test.startTest();
    //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());
    String jobId = System.schedule('testBasicScheduledApex',
    CRON_EXP, 
    new  MediaTraxAuthTokenScheduler());
    // Get the information from the CronTrigger API object
    
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
    NextFireTime
    FROM CronTrigger WHERE id = :jobId];
    
    // Verify the expressions are the same
    System.assertEquals(CRON_EXP, 
    ct.CronExpression);
    
    // Verify the job has not run
    System.assertEquals(0, ct.TimesTriggered);
    Test.stopTest();
  }
}