public with sharing class NationalStagingLineItemHandlerController {
    public static void onBeforeInsert(list<National_Staging_Line_Item__c> lstNSLI) {
        //SYSTEM.DEBUG('Testingg nsli a Limits.getCpuTime() '+Limits.getCpuTime() + ' of ' + Limits.getLimitCpuTime());
        populateProductByUDAC(lstNSLI, null);
        //SYSTEM.DEBUG('Testingg nsli b Limits.getCpuTime() '+Limits.getCpuTime() + ' of ' + Limits.getLimitCpuTime());
        populateProductDPM(lstNSLI);
        //SYSTEM.DEBUG('Testingg nsli c Limits.getCpuTime() '+Limits.getCpuTime() + ' of ' + Limits.getLimitCpuTime());
    }
    
    public static void onBeforeUpdate(list<National_Staging_Line_Item__c> lstNSLI, Map<Id, National_Staging_Line_Item__c> oldMap) {
        //SYSTEM.DEBUG('Testingg nsli d Limits.getCpuTime() '+Limits.getCpuTime() + ' of ' + Limits.getLimitCpuTime());
        populateProductDPM(lstNSLI, oldMap);
        //SYSTEM.DEBUG('Testingg nsli e Limits.getCpuTime() '+Limits.getCpuTime() + ' of ' + Limits.getLimitCpuTime());
        poupulatePrevFieldValues(lstNSLI, oldMap);
    }
    
    public static void onAfterUpdate(list<National_Staging_Line_Item__c> lstNSLI, Map<Id, National_Staging_Line_Item__c> oldMap) {
        //SYSTEM.DEBUG('Testingg nsli f Limits.getCpuTime() '+Limits.getCpuTime() + ' of ' + Limits.getLimitCpuTime());
        populateSalesRate(lstNSLI);
        //SYSTEM.DEBUG('Testingg nsli g Limits.getCpuTime() '+Limits.getCpuTime() + ' of ' + Limits.getLimitCpuTime());
    }
    
    public static void onAfterInsert(list<National_Staging_Line_Item__c> lstNSLI) {
        //SYSTEM.DEBUG('Testingg nsli h Limits.getCpuTime() '+Limits.getCpuTime() + ' of ' + Limits.getLimitCpuTime());
        populateSalesRate(lstNSLI);
        //SYSTEM.DEBUG('Testingg nsli i Limits.getCpuTime() '+Limits.getCpuTime() + ' of ' + Limits.getLimitCpuTime());
    }
    
    /*private static boolean findDataMigrationUser() {
        String strCurrentUserId = Userinfo.getUserId();
        if(strCurrentUserId.length() > 15) {
            strCurrentUserId = strCurrentUserId.substring(0, 15);
        }
        if(strCurrentUserId.equals(System.Label.IntegrationUser)) {
            return true;
        }
        return false;
    }*/
    
    public static void populateProductByUDAC(list<National_Staging_Line_Item__c> lstNSLI, map<Id, National_Staging_Line_Item__c> oldMap) {
        Set<String> setProdcutUDAC = new set<String>();
        for(National_Staging_Line_Item__c iterator : lstNSLI) {
            if(String.isNotBlank(iterator.UDAC__c)) {
                setProdcutUDAC.add(iterator.UDAC__c);
            }
                if(iterator.UDAC__c!='TMCP1')
                    iterator.Formatted_Advertising_Data__c=formatAdvertisingData(iterator.Advertising_Data__c,iterator.SPINS__c);
                else
                    iterator.Formatted_Advertising_Data__c=iterator.Advertising_Data__c;
        }
        
        map<String, Product2> mapProduct = new map<String, Product2>();
        map<String, Product2> mapInactiveProduct = new map<String, Product2>();
        map<String, Product2> mapAllProduct = new map<String, Product2>();
        if(setProdcutUDAC.size() > 0) {
            list<Product2> lstProdcut = [Select Id, Name,IsActive,ProductCode from Product2 where ProductCode IN:setProdcutUDAC];
            for(Product2 iterator : lstProdcut) {
                mapAllProduct.put(iterator.ProductCode, iterator);
                if(iterator.IsActive) 
                mapProduct.put(iterator.ProductCode, iterator);
                else
                mapInactiveProduct.put(iterator.ProductCode, iterator);
            }
        }
        
        for(National_Staging_Line_Item__c iterator : lstNSLI) {
            if(String.isNotBlank(iterator.UDAC__c)) {
                if(mapProduct.get(iterator.UDAC__c) != null) {
                    iterator.Product2__c = mapProduct.get(iterator.UDAC__c).Id;
                }
                if(mapInactiveProduct.containskey(iterator.UDAC__c)){
                   //iterator.Line_Error_Description__c+=' Invalid Product';
                   //iterator.Line_Error_Description__c = iterator.Line_Error_Description__c == null ? 'Invalid Product' : iterator.Line_Error_Description__c + ' Invalid Product'; 
                   iterator.Line_Error_Description__c = iterator.Line_Error_Description__c == null ? 'Invalid Product' : iterator.Line_Error_Description__c.contains('Invalid Product') ? iterator.Line_Error_Description__c: iterator.Line_Error_Description__c+' Invalid Product'; 
                   iterator.Product2__c = mapInactiveProduct.get(iterator.UDAC__c).Id;
                }
                if(!mapAllProduct.containskey(iterator.UDAC__c)){
                    iterator.Line_Error_Description__c = iterator.Line_Error_Description__c == null ? 'Invalid Product' : iterator.Line_Error_Description__c.contains('Invalid Product') ? iterator.Line_Error_Description__c: iterator.Line_Error_Description__c+' Invalid Product'; 
                }
            }
        }
    }
    
    public static void populateProductDPM(list<National_Staging_Line_Item__c> lstNSLI, map<Id, National_Staging_Line_Item__c> oldMap){
        
        list<National_Staging_Line_Item__c> lstUpdtNSLI = new list<National_Staging_Line_Item__c>();
        //System.debug('Testingg 1');
        for(National_Staging_Line_Item__c iterator : lstNSLI){
            //System.debug('*******NS RECORD ENTER*********');
            National_Staging_Line_Item__c objbeforeUpdateNSLI = oldMap.get(iterator.Id);
            if(iterator.UDAC__c!='TMCP1')
                    iterator.Formatted_Advertising_Data__c=formatAdvertisingData(iterator.Advertising_Data__c,iterator.SPINS__c);
                else
                    iterator.Formatted_Advertising_Data__c=iterator.Advertising_Data__c;
           // if(objbeforeUpdateNSLI.UDAC__c != iterator.UDAC__c || objbeforeUpdateNSLI.Directory__c != iterator.Directory__c)
           // {
                if(iterator.UDAC__c == null){
                    iterator.Product2__c = null;
                    iterator.National_Pricing__c = null;
                }
                else{
                    lstUpdtNSLI.add(iterator);
                }
          //  }
        }
        if(lstUpdtNSLI.size() > 0) {
            populateProductDPM(lstUpdtNSLI);
        }
    }
    
    public static void populateProductDPM(list<National_Staging_Line_Item__c> lstNSLI) {
        set<Id> NationalStagingLIOldId = new set<Id>();
        set<string> setStrUdac = new set<string>();
        map<string,National_Staging_Line_Item__c> mapUdacDirNSLI = new map<string,National_Staging_Line_Item__c>();
        map<string,Directory_Product_Mapping__c> mapUDACDirIdDPM = new map<string,Directory_Product_Mapping__c>();
        set<id> setDirIds = new set<id>();
        National_Staging_Line_Item__c objNSLI;
        
        list<National_Staging_Line_Item__c> lstUpdtNSLI = new list<National_Staging_Line_Item__c>();
        for(National_Staging_Line_Item__c iterator : lstNSLI){
        
            if(iterator.UDAC__c != null && iterator.Directory__c!=null){
                //System.debug('UDAC Infor : '+ iterator.UDAC__c + ' Dir Infor : '+ iterator.Directory__c);
                setStrUdac.add(iterator.UDAC__c);
                setDirIds.add(iterator.Directory__c);
                mapUdacDirNSLI.put(iterator.UDAC__c+iterator.Directory__c.substring(0,15), iterator);
                
            //System.debug('Testingg 8');
            }
        }
        
        if(setStrUdac.size() > 0){
            List<Directory_Product_Mapping__c> lstDirProdMap = DirectoryProductMappingSOQLMethods.getDirProdMapByUdacAndDirId(setStrUdac, setDirIds);
            
            //System.debug('Testingg 9');
            for(Directory_Product_Mapping__c objDPM : lstDirProdMap){
                //System.debug('UDAC DMP : '+ objDPM.Product_Code_UDAC__c + ' Dir DMP : '+ objDPM.Directory__c);
                mapUDACDirIdDPM.put(objDPM.Product_Code_UDAC__c + String.valueOf(objDPM.Directory__c).substring(0,15), objDPM);                                        
            }            
        }
        
        //if(mapUDACDirIdDPM.size() > 0) {
            
            //System.debug('Testingg 10');
            list<Directory_Product_Mapping__c> lstDPMInsert = new list<Directory_Product_Mapping__c>();
            set<String> setProdcut = new set<String>();
            String strUDACDir;
            for(National_Staging_Line_Item__c iterator : lstNSLI){
                if(String.isNotBlank(iterator.UDAC__c) && iterator.Directory__c!=null)  {
                    strUDACDir = iterator.UDAC__c+iterator.Directory__c.substring(0,15);
                    //System.debug('Testingg 11');
                    if(mapUDACDirIdDPM.size() > 0 && mapUDACDirIdDPM.get(strUDACDir) != null) {
                        //if(!findDataMigrationUser()) {
                            iterator.National_Pricing__c = mapUDACDirIdDPM.get(strUDACDir).id;
                            //System.debug('Testingg 12===>'+mapUDACDirIdDPM.get(strUDACDir));
                            iterator.Product2__c = mapUDACDirIdDPM.get(strUDACDir).Product2__c;
                        //}
                    }
                    else {
                        //added as per QA-1962 on 10/15
                        //iterator.Line_Error_Description__c = 'Invalid UDAC';
                       iterator.Line_Error_Description__c = iterator.Line_Error_Description__c == null ? 'Invalid Product' : iterator.Line_Error_Description__c.contains('Invalid Product') ? iterator.Line_Error_Description__c: iterator.Line_Error_Description__c+' Invalid Product'; 
                       // iterator.Line_Error_Description__c = iterator.Line_Error_Description__c == null ? 'Invalid Product' : iterator.Line_Error_Description__c + ' Invalid Product'; 
                   
                        //Commented as per qa-1962 on 10/15
                        /*if(!setProdcut.contains(iterator.UDAC__c)) {
                            //System.debug('Debug By Sat : '+ iterator.UDAC__c+''+iterator.Directory__c);
                            setProdcut.add(iterator.UDAC__c);
                            if(iterator.Product2__c != null && iterator.Directory__c != null) {
                                lstDPMInsert.add(new Directory_Product_Mapping__c(Name = iterator.Directory_Code__c + ' ' +iterator.UDAC__c, Directory__c = iterator.Directory__c, Product2__c = iterator.Product2__c, is_Active__c = true));
                            }
                        }*/
                    }
                }
            }
            
           /* if(lstDPMInsert.size() > 0) {
                //System.debug('Before Insert : '+ lstDPMInsert);
                insert lstDPMInsert;
                //if(!findDataMigrationUser()) {
                    for(Directory_Product_Mapping__c objDPM : lstDPMInsert){
                        mapUDACDirIdDPM.put(objDPM.Product_Code_UDAC__c + String.valueOf(objDPM.Directory__c).substring(0,15), objDPM);
                    }               
                    //String strUDACDir;
                    for(National_Staging_Line_Item__c iterator : lstNSLI){
                        strUDACDir = iterator.UDAC__c+iterator.Directory__c.substring(0,15);
                        if(String.isNotBlank(iterator.UDAC__c)){
                            if(mapUDACDirIdDPM.get(strUDACDir) != null) {
                                iterator.National_Pricing__c = mapUDACDirIdDPM.get(strUDACDir).id;
                                iterator.Product2__c = mapUDACDirIdDPM.get(strUDACDir).Product2__c;
                            }
                        }
                    }
                //}
            }*/
        //}
    }
    
    public static void populateSalesRate(list<National_Staging_Line_Item__c> lstNSLI) {
        if(CommonVariables.NSLISaleRateRepeatCheck) {
            CommonVariables.NSLISaleRateRepeatCheck = false;
            list<National_Staging_Line_Item__c> listNSLI = new list<National_Staging_Line_Item__c>();      
            
            listNSLI = NationalStagingLineItemSOQLMethods.getNationalStagingLIListByNSLI(lstNSLI);                       
                
            for(National_Staging_Line_Item__c iterator : listNSLI) {
                //Decimal tempSalesRate = (iterator.Local_Full_Rate__c - (iterator.Local_Full_Rate__c * iterator.National_Discount__c)).setScale(3);                
                Decimal tempSalesRate = 0.000;
                if(iterator.National_Staging_Header__r.Directory__r.IsCompanion__c) {
                    if(iterator.Product2__r.Print_Product_Type__c == CommonMessages.Specialty) {
                        tempSalesRate = (iterator.Local_Full_Rate__c - (iterator.Local_Full_Rate__c * (iterator.National_Discount__c/100))).setScale(3);
                    }
                } else if(iterator.Product2__r.Print_Product_Type__c == CommonMessages.Specialty) {                 
                    tempSalesRate = (iterator.Local_Full_Rate__c - (iterator.Local_Full_Rate__c * (iterator.National_Discount__c/100))).setScale(3);
                } else if(iterator.National_Pricing__r.Exclude_National_Upcharge__c) { 
                        tempSalesRate = (iterator.Local_Full_Rate__c + (iterator.Local_Full_Rate__c * (iterator.National_Discount__c/100))).setScale(3);
                    } else if(iterator.National_Staging_Header__r.Directory__r.National_Rate_Up_for_Companion__c != null) {
                        Decimal t = Math.CEIL(((((iterator.National_Staging_Header__r.Directory__r.National_Rate_Up_for_Companion__c)/100) + 1) * iterator.Local_Full_Rate__c)/0.05) * 0.05; 
                        tempSalesRate = (t - (t * (iterator.National_Discount__c/100))).setScale(3);
                    } else {
                        tempSalesRate = (iterator.Local_Full_Rate__c - (iterator.Local_Full_Rate__c * (iterator.National_Discount__c/100))).setScale(3);    
                }
                
                Integer strLen = String.valueOf(tempSalesRate).length();
                
                if(String.valueOf(tempSalesRate).right(1) == '0') {
                    if(String.valueOf(tempSalesRate).substring(strLen - 2, strLen - 1) == '0') {
                        if(iterator.National_Staging_Header__r.Directory__c != null) {
                            iterator.Sales_Rate__c = tempSalesRate.setScale(2) * iterator.National_Staging_Header__r.Directory__r.Months_for_National__c;
                        }
                    } else {
                        if(Integer.valueOf(String.valueOf(tempSalesRate).substring(strLen - 2, strLen - 1)) > 5) {
                            if(Integer.valueOf(String.valueOf(tempSalesRate).substring(strLen - 3, strLen - 2)) > 5) {
                                if(Integer.valueOf(String.valueOf(tempSalesRate).substring(strLen - 3, strLen - 2)) == 9) {
                                    if(iterator.National_Staging_Header__r.Directory__c != null) {
                                        iterator.Sales_Rate__c = Decimal.valueOf(tempSalesRate.round(System.RoundingMode.CEILING)) * iterator.National_Staging_Header__r.Directory__r.Months_for_National__c;
                                    }
                                } else {
                                    if(iterator.National_Staging_Header__r.Directory__c != null) {
                                        iterator.Sales_Rate__c = Decimal.valueOf(String.valueOf(tempSalesRate).left(strLen - 3) + String.valueOf(Integer.valueOf(String.valueOf(tempSalesRate).substring(strLen - 3, strLen - 2)) + 1)) * iterator.National_Staging_Header__r.Directory__r.Months_for_National__c;
                                    }
                                }                                
                            } else {
                                if(iterator.National_Staging_Header__r.Directory__c != null) {
                                    iterator.Sales_Rate__c = Decimal.valueOf(String.valueOf(tempSalesRate).left(strLen - 3) + String.valueOf(Integer.valueOf(String.valueOf(tempSalesRate).substring(strLen - 3, strLen - 2)) + 1)) * iterator.National_Staging_Header__r.Directory__r.Months_for_National__c;
                                }
                            }
                        } else {
                            if(iterator.National_Staging_Header__r.Directory__c != null) {
                                iterator.Sales_Rate__c = Decimal.valueOf(String.valueOf(tempSalesRate).left(strLen - 2) + '5') * iterator.National_Staging_Header__r.Directory__r.Months_for_National__c;
                            }
                        }
                    }
                } else if(String.valueOf(tempSalesRate).substring(strLen - 2, strLen - 1) == '0') {
                        if(Integer.valueOf(String.valueOf(tempSalesRate).right(1)) >= 1) {
                            if(iterator.National_Staging_Header__r.Directory__c != null) {
                                iterator.Sales_Rate__c = Decimal.valueOf(String.valueOf(tempSalesRate).left(strLen - 2) + '5') * iterator.National_Staging_Header__r.Directory__r.Months_for_National__c;
                            }
                        } else {
                            if(iterator.National_Staging_Header__r.Directory__c != null) {
                                iterator.Sales_Rate__c = tempSalesRate.setScale(2) * iterator.National_Staging_Header__r.Directory__r.Months_for_National__c;
                            }
                        } 
                } else {
                    if(Integer.valueOf(String.valueOf(tempSalesRate).substring(strLen - 2, strLen - 1)) >= 5) {
                        if(iterator.National_Staging_Header__r.Directory__c != null) {
                            iterator.Sales_Rate__c = Decimal.valueOf(String.valueOf(tempSalesRate).left(strLen - 3) + String.valueOf(Integer.valueOf(String.valueOf(tempSalesRate).substring(strLen - 3, strLen - 2)) + 1) + '0') * iterator.National_Staging_Header__r.Directory__r.Months_for_National__c;
                        }
                    } else {
                        if(iterator.National_Staging_Header__r.Directory__c != null) {
                            iterator.Sales_Rate__c = Decimal.valueOf(String.valueOf(tempSalesRate).left(strLen - 2) + '5') * iterator.National_Staging_Header__r.Directory__r.Months_for_National__c;
                        }
                    }
                }
            }           
            update listNSLI;
        }
    }
    
    public static string formatAdvertisingData(string AdvData,string SPINS){
        string formattedAdvData=AdvData;
        
        if(AdvData!=null && AdvData.length()>0 ){
            string lowercaseAdvData=AdvData.tolowercase();
            if(lowercaseAdvData.startsWith('www.') || lowercaseAdvData.startsWith('http://') || lowercaseAdvData.startsWith('https://')){
                formattedAdvData=AdvData.toLowerCase();
            }
            else{
                if(SPINS!=null && SPINS.length()>0){
                    if(SPINS!='SP' && SPINS != 'A' ){
                        List<string> strString=AdvData.split(' ');
                        string strCapitalAdvData='';
                        for (String x : strString) {
                            if(x != '') {
                                strCapitalAdvData += x.substring(0,1).toUpperCase()+x.substring(1,x.length()).toLowerCase() + ' ';
                            }
                        }
                       formattedAdvData=strCapitalAdvData;
                    }
                    else{
                       formattedAdvData=AdvData;
                    }
               }
               else{
                   List<string> strString=AdvData.split(' ');
                    string strCapitalAdvData='';
                    for (String x : strString) {
                        if(x != '') {
                            strCapitalAdvData += x.substring(0,1).toUpperCase()+x.substring(1,x.length()).toLowerCase() + ' ';
                        }
                    }
                    formattedAdvData=strCapitalAdvData;
               }
               system.debug('########'+formattedAdvData);
            }
        }
        return formattedAdvData;
    }
    
    public static void poupulatePrevFieldValues(list<National_Staging_Line_Item__c> lstNSLI, map<Id, National_Staging_Line_Item__c> oldMap) {
        for(National_Staging_Line_Item__c iterator : lstNSLI) {
            National_Staging_Line_Item__c oldNSLI = oldMap.get(iterator.Id);
            if(iterator.Sales_Rate__c != oldNSLI.Sales_Rate__c) {
                iterator.Previous_Sales_Rate__c = oldNSLI.Sales_Rate__c;
            }
            if(iterator.UDAC__c != oldNSLI.UDAC__c) {
                iterator.NSLI_Previous_UDAC__c = oldNSLI.UDAC__c;
            }
        }
    }
}