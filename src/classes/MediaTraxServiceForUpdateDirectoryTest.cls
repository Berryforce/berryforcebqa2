@isTest
public class MediaTraxServiceForUpdateDirectoryTest{
    public static testmethod void MediaTraxUpdateDirectoryBatchTest(){
        //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());
        
        
        List<MediaTrax_API_Configuration__c> lstMedia = new list<MediaTrax_API_Configuration__c>();
        MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='GetAuthToken',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/authentication.cfc',HEADER_SOAPACTION__C='GetAuthentication',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='GetAuthenticationResponse',RETURN_METHOD__C='GetAuthenticationReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='',OPERATION__C='');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='UpdateDirectories',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/directories.cfc',HEADER_SOAPACTION__C='UpdateDirectories',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='UpdateDirectoriesResponse',RETURN_METHOD__C='UpdateDirectoriesReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='directoryIds,publisherIds,directoryNames,directoryNumbers,directoryNumber2s,telcoRecoNumbers,timeZoneIds',OPERATION__C='Update');
        lstMedia.add(objMedia);
        insert lstMedia;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        Account objAcc = TestMethodsUtility.generateAccount('customer');
        lstAccount.add(objAcc);
        objAcc = TestMethodsUtility.generateAccount('publication');
        objAcc.Media_Trax_Publisher_ID__c = '123';
        lstAccount.add(objAcc);
        
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        system.assertNotEquals(objTelco.Telco_Code__c, null);
        System.debug('Testingg objTelco '+objTelco);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        objDir.Media_Trax_Directory_ID__c = '123';
        objDir.Is_Changed__c = true;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory(); 
        
        MediaTraxServiceForUpdateDirectoryBatch objUDCallout = new MediaTraxServiceForUpdateDirectoryBatch();
        database.executebatch(objUDCallout );
    }
}