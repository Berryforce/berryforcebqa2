public with sharing class NewOpportunityOverriddenController {

    public Pagereference onLoad(){
        system.debug('********onLoad start**********');
        ID accountID = ApexPages.currentPage().getParameters().get('accid');
        system.debug('********onLoad start : '+ accountID);
        string modifyid = ApexPages.currentPage().getParameters().get('modifyid');
        string strURL;
        PageReference newPage = new PageReference('/006/e?');
        
        if(accountID != null) {
            Set<ID> accID = new Set<ID>();
            accID.add(accountID);
            map<ID, Account> mapAccount = AccountSOQLMethods.getAccountContactByAccountID(accID);
            Account objAccount = mapAccount.get(accountID);
            /*if(objAccount.Delinquency_Indicator__c) {
                CommonMethods.addError(CommonMessages.accountDelinquencyIndicator);
                return null;
            }*/
            integer i = 0;
            for(String keyValue : ApexPages.currentPage().getParameters().keySet()){
                if(!keyValue.contains('override') ){
                    newPage.getParameters().put(keyValue, ApexPages.currentPage().getParameters().get(keyValue));
                }
            }
            newPage.getParameters().remove('save_new');
            
            
            NewOpportunityAutoPopulate__c autoPopulateFields = NewOpportunityAutoPopulate__c.getValues('opportunity');
            String[] strArray = autoPopulateFields.Opportunity_Fields__c.split(',');
            
            newPage.getParameters().put(strArray[0],string.valueOf(objAccount.Name) + ' '+ DateTime.now().format('MM/dd/yyyy'));
            newPage.getParameters().put(strArray[1],string.valueOf(objAccount.Account_Manager__c));
            newPage.getParameters().put(strArray[2],objAccount.Account_Manager__r.Name);
            if(objAccount.Primary_Canvass__c != null){
                newPage.getParameters().put(strArray[3],string.valueOf(objAccount.Primary_Canvass__c));
                newPage.getParameters().put(strArray[4],string.valueOf(objAccount.Primary_Canvass__r.Name));
            }
            if(objAccount.Contacts.size() > 0){
                Contact objContact = objAccount.Contacts[0];
                newPage.getParameters().put(strArray[5],string.valueOf(objContact.Id));
                newPage.getParameters().put(strArray[6],string.valueOf(objContact.Name));
            }
            newPage.getParameters().put(strArray[7],string.valueOf(objAccount.Account_Number__c));
        } else {
            /*for(String keyValue : ApexPages.currentPage().getParameters().keySet()){
                if(!keyValue.contains('override') && !keyValue.contains('save')){
                    newPage.getParameters().put(keyValue, ApexPages.currentPage().getParameters().get(keyValue));
                }
            }*/
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please navigate to Account detail page and click on New Opportunity button under Opportunities related list to create a Opportunity.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        newPage.getParameters().put('00NZ0000000wdL9',modifyid);
        newPage.getParameters().put('nooverride','1');
        system.debug('********onLoad End**********');
        return newPage.setRedirect(true);
    }
    
    public NewOpportunityOverriddenController(ApexPages.StandardController controller){
        
    }

}