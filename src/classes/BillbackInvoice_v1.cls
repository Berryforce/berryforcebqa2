public class BillbackInvoice_v1 {

    public String dateVal { get; set; }
    public String DirName { get; set; }
    public String invNo { get; set; }
    public String todayDate { get; set; }
    public String deId;
    public String AccId;
    public String TelcoId;
    public String datepubVal{get;set;}
    public Date Today { get { return Date.today(); }}
    public Directory_Edition__c objDE{get;set;}
    public Edition_Cost__c objEC{get;set;}
    public List<Edition_Cost__c> objECLst{get;set;}
    public Telco__c objTelco{get;set;}
    public Account objAcc{get;set;}
    public Contact objCon{get;set;}
    public billing_settlement__c objBS{get;set;}
    
    
    public BillbackInvoice_v1 (){
        objDE = new Directory_Edition__c();
        objTelco = new Telco__c();
        objAcc = new Account();
        objcon = new contact();
        objBS = new billing_settlement__c();
        
        //Calculation of Todays date
        Date tdate = system.today();
        String Year =string.valueof(tdate.year());
        String Month = string.valueof(tdate.month());
        String day = string.valueof(tdate.day());
        if(Month.length() < 2){
            dateVal = '-'+'0' + Month + Year;
        }
        else {
            dateVal= '-'+ Month + Year;
        }
        deId = apexPages.currentpage().getParameters().get('id');
        if(deId != null){
            system.debug('************Directory Edition ID ******'+deId); 
            objDE = [Select Id,Name,Directory_code__c,Directory__r.Name,Setup_Cost__c,Printing_Cost__c,Other_Cost__c,Total_Cost__c,Total_Edition_Cost__c,pub_date__c,Telco__c from Directory_Edition__c where Id =: deId];
            if(objDE.pub_date__c != null){
            Date pdate = objDE.Pub_Date__c;
                String pYear =string.valueof(pdate.year());
                String pMonth = string.valueof(pdate.month());
                String pday = string.valueof(pdate.day());
                if(pMonth.length() < 2){
                    datepubVal = '0' + pMonth +'/'+ pYear;
                }
                else {
                    datepubVal = pMonth +'/'+ pYear;
                }
            }
        }
        if(objDE.Telco__c != null){
           objTelco = [Select Id,Name,Account__c from Telco__c where Id =: objDE.Telco__c]; 
        }
        if(objTelco.Account__c != null){
            AcciD = objTelco.Account__c;
        }
        
        if(AccId != null){
            objAcc=[Select Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Phone FROM Account where Id =:AcciD limit 1 ];
            objCon =[Select PrimaryBilling__c, Name,Email, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity, LastName, FirstName, AccountId,Account.Name From Contact where AccountId =: AccId and PrimaryBilling__c = True];
                         
        }
        
        if(objDE.id != null){
            objBS = [select id,name,Telco_Cost_Share__c from billing_settlement__c where Directory_Edition__c =: objDE.Id limit 1]; 
        }
        editionCostLst();        
    }
    
    public List<Edition_Cost__c> editionCostLst(){
        objECLst= new List<Edition_Cost__c>();
        objECLst = [Select Id,Name,Cost_Type__c,Amount__c from Edition_Cost__c where Directory_Edition__c = : deId];
        if(objECLst.size()>0){
         return objECLst;
        }
        
        return null;    
    }
}