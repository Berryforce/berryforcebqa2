global class MediaTraxServiceForAddDirectoryBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global String SOQL = '';
    global Database.QueryLocator start(Database.BatchableContext bc){
        SOQL = 'Select Id, Directory_Code__c, Name, Media_Trax_Publisher_ID__c, Media_Trax_Directory_ID__c, Telco_Provider_Code__c from Directory__c where Directory_Code__c != null and Media_Trax_Publisher_ID__c != null and Telco_Provider_Code__c != null and Media_Trax_Directory_ID__c = null';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, list<Directory__c> lstDirectory){
        
        MediaTraxHeadingCalloutController_V1 objMediaTraxService = new MediaTraxHeadingCalloutController_V1();
        objMediaTraxService.ProcessMediaTraxForAddDirectoryScope(lstDirectory);
    }
    
    global void finish(Database.BatchableContext bc){
    }
}