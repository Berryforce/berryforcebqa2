@isTest
public class TestBSure_ViewCreditApplicationOpp
{
     static testMethod void myUnitTest() {
        Vertex_Berry__BSure_Configuration_Settings__c objCSettings1 = new Vertex_Berry__BSure_Configuration_Settings__c();
        objCSettings1.Name = 'CACreateAccess';
        objCSettings1.Vertex_Berry__Parameter_Key__c = 'CACreateAccess';
        objCSettings1.Vertex_Berry__Parameter_Value__c = 'CACreateAccess';
        insert objCSettings1;
        system.assertEquals('CACreateAccess', objCSettings1.Vertex_Berry__Parameter_Value__c);
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name =
                                 'Standard User' or name = 'System Administrator'];
        for(Profile p : ps){
         profiles.put(p.name, p.id);
        } 
        User standard = new User(
                alias = 'standt',
                email='standarduser@testorg.com',
                emailencodingkey='UTF-8', 
                lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US',
                profileid = profiles.get('Standard User'),
                timezonesidkey='America/Los_Angeles',
                username='standardusertest@testorg.com');
        insert standard;
        
         //BSure_ViewCreditApplicationOpp cobj = new BSure_ViewCreditApplicationOpp();
         BSure_ViewCreditApplicationOpp.validatePermissionCA(standard.id);
        
     }
}