//OrderLineItemTrigger Trigger test class
@isTest(SeeAllData = true)
public class  OrderLineItem_BUIDAIUTest {
    public static list<Account> lstAccount;
    static testmethod void OLItest() {
        Test.startTest();
        lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();        
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        List<Directory_Edition__c> objDirEdList=new List<Directory_Edition__c>();
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        Date myDate = Date.newInstance(1960, 2, 17);
        objDirEd.Pub_Date__c=myDate;
        objDirEdList.add(objDirEd);
        //insert objDirEd;
        
        Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        Date myDate1 = Date.newInstance(1962, 3, 17);
        objDirEd1.Pub_Date__c=myDate1;
         objDirEdList.add(objDirEd1);
        //insert objDirEd1;
        
        insert objDirEdList;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
            newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        //added by Mythili...
       /* list<PricebookEntry> lstPBE=new list<PricebookEntry>();
        for(Product2 iterator:lstProduct){
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstPBE.add(pbe);
        }
        insert lstPBE;*/
      for(Product2 iterator : lstProduct) {
            Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
            objDPM.Product2__c = iterator.Id;
            lstDPM.add(objDPM);
        }
        insert lstDPM;
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        insert newOpportunity;
        
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
            OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
            objOLI.PricebookEntryId = iterator.Id;
            objOLI.Billing_Duration__c = 12;
            objOLI.Directory__c = objDir.Id;
            //objOLI.Directory_Edition__c = objDirEd.Id;
            objOLI.Directory_Edition__c = objDirEdList[0].Id;
            objOLI.Full_Rate__c = 30.00;
            objOLI.UnitPrice = 30.00;
            objOLI.Package_ID__c = '123456';
            objOLI.Package_ID_External__c = TestMethodsUtility.generateRandomString(5);
            objOLI.Billing_Partner__c = objOLI.Id;
            objOLI.OpportunityId = newOpportunity.Id;
            objOLI.Directory_Heading__c = objDH.Id;
            objOLI.Directory_Section__c = objDS.Id;
            lstOLI.add(objOLI);
            mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        for(OpportunityLineItem iterator : lstOLI) {
            Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
            objOLI.Package_ID__c = iterator.Package_ID__c;
            objOLI.Package_Item_Quantity__c = 2;
            objOLI.Package_ID_External__c = iterator.Package_ID_External__c;
            objOLI.Directory__c = iterator.Directory__c;
            objOLI.Directory_Edition__c = iterator.Directory_Edition__c;
            objOLI.UnitPrice__c = iterator.UnitPrice;
            objOLI.Opportunity_line_Item_id__c = iterator.Id;
            objOLI.Media_Type__c = CommonMessages.oliPrintProductType;
            objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
            objOLI.Directory_Section__c = iterator.Directory_Section__c;
            objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
            objOLI.Product_Inventory_Tracking_Group__c = 'YP Leader Ad';
            lstOrderLI.add(objOLI);
        }
                
        insert lstOrderLI;
        Order_Line_Items__c firstOLI = lstOrderLI[0];
        firstOLI.Talus_Go_Live_Date__c = system.today();
        firstOLI.Directory_Edition__c = objDirEdList[1].Id;
        firstOLI.Talus_Cancel_Date__c = system.today();
        firstOLI.Status__c = 'Cancelation Requested';
        update firstOLI;
        Order_Line_Items__c secondOLI = lstOrderLI[1];
        secondOLI.Talus_Go_Live_Date__c = system.today();
        update secondOLI;
       
        Test.stopTest();
    }
    
    static testmethod void OLIDeleteTest() {
        Test.startTest();
        lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        List<Directory_Edition__c> objDirEdList=new List<Directory_Edition__c>();
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        Date myDate2 = Date.newInstance(1962, 3, 17);
        objDirEd.Pub_Date__c=myDate2;
        objDirEdList.add(objDirEd);
        //objDirEd.Edition_code__c='NA';
        //insert objDirEd;
        
        Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        Date myDate3 = Date.newInstance(1982, 3, 17);
        objDirEd1.Pub_Date__c=myDate3;
         objDirEdList.add(objDirEd1);
        //objDirEd1.Edition_code__c='BOTS';
        //insert objDirEd1;
        
        insert objDirEdList;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
            newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        
        //added by Mythili...
       /* list<PricebookEntry> lstPBE=new list<PricebookEntry>();
        for(Product2 iterator:lstProduct){
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstPBE.add(pbe);
        }
        insert lstPBE;*/
        
        for(Product2 iterator : lstProduct) {
            Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
            objDPM.Product2__c = iterator.Id;
            lstDPM.add(objDPM);
        }
        insert lstDPM;
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        insert newOpportunity;
        
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
            OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
            objOLI.PricebookEntryId = iterator.Id;
            objOLI.Billing_Duration__c = 12;
            objOLI.Directory__c = objDir.Id;
            objOLI.Directory_Edition__c = objDirEdList[0].Id;
            objOLI.Full_Rate__c = 30.00;
            objOLI.UnitPrice = 30.00;
            objOLI.Package_ID__c = '123456';
            objOLI.Package_ID_External__c = TestMethodsUtility.generateRandomString(5);
            objOLI.Billing_Partner__c = objOLI.Id;
            objOLI.OpportunityId = newOpportunity.Id;
            objOLI.Directory_Heading__c = objDH.Id;
            objOLI.Directory_Section__c = objDS.Id;
            lstOLI.add(objOLI);
            mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        for(OpportunityLineItem iterator : lstOLI) {
            Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
            objOLI.Package_ID__c = iterator.Package_ID__c;
            objOLI.Package_Item_Quantity__c = 2;
            objOLI.Package_ID_External__c = iterator.Package_ID_External__c;
            objOLI.Directory__c = iterator.Directory__c;
            objOLI.Directory_Edition__c = iterator.Directory_Edition__c;
            objOLI.UnitPrice__c = iterator.UnitPrice;
            objOLI.Opportunity_line_Item_id__c = iterator.Id;
            objOLI.Media_Type__c = CommonMessages.oliPrintProductType;
            objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
            objOLI.Directory_Section__c = iterator.Directory_Section__c;
            objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
            objOLI.Product_Inventory_Tracking_Group__c = 'YP Leader Ad';
            lstOrderLI.add(objOLI);
        }
        insert lstOrderLI;
        Order_Line_Items__c firstOLI = lstOrderLI[0];
        firstOLI.Talus_Go_Live_Date__c = system.today();
        firstOLI.Directory_Edition__c = objDirEdList[1].Id;
        update firstOLI;
        Order_Line_Items__c secondOLI = lstOrderLI[1];
        secondOLI.Talus_Go_Live_Date__c = system.today();
        update secondOLI;
        
        delete secondOLI;
        Test.stopTest();
    }
    
    static testmethod void OLItestDigital() {
        Test.startTest();
        lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Product_Type__c = 'SEM';
            newProduct.Inventory_Tracking_Group__c = 'Spine';
            newProduct.RGU__c = 'SEM';
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        TestMethodsUtility.createDimension2(lstProduct[0]);
        
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        
        //added by Mythili...
       /* list<PricebookEntry> lstPBE=new list<PricebookEntry>();
        for(Product2 iterator:lstProduct){
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstPBE.add(pbe);
        }
        insert lstPBE;*/
        list<Canvass__c> lstCanvass = [Select Id, Name, Canvass_Code__c from Canvass__c where Id =:newAccount.Primary_Canvass__c];
        system.assertNotEquals(lstCanvass, null);
        TestMethodsUtility.createDimension1(lstCanvass[0].Name, lstCanvass[0].Canvass_Code__c);
        
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;
        insert newOpportunity;
        
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
            OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
            objOLI.PricebookEntryId = iterator.Id;
            objOLI.Billing_Duration__c = 12;
            objOLI.Full_Rate__c = 30.00;
            objOLI.UnitPrice = 30.00;
            objOLI.Package_ID__c = '45678';
            objOLI.Package_ID_External__c = TestMethodsUtility.generateRandomString(5);
            objOLI.Billing_Partner__c = objOLI.Id;
            objOLI.OpportunityId = newOpportunity.Id;
            objOLI.Directory_Heading__c = objDH.Id;
            lstOLI.add(objOLI);
            mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        for(OpportunityLineItem iterator : lstOLI) {
            Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
            objOLI.Package_ID__c = iterator.Package_ID__c;
            objOLI.Package_Item_Quantity__c = 2;
            objOLI.Package_ID_External__c = iterator.Package_ID_External__c;
            objOLI.UnitPrice__c = iterator.UnitPrice;
            objOLI.Opportunity_line_Item_id__c = iterator.Id;
            objOLI.Media_Type__c = 'Digital';
            objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
            objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
            objOLI.Product_Inventory_Tracking_Group__c = 'Spine';
            objOLI.Payment_Method__c = newOpportunity.Payment_Method__c;
            objOLI.Quantity__c = 1;
            lstOrderLI.add(objOLI);
        }
        insert lstOrderLI;
        system.assertEquals(lstOrderLI[0].Quantity__c, 1);
        system.assertEquals(lstOrderLI[1].Quantity__c, 1);
        TestMethodsUtility.createDimension3(lstOrderLI[0]);
        for(Order_Line_Items__c iterator : lstOrderLI) {
            iterator.Talus_Go_Live_Date__c = system.today();
        }
        update lstOrderLI;
        Test.stopTest();
    }
    
    static testmethod void OLItestSingleDigital() {
        Test.startTest();
        lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Product_Type__c = 'SEM';
            newProduct.Inventory_Tracking_Group__c = 'Spine';
            newProduct.RGU__c = 'SEM';
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        TestMethodsUtility.createDimension2(lstProduct[0]);
        
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        //added by Mythili...
        /*list<PricebookEntry> lstPBE=new list<PricebookEntry>();
        for(Product2 iterator:lstProduct){
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstPBE.add(pbe);
        }
        insert lstPBE;*/
        list<Canvass__c> lstCanvass = [Select Id, Name, Canvass_Code__c from Canvass__c where Id =:newAccount.Primary_Canvass__c];
        system.assertNotEquals(lstCanvass, null);
        TestMethodsUtility.createDimension1(lstCanvass[0].Name, lstCanvass[0].Canvass_Code__c);
        
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;
        insert newOpportunity;
        
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
            OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
            objOLI.PricebookEntryId = iterator.Id;
            objOLI.Billing_Duration__c = 12;
            objOLI.Full_Rate__c = 30.00;
            objOLI.UnitPrice = 30.00;
            objOLI.Package_ID__c = TestMethodsUtility.generateRandomString(5);
            objOLI.Package_ID_External__c = TestMethodsUtility.generateRandomString(5);
            objOLI.Billing_Partner__c = objOLI.Id;
            objOLI.OpportunityId = newOpportunity.Id;
            objOLI.Directory_Heading__c = objDH.Id;
            lstOLI.add(objOLI);
            mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        for(OpportunityLineItem iterator : lstOLI) {
            Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
            objOLI.Package_ID__c = iterator.Package_ID__c;
            objOLI.Package_Item_Quantity__c = 1;
            objOLI.Package_ID_External__c = iterator.Package_ID_External__c;
            objOLI.UnitPrice__c = iterator.UnitPrice;
            objOLI.Opportunity_line_Item_id__c = iterator.Id;
            objOLI.Media_Type__c = 'Digital';
            objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
            objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
            objOLI.Product_Inventory_Tracking_Group__c = 'Spine';
            objOLI.Payment_Method__c = newOpportunity.Payment_Method__c;
            objOLI.Quantity__c = 1;
            lstOrderLI.add(objOLI);
        }
        insert lstOrderLI;
        system.assertEquals(lstOrderLI[0].Quantity__c, 1);
        system.assertEquals(lstOrderLI[1].Quantity__c, 1);
        TestMethodsUtility.createDimension3(lstOrderLI[0]);
        for(Order_Line_Items__c iterator : lstOrderLI) {
            iterator.Talus_Go_Live_Date__c = system.today();
        }
        update lstOrderLI;
        Test.stopTest();
    }
    
    static testmethod void OLISuppressing() {        
        lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
       /* Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        Date myDate4 = Date.newInstance(1962, 3, 17);
        objDirEd.Pub_Date__c=myDate4;
        //objDirEd.Edition_code__c='NA';
        insert objDirEd;
        
        /*Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        Date myDate5 = Date.newInstance(1972, 3, 17);
        objDirEd1.Pub_Date__c=myDate5;
        insert objDirEd1;*/
        
        Listing__c objMListing = TestMethodsUtility.generateListing('Main Listing');
        objMListing.Account__c = newAccount.Id;
        objMListing.Primary_Canvass__c = newAccount.Primary_Canvass__c;
        insert objMListing;
        
        Listing__c objListing = TestMethodsUtility.generateListing('Additional Listing');
        objListing.Account__c = newAccount.Id;
        objListing.Primary_Canvass__c = newAccount.Primary_Canvass__c;
        objListing.Main_Listing__c = objMListing.id;
        insert objListing;
        
       // list<Directory_Listing__c> lstDL = new list<Directory_Listing__c>();
        Directory_Listing__c objDL = TestMethodsUtility.generateDirectoryListing('Caption Member');
        objDL.Listing__c = objListing.Id;
        //Directory_Listing__c objDL1 = TestMethodsUtility.generateDirectoryListing('Caption Member');
        //objDL1.Listing__c = objListing.Id;
        
        //lstDL.add(objDL);
        //lstDL.add(objDL1);
        //insert lstDL;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
            newProduct.Inventory_Tracking_Group__c = 'Spine';
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        //added by Mythili...
       /* list<PricebookEntry> lstPBE=new list<PricebookEntry>();
        for(Product2 iterator:lstProduct){
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstPBE.add(pbe);
        }
        insert lstPBE;*/
        
        for(Product2 iterator : lstProduct) {
            Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
            objDPM.Product2__c = iterator.Id;
            lstDPM.add(objDPM);
        }
        insert lstDPM;
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        insert newOpportunity;
        
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
            OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
            objOLI.PricebookEntryId = iterator.Id;
            objOLI.Billing_Duration__c = 12;
            objOLI.Directory__c = objDir.Id;
            objOLI.Directory_Edition__c = objDirEd.Id;
            objOLI.Full_Rate__c = 30.00;
            objOLI.UnitPrice = 30.00;
            objOLI.Package_ID__c = '123456';
            objOLI.Package_ID_External__c = TestMethodsUtility.generateRandomString(5);
            objOLI.Billing_Partner__c = objOLI.Id;
            objOLI.OpportunityId = newOpportunity.Id;
            objOLI.Directory_Heading__c = objDH.Id;
            objOLI.Directory_Section__c = objDS.Id;
            lstOLI.add(objOLI);
            mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Test.startTest();
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        for(OpportunityLineItem iterator : lstOLI) {
            Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
            objOLI.Package_ID__c = iterator.Package_ID__c;
            objOLI.Package_Item_Quantity__c = 2;
            objOLI.Package_ID_External__c = iterator.Package_ID_External__c;
            objOLI.Directory__c = iterator.Directory__c;
            objOLI.Directory_Edition__c = iterator.Directory_Edition__c;
            objOLI.UnitPrice__c = iterator.UnitPrice;
            objOLI.Opportunity_line_Item_id__c = iterator.Id;
            objOLI.Media_Type__c = CommonMessages.oliPrintProductType;
            objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
            objOLI.Directory_Section__c = iterator.Directory_Section__c;
            objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
            objOLI.Product_Inventory_Tracking_Group__c = 'Spine';
            lstOrderLI.add(objOLI);
        }
        insert lstOrderLI;
        
        Order_Line_Items__c firstOLI = lstOrderLI[0];
        objDL.Suppressed_By_OLI__c = firstOLI.Id;
        objDL.Directory_Section__c = objDS.Id;
        insert objDL;
        
        firstOLI.Talus_Cancel_Date__c = system.today();
        firstOLI.Status__c = 'Cancelation Requested';
         firstOLI.IsCanceled__c = true;
        firstOLI.Media_Type__c.equals(CommonMessages.oliPrintProductType);
        update firstOLI;
        
        //Order_Line_Items__c fstOLI = lstOrderLI[0];
        //fstOLI.IsCanceled__c = true;
        //fstOLI.Media_Type__c.equals(CommonMessages.oliPrintProductType);
       // update fstOLI;
        delete firstOLI;
        Test.stopTest();
    }
}