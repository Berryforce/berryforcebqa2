/*
This class is used only for Data migration.
If you do any changes to this class, check and do the changes in LOBReportGenerationBatch class also.
*/

global class BillingAmountPopulationOnDMIGRPrintOLI Implements Database.Batchable <sObject> {    
    global Database.queryLocator start(Database.BatchableContext bc) {
        set<String> setDEStatus = new set<String>{'BOTS'};
        String SOQL = 'SELECT Id FROM Directory_Edition__c where Book_Status__c IN:setDEStatus';
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<Directory_Edition__c> listDirEd) {
        Map<Id, Order_Line_Items__c> mapOLIForBillAmt = new Map<Id, Order_Line_Items__c>();
        Set<Id> acctIds = new Set<Id>();
        List<Order_Line_Items__c> listOLI = new List<Order_Line_Items__c>();
        List<List_of_Business__c> listLOB = new List<List_of_Business__c>();
        Map<Id, Map<String, Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>>> mapAccBPBFPhoneLisNamOLI = new Map<Id, Map<String, Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>>>();     
        RecordType  objRT = CommonMethods.getRecordTypeDetailsByName('Order_Line_Items__c','National Order Line Item');               
        listOLI = [SELECT (Select Id from Sales_Invoice_Line_Items__r
                WHERE Offset__c=true OR Is_Manually_Created__c=true OR c2g__Invoice__r.Telco_Reversal__c=true),Account__r.Primary_Contact__r.Phone, Listing__r.Phone__c, Account__r.X3l_External_Id__c, Account__r.Parent_3L_External_Id__c,Id,
                Digital_Product_Requirement__r.business_phone_number_office__c,UnitPrice__c, Billing_Contact__r.Phone,Account__c, Account__r.Account_Number__c,
                Account__r.Parent.Account_Number__c,Account__r.Phone,Account__r.Name,Telco__r.Name,Listing_Name__c,Listing__r.Name,Directory__c,
                Billing_Partner__c,Billing_Frequency__c,RecordtypeId,Order_Line_Total__c, Account__r.Parent.Name, Canvass__c, Directory_Code__c,
                Edition_Code__c, Media_Type__c, Telco_Invoice_Date__c, Service_End_Date__c, Service_Start_Date__c FROM Order_Line_Items__c WHERE 
                RecordtypeId !=: objRT.Id AND isCanceled__c = false AND Product2__r.Media_Type__c='Print' 
                AND Directory_Edition__c =: listDirEd AND Billing_Partner__c != null ORDER BY Billing_Partner__c, Account__r.Name];
        if(listOLI.size() > 0) {
            for(Order_Line_Items__c OLI : listOLI) {                
                String AdvPhone = OLI.Digital_Product_Requirement__r.business_phone_number_office__c;
                String ListingName = String.valueof(OLI.Listing__r.Name + OLI.Listing__r.Phone__c).replaceAll('\\s+','');
                acctIds.add(OLI.Account__c);
                if(!mapAccBPBFPhoneLisNamOLI.containsKey(OLI.Account__c)) {
                    mapAccBPBFPhoneLisNamOLI.put(OLI.Account__c, new Map<String, Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>>());
                } 
                if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).containsKey(OLI.Billing_Partner__c)) {
                    mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).put(OLI.Billing_Partner__c, new Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>());
                }
                if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).containsKey(OLI.Billing_Frequency__c)) {
                    mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).put(OLI.Billing_Frequency__c, new Map<String, Map<String, List<Order_Line_Items__c>>>());
                }
                if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).containsKey(AdvPhone)) {
                    mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).put(AdvPhone, new Map<String, List<Order_Line_Items__c>>());
                }
                if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).get(AdvPhone).containsKey(ListingName)) {
                    mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).get(AdvPhone).put(ListingName, new List<Order_Line_Items__c>());
                }
                mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).get(AdvPhone).get(ListingName).add(OLI);
            }            
            
            for(Id acctId : mapAccBPBFPhoneLisNamOLI.keySet()) {
                for(String BP : mapAccBPBFPhoneLisNamOLI.get(acctId).keySet()) {
                    for(String BF : mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).keySet()) {
                        for(String advPhone : mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).get(BF).keySet()) {
                            for(String listName : mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).get(BF).get(advPhone).keySet()) {
                                Double billAmount = 0;
                                List<Order_Line_Items__c> listTempOLI = mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).get(BF).get(advPhone).get(listName);
                                if(listTempOLI != null) {
                                    Order_Line_Items__c tempOLI = listTempOLI.get(0);
                                    String OLIIds = '';
                                    set<Id> setOLIIds = new set<Id>();
                                    for(Order_Line_Items__c OLI : listTempOLI) {
                                        billAmount += OLI.UnitPrice__c;
                                        OLIIds += OLI.Id + ';';
                                        setOLIIds.add(OLI.Id);
                                    }   
                                    if(setOLIIds.size() <= Integer.valueOf(system.label.OLI_Count_on_LOB)) {
                                        for(Order_Line_Items__c OLI : listTempOLI) {
                                            mapOLIForBillAmt.put(OLI.Id, new Order_Line_Items__c(Id = OLI.Id, OLI_Billing_Amount__c = billAmount));
                                        } 
                                    } else {                                
	                                    List_of_Business__c lob = new List_of_Business__c();   
	                                    lob = LOBReportGenerationBatch.newLOB(tempOLI, billAmount, OLIIds.removeEnd(';'), setOLIIds.size());          
                                    	listLOB.add(lob);  
                                    }
                                }
                            }
                        }
                    }
                }
            }    
        }       
        if(listLOB.size() > 0) {
            insert listLOB;
        }    
        if(mapOLIForBillAmt.size() > 0) {
            update mapOLIForBillAmt.values();
        }
    }

    global void finish(Database.BatchableContext bc) {
        BillingAmountPopulationOnOLIFromLOBBatch obj = new BillingAmountPopulationOnOLIFromLOBBatch();
        Database.executeBatch(obj, Integer.valueOf(system.label.OLI_Bill_Amount_Batch_Size));
    } 
}