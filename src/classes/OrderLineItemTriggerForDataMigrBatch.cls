global class OrderLineItemTriggerForDataMigrBatch implements Database.Batchable<sObject>{

    global string filtercondition;
    global set<String> setLeads;
    
    /*global OrderLineItemTriggerForDataMigrBatch(){
    }
*/    
    global OrderLineItemTriggerForDataMigrBatch(String condition, set<String> setLead){
        filtercondition = condition;
        setLeads = setLead;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
         String query = 'Select o.Product_Code__c,o.Directory_Edition__r.Name,o.Directory_Edition__r.Edition_Code__c,o.Directory_Edition__r.Year__c,o.OL_Print_Specialty_Product_Type__c,o.zero_Cancel_Fee_Requested__c, o.strSuppressionCheck__c, o.strSeniorityDateCombo__c, o.strOLICombo__c, o.line_color_id__c, o.isCanceled__c,'+
                        'o.checked__c, o.Zero_Cancel_Fee_Approved__c, o.Website_URL__c, o.Vendor_Product_Key__c,'+
                        'o.User_who_initiated_the_transfer__c, o.UnitPrice__c, o.UDAC__c, o.UDAC_Group__c, o.Type__c,'+
                        'o.Transferred_from__c, o.Transferred_To__c, o.Transaction_ID__c, o.Trans__c, o.Trans_Version__c,'+
                        'o.TransIDLineNo__c, o.Trademark_Finding_Line_OLI__c, o.Trademark_Display_Text__c, o.Total_Prorated_Days_for_contract__c, o.Total_Prorate__c, o.Total_Number_Free__c, o.Total_Billing_Amount_Transfered__c, o.To__c, o.To_P__c, o.Timestamp_Update__c, o.Telco__c,'+ 
                        'o.Telco_Invoice_Date__c, o.Tear_Page_PDF_Path__c, o.Talus_Subscription_ID__c, o.Talus_OrderLineItem__c, o.Talus_Go_Live_Date__c, o.Talus_Fulfillment_Date__c,'+ 
                        'o.Talus_DFF_Id__c, o.Talus_Cancel_Date__c, o.SystemModstamp, o.Successful_Payments__c, o.Subscription_ID__c, o.Status__c, o.Statement_Suppression__c, o.Sort_As__c, o.Sort_As_FORMULA__c, o.Sort_As_Check_Test__c, o.Service_Start_Date__c, o.Service_End_Date__c, o.Sent_to_fulfillment_on__c, o.Seniority_Date__c, o.Selected_Cancel__c, o.Selected_Cancel_Date__c, o.Section_Page_Type__c, o.Scoped_Suppression_Processing_Complete__c, o.Scoped_Caption_Header__c, o.Scope__c,'+
                        'o.SPINS__c, o.Requires_Scoped_Suppression_Processing__c, o.RecordTypeId, o.Reason_for_Transfer__c, o.ReadyForProcess__c, o.RCF_Tracked__c, o.Quote_signing_method__c, o.Quote_Signed_Date__c, o.Quantity__c, o.Publication_Date__c, o.Publication_Company__c, o.Publication_Code__c, o.Proxy_URL__c,'+ 
                        'o.Proration_Billing_Period__c, o.Prorate_Stored_Value__c, o.Prorate_Credit__c, o.Prorate_Credit_Days__c, o.Product_Type__c, o.Product_Is_IBUN_Bundle_Product__c, o.Product_Inventory_Tracking_Group__c, o.ProductCode__c, o.Product2__c, o.Print_Product_Type__c, o.Print_First_Bill_Date__c, o.Pricing_Program__c, o.Previous_Start_Date__c, o.Previous_End_Date__c, o.Previous_Directory_Edition__c, o.Pending_Disconnect__c, o.Payments_Remaining__c, o.Payment_Method__c, o.Payment_Duration__c, o.Parent_Line_Item__c, o.Parent_ID_of_Addon__c, o.Parent_ID__c,'+
                        'o.Page_Number__c, o.Package_Name__c, o.Package_Item_Quantity__c, o.Package_ID__c, o.Package_ID_External__c, o.P4P_Tracking_Number_Status__c, o.P4P_Tracking_Number_ID__c, o.P4P_Price_Per_Click_Lead__c, o.P4P_Current_Months_Clicks_Leads__c, o.P4P_Current_Billing_Clicks_Leads__c, o.P4P_Billing__c, o.Original_Order_Line_Item__c, o.OriginalBillingDate__c,'+ 
                        'o.Order__c, o.Order_Line_Total__c, o.Order_Line_Item_Locality__c, o.Order_Group__c, o.Order_Billing_Date_Changed__c, o.Order_Anniversary_Start_Date__c, o.Opportunity_line_Item_id__c, o.Opportunity__c, o.Opportunity_Close_Date__c, o.Number_of_Months_Transferred__c, o.Next_Billing_Date__c, o.National_Staging_Line_Item__c, o.Name, o.NAT__c, o.NAT_Client_ID__c, o.N2L__c, o.Mobile_URL__c,'+
                        'o.Migration_Directory_Heading4__c, o.Migration_Directory_Heading3__c, o.Migration_Directory_Heading2__c, o.Migration_Directory_Heading1__c, o.Media_Type__c, o.Locality__c, o.Listing__c, o.Listing_Name__c, o.ListPrice__c, o.Linvio_Payment_Method_Id__c, o.Line_Status__c, o.Line_Number__c, o.Last_successful_settlement__c, o.Last_Invoice__c, o.Last_Billing_Date__c, o.LastViewedDate, o.LastReferencedDate, o.LastModifiedDate, o.LastModifiedById, o.LastActivityDate, o.Landing_Page_URL__c, o.L2N__c, o.Is_Trademark_Finding_Line__c, o.Is_P4P__c,'+
                        'o.Is_Handled__c, o.Is_Child__c, o.Is_Caption__c, o.Is_Billing_Cycle__c, o.IsDeleted, o.Indent_Order__c, o.Id, o.Geo_Type__c, o.Geo_Code__c, o.Fulfillment_Status__c, o.FulfillmentDate__c, o.Fulfilled_on__c, o.From__c, o.From_C__c, o.Facebook_URL__c, o.Facebook_Admin__c, o.Expiration_Date__c, o.Evergreen_Payments__c, o.Effective_Date__c, o.Edition_Code__c, o.Domain_Email_Addresses__c, o.Distribution_Area__c,'+ 
                        'o.Discount__c, o.Directory__c, o.Directory_Section__c, o.Directory_Name__c, o.Directory_Heading__c, o.Directory_Edition__c, o.Directory_Edition_Renewal_Date__c, o.Directory_Edition_Name__c, o.Directory_Edition_Book_Status__c, o.Directory_Code__c, o.Digital_Product_Requirement__c, o.Destination_Phone_Number__c, o.Description__c, o.Days_from_Submit_to_Fulfilmment__c, o.Days_From_Creation_to_Fulfillment__c, o.Days_For_Task_Notification__c, o.Days_B_W__c, o.Date_of_Transfer__c, o.Date__c, o.DM_isTriggerExecuted__c, o.DAT__c, o.Cutomer_Cancel_Date__c, o.Custom_Domain_Name__c, o.Current_Rate__c,'+
                        'o.Current_Month_for_P4P__c, o.Current_Invoice__c, o.Current_Daily_Prorate__c, o.Current_Billing_Period_Days__c, o.Current_Billing_Period_Days2__c, o.CreatedDate, o.CreatedById, o.Core_Opportunity_Line_ID__c, o.Contract_Start_Date__c,  o.Contract_End_Date__c, o.Continious_Billing__c, o.Comments__c, o.Client_Number__c, o.Category__c, o.Case__c, o.Caption_Or_Extra_Line_Text__c, o.Canvass__c, o.Cancellation__c, o.Cancelation_Fee__c, o.Cancel_Fee_Sales_Invoice_Created__c, o.Call_Tracking_Phone_Number__c, o.CMR_Number__c, o.CMR_Number_Client_Number__c, o.CMR_Name__c,'+
                        'o.Billing_Transfer_Type__c, o.Billing_Start_Date__c, o.Billing_Partner__c, o.Billing_Partner_Transferred__c, o.Billing_Partner_Change__c, o.Billing_Partner_Account__c, o.Billing_Frequency__c, o.Billing_Frequency_Change__c,'+ 
                        'o.Billing_Frequency_Change_Date__c, o.Billing_End_Date__c, o.Billing_Contact__c, o.Billing_Close_Date__c, o.Billing_Change_Prorate_Credit__c, o.BillingTransfered__c, o.BillingTransferedInfo__c, o.BillingChangesPayment__c, o.BillingChangeProrateCreditDays__c, o.BillingChangeNoofDays__c, o.BAS__c, o.Anchor_Listing_Caption_Header__c, o.Anchor_Caption_Header_Record_ID__c,'+
                        'o.Advertising_Data__c, o.Advertised_Phone_Number__c, o.Addon_Type__c, o.Action__c,o.strOppliCombo__c, o.Action_Code__c, o.Account__c,o.OLI_Do_Not_Create_Pagination_Record__c From Order_Line_Items__c o '+filtercondition;
        return Database.getQueryLocator(query);
    }  
    
    global void execute(Database.BatchableContext bc, List<Order_Line_Items__c> listOLI) {
        Savepoint sp = Database.setSavepoint();
        try{
            OrderLineItemHandlerController.splitProcessAfterInsert(listOLI);
            for(Order_Line_Items__c oli:listOLI){
                oli.DM_isTriggerExecuted__c=true;
                if(oli.strOppliCombo__c!=null && setLeads.contains(oli.strOppliCombo__c)){
                    oli.Order_Line_Item_Locality__c = 'Local';
                }
                else{
                    oli.Order_Line_Item_Locality__c = 'Foreign';
                }
            }
            update listOLI;
        }catch(Exception e){
            System.debug('The exception is'+e);
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch update of migrated Opportunity');
        } 
        /*Savepoint sp;
        try{
            sp = Database.setSavepoint();
            system.debug('**************'+listOLI);
            if(listOLI!=null){
               
                OrderLineItemHandlerController.createDirectoryPagination(listOLI);
                OrderLineItemHandlerController.prodInventoryCreationNew(listOLI);
                //for Internet bundle appearences
                InternetBundleAppearances.IncreaseDecreaseIBAppearancesNew(listOLI, null);
                OrderLineItemHandlerController.increaseLeaderAdSoldAppearCounts(listOLI);
              
                for(Order_Line_Items__c oli:listOLI){
                    oli.DM_isTriggerExecuted__c=true;
                    if(oli.strOppliCombo__c!=null && setLeads.contains(oli.strOppliCombo__c)){
                        oli.Order_Line_Item_Locality__c = 'Local';
                    }
                    else{
                        oli.Order_Line_Item_Locality__c = 'Foreign';
                    }
                }
                update listOLI;
            }
        }catch(Exception e){
            System.debug('The exception is'+e);
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch update of migrated Opportunity');
        }*/
        
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}