/******************************************************
Apex Class to Schedule DeleteFulfillmentBatch Batch Job
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 08/14/2015
$Id$
*******************************************************/
global class ScheduleDeleteFulfillmentBatch implements Schedulable {
    //public static String sched = '0 00 23 * * ?'; //batch will run every night

    //global static String scheduleBatch() {
          //ScheduleYpcAddonPatch SC = new ScheduleYpcAddonPatch();
          //return System.schedule('Ypc Addon Patch Job', sched, SC);
    //}

    global void execute(SchedulableContext sc) {

	    DeleteFulfillmentBatch obj = new DeleteFulfillmentBatch();
	    ID batchprocessid = database.executebatch(obj, 10);

    }
}