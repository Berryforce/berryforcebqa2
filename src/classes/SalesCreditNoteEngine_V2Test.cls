@isTest(seeAllData=true)
public with sharing class SalesCreditNoteEngine_V2Test {
    static testMethod void SCNTest() {
        
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        //Account acct = TestMethodsUtility.createAccount('customer');
        // Account CMRacct = TestMethodsUtility.createAccount('cmr');
        
        
        Account acct = TestMethodsUtility.generateAccount('telco');
        insert acct;
        Account customeraccount = TestMethodsUtility.generateAccount('customer');
        customeraccount.Telco_Partner__c =acct.id;
        customeraccount.Client_Tier__c='Platinum';
        insert customeraccount;
        
        Account CMRacct = TestMethodsUtility.createAccount('cmr');
        
        Case cas = TestMethodsUtility.generateCase('CS Claim');
        cas.Contracted_Total_Value__c = 90;
        cas.AccountId = customeraccount.Id;
        cas.Total_BCC__c = 90;
        cas.BCC_Duration__c='2';
        cas.PAN1__c ='123456';
        insert cas;
        Contact cnt = TestMethodsUtility.createContact(customeraccount.Id);
        Order__c ord = TestMethodsUtility.createOrder(customeraccount.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(customeraccount, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(customeraccount, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 prod = TestMethodsUtility.createproduct();
        Directory__c dir = TestMethodsUtility.createDirectory();
        
        c2g__codaDimension1__c  dimension1 = TestMethodsUtility.createDimension1(dir);
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(customeraccount, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c = system.today();
        oln.Print_First_Bill_Date__c = system.today();
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Product2__c = prod.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Billing_Partner__c = CommonMessages.BerryForDimension;
        oln.Payment_Method__c = CommonMessages.ACHPaymentMethod;
        oln.Billing_Frequency__c = CommonMessages.singlePayment;
        oln.Successful_Payments__c = 9;
        oln.CMR_Name__c = CMRacct.Id;
        oln.Payment_Duration__c = 1;
        oln.Media_Type__c = CommonMessages.oliPrintProductType;
        oln.Directory_Section__c = dirSec.Id;
        insert oln;      
        String directory_code = [select Directory_Code__c FROM Order_Line_Items__c where id =:oln.id limit 1].Directory_Code__c;
        
        System.debug('Print #### Directory ## Code 1 is :' +directory_code );
        
        Order_Line_Item_Discount__c OLID = TestMethodsUtility.generateOrderLineItemDiscount(og, oln);
        OLID.total_Credit_amount__c = '90';
        OLID.Case__c = cas.Id;
        OLID.Number_of_Billing_Periods_to_discount__c = 9;
        OLID.Issue_as_Single_Credit__c = true;
        OLID.Credit_Issued_To1__c='Berry';
        insert OLID;
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(cas);
        SalesCreditNoteEngine_V2 obj = new SalesCreditNoteEngine_V2(controller);
        obj.onLoad();
        obj.roundValues(34);
       // obj.dateCalculation(oln);
        obj.checkfornullnegative(-1);
        obj.checkfornullnegative(1);
        obj.backToCase();
        Test.stopTest();
    }
    
    static testMethod void SCNTestV1() {
        
        Canvass__c canvass = TestMethodsUtility.createCanvass();
      //  Account acct = TestMethodsUtility.createAccount('customer');
       // Account CMRacct = TestMethodsUtility.createAccount('cmr');
         
        Account acct = TestMethodsUtility.generateAccount('telco');
        insert acct;
        Account customeraccount = TestMethodsUtility.generateAccount('customer');
        customeraccount.Telco_Partner__c =acct.id;
        insert customeraccount;
        
        Account CMRacct = TestMethodsUtility.createAccount('cmr');
        
        Id CaseRecordTypeId=CommonMethods.getRedordTypeIdByName(CommonMessages.NationalCase, CommonMessages.caseObjectName);
        Case cas = TestMethodsUtility.generateCase('CS Claim');
        cas.Contracted_Total_Value__c = 90;
        cas.AccountId = customeraccount.Id;
        cas.Total_BCC__c = 90;
        cas.BCC_Duration__c='2';
        cas.PAN1__c ='123456';
        cas.RecordTypeId=CaseRecordTypeId;
        insert cas;
        Contact cnt = TestMethodsUtility.createContact(customeraccount.Id);
        Order__c ord = TestMethodsUtility.createOrder(customeraccount.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(customeraccount, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(customeraccount, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 prod = TestMethodsUtility.createproduct();
        Directory__c dir = TestMethodsUtility.createDirectory();
        c2g__codaDimension1__c  dimension1 = TestMethodsUtility.createDimension1(dir);
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(customeraccount, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c = system.today();
        oln.Print_First_Bill_Date__c = system.today();
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Product2__c = prod.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Billing_Partner__c = CommonMessages.BerryForDimension;
        oln.Payment_Method__c = CommonMessages.ACHPaymentMethod;
        oln.Billing_Frequency__c = CommonMessages.monthlyPayment;
        oln.Successful_Payments__c = 9;
        oln.CMR_Name__c = CMRacct.Id;
        oln.Payment_Duration__c = 1;
        oln.Media_Type__c = CommonMessages.oliPrintProductType;
        oln.Directory_Section__c = dirSec.Id;
        oln.P4P_Billing__c =true;
        insert oln; 

        String directory_code = [select Directory_Code__c FROM Order_Line_Items__c where id =:oln.id limit 1].Directory_Code__c;
        
        System.debug('Print #### Directory ## Code 2 is :' +directory_code );
                
        Order_Line_Item_Discount__c OLID = TestMethodsUtility.generateOrderLineItemDiscount(og, oln);
        OLID.total_Credit_amount__c = '90';
        OLID.Case__c = cas.Id;
        OLID.Number_of_Billing_Periods_to_discount__c = 9;
        OLID.Issue_as_Single_Credit__c = false;
        OLID.Credit_Issued_To1__c='Berry';
        insert OLID;
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(cas);
        SalesCreditNoteEngine_V2 obj = new SalesCreditNoteEngine_V2(controller);
        obj.onLoad();
        obj.roundValues(34);
       // obj.dateCalculation(oln);
        Test.stopTest();
    }
    
    /*
    static testMethod void SCNTestV2() {
        
        Canvass__c canvass = TestMethodsUtility.createCanvass();
       // Account acct = TestMethodsUtility.createAccount('customer');
       // Account CMRacct = TestMethodsUtility.createAccount('cmr');
       
        Account acct = TestMethodsUtility.generateAccount('telco');
        insert acct;
        Account customeraccount = TestMethodsUtility.generateAccount('customer');
        customeraccount.Telco_Partner__c =acct.id;
        insert customeraccount;
        
        Account CMRacct = TestMethodsUtility.createAccount('cmr');
        
        Case cas = TestMethodsUtility.generateCase('CS Claim');
        cas.Contracted_Total_Value__c = 90;
        cas.AccountId = customeraccount.Id;
        cas.Total_BCC__c = 90;
        cas.BCC_Duration__c='2';
        cas.PAN1__c ='123456';
        insert cas;
        Contact cnt = TestMethodsUtility.createContact(customeraccount.Id);
        Order__c ord = TestMethodsUtility.createOrder(customeraccount.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(customeraccount, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(customeraccount, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 prod = TestMethodsUtility.createproduct();
        Directory__c dir = TestMethodsUtility.createDirectory();
        c2g__codaDimension1__c  dimension1 = TestMethodsUtility.createDimension1(dir);
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(customeraccount, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c = system.today();
        oln.Print_First_Bill_Date__c = system.today();
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Product2__c = prod.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Billing_Partner__c = CommonMessages.BerryForDimension;
        oln.Payment_Method__c = CommonMessages.ACHPaymentMethod;
        oln.Billing_Frequency__c = CommonMessages.singlePayment;
        oln.Successful_Payments__c = 9;
        oln.CMR_Name__c = CMRacct.Id;
        oln.Payment_Duration__c = 1;
        oln.Media_Type__c = CommonMessages.oliPrintProductType;
        oln.Directory_Section__c = dirSec.Id;
        insert oln;      
        
         String directory_code = [select Directory_Code__c FROM Order_Line_Items__c where id =:oln.id limit 1].Directory_Code__c ; 
        
        System.debug('Print #### Directory ## Code 3 is :' +directory_code );
        
        Order_Line_Item_Discount__c OLID = TestMethodsUtility.generateOrderLineItemDiscount(og, oln);
        OLID.total_Credit_amount__c = '90';
        OLID.Case__c = cas.Id;
        OLID.Number_of_Billing_Periods_to_discount__c = 9;
        OLID.Issue_as_Single_Credit__c = false;
        OLID.Credit_Issued_To1__c='Berry';
        insert OLID;
        
        Order_Line_Item_Discount__c OLID1 = new Order_Line_Item_Discount__c(Issue_as_Single_Credit__c=true,total_Credit_amount__c='100');
        insert OLID1;
        
        Test.startTest();
        
        
        
        ApexPages.StandardController controller = new ApexPages.StandardController(cas);
        SalesCreditNoteEngine_V2 obj = new SalesCreditNoteEngine_V2(controller);
        obj.onLoad();
        obj.roundValues(34);
       // obj.dateCalculation(oln);
        Test.stopTest();
    }
    
    static testMethod void SCNTestV3() {
        
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.generateAccount('telco');
        insert acct;
        Account customeraccount = TestMethodsUtility.generateAccount('customer');
        customeraccount.Telco_Partner__c =acct.id;
        customeraccount.Client_Tier__c='Platinum';
        customeraccount.CMR_Number__c = '0114';
        insert customeraccount;
        
        Account CMRacct = TestMethodsUtility.createAccount('cmr');
        
        Case cas = TestMethodsUtility.generateCase('National Claim');
        cas.Contracted_Total_Value__c = 90;
        cas.AccountId = customeraccount.Id;
        cas.Total_BCC__c = 90;
        cas.BCC_Duration__c='2';
        cas.PAN1__c ='123456';
        insert cas;
        Contact cnt = TestMethodsUtility.createContact(customeraccount.Id);
        Order__c ord = TestMethodsUtility.createOrder(customeraccount.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(customeraccount, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(customeraccount, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 prod = TestMethodsUtility.createproduct();
        Directory__c dir = TestMethodsUtility.createDirectory();
        
        c2g__codaDimension1__c  dimension1 = TestMethodsUtility.createDimension1(dir);
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(customeraccount, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c = system.today();
        oln.Print_First_Bill_Date__c = system.today();
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Product2__c = prod.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Billing_Partner__c = CommonMessages.BerryForDimension;
        oln.Payment_Method__c = CommonMessages.ACHPaymentMethod;
        oln.Billing_Frequency__c = CommonMessages.singlePayment;
        oln.Successful_Payments__c = 9;
        oln.CMR_Name__c = CMRacct.Id;
        oln.Payment_Duration__c = 1;
        oln.Media_Type__c = CommonMessages.oliPrintProductType;
        oln.Directory_Section__c = dirSec.Id;
        insert oln;      
        String directory_code = [select Directory_Code__c FROM Order_Line_Items__c where id =:oln.id limit 1].Directory_Code__c;
        
        System.debug('Print #### Directory ## Code 1 is :' +directory_code );
        
        Order_Line_Item_Discount__c OLID = TestMethodsUtility.generateOrderLineItemDiscount(og, oln);
        OLID.total_Credit_amount__c = '90';
        OLID.Case__c = cas.Id;
        OLID.Number_of_Billing_Periods_to_discount__c = 9;
        OLID.Issue_as_Single_Credit__c = true;
        OLID.Credit_Issued_To1__c='Berry';
        insert OLID;
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(cas);
        SalesCreditNoteEngine_V2 obj = new SalesCreditNoteEngine_V2(controller);
        obj.onLoad();
        obj.roundValues(34);
       // obj.dateCalculation(oln);
        obj.checkfornullnegative(-1);
        obj.checkfornullnegative(1);
        Test.stopTest();
    }
    
    static testMethod void SCNTestV4() {
        
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.generateAccount('telco');
        insert acct;
        Account customeraccount = TestMethodsUtility.generateAccount('customer');
        customeraccount.Telco_Partner__c =acct.id;
        customeraccount.Client_Tier__c='Platinum';
        insert customeraccount;
        
        Account CMRacct = TestMethodsUtility.createAccount('cmr');
        
        Case cas = TestMethodsUtility.generateCase('CS Claim');
        cas.Contracted_Total_Value__c = 90;
        cas.AccountId = customeraccount.Id;
        cas.Total_BCC__c = 90;
        cas.BCC_Duration__c='2';
        cas.PAN1__c ='123456';
        insert cas;
        Contact cnt = TestMethodsUtility.createContact(customeraccount.Id);
        Order__c ord = TestMethodsUtility.createOrder(customeraccount.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(customeraccount, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(customeraccount, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 prod = TestMethodsUtility.createproduct();
        Directory__c dir = TestMethodsUtility.createDirectory();
        
        c2g__codaDimension1__c  dimension1 = TestMethodsUtility.createDimension1(dir);
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(customeraccount, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c = system.today();
        oln.Print_First_Bill_Date__c = system.today();
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Product2__c = prod.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Billing_Partner__c = CommonMessages.BerryForDimension;
        oln.Payment_Method__c = CommonMessages.ACHPaymentMethod;
        oln.Billing_Frequency__c = CommonMessages.singlePayment;
        oln.Successful_Payments__c = 9;
        oln.CMR_Name__c = CMRacct.Id;
        oln.Payment_Duration__c = 1;
        oln.Media_Type__c = CommonMessages.oliPrintProductType;
        oln.Directory_Section__c = dirSec.Id;
        insert oln;      
        String directory_code = [select Directory_Code__c FROM Order_Line_Items__c where id =:oln.id limit 1].Directory_Code__c;
        
        System.debug('Print #### Directory ## Code 1 is :' +directory_code );
        
        Order_Line_Item_Discount__c OLID = TestMethodsUtility.generateOrderLineItemDiscount(og, oln);
        OLID.total_Credit_amount__c = '90';
        OLID.Case__c = cas.Id;
        OLID.Number_of_Billing_Periods_to_discount__c = 9;
        OLID.Issue_as_Single_Credit__c = true;
        OLID.Credit_Issued_To1__c = 'Telco';
        OLID.Telco__c = acct.Id;
        insert OLID;
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(cas);
        SalesCreditNoteEngine_V2 obj = new SalesCreditNoteEngine_V2(controller);
        obj.onLoad();
        obj.roundValues(34);
       // obj.dateCalculation(oln);
        obj.checkfornullnegative(-1);
        obj.checkfornullnegative(1);
        Test.stopTest();
    }
    
    static testMethod void SCNTestV5() {
        
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.generateAccount('telco');
        insert acct;
        Account customeraccount = TestMethodsUtility.generateAccount('customer');
        customeraccount.Telco_Partner__c =acct.id;
        customeraccount.Client_Tier__c='Platinum';
        insert customeraccount;
        Boolean flag = false;
        Account CMRacct = TestMethodsUtility.createAccount('cmr');
        
        Case cas = TestMethodsUtility.generateCase('CS Claim');
        cas.Contracted_Total_Value__c = 90;
        cas.AccountId = customeraccount.Id;
        cas.Total_BCC__c = 90;
        cas.BCC_Duration__c='2';
        cas.PAN1__c ='123456';
        insert cas;
        Contact cnt = TestMethodsUtility.createContact(customeraccount.Id);
        Order__c ord = TestMethodsUtility.createOrder(customeraccount.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(customeraccount, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(customeraccount, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 prod = TestMethodsUtility.createproduct();
        Directory__c dir = TestMethodsUtility.createDirectory();
        
        c2g__codaDimension1__c  dimension1 = TestMethodsUtility.createDimension1(dir);
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(customeraccount, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c = system.today();
        oln.Print_First_Bill_Date__c = system.today();
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Product2__c = prod.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Billing_Partner__c = CommonMessages.TelcoForDimension;
        oln.Payment_Method__c = CommonMessages.ACHPaymentMethod;
        oln.Billing_Frequency__c = CommonMessages.singlePayment;
        oln.Successful_Payments__c = 9;
        oln.CMR_Name__c = CMRacct.Id;
        oln.Payment_Duration__c = 1;
        oln.Media_Type__c = CommonMessages.oliPrintProductType;
        oln.Directory_Section__c = dirSec.Id;
        oln.Billing_Partner_Account__c = acct.Id;
        insert oln;      
        String directory_code = [select Directory_Code__c FROM Order_Line_Items__c where id =:oln.id limit 1].Directory_Code__c;
        
        System.debug('Print #### Directory ## Code 1 is :' +directory_code );
        
        Order_Line_Item_Discount__c OLID = TestMethodsUtility.generateOrderLineItemDiscount(og, oln);
        OLID.total_Credit_amount__c = '90';
        OLID.Case__c = cas.Id;
        OLID.Number_of_Billing_Periods_to_discount__c = 9;
        OLID.Issue_as_Single_Credit__c = true;
        OLID.Credit_Issued_To1__c = 'Telco';
        OLID.Telco__c = acct.Id;
        OLID.Total_Net_Commission__c = 43;
        insert OLID;
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(cas);
        SalesCreditNoteEngine_V2 obj = new SalesCreditNoteEngine_V2(controller);
        obj.onLoad();
        obj.roundValues(34);
       // obj.dateCalculation(oln);
        obj.checkfornullnegative(-1);
        obj.checkfornullnegative(1);
        Test.stopTest();
    }
    
    static testMethod void SCNTestV6() {
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo.get('Telco Partner').getRecordTypeId();
        
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = new Account(Name = 'Test Acc', Phone ='(666)666-6666', Primary_Canvass__c = canvass.Id, recordtypeid=AccountRecordTypeInfo.get('Telco Partner').getRecordTypeId());
        insert acct;
        Account customeraccount = TestMethodsUtility.generateAccount('customer');
        customeraccount.Telco_Partner__c =acct.id;
        customeraccount.Client_Tier__c='Platinum';
        insert customeraccount;
        Boolean flag = false;
        Account CMRacct = TestMethodsUtility.createAccount('cmr');
        
        Case cas = TestMethodsUtility.generateCase('CS Claim');
        cas.Contracted_Total_Value__c = 90;
        cas.AccountId = customeraccount.Id;
        cas.Total_BCC__c = 90;
        cas.BCC_Duration__c='2';
        cas.PAN1__c ='123456';
        insert cas;
        Contact cnt = TestMethodsUtility.createContact(customeraccount.Id);
        Order__c ord = TestMethodsUtility.createOrder(customeraccount.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(customeraccount, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(customeraccount, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 prod = TestMethodsUtility.createproduct();
        Directory__c dir = TestMethodsUtility.createDirectory();
        
        c2g__codaDimension1__c  dimension1 = TestMethodsUtility.createDimension1(dir);
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(customeraccount, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c = system.today();
        oln.Print_First_Bill_Date__c = system.today();
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Product2__c = prod.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Billing_Partner__c = CommonMessages.TelcoForDimension;
        oln.Payment_Method__c = CommonMessages.ACHPaymentMethod;
        oln.Billing_Frequency__c = CommonMessages.singlePayment;
        oln.Successful_Payments__c = 9;
        oln.CMR_Name__c = CMRacct.Id;
        oln.Payment_Duration__c = 1;
        oln.Media_Type__c = CommonMessages.oliPrintProductType;
        oln.Directory_Section__c = dirSec.Id;
        oln.Billing_Partner_Account__c = acct.Id;
        insert oln;      
        String directory_code = [select Directory_Code__c FROM Order_Line_Items__c where id =:oln.id limit 1].Directory_Code__c;
        
        System.debug('Print #### Directory ## Code 1 is :' +directory_code );
        
        Order_Line_Item_Discount__c OLID = TestMethodsUtility.generateOrderLineItemDiscount(og, oln);
        OLID.total_Credit_amount__c = '90';
        OLID.Case__c = cas.Id;
        OLID.Number_of_Billing_Periods_to_discount__c = 9;
        OLID.Issue_as_Single_Credit__c = true;
        OLID.Credit_Issued_To1__c = 'Telco';
        OLID.Telco__c = acct.Id;
        OLID.Total_Net_Commission__c = 43;
        insert OLID;
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(cas);
        SalesCreditNoteEngine_V2 obj = new SalesCreditNoteEngine_V2(controller);
        obj.onLoad();
        obj.roundValues(34);
       // obj.dateCalculation(oln);
        obj.checkfornullnegative(-1);
        obj.checkfornullnegative(1);
        Test.stopTest();
    }
    
    static testMethod void SCNTestV7() {
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo.get('Telco Partner').getRecordTypeId();
        
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = new Account(Name = 'Frontier Communications Corp', Phone ='(666)666-6666', Primary_Canvass__c = canvass.Id, recordtypeid=AccountRecordTypeInfo.get('Telco Partner').getRecordTypeId());
        insert acct;
        Account customeraccount = TestMethodsUtility.generateAccount('customer');
        customeraccount.Telco_Partner__c =acct.id;
        insert customeraccount;
        Boolean flag = false;
        Account CMRacct = TestMethodsUtility.createAccount('cmr');
        
        Case cas = TestMethodsUtility.generateCase('CS Claim');
        cas.Contracted_Total_Value__c = 90;
        cas.AccountId = customeraccount.Id;
        cas.Total_BCC__c = 90;
        cas.BCC_Duration__c='2';
        cas.PAN1__c ='123456';
        insert cas;
        Contact cnt = TestMethodsUtility.createContact(customeraccount.Id);
        Order__c ord = TestMethodsUtility.createOrder(customeraccount.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(customeraccount, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(customeraccount, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 prod = TestMethodsUtility.createproduct();
        Directory__c dir = TestMethodsUtility.createDirectory();
        
        c2g__codaDimension1__c  dimension1 = TestMethodsUtility.createDimension1(dir);
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(customeraccount, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c = system.today();
        oln.Print_First_Bill_Date__c = system.today();
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Product2__c = prod.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Billing_Partner__c = CommonMessages.TelcoForDimension;
        oln.Payment_Method__c = CommonMessages.ACHPaymentMethod;
        oln.Billing_Frequency__c = CommonMessages.singlePayment;
        oln.Successful_Payments__c = 9;
        oln.CMR_Name__c = CMRacct.Id;
        oln.Payment_Duration__c = 1;
        oln.Media_Type__c = CommonMessages.oliPrintProductType;
        oln.Directory_Section__c = dirSec.Id;
        oln.Billing_Partner_Account__c = customeraccount.Id;
        insert oln;      
        String directory_code = [select Directory_Code__c FROM Order_Line_Items__c where id =:oln.id limit 1].Directory_Code__c;
        
        System.debug('Print #### Directory ## Code 1 is :' +directory_code );
        
        Order_Line_Item_Discount__c OLID = TestMethodsUtility.generateOrderLineItemDiscount(og, oln);
        OLID.total_Credit_amount__c = '90';
        OLID.Case__c = cas.Id;
        OLID.Number_of_Billing_Periods_to_discount__c = 9;
        OLID.Issue_as_Single_Credit__c = true;
        OLID.Credit_Issued_To1__c = 'Telco';
        OLID.Telco__c = acct.Id;
        OLID.Total_Net_Commission__c = 43;
        insert OLID;
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(cas);
        SalesCreditNoteEngine_V2 obj = new SalesCreditNoteEngine_V2(controller);
        obj.onLoad();
        obj.roundValues(34);
       // obj.dateCalculation(oln);
        obj.checkfornullnegative(-1);
        obj.checkfornullnegative(1);
        Test.stopTest();
    }
    */
    static testMethod void SCNTestV8() {
        
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.generateAccount('telco');
        insert acct;
        Account customeraccount = TestMethodsUtility.generateAccount('customer');
        customeraccount.Telco_Partner__c =acct.id;
        insert customeraccount;
        Boolean flag = false;
        Account CMRacct = TestMethodsUtility.createAccount('cmr');
        
        Case cas = TestMethodsUtility.generateCase('CS Claim');
        cas.Contracted_Total_Value__c = 90;
        cas.AccountId = customeraccount.Id;
        //cas.AccountId = acct.Id;
        cas.Total_BCC__c = 90;
        cas.BCC_Duration__c='2';
        cas.PAN1__c ='123456';
        insert cas;
        Contact cnt = TestMethodsUtility.createContact(customeraccount.Id);
        Order__c ord = TestMethodsUtility.createOrder(customeraccount.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(customeraccount, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(customeraccount, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 prod = TestMethodsUtility.createproduct();
        Directory__c dir = TestMethodsUtility.createDirectory();
        
        c2g__codaDimension1__c  dim1 = new c2g__codaDimension1__c();
        dim1.Directory__c = dir.Id;
        dim1.c2g__ReportingCode__c = '012345';
        dim1.Name = 'test Dim1';
        dim1.Type__c = 'Print';
        insert dim1;
        
        c2g__codaDimension2__c dim2 = new c2g__codaDimension2__c();
        dim2.Name = 'test Dim2';
        dim2.Type__c = 'Print';
        dim2.c2g__ReportingCode__c = '0123';
        insert dim2;
        
        c2g__codaDimension3__c dim3 = new c2g__codaDimension3__c();
        dim3.Name = 'Local';
        dim3.c2g__ReportingCode__c = 'Telco (L1)';
        insert dim3;
        
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);    
        
        Order_Line_Item_Discount__c OLID = new Order_Line_Item_Discount__c();
        OLID.total_Credit_amount__c = '90';
        OLID.Case__c = cas.Id;
        OLID.Number_of_Billing_Periods_to_discount__c = 9;
        OLID.Issue_as_Single_Credit__c = true;
        OLID.Credit_Issued_To1__c = 'Telco';
        OLID.Telco__c = acct.Id;
        //OLID.SCNTelco__c=acct.Id;
        OLID.Total_Net_Commission__c = 43;
        OLID.Product__c = prod.Id;
        OLID.Product_Non_OLI__c = 'Print';
        OLID.Dimension_1__c = dim1.Id;
        OLID.Dimension_2__c = dim2.Id;
        OLID.Dimension_3__c = dim3.Id;
        insert OLID;
        
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(cas);
        SalesCreditNoteEngine_V2 obj = new SalesCreditNoteEngine_V2(controller);
        obj.onLoad();
        obj.roundValues(34);
        obj.checkfornullnegative(-1);
        obj.checkfornullnegative(1);
        obj.backToCase();
        Test.stopTest();
    }
    
 }