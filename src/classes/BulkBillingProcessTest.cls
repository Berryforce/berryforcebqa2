@isTest
public class BulkBillingProcessTest {
  static testMethod void bulkBillTest() {
        Test.StartTest();
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.createAccount('cmr');
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        insert og;
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Media_Type__c = CommonMessages.oliPrintProductType;
        oln.Payment_Method__c = CommonMessages.ccPaymentMethod;
        insert oln; 
        Order_Line_Items__c oln1 = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);              
        oln1.Quote_Signed_Date__c=system.today()+1;
        oln1.Order_Anniversary_Start_Date__c=system.today()-5;
        oln1.Talus_Go_Live_Date__c=system.today()+30;
        oln1.UnitPrice__c = 300;
        oln1.Media_Type__c = CommonMessages.digitalMediaType;
                oln1.Billing_Frequency__c='Monthly';
        oln1.Payment_Method__c = CommonMessages.ccPaymentMethod;
                oln1.Contract_Start_Date__c=Date.today();
                oln1.Contract_End_Date__c=Date.today().addMonths(12);   
                oln1.Payments_Remaining__c=4;            
        insert oln1; 
        
        BulkBillingProcess.bulkBill(acct.Id, System.label.TestAccountCMRRT);
        Test.StopTest();        
  }
  static testMethod void bulkBillTest2() {
        Test.StartTest();
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.createAccount('cmr');
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        insert og;
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Media_Type__c = CommonMessages.oliPrintProductType;
        oln.Payment_Method__c = CommonMessages.ccPaymentMethod;
        insert oln; 
        Order_Line_Items__c oln1 = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);              
        oln1.Quote_Signed_Date__c=system.today()+1;
        oln1.Order_Anniversary_Start_Date__c=system.today()-5;
        oln1.Talus_Go_Live_Date__c=system.today()+30;
        oln1.UnitPrice__c = 300;
        oln1.Media_Type__c = 'Print';
                oln1.Billing_Frequency__c='Monthly';
                oln1.Contract_Start_Date__c=Date.today();
                oln1.Contract_End_Date__c=Date.today().addMonths(12);
                oln1.Payments_Remaining__c=4;
        oln1.Payment_Method__c = CommonMessages.ccPaymentMethod;
        insert oln1; 
        
        BulkBillingProcess.bulkBill(acct.Id, System.label.TestAccountCMRRT);
        Test.StopTest();        
  } 
}