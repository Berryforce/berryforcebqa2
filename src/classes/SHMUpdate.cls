global class  SHMUpdate implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        set<String> strStatus = new set<String>{'Complete'};
        String query = 'Select Id, Directory_Code__c,Directory_Section__c,Directory_Heading__c  From Section_Heading_Mapping__c where Directory_Code__c = null order by Directory_Section__c,Directory_Heading__c';
        return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext bc, List<Section_Heading_Mapping__c> dirListingList) {
         update dirListingList;
     }
     
     global void finish(Database.BatchableContext bc) {
         
     }
}