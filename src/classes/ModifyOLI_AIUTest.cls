//ModifyOLI_AIU Trigger Test class
@isTest(SeeAllData=True)
public class ModifyOLI_AIUTest{
    static testmethod void modifyolitest(){
        Test.starttest();
        
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.createproduct();
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        Directory__c directory = TestMethodsUtility.createDirectory();
       // Directory_Section__c dirSection = TestMethodsUtility.createDirectorySection(directory);

        Modification_Order_Line_Item__c mol = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI.id);
       // mol.Directory_Section__c = dirSection.Id;
        insert mol;
        
        ModificationOLIHandlerController.onBeforeInsert(null);
        ModificationOLIHandlerController.onAfterUpdate(null, null);
        ModificationOLIHandlerController.onBeforeUpdate(null, null);
        
        Test.stoptest();
    }
}