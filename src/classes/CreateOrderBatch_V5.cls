global class CreateOrderBatch_V5 implements Database.Batchable<sObject>,Database.Stateful{
    global String SOQL = '';
    global Set<id> setOppId;
    global Boolean bolBots;
    global String strLabel;
    global Boolean strBOTS;
    global DMOpportunityBatch__c objDMBatOpp;
    
    global CreateOrderBatch_V5(){
    }
    
    global CreateOrderBatch_V5(Set<id> setOppLi, DMOpportunityBatch__c objDMOpp){
        setOppId = setOppLi;
        strBOTS = false;
        objDMBatOpp = objDMOpp;
        if(setOppId != null && setOppId.size() > 0) {
            strLabel = label.NI_Order_Creation_Query_Where_Cond_batch1_exclude_orphans;
        }
        else {          
            strLabel = label.NI_Order_Creation_Query_Where_Cond_batch1;
        }        
    }
    
    global CreateOrderBatch_V5(DMOpportunityBatch__c objDMOpp){
        objDMBatOpp = objDMOpp;
        strBOTS = true;
        strLabel = label.BOTS_Order_Creation_Query_Where_Cond_Batch1;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        String strCond = '';
        if(String.isNotBlank(objDMBatOpp.Core_Start_Number__c)) {
            strCond = ' and CORE_Migration_ID__c > \''+objDMBatOpp.Core_Start_Number__c+'\'';
        }
        if(String.isNotBlank(objDMBatOpp.Core_End_Number__c)) {
            strCond = strCond + ' and CORE_Migration_ID__c <= \''+objDMBatOpp.Core_End_Number__c+'\'';
        }
        if(String.isNotBlank(objDMBatOpp.Additional_Condition__c)) {
            strCond = strCond + ' ' + objDMBatOpp.Additional_Condition__c;
        }
        
        //Set<Id> setUserId = new set<Id>{'005K0000001yKP3IAM'};
        
        if(SOQL == ''){
            SOQL = 'Select Owner.CommunityNickname,Account_Primary_Canvass__r.Billing_Entity__c,Owner.LastName,Account.Billing_Anniversary_Date__c, RAS_ID__c, RAS_Date__c, StageName,LastModifiedBy.Email,LastModifiedBy.ManagerId,Account.Name,Account.Berry_ID__c, Signing_Method__c, Owner.Name, Owner.email, Billing_Partner__c, modifyid__c, isLocked__c, Payment_Method__c, Name, Billing_Frequency__c, Billing_Contact__r.Contact_Role__c, Billing_Contact__r.Email, Billing_Contact__r.Statement_Suppression__c, Billing_Contact__r.Phone, Billing_Contact__r.MailingCountry, Billing_Contact__r.MailingPostalCode, Billing_Contact__r.MailingState, Billing_Contact__r.MailingCity, Billing_Contact__r.MailingStreet, Billing_Contact__c, Account.Delinquency_Indicator__c, Account.BillingCountry, Account.BillingPostalCode, Account.BillingState, Account.BillingCity, Account.BillingStreet, AccountId, Signing_Contact__c, Account.Phone, Account.TalusAccountId__c, Billing_Contact__r.TalusContactId__c, Account.Statement_Suppression__c,Account_Number__c, Account.Account_Number__c, Account_Manager__r.Email, Account_Manager__r.FirstName, Account_Manager__r.LastName, Account_Manager__c, Close_Date__c, IsClosed, IsWon, DocuSign_Received__c, OwnerId, Owner.FirstName, Owner.CompanyName, Owner.Fax, Owner.Phone, Owner.PostalCode, Owner.Street, Owner.city, Owner.State, Owner.Country, (Select Id, Opportunity.modifyid__c, OpportunityId, Full_Rate__c,SortOrder, PricebookEntryId, Quantity, Discount, UnitPrice, ServiceDate, Description, Listing_Id__c, Listing__c, Renewals_Action__c, Is_Anchor__c,Is_Caption__c,Is_Child__c, Canvass__c, PricebookEntry.Name, directory__c, PricebookEntry.Product2.Media_Type__c, PricebookEntry.Pricebook2Id, Original_Line_Item_ID__r.Status__c, PricebookEntry.Product2Id, PricebookEntry.ProductCode, Parent_ID__c, PricebookEntry.Product2.Product_Type__c, PricebookEntry.Product2.Addon_Type__c, Distribution_Area__c, Original_Line_Item_ID__r.Order_Group__c, Digital_Product_Requirement__c,Effective_Date__c,Billing_Duration__c,Product_Type__c,Billing_Start_Date__c, Billing_End_Date__c, isModify__c,Directory__r.Telco_Provider__c,Canvass__r.Primary_Telco__c, Package_ID__c, Parent_Line_Item__c, Geo_Type__c, Geo_Code__c, is_p4p__c,Scope__c, Directory_Section__c, Status__c,Category__c, Directory_Heading__c, P4P_Price_Per_Click_Lead__c,P4P_Billing__c , Directory_Edition__r.Bill_Prep__c, Auto_Number__c, Locality__c, Directory_Edition__c, Directory_Edition__r.New_Print_Bill_Date__c, Listing_Name__c,Pricing_Program__c, Parent_ID_of_Addon__c, Original_Line_Item_ID__c, strOppliCombo__c, Package_Item_Quantity__c, PricebookEntry.Product2.Inventory_Tracking_Group__c,PricebookEntry.Product2.Is_IBUN_Bundle_Product__c, Package_Name__c,Requires_Inventory_Tracking__c,CORE_Migration_ID__c,Package_ID_External__c,Original_Line_Item_ID__r.Directory_Edition__c,Display_Ad__c,PricebookEntry.Product2.Vendor__c,Original_Line_Item_ID__r.Digital_Product_Requirement__c,Original_Line_Item_ID__r.Effective_Date__c,UDAC__c,Seniority_Date_Reset__c,Original_OLI_Seniority_Date__c,Original_Line_Item_ID__r.Talus_Go_Live_Date__c,Original_Line_Item_ID__r.Sent_to_fulfillment_on__c, Original_Line_Item_ID__r.Digital_Product_Requirement__r.Data_Fulfillment_Form__c, Original_Line_Item_ID__r.FulfillmentDate__c,Original_Line_Item_ID__r.Directory__c, Original_Line_Item_ID__r.Listing__c, Original_Line_Item_ID__r.ProductCode__c, Original_Line_Item_ID__r.Directory_Heading__c,Original_Line_Item_ID__r.P4P_Tracking_Number_ID__c,Original_Line_Item_ID__r.P4P_Tracking_Number_Status__c,BCC_Applied__c,BCC_Duration__c,BCC_Name__c,BCC_Redeemed__c,Berry_Cares_Certificate_ID__c, Directory_Edition__r.Name, OPPLI_Exception_Discount__c From OpportunityLineItems where DoNotConvert__c = false order by Parent_ID__c, Parent_ID_of_Addon__c) From Opportunity '+strLabel+strCond;
            
        }
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> lstOpp){
        //System.debug('Testingg a Number of Queries used in this batch apex code so far: ' + Limits.getQueries());
        set<Id> setOppIds = new set<Id>();
        map<id,Opportunity> mapOpportunity = new map<id,Opportunity>();
        Savepoint sp = Database.setSavepoint();
        try {
            map<string,List<Opportunity>> mpValidatedOpp = createBulkOrderController.ValidateOpportunity(lstOpp);        
            List<Opportunity> lstOpportunity = mpValidatedOpp.get('BOTS');
            List<Opportunity> lstModifiedOpportunity = mpValidatedOpp.get('NI');
            
            if(lstModifiedOpportunity != null && lstModifiedOpportunity.size() > 0) {
                List<Opportunity> lstModOpp = OpportunitySOQLMethods.getOpportunityForLockCloneCreateOrder(lstModifiedOpportunity);
                //list<OpportunityLineItem> lstOLI = objOpportunity.OpportunityLineItems;
                List<Order__c> lstOrder = createBulkOrderController.CheckAndCreateNewOrderIfNotExists(lstModOpp);
                
                createBulkOrderModifyController.CreateOrderAndModifyLineItem(lstModifiedOpportunity, lstOrder);
                
                createBulkOrderController.setOpportunityToClosedWon(lstModifiedOpportunity);
                
            }
            
            if(lstOpportunity != null && lstOpportunity.size() > 0) {
                //System.debug('Testingg b Number of Queries used in this batch apex code so far: ' + Limits.getQueries());
                List<Order__c> lstOrder = createBulkOrderController.CheckAndCreateNewOrderIfNotExists(lstOpportunity);
                //System.debug('Testingg c Number of Queries used in this batch apex code so far: ' + Limits.getQueries());
                
                List<Order_Group__c> lstOrderGroup = createBulkOrderController.CreateNewOrderGroupNew(lstOpportunity, lstOrder);
                //System.debug('Testingg l4 lstorderGroup in batch ' +lstorderGroup);
                //System.debug('Testingg d Number of Queries used in this batch apex code so far: ' + Limits.getQueries());
                
                List<Order_Line_Items__c> lstOLI = createBulkOrderController.CreateOrderLineItem(lstOpportunity, lstOrder, lstOrderGroup);              
                //System.debug('Testingg e Number of Queries used in this batch apex code so far: ' + Limits.getQueries());
                
                for(Opportunity objOpp : lstOpportunity){
                    setOppIds.add(objOpp.AccountId);
                    mapOpportunity.put(objOpp.id, objOpp);
                }
                
                //map<Id, Fulfillment_Profile__c> mapFProfile = FulfillmentProfileSOQLMethods.getFulFillmentProfileByAccountId(setOppIds);
                
                List<Fulfillment_Profile__c> lstFP = FulfillmentProfileSOQLMethods.getFulFillmentProfileListByAccountId(setOppIds);
                map<Id, Fulfillment_Profile__c> mapFProfile = new map<Id, Fulfillment_Profile__c>();
                
                for(Fulfillment_Profile__c objFP : lstFP) {
                    mapFProfile.put(objFP.Account__c, objFP);
                }
                
                //System.debug('Testingg f Number of Queries used in this batch apex code so far: ' + Limits.getQueries());
                
                if(lstOLI != null && lstOpportunity != Null && mapFProfile != Null){
                    createBulkOrderController.FindAddOnProductForDFFAndCreateDFF(lstOLI, mapOpportunity, mapFProfile);
                }
                
                //System.debug('Testingg g Number of Queries used in this batch apex code so far: ' + Limits.getQueries());
                
                createBulkOrderController.setOpportunityToClosedWon(lstOpportunity);
            }
        }catch(CustomException objExp){
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+objExp.strType+'. Error Message : '+objExp.getMessage(), objExp.strStackTrace, 'Batch update of migrated Opportunity');
            
        }catch(Exception objExp){
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+objExp.getTypename()+'. Error Message : '+objExp.getMessage(), objExp.getStackTraceString(), 'Batch update of migrated Opportunity');
            
        }
        
        //System.debug('Testingg h Number of Queries used in this batch apex code so far: ' + Limits.getQueries());
    }
    
    global void finish(Database.BatchableContext bc){
        String strErrorMessage = '';
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        
        // Send an email to the Apex job's submitter notifying of job completion.  
        //Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email, 'Mythreyee.kumar@theberrycompany.com'};
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
        //CommonEmailUtils.sendHTMLEmail(toAddresses, 'Migrated Opportunity Records processing status ' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems +
          //' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'. Please check Exception records for any other errors that might have occured while processing Opportunities.');
    
        DMOpportunityBatch__c objUpdtDMOppBatch = new DMOpportunityBatch__c(id=objDMBatOpp.id, Processed__c = true);
        update objUpdtDMOppBatch;
        
        DMOpportunityBatch__c objDMNextOpp = CreateOrderTestController.getNextUnProcessedSet(Integer.valueOf(objDMBatOpp.name)+1);
        CreateOrderBatch_V5 objCreateOrder;
        if(objDMNextOpp != null) {
            if(strBOTS) {
            objCreateOrder = new CreateOrderBatch_V5(objDMNextOpp);
            }
            else {
            List<OpportunityLineItem> lstOppLI = [select opportunityid from OpportunityLineItem where CORE_Migration_ID__c != null and Opportunity.isLocked__c = false and Opportunity.modifyid__c != null and Opportunity.stagename in ('Closed Decrease','Closed Won','Closed Lost','Closed Migrated') and Original_Line_Item_ID__c = null and Renewals_Action__c in ('Cancel','Renew','Modify')];
            Set<id> setOppLi = new Set<id>();
            for(OpportunityLineItem objOppLi : lstOppLI) {
                setOppLi.add(objOppLi.opportunityid);
            }
            objCreateOrder = new CreateOrderBatch_V5(setOppLi,objDMNextOpp);
            }
            //system.debug('Testingg CreateOrderTestController closeOppsandCreateOrder Normal flow');
            database.executebatch(objCreateOrder,Integer.valueOf(objDMNextOpp.Batch_Size__c));
        }
    }
}