@isTest
public class AccountSOQLMethodsTest {
	@isTest static void AccountSOQLMethodsCoverage() {
        User u = TestMethodsUtility.generateStandardUser();
        System.runAs(u) {
        	
        	list<Account> lstAccount = new list<Account>();
	        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
	        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
	        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
	        lstAccount.add(TestMethodsUtility.generateAccount('cmr'));
	        lstAccount.add(TestMethodsUtility.generateAccount('national'));
	        
	        insert lstAccount;
	        
	        system.assertNotEquals(null, lstAccount);
	        Account newAccount = new Account();
	        Account newPubAccount = new Account();
	        Account newTelcoAccount = new Account();
	        Account newCMRAccount = new Account();
	        Account newClientAccount = new Account();
	        
	        for(Account iterator : lstAccount) {
	        	if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
	        		newAccount = iterator;
	        	}
	        	else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
	        		newPubAccount = iterator;
	        	}
	        	else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountTelcoRT)) {
	        		newTelcoAccount = iterator;
	        	}
	        	else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCMRRT)) {
	        		newCMRAccount = iterator;
	        	}
	        	else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountNationalRT)) {
	        		newClientAccount = iterator;
	        	}
	        }
	        
	        newClientAccount.CMR_Number__c = newCMRAccount.CMR_Number__c;
	        newClientAccount.ParentId = newCMRAccount.Id;
	        update newClientAccount;
	        
	        system.assertNotEquals(null, newClientAccount.CMR_Number__c);
	        system.assertNotEquals(null, newCMRAccount.CMR_Number__c);
	        
	        Contact objContact = TestMethodsUtility.createContact(newAccount.Id);           
            system.assertNotEquals(null, objContact);
            system.assert(objContact != null, 'Contact ID : ' + objContact.Id);
            system.assertNotEquals(null, AccountSOQLMethods.getAccountContactByAccountID(new set<Id>{newAccount.Id}));
            system.assertNotEquals(null, AccountSOQLMethods.getAccountByAccountID(newAccount.Id));
            system.assertNotEquals(null, AccountSOQLMethods.getContactByAccountID(new set<Id>{newAccount.Id}, new set<Id>{objContact.Id}));
            AccountSOQLMethods.getAccountByOpportunityAccountID(new set<ID>{newAccount.Id});
            AccountSOQLMethods.getContactOpportunityByAccountId(new set<ID>{newAccount.Id});
            system.assertEquals(0, AccountSOQLMethods.getAccountByParentId(new set<Id> {newAccount.Id}).size());
            system.assertEquals(0, AccountSOQLMethods.getAccountByAccountName(string.valueof(newAccount.Name), new set<Id>{newAccount.Id}).size());
            AccountSOQLMethods.getAccountByIDandParentID(new set<Id>{newAccount.Id});
            AccountSOQLMethods.getNationalCommissionById(new set<Id>{newAccount.Id});
            AccountSOQLMethods.getClientForMediaTrax(String.valueOf(newAccount.RecordTypeId));
            AccountSOQLMethods.getClientForMediaTraxUpdate(String.valueOf(newAccount.RecordTypeId));
            AccountSOQLMethods.getAccountWithFulfillmentProfileByIds(new set<Id>{newAccount.Id});
            AccountSOQLMethods.getAccountByCode(new set<String>{newCMRAccount.CMR_Number__c}, new set<String>{newClientAccount.Client_Number__c}, new set<String>{newPubAccount.Publication_Code__c});
            AccountSOQLMethods.getClientNatAcctByNumbers(newClientAccount.Client_Number__c, newCMRAccount.CMR_Number__c);
            AccountSOQLMethods.getCMRAccountByNumber(newCMRAccount.CMR_Number__c, System.Label.TestAccountCMRRT, System.Label.TestAccountNationalRT);
            system.assertEquals(AccountSOQLMethods.getAccountByCode(new set<String>(), new set<String>(), new set<String>()).size(), 0);
            AccountSOQLMethods.getClientAccountByNumber(new set<String>{newClientAccount.CMR_Number_Client_Number__c});
            AccountSOQLMethods.getClientAccountByNumber(newCMRAccount.AccountNumber, System.Label.TestAccountCustomerRT, newClientAccount.AccountNumber);
            AccountSOQLMethods.getSetDelinquencyIndicatorAccountByParentID(new set<Id>{newAccount.Id});
            AccountSOQLMethods.getAccountWithStatementById(new set<Id>{newAccount.Id});
            AccountSOQLMethods.getCMRAccountByNumber(new set<String>{newCMRAccount.AccountNumber});
            AccountSOQLMethods.getAccountByPubCode(new set<String>{newPubAccount.Publication_Code__c});
            AccountSOQLMethods.getAccountsByAccountIdWithSensitiveHeadFalse(new set<Id>{newAccount.Id});
            AccountSOQLMethods.getAccountsByAcctId(new set<Id>{newAccount.Id});
            AccountSOQLMethods.getAccountsByAcctIdAndCSClaimRT(new set<Id>{newAccount.Id}, system.label.Case_RecordType_Id);
        }
    }
}