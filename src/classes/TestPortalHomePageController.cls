/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestPortalHomePageController {

    static User portalCont;
    static User testUser;
    static {
    portalCont = getUser();
    portalCont.Terms_And_Conditions_Agreed__c = false;
    
    list<User> testUser1 = [Select id from User where username='testclassvertex@theberrycompany.com'];
    if(testUser1!=null && testUser1.size()>0)
    {
        testUser1[0].Terms_And_Conditions_Agreed__c=false;
        update testUser1;
    }else
    {
        insert portalCont;
    }
    }
    public static User getUser()
    {
      //Select id,Name,Terms_And_Conditions_Agreed__c from User where id = :contactId 
      //Profile prof = new Profile(Existing Profile);
      Profile p = [select id from profile where name='Standard User'];
 
      testUser = new User(alias = 'u118', email='testclassvertex@theberrycompany.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id, country='United States',Phone='5131111111',
        timezonesidkey='America/Los_Angeles', username='testclassvertex@theberrycompany.com');
   
      //insert testUser;
      return testUser;
    }
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Test.startTest();
        PortalHomePageController portalHome = new PortalHomePageController();
        portalHome.portalHome();
        Test.stopTest();
        
        
    }
}