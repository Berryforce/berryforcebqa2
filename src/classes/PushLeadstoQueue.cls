global class PushLeadstoQueue implements Database.Batchable<sObject>
{
     global final string AssignmentRule;
     
     global Database.QueryLocator start(Database.BatchableContext bc)
     {
        map<Date, DateTime> mapFinalHolidays = getHolidayList();
        Date TodayDate = Date.today();
        Date LastModifDt = DateCalculatenew(TodayDate, mapFinalHolidays,-3);
        Datetime dt1 = datetime.newInstance(LastModifDt.year(), LastModifDt.month(),LastModifDt.day());
        
        return database.getQuerylocator('SELECT CreatedById,CreatedDate,FirstName,Id,Industry,IsConverted,IsDeleted,IsLead__c,LastModifiedById,LastModifiedDate,LastName,Name,OwnerId,Phone,Rating,RecordTypeId,Salutation,State,Status,Street,SystemModstamp,Website,Primary_Canvass__c, Primary_Canvass__r.Name FROM Lead WHERE LastModifiedDate <: dt1 AND IsConverted = false');

    }
    
    
    global void execute(Database.BatchableContext bc, List<Lead> LstUniqueLead)
    {
        map<Date, DateTime> mapFinalHolidays = getHolidayList();
        Date TodayDate = Date.today();
        Date LastModifDt = DateCalculatenew(TodayDate, mapFinalHolidays,-3);
        Datetime dt1 = LastModifDt.adddays(1);
           
        List<Lead> leadstoUpdate = new List<Lead>();

        leadstoUpdate =[SELECT CreatedById,CreatedDate,FirstName,Id,Industry,IsConverted,IsDeleted,IsLead__c,LastModifiedById,LastModifiedDate,LastName,Name,OwnerId,Phone,Rating,RecordTypeId,Salutation,State,Status,Street,SystemModstamp,Website,Primary_Canvass__c, Primary_Canvass__r.Name FROM Lead WHERE ID IN: LstUniqueLead AND LastModifiedDate<:dt1 AND IsConverted = false];

        AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType = 'Lead' and Active = true limit 1];

        
        for(Lead selectLeadStatus : leadstoUpdate)
        {
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
            selectLeadStatus.setOptions(dmlOpts);
        }
        update leadstoUpdate;
      
        
    }   
    
    public static Date DateCalculatenew(Date X, map<Date, DateTime> mapholidays, integer DayDifference) 
    {      
   
        if(DayDifference > 0)
        {
           integer counter = DayDifference;
           for(integer i =1;i<=counter;i++)
           {
               DateTime dt = DateTime.newInstance(X.addDays(i), Time.newInstance(0, 0, 0, 0));
               
               String dayOfWeek = dt.format('EEEE');
               if(dayOfWeek.toUpperCase() =='SATURDAY' || dayOfWeek.toUpperCase() =='SUNDAY' || mapholidays.containsKey(X.addDays(i)))
               {
                 counter++;
               }
           }
           return X.addDays(counter);
        }
        else
        {   
           integer counter = DayDifference;
           for(integer i =-1;i>=counter;i--)
           {
               DateTime dt = DateTime.newInstance(X.addDays(i), Time.newInstance(0, 0, 0, 0));
       
               String dayOfWeek = dt.format('EEEE');
               if(dayOfWeek.toUpperCase() =='SATURDAY' || dayOfWeek.toUpperCase() =='SUNDAY' || mapholidays.containsKey(X.addDays(i)))
               {
                 counter--;
               }
           }
           return X.addDays(counter);
    
        }
        return null;
    } 
    
        
    public static map<Date,DateTime> getHolidayList() 
    {
        map<Date,DateTime> mapHolidays = new map<Date,DateTime>();
        
        for(Holiday objH : [select id,ActivityDate,Name, RecurrenceType, RecurrenceStartDate, RecurrenceMonthOfYear, RecurrenceInstance, RecurrenceDayOfWeekMask, IsRecurrence, IsAllDay from Holiday])
        {
            mapHolidays.put(Date.newInstance((objH.ActivityDate).year(),(objH.ActivityDate).month(),(objH.ActivityDate).day()),objH.ActivityDate);
        }
        return mapHolidays;
    }
    
    
    global void finish(Database.BatchableContext bc){
    
    }
}