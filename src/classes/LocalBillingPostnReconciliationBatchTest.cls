@IsTest(SeeAlldata=true)
public with sharing class LocalBillingPostnReconciliationBatchTest {

  public static testMethod void TestInvoicePostBatch(){
          Canvass__c c=TestMethodsUtility.createCanvass();
            
            list<Account> lstAccount1 = new list<Account>();
            lstAccount1.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount1[0].Billing_Anniversary_Date__c=date.today();
            insert lstAccount1;  
            Account newAccount1 = new Account();
            for(Account iterator : lstAccount1) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount1 = iterator;
                }
           }
            Telco__c objTelco1 =TestMethodsUtility.createTelco(newAccount1.Id);
            objTelco1.Telco_Code__c = 'Test';
            update objTelco1;
            system.assertNotEquals(newAccount1.ID, null);
            Contact newContact1 = TestMethodsUtility.createContact(newAccount1.Id);
            Pricebook2 newPriceBook1 = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv1 = TestMethodsUtility.createDivision();
        
            Directory__c objDir1 = TestMethodsUtility.generateDirectory();
            objDir1.Telco_Recives_Electronice_File__c=true;
            objDir1.Telco_Provider__c = objTelco1.Id;
            objDir1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDir1.Publication_Company__c = newAccount1.Id;
            objDir1.Division__c = objDiv1.Id;
            objDir1.Directory_Code__c = '100000';
            insert objDir1;
            
            Directory_Heading__c objDH1 = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS1 = TestMethodsUtility.createDirectorySection(objDir1);
            Section_Heading_Mapping__c objSHM1 = TestMethodsUtility.generateSectionHeadingMapping(objDS1.Id, objDH1.Id);
            insert objSHM1;
            
            Directory__c objDirNew1 = TestMethodsUtility.generateDirectory();
            objDirNew1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDirNew1.Publication_Company__c = newAccount1.Id;
            objDirNew1.Division__c = objDiv1.Id;
            objDirNew1.Directory_Code__c = '100001';
            insert objDirNew1;
            
            Directory_Edition__c objDirEdNew2 = TestMethodsUtility.generateDirectoryEdition(objDirNew1);
            objDirEdNew2.XML_Output_Total_Amount__c=200;
            objDirEdNew2.Pub_Date__c =Date.today();
            objDirEdNew2.Bill_Prep__c=date.today();
            insert objDirEdNew2;
            
            Product2 newProduct3 = TestMethodsUtility.generateproduct();
            newProduct3.Family = 'Print';
            insert newProduct3;
            
            Product2 objProd3 = new Product2();
            objProd3.Name = 'Test';
            objProd3.Product_Type__c = 'Print';
            objProd3.ProductCode = 'WLCSH';
            objProd3.Print_Product_Type__c='Display';
            insert  objProd3;
            
            Product2 objProd4 = new Product2();
            objProd4.Name = 'Test';
            objProd4.Product_Type__c = 'Print';
            objProd4.ProductCode = 'GC50';
            objProd4.Print_Product_Type__c='Specialty';          
            insert objProd4;
            
            Opportunity newOpportunity2 = TestMethodsUtility.generateOpportunity('new');
            newOpportunity2.AccountId = newAccount1.Id;
            newOpportunity2.Pricebook2Id = newPriceBook1.Id;
            insert newOpportunity2;
            
            Order__c newOrder1 = TestMethodsUtility.createOrder(newAccount1.Id);
            
            Order_Group__c newOrderSet1 = TestMethodsUtility.createOrderSet(newAccount1, newOrder1, newOpportunity2);
            
          
            c2g__codaCompany__c companyObj =TestMethodsUtility.generateCompany();
            
            Order_Line_Items__c objOrderLineItemStmtOdd = new Order_Line_Items__c(Billing_Partner__c='Berry123',Account__c=newAccount1.Id, 
            Billing_Contact__c=newContact1.id, Opportunity__c=newOpportunity2.id, Order_Group__c=newOrderSet1.id,
            Order__c=newOrder1.id,Product2__c=objProd4.Id,is_p4p__c=false,media_type__c='Print',
            Directory_Edition__c = objDirEdNew2 .Id,Directory__c=objDirNew1.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Statement',
            Package_ID__c='pkgid_12',Payments_Remaining__c=10,Successful_Payments__c=1);
            insert objOrderLineItemStmtOdd;
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount1,newOpportunity2);
            invoice.Customer_Name__c=newAccount1.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.c2g__OwnerCompany__c = companyObj.id;
            invoice.c2g__Account__c= newAccount1.id;
            invoice.Is_Statement_Generated__c=false;
            insert invoice;
            
            c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDirNew1);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd4);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            c2g__codaDimension3__c  dimension3_ss=TestMethodsUtility.createDimension3(objOrderLineItemStmtOdd);
            c2g__codaInvoiceLineItem__c invoiceLiStmt=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItemStmtOdd,invoice,objProd4,dimension1,dimension2,dimension3_ss,dimension4 );
       
            invoice.SI_Successful_Payments__c=2;
            invoice.SI_P4P__c=false;
            invoice.SI_Media_Type__c='Print';
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.SI_Directory_Code__c='100001';
            invoice.SI_Directory_Edition_Code__c=[Select Edition_Code__c from Directory_Edition__c where id=:objDirEdNew2.Id].Edition_Code__c ;
            invoice.SI_Payment_Method__c='Telco Billing';
            invoice.SI_Payments_Remaining__c =10;
            update invoice;
            
            Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today(),From_Date__c = date.newinstance(2012, 3, 17),To_Date__c =date.newinstance(2015, 1, 16));
            insert objDummy;
            
            Test.startTest();
               List<c2g__codaInvoice__c> ListInvoice=new List<c2g__codaInvoice__c>{invoice};
               LocalBillingSIPostOrReconciliationBatch lbSI=new LocalBillingSIPostOrReconciliationBatch(objDummy,'post');
               Database.BatchableContext bc;
               lbSI.start(bc);
               lbSI.execute(bc,ListInvoice);
              
            Test.stopTest();
  }
  
  public static testMethod void TestInvoiceReconcileBatch(){
          Canvass__c c=TestMethodsUtility.createCanvass();
            
            list<Account> lstAccount1 = new list<Account>();
            lstAccount1.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount1[0].Billing_Anniversary_Date__c=date.today();
            insert lstAccount1;  
            Account newAccount1 = new Account();
            for(Account iterator : lstAccount1) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount1 = iterator;
                }
           }
            Telco__c objTelco1 =TestMethodsUtility.createTelco(newAccount1.Id);
            objTelco1.Telco_Code__c = 'Test';
            update objTelco1;
            system.assertNotEquals(newAccount1.ID, null);
            Contact newContact1 = TestMethodsUtility.createContact(newAccount1.Id);
            Pricebook2 newPriceBook1 = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv1 = TestMethodsUtility.createDivision();
        
            Directory__c objDir1 = TestMethodsUtility.generateDirectory();
            objDir1.Telco_Recives_Electronice_File__c=true;
            objDir1.Telco_Provider__c = objTelco1.Id;
            objDir1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDir1.Publication_Company__c = newAccount1.Id;
            objDir1.Division__c = objDiv1.Id;
            objDir1.Directory_Code__c = '100000';
            insert objDir1;
            
            Directory_Heading__c objDH1 = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS1 = TestMethodsUtility.createDirectorySection(objDir1);
            Section_Heading_Mapping__c objSHM1 = TestMethodsUtility.generateSectionHeadingMapping(objDS1.Id, objDH1.Id);
            insert objSHM1;
            
            Directory__c objDirNew1 = TestMethodsUtility.generateDirectory();
            objDirNew1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDirNew1.Publication_Company__c = newAccount1.Id;
            objDirNew1.Division__c = objDiv1.Id;
            objDirNew1.Directory_Code__c = '100001';
            insert objDirNew1;
            
            Directory_Edition__c objDirEdNew2 = TestMethodsUtility.generateDirectoryEdition(objDirNew1);
            objDirEdNew2.XML_Output_Total_Amount__c=200;
            objDirEdNew2.Pub_Date__c =Date.today();
            objDirEdNew2.Bill_Prep__c=date.today();
            insert objDirEdNew2;
            
            Product2 newProduct3 = TestMethodsUtility.generateproduct();
            newProduct3.Family = 'Print';
            insert newProduct3;
            
            Product2 objProd3 = new Product2();
            objProd3.Name = 'Test';
            objProd3.Product_Type__c = 'Print';
            objProd3.ProductCode = 'WLCSH';
            objProd3.Print_Product_Type__c='Display';
            insert  objProd3;
            
            Product2 objProd4 = new Product2();
            objProd4.Name = 'Test';
            objProd4.Product_Type__c = 'Print';
            objProd4.ProductCode = 'GC50';
            objProd4.Print_Product_Type__c='Specialty';          
            insert objProd4;
            
            Opportunity newOpportunity2 = TestMethodsUtility.generateOpportunity('new');
            newOpportunity2.AccountId = newAccount1.Id;
            newOpportunity2.Pricebook2Id = newPriceBook1.Id;
            insert newOpportunity2;
            
            Order__c newOrder1 = TestMethodsUtility.createOrder(newAccount1.Id);
            
            Order_Group__c newOrderSet1 = TestMethodsUtility.createOrderSet(newAccount1, newOrder1, newOpportunity2);
            
          
            c2g__codaCompany__c companyObj =TestMethodsUtility.generateCompany();
            
            Order_Line_Items__c objOrderLineItemStmtOdd = new Order_Line_Items__c(Billing_Partner__c='Berry123',Account__c=newAccount1.Id, 
            Billing_Contact__c=newContact1.id, Opportunity__c=newOpportunity2.id, Order_Group__c=newOrderSet1.id,
            Order__c=newOrder1.id,Product2__c=objProd4.Id,is_p4p__c=false,media_type__c='Print',
            Directory_Edition__c = objDirEdNew2 .Id,Directory__c=objDirNew1.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Statement',
            Package_ID__c='pkgid_12',Payments_Remaining__c=10,Successful_Payments__c=1);
            insert objOrderLineItemStmtOdd;
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount1,newOpportunity2);
            invoice.Customer_Name__c=newAccount1.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.c2g__OwnerCompany__c = companyObj.id;
            invoice.c2g__Account__c= newAccount1.id;
            invoice.Is_Statement_Generated__c=false;
            insert invoice;
            
            c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDirNew1);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd4);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            c2g__codaDimension3__c  dimension3_ss=TestMethodsUtility.createDimension3(objOrderLineItemStmtOdd);
            c2g__codaInvoiceLineItem__c invoiceLiStmt=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItemStmtOdd,invoice,objProd4,dimension1,dimension2,dimension3_ss,dimension4 );
       
            invoice.SI_Successful_Payments__c=2;
            invoice.SI_P4P__c=false;
            invoice.SI_Media_Type__c='Print';
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.SI_Directory_Code__c='100001';
            invoice.SI_Directory_Edition_Code__c=[Select Edition_Code__c from Directory_Edition__c where id=:objDirEdNew2.Id].Edition_Code__c ;
            invoice.SI_Payment_Method__c='Telco Billing';
            invoice.SI_Payments_Remaining__c =10;
            update invoice;
            
                     
            Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today(),From_Date__c = date.newinstance(2012, 3, 17),To_Date__c =date.newinstance(2015, 1, 16));
            insert objDummy;
            
            Test.startTest();
               List<c2g__codaInvoice__c> ListInvoice=new List<c2g__codaInvoice__c>{invoice};
               LocalBillingSIPostOrReconciliationBatch lbSI=new LocalBillingSIPostOrReconciliationBatch(objDummy,'reconcile');
               Database.BatchableContext bc;
               lbSI.start(bc);
               lbSI.execute(bc,ListInvoice);
               
            Test.stopTest();
  }
  public static testMethod void TestSCNPostBatch(){
             Canvass__c c=TestMethodsUtility.createCanvass();
            list<Account> lstAccount1 = new list<Account>();
            lstAccount1.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount1[0].Billing_Anniversary_Date__c=date.today();
            insert lstAccount1;  
            Account newAccount1 = new Account();
            for(Account iterator : lstAccount1) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount1 = iterator;
                }
           }
            Telco__c objTelco1 =TestMethodsUtility.createTelco(newAccount1.Id);
            objTelco1.Telco_Code__c = 'Test';
            update objTelco1;
            system.assertNotEquals(newAccount1.ID, null);
            Contact newContact1 = TestMethodsUtility.createContact(newAccount1.Id);
            Pricebook2 newPriceBook1 = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv1 = TestMethodsUtility.createDivision();
        
            Directory__c objDir1 = TestMethodsUtility.generateDirectory();
            objDir1.Telco_Recives_Electronice_File__c=true;
            objDir1.Telco_Provider__c = objTelco1.Id;
            objDir1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDir1.Publication_Company__c = newAccount1.Id;
            objDir1.Division__c = objDiv1.Id;
            objDir1.Directory_Code__c = '100000';
            insert objDir1;
            
            Directory_Heading__c objDH1 = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS1 = TestMethodsUtility.createDirectorySection(objDir1);
            Section_Heading_Mapping__c objSHM1 = TestMethodsUtility.generateSectionHeadingMapping(objDS1.Id, objDH1.Id);
            insert objSHM1;
            
            Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir1);
            objDirEd1.New_Print_Bill_Date__c=date.today();
            objDirEd1.Bill_Prep__c=date.parse('01/01/2013');
            objDirEd1.XML_Output_Total_Amount__c=100;
            objDirEd1.Pub_Date__c =Date.today().addMonths(1);
            insert objDirEd1;
            
            Directory_Edition__c objDirE1 = new Directory_Edition__c();
            objDirE1.Name = 'Test DirE1';
            objDirE1.Directory__c = objDir1.Id;
            objDirE1.Letter_Renewal_Stage_1__c = system.today();
            objDirE1.Sales_Lockout__c=Date.today().addDays(30);
            objDirE1.book_status__c='NI';
            objDirE1.Pub_Date__c =Date.today().addMonths(2);
            insert objDirE1;
            
            Directory_Edition__c objDirE2 = new Directory_Edition__c();
            objDirE2.Name = 'Test DirE2';
            objDirE2.Directory__c = objDir1.Id;
            objDirE2.Letter_Renewal_Stage_1__c = system.today();
            objDirE2.Sales_Lockout__c=Date.today().addDays(30);
            objDirE2.book_status__c='BOTS';
            objDirE2.Pub_Date__c =Date.today().addMonths(3);
            insert objDirE2;     
            
            Directory__c objDirNew1 = TestMethodsUtility.generateDirectory();
            objDirNew1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDirNew1.Publication_Company__c = newAccount1.Id;
            objDirNew1.Division__c = objDiv1.Id;
            objDirNew1.Directory_Code__c = '100001';
            insert objDirNew1;
            Directory_Edition__c objDirEdNew2 = TestMethodsUtility.generateDirectoryEdition(objDirNew1);
            objDirEdNew2.XML_Output_Total_Amount__c=200;
            objDirEdNew2.Pub_Date__c =Date.today().addMonths(4);
            insert objDirEdNew2;
            
            Product2 newProduct3 = TestMethodsUtility.generateproduct();
            newProduct3.Family = 'Print';
            insert newProduct3;
            
            Product2 objProd3 = new Product2();
            objProd3.Name = 'Test';
            objProd3.Product_Type__c = 'Print';
            objProd3.ProductCode = 'WLCSH';
            objProd3.Print_Product_Type__c='Display';
            insert  objProd3;
            
            Product2 objProd4 = new Product2();
            objProd4.Name = 'Test';
            objProd4.Product_Type__c = 'Print';
            objProd4.ProductCode = 'GC50';
            objProd4.Print_Product_Type__c='Specialty';          
            insert objProd4;
            
            Opportunity newOpportunity2 = TestMethodsUtility.generateOpportunity('new');
            newOpportunity2.AccountId = newAccount1.Id;
            newOpportunity2.Pricebook2Id = newPriceBook1.Id;
            insert newOpportunity2;
            
            Order__c newOrder1 = TestMethodsUtility.createOrder(newAccount1.Id);
            
            Order_Group__c newOrderSet1 = TestMethodsUtility.createOrderSet(newAccount1, newOrder1, newOpportunity2);
            
          
            c2g__codaCompany__c companyObj =TestMethodsUtility.generateCompany();
            
            Order_Line_Items__c objOrderLineItemStmtOdd = new Order_Line_Items__c(Billing_Partner__c='Berry123',Account__c=newAccount1.Id, 
            Billing_Contact__c=newContact1.id, Opportunity__c=newOpportunity2.id, Order_Group__c=newOrderSet1.id,
            Order__c=newOrder1.id,Product2__c=objProd4.Id,is_p4p__c=true,media_type__c='Print',
            Directory_Edition__c = objDirEdNew2 .Id,Directory__c=objDirNew1.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Statement',
            Package_ID__c='pkgid_12',Payments_Remaining__c=10,Successful_Payments__c=2);
            insert objOrderLineItemStmtOdd;
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount1,newOpportunity2);
            invoice.Customer_Name__c=newAccount1.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.c2g__OwnerCompany__c = companyObj.id;
            invoice.c2g__Account__c= newAccount1.id;
            invoice.Is_Statement_Generated__c=false;
            insert invoice;
            
           
            
            c2g__codaCreditNote__c SCNP4PSTMT=TestMethodsUtility.generateSalesCreditNote(invoice, newAccount1);
            SCNP4PSTMT.Transaction_Type__c='TD - Billing Transfer Invoice';
            SCNP4PSTMT.c2g__CreditNoteStatus__c='In Progress';
            SCNP4PSTMT.Is_Statement_Generated__c=false;
            SCNP4PSTMT.SC_Media_Type__c ='Print';
            SCNP4PSTMT.Transaction_Type__c='Statement';
            SCNP4PSTMT.c2g__CreditNoteDate__c =date.today();
            SCNP4PSTMT.SC_P4P__c=false;
            insert SCNP4PSTMT;
            
            c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDirNew1);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd4);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            c2g__codaDimension3__c  dimension3_ss=TestMethodsUtility.createDimension3(objOrderLineItemStmtOdd);
            c2g__codaInvoiceLineItem__c invoiceLiStmt=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItemStmtOdd,invoice,objProd4,dimension1,dimension2,dimension3_ss,dimension4 );
       
            c2g__codaCreditNoteLineItem__c scliStmt=TestMethodsUtility.generateSalesCreditNoteLineItem(SCNP4PSTMT,objProd4);
            scliStmt.Sales_Invoice_Line_Item__c=invoiceLiStmt.id;
            scliStmt.c2g__Dimension1__c=dimension1.id;
            scliStmt.c2g__Dimension2__c=dimension2.id;
            scliStmt.c2g__Dimension3__c= dimension3_ss.id;
            scliStmt.c2g__Dimension4__c=dimension4.id;
            scliStmt.Billing_Frequency__c='Monthly';
            insert scliStmt;
         
            //Dummy_Object__c objDummy= new Dummy_Object__c(Name='Test',Date__c=date.today(),From_Date__c = date.newinstance(2012, 3, 17),To_Date__c =date.newinstance(2015, 1, 16));
            Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today(),From_Date__c = date.today(),To_Date__c =date.newinstance(2015, 1, 16));

            insert objDummy;
            
            
            
            Test.startTest();
                 List<c2g__codaCreditNote__c> ListSCN=new List<c2g__codaCreditNote__c>{SCNP4PSTMT};
                LocalBillingSCNPostOrReconciliationBatch lbSCN=new LocalBillingSCNPostOrReconciliationBatch(objDummy,'post');
                Database.BatchableContext bc;
                lbSCN.start(bc);
                lbSCN.execute(bc,ListSCN);
                
            Test.stopTest();
  }
  
  public static testMethod void TestSCNReconcileBatch(){
             Canvass__c c=TestMethodsUtility.createCanvass();
            list<Account> lstAccount1 = new list<Account>();
            lstAccount1.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount1[0].Billing_Anniversary_Date__c=date.today();
            insert lstAccount1;  
            Account newAccount1 = new Account();
            for(Account iterator : lstAccount1) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount1 = iterator;
                }
           }
            Telco__c objTelco1 =TestMethodsUtility.createTelco(newAccount1.Id);
            objTelco1.Telco_Code__c = 'Test';
            update objTelco1;
            system.assertNotEquals(newAccount1.ID, null);
            Contact newContact1 = TestMethodsUtility.createContact(newAccount1.Id);
            Pricebook2 newPriceBook1 = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv1 = TestMethodsUtility.createDivision();
        
            Directory__c objDir1 = TestMethodsUtility.generateDirectory();
            objDir1.Telco_Recives_Electronice_File__c=true;
            objDir1.Telco_Provider__c = objTelco1.Id;
            objDir1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDir1.Publication_Company__c = newAccount1.Id;
            objDir1.Division__c = objDiv1.Id;
            objDir1.Directory_Code__c = '100000';
            insert objDir1;
            
            Directory_Heading__c objDH1 = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS1 = TestMethodsUtility.createDirectorySection(objDir1);
            Section_Heading_Mapping__c objSHM1 = TestMethodsUtility.generateSectionHeadingMapping(objDS1.Id, objDH1.Id);
            insert objSHM1;
            
            Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir1);
            objDirEd1.New_Print_Bill_Date__c=date.today();
            objDirEd1.Bill_Prep__c=date.parse('01/01/2013');
            objDirEd1.XML_Output_Total_Amount__c=100;
            objDirEd1.Pub_Date__c =Date.today().addMonths(1);
            insert objDirEd1;
            
            Directory_Edition__c objDirE1 = new Directory_Edition__c();
            objDirE1.Name = 'Test DirE1';
            objDirE1.Directory__c = objDir1.Id;
            objDirE1.Letter_Renewal_Stage_1__c = system.today();
            objDirE1.Sales_Lockout__c=Date.today().addDays(30);
            objDirE1.book_status__c='NI';
            objDirE1.Pub_Date__c =Date.today().addMonths(2);
            insert objDirE1;
            
            Directory_Edition__c objDirE2 = new Directory_Edition__c();
            objDirE2.Name = 'Test DirE2';
            objDirE2.Directory__c = objDir1.Id;
            objDirE2.Letter_Renewal_Stage_1__c = system.today();
            objDirE2.Sales_Lockout__c=Date.today().addDays(30);
            objDirE2.book_status__c='BOTS';
            objDirE2.Pub_Date__c =Date.today().addMonths(3);
            insert objDirE2;     
            
            Directory__c objDirNew1 = TestMethodsUtility.generateDirectory();
            objDirNew1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDirNew1.Publication_Company__c = newAccount1.Id;
            objDirNew1.Division__c = objDiv1.Id;
            objDirNew1.Directory_Code__c = '100001';
            insert objDirNew1;
            Directory_Edition__c objDirEdNew2 = TestMethodsUtility.generateDirectoryEdition(objDirNew1);
            objDirEdNew2.XML_Output_Total_Amount__c=200;
            objDirEdNew2.Pub_Date__c =Date.today().addMonths(4);
            insert objDirEdNew2;
            
            Product2 newProduct3 = TestMethodsUtility.generateproduct();
            newProduct3.Family = 'Print';
            insert newProduct3;
            
            Product2 objProd3 = new Product2();
            objProd3.Name = 'Test';
            objProd3.Product_Type__c = 'Print';
            objProd3.ProductCode = 'WLCSH';
            objProd3.Print_Product_Type__c='Display';
            insert  objProd3;
            
            Product2 objProd4 = new Product2();
            objProd4.Name = 'Test';
            objProd4.Product_Type__c = 'Print';
            objProd4.ProductCode = 'GC50';
            objProd4.Print_Product_Type__c='Specialty';          
            insert objProd4;
            
            Opportunity newOpportunity2 = TestMethodsUtility.generateOpportunity('new');
            newOpportunity2.AccountId = newAccount1.Id;
            newOpportunity2.Pricebook2Id = newPriceBook1.Id;
            insert newOpportunity2;
            
            Order__c newOrder1 = TestMethodsUtility.createOrder(newAccount1.Id);
            
            Order_Group__c newOrderSet1 = TestMethodsUtility.createOrderSet(newAccount1, newOrder1, newOpportunity2);
            
          
            c2g__codaCompany__c companyObj =TestMethodsUtility.generateCompany();
            
            Order_Line_Items__c objOrderLineItemStmtOdd = new Order_Line_Items__c(Billing_Partner__c='Berry123',Account__c=newAccount1.Id, 
            Billing_Contact__c=newContact1.id, Opportunity__c=newOpportunity2.id, Order_Group__c=newOrderSet1.id,
            Order__c=newOrder1.id,Product2__c=objProd4.Id,is_p4p__c=false,media_type__c='Print',
            Directory_Edition__c = objDirEdNew2 .Id,Directory__c=objDirNew1.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Statement',
            Package_ID__c='pkgid_12',Payments_Remaining__c=10,Successful_Payments__c=2);
            insert objOrderLineItemStmtOdd;
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount1,newOpportunity2);
            invoice.Customer_Name__c=newAccount1.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.c2g__OwnerCompany__c = companyObj.id;
            invoice.c2g__Account__c= newAccount1.id;
            invoice.Is_Statement_Generated__c=false;
            insert invoice;
            
           
            
            c2g__codaCreditNote__c SCNP4PSTMT=TestMethodsUtility.generateSalesCreditNote(invoice, newAccount1);
            SCNP4PSTMT.Transaction_Type__c='TD - Billing Transfer Invoice';
            SCNP4PSTMT.c2g__CreditNoteStatus__c='In Progress';
            SCNP4PSTMT.Is_Statement_Generated__c=false;
            SCNP4PSTMT.SC_Media_Type__c ='Print';
            SCNP4PSTMT.Transaction_Type__c='Statement';
            SCNP4PSTMT.c2g__CreditNoteDate__c =date.today();
            SCNP4PSTMT.SC_P4P__c=false;
            insert SCNP4PSTMT;
            
            c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDirNew1);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd4);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            c2g__codaDimension3__c  dimension3_ss=TestMethodsUtility.createDimension3(objOrderLineItemStmtOdd);
            
            c2g__codaInvoiceLineItem__c invoiceLiStmt=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItemStmtOdd,invoice,objProd4,dimension1,dimension2,dimension3_ss,dimension4 );
       
            c2g__codaCreditNoteLineItem__c scliStmt=TestMethodsUtility.generateSalesCreditNoteLineItem(SCNP4PSTMT,objProd4);
            scliStmt.Sales_Invoice_Line_Item__c=invoiceLiStmt.id;
            scliStmt.c2g__Dimension1__c=dimension1.id;
            scliStmt.c2g__Dimension2__c=dimension2.id;
            scliStmt.c2g__Dimension3__c= dimension3_ss.id;
            scliStmt.c2g__Dimension4__c=dimension4.id;
            scliStmt.Billing_Frequency__c='Monthly';
            insert scliStmt;
         
             List<c2g__codaCreditNote__c> ListSCN=new List<c2g__codaCreditNote__c>{SCNP4PSTMT};
            //Dummy_Object__c objDummy= new Dummy_Object__c(Name='Test',Date__c=date.today(),From_Date__c = date.newinstance(2012, 3, 17),To_Date__c =date.newinstance(2015, 1, 16));
            Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today(),From_Date__c = date.today(),To_Date__c =date.newinstance(2015, 1, 16));

            insert objDummy;
            
            
            
            Test.startTest();
                LocalBillingSCNPostOrReconciliationBatch lbSCN=new LocalBillingSCNPostOrReconciliationBatch(objDummy,'reconcile');
               Database.BatchableContext bc;
                lbSCN.start(bc);
                lbSCN.execute(bc,ListSCN);
                
            Test.stopTest();
  }
  
  public static testMethod void TestSIReconcileBatch1(){
             Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today(),From_Date__c = date.today(),To_Date__c =date.newinstance(2015, 1, 16));

            insert objDummy;
            
            Test.startTest();
                LocalBillingSIPostOrReconciliationBatch  lbSCN=new LocalBillingSIPostOrReconciliationBatch (objDummy,'reconcile');
                Id  SCNBatchId=Database.executeBatch(lbSCN,1);
                
            Test.stopTest();
  }
  public static testMethod void TestSIReconcileBatch2(){
            Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today(),From_Date__c = date.today(),To_Date__c =date.newinstance(2015, 1, 16));

            insert objDummy;
            
            Test.startTest();
                LocalBillingSIPostOrReconciliationBatch  lbSCN=new LocalBillingSIPostOrReconciliationBatch (objDummy,'reconcile');
                Id  SCNBatchId=Database.executeBatch(lbSCN,1);
                
            Test.stopTest();
  }
  public static testMethod void TestSCNReconcileBatch1(){
             Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today(),From_Date__c = date.today(),To_Date__c =date.newinstance(2015, 1, 16));

            insert objDummy;
            
            Test.startTest();
                LocalBillingSCNPostOrReconciliationBatch lbSCN=new LocalBillingSCNPostOrReconciliationBatch(objDummy,'reconcile');
                Id  SCNBatchId=Database.executeBatch(lbSCN,1);
                
            Test.stopTest();
  }
  public static testMethod void TestSCNReconcileBatch2(){
             Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today(),From_Date__c = date.today(),To_Date__c =date.newinstance(2015, 1, 16));

            insert objDummy;
            
            Test.startTest();
                LocalBillingSCNPostOrReconciliationBatch lbSCN=new LocalBillingSCNPostOrReconciliationBatch(objDummy,'poat');
                Id  SCNBatchId=Database.executeBatch(lbSCN,1);
                
            Test.stopTest();
  }
}