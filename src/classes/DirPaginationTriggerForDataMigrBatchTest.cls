@isTest
public class DirPaginationTriggerForDataMigrBatchTest {
    
    static testmethod void test_DirPaginationTriggerForDataMigrBatch() {
        Account newTelcoAccount = TestMethodsUtility.generateTelcoAccount();
        insert newTelcoAccount;
        Telco__c newTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        Directory__c objDir = TestMethodsUtility.generateDirectory();  
        objDir.Telco_Provider__c = newTelco.Id;
        insert objDir;
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Directory_Heading__c dirHeading = Testmethodsutility.generateDirectoryHeading(); 
        dirHeading.Directory_Heading_Name__c=Testmethodsutility.generateRandomString(100);
        insert dirHeading;
        Directory_Heading__c dirHeading1 = Testmethodsutility.generateDirectoryHeading(); 
        dirHeading1.Directory_Heading_Name__c=Testmethodsutility.generateRandomString(101);
        insert dirHeading1;
        Listing__c  listing = Testmethodsutility.createListing('Main Listing');
        Directory_Listing__c DL1 = Testmethodsutility.generateDirectoryListing();
        DL1.Directory__c=objDir.Id;
        DL1.Listing__c=listing.Id;
        DL1.Directory_Section__c=objDS.Id;
        DL1.Directory_Heading__c = dirHeading.Id;
        DL1.Telco_Provider__c = newTelco.Id;
        DL1.DL_DM_isTriggerExecuted__c=false;
        insert DL1;
        Directory_Pagination__c dirPag=new Directory_Pagination__c();
       //   dirPag.Name='test';
        dirPag.Reference_Listing__c=DL1.Id;
        List<Directory_Pagination__c> dirPagList=new List<Directory_Pagination__c>();
        dirPagList.add(dirPag);
        insert dirPagList;
        Database.executeBatch(new DirPaginationTriggerForDataMigrBatch('where id!=null'));
        
    }

}