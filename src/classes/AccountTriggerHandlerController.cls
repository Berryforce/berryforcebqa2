public class AccountTriggerHandlerController {
    //$Id$
    public static void onBeforeInsert(list<Account> lstNewAccount) {
        map<String, Id> RecordTypeIdName = CommonMethods.getsObjectRecordTypeNameKeyset(CommonMessages.accountObjectName);
        prepopulateValuesonAccountFields(lstNewAccount, null, RecordTypeIdName);
    }
    
    public static void onAfterInsert(list<Account> lstNewAccount, map<Id, Account> mapNewAccount) {
        map<String, Id> RecordTypeIdName = CommonMethods.getsObjectRecordTypeNameKeyset(CommonMessages.accountObjectName);
        createORUpdateFulfillmentProfile(lstNewAccount, null, mapNewAccount, RecordTypeIdName);
        if(!CommonMethods.skipTriggerMethodLogic(CommonMessages.accountObjectName)) {
            setDelinquencyIndicator(lstNewAccount, null);
        }
    }
    
    public static void onBeforeUpdate(list<Account> lstNewAccount, map<Id, Account> oldMap) {
        map<String, Id> RecordTypeIdName = CommonMethods.getsObjectRecordTypeNameKeyset(CommonMessages.accountObjectName);
        prepopulateValuesonAccountFields(lstNewAccount, oldMap, RecordTypeIdName);
    }
    
    public static void onAfterUpdate(list<Account> lstNewAccount, map<Id, Account> oldMap, map<Id, Account> mapNewAccount) {
        map<String, Id> RecordTypeIdName = CommonMethods.getsObjectRecordTypeNameKeyset(CommonMessages.accountObjectName);
        boolean bFlag = CommonMethods.skipTriggerMethodLogic(CommonMessages.accountObjectName);
        if(!bFlag) {
            statementSuppressionUpdate(lstNewAccount, oldMap);
        }
        createORUpdateFulfillmentProfile(lstNewAccount, oldMap, mapNewAccount, RecordTypeIdName);
        if(!bFlag) {
            setDelinquencyIndicator(lstNewAccount, oldMap);
        }
    }
    
    //Populating National Account, Collection Status, Late Fee Suppression, Delinquency Indicater, statement suppression
    //Populating Account Receivable based on Record Type
    //Populating Account Source and Record type for Converted from Lead.
    private static void prepopulateValuesonAccountFields(list<Account> lstNewAccount,  map<Id, Account> oldAccountMap, map<String, Id> RecordTypeIdName) {
        Boolean skipTrigger=CommonMethods.skipTriggerMethodLogic(CommonMessages.accountObjectName);         
        set<Id> setRTID = new set<Id>();
        for(String RT : RecordTypeIdName.keySet()) {
            if(RT.equals(CommonMessages.accountCMRRT) || RT.equals(CommonMessages.accountCMRNet) || RT.equals(CommonMessages.accountNationalActRT)) {
                setRTID.add(RecordTypeIdName.get(RT));
            }
        }
        
        //Below vaiables for DoCustomerPartner population
        Id customerRT = RecordTypeIdName.get(CommonMessages.accountCustomerRT);
        set<Id> setAccIds = new set<Id>();
        set<Id> setCanvassIds = new set<Id>();
        
        for(Account iterator : lstNewAccount) { 
            if(setRTID.contains(iterator.RecordTypeId)) {
                iterator.National_Account__c = true;
            }
            
            if(iterator.National_Account__c == true) {
                iterator.Collection_Status__c = '';
            }
            //System.debug('Collection Stauts is ' + iterator.Collection_Status__c);
            if(iterator.Internal_Account__c == true && iterator.Collection_Status__c == CommonMessages.accountthirdPartyWriteOff) {
                iterator.Collection_Status__c.addError('Collection Status cannot be 3rd Part Write-off when Internal Account is Checked');
            }
            
            if(iterator.Bankruptcy__c == true) {
                iterator.Late_Fee_Suppression__c = true;
                iterator.Delinquency_Indicator__c = true;
                iterator.Statement_Suppression__c = true;
            }
            
            if(iterator.RecordTypeId == RecordTypeIdName.get(CommonMessages.accountCMRRT) || iterator.RecordTypeId == RecordTypeIdName.get(CommonMessages.accountCMRNet) || iterator.RecordTypeId == RecordTypeIdName.get(CommonMessages.accountCustomerRT) || iterator.RecordTypeId == RecordTypeIdName.get(CommonMessages.accountNationalActRT)) {
                iterator.c2g__CODAAccountsReceivableControl__c = Id.valueOf(System.Label.AcctRecForNonTelco);
            } 
            else if(iterator.RecordTypeId == RecordTypeIdName.get(CommonMessages.accountTelcoRT)){
                iterator.c2g__CODAAccountsReceivableControl__c = Id.valueOf(System.Label.AcctRecForTelco);
            }
            
            if(oldAccountMap == null) {
                //populating the lead source value to account source field from Lead (While convering from lead to account)
                if(String.isNotEmpty(iterator.Lead_Source__c)) {
                    iterator.AccountSource = iterator.Lead_Source__c;
                }
                //Populating customer RT
                if(iterator.Conversion_Status__c == 'Yes') {
                    iterator.RecordTypeId = RecordTypeIdName.get(CommonMessages.accountCustomerRT);
                }
                
                //DoCustomerPartner population logic
                //if(iterator.RecordTypeId != telcoRT) {
                system.debug('Customer RT : '+customerRT +' --> '+ iterator.RecordTypeId);
                 if(iterator.RecordTypeId == customerRT && !skipTrigger) {
                    if(iterator.Primary_Canvass__c != null && iterator.Telco_Partner__c == null) {
                        setAccIds.add(iterator.Id);
                        setCanvassIds.add(iterator.Primary_Canvass__c);
                    }
                }
                if(!skipTrigger){
                    if(iterator.Collection_Status__c == CommonMessages.accountFistPartyBulk || iterator.Bankruptcy__c) { // Add DM Skip 
                        iterator.RecordTypeId = RecordTypeIdName.get(CommonMessages.accountBulkBillRT);
            			iterator.Old_Record_Type_Id_for_Bulk_Bill__c = RecordTypeIdName.get(CommonMessages.accountCustomerRT);
                    }
                }
                iterator.Last_Modification_Date__c = Date.Today();           
            }
            else {
                Account objOldAccount = oldAccountMap.get(iterator.Id);
                
                //DoCustomerPartner population logic
                //if(iterator.RecordTypeId != telcoRT) {
                system.debug('Customer RT : '+customerRT +' --> '+ iterator.RecordTypeId);
                if(iterator.RecordTypeId == customerRT && !skipTrigger) {
                    if(iterator.Primary_Canvass__c != objOldAccount.Primary_Canvass__c ) {
                        if(iterator.Primary_Canvass__c != null) {
                            setAccIds.add(iterator.Id);
                            setCanvassIds.add(iterator.Primary_Canvass__c);
                        }
                    }
                }
                    
                if(iterator.Collection_Status__c != objOldAccount.Collection_Status__c) {
                    iterator.Last_Modification_Date__c = Date.Today();                          
                }
                 // Add DM Skip
                if(!skipTrigger) {
                    if((iterator.Collection_Status__c != objOldAccount.Collection_Status__c && iterator.Collection_Status__c == CommonMessages.accountFistPartyBulk) || (iterator.Bankruptcy__c != objOldAccount.Bankruptcy__c && iterator.Bankruptcy__c)) {                
                        iterator.Old_Record_Type_Id_for_Bulk_Bill__c = RecordTypeIdName.get(CommonMessages.accountCustomerRT);            
                        iterator.RecordTypeId = RecordTypeIdName.get(CommonMessages.accountBulkBillRT);
                    }
                }
            }
        }
        
        if(setAccIds.size() > 0) {
            doCustomerAccountsNew(lstNewAccount, setAccIds, setCanvassIds);
        }
    }
    
    public static void statementSuppressionUpdate(list<Account> lstNewAccount, map<Id, Account> oldMap) {
        Set<ID> setAccIDs = new Set<ID>();
        
        for(Account iterator : lstNewAccount) {
            if(oldMap != null) {
                Account objOldAcc = oldMap.get(iterator.Id);
                if(iterator.Bankruptcy__c == true && objOldAcc.Bankruptcy__c == false) {
                    setAccIDs.add(iterator.Id);
                }
            }
        }
        
        if(setAccIDs.size() > 0) {
            list<Contact> lstContact = ContactSOQLMethods.getContactStmtSuppressionByAccountID(setAccIDs);
            if(lstContact.size() > 0) {
                for(Contact Con : lstContact) {
                    Con.Statement_Suppression__c = true;
                }
                update lstContact;
            }
        }
    }
    
    private static void doCustomerAccountsNew(list<Account> lstNewAccount, set<Id> setAccIds, set<Id> setCanvassIds) {
        if(setAccIds.size() > 0) {
            map<Id, list<Directory_mapping__c>> mapDirectoryListing = new map<Id, list<Directory_Mapping__c>>();
            //Fech the Direcotry listing by primary canvass
            list<Directory_Mapping__c> lstDirectoryListing = DirectoryListingSOQLMethods.getDirectoryListingByCanvassID(setCanvassIds);
            for(Directory_Mapping__c iteratorDL : lstDirectoryListing) {
                //One canvass have multiple direcory listing, So using collection to store the data's
                if(!mapDirectoryListing.Containskey(iteratorDL.Canvass__c)) {
                    mapDirectoryListing.put(iteratorDL.Canvass__c, new list<Directory_Mapping__c>());
                }
                mapDirectoryListing.get(iteratorDL.Canvass__c).add(iteratorDL);
            }
            
            if(mapDirectoryListing.size() > 0) {
                for(Account iterator : lstNewAccount) {
                    if(setAccIds.contains(iterator.Id)) {
                        list<Directory_Mapping__c> lstDirectoryListingByCanvass = mapDirectoryListing.get(iterator.Primary_Canvass__c);
                        if(lstDirectoryListingByCanvass != null && lstDirectoryListingByCanvass.size() > 0) {
                            iterator.Telco_Partner__c = lstDirectoryListingByCanvass[0].Telco__r.Account__c; 
                        }
                        else {
                            iterator.Telco_Partner__c = null;
                        }
                    }
                }
            }
        }
    }

    public static void createORUpdateFulfillmentProfile(list<Account> lstNewAccount, map<Id, Account> mapOldAccount, map<Id, Account> mapNewAccount, map<String, Id> RecordTypeIdName) {
        Id accCusRTId = RecordTypeIdName.get(CommonMessages.accountCustomerRT);
        set<Id> setAccId = new set<Id>();
        for(Account iterator : lstNewAccount) {
            if(iterator.RecordTypeId == accCusRTId) {
                if(mapOldAccount != null) {
                    Account objOldAccount = mapOldAccount.get(iterator.Id);
                    if(iterator.Name != objOldAccount.Name || iterator.Phone != objOldAccount.Phone || iterator.Billingcity != objOldAccount.Billingcity 
                        || iterator.BillingState != objOldAccount.BillingState || iterator.BillingStreet != objOldAccount.BillingStreet || iterator.BillingPostalCode != objOldAccount.BillingPostalCode 
                        || iterator.Website != objOldAccount.Website) {
                        setAccId.add(iterator.Id);
                    }
                }
                else {
                    setAccId.add(iterator.Id);
                }
            }
        }
        
        if(setAccId.size() > 0) {
            if(mapOldAccount != null) {
                updateFulfillmentProfile(setAccId, mapNewAccount);
            }
            else {
                createFulfillmentProfile(setAccId, mapNewAccount);
            }
        }
    }
    
    public static void createFulfillmentProfile(Set<Id> setAccId, map<Id, Account> mapNewAccount) {
        map<String, Id> RecordTypeIdNameFP = CommonMethods.getsObjectRecordTypeNameKeyset(CommonMessages.FPObjectName);
        Id FPRTId = RecordTypeIdNameFP.get(CommonMessages.fProfileRT);
        list<Fulfillment_Profile__c> lstFPInsert = new list<Fulfillment_Profile__c>();
        Schema.DescribeFieldResult F = Fulfillment_Profile__c.target_gender__c.getDescribe();
        list<Schema.PicklistEntry> pickVals = F.getPicklistValues();
        String defPickListValTarget_gender = null;
        String defPickListValOmit_phone_ind = null;
        for (Schema.PicklistEntry pv: pickVals) {
            if (pv.isDefaultValue()) {
                defPickListValTarget_gender = pv.getValue();
            }
        }
        
        Schema.DescribeFieldResult F1 = Fulfillment_Profile__c.omit_phone_ind__c.getDescribe();
        list<Schema.PicklistEntry> pickVals1 = F1.getPicklistValues();
        for (Schema.PicklistEntry pv: pickVals1) {
            if (pv.isDefaultValue()) {
                defPickListValOmit_phone_ind = pv.getValue();
            }
        }
        
        for(Id accountID : setAccId) {
            Account objNewAccount = mapNewAccount.get(accountID);
            String myString = objNewAccount.Name;
            if(myString.length()>64) {
               myString = myString.substring(0, 64);
            }
            
            lstFPInsert.add(new Fulfillment_Profile__c(Account__c=objNewAccount.id, Name='Main ' + myString  + ' Profile', Default_Profile__c=true, Automatically_Sync__c=true,  
                                business_phone_number_office__c=objNewAccount.Phone, business_city__c=objNewAccount.Billingcity, business_address1__c=objNewAccount.BillingStreet, 
                                business_name__c=objNewAccount.Name, business_state__c=objNewAccount.BillingState, business_postal_code__c=objNewAccount.BillingPostalCode,    
                                business_url__c=objNewAccount.Website, RecordTypeId=FPRTId 
                                //Advertiser_license_type__c=1, Budget_AllowBalanceRollover__c=true, budget_cpa__c=0, budget_cpc__c=0, 
                                //budget_cpm__c=1, budget_dailyspendlimit__c=0, budget_type__c=1, color_scheme__c='Full', duration_continuous__c=true, is_legacy__c=false, 
                                //is_open_247__c=false, mobile_opt_out__c=false, Omit_Address_Description__c='Show Full Address', Omit_Address__c=false, omit_phone_ind__c='N', 
                                //opt_in_domain_email__c=false, opt_out_local_profile__c=false, send_alerts__c=true, send_reports__c=true, send_tutorials__c=true, 
                                //Show_Location_on_LP__c=true, Show_Map_on_LP__c=false, show_operating_hours__c=true, signup_source__c=1, signup_url__c='http://www.theberrycompany.com', 
                                //sms_short_code__c=24000, target_gender__c=defPickListValTarget_gender, web_page_to_ads_enabled__c=false 
                                ));
            }
        
        if(lstFPInsert.size() > 0) {
            insert lstFPInsert;
        }
    }
    
    private static void updateFulfillmentProfile(set<Id> setAccId, map<Id, Account> mapNewAccount) {
        if(setAccId.size() > 0) {
            list<Fulfillment_Profile__c> lstFProfile = FulfillmentProfileSOQLMethods.getFulfillmentProfilebyAccountIdForUpdatePF(setAccId);
            for(Fulfillment_Profile__c fpIterator : lstFProfile) {
                Account objNewAccount = mapNewAccount.get(fpIterator.Account__c);
                fpIterator.Default_Profile__c = true;
                fpIterator.Automatically_Sync__c = true;
                fpIterator.business_name__c = objNewAccount.Name;
                fpIterator.business_phone_number_office__c = objNewAccount.Phone;
                if(objNewAccount.Billingcity != null){
                    fpIterator.business_city__c = objNewAccount.Billingcity;
                }
                if(objNewAccount.BillingState != null){
                    fpIterator.business_state__c = objNewAccount.BillingState;
                }
                if(objNewAccount.BillingStreet != null){
                    fpIterator.business_address1__c = objNewAccount.BillingStreet;
                }
                if(objNewAccount.BillingPostalCode != null){
                    fpIterator.business_postal_code__c = objNewAccount.BillingPostalCode;
                }
                fpIterator.business_url__c = objNewAccount.Website;
            }
            
            if(lstFProfile.size() > 0) {
                update lstFProfile;
            }
        }        
    }
    /*
        This method is used to set Delinquency Indicator to true to parent and child accounts
    */   
    public static void setDelinquencyIndicator(list<Account> listAccount, map<Id, Account> acctOldMap) {
        //if(CommonVariables.AccountRepeatCheck) {
           // system.debug('Inside Delinquency');
            //CommonVariables.AccountRepeatCheck = false;
            set<Id> setCurrentAcctIds = new set<Id>();
            set<Id> setAccountIds = new set<Id>();
            for(Account acct : listAccount) {
                if(acctOldMap == null) {
                    if(acct.Delinquency_Indicator__c) {
                        setCurrentAcctIds.add(acct.Id);
                        if(acct.ParentId != null) {
                            setAccountIds.add(acct.ParentId);
                        }
                        setAccountIds.add(acct.Id);
                    }
                }
                else {
                    Account oldAcct = acctOldMap.get(acct.Id);
                    if(acct.Delinquency_Indicator__c && !oldAcct.Delinquency_Indicator__c) {
                        setCurrentAcctIds.add(acct.Id);
                        if(acct.ParentId != null) {
                            setAccountIds.add(acct.ParentId);                            
                        }
                        setAccountIds.add(acct.Id);
                    }
                }
            }
            if(setAccountIds.size() > 0) {
                list<Account> listFetchAccount = AccountSOQLMethods.getSetDelinquencyIndicatorAccountByParentID(setAccountIds);
                list<Account> lstUpdateAccount = new list<Account>();
                for(Account acct : listFetchAccount) {
                    if(!setCurrentAcctIds.contains(acct.Id)) {
                        acct.Delinquency_Indicator__c = true;
                        lstUpdateAccount.add(acct);
                    }
                }
                if(lstUpdateAccount.size() > 0) {
                    update lstUpdateAccount;
                }
            }
        //}
    }
}