@isTest(SeeAlldata=true)
public class LocalBillingSCNPostOrReconciliationTest{

    public static testMethod void TestLocalBillingSCNReconciliation(){
            Canvass__c c=TestMethodsUtility.createCanvass();
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('telco'));
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount.add(TestMethodsUtility.generateAccount('publication'));
            insert lstAccount;  
            Account newAccount = new Account();
            Account newPubAccount = new Account();
            Account newTelcoAccount = new Account();
            for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
            newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
            newPubAccount = iterator;
            }
            else {
            newTelcoAccount = iterator;
            }
            }
            Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
            objTelco.Telco_Code__c = 'Test';
            update objTelco;
            system.assertNotEquals(newTelcoAccount.ID, null);
            Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
            Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv = TestMethodsUtility.createDivision();
        
            Directory__c objDir = TestMethodsUtility.generateDirectory();
            objDir.Telco_Recives_Electronice_File__c=true;
            objDir.Telco_Provider__c = objTelco.Id;
            objDir.Canvass__c = newAccount.Primary_Canvass__c;        
            objDir.Publication_Company__c = newPubAccount.Id;
            objDir.Division__c = objDiv.Id;
            objDir.Directory_Code__c = '100000';
            insert objDir;
            
            Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
            Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
            insert objSHM;
            
            Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
            objDirEd .New_Print_Bill_Date__c=date.today();
            objDirEd .Bill_Prep__c=date.today().adddays(4);
            objDirEd.XML_Output_Total_Amount__c=100;
            objDirEd.Pub_Date__c =Date.today();
            insert objDirEd;
            
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Family = 'Print';
            insert newProduct;
            
            Product2 objProd = new Product2();
            objProd.Name = 'Test';
            objProd.Product_Type__c = 'Print';
            objProd.ProductCode = 'WLCSH';
            objProd.Print_Product_Type__c='Display';
            insert  objProd;
            
            Product2 objProd1 = new Product2();
            objProd1.Name = 'Test';
            objProd1.Product_Type__c = 'Print';
            objProd1.ProductCode = 'GC50';
            objProd1.Print_Product_Type__c='Specialty';          
            insert objProd1;
            
            Test.StartTest();
            Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
            newOpportunity.AccountId = newAccount.Id;
            newOpportunity.Pricebook2Id = newPriceBook.Id;
            insert newOpportunity;
            
            Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
            
            Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
            
            c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDir);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd1);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            
           Date startDate = date.newInstance(2016, 1, 1);
           c2g__codaYear__c year =[select id from c2g__codaYear__c limit 1];
          // c2g__codaYear__c year=new c2g__codaYear__c(Name ='2017', c2g__StartDate__c = startDate, c2g__NumberOfPeriods__c = 12, c2g__PeriodCalculationBasis__c = 'Month' );
          // insert year;
           c2g__codaPeriod__c period=[select id from c2g__codaPeriod__c limit 1 ];
          // c2g__codaPeriod__c period=new c2g__codaPeriod__c(Name = TestMethodsUtility.generateRandomString(16), c2g__YearName__c = year.Id );
           //insert period;
            
            Date currentdt=Date.today();
            String currentYear=String.valueof(currentdt.Year()).substring(2,4);
            String currentMonth=String.valueOf(currentdt.month());
            if(currentMonth.length()==1) currentMonth='0'+currentMonth;
            String DECode=currentYear+currentMonth;
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount,newOpportunity);
            invoice.Customer_Name__c=newAccount.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.SI_Successful_Payments__c=1;
            invoice.SI_P4P__c=false;
            invoice.SI_Media_Type__c='Print';
            invoice.SI_Payments_Remaining__c=11;
            invoice.SI_Directory_Edition_Code__c=DECode;
            invoice.SI_Directory_Code__c=objDir.Directory_Code__c;
            invoice.SI_Billing_Partner__c='THE BERRY COMPANY';
            invoice.SI_Payment_Method__c='Telco Billing';
            insert invoice;
            
            c2g__codaCreditNote__c SCN=TestMethodsUtility.generateSalesCreditNote(invoice, newAccount);
             SCN.Customer_Name__c=newAccount.id;
             SCN.c2g__CreditNoteStatus__c ='In Progress';
             SCN.SC_P4P__c =false;
             SCN.SC_Media_Type__c ='Print';
             SCN.Transaction_Type__c ='FC - Frequency Change';
             SCN.SC_Payment_Method__c ='Telco Billing';
             SCN.c2g__CreditNoteDate__c=Date.today();
            insert SCN;
            
            //Telco Billing
            Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
            Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
            Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
            Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1);
            insert objOrderLineItem;
            
            c2g__codaDimension3__c dimension3=TestMethodsUtility.createDimension3(objOrderLineItem);
            
            c2g__codaInvoiceLineItem__c invoiceLi= new c2g__codaInvoiceLineItem__c(Order_Line_Item__c =objOrderLineItem.Id,c2g__Quantity__c = 1, c2g__UnitPrice__c = 100, c2g__Invoice__c = invoice.Id, c2g__Product__c = objProd1.Id,c2g__Dimension1__c=dimension1.id,c2g__Dimension2__c=dimension2.id,c2g__Dimension3__c= dimension3.id,c2g__Dimension4__c=dimension4.id);
            invoiceLi.Directory_Edition__c=objDirEd.Id;
            invoiceLi.Directory__c=Objdir.Id;
            invoiceLi.Billing_Frequency__c='Monthly';
            insert invoiceLi;
       
            //Berry Billing Partner
            Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Billing_Partner__c='Berry',
            Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
            Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
            Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,
            Payment_Method__c='ACH',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=11,
            Successful_Payments__c=1 );
            insert objOrderLineItem1;
            
            
           // c2g__codaInvoiceLineItem__c invoiceLi1=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItem1,invoice,objProd1,dimension1,dimension2,dimension3,dimension4 );
            
            c2g__codaInvoiceLineItem__c invoiceLi1= new c2g__codaInvoiceLineItem__c(Order_Line_Item__c =objOrderLineItem.Id,c2g__Quantity__c = 1, c2g__UnitPrice__c = 100, c2g__Invoice__c = invoice.Id, c2g__Product__c = objProd1.Id,c2g__Dimension1__c=dimension1.id,c2g__Dimension2__c=dimension2.id,c2g__Dimension3__c= dimension3.id,c2g__Dimension4__c=dimension4.id);
            invoiceLi1.Directory_Edition__c=objDirEd.Id;
            invoiceLi1.Directory__c=Objdir.Id;
            invoiceLi1.Billing_Frequency__c='Monthly';
            insert invoiceLi1;
            
            Test.StopTest();
             
            Dummy_Object__c objDummy =new Dummy_Object__c(From_Date__c=date.today());
            LocalBillingSCNPostOrReconciliationBatch LBSCNbatch=new LocalBillingSCNPostOrReconciliationBatch(objDummy,'reconcile');
            Database.executeBatch(LBSCNbatch);
           
    }
    
    public static testMethod void TestLocalBillingSCNPost(){
            Canvass__c c=TestMethodsUtility.createCanvass();
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('telco'));
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount.add(TestMethodsUtility.generateAccount('publication'));
            insert lstAccount;  
            Account newAccount = new Account();
            Account newPubAccount = new Account();
            Account newTelcoAccount = new Account();
            for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
            newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
            newPubAccount = iterator;
            }
            else {
            newTelcoAccount = iterator;
            }
            }
            Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
            objTelco.Telco_Code__c = 'Test';
            update objTelco;
            system.assertNotEquals(newTelcoAccount.ID, null);
            Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
            Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv = TestMethodsUtility.createDivision();
        
            Directory__c objDir = TestMethodsUtility.generateDirectory();
            objDir.Telco_Recives_Electronice_File__c=true;
            objDir.Telco_Provider__c = objTelco.Id;
            objDir.Canvass__c = newAccount.Primary_Canvass__c;        
            objDir.Publication_Company__c = newPubAccount.Id;
            objDir.Division__c = objDiv.Id;
            objDir.Directory_Code__c = '100000';
            insert objDir;
            
            Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
            Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
            insert objSHM;
            
            Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
            objDirEd .New_Print_Bill_Date__c=date.today();
            objDirEd .Bill_Prep__c=date.today().adddays(4);
            objDirEd.XML_Output_Total_Amount__c=100;
            objDirEd.Pub_Date__c =Date.today();
            insert objDirEd;
            
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Family = 'Print';
            insert newProduct;
            
            Product2 objProd = new Product2();
            objProd.Name = 'Test';
            objProd.Product_Type__c = 'Print';
            objProd.ProductCode = 'WLCSH';
            objProd.Print_Product_Type__c='Display';
            insert  objProd;
            
            Product2 objProd1 = new Product2();
            objProd1.Name = 'Test';
            objProd1.Product_Type__c = 'Print';
            objProd1.ProductCode = 'GC50';
            objProd1.Print_Product_Type__c='Specialty';          
            insert objProd1;
            
            Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
            newOpportunity.AccountId = newAccount.Id;
            newOpportunity.Pricebook2Id = newPriceBook.Id;
            insert newOpportunity;
            
            Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
            
            Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
            
            c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDir);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd1);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            
           Date startDate = date.newInstance(2016, 1, 1);
           c2g__codaYear__c year =[select id from c2g__codaYear__c limit 1];
           c2g__codaPeriod__c period=[select id from c2g__codaPeriod__c limit 1 ];
           
            Date currentdt=Date.today();
            String currentYear=String.valueof(currentdt.Year()).substring(2,4);
            String currentMonth=String.valueOf(currentdt.month());
            if(currentMonth.length()==1) currentMonth='0'+currentMonth;
            String DECode=currentYear+currentMonth;
            
            Test.StartTest();
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount,newOpportunity);
            invoice.Customer_Name__c=newAccount.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.SI_Successful_Payments__c=1;
            invoice.SI_P4P__c=false;
            invoice.SI_Media_Type__c='Print';
            invoice.SI_Payments_Remaining__c=11;
            invoice.SI_Directory_Edition_Code__c=DECode;
            invoice.SI_Directory_Code__c=objDir.Directory_Code__c;
            invoice.SI_Billing_Partner__c='THE BERRY COMPANY';
            invoice.SI_Payment_Method__c='Telco Billing';
            insert invoice;
            
            c2g__codaCreditNote__c SCN=TestMethodsUtility.generateSalesCreditNote(invoice, newAccount);
             SCN.Customer_Name__c=newAccount.id;
             SCN.c2g__CreditNoteStatus__c ='In Progress';
             SCN.SC_P4P__c =false;
             SCN.SC_Media_Type__c ='Print';
             SCN.Transaction_Type__c ='FC - Frequency Change';
             SCN.SC_Payment_Method__c ='Telco Billing';
             SCN.c2g__CreditNoteDate__c=Date.today();
            insert SCN;
            
            //Telco Billing
            Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
            Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
            Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
            Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1);
            insert objOrderLineItem;
            
            c2g__codaDimension3__c dimension3=TestMethodsUtility.createDimension3(objOrderLineItem);
            
            c2g__codaInvoiceLineItem__c invoiceLi= new c2g__codaInvoiceLineItem__c(Order_Line_Item__c =objOrderLineItem.Id,c2g__Quantity__c = 1, c2g__UnitPrice__c = 100, c2g__Invoice__c = invoice.Id, c2g__Product__c = objProd1.Id,c2g__Dimension1__c=dimension1.id,c2g__Dimension2__c=dimension2.id,c2g__Dimension3__c= dimension3.id,c2g__Dimension4__c=dimension4.id);
            invoiceLi.Directory_Edition__c=objDirEd.Id;
            invoiceLi.Directory__c=Objdir.Id;
            invoiceLi.Billing_Frequency__c='Monthly';
            insert invoiceLi;
       
            //Berry Billing Partner
            Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Billing_Partner__c='Berry',
            Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
            Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
            Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,
            Payment_Method__c='ACH',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=11,
            Successful_Payments__c=1 );
            insert objOrderLineItem1;
            
            
           // c2g__codaInvoiceLineItem__c invoiceLi1=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItem1,invoice,objProd1,dimension1,dimension2,dimension3,dimension4 );
            
            c2g__codaInvoiceLineItem__c invoiceLi1= new c2g__codaInvoiceLineItem__c(Order_Line_Item__c =objOrderLineItem.Id,c2g__Quantity__c = 1, c2g__UnitPrice__c = 100, c2g__Invoice__c = invoice.Id, c2g__Product__c = objProd1.Id,c2g__Dimension1__c=dimension1.id,c2g__Dimension2__c=dimension2.id,c2g__Dimension3__c= dimension3.id,c2g__Dimension4__c=dimension4.id);
            invoiceLi1.Directory_Edition__c=objDirEd.Id;
            invoiceLi1.Directory__c=Objdir.Id;
            invoiceLi1.Billing_Frequency__c='Monthly';
            insert invoiceLi1;
            
            Dummy_Object__c objDummy =new Dummy_Object__c(From_Date__c=date.today());
            LocalBillingSCNPostOrReconciliationBatch LBSCNbatch=new LocalBillingSCNPostOrReconciliationBatch(objDummy,'post');
            Database.executeBatch(LBSCNbatch);
            Test.StopTest();
            
    }
}