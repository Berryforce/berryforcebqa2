@isTest 
public class P4PTransaction_AITest {
    static testMethod void P4PTriggerTest() {
        Test.startTest();
        Account acc = TestMethodsUtility.createAccount('cmr');
        Contact con = TestMethodsUtility.createContact(acc);
        Opportunity Opp = TestMethodsUtility.createOpportunity(acc, con);
        insert Opp;
        Order__c Order = TestMethodsUtility.createOrder(acc.Id);
        Order_Group__c OrderSet = TestMethodsUtility.createOrderSet(acc, Order, Opp);
        Order_Line_Items__c OLI = TestMethodsUtility.generateOrderLineItem(acc, Con, Opp, Order, OrderSet);
        OLI.Order_Anniversary_Start_Date__c = date.today();
        OLI.Is_P4P__c = true;
        OLI.P4P_Billing__c = true;
        insert OLI;
        P4P_Transaction__c P4PTrans = TestMethodsUtility.generateP4PTransaction(OLI.Id);
        P4PTrans.Date_of_Click_Call_Lead__c = system.today();
        P4PTrans.Total_Billable_Calls__c = 10;
        
        insert P4PTrans;
        Test.stopTest();
    }
}