public with sharing class ProcessorConnectionSOQLMethods {
	public static list<pymt__Processor_Connection__c> getProcessorConnectionByName(String processorConnectionName) {
		return [SELECT Id FROM pymt__Processor_Connection__c WHERE Name = :CommonMessages.processorConnectionName limit 1];
	}
	
	@IsTest
	public static void ProcessorConnectionSOQLMethodsTest() {
		getProcessorConnectionByName('Test');
	}
}