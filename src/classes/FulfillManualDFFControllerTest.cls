@isTest(seeAllData = true)
public class FulfillManualDFFControllerTest {

    public static testmethod void flManualDFF_1() {

        Test.startTest();

        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'Video';
        insert prdt;

        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;

        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c ordGrp = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp;

        Order_Line_Items__c OLI = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
        OLI.Product2__c = prdt.Id;
        OLI.Status__c = 'In Progress';
        OLI.Action_Code__c = 'Cancel';
        OLI.Effective_Date__c = system.Today();
        insert OLI;

        User u = [SELECT Id, Name FROM User WHERE ProfileId = : [SELECT Id from Profile where Name = 'ICR'] and IsActive = true limit 1];

        System.runAs(u) {

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.OwnerId = u.Id;
        dff.UDAC__c = 'VIDA';
        dff.recordTypeId = Label.Video_RT_Id;
        dff.OrderLineItemID__c = OLI.Id;
        dff.Account__c = a.Id;
        dff.DFF_Product__c = prdt.Id;
        dff.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
        insert dff;

        callPgRef(dff);

        OLI.Status__c = 'Cancelation Requested';
        update OLI;

        callPgRef(dff);

        Modification_Order_Line_Item__c mOLI = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, ordGrp);
        mOLI.Status__c = 'In Progress';
        mOLI.Order_Line_Item__c = OLI.Id;
        insert mOLI;

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.OwnerId = u.Id;
        dff1.UDAC__c = 'VIDA';
        dff1.recordTypeId = Label.Video_RT_Id;
        dff.ModificationOrderLineItem__c = mOLI.Id;
        dff1.Account__c = a.Id;
        dff1.DFF_Product__c = prdt.Id;
        dff1.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
        insert dff1;

        callPgRef(dff1);
                       
        }

        Test.stopTest();

    }

    public static testmethod void flManualDFF_2() {

        Test.startTest();

        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'Video';
        insert prdt;

        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;

        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c ordGrp = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp;

        Order_Line_Items__c OLI = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
        OLI.Product2__c = prdt.Id;
        OLI.Status__c = 'In Progress';
        OLI.Action_Code__c = 'Cancel';
        OLI.Effective_Date__c = system.Today();
        insert OLI;

        Modification_Order_Line_Item__c mOLI = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, ordGrp);
        mOLI.Status__c = 'In Progress';
        mOLI.Order_Line_Item__c = OLI.Id;
        insert mOLI;

        User u = [SELECT Id, Name FROM User WHERE ProfileId = : [SELECT Id from Profile where Name = 'ICR'] and IsActive = true limit 1];

        System.runAs(u) {

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        //dff.OwnerId = u.Id;
        dff.UDAC__c = 'VIDA';
        dff.recordTypeId = Label.Video_RT_Id;
        dff.ModificationOrderLineItem__c = mOLI.Id;
        dff.Account__c = a.Id;
        dff.DFF_Product__c = prdt.Id;
        dff.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
        insert dff;

        callPgRef(dff);
                       
        }

        Test.stopTest();

    }

    public static void callPgRef(Digital_Product_Requirement__c dff){

        PageReference pgRf1 = Page.StartManualFlmnt;
        Test.setCurrentPage(pgRf1);

        ApexPages.StandardController sc = new ApexPages.StandardController(dff);
        FulfillManualDFFController flMnlDff = new FulfillManualDFFController(sc);
        ApexPages.currentPage().getParameters().put('id', dff.Id);
        flMnlDff.dffFulfill();

    }

}