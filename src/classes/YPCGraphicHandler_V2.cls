public with sharing class YPCGraphicHandler_V2 {
	public static void createChildern(list<Digital_Product_Requirement__c> DFFChildList, map<Id, Digital_Product_Requirement__c> mapDFFChildListOld, map<Id, String> mapRecordTypes) {
		
		list<YPC_Graphics__c> YPCGlistFinal = new list<YPC_Graphics__c>();
		list<YPC_Graphics__c> lstYPCInsertNew = new list<YPC_Graphics__c>();
		set<Id> setYPCId = new set<Id>();
		set<Id> setYPCNewStatusCount = new set<Id>();
		set<Id> setYPCNewPAOStatusCount = new set<Id>();
		
		// get a list of YPC graphics records that exist  from the dff ids passed in 
        list<YPC_Graphics__c>  YPCGlist =[SELECT Id, Name, DFF__c, Miles_Status__c, Date_Submitted__c, Date_Graphics_Received__c, 
                                            Date_Graphics_Complete__c, YPC_Graphics_URL__c, Production_Notes_for_YPC_Graphic__c, 
                                            YPC_Graphics_Type__c, Account_Name__c, URN__c 
                                            FROM YPC_Graphics__c
                                            WHERE DFF__c in :mapDFFChildListOld.keySet()];
	
		//make the YPC_Graphics__c maps
        map<String, YPC_Graphics__c> mapYPCG = new map<String, YPC_Graphics__c >();
        for(YPC_Graphics__c YPCG: YPCGlist) {
            mapYPCG.put(YPCG.DFF__c+'!'+YPCG.YPC_Graphics_Type__c, YPCG);
            if(YPCG.Miles_Status__c == 'New') {
            	setYPCNewStatusCount.add(YPCG.Id);
            }
            if(YPCG.Miles_Status__c != 'New' && YPCG.YPC_Graphics_Type__c != 'PAO') {
            	setYPCNewPAOStatusCount.add(YPCG.Id);
            }
        }
        
        for(Digital_Product_Requirement__c DFF : DFFChildList) {
        	//update YPC_Graphics__c records as needed         
            // PAO
            Digital_Product_Requirement__c DFFOLD = mapDFFChildListOld.get(DFF.Id);
            if(DFF.Production_Notes_for_PrintAdOrderGraphic__c != DFFOLD.Production_Notes_for_PrintAdOrderGraphic__c) {
            	if(DFFOLD.Production_Notes_for_PrintAdOrderGraphic__c == null && mapYPCG.get(DFF.id+'!PAO') == Null) {
            		//Create New record.
            		lstYPCInsertNew.add(generateYPCRecords(DFF.Id, 'PAO', DFF.Production_Notes_for_PrintAdOrderGraphic__c));
            	}
            	else {
            		//Update existing records production note and status.
            		if(mapYPCG.get(DFF.id+'!PAO') != Null) {
            			mapYPCG.get(DFF.id+'!PAO').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_PrintAdOrderGraphic__c;
            			if(mapYPCG.get(DFF.id+'!PAO').Miles_Status__c != 'New') {
            				mapYPCG.get(DFF.id+'!PAO').Miles_Status__c = 'Change';
            				setYPCId.add(mapYPCG.get(DFF.id+'!PAO').Id);
            			}
            			if(setYPCNewPAOStatusCount.size() > 0) {
            				setYPCId.add(mapYPCG.get(DFF.id+'!PAO').Id);
            			}
            			YPCGlistFinal.add(mapYPCG.get(DFF.id+'!PAO'));
            		}
            	}
            }
            
            if(DFF.Production_Notes_for_Logo__c != DFFOLD.Production_Notes_for_Logo__c && (mapRecordTypes.get(DFF.RecordTypeId).contains('Silver') || mapRecordTypes.get(DFF.RecordTypeId).contains('Diamond'))) {
            	if(DFFOLD.Production_Notes_for_Logo__c == null && mapYPCG.get(DFF.id+'!Logo') == null) {
            		//Create New record.
            		lstYPCInsertNew.add(generateYPCRecords(DFF.Id, 'Logo', DFF.Production_Notes_for_Logo__c));
            		lstYPCInsertNew.add(generateYPCRecords(DFF.Id, 'Logo Distribution', DFF.Production_Notes_for_Logo__c));
            	}
            	else {
            		//Update existing records production note and status.
            		if(mapYPCG.get(DFF.id+'!Logo') != Null) {
	            		mapYPCG.get(DFF.id+'!Logo').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Logo__c;
	            		if(mapYPCG.get(DFF.id+'!Logo').Miles_Status__c != 'New') {
	            			mapYPCG.get(DFF.id+'!Logo').Miles_Status__c = 'Change';
	            			setYPCId.add(mapYPCG.get(DFF.id+'!Logo').Id);
	            		}
						YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Logo'));
            		}
            		if(mapYPCG.get(DFF.id+'!Logo Distribution') != Null) {
						mapYPCG.get(DFF.id+'!Logo Distribution').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Logo__c;
						if(mapYPCG.get(DFF.id+'!Logo Distribution').Miles_Status__c != 'New') {
							mapYPCG.get(DFF.id+'!Logo Distribution').Miles_Status__c = 'Change';
							setYPCId.add(mapYPCG.get(DFF.id+'!Logo Distribution').Id);
						}
						YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Logo Distribution'));
            		}
            	}
            }
            
            if(DFF.Production_Notes_for_Banner_Graphic__c != DFFOLD.Production_Notes_for_Banner_Graphic__c && (mapRecordTypes.get(DFF.RecordTypeId).contains('Gold')|| mapRecordTypes.get(DFF.RecordTypeId).contains('Platinum') || mapRecordTypes.get(DFF.RecordTypeId).contains('Priority'))) {
            	if(DFFOLD.Production_Notes_for_Banner_Graphic__c == null && mapYPCG.get(DFF.id+'!Banner') == Null) {
            		//Create New record.
            		lstYPCInsertNew.add(generateYPCRecords(DFF.Id, 'Banner', DFF.Production_Notes_for_Banner_Graphic__c));
            		lstYPCInsertNew.add(generateYPCRecords(DFF.Id, 'Banner Distribution', DFF.Production_Notes_for_Banner_Graphic__c));
            	}
            	else {
            		//Update existing records production note and status.
            		if(mapYPCG.get(DFF.id+'!Banner') != Null) {
	            		mapYPCG.get(DFF.id+'!Banner').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Banner_Graphic__c;
	            		if(mapYPCG.get(DFF.id+'!Banner').Miles_Status__c != 'New') {
	            			mapYPCG.get(DFF.id+'!Banner').Miles_Status__c = 'Change';
	            			setYPCId.add(mapYPCG.get(DFF.id+'!Banner').Id);
	            		}
		                YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Banner'));
            		}
	                if(mapYPCG.get(DFF.id+'!Banner Distribution') != Null) {
		                mapYPCG.get(DFF.id+'!Banner Distribution').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Banner_Graphic__c;
		                if(mapYPCG.get(DFF.id+'!Banner Distribution').Miles_Status__c != 'New') {
		                	mapYPCG.get(DFF.id+'!Banner Distribution').Miles_Status__c = 'Change';
		                	setYPCId.add(mapYPCG.get(DFF.id+'!Banner Distribution').Id);
		                }
		                YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Banner Distribution'));
	                }
	                 
            	}
            }
            
            if(DFF.Production_Notes_for_Diamond_Graphic__c != DFFOLD.Production_Notes_for_Diamond_Graphic__c && mapRecordTypes.get(DFF.RecordTypeId).contains('Diamond')) {
            	if(DFFOLD.Production_Notes_for_Diamond_Graphic__c == null && mapYPCG.get(DFF.id+'!Diamond Distribution') == null) {
            		//Create New record.
            		lstYPCInsertNew.add(generateYPCRecords(DFF.ID, 'Diamond Distribution', DFF.Production_Notes_for_Diamond_Graphic__c));
            		lstYPCInsertNew.add(generateYPCRecords(DFF.ID, 'Diamond Logo', DFF.Production_Notes_for_Diamond_Graphic__c));
            	}
            	else {
            		//Update existing records production note and status.
            		if(mapYPCG.get(DFF.id+'!Diamond Distribution') != Null) {
            			mapYPCG.get(DFF.id+'!Diamond Distribution').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Diamond_Graphic__c;
            			if(mapYPCG.get(DFF.id+'!Diamond Distribution').Miles_Status__c != 'New') {
            				mapYPCG.get(DFF.id+'!Diamond Distribution').Miles_Status__c = 'Change';
            				setYPCId.add(mapYPCG.get(DFF.id+'!Diamond Distribution').Id);
            			}
                        YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Diamond Distribution'));
            		}
            		
            		if(mapYPCG.get(DFF.id+'!Diamond Logo') != Null) {
            			mapYPCG.get(DFF.id+'!Diamond Logo').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Diamond_Graphic__c;
            			if(mapYPCG.get(DFF.id+'!Diamond Logo').Miles_Status__c != 'New') {//updating code to Diamond Logo as this was checking for Diamond Distribution earlier:Reddy 11:02 am EST
                        	mapYPCG.get(DFF.id+'!Diamond Logo').Miles_Status__c = 'Change';
                        	setYPCId.add(mapYPCG.get(DFF.id+'!Diamond Logo').Id);
            			}
                        YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Diamond Logo'));
            		}
            	}
            }
        }
        
        if(lstYPCInsertNew.size() > 0) {
	        list<Dummy_Object__c> lstDummyObj = new list<Dummy_Object__c>();
	        for(YPC_Graphics__c iterator : lstYPCInsertNew) {
	        	lstDummyObj.add(new Dummy_Object__c());
	        }
	        
	        if(lstDummyObj.size() > 0) {
	        	insert lstDummyObj;
	        	Integer intCount = 0;
	        	list<Dummy_Object__c> lstSelDummyObj = [select name from Dummy_Object__c where id in :lstDummyObj];
	        	for(YPC_Graphics__c objYPCUrnPopulate : lstYPCInsertNew) {
					objYPCUrnPopulate.name = lstSelDummyObj[intCount].name;
					intCount++;
					YPCGlistFinal.add(objYPCUrnPopulate);
	        	}
	        }
        }
        
        // save and update records       
        if(YPCGlistFinal.size() > 0 ){
            system.debug('DFF YPC Upsert : '+ YPCGlistFinal);
            upsert YPCGlistFinal;
            if(setYPCNewStatusCount.size() <= 0 && YPCGlist.size() > 0) {
	            for(YPC_Graphics__c iterator : YPCGlistFinal) {
	            	setYPCId.add(iterator.Id);
	            }
            }
            if(setYPCId.size() > 0) {
	        	Miles33XMLFormationHandlerController.generateXMLForYPC(setYPCId);
            }
        }
	}
	
	private static YPC_Graphics__c generateYPCRecords(Id dffID, String strType, String strProdNote) {
		YPC_Graphics__c objYPC = new YPC_Graphics__c(Name = '0000', Sequence__c = 1, Miles_Status__c = 'New', DFF__c = dffID, Production_Notes_for_YPC_Graphic__c = strProdNote, YPC_Graphics_Type__c = strType);
		return objYPC;
	}
	
	
    /*public static void createChildern(list<Digital_Product_Requirement__c> DFFChildList, list<Digital_Product_Requirement__c> DFFChildListOld, map<Id, String> mapRecordTypes){
        Dummy_Object__c objDummyObj;
        List<Dummy_Object__c> lstDummyObj = new List<Dummy_Object__c>();
        //integer DFFmaxSEQi = 0;
        //DFFmaxSEQi = integer.valueof(DFFmaxSEQ);
        list<string> DFFidList = new  list<string>();
        
        //loop through the records to create an id list     
        for(Digital_Product_Requirement__c DFFx : DFFChildList){
            DFFidList.add(DFFx.Id);
        }
        
        // get a list of YPC graphics records that exist  from the dff ids passed in 
        list<YPC_Graphics__c>  YPCGlist =[SELECT Id, Name, DFF__c, Miles_Status__c, Date_Submitted__c, Date_Graphics_Received__c, 
                                            Date_Graphics_Complete__c, YPC_Graphics_URL__c, Production_Notes_for_YPC_Graphic__c, 
                                            YPC_Graphics_Type__c, Account_Name__c, URN__c 
                                            FROM YPC_Graphics__c
                                            WHERE DFF__c in :DFFidList ];
        //make the YPC_Graphics__c maps
        map<String, YPC_Graphics__c> mapYPCG = new map<String, YPC_Graphics__c >();
        for(YPC_Graphics__c YPCG: YPCGlist){
            mapYPCG.put(YPCG.DFF__c+'!'+YPCG.YPC_Graphics_Type__c, YPCG);
        }
        
        list<YPC_Graphics__c>  YPCGlistFinal = new list<YPC_Graphics__c>();
        list<YPC_Graphics__c>  lstInsYPCGTemp = new list<YPC_Graphics__c>();
        map<Id, String> mapNotesByYPCId = new map<Id, String>();
        set<Id> setYPCId = new set<Id>();
        
        for(Digital_Product_Requirement__c DFF : DFFChildList) {
             //update YPC_Graphics__c records as needed         
             // PAO         
             if(mapYPCG.get(DFF.id+'!PAO') != Null){             
                 for(Digital_Product_Requirement__c DFFOLD : DFFChildListOld){
                     if(DFF.Production_Notes_for_PrintAdOrderGraphic__c != DFFOLD.Production_Notes_for_PrintAdOrderGraphic__c && DFF.id == DFFOLD.id){
                         mapYPCG.get(DFF.id+'!PAO').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_PrintAdOrderGraphic__c;
                         YPCGlistFinal.add(mapYPCG.get(DFF.id+'!PAO'));
                           if(mapYPCG.get(DFF.id+'!PAO').Miles_Status__c != NULL) {
                             setYPCId.add(mapYPCG.get(DFF.id+'!PAO').id);
                             mapNotesByYPCId.put(mapYPCG.get(DFF.id+'!PAO').id, DFF.Production_Notes_for_PrintAdOrderGraphic__c);
                           }
                     }
                 }    
             }
            
            //logo Diamond Silver
             if(mapYPCG.get(DFF.id+'!Logo') != Null  && (mapRecordTypes.get(DFF.RecordTypeId).contains('Silver') || mapRecordTypes.get(DFF.RecordTypeId).contains('Diamond'))){   
                for(Digital_Product_Requirement__c DFFOLD : DFFChildListOld){
                     if(DFF.Production_Notes_for_Logo__c != DFFOLD.Production_Notes_for_Logo__c && DFF.id == DFFOLD.id){
                         mapYPCG.get(DFF.id+'!Logo').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Logo__c;
                         YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Logo'));
                         if(mapYPCG.get(DFF.id+'!Logo').Miles_Status__c != NULL) {
                             setYPCId.add(mapYPCG.get(DFF.id+'!Logo').id);
                             mapNotesByYPCId.put(mapYPCG.get(DFF.id+'!Logo').id, DFF.Production_Notes_for_Logo__c);
                         }
                         mapYPCG.get(DFF.id+'!Logo Distribution').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Logo__c;
                         YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Logo Distribution'));
                         if(mapYPCG.get(DFF.id+'!Logo Distribution').Miles_Status__c != NULL) {
                             setYPCId.add(mapYPCG.get(DFF.id+'!Logo Distribution').id);
                             mapNotesByYPCId.put(mapYPCG.get(DFF.id+'!Logo Distribution').id, DFF.Production_Notes_for_Logo__c);
                         }
                     }
                 }    
             }
            
            // Banner gold Platinum
             if(mapYPCG.get(DFF.id+'!Banner') != Null  && (mapRecordTypes.get(DFF.RecordTypeId).contains('Gold')|| mapRecordTypes.get(DFF.RecordTypeId).contains('Platinum') || mapRecordTypes.get(DFF.RecordTypeId).contains('Priority'))){   
                for(Digital_Product_Requirement__c DFFOLD : DFFChildListOld){
                     if(DFF.Production_Notes_for_Banner_Graphic__c != DFFOLD.Production_Notes_for_Banner_Graphic__c && DFF.id == DFFOLD.id){
                         mapYPCG.get(DFF.id+'!Banner').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Banner_Graphic__c;
                         YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Banner'));
                         if(mapYPCG.get(DFF.id+'!Banner').Miles_Status__c != NULL) {
                             setYPCId.add(mapYPCG.get(DFF.id+'!Banner').id);
                             mapNotesByYPCId.put(mapYPCG.get(DFF.id+'!Banner').id, DFF.Production_Notes_for_Banner_Graphic__c);
                         }
                         mapYPCG.get(DFF.id+'!Banner Distribution').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Banner_Graphic__c;
                         YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Banner Distribution'));
                         if(mapYPCG.get(DFF.id+'!Banner Distribution').Miles_Status__c != NULL) {
                             setYPCId.add(mapYPCG.get(DFF.id+'!Banner Distribution').id);
                             mapNotesByYPCId.put(mapYPCG.get(DFF.id+'!Banner Distribution').id, DFF.Production_Notes_for_Banner_Graphic__c);
                         }
                     }
                 }    
             }
             
             // Diamond Distribution - added by Joe henry
             if(mapYPCG.get(DFF.id+'!Diamond Distribution') != Null  && (mapRecordTypes.get(DFF.RecordTypeId).contains('Diamond'))){   
                for(Digital_Product_Requirement__c DFFOLD : DFFChildListOld){
                     if(DFF.Production_Notes_for_Diamond_Graphic__c != DFFOLD.Production_Notes_for_Diamond_Graphic__c && DFF.id == DFFOLD.id){
                         mapYPCG.get(DFF.id+'!Diamond Distribution').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Diamond_Graphic__c;
                         YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Diamond Distribution'));
                         if(mapYPCG.get(DFF.id+'!Diamond Distribution').Miles_Status__c != NULL) {
                             setYPCId.add(mapYPCG.get(DFF.id+'!Diamond Distribution').id);
                             mapNotesByYPCId.put(mapYPCG.get(DFF.id+'!Diamond Distribution').id, DFF.Production_Notes_for_Diamond_Graphic__c);
                         }
                          mapYPCG.get(DFF.id+'!Diamond Logo').Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Diamond_Graphic__c;
                          YPCGlistFinal.add(mapYPCG.get(DFF.id+'!Diamond Logo'));
                          if(mapYPCG.get(DFF.id+'!Diamond Logo').Miles_Status__c != NULL) {
                             setYPCId.add(mapYPCG.get(DFF.id+'!Diamond Logo').id);
                             mapNotesByYPCId.put(mapYPCG.get(DFF.id+'!Diamond Logo').id, DFF.Production_Notes_for_Diamond_Graphic__c);
                          }
                     }
                 }    
             }
            
            // here we create YPC_Graphics__c records as needed 
            // no YPCG records found for these 
            // local var
            YPC_Graphics__c nYPCG;
             
             /// everything may get PAO create as needed
             if(DFF.Production_Notes_for_PrintAdOrderGraphic__c != null && (mapYPCG.get(DFF.id+'!PAO') == null || YPCGlist.size() == 0)){
                 //increment the urn seq 
                 //DFFmaxSEQi++;
                 // create a new YPC_Graphics__c record 
                 nYPCG = new YPC_Graphics__c(Sequence__c = 1, DFF__c = DFF.id, Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_PrintAdOrderGraphic__c, YPC_Graphics_Type__c = 'PAO' );
				 if(DFF.Core_Opportunity_Line_ID__c != null) {
					 nYPCG.name = '0000';
				 }
				 else {
					objDummyObj = new Dummy_Object__c();
					 //objDummyObj.DO_URN_number__c = DFFmaxSEQi;
					 lstDummyObj.add(objDummyObj);
				}
				 // add the YPC_Graphics__c record to the list
				 lstInsYPCGTemp.add(nYPCG); 
                 //add to the map   
                 mapYPCG.put(DFF.id+'!PAO', nYPCG);
             }
             
             //create logos as needed   
            if(mapYPCG.get(DFF.id+'!Logo') == null || YPCGlist.size() == 0) {
             if((mapRecordTypes.get(DFF.RecordTypeId).contains('Silver')|| mapRecordTypes.get(DFF.RecordTypeId).contains('Diamond')) && DFF.Production_Notes_for_Logo__c != Null) {
                 //increment the urn seq 
                 //DFFmaxSEQi++;
                 // create a new YPC_Graphics__c record 
                 nYPCG = new YPC_Graphics__c(Sequence__c = 1, DFF__c = DFF.id, Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Logo__c, YPC_Graphics_Type__c = 'Logo' );
                 if(DFF.Core_Opportunity_Line_ID__c != null) {
					 nYPCG.name = '0000';
				 }
				 else {
					 objDummyObj = new Dummy_Object__c();
					 //objDummyObj.DO_URN_number__c = DFFmaxSEQi;
					 lstDummyObj.add(objDummyObj);
				}
				 // add the YPC_Graphics__c record to the list
				 lstInsYPCGTemp.add(nYPCG);				 
                 //add to the map   
                 mapYPCG.put(DFF.id+'!Logo', nYPCG);
                 //increment the urn seq 
                 //DFFmaxSEQi++;
                 // create a new YPC_Graphics__c record 
                 nYPCG = new YPC_Graphics__c(Sequence__c = 1, DFF__c = DFF.id, Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Logo__c, YPC_Graphics_Type__c = 'Logo Distribution' );
				 if(DFF.Core_Opportunity_Line_ID__c != null) {
					 nYPCG.name = '0000';
				 }
				 else {				 
					 objDummyObj = new Dummy_Object__c();
					 //objDummyObj.DO_URN_number__c = DFFmaxSEQi;
					 lstDummyObj.add(objDummyObj);
				 }
				 // add the YPC_Graphics__c record to the list
				 lstInsYPCGTemp.add(nYPCG);
                 //add to the map   
                 mapYPCG.put(DFF.id+'!Logo Distribution', nYPCG);
     
             }
            }
             
             //create banners as needed 
             if(mapYPCG.get(DFF.id+'!Banner') == null || YPCGlist.size() == 0) {
                 if((mapRecordTypes.get(DFF.RecordTypeId).contains('Gold')|| mapRecordTypes.get(DFF.RecordTypeId).contains('Platinum') || mapRecordTypes.get(DFF.RecordTypeId).contains('Priority')) && DFF.Production_Notes_for_Banner_Graphic__c != Null) {
                      //increment the urn seq 
                     //DFFmaxSEQi++;
                     // create a new YPC_Graphics__c record 
                     nYPCG = new YPC_Graphics__c(Sequence__c = 1, DFF__c = DFF.id, Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Banner_Graphic__c, YPC_Graphics_Type__c = 'Banner' );
                     if(DFF.Core_Opportunity_Line_ID__c != null) {
						 nYPCG.name = '0000';
					 }
					 else {
						 objDummyObj = new Dummy_Object__c();
						 //objDummyObj.DO_URN_number__c = DFFmaxSEQi;
						 lstDummyObj.add(objDummyObj);
					 }
					 // add the YPC_Graphics__c record to the list
					 lstInsYPCGTemp.add(nYPCG); 
                     //add to the map   
                     mapYPCG.put(DFF.id+'!Logo', nYPCG);
                                  //increment the urn seq 
                     //DFFmaxSEQi++;
                     // create a new YPC_Graphics__c record 
                     nYPCG = new YPC_Graphics__c(Sequence__c = 1, DFF__c = DFF.id, Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Banner_Graphic__c, YPC_Graphics_Type__c = 'Banner Distribution' );
					if(DFF.Core_Opportunity_Line_ID__c != null) {
						 nYPCG.name = '0000';
					}
					 else {
						objDummyObj = new Dummy_Object__c();
						 //objDummyObj.DO_URN_number__c = DFFmaxSEQi;
						 lstDummyObj.add(objDummyObj);
					 }
                     // add the YPC_Graphics__c record to the list
                     lstInsYPCGTemp.add(nYPCG); 
                     //add to the map   
                     mapYPCG.put(DFF.id+'!Banner Distribution', nYPCG);
                 }
             }
              //create Diamon Distribution as needed 
             if(mapYPCG.get(DFF.id+'!Diamond Distribution') == null || YPCGlist.size() == 0) {
                 if((mapRecordTypes.get(DFF.RecordTypeId).contains('Diamond')) && DFF.Production_Notes_for_Diamond_Graphic__c != Null) {
                      //increment the urn seq 
                        //DFFmaxSEQi++;
                     // create a new YPC_Graphics__c record 
                     nYPCG = new YPC_Graphics__c(Sequence__c = 1, DFF__c = DFF.id, Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Diamond_Graphic__c , YPC_Graphics_Type__c = 'Diamond Logo' );
                     if(DFF.Core_Opportunity_Line_ID__c != null) {
						 nYPCG.name = '0000';
					 }
					 else {
						 objDummyObj = new Dummy_Object__c();
						 //objDummyObj.DO_URN_number__c = DFFmaxSEQi;
						 lstDummyObj.add(objDummyObj);
					 }
					 // add the YPC_Graphics__c record to the list
					 lstInsYPCGTemp.add(nYPCG);
                     //add to the map   
                     mapYPCG.put(DFF.id+'!Diamond Logo', nYPCG);
                     
                     //increment the urn seq 
                     //DFFmaxSEQi++;
                     // create a new YPC_Graphics__c record 
                     nYPCG = new YPC_Graphics__c(Sequence__c = 1, DFF__c = DFF.id, Production_Notes_for_YPC_Graphic__c = DFF.Production_Notes_for_Diamond_Graphic__c , YPC_Graphics_Type__c = 'Diamond Distribution' );
                     if(DFF.Core_Opportunity_Line_ID__c != null) {
						 nYPCG.name = '0000';
					 }
					 else {
						 objDummyObj = new Dummy_Object__c();
						 //objDummyObj.DO_URN_number__c = DFFmaxSEQi;
						 lstDummyObj.add(objDummyObj);
					 }
					 // add the YPC_Graphics__c record to the list
					 lstInsYPCGTemp.add(nYPCG); 
                     //add to the map   
                     mapYPCG.put(DFF.id+'!Diamond Distribution', nYPCG);
                 }
             }
        }
        
        if(setYPCId.isEmpty()) {
            Miles33XMLFormationHandlerController.generateXMLForYPC(setYPCId, mapNotesByYPCId);
        }
        Integer intCount = 0;
        if(lstDummyObj.size() > 0) {
        	insert lstDummyObj;
        	List<Dummy_Object__c> lstSelDummyObj = [select name from Dummy_Object__c where id in :lstDummyObj];
        	for(YPC_Graphics__c objYPCUrnPopulate : lstInsYPCGTemp) {
				if(objYPCUrnPopulate.name != '0000') {
					objYPCUrnPopulate.name = lstSelDummyObj[intCount].name;
					intCount++;
					
				}
				YPCGlistFinal.add(objYPCUrnPopulate);
        	}
        }
		else {
			YPCGlistFinal.addAll(lstInsYPCGTemp);
		}
        
        // save and update records       
        if(YPCGlistFinal.size() > 0 ){
            system.debug('DFF YPC Upsert : '+ YPCGlistFinal);
            upsert YPCGlistFinal;
        }
    }*/
}