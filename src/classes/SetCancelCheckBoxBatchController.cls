global class SetCancelCheckBoxBatchController implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
	global SetCancelCheckBoxBatchController(){}
	global Set<Id> setOfOliIds=new Set<Id>();
	global Set<Id> setOlnIds = new Set<Id>();
	
	global SetCancelCheckBoxBatchController(Set<Id> OlSet) {
		this.setOlnIds = olSet;
	}
	
	global void execute(SchedulableContext ctx)
	{
		Database.executeBatch(new SetCancelCheckBoxBatchController(), 50);   
	}


	global Database.QueryLocator start(Database.BatchableContext bc) {
		string query='select id,isCanceled__c,Media_Type__c,Cutomer_Cancel_Date__c from Order_Line_Items__c where isCanceled__c = False  and Cutomer_Cancel_Date__c != null and Cutomer_Cancel_Date__c <= today' ;
		return database.getQuerylocator(query);
	}
	
	global void execute(Database.BatchableContext bc, List<Order_Line_Items__c > oliList) {
		List<Order_Line_Items__c> oliListForUPdate=new List<Order_Line_Items__c>();
		List<Modification_Order_Line_Item__c> lstMOLIInactivate = new List<Modification_Order_Line_Item__c>();

		List<Modification_Order_Line_Item__c> lstMOLI = [select id from Modification_Order_Line_Item__c where Order_Line_Item__c in :oliList and Inactive__c = false and Completed__c = false];

		for(Modification_Order_Line_Item__c objMOLI : lstMOLI) {
			lstMOLIInactivate.add(new Modification_Order_Line_Item__c(id=objMOLI.id, Inactive__c = true));
		}

		for(Order_Line_Items__c OLI:oliList) {
			//if(OLI.Cutomer_Cancel_Date__c<=System.today())
			//{
			oliListForUPdate.add(new Order_Line_Items__c(isCanceled__c=true,Id=OLI.Id));
			setOfOliIds.add(OLI.Id);
			//}

		}
		System.debug('############ '+oliListForUPdate);
		if(oliListForUPdate.size()>0) {
			update oliListForUPdate;
		}

		if(lstMOLIInactivate.size() > 0) {
			update lstMOLIInactivate;
		}

	}
	
	global void finish(Database.BatchableContext bc) {
		List<order_Line_Items__c> OLIListForInvoice=[SELECT Id,Directory_Edition__c,Canvass__r.Canvass_Code__c,Directory_Code__c,Billing_Partner_Account__c,Account__c,Order_Line_Items__c.Product2__r.Name,Order_Line_Total__c,Order_Line_Items__c.Product2__r.Family,Order_Line_Items__c.Product2__r.RGU__c,Order_Line_Items__c.Opportunity__r.Name,Media_Type__c, Name, Billing_Contact__c,Billing_Contact__r.Email, Billing_End_Date__c, Billing_Frequency__c, Billing_Partner__c, 
		Billing_Start_Date__c, isCanceled__c, Cancellation__c, Canvass__c, Continious_Billing__c, Cutomer_Cancel_Date__c, 
		Digital_Product_Requirement__c, Discount__c, Fulfilled_on__c, Description__c, ListPrice__c, Opportunity__c, 
		Order__c, Order_Anniversary_Start_Date__c, FulfillmentDate__c, Order_Group__c, Payment_Duration__c, Payment_Method__c, 
		Payments_Remaining__c, Product2__c, Total_Prorated_Days_for_contract__c, Prorate_Stored_Value__c, ProductCode__c, Quantity__c, Prorate_Credit__c, 
		Total_Prorate__c, Status__c, Prorate_Credit_Days__c, Talus_Go_Live_Date__c, Talus_Fulfillment_Date__c, Talus_OrderLineItem__c, Talus_Cancel_Date__c, 
		Successful_Payments__c, Quote_Signed_Date__c, Quote_signing_method__c, UnitPrice__c, Sent_to_fulfillment_on__c, Service_End_Date__c, Service_Start_Date__c, 
		Line_Status__c, Billing_Close_Date__c,  Current_Daily_Prorate__c, Next_Billing_Date__c, 
		Subscription_ID__c, Days_B_W__c, Product_Type__c, Current_Rate__c, Package_ID__c,Comments__c,
		Directory_Section__c, Directory_Heading__c, Scope__c, Last_successful_settlement__c, Telco__c, Directory__c, 
		Effective_Date__c,Canvass__r.Branch_Manager__c, P4P_Billing__c, Is_P4P__c, P4P_Tracking_Number_Status__c, P4P_Tracking_Number_ID__c, checked__c, Cancelation_Fee__c, Current_Billing_Period_Days__c, zero_Cancel_Fee_Requested__c, Zero_Cancel_Fee_Approved__c ,Canvass__r.name,Package_ID_External__c,Parent_ID_of_Addon__c,Parent_ID__c,CMR_Name__c
		FROM 
		Order_Line_Items__c    
		WHERE
		Id in :setOfOliIds AND isCanceled__c=true AND Cancel_Fee_Sales_Invoice_Created__c=false AND Cancelation_Fee__c>0];

		list<c2g__codaInvoice__c> invoiceList=SalesInvoiceHandlerController.createSalesInvoiceForCancel(OLIListForInvoice);
		for(Order_Line_Items__c OLI:OLIListForInvoice) {
			OLI.Cancel_Fee_Sales_Invoice_Created__c=true;
		}    

		update OLIListForInvoice;   
		
        Batch_Size__c objBatch = Batch_Size__c.getInstance('DeleteTalus'); 
        database.executebatch(new DeleteFulfillmentBatch(), Integer.valueOf(objBatch.Size__c));
	}
}