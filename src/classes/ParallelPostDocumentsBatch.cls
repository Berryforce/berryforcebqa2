/**
 * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
 * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
 * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
 * result in criminal or other legal proceedings.
 *
 * Copyright FinancialForce.com, inc. All rights reserved.
 * Created by Agustina Garcia
 */

public with sharing class ParallelPostDocumentsBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private String objectName;
    private String statusFieldName;

    private String documentType;

    private Integer jobPos;
    private Integer minRange;
    private Integer maxRange;

    private String nameMask;
    private String minRangeWithMask;
    private String maxRangeWithMask;
    private List<String> maskValues;

    private Integer scopeSize;

    private List<Id> documentsPosted;
    private List<String> errorMessages;

    public ParallelPostDocumentsBatch(String docType, Integer scopeValue, String nameMaskVal, Integer minRangeVal, Integer maxRangeVal, Integer jobPosVal)
    {
        documentType = docType;

        //Check docType and get document
        if(documentType == ParallelCreateAndPostDocumentsService.SINV)
        {
            objectName = ParallelCreateAndPostDocumentsService.SINV_OBJECTNAME;
            statusFieldName = ParallelCreateAndPostDocumentsService.SINV_STATUS_FIELDNAME;
        }
        else if(documentType == ParallelCreateAndPostDocumentsService.SCRN)
        {
            objectName = ParallelCreateAndPostDocumentsService.SCRN_OBJECTNAME;
            statusFieldName = ParallelCreateAndPostDocumentsService.SCRN_STATUS_FIELDNAME;
        }
        else if(docType == ParallelCreateAndPostDocumentsService.CE)
        {
            objectName = ParallelCreateAndPostDocumentsService.CE_OBJECTNAME;
            statusFieldName = ParallelCreateAndPostDocumentsService.CE_STATUS_FIELDNAME;
        }
        else
        {
            throw new ParallelCreateAndPostDocumentsService.ParallelException('Not able to run Post. Your document type is ' + documentType + ' and it should be ' + ParallelCreateAndPostDocumentsService.SINV + ', ' + ParallelCreateAndPostDocumentsService.SCRN + ' or ' + ParallelCreateAndPostDocumentsService.CE + '.');
        }

        nameMask = nameMaskVal;
        scopeSize = scopeValue;

        minRange = minRangeVal;
        maxRange = maxRangeVal;

        jobPos = jobPosVal;

        documentsPosted = new List<Id>();
        errorMessages = new List<String>();

        maskValues = nameMask.split('\\{');
        maskValues[1] = maskValues.get(1).split('}').get(0);

        Map<Integer, String> minAndMaxWithMask = ParallelCreateAndPostDocumentsService.calculateMinAndMaxWithMask(maskValues, minRange, maxRange);

        minRangeWithMask = minAndMaxWithMask.get(1);
        maxRangeWithMask = minAndMaxWithMask.get(2);
    }

    public ParallelPostDocumentsBatch(String docType, Integer scopeValue, String nameMaskVal, Integer minRangeVal, Integer jobPosVal)
    {
        documentType = docType;

        //Check docType and get document
        if(documentType == ParallelCreateAndPostDocumentsService.SINV)
        {
            objectName = ParallelCreateAndPostDocumentsService.SINV_OBJECTNAME;
            statusFieldName = ParallelCreateAndPostDocumentsService.SINV_STATUS_FIELDNAME;
        }
        else if(documentType == ParallelCreateAndPostDocumentsService.SCRN)
        {
            objectName = ParallelCreateAndPostDocumentsService.SCRN_OBJECTNAME;
            statusFieldName = ParallelCreateAndPostDocumentsService.SCRN_STATUS_FIELDNAME;
        }
        else if(docType == ParallelCreateAndPostDocumentsService.CE)
        {
            objectName = ParallelCreateAndPostDocumentsService.CE_OBJECTNAME;
            statusFieldName = ParallelCreateAndPostDocumentsService.CE_STATUS_FIELDNAME;
        }
        else
        {
            throw new ParallelCreateAndPostDocumentsService.ParallelException('Not able to run Post. Your document type is ' + documentType + ' and it should be ' + ParallelCreateAndPostDocumentsService.SINV + ', ' + ParallelCreateAndPostDocumentsService.SCRN + ' or ' + ParallelCreateAndPostDocumentsService.CE + '.');
        }

        nameMask = nameMaskVal;
        scopeSize = scopeValue;

        minRange = minRangeVal;
        maxRange = minRange*2;

        jobPos = jobPosVal;

        documentsPosted = new List<Id>();
        errorMessages = new List<String>();

        maskValues = nameMask.split('\\{');
        maskValues[1] = maskValues.get(1).split('}').get(0);

        Map<Integer, String> minAndMaxWithMask = ParallelCreateAndPostDocumentsService.calculateMinAndMaxWithMask(maskValues, minRange, 0);

        minRangeWithMask = minAndMaxWithMask.get(1);
        maxRangeWithMask = minAndMaxWithMask.get(2);
    }

    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        String qry;
        String invStatus = 'In Progress';

        if(jobPos <= 2)
        {
            qry = 'SELECT Id ';
            qry += 'FROM ' + objectName + ' ';
            qry += 'WHERE ' + statusFieldName  + ' = :invStatus ';
            qry += 'AND Name >= :minRangeWithMask ';
            qry += 'AND Name <= :maxRangeWithMask ';
            qry += 'ORDER BY Name ';
        }
        else
        {
            if(jobPos == 3)
            {
                qry = 'SELECT Id ';
                qry += 'FROM ' + objectName + ' ';
                qry += 'WHERE ' + statusFieldName  + ' = :invStatus ';
                qry += 'AND Name >= :minRangeWithMask ';
                qry += 'ORDER BY Name ';
            }
        }

        return Database.getQueryLocator(qry);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.Savepoint sp = Database.setSavepoint();

        List<c2g.CODAAPICommon.Reference> refs = new List<c2g.CODAAPICommon.Reference>();

        try
        {
            for(SObject so : scope)
            {
                documentsPosted.add(so.Id);
                refs.add( c2g.CODAAPICommon.getRef( so.Id, null ) );
            }

            if(documentType == ParallelCreateAndPostDocumentsService.SINV)
            {
                c2g.CODAAPISalesInvoice_8_0.BulkPostInvoice( null, refs );
            }
            else if(documentType == ParallelCreateAndPostDocumentsService.SCRN)
            {
                c2g.CODAAPISalesCreditNote_8_0.BulkPostCreditNote( null, refs );
            }
            else if(documentType == ParallelCreateAndPostDocumentsService.CE)
            {
                c2g.CODAAPICashEntry_6_0.BulkPostCashEntry( null, refs );
            }
            else
            {
                throw new ParallelCreateAndPostDocumentsService.ParallelException('Not able to run Post. Your document type is ' + documentType + ' and it should be ' + ParallelCreateAndPostDocumentsService.SINV + ', ' + ParallelCreateAndPostDocumentsService.SCRN + ' or ' + ParallelCreateAndPostDocumentsService.CE + '.');
            }
        }
        catch (Exception e)
        {
            Database.rollback(sp);

            //@UVES, REMOVE BELLOW LINE AND LEAVE THE ONE THAT IS RIGHT NOT COMMENTED DURING MERGE PROCESS
            errorMessages.add('Error during Post process; Exception: ' + e.getMessage() +  '\n' + e.getStackTraceString() + '\n' + e.getLineNumber() + '\n\n');
            //errorMessages.add('Error during Post process; Exception: ' + e.getMessage() + '\n\n');
        }
    }

    public void finish(Database.BatchableContext BC)
    {
        //Send email for each execution
        ParallelCreateAndPostDocumentsService.sendEmailAfterPost(documentType, jobPos, errorMessages, documentsPosted);

        if(jobPos >= 2)
        {
            Map<Integer, String> minAndMaxWithMask = ParallelCreateAndPostDocumentsService.calculateMinAndMaxWithMask(maskValues, 0, (maxRange+1));

            Integer remainingRecords = ParallelCreateAndPostDocumentsService.calculateRemainingDocumentsToPost(objectName, statusFieldName, minAndMaxWithMask.get(2));

            if(remainingRecords > 0)
            {
                Boolean canRun = ParallelCreateAndPostDocumentsService.canRunAJob(false,'ParallelPostDocumentsBatch');

                if(canRun)
                {
                    ParallelPostDocumentsBatch postDocuments = new ParallelPostDocumentsBatch(documentType, scopeSize, nameMask, (maxRange+1), 3);
                    Database.executeBatch(postDocuments, scopeSize);
                }
            }
        }
    }
}