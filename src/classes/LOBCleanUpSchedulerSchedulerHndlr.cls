global class LOBCleanUpSchedulerSchedulerHndlr implements LOBCleanUpScheduler.LOBCleanUpSchedulerInterface {
    global void execute(SchedulableContext sc) {
        LOBRecordDeleteBatch objLOB = new LOBRecordDeleteBatch();
        Database.executeBatch(objLOB,2000);
    }   
}