global class MOLIProcessforEffective implements Database.Batchable<sObject>, Database.Stateful{
    global Date dtRunDate;
    global String dtBD;
    global String strSOQLCond;
    global Map<Id,Map<Id,String>> mapDeleteTalusOLI;
    
    global MOLIProcessforEffective(Date dtBillingDate) {
        dtRunDate = dtBillingDate;     
        dtBD = String.valueOf(dtBillingDate);
        System.debug('Testingg dtBillingDate '+dtBillingDate);
    }
    
    global MOLIProcessforEffective(Date dtBillingDate, String strSOQL) {
        dtRunDate = dtBillingDate;   
        dtBD = String.valueOf(dtBillingDate);
        strSOQLCond = strSOQL;
        System.debug('Testingg dtBillingDate '+dtBillingDate);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
      String query ='SELECT MOLI_Package_Name__c, MOLI_Package_Item_Quantity__c, Product2__r.Addon_Type__c, Migration_Directory_Heading1__c, Migration_Directory_Heading2__c, Migration_Directory_Heading3__c, Migration_Directory_Heading4__c, Opportunity__r.Account_Primary_Canvass__r.Billing_Entity__c, Order__r.Billing_Anniversary_Date__c, MOLI_DM_Opportunity_Close_Date__c, Core_Opportunity_Line_ID__c , Id, Name, Patch_Complete_Date__c,IsUDACChange__c,Seniority_Date__c, Billing_Contact__c, Talus_DFF_Id__c,'+
'MOLI_Original_Package_ID_External__c, Talus_Subscription_ID__c,Distribution_Area__c,Billing_End_Date__c, Billing_Frequency__c, Billing_Partner__c,MOLI_BCC_Applied__c,'+
'MOLI_BCC_Duration__c,MOLI_BCC_Name__c,MOLI_BCC_Redeemed__c,MOLI_Berry_Cares_Certificate_ID__c,Billing_Start_Date__c, Cancellation__c,'+ 
'Canvass__c,MOLI_Exception_Discount__c,Digital_Product_Requirement__c, Product2__r.Is_IBUN_Bundle_Product__c,'+ 
'Discount__c, Description__c, ListPrice__c, Product2__r.Media_Type__c, Opportunity__c,Opportunity__r.Close_Date__c,Order_Line_Item__c, Order__c,'+
'Product2__r.Inventory_Tracking_Group__c, FulfillmentDate__c, Order_Group__c, Payment_Duration__c,'+ 
'MOLI_Payment_Method__c,Product2__c, Total_Prorated_Days_for_contract__c,Prorate_Stored_Value__c, Quantity__c, Prorate_Credit__c, Total_Prorate__c,'+ 
'Status__c, Prorate_Credit_Days__c, Talus_Go_Live_Date__c,Quote_Signed_Date__c,Quote_Signing_Method__c, UnitPrice__c, Sent_to_fulfillment_on__c,'+  
'line_Status__c,Order_Line_Total__c,Product_Type__c, Current_Rate__c, Package_ID__c,P4P_Price_Per_Click_Lead__c,'+ 
'Telco__c, Order_Line_Item__r.Digital_Product_Requirement__c,Cancelation_Fee__c, zero_Cancel_Fee_Requested__c,'+  
'zero_Cancel_Fee_Approved__c,Directory_Heading__c,Parent_ID__c,Parent_ID_of_Addon__c,Account__c,Is_P4P__c,Opportunity_line_Item_id__c,'+ 
'Effective_Date__c,P4P_Billing__c,Package_ID_External__c,Action_Code__c,Geo_Type__c, Geo_Code__c, Parent_Line_Item__c, Inactive__c, Completed__c,Order_Line_Item__r.Digital_Product_Requirement__r.Bundle__c, '+ 
'(Select zip4__c, web_page_to_ads_enabled__c,video_url__c, vid_add_on__c,udac_sales_message__c,udac_points__c, udac_headings__c, udac_email__c,'+  
'time_zone_description__c, testimonials__c, target_interests__c, target_gender__c, target_age_range__c, suggested_email_addresses__c,'+  
'suggested_domain_name__c, sms_short_code__c, slogans__c, signup_url__c, signup_source__c, show_operating_hours__c, setup_notes__c,'+  
'setup_fee__c, services__c, send_tutorials__c, send_reports__c, send_alerts__c, selling_propositions__c,'+  
'reserved_keyword__c, promotion_url_desc__c, '+  
'promotion_label__c, promotion_click_url__c, promotion_category__c, promo_code__c,'+  
'opt_out_local_profile__c, opt_in_domain_email__c, omit_phone_ind__c, omit_address_ind__c,'+  
'mobile_opt_out__c, mediapackage_title__c, mediapackage_logo_imageurl__c, mediapackage_location__c, mediapackage_imageurl_3__c,'+  
'mediapackage_imageurl_2__c, mediapackage_imageurl_1__c, mediapackage_description__c, logo_url__c, langs_spoken__c,'+  
'is_open_247__c, is_legacy__c, industry__c, image_url__c,hours__c, graphic_pao__c, graphic_logo__c, graphic_banner__c, duration_continuous__c,'+  
'dir_codes__c,desired_service_term__c, desired_customer_type__c, contact_phone_number_office__c,'+  
'contact_phone_number_fax__c,certifications__c, category__c, business_url__c, business_state__c, business_service_radius__c,'+  
'business_service_area__c, business_postal_code__c, business_phone_number_office__c, business_name__c, business_industries__c,'+  
'business_email__c, business_city__c, business_address2__c, business_address1__c, business_about__c, budget_type__c,'+  
'budget_dailyspendlimit__c, budget_cpm__c, budget_cpc__c, budget_cpa__c,  budget_allowBalanceRollover__c,add_ons__c,'+  
'accepted_payments__c, Years_in_Business__c,'+  
'Why_would_a_potential_customer_seek_out__c, Provide_2_3_Paragraphs_About_Business__c,'+ 
'When_are_you_NOT_available__c,What_sets_you_apart_from_your_competitor__c,'+  
'What_is_your_slowest_season__c, What_is_your_Online_Marketing_Objective__c, What_is_your_busiest_season__c, What_are_the_FAQs_you_receive__c,'+
'WT__c, WF__c, Vendor_Status__c,Vanity_Telephone_Number_Description_4__c,'+  
'Vanity_Telephone_Number_Description_3__c, Vanity_Telephone_Number_Description_2__c, Vanity_Telephone_Number_Description_1__c,'+   
'Vanity_Telephone_Number_4__c, Vanity_Telephone_Number_3__c, Vanity_Telephone_Number_2__c, Vanity_Telephone_Number_1__c, '+  
' UserNameAuto__c, URN_text__c, URN_number__c, URL_Transfer_Info_if_Not_Berry_Owned__c, '+  
'Tracking_Phone_Website__c, Tracking_Phone_SEM__c, Toll_Free_Telephone_Number_Description_4__c,'+  
'Toll_Free_Telephone_Number_Description_3__c, Toll_Free_Telephone_Number_Description_2__c, Toll_Free_Telephone_Number_Description_1__c,'+  
'Toll_Free_Telephone_Number_4__c, Toll_Free_Telephone_Number_3__c, Toll_Free_Telephone_Number_2__c, Toll_Free_Telephone_Number_1__c,'+  
'ThT__c, ThF__c, '+  
'Template_ID__c, Talus_Subscription_Id__c,'+  
'Talus_DFF_Id__c, Tagline__c, TT__c, TF__c, Surname__c, Suppress_Address_on_Website_c__c, Submitted__c, Submit_To_Yodle__c,  '+
'Status__c,Spec_Art_Request__c, '+  
'Snaptag_Start_Date__c, Snaptag_ID__c, Snaptag_End_Date__c, SnT__c, SnF__c, Show_Map_on_LP__c, Show_Location_on_LP__c,'+  
'Services_to_Use_in_Online_Ads_3_c__c, Services_to_Use_in_Online_Ads_2_c__c, Services_Not_Highlighted__c, Service_Products__c, Sequence__c,'+  
'Sales_Rep_Phone__c, Sales_Rep_Email__c,'+  
'ST__c, SF__c, Response_Message_sent_via_Text__c,'+  
'Response_Message_if_sent_via_Email_App__c, RecordTypeId, '+  
'RCF__c, Proof__c, Proof_Contact__c,'+  
'DFF_Proof_Contact_Email_Address__c, Production_notes__c,'+  
'Production_Notes_for_PrintAdOrderGraphic__c, Production_Notes_for_Logo__c, Production_Notes_for_Banner_Graphic__c,'+  
'Product_Type__c, Print_Ad_URL__c, Price_Range__c, Previous_URN_Number__c,'+  
'Pgroup__c,'+  
'Paper_or_Email__c,'+  
'Pages_Contracted__c, Pager_Number_Description_1__c, Pager_Number_Description_2__c, Pager_Number_Description_3__c, Pager_Number_Description_4__c,'+
'Pager_Number_4__c, Pager_Number_3__c, Pager_Number_2__c, Pager_Number_1__c, PAO__c,'+
'P4P_Forwarded_Number__c, OwnerId, Order_Text_Misc_Data__c, '+
'Order_Row_Amend__c,OrderLineItemID__c,OpportunityID__c,'+  
'One_Thing_That_Sets_Company_Apart__c, Omit_Address__c,'+
'Notes1__c, New_Customer_Reason__c, '+   
'ModificationOrderLineItem__c, Mobile_Number_Description_4__c,'+
'Mobile_Number_Description_3__c, Mobile_Number_Description_2__c,Mobile_Number_Description_1__c, Mobile_Number_4__c, Mobile_Number_3__c,'+  
'Mobile_Number_2__c, Mobile_Number_1__c, Misc_Data__c, Miles_Status__c,'+
'Miles33_ImportAdvert__c, Migrate_to_Miles_33__c, '+  
'MT__c, MF__c,Logo_XGP__c,Logo_Graphic_Url__c, Logo_Graphic_Link__c, Logo_Graphic_Dist_Url__c,'+
'Log_on_name__c, LocalVox_Category__c, List_3_Services_to_Use_in_Online_Ads_1__c, '+
'LastActivityDate,Keyword_5__c, Keyword_4__c, Keyword_3__c, Keyword_2__c, Keyword_1__c,'+
'Issue_Number__c,Is_Website_Currently_Live__c, Is_Multied__c, IsDeleted,'+
'Insert_net_price__c, '+  
'Id, Heading_5__c, Heading_4__c, Heading_3__c, Heading_2__c, Heading_1__c,'+ 
'Have_you_claimed_your_Google_Plus_Local__c, Graphics_Complete_Date__c, Graphic_Pao_Url__c,'+  
'Geographic_Service_Area__c, '+  
'Fulfillment_Submit_Status__c, Fulfillment_Start_Date__c, Fulfillment_Profile__c,'+  
'Fulfillment_End_Date__c, Fornames__c, Formatted_stop_number__c, Formatted_box_number__c, Forenames__c, Five_Key_Talking_Points_Slide_Show__c,'+  
'Final_Status__c,Fax_Numbers_Description_1__c, Fax_Numbers_Description_4__c, Fax_Numbers_Description_3__c,'+  
'Fax_Numbers_Description_2__c,Fax_Number_4__c, Fax_Number_3__c, Fax_Number_2__c, Fax_Number_1__c, Facebook_URL__c, FT__c, FSD_Status__c,'+  
'FF__c, FED_Status__c, Extra_Domain_Name__c, '+  
'Emergency_Number_Description_4__c,'+
'Emergency_Number_Description_3__c, Emergency_Number_Description_2__c, Emergency_Number_Description_1__c, Emergency_Number_4__c, Emergency_Number_3__c,'+
'Emergency_Number_2__c, Emergency_Number_1__c,Email_Setting__c, Email_Description_4__c, Email_Description_3__c, Email_Description_2__c,'+
'Email_Description_1__c, Email_Address_4__c,Email_Address_3__c, Email_Address_2__c, Email_Address_1__c, '+  
'Do_you_have_a_blog_at_your_website__c, Do_you_have_a_Facebook_Twitter_Account__c, Discounts_Specials_Free_Services__c, '+  
'Directory_Number__c, Directory_Edition__c, '+  
'Destination_Phone_Website__c, Destination_Phone_SEM__c, Destination_Number__c, '+  
'Data_Fulfillment_Form__c, Customer_Telephone_Country_Code__c, '+  
'Contract_number__c,Contract_key__c,'+
'List_Products_Services_to_Promote__c, Contact__c, '+  
'Competitor_3__c, Competitor_2__c, Competitor_1__c, Common_Business_Names_3__c,'+  
'Common_Business_Names_2__c, Common_Business_Names_1__c, Color_Scheme__c, Clicks_Contracted__c,'+  
'Category_1__c, '+
'Budget_Contracted__c, Brands__c,'+   
'Best_Time_of_Day_to_be_Reached__c, Best_Method_of_Contact__c, Banner_Url__c,'+  
'Banner_Link__c, Banner_Dist_Url__c, AutoNumber__c, Associations__c, '+  
'Amenities__c, Also_Known_As_aka_5__c, Also_Known_As_aka_4__c,'+  
'Also_Known_As_aka_3__c, Also_Known_As_aka_2__c, Also_Known_As_aka_1__c, '+  
'After_Hours_Number_Description__c, After_Hours_Number_Description_4__c, After_Hours_Number_Description_3__c, After_Hours_Number_Description_2__c,'+  
'After_Hours_Number_4__c, After_Hours_Number_3__c, After_Hours_Number_2__c, After_Hours_Number_1__c,'+  
'Affiliation_1__c, Affiliation_2__c, Affiliation_3__c, Affiliation_4__c,'+  
'Adversite_URL_Mirror_URL__c, Additional_URL_Description_4__c, Additional_URL_Description_3__c,'+  
'Additional_URL_Description_2__c, Additional_URL_Description_1__c, Additional_URL_4__c, Additional_URL_3__c, Additional_URL_2__c, Additional_URL_1__c,'+  
'Additional_Telephone_Number__c, Additional_Telephone_Number_4__c, Additional_Telephone_Number_3__c, Additional_Telephone_Number_2__c,'+  
'Additional_Telephone_Description__c, Additional_Telephone_Description_4__c, Additional_Telephone_Description_3__c, Additional_Telephone_Description_2__c,'+
'Omit_Address_Description__c, Payment_Options__c, Account__c, Neighborhoods__c'+  
' From Data_Fulfillment_Forms__r)'+   
'FROM Modification_Order_Line_Item__c WHERE (((Action_Code__c = \'New\' or IsUDACChange__c=false) AND Effective_Date__c <='+dtBD+ ') OR (IsUDACChange__c= true and Patch_Complete_Date__c <='+dtBD+ ')) and Completed__c=false and Inactive__c=false';

        if(strSOQLCond != null && strSOQLCond != '') {
            query = query+strSOQLCond;
        }
        
        query=query+' Order By MOLI_Payment_Method__c,Account__c';
        
        System.debug('Testingg query '+query);
        return Database.getQueryLocator(query);
    }  
    
    global void execute(Database.BatchableContext bc, List<Modification_Order_Line_Item__c> listOLI) {
      
        if(!Test.isRunningTest()) {
            ModifyOrderLineItemProcessor_v1.ProcessTimedMOLI(listOLI);
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        /*if(setDeleteTalusOLIId != null && setDeleteTalusOLIId.size() > 0) {
            System.debug('Testingg setDeleteTalusOLIId '+setDeleteTalusOLIId);
            DeleteFulfillmentBatch objDF = new DeleteFulfillmentBatch(setDeleteTalusOLIId);
            database.executebatch(objDF,5);
        }*/
         Batch_Size__c MOLIBatch = Batch_Size__c.getInstance('MOLI');
         AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
         //String[] toAddresses = new String[] {a.CreatedBy.Email};
         if(a.NumberOfErrors > 0) {
            futureCreateErrorLog.createErrorRecordBatch(a.ExtendedStatus, 'MOLI batch process status :'  + a.Status+'. The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures.', 'MOLI batch process');
            CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'MOLI batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.');
         }else if(a.JobItemsProcessed != a.TotalJobItems) {
            futureCreateErrorLog.createErrorRecordBatch('Either the batch job was aborted or Job failed to process due to internal error', 'MOLI batch process status : ' + a.Status+'. The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
                ' batches with '+ a.NumberOfErrors + ' failures.', 'MOLI batch process');
            
            CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'MOLI batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.');
            
        }
        else {
            CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'MOLI batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures.');
        }
        
        if(a.NumberOfErrors > 0 && MOLIBatch.Size__c != 1) {
            MOLIProcessforEffective objMOLIProc;
            if(strSOQLCond != null && strSOQLCond != '') {
                objMOLIProc = new MOLIProcessforEffective(dtRunDate, strSOQLCond);
            } else{
                objMOLIProc = new MOLIProcessforEffective(dtRunDate);
            }
            Database.executeBatch(objMOLIProc, 1);
            MOLIBatch.Size__c = 1;
            update MOLIBatch;
        } else {
            Batch_Size__c objBatch = Batch_Size__c.getInstance('SetCancel');
            Database.executeBatch(new SetCancelCheckBoxBatchController(), Integer.valueOf(objBatch.Size__c));
            MOLIBatch.Size__c = 50;
            update MOLIBatch;
        }
    }
}