global class SortAsPopulationBatch Implements Database.Batchable < sObject > {

    //ATP-4249 Added isCanceled__c = false && DPSection 
    String soql;
    String whereCondition;
    Id directoryId;
    set<Id> setSectionId;
    Galley_Staging_Job__c objGSJ;
   /* string DPSection;
    List<Directory_Section__c> DSs = new  List<Directory_Section__c>();
    List<Pagination_Job__c> PJs = new list <Pagination_Job__c>();
    String year;
    global SortAsPopulationBatch(string DPsectionName,List<Directory_Section__c> DSsValue,List<Pagination_Job__c> PJsValue,String yearValue)
    {
        if(!string.IsBlank(DPsectionName)){
            DPSection = DPsectionName;
            DSs.addall(DSsValue);
            PJs.addall(PJsValue);
            year =yearValue;
        }
    }
    */
    global SortAsPopulationBatch() {        
    }
    
    global SortAsPopulationBatch(Galley_Staging_Job__c GSJ, String whereCondition, Id directoryId) {
        objGSJ = new Galley_Staging_Job__c();
        this.objGSJ = GSJ; 
        this.whereCondition = whereCondition;       
        this.directoryId = directoryId;
    }
    
    global SortAsPopulationBatch(Galley_Staging_Job__c GSJ, String whereCondition, Id directoryId, set<Id> setSectionId) {
        objGSJ = new Galley_Staging_Job__c();
        setSectionId = new set<Id>();
        this.objGSJ = GSJ; 
        this.whereCondition = whereCondition;       
        this.directoryId = directoryId;
        this.setSectionId = setSectionId;
    }
    
    global Database.Querylocator start(Database.BatchableContext bc) {
  /*  if(string.IsBlank(DPSection) && DSs.isEmpty() && PJs.isEmpty())
    soql = 'SELECT Sort_As__c, Id, Sort_As_FORMULA__c FROM Order_Line_Items__c WHERE Sort_As_Check_Test__c = TRUE AND Sort_As_FORMULA__c != null  AND isCanceled__c = false order by Anchor_Listing_Caption_Header__c desc';
else
    soql = 'SELECT Sort_As__c, Id, Sort_As_FORMULA__c FROM Order_Line_Items__c WHERE Sort_As_Check_Test__c = TRUE AND Sort_As_FORMULA__c != null  AND isCanceled__c = false and Directory_Section__c =: DPSection order by Anchor_Listing_Caption_Header__c desc';
  */
        soql = 'SELECT Sort_As__c, Id, Sort_As_FORMULA__c FROM Order_Line_Items__c WHERE Sort_As_Check_Test__c = TRUE AND Sort_As_FORMULA__c != null  AND isCanceled__c = false ';
        //soql = 'SELECT Sort_As__c, Id, Sort_As_FORMULA__c FROM Order_Line_Items__c ';
              
        if(String.isNotBlank(directoryId)) {
            soql += 'AND Directory__c =: directoryId ';             
        }    
        
        soql +=  'ORDER BY Anchor_Listing_Caption_Header__c desc';
        system.debug(soql);
        return Database.getQueryLocator(soql);
    }

    global void execute(Database.BatchableContext bc, List < Order_Line_Items__c > listOLI) {
        List<Order_line_Items__c> listUpdateOLI = new List <Order_line_Items__c> ();
        for(Order_Line_Items__c OLI : listOLI) {
            if(OLI.Sort_As__c != OLI.Sort_As_FORMULA__c) {
                OLI.Sort_As__c = OLI.Sort_As_FORMULA__c;
                listUpdateOLI.add(OLI);
            }
        }
        if(listUpdateOLI.size() > 0) {
            update listUpdateOLI;
        }
    }

    global void finish(Database.BatchableContext bc) {
        
        
        if(objGSJ != null) {
            List<Community_Section_Abbreviation__c> CASlst = CommunitySectionAbbreviationSOQLMethods.fetchCommSecAbb(setSectionId);
            //set<string> setDS = new set<string>();
            set<string> setCity = new set<string>();
            map<string,Community_Section_Abbreviation__c> mapstrCSA = new map<string,Community_Section_Abbreviation__c>();
            if(CASlst.size()>0) {
                for(Community_Section_Abbreviation__c iteratorCSA : CASlst) {
                    if(iteratorCSA.Directory_Section__c != null && iteratorCSA.Community_Name__c != null) {
                        mapstrCSA.put(iteratorCSA.Directory_Section__c+iteratorCSA.Community_Name__c,iteratorCSA);
                    }
                    /*if(iteratorCSA.Directory_Section__c != null){
                        setDS.add(iteratorCSA.Directory_Section__c);   
                    }*/
                    if(iteratorCSA.Community_Name__c != null){
                        setCity.add(iteratorCSA.Community_Name__c);   
                    }
                }
            }
            //if(setDS.size()> 0) {
                DirPaginationSortAndCityServiceBatch apcsBatch= new DirPaginationSortAndCityServiceBatch(objGSJ, whereCondition,setCity,mapstrCSA);
                Database.executeBatch(apcsBatch, 2000);
            //}
        } else {
            AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
            if(a.NumberOfErrors > 0) {
                futureCreateErrorLog.createErrorRecordBatch(a.ExtendedStatus, 'SortAsPopulationBatch batch process status :'  + a.Status+'. The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
                    ' batches with '+ a.NumberOfErrors + ' failures.', 'SortAsPopulationBatch batch process');
                CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'SortAsPopulationBatch batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
                    ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.');
            }
        }
        
    //Added for ATP-4249
     
        
        /*if(objGSJ != null && String.isNotBlank(whereCondition)) {
            DirectoryPaginationSortAsFieldUpdate obj = new DirectoryPaginationSortAsFieldUpdate(objGSJ, whereCondition);
            Database.executeBatch(obj, 2000);
        }*/
    }
}