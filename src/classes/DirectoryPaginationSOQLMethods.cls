public class DirectoryPaginationSOQLMethods {
  
    public static List<Directory_Pagination__c> getDirPaginationByUnderCaptions(Set<String> setUnderCaption) {
        return [SELECT Sequence_in_Section__c, Directory_Heading__c, UDAC_Group__c, Id, Anchor_Ref__c, DP_OLI_Anchor_Caption_Header__c FROM Directory_Pagination__c 
                WHERE DP_OLI_Anchor_Caption_Header__c IN : setUnderCaption ORDER BY Sequence_in_Section__c];
    }
    
    public static List<Directory_Pagination__c> getDirPaginationByUnderCaption(Set<String> setUnderCaption) {
        return [SELECT Sequence_in_Section__c, Directory_Heading__c, UDAC_Group__c, Id, Anchor_Ref__c, DP_OLI_Anchor_Caption_Header__c FROM Directory_Pagination__c 
                WHERE DP_OLI_Anchor_Caption_Header__c IN : setUnderCaption ORDER BY Sequence_in_Section__c];
    }
    
    public static list<Directory_pagination__c> getDirPaginationBySLId(set<Id> setSLId) {
        return [Select Id,Sequence_in_Section__c,Sequence_Padded__c,Reference_Listing__c,Directory_Section__c,Incorrect_Setup_Excluded_from_XML__c,DP_Incorrect_Setup_Excluded_XML_Reason__c,Scoped_Caption_Header__c,
                Caption_Header__c,Caption_Member__c,Section_Code__c,Section_Type__c,Directory_Code__c,Type__c,Scoped_Indent_Order__c,Under_Caption__c,DP_Under_Caption_Anchor_UDAC__c,
                Is_Cross_Reference__c,UDAC__c,Extra_Line_Type_Product__c,TCAP_Trademark_Line__c,Listing__c,Anchor__c,Display_Ad__c,spacead__c,Coupon_Ad__c,Leader_Ad__c,
                DP_UnderCaption_s_Listing__c,DP_Product__c,Continuous_Service_Order_Appearance__c,Directory_Heading__c,Directory_Heading_Code__c,Section_Heading_Mapping_ID__c,Scoped_Caption_Member__c,DP_SLs_Under_Caption__c,DP_Listing__c from Directory_Pagination__c where Reference_Listing__c IN : setSLId];
    }
    
    public static List<Directory_Pagination__c> getDirPaginationByUnderCaptionId(Set<Id> underCapIdSet) {
        return [select id,DP_UnderCaption_s_Listing__c,DP_SLs_Under_Caption__c from Directory_Pagination__c where DP_SLs_Under_Caption__c  IN :underCapIdSet];
    }
   
}