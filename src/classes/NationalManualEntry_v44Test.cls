@IsTest(SeeAllData=true)
public class NationalManualEntry_v44Test {
     public static testMethod void natManualCommonLinesTest() {
            Test.StartTest();
            
        Account CMRAcctReject = TestMethodsUtility.generateCMRAccount();
        CMRAcctReject.accountnumber='1234';
        CMRAcctReject.National_Credit_Status__c='Rejected';
        CMRAcctReject.Is_Active__c=true;
        insert CMRAcctReject;
        
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1234';
        CMRAcct.National_Credit_Status__c='Approved';
        CMRAcct.Is_Active__c=true;
        insert CMRAcct;
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='2345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS.CMR_Name__c = CMRAcct.id;
        NSOS.Client_Name__c = ClientAcct.id;
        NSOS.Client_Name_Text__c = ClientAcct.Name;
        NSOS.Client_Number__c = ClientAcct.Client_Number__c;
        NSOS.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS.Directory_Edition_Number__c = '4444';
        NSOS.Directory_Version__c = '1234';
        NSOS.Directory__c = dir.Id;
        NSOS.Directory_Number__c = dir.Directory_Code__c;
        NSOS.Is_Ready__c=false;
        insert NSOS;
        
        
        National_Staging_Line_Item__c NSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLI.Line_Number__c = '11122';
        NSLI.UDAC__c = prod.ProductCode;
        NSLI.National_Graphics_ID__c='GRAPH1234567';
        NSLI.Is_Changed__c=true;
        insert NSLI;
        
        Advice_Query__c adviceQuery = TestMethodsUtility.createAdviceQuery(NSLI);
        Advice_Query__c adviceQueryExist = TestMethodsUtility.createAdviceQuery(NSLI);
        
        ApexPages.CurrentPage().getParameters().put('id', NSOS.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI.Id);   
        string NSLIDs=string.valueof(NSLI.Id);
        Cookie NSLICookie = new Cookie(NSOS.Id,NSLIDs,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookie});
        
        ApexPages.StandardController controller = new ApexPages.StandardController(NSOS);
        NationalManualEntry_V44 obj = new NationalManualEntry_V44(controller);
        obj.advID = adviceQuery.Id;
        
        Test.StopTest();
        
        National_Staging_Order_Set__c NSOSNew= TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOSNew.CMR_Name__c = CMRAcctReject.id;
        NSOSNew.Client_Name__c = ClientAcct.id;
        NSOSNew.Client_Name_Text__c = ClientAcct.Name;
        NSOSNew.Client_Number__c = ClientAcct.Client_Number__c;
        NSOSNew.CMR_Number__c = CMRAcctReject.CMR_Number__c;
        NSOSNew.Directory_Edition_Number__c = '4444';
        NSOSNew.Directory_Version__c = '1234';
        NSOSNew.Directory__c = dir.Id;
        NSOSNew.Directory_Number__c = dir.Directory_Code__c;
        NSOSNew.Is_Ready__c=false;
        NSOSNew.TRANS_Code__c='T';
        NSOSNew.Override__c=false;
        insert NSOSNew;
        
        
        National_Staging_Line_Item__c NSLINew = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOSNew);
        NSLINew.Line_Number__c = '11122';
        NSLINew.UDAC__c = prod.ProductCode;
        NSLINew.National_Graphics_ID__c='GRAPH1234567';
        NSLINew.Is_Changed__c=true;
        NSLINew.Standing_Order__c=true;
        insert NSLINew;
        
        string NSLIDNew=string.valueof(NSLINew.Id);
        Cookie NSLICookieNew = new Cookie(NSOSNew.Id,NSLIDNew,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew});
        ApexPages.CurrentPage().getParameters().put('id', NSOSNew.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLINew.Id);   
        ApexPages.StandardController controllerNew = new ApexPages.StandardController(NSOSNew);
        NationalManualEntry_V44 objNew = new NationalManualEntry_V44(controllerNew);
        objNew.setErrorRowNo.add(1);
        objNew.redirectEditPage();
        objNew.getToFromList();
        objNew.createNewAccount(NSOSNew);
        objNew.valueChanged();
        objNew.checkError(1);
        objNew.checkError(2);
        objNew.addRow();
        objNew.productByUDAC();
        objNew.overrideNSOS();
        objNew.fetchDSIds(new set<id>{dirSec.Id},dir.Id);
        string NSLIDNew1=string.valueof(NSLINew.Id);
        Cookie NSLICookieNew1 = new Cookie(NSOSNew.Id,NSLIDNew,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew1});
        objNew.lockNationalOrderSet();
        
        Locked_National_Order__c  lock=new Locked_National_Order__c (Is_Locked__c=true,Locked_By__c=UserInfo.getUserId(),National_Staging_Order_Set__c=NSOSNew.Id);
        insert lock;
        objNew.lstLockedOrders.add(lock);
        string NSLIDNew2=string.valueof(NSLINew.Id);
        Cookie NSLICookieNew2 = new Cookie(NSOSNew.Id,NSLIDNew2,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew2});
        objNew.lockNationalOrderSet();
        string NSLIDNew3=string.valueof(NSLINew.Id);
        Cookie NSLICookieNew3 = new Cookie(NSOSNew.Id,NSLIDNew3,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew3});
        objNew.UnlockNationalOrderSet();
        objNew.removeEntry();
        objNew.Cancel();
        objNew.addLineItems();
        objNew.setErrorRowNo.add(1);
        objNew.addLineItems();
        obj.fetchDirectoryByCode();
        obj.fetchDEByLSANo();
        obj.NatLinLstObj.add(NSLINew);
        obj.UDAC=prod.ProductCode;
        obj.productByUDAC();
       }
       
       public static testMethod void natManualNegativeTest3(){
    
     Test.StartTest();
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1234';
        CMRAcct.Delinquency_Indicator__c=true;
        CMRAcct.Is_Active__c=true;
        insert CMRAcct;
        
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='2345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS.CMR_Name__c = CMRAcct.id;
        NSOS.Client_Name__c = null;
        NSOS.Client_Name_Text__c = ClientAcct.Name;
        NSOS.Client_Number__c = ClientAcct.Client_Number__c;
        NSOS.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS.Directory_Edition_Number__c = '1234';
        NSOS.Directory_Version__c = '1234';
        NSOS.Directory__c = dir.Id;
        NSOS.Directory_Number__c = dir.Directory_Code__c;
        NSOS.Late_Order__c=true;
        NSOS.Date__c=null;
        insert NSOS;
        
        National_Staging_Line_Item__c NSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLI.Line_Number__c = '11122';
        NSLI.UDAC__c = prod.ProductCode;
        insert NSLI;
        
        National_Staging_Line_Item__c NSLINegative = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLINegative.Line_Number__c = '1112';
        NSLINegative.UDAC__c = prod.ProductCode;
        NSLINegative.Directory_Section__c=null;
        NSLINegative.Directory_Heading__c=null;
        NSLINegative.National_Pricing__c=null;
        NSLINegative.Discount__c=null;
        insert NSLINegative;
        
        Advice_Query__c adviceQuery = TestMethodsUtility.createAdviceQuery(NSLI);
        Advice_Query__c adviceQueryExist = TestMethodsUtility.createAdviceQuery(NSLINegative);
        
        National_Staging_Order_Set__c NSOS1 = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS1.CMR_Name__c = CMRAcct.id;
        NSOS1.Client_Name__c = null;
        NSOS1.Client_Name_Text__c = ClientAcct.Name;
        NSOS1.Client_Number__c = ClientAcct.Client_Number__c;
        NSOS1.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS1.Directory_Edition_Number__c = '1234';
        NSOS1.Directory_Version__c = '1234';
        NSOS1.Directory__c = dir.Id;
        NSOS1.Directory_Number__c = dir.Directory_Code__c;
        NSOS1.Late_Order__c=true;
        NSOS1.Date__c=null;
        NSOS1.TRANS_Code__c='T';
        insert NSOS1;
        
        National_Staging_Line_Item__c NSLINegative1 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS1);
        NSLINegative1.Line_Number__c = '1112';
        NSLINegative1.UDAC__c = prod.ProductCode;
        NSLINegative1.Directory_Section__c=null;
        NSLINegative1.Directory_Heading__c=null;
        NSLINegative1.National_Pricing__c=null;
        NSLINegative1.Discount__c=null;
        NSLINegative1.Standing_Order__c = true;
        NSLINegative1.Prior_UDAC_Group__c='TEST';
        insert NSLINegative1;
        
        National_Staging_Order_Set__c NSOS2 = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS2.CMR_Name__c = CMRAcct.id;
        NSOS2.Client_Name__c = null;
        NSOS2.Client_Name_Text__c = ClientAcct.Name;
        NSOS2.Client_Number__c = ClientAcct.Client_Number__c;
        NSOS2.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS2.Directory_Edition_Number__c = '1234';
        NSOS2.Directory_Version__c = '1234';
        NSOS2.Directory__c = dir.Id;
        NSOS2.Directory_Number__c = dir.Directory_Code__c;
        NSOS2.Late_Order__c=true;
        NSOS2.Date__c=null;
        NSOS2.TRANS_Code__c='X';
        insert NSOS2;
        
        National_Staging_Line_Item__c NSLINegative2 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS2);
        NSLINegative2.Line_Number__c = '1112';
        NSLINegative2.UDAC__c = prod.ProductCode;
        NSLINegative2.Directory_Section__c=null;
        NSLINegative2.Directory_Heading__c=null;
        NSLINegative2.National_Pricing__c=null;
        NSLINegative2.Discount__c=null;
        NSLINegative2.Standing_Order__c = true;
        insert NSLINegative2;
        
        
        Locked_National_Order__c  lock=new Locked_National_Order__c (Is_Locked__c=true,Locked_By__c=UserInfo.getUserId(),National_Staging_Order_Set__c=NSOS.Id);
        insert lock;
        ApexPages.CurrentPage().getParameters().put('id', null);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLINegative.Id);     
        ApexPages.StandardController controller = new ApexPages.StandardController(NSOS);
        
        string NSLIDNew=string.valueof(NSLINegative.Id);
        Cookie NSLICookieNew = new Cookie(NSOS.Id,NSLIDNew,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew});
        
        NationalManualEntry_V44 obj = new NationalManualEntry_V44(controller);
        obj.lstLockedOrders.add(lock);
        obj.setErrorRowNo.add(1);
        obj.validateOLI();
        obj.processOLI();
        obj.save();
        obj.setErrorRowNo.add(1);
        obj.fetchClientbyNumber();
        obj.addAdviceQuery();
        ApexPages.CurrentPage().getParameters().put('save_new','1');
        obj.processOLI();
        obj.onValidationLoad();
        lock.Is_Locked__c=false;
        update lock;
        string NSLIDNew1=string.valueof(NSLINegative.Id);
        Cookie NSLICookieNew1 = new Cookie(NSOS.Id,NSLIDNew1,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew1});
        
        obj.lockNationalOrderSet();
        obj.setErrorRowNo.add(1);
        obj.fetchAdviceLines();
        obj.setErrorRowNo.add(1);
        obj.fetchDEByLSANo();
        Test.StopTest();
        
        User u1=TestMethodsUtility.createUser();
        system.runas(u1){
            
        string NSLIDNew2=string.valueof(NSLINegative.Id);
        Cookie NSLICookieNew2 = new Cookie(NSOS.Id,NSLIDNew2,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew2});
        obj.onload();
        
        
        ApexPages.CurrentPage().getParameters().put('id', NSOS1.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLINegative1.Id);     
        ApexPages.StandardController controller3 = new ApexPages.StandardController(NSOS1);
        string NSLIDNew3=string.valueof(NSLINegative1.Id);
        Cookie NSLICookieNew3 = new Cookie(NSOS1.Id,NSLIDNew3,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew3});
        NationalManualEntry_V44 obj1 = new NationalManualEntry_V44(controller3);
        obj1.onValidationLoad();
        obj1.fetchCMRbyNumber();
        obj1.fetchClientbyNumber();
        
        ApexPages.CurrentPage().getParameters().put('id', NSOS2.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLINegative2.Id);     
        ApexPages.StandardController controller4 = new ApexPages.StandardController(NSOS2);
        string NSLIDNew4=string.valueof(NSLINegative2.Id);
        Cookie NSLICookieNew4 = new Cookie(NSOS2.Id,NSLIDNew4,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew4});
        NationalManualEntry_V44 obj2 = new NationalManualEntry_V44(controller4);
        obj2.onValidationLoad();
        }
  }
  public static testMethod void CoveringProcessOLI(){
    Test.StartTest();
        ID cmrAccRTID = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCMRRT, CommonMessages.accountObjectName);
        
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1234';
        CMRAcct.Delinquency_Indicator__c=false;
        CMRAcct.Is_Active__c=false;
        CMRAcct.National_Credit_Status__c='Reject';
        CMRAcct.RecordTypeId=cmrAccRTID;
        insert CMRAcct;
        
        Account CMRAcct1 = TestMethodsUtility.generateCMRAccount();
        CMRAcct1.accountnumber='1234';
        CMRAcct1.Delinquency_Indicator__c=false;
        CMRAcct1.Is_Active__c=false;
        CMRAcct1.National_Credit_Status__c='Reject';
        CMRAcct1.RecordTypeId=cmrAccRTID;
        insert CMRAcct1;
        
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='2345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        //Product2 prod = TestMethodsUtility.createproduct();
        Product2 prod = new Product2();
        prod.Name = 'Test';
        prod.ProductCode='TEST';
        prod.IsActive = true;
        prod.Family = CommonMessages.printRGU;
        prod.RGU__c = CommonMessages.printRGU;
        prod.Product_Type__c = 'Print';
        prod.Print_Product_Type__c='display';
        insert prod;
        
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS.CMR_Name__c = CMRAcct1.id;
        NSOS.Client_Name__c = ClientAcct.id;
        NSOS.Client_Name_Text__c = ClientAcct.Name;
        NSOS.Client_Number__c = ClientAcct.Client_Number__c;
        NSOS.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS.Directory_Edition_Number__c = '4444';
        NSOS.Directory_Version__c = '1234';
        NSOS.Directory__c = dir.Id;
        NSOS.Directory_Number__c = dir.Directory_Code__c;
        NSOS.Late_Order__c=true;
        NSOS.Approved_to_Process__c=false;
        insert NSOS;
        
        National_Staging_Line_Item__c NSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLI.Line_Number__c = '11122';
        NSLI.UDAC__c = prod.ProductCode;
        insert NSLI;
        
        National_Staging_Line_Item__c NSLINegative = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLINegative.Line_Number__c = '1112';
        NSLINegative.UDAC__c = prod.ProductCode;
        NSLINegative.Directory_Section__c=null;
        NSLINegative.Directory_Heading__c=null;
        NSLINegative.National_Pricing__c=null;
        NSLINegative.Discount__c=null;
        NSLINegative.Seniority_Date__c=null;
        insert NSLINegative;
        
        string NSLIDNew1=string.valueof(NSLINegative.Id);
        Cookie NSLICookieNew1 = new Cookie(NSOS.Id,NSLIDNew1,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew1});
        
        ApexPages.currentPage().getParameters().put('NSLIID', NSLINegative.Id);  
        ApexPages.CurrentPage().getParameters().put('id', NSOS.Id);   
        ApexPages.StandardController controller = new ApexPages.StandardController(NSOS);
        NationalManualEntry_V44 obj = new NationalManualEntry_V44(controller);  
        Test.StopTest();        
        National_Staging_Order_Set__c NSOSNew = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOSNew.CMR_Name__c = CMRAcct.id;
        NSOSNew.Client_Name__c = ClientAcct.id;
        NSOSNew.Client_Name_Text__c = ClientAcct.Name;
        NSOSNew.Client_Number__c = ClientAcct.Client_Number__c;
        NSOSNew.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOSNew.Directory_Edition_Number__c = '4444';
        NSOSNew.Directory_Version__c = '1234';
        NSOSNew.Directory__c = dir.Id;
        NSOSNew.Directory_Number__c = dir.Directory_Code__c;
        NSOSNew.Late_Order__c=true;
        NSOSNew.Date__c=Date.today();
        NSOSNew.Is_Ready__c=false;
        NSOSNew.TRANS_Code__c='0';
        insert NSOSNew;
        
        National_Staging_Line_Item__c NSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOSNew);
        NSLI1.Line_Number__c = '11122';
        NSLI1.UDAC__c = prod.ProductCode;
        NSLI1.Standing_Order__c=true;
        insert NSLI1;
        
        National_Staging_Line_Item__c NSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOSNew);
        NSLI2.Line_Number__c = '1112';
        NSLI2.UDAC__c = prod.ProductCode;
        NSLI2.Directory_Section__c=null;
        NSLI2.Directory_Heading__c=null;
        NSLI2.National_Pricing__c=null;
        NSLI2.Discount__c=null;
        NSLI2.Standing_Order__c=true;
        insert NSLI2;
        
        National_Staging_Line_Item__c NSLI3= TestMethodsUtility.generateNationalStagingLineItemCreation(NSOSNew);
        NSLI3.Line_Number__c = '1112';
        NSLI3.UDAC__c = prod.ProductCode;
        NSLI3.Directory_Section__c=dirSec.Id;
        NSLI3.Directory_Heading__c=objDH.Id;
        NSLI3.National_Pricing__c=objDPM.Id;
        NSLI3.Discount__c=null;
        NSLI3.Standing_Order__c=true;
        NSLI3.Listing__c=listing.Id;
        insert NSLI3;
        
        National_Staging_Line_Item__c NSLI4 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOSNew);
        NSLI4.Line_Number__c = '1112';
        NSLI4.UDAC__c = prod.ProductCode;
        NSLI4.Directory_Section__c=null;
        NSLI4.Directory_Heading__c=null;
        NSLI4.National_Pricing__c=null;
        NSLI4.Discount__c=null;
        NSLI4.Standing_Order__c=true;
        insert NSLI4;
        
        string NSLIDNew3=string.valueof(NSLI2.Id+','+NSLI1.Id);
        Cookie NSLICookieNew3 = new Cookie(NSOSNew.Id,NSLIDNew3,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew3});
        
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI2.Id);  
        ApexPages.CurrentPage().getParameters().put('id', NSOSNew.Id);   
        ApexPages.StandardController controller1 = new ApexPages.StandardController(NSOSNew);
        NationalManualEntry_V44 obj1 = new NationalManualEntry_V44(controller1);
        obj1.validateOLI();
  } 
   
   public static testMethod void CoveringProcessOLI2(){
        Test.StartTest();
        ID cmrAccRTID3 = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCMRRT, CommonMessages.accountObjectName);
        
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1234';
        CMRAcct.Delinquency_Indicator__c=false;
        CMRAcct.Is_Active__c=true;
        CMRAcct.National_Credit_Status__c='Approved';
        CMRAcct.RecordTypeId=cmrAccRTID3;
        insert CMRAcct;
        
        Account CMRAcct1 = TestMethodsUtility.generateCMRAccount();
        CMRAcct1.accountnumber='1234';
        CMRAcct1.Delinquency_Indicator__c=false;
        CMRAcct1.Is_Active__c=true;
        CMRAcct1.National_Credit_Status__c='In Progress';
        CMRAcct1.RecordTypeId=cmrAccRTID3;
        insert CMRAcct1;
        
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='2345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = dir.Directory_Code__c;
        objDirEd.Book_Status__c ='NI';
        objDirEd.Name='Test';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        National_Staging_Order_Set__c NSOSNew = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOSNew.CMR_Name__c = CMRAcct.Id;
        NSOSNew.Client_Name__c = ClientAcct.Id;
        NSOSNew.Client_Name_Text__c = ClientAcct.Name;
        NSOSNew.Client_Number__c = ClientAcct.Client_Number__c;
        NSOSNew.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOSNew.Directory_Edition_Number__c = dir.Directory_Code__c;
        NSOSNew.Directory_Version__c = dir.Directory_Code__c;
        NSOSNew.Directory__c = dir.Id;
        NSOSNew.Directory_Number__c = dir.Directory_Code__c;
        NSOSNew.Late_Order__c=true;
        NSOSNew.Date__c=Date.today();
        NSOSNew.Is_Ready__c=false;
        insert NSOSNew;
        ApexPages.StandardController controller11 = new ApexPages.StandardController(NSOSNew);
        NationalManualEntry_V44 obj144 = new NationalManualEntry_V44(controller11);
        obj144.processOLI();
        National_Staging_Line_Item__c NSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOSNew);
        NSLI1.Line_Number__c = '11122';
        NSLI1.UDAC__c = prod.ProductCode;
        NSLI1.National_Graphics_ID__c='GRAPH1234567';
        NSLI1.Is_Changed__c=true;
        insert NSLI1;
        
        string NSLIDNew3=string.valueof(NSLI1.Id+','+NSLI1.Id);
        Cookie NSLICookieNew3 = new Cookie(NSOSNew.Id,NSLIDNew3,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew3});
        
        ApexPages.CurrentPage().getParameters().put('id', NSOSNew.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI1.Id);     
        
        ApexPages.StandardController controller1 = new ApexPages.StandardController(NSOSNew);
        NationalManualEntry_V44 obj1 = new NationalManualEntry_V44(controller1);
        obj1.setErrorRowNo.add(1);
        obj1.fetchClientbyNumber();
        obj1.fetchCMRbyNumber();
        obj1.setErrorRowNo.add(1);
        obj1.fetchDirectoryByCode();
        obj1.setErrorRowNo.add(1);
        obj1.fetchDEByLSANo();
        
       Test.StopTest();
  } 
  
  
  /*
   public static testMethod void natManualCommonLinesTestNeg2() {
            Test.StartTest();
              ID cmrAccRTID2 = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCMRRT, CommonMessages.accountObjectName);
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1784';
        CMRAcct.National_Credit_Status__c='Approved';
        CMRAcct.Is_Active__c=true;
        CMRAcct.RecordTypeId=cmrAccRTID2;
        insert CMRAcct;
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='1345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS23 = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS23.CMR_Name__c = CMRAcct.id;
        NSOS23.Client_Name__c = ClientAcct.id;
        NSOS23.Client_Name_Text__c = ClientAcct.Name;
        NSOS23.Client_Number__c =ClientAcct.Client_Number__c;
        NSOS23.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS23.Directory_Edition_Number__c = '4444';
        NSOS23.Directory_Version__c = '1234';
          NSOS23.Late_Order__c=false;
          NSOS23.Approved_to_process__c=false;
        NSOS23.Directory__c = dir.Id;
        NSOS23.Directory_Number__c = dir.Directory_Code__c;
        NSOS23.Is_Ready__c=true;
        
        insert NSOS23;
        
        
        National_Staging_Line_Item__c NSLI23 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS23);
        NSLI23.Line_Number__c = '11122';
        NSLI23.UDAC__c = prod.ProductCode;
        NSLI23.National_Graphics_ID__c='GRAPH1234567';
        NSLI23.Is_Changed__c=false;
        NSLI23.Directory_Heading__c = objDH.Id;
        NSLI23.National_Pricing__c = dirProdMap.Id;
        NSLI23.Directory_Section__c = dirSec.Id;
        NSLI23.Discount__c = 'Full Price';
        NSLI23.Standing_Order__c = true;
        NSLI23.Product2__c = prod.Id;
        
        insert NSLI23;
        
        National_Staging_Line_Item__c NSLI21 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS23);
        NSLI21.Line_Number__c = '1112';
        NSLI21.UDAC__c =null;
        NSLI21.Directory_Section__c=null;
        NSLI21.Directory_Heading__c=null;
        NSLI21.National_Pricing__c=null;
        NSLI21.Discount__c=null;
        insert NSLI21;
        Advice_Query__c adviceQuery = TestMethodsUtility.createAdviceQuery(NSLI23);
        Advice_Query__c adviceQueryExist = TestMethodsUtility.createAdviceQuery(NSLI23);
        ApexPages.CurrentPage().getParameters().put('id', NSOS23.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI23.Id);     
        ApexPages.StandardController controller = new ApexPages.StandardController(NSOS23);
        NationalManualEntry_V44 obj = new NationalManualEntry_V44(controller);
         obj.NatHdrObj = NSOS23;
         obj.advID = adviceQuery.Id;
       
         obj.processOLI();
         obj.save();
       
        Test.StopTest();
       }
    */ 
    
   public static testMethod void natManualCommonLinesTestAQ() {
        Test.StartTest();
        ID cmrAccRTID1 = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCMRRT, CommonMessages.accountObjectName);
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1784';
        CMRAcct.National_Credit_Status__c='Approved';
        CMRAcct.Is_Active__c=true;
        CMRAcct.RecordTypeId=cmrAccRTID1;
        insert CMRAcct;
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='1345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS24 = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS24.CMR_Name__c = CMRAcct.id;
        NSOS24.Client_Name__c = ClientAcct.id;
        NSOS24.Client_Name_Text__c = ClientAcct.Name;
        NSOS24.Client_Number__c =ClientAcct.Client_Number__c;
        NSOS24.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS24.Directory_Edition_Number__c = '4444';
        NSOS24.Directory_Version__c = '1234';
        NSOS24.Late_Order__c=false;
        NSOS24.Approved_to_process__c=false;
        NSOS24.Directory__c = dir.Id;
        NSOS24.Directory_Number__c = dir.Directory_Code__c;
        NSOS24.Is_Ready__c=true;
        
        insert NSOS24;
        
        National_Staging_Line_Item__c NSLI24 = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS24);
        NSLI24.Line_Number__c = '11122';
        NSLI24.UDAC__c = prod.ProductCode;
        NSLI24.National_Graphics_ID__c='GRAPH1234567';
        NSLI24.Is_Changed__c=false;
         NSLI24.Directory_Section__c=null;
        NSLI24.Directory_Heading__c=null;
        NSLI24.National_Pricing__c=null;
        NSLI24.Discount__c=null;
        NSLI24.Standing_Order__c = true;
        NSLI24.Product2__c =null;
        
        insert NSLI24;
        
        Advice_Query__c adviceQuery = TestMethodsUtility.createAdviceQuery(NSLI24);
        Advice_Query__c adviceQueryExist = TestMethodsUtility.createAdviceQuery(NSLI24);
        ApexPages.CurrentPage().getParameters().put('id', NSOS24.Id);
        Apexpages.currentpage().getparameters().put('index', '0');   
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI24.Id);     
        string NSLIDNew2=string.valueof(NSLI24.Id);
        Cookie NSLICookieNew2 = new Cookie(NSOS24.Id,NSLIDNew2,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{NSLICookieNew2});
        ApexPages.StandardController controller = new ApexPages.StandardController(NSOS24);
        
        NationalManualEntry_V44 obj24 = new NationalManualEntry_V44(controller);
        obj24.NatHdrObj = NSOS24;
        obj24.advID = adviceQuery.Id;
        obj24.processOLI();
       
        Advice_Query__c objAQ = new Advice_Query__c();
        objAQ.Advice_Query__c = NSLI24.id;
        objAQ.Process_Completed__c = false;
        objAQ.Frequent_Comments_Questions__c = 'Advice';
        objAQ.Advice_Query_Options__c = 'Empty Telephone Number';
        objAQ.Free_text__c = 'Testing';
        insert objAQ;
        obj24.advID = objAQ.id;        
        obj24.adviceQueryList= new List<Advice_Query__c>();
        obj24.adviceQueryList.add(objAQ);
        obj24.lstExisAdviceQuery.add(objAQ);
        obj24.nationalStagingLineItemId = NSLI24.id;
        obj24.saveAdviceQuery();
        obj24.editAdvice();
        obj24.setErrorRowNo.add(1);
        
        ApexPages.currentPage().getParameters().put('NSLIID', NSLI24.Id); 
        obj24.addAdviceQuery();
        obj24.advID = objAQ.id;  
        obj24.deleteAdviceQuery();
        Test.StopTest();
       }
     
}