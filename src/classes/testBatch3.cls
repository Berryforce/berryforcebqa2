global class testBatch3 implements Database.Batchable<sObject>{
    global testBatch3(){
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        //String SOQL = 'select DP_Edition_Year__c, Directory_Edition__r.Year__c from Directory_Pagination__c where DP_Edition_Year__c = null';            
        
        String SOQL = 'SELECT id FROM Directory_Listing__c where createdbyid=\'005G0000005hhf4\'';
        
        //String SOQL = 'SELECT id FROM Directory_Pagination__c where createddate > 2015-01-05T00:00:00Z and createdbyid=\'005K0000002MSSp\'';
        
        return Database.getQueryLocator(SOQL);
    }
    global void execute(Database.BatchableContext bc, List<Directory_Listing__c> scope){
    
        delete scope;
    
        /*List<Directory_Pagination__c> lstDPNew = new List<Directory_Pagination__c>();
        for(Directory_Pagination__c iterator : lstDP) {
            iterator.DP_Edition_Year__c = iterator.Directory_Edition__r.Year__c;
            lstDPNew.add(iterator);
        }
        
        update lstDPNew;*/
        
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Batch Process Status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures.');
    }

}