public Class ModifyOrderLineItemProcessor_v1 {
    
    public static void ProcessTimedMOLI(List <Modification_Order_Line_Item__c> MOLIs) {
        list<Order_Line_Items__c> lstUpdateOLI = new list<Order_Line_Items__c>();
        list<Digital_Product_Requirement__c> lstUpdateDFFTemp = new list<Digital_Product_Requirement__c>();
        list<Digital_Product_Requirement__c> lstUpdateDFF = new list<Digital_Product_Requirement__c>();
        Set<id> setNewMOLI = new Set<Id>();        
        
        //added by Mythili
        List<Id> orderItemIdList=new List<Id>();
        List<Modification_Order_Line_Item__c> MOLIListUpdate=new List<Modification_Order_Line_Item__c>();
        List<Line_Item_History__c> historyRecordsToInsert=new List<Line_Item_History__c>();
        Map<Id,Id> mapOliDFF = new Map<Id,Id>(); 
        set<Id> billingContactIdSet=new set<Id>();
        Map<Id,Contact> billingContactMap=null;
        Map<Id,Map<Id,List<Modification_Order_Line_Item__c>>> accIdContactIdMOLIMap=new Map<Id,Map<Id,List<Modification_Order_Line_Item__c>>>();
        
        String recType=[select Id from RecordType where RecordType.Name like 'New_Order_Line_Item'].Id;
        
        //added by Mythili - to add Payment record to account for new MOLIs
        //added a separate loop here because map-billingContactMap has to be passed to copyValueFromMOLItoOLI method in next loop
        for(Modification_Order_Line_Item__c moli : MOLIs){
            if((moli.Action_Code__c=='New' || moli.Action_Code__c==null) && moli.MOLI_Payment_Method__c=='Credit Card'){
                billingContactIdSet.add(moli.Billing_Contact__c);
                if(!accIdContactIdMOLIMap.containsKey(moli.Account__c)){
                    accIdContactIdMOLIMap.put(moli.Account__c,new Map<Id,List<Modification_Order_Line_Item__c>>());
                }
                
                if(!accIdContactIdMOLIMap.get(moli.Account__c).containsKey(moli.Billing_Contact__c)){
                    accIdContactIdMOLIMap.get(moli.Account__c).put(moli.Billing_Contact__c,new List<Modification_Order_Line_Item__c>());
                }
                accIdContactIdMOLIMap.get(moli.Account__c).get(moli.Billing_Contact__c).add(moli);  
            }
        }
        
        if(billingContactIdSet!=null && billingContactIdSet.size()>0){
            billingContactMap=new Map<Id,Contact>([select id,(select id,pymt__Payment_Type__c,pymt__Contact__c from pymt__Payment_Methods__r where pymt__Default__c=true) from Contact where id IN :billingContactIdSet]);
        }
      
        for(Modification_Order_Line_Item__c objMOLI : MOLIs) {
            if(objMOLI.Action_Code__c.equals(CommonMessages.cancelRenewalAction)) {
                lstUpdateDFFTemp.add(new Digital_Product_Requirement__c(Id = objMOLI.Order_Line_Item__r.Digital_Product_Requirement__c, Core_Opportunity_Line_ID__c =  objMOLI.Core_Opportunity_Line_ID__c));
            }
            else {
                if(objMOLI.Action_Code__c.equals('New')) {
                    setNewMOLI.add(objMOLI.Digital_Product_Requirement__c);
                }
                else {
                    lstUpdateDFFTemp.add(new Digital_Product_Requirement__c(Id = objMOLI.Order_Line_Item__r.Digital_Product_Requirement__c, OrderLineItemID__c = null));
                }
            }
            Order_Line_Items__c OLIToUpdate=copyValueFromMOLItoOLI(objMOLI, recType, billingContactMap);
            lstUpdateOLI.add(OLIToUpdate);
            
            /*//creating history records for order line items
            if(!objMOLI.Action_Code__c.equals(CommonMessages.cancelRenewalAction)){
                historyRecordsToInsert.add(lockCloneHandlerController.newOrderLineItemHistory(OLIToUpdate));
            }*/
            
            for(Digital_Product_Requirement__c objMOLIDFF : objMOLI.Data_Fulfillment_Forms__r) {
                if(!objMOLI.Action_Code__c.equals('New')) {
                    objMOLIDFF.OrderLineItemID__c=objMOLI.Order_Line_Item__c;
                }
                objMOLIDFF.ModificationOrderLineItem__c =  null;
                lstUpdateDFFTemp.add(objMOLIDFF);   
            }
           
            objMOLI.Completed__c=true;
            MOLIListUpdate.add(objMOLI);
        }
        
        //system.debug('DFF list to update'+lstUpdateDFFTemp);
        if(lstUpdateOLI.size() > 0) {
            System.debug('Testingg lstUpdateOLI '+lstUpdateOLI);
            upsert lstUpdateOLI;
            
            for(Order_Line_Items__c iterator : lstUpdateOLI) {
                if(setNewMOLI.contains(iterator.Digital_Product_Requirement__c)) {
                    mapOliDFF.put(iterator.Digital_Product_Requirement__c, iterator.id);
                }
            }
            
            for(Digital_Product_Requirement__c iterator : lstUpdateDFFTemp) {
                if(mapOliDFF.containsKey(iterator.id)) {
                    iterator.OrderLineItemID__c=mapOliDFF.get(iterator.id);
                }
                lstUpdateDFF.add(iterator);
            }
            
             if(lstUpdateDFF.size() > 0) {
                 
                update lstUpdateDFF;
                //Commented out by sathish -- As discussed with ready, this process has been done through Submit DFF button on DFF records, Here not required.
                /*
                set<String> setDFFID = new set<String>();
                for(Digital_Product_Requirement__c iterator : lstUpdateDFF) {
                setDFFID.add(iterator.Id);
                }

                //putting the debug statement to see what dffs are sent to FinancialChangesToTalusBulk Class----Reddy
                //system.debug('*******FinalDFFs**********' + setDFFID);

                //String query = 'Select Id, RecordType.DeveloperName from Digital_Product_Requirement__c where Id IN:setDFFID';
                FinancialChangesToTalusBulk objDFFBulk = new FinancialChangesToTalusBulk(setDFFID);
                DataBase.executeBatch(objDFFBulk, 5);
                */
            }
            
           
        }
        
        List<Order_Line_Items__c> lstSelOLIForHistory = [select Name, Billing_Contact__r.Phone, Billing_Contact__r.Name, Billing_Partner__c, Directory_Edition__c, Directory_Section__c, Talus_Go_Live_Date__c, Directory_Heading__r.Directory_Heading_Name__c, Id, Order_Line_Total__c, Payment_Duration__c, Product2__c, ProductCode__c, Product_Type__c, UnitPrice__c, Status__c, Media_Type__c from Order_Line_Items__c where id in : lstUpdateOLI and Action_Code__c != 'Cancel'];
        
        lockCloneHandlerController.orderLineItemHistoryProcess(lstSelOLIForHistory);
        
        /*for(Order_Line_Items__c objOliIter: lstSelOLIForHistory) {
            //creating history records for order line items
            //if(!objOliIter.Action_Code__c.equals(CommonMessages.cancelRenewalAction)){
                historyRecordsToInsert.add(lockCloneHandlerController.newOrderLineItemHistory(objOliIter));
            //}
        }*/
        
        if(MOLIListUpdate.size()>0){
            update MOLIListUpdate;
        }
        
        /*if(historyRecordsToInsert.size()>0){
            insert historyRecordsToInsert;
        }*/
        
        /*if(prevDFFOfOLI.size()>0){
            update prevDFFOfOLI;
        }*/
        
        /*if(setDeleteTalusOLIId.size() > 0) {
            DeleteTalusSubscription.deleteSubscriptionNonFuture(setDeleteTalusOLIId);
        }*/
        
         if(accIdContactIdMOLIMap!=null && accIdContactIdMOLIMap.size()>0){
                createPaymentMethodsForAccount(accIdContactIdMOLIMap,billingContactMap);   
         }
    }
    
    //this method creates new Payment record for each account
    public static void createPaymentMethodsForAccount(Map<Id,Map<id,List<Modification_Order_Line_Item__c>>> accIdContactIdMOLIMap,Map<Id,Contact> billingContactMap){
        List<pymt__PaymentX__c> paymentList=new List<pymt__PaymentX__c>();
        pymt__Payment_Method__c pymntMethod=null;
        List<Modification_Order_Line_Item__c> moliList=null;
        for(Id accId:accIdContactIdMOLIMap.KeySet()){
            for(Id conId:accIdContactIdMOLIMap.get(accId).KeySet()){
                pymntMethod=billingContactMap.get(conId).pymt__Payment_Methods__r[0];
                Decimal totalUnitPrice=0;
                moliList=accIdContactIdMOLIMap.get(accId).get(conId);
                for(Modification_Order_Line_Item__c moli:moliList){
                    if(moli.UnitPrice__c!=null){
                        totalUnitPrice=totalUnitPrice+moli.UnitPrice__c;
                    }
                }
                paymentList.add(newPaymentX(pymntMethod,totalUnitPrice,moliList[0]));
            }
        }
        
        if(paymentList!=null && paymentList.size()>0){
            insert paymentList;
        }
    }
    
    //Populates new Payment record
    public static pymt__PaymentX__c newPaymentX(pymt__Payment_Method__c objPM,Decimal totalUnitPrice,Modification_Order_Line_Item__c moli) {
        pymt__PaymentX__c PCpayment = new pymt__PaymentX__c(Name = string.valueof(CommonMessages.systemDate)+'- Monthly payment', 
                        pymt__Account__c = moli.Account__c, pymt__Contact__c = objPM.pymt__Contact__c, pymt__Amount__c = totalUnitPrice, 
                        pymt__Auth_Amount__c = totalUnitPrice, pymt__Opportunity__c = moli.Opportunity__c, 
                        pymt__Transaction_Type__c = CommonMessages.pymtTransactionType, pymt__currency_iso_code__c = CommonMessages.pymtCurrencyISOCode, 
                        pymt__Date__c = CommonMessages.systemDate, pymt__Scheduled_Payment_Date__c = CommonMessages.systemDate, 
                        pymt__Payment_Type__c = objPM.pymt__Payment_Type__c, pymt__Status__c = CommonMessages.pymtStatus, 
                        pymt__Batch_Processing_Action__c = CommonMessages.pymtBatchProcessingAction);
        
        if(objPM != null) 
        {
            PCpayment.pymt__Payment_Method__c = objPM.Id;
        }
        
            PCpayment.pymt__Processor_Connection__c = CommonMessages.processorConnectionId;
            PCpayment.pymt__Payment_Processor__c = CommonMessages.processorConnectionName;
            return PCpayment;
    }
    

    public static Order_Line_Items__c copyValueFromMOLItoOLI(Modification_Order_Line_Item__c MOLI,String recType, Map<Id,Contact> billingContactMap) {
        Order_Line_Items__c OLI=null;
        if(MOLI.Order_Line_Item__c!=null){
            OLI = new Order_Line_Items__c(id = MOLI.Order_Line_Item__c);
            OLI.Action_Code__c=MOLI.Action_Code__c;
            if(MOLI.Action_Code__c!=CommonMessages.cancelRenewalAction){ 
                OLI.Last_Billing_Date__c = MOLI.Effective_Date__c.addMonths(Integer.valueOf(MOLI.Payment_Duration__c))-1;
            }
         }else{
            OLI = new Order_Line_Items__c();
            OLI.RecordTypeId = recType;
            OLI.Order_Anniversary_Start_Date__c = MOLI.Order__r.Billing_Anniversary_Date__c;
            //added by Mythili for ticket CC-2280
            OLI.Successful_Payments__c=0;    
            
            //Populating linvio paymentmethodId for New Oli
            if(billingContactMap!=null && billingContactMap.size()>0 && MOLI.MOLI_Payment_Method__c=='Credit Card') {
                Contact con=billingContactMap.get(MOLI.Billing_Contact__c);
                if(con!=null && con.pymt__Payment_Methods__r!=null && con.pymt__Payment_Methods__r.size()>0){
                    pymt__Payment_Method__c pymntMethod=con.pymt__Payment_Methods__r[0];
                    OLI.Linvio_Payment_Method_Id__c=pymntMethod.Id;
                }
            }
         }
         
         if(MOLI.Core_Opportunity_Line_ID__c != null) {
            OLI.Core_Opportunity_Line_ID__c = MOLI.Core_Opportunity_Line_ID__c;
            OLI.Opportunity_Close_Date__c = MOLI.MOLI_DM_Opportunity_Close_Date__c;
         }
         if(MOLI.Action_Code__c==CommonMessages.cancelRenewalAction){
            OLI.Cutomer_Cancel_Date__c = MOLI.Effective_Date__c;
            OLI.Cancellation__c=MOLI.Cancellation__c;
           // OLI.isCanceled__c=MOLI.isCanceled__c;
           if(MOLI.Core_Opportunity_Line_ID__c != null && MOLI.Effective_Date__c <= System.today()) {
            OLI.isCanceled__c = true;
           }
        }else{
             /*if(MOLI.ModificationOrderLineItem_Status__c=='Renewed') {
                OLI.OrderLineItem_Status__c=MOLI.ModificationOrderLineItem_Status__c;
            }
            else{
                OLI.OrderLineItem_Status__c='Modified';
            }*/

            OLI.Package_ID__c = MOLI.Package_ID__c;               
            OLI.Current_Rate__c = MOLI.Current_Rate__c;    
            OLI.Telco__c = MOLI.Telco__c;
            if(MOLI.Action_Code__c=='Modify' && MOLI.IsUDACChange__c==true && MOLI.Patch_Complete_Date__c!=null){
                OLI.Effective_Date__c = MOLI.Patch_Complete_Date__c;
            }
            OLI.Effective_Date__c = MOLI.Effective_Date__c;
            
            //OLI.Directory_Edition__c= MOLI.Directory_Edition__c;
            OLI.Quote_signing_method__c = MOLI.Quote_signing_method__c;
            OLI.Quote_Signed_Date__c = MOLI.Quote_Signed_Date__c;
            OLI.Directory_Heading__c = MOLI.Directory_Heading__c;
            OLI.UnitPrice__c = MOLI.UnitPrice__c;
            OLI.Quantity__c = MOLI.Quantity__c;  
            OLI.Product2__c = MOLI.Product2__c;
            OLI.Product_Inventory_Tracking_Group__c = MOLI.Product2__r.Inventory_Tracking_Group__c;
            OLI.Product_Is_IBUN_Bundle_Product__c = MOLI.Product2__r.Is_IBUN_Bundle_Product__c;
            OLI.Media_Type__c = MOLI.Product2__r.Media_Type__c;
            OLI.Canvass__c = MOLI.Canvass__c;
            OLI.Discount__c = MOLI.Discount__c;
            OLI.Digital_Product_Requirement__c = MOLI.Digital_Product_Requirement__c;
            OLI.ListPrice__c = MOLI.ListPrice__c;
            OLI.Order__c = MOLI.Order__c;
            //OLI.Order_Anniversary_Start_Date__c = MOLI.Order_Anniversary_Start_Date__c;
            OLI.Order_Group__c = MOLI.Order_Group__c;
            OLI.Opportunity__c = MOLI.Opportunity__c;
            OLI.Billing_Entity__c = MOLI.Opportunity__r.Account_Primary_Canvass__r.Billing_Entity__c;
            if(MOLI.Core_Opportunity_Line_ID__c == null) {
                OLI.opportunity_close_date__c = MOLI.Opportunity__r.Close_Date__c;
            }
            
            OLI.Geo_Type__c = MOLI.Geo_Type__c;
            OLI.Geo_Code__c = MOLI.Geo_Code__c;
            OLI.Parent_Line_Item__c =MOLI.Parent_Line_Item__c;
            
            //For ticket ATP-2293
            OLI.OLI_BCC_Applied__c=MOLI.MOLI_BCC_Applied__c;
            OLI.OLI_BCC_Duration__c=MOLI.MOLI_BCC_Duration__c;
            OLI.OLI_BCC_Name__c=MOLI.MOLI_BCC_Name__c;
            OLI.OLI_BCC_Redeemed__c=MOLI.MOLI_BCC_Redeemed__c;
            OLI.OLI_Berry_Cares_Certificate_ID__c=MOLI.MOLI_Berry_Cares_Certificate_ID__c;
            //for ticket ATP-2285
            OLI.OLI_Exception_Discount__c=MOLI.MOLI_Exception_Discount__c;
            
            //added by Mythili
            OLI.Zero_Cancel_Fee_Approved__c=MOLI.zero_Cancel_Fee_Approved__c;
            OLI.zero_Cancel_Fee_Requested__c=MOLI.zero_Cancel_Fee_Requested__c;
            OLI.Account__c=MOLI.Account__c;
            OLI.Distribution_Area__c=MOLI.Distribution_Area__c;
            OLI.Is_P4P__c=MOLI.Is_P4P__c;
            OLI.Opportunity_line_Item_id__c=MOLI.Opportunity_line_Item_id__c;
            OLI.FulfillmentDate__c=MOLI.FulfillmentDate__c;
            OLI.P4P_Billing__c=MOLI.P4P_Billing__c;
            OLI.P4P_Price_Per_Click_Lead__c=MOLI.P4P_Price_Per_Click_Lead__c;
            OLI.Package_ID_External__c=MOLI.Package_ID_External__c;
            OLI.OLI_Original_Package_ID_External__c=MOLI.MOLI_Original_Package_ID_External__c;
            OLI.Product_Type__c=MOLI.Product_Type__c;
            OLI.Sent_to_fulfillment_on__c=MOLI.Sent_to_fulfillment_on__c;
            
            OLI.Migration_Directory_Heading1__c= MOLI.Migration_Directory_Heading1__c;
	        OLI.Migration_Directory_Heading2__c= MOLI.Migration_Directory_Heading2__c;
	        OLI.Migration_Directory_Heading3__c= MOLI.Migration_Directory_Heading3__c;
	        OLI.Migration_Directory_Heading4__c= MOLI.Migration_Directory_Heading4__c;
	        
	        OLI.Addon_Type__c = MOLI.Product2__r.Addon_Type__c;
	        OLI.Package_Item_Quantity__c = MOLI.MOLI_Package_Item_Quantity__c;
      		OLI.Package_Name__c=MOLI.MOLI_Package_Name__c;
            
            OLI.Parent_ID__c=MOLI.Parent_ID__c;
            OLI.Parent_ID_of_Addon__c=MOLI.Parent_ID_of_Addon__c;
            OLI.Parent_Line_Item__c=MOLI.Parent_Line_Item__c;
            OLI.Status__c=MOLI.Status__c;
            OLI.Seniority_Date__c=MOLI.Seniority_Date__c;
            OLI.Description__c=MOLI.Description__c;
            OLI.Payment_Duration__c=MOLI.Payment_Duration__c;
            OLI.Payments_Remaining__c=MOLI.Payment_Duration__c;
            OLI.Payment_Method__c=MOLI.MOLI_Payment_Method__c;
            OLI.line_Status__c=MOLI.line_Status__c;
            //OLI.Talus_DFF_Id__c=MOLI.Talus_DFF_Id__c;
            if(String.isNotBlank(MOLI.Talus_Subscription_ID__c)) {
                OLI.Talus_Subscription_ID__c=MOLI.Talus_Subscription_ID__c;
            }
            OLI.Billing_Contact__c=MOLI.Billing_Contact__c;
            OLI.Billing_Frequency__c=MOLI.Billing_Frequency__c;
            OLI.Billing_Partner__c=MOLI.Billing_Partner__c;
            OLI.Payment_Method__c=MOLI.MOLI_Payment_Method__c;
            OLI.Continious_Billing__c = false;
            if(String.isNotBlank(MOLI.Talus_DFF_Id__c)) {        
                OLI.Talus_DFF_Id__c=MOLI.Talus_DFF_Id__c;
            }
            OLI.Talus_Go_Live_Date__c=MOLI.Talus_Go_Live_Date__c;
            //OLI.Talus_Subscription_ID__c=MOLI.Talus_Subscription_ID__c;
            
          
          }
                      
        return OLI;
        
    }
}