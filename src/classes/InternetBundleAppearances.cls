public class InternetBundleAppearances{
    public static void IncreaseDecreaseIBAppearancesNew(list<Order_Line_Items__c> lstOLI, map<Id, Order_Line_Items__c> oldMap) {
        if(CommonVariables.InternetBundleLogic) {
           
            map<String,list<Order_line_items__c>> mapIncreaseStrOli = new map<String,list<Order_line_items__c>>();
            map<String,list<Order_line_items__c>> mapDecreaseStrOli = new map<String,list<Order_line_items__c>>();
            
            //set<String> setComboValue = new set<String>();
            set<Id> setDirectorySectionId = new set<Id>();
            set<Id> setDirectoryHeadingId = new set<Id>();
            system.debug('********OLI List size*********'+lstOLI.size());
            for(Order_line_items__c oli : lstOLI) {
                if(OLI.Media_Type__c != null && OLI.Media_Type__c.equals(CommonMessages.oliPrintProductType)) { 
                    if(oli.Product_Is_IBUN_Bundle_Product__c == true) {
                        if(oli.Directory_section__c != null && oli.Directory_Heading__c != null) {
                            boolean bFlag = true;
                            if(oldMap != null && oli.isCanceled__c == false) {
                                bFlag = false;
                                Order_line_items__c objOldOLI = oldMap.get(oli.Id);
                                if(objOldOLI.Directory_section__c != oli.Directory_section__c || oli.Directory_Heading__c != objOldOLI.Directory_Heading__c) {
                                    bFlag = true;
                                    String oliDirSHStrOld = String.valueOf(objOldOLI.Directory_section__c).substring(0, 15) + String.valueOf(objOldOLI.Directory_Heading__c).substring(0, 15);
                                    //setDecreaseAppearances.add(oliDirSHStrOld);
                                    if(!mapDecreaseStrOli.containsKey(oliDirSHStrOld)){
                                        mapDecreaseStrOli.put(oliDirSHStrOld, new list<order_line_items__c>());
                                    }
                                    mapDecreaseStrOli.get(oliDirSHStrOld).add(oli);
                                    //setComboValue.add(oliDirSHStrOld);
                                    setDirectorySectionId.add(objOldOLI.Directory_section__c);
                                    setDirectoryHeadingId.add(objOldOLI.Directory_Heading__c);
                                }
                            }
                            if(bFlag) {
                                string oliDirSHStr = String.valueOf(oli.Directory_section__c).substring(0, 15) + String.valueOf(oli.Directory_Heading__c).substring(0, 15);                     
                                system.debug('********String combination********'+oliDirSHStr);
                                //setComboValue.add(oliDirSHStr);
                                setDirectorySectionId.add(oli.Directory_section__c);
                                setDirectoryHeadingId.add(oli.Directory_Heading__c);
                                if(oli.isCanceled__c) {
                                    //setDecreaseAppearances.add(oliDirSHStr);
                                    if(!mapDecreaseStrOli.containsKey(oliDirSHStr)){
                                        mapDecreaseStrOli.put(oliDirSHStr, new list<order_line_items__c>());
                                    }
                                    mapDecreaseStrOli.get(oliDirSHStr).add(oli);
                                }
                                else {
                                    //setIncreaseAppearances.add(oliDirSHStr);
                                    if(!mapIncreaseStrOli.containsKey(oliDirSHStr)){
                                        mapIncreaseStrOli.put(oliDirSHStr, new list<order_line_items__c>());
                                    }
                                    mapIncreaseStrOli.get(oliDirSHStr).add(oli);
                                }
                            }
                        }
                    }
                }
            }
            system.debug('********Increase value map**********'+mapIncreaseStrOli.size());
            system.debug('********decrease value map**********'+mapDecreaseStrOli.size());
            map<string,Section_Heading_Mapping__c> mapSHM = new map<string,Section_Heading_Mapping__c>();
            if(setDirectorySectionId.size() > 0 && setDirectoryHeadingId.size() > 0 ) {
                for(Section_Heading_Mapping__c objSHM : SectionHeadingMappingSOQLMethods.getSecHeadMapByDirSecDirHeadIds(setDirectorySectionId,setDirectoryHeadingId)) {
                //[Select Id,Name,Directory_section__c,Directory_Heading__c,Internet_Bundle_Appearances__c,strBundleAppearenceCombo__c from Section_Heading_Mapping__c where strBundleAppearenceCombo__c IN :setComboValue]){
                    system.debug('********String combination SHM********'+objSHM.strBundleAppearenceCombo__c);
                    if(!mapSHM.containsKey(objSHM.strBundleAppearenceCombo__c)) {
                        mapSHM.put(objSHM.strBundleAppearenceCombo__c,objSHM);
                    }
                }
                
                if(mapSHM.size() > 0) {
                    list<Section_Heading_Mapping__c> objSHMUpdateLst = new list<Section_Heading_Mapping__c>();
                    for(string objStr : mapSHM.keyset()) {
                        if(mapIncreaseStrOli.get(objStr) != null) {
                            Section_Heading_Mapping__c objSecHmInc = mapSHM.get(objStr);
                            for(integer i=1;i<=mapIncreaseStrOli.get(objStr).size();i++){
                            system.debug('********Increase value map count**********'+i);
                                //Section_Heading_Mapping__c objSecHmInc = mapSHM.get(objStr);
                                if(objSecHmInc.Internet_Bundle_Appearances__c == null || objSecHmInc.Internet_Bundle_Appearances__c == 0){
                                    objSecHmInc.Internet_Bundle_Appearances__c = 1;
                                }
                                else {
                                    objSecHmInc.Internet_Bundle_Appearances__c = objSecHmInc.Internet_Bundle_Appearances__c + 1;
                                }
                                //objSHMUpdateLst.add(objSecHmInc);
                            }
                            objSHMUpdateLst.add(objSecHmInc);
                        }
                        else if(mapDecreaseStrOli.get(objStr) != null) {
                            Section_Heading_Mapping__c objSecHmDec = mapSHM.get(objStr);
                            for(integer i=1;i<=mapDecreaseStrOli.get(objStr).size();i++){
                            system.debug('********decrease value map count**********'+i);
                                //Section_Heading_Mapping__c objSecHmDec = mapSHM.get(objStr);
                                if(objSecHmDec.Internet_Bundle_Appearances__c == 0 || objSecHmDec.Internet_Bundle_Appearances__c == null ){
                                    objSecHmDec.Internet_Bundle_Appearances__c = 0;
                                }
                                else {
                                    objSecHmDec.Internet_Bundle_Appearances__c = objSecHmDec.Internet_Bundle_Appearances__c - 1;
                                }
                                //objSHMUpdateLst.add(objSecHmDec);
                            }
                            objSHMUpdateLst.add(objSecHmDec);
                        }
                    }
                    system.debug('********SHM records list**********'+objSHMUpdateLst);
                    if(objSHMUpdateLst.size() > 0) {
                        update objSHMUpdateLst;
                        CommonVariables.InternetBundleLogic = false;
                    }
                }
            }
        }
    }
}