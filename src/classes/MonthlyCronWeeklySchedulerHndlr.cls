global class MonthlyCronWeeklySchedulerHndlr implements MonthlyCronWeeklyScheduler.MonthlyCronWeeklySchedulerInterface{
   global void execute(SchedulableContext sc) {
        MonthlyCronCreationBatch obj = new MonthlyCronCreationBatch(system.today() + 7, null, true);
        Database.executeBatch(obj, 2000);
    }  
}