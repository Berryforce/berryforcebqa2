public class NationalOpportunityCreationHandler_v2 {
    
    public static void createNationalOpportunity() 
    {
        list<National_Staging_Order_Set__c> lstNSOS = NationalStagingOrderSetSOQLMethods.getSortedNationalStagingOSWithLineItem();
        map<Id, National_Staging_Order_Set__c> mapNSOS = new map<Id, National_Staging_Order_Set__c>();
        set<String> setUDAC = new set<String>();
        map<ID, list<National_Staging_Line_Item__c>> mapNSOSLI = new map<ID, list<National_Staging_Line_Item__c>>();
        
        Integer iCount = 0;
        Integer iInnerCount = 0;
        Id nationalOPPRTID = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityNationalRT, CommonMessages.opportunityObjectName);
        //map<string,set<id>> mpTransUniqueIdNSOSId = new map<string,set<id>>();
        map<string,id> mpTransUniqueIdOppLIId = new map<string,id>();
        map<id,string> mpNSOSIdTransUniqueId = new map<id,string>();
        set<string> setClient = new set<string>();
        set<string> setCMR = new set<string>();
        set<string> setDirCode = new set<string>();
        set<string> setEditionCode = new set<string>();
        string strTransUniqueId;
        set<Id> setAccount = new set<Id>();
        Integer intTempCount;
        
        for(National_Staging_Order_Set__c iterator : lstNSOS) {
            
            strTransUniqueId = iterator.Client_Number__c + iterator.CMR_Number__c + iterator.Directory_Number__c + iterator.Directory_Edition_Number__c;
            /*if(!mpTransUniqueIdNSOSId.containsKey(strTransUniqueId)) {
                mpTransUniqueIdNSOSId.put(strTransUniqueId, new set<id>());
            }
            mpTransUniqueIdNSOSId.get(strTransUniqueId).add(iterator.id);*/
            mpNSOSIdTransUniqueId.put(iterator.id, strTransUniqueId);
            
            setClient.add(iterator.Client_Number__c);
            setCMR.add(iterator.CMR_Number__c);
            setDirCode.add(iterator.Directory_Number__c);
            setEditionCode.add(iterator.Directory_Edition_Number__c);
            setAccount.add(iterator.CMR_Name__c);
        }
        
        list<OpportunityLineItem> lstExistingOppLI = [select opportunityid, CMR_Number__c, Client_Number__c, Directory__r.Directory_Code__c, Directory_Edition__r.Edition_Code__c from OpportunityLineItem where CMR_Number__c in :setCMR and Client_Number__c in :setClient and Directory__r.Directory_Code__c in :setDirCode and Directory_Edition__r.Edition_Code__c in :setEditionCode];
        
        for(OpportunityLineItem iterator : lstExistingOppLI) {          
            strTransUniqueId = iterator.Client_Number__c + iterator.CMR_Number__c + iterator.Directory__r.Directory_Code__c + iterator.Directory_Edition__r.Edition_Code__c;
            if(!mpTransUniqueIdOppLIId.containsKey(strTransUniqueId)) {
                mpTransUniqueIdOppLIId.put(strTransUniqueId, iterator.opportunityid);
            }
        }
        for(National_Staging_Order_Set__c iterator : lstNSOS) {
            if(iterator.National_Staging_Line_Items__r.size() > 0) {
                mapNSOS.put(iterator.Id, iterator);
                for(National_Staging_Line_Item__c iteratorC : iterator.National_Staging_Line_Items__r) {
                    if(String.isNotBlank(iteratorC.UDAC__c)) {
                        setUDAC.add(iteratorC.UDAC__c);
                    }
                    if(!mapNSOSLI.containsKey(iterator.Id)) {
                        mapNSOSLI.put(iterator.Id, new list<National_Staging_Line_Item__c>());
                    }
                    mapNSOSLI.get(iterator.id).add(iteratorC);
                }
            }
        }
        map<String, Id> mapUDAC = new map<String, Id>();
        if(setUDAC.size() > 0) {
            mapUDAC = getPriceBookEntryByUDAC(setUDAC);
        }
        
        set<Id> setOpportunity = new set<id>();
        Id opportunityId;       
        list<Opportunity> lstUpsertOpp = new list<Opportunity>();
        map<Integer, list<OpportunityLineItem>> mapUpsertOppLineItem = new map<Integer, list<OpportunityLineItem>>();
        list<National_Staging_Order_Set__c> lstNSOSConverted = new list<National_Staging_Order_Set__c>();
        list<OpportunityLineItem> lstOppLI = new list<OpportunityLineItem>();
        map<Integer,Set<id>> mapAutoNoNSOSId = new map<Integer,Set<id>>();
        map<String,Integer> mpUniqueIdCount = new map<String,Integer>();        
        National_Staging_Order_Set__c objNSOS;
        for(ID osID : mapNSOSLI.keySet()) {
            iCount++;
            intTempCount = iCount;
            strTransUniqueId = mpNSOSIdTransUniqueId.get(osID);
            if(!mpTransUniqueIdOppLIId.containsKey(strTransUniqueId)) {
                if(mpUniqueIdCount.containsKey(strTransUniqueId)) {
                    intTempCount = mpUniqueIdCount.get(strTransUniqueId);
                    mapAutoNoNSOSId.get(intTempCount).add(osID);
                }
                else {
                    //Generating opportunity for update records.
                    lstUpsertOpp.add(generateOpportunity(mapNSOS.get(osID), iCount, nationalOPPRTID));
                    mapAutoNoNSOSId.put(iCount, new Set<id> {osID});
                    mpUniqueIdCount.put(strTransUniqueId, iCount);
                }
            }
            else {
                opportunityId = mpTransUniqueIdOppLIId.get(strTransUniqueId);
                objNSOS = new National_Staging_Order_Set__c(Id = osID, Is_Converted__c = true, Opportunity__c = opportunityId);
                lstNSOSConverted.add(objNSOS);
                setOpportunity.add(opportunityId);
            }
            for(National_Staging_Line_Item__c osliID : mapNSOSLI.get(osID)) {
                iInnerCount++;
                if(!mapUpsertOppLineItem.containsKey(intTempCount)) {
                    mapUpsertOppLineItem.put(intTempCount, new list<OpportunityLineItem>());
                }
                //Create/update Opportunity Line Item
                if(mpTransUniqueIdOppLIId.containsKey(strTransUniqueId)) {
                    lstOppLI.add(generateOpportunityLineItem(mapNSOS.get(osID), osliID, iInnerCount, mapUDAC, mpTransUniqueIdOppLIId.get(strTransUniqueId)));
                }
                else {
                    mapUpsertOppLineItem.get(intTempCount).add(generateOpportunityLineItem(mapNSOS.get(osID), osliID, iInnerCount, mapUDAC, null));
                }
            }
        }
        list<OpportunityLineItem> lstUpsertOPPLI = new list<OpportunityLineItem>();
        if(lstUpsertOpp.size() > 0) {
            //Insert/Updating oppotyunity
            upsert lstUpsertOpp;
            
            for(Opportunity iterator : lstUpsertOpp) {
                setOpportunity.add(iterator.Id);
                if(mapUpsertOppLineItem.get(Integer.valueOf(iterator.Auto_Number__c)) != null) {
                    for(OpportunityLineItem iteratorOppLI : mapUpsertOppLineItem.get(Integer.valueOf(iterator.Auto_Number__c))) {
                        //Populating oppotyunity ID on opportunity line item records
                        if(iteratorOppLI.Id == null) {
                            iteratorOppLI.OpportunityId = iterator.Id;
                        }
                        lstUpsertOPPLI.add(iteratorOppLI);
                    }
                }
                if(mapAutoNoNSOSId.containsKey(Integer.valueOf(iterator.Auto_Number__c))) {
                    intTempCount = Integer.valueOf(iterator.Auto_Number__c);
                    for(id osId : mapAutoNoNSOSId.get(intTempCount)) {
                        objNSOS = new National_Staging_Order_Set__c(Id = osId, Is_Converted__c = true, Opportunity__c = iterator.id);
                        lstNSOSConverted.add(objNSOS);
                    }                   
                }
            }
        }
        if(lstOppLI != null && lstOppLI.size() > 0) {
            lstUpsertOPPLI.addAll(lstOppLI);
        }
        //Insert/Updating opportunity line item
        if(lstUpsertOPPLI.size() > 0) {
            upsert lstUpsertOPPLI;
        }
        
        //Creating Order/Order Set/Order Line Item
        if(setAccount.size() > 0 && setOpportunity.size() > 0) {
            NationalOrderCreationHandler_V2.createNationalOrder(setOpportunity, setAccount);
        }
        
        //Updating National Staging Order Set with Status is converted is true.
        if(lstNSOSConverted.size() > 0) {
            update lstNSOSConverted;        
        }
        
        
       // map<ID, map<ID, set<ID>>> mapNSOSLI = new map<ID, map<ID, set<ID>>>();
        /*map<Id, National_Staging_Order_Set__c> mapNSOS = new map<Id, National_Staging_Order_Set__c>();
        map<String, Id> mapUDAC = new map<String, Id>();
        set<Id> setLineId = new set<Id>();
        set<String> setUDAC = new set<String>();
        map<ID, map<String, Id>> mapUDACRecord = new map<ID, map<String, Id>>();
        map<ID, map<String, set<ID>>> mapNSOSLI = new map<ID, map<String, set<ID>>>();
        for(National_Staging_Order_Set__c iterator : lstNSOS) 
        {
            mapNSOSLI.put(iterator.Id, new map<String, set<ID>>());
            mapUDACRecord.put(iterator.Id, new map<String, ID>());
            String strUDAC = '';
            for(National_Staging_Line_Item__c iteratorC : iterator.National_Staging_Line_Items__r) 
            {
                if(iteratorC.DAT__c != 'H' ) {
                    if(iteratorC.UDAC__c != null) 
                    {
                        if(!setUDAC.contains(iteratorC.UDAC__c)) 
                        {
                            setUDAC.add(iteratorC.UDAC__c);
                            strUDAC = iteratorC.UDAC__c;
                            mapUDACRecord.get(iterator.Id).put(strUDAC, iteratorC.Id);
                        }
                    }
        
                    if(!mapNSOSLI.get(iterator.Id).containsKey(strUDAC)) 
                    {
                        mapNSOSLI.get(iterator.Id).put(strUDAC, new set<ID>());
                    }
                    mapNSOSLI.get(iterator.Id).get(strUDAC).add(iteratorC.Id);
                }
            }
            
            if(mapNSOSLI.get(iterator.Id).size() <= 0) {
                    mapNSOSLI.remove(iterator.Id);
                    mapNSOS.remove(iterator.Id);
                }
          }  
        
        system.debug('mapNSOSLI : '+ mapNSOSLI);
        
        if(setUDAC.size() > 0) {
            mapUDAC = getPriceBookEntryByUDAC(setUDAC);
        }
        
        system.debug('setLineId : '+ setLineId);
        
        if(setLineId.size() > 0) {
            map<Id, National_Staging_Line_Item__c> mapNSLI = NationalStagingLineItemSOQLMethods.getNationalStagingLineItemByIds(setLineId);
            Integer iCount = 0;
            Integer iInnerCount = 0;
            Id nationalOPPRTID = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityNationalRT, CommonMessages.opportunityObjectName);
            list<Opportunity> lstUpsertOpp = new list<Opportunity>();
            map<Integer, list<OpportunityLineItem>> mapUpsertOppLineItem = new map<Integer, list<OpportunityLineItem>>();
            map<Integer, list<National_Child_Lines__c>> mapUpsertChildLine = new map<Integer, list<National_Child_Lines__c>>();
            list<National_Staging_Order_Set__c> lstNSOSConverted = new list<National_Staging_Order_Set__c>();
            for(ID osID : mapNSOSLI.keySet()) {
                iCount++;
                lstNSOSConverted.add(new National_Staging_Order_Set__c(Id = osID, Is_Converted__c = true));
                lstUpsertOpp.add(generateOpportunity(mapNSOS.get(osID), iCount, nationalOPPRTID));              
                for(ID osliID : mapNSOSLI.get(osID).keySet()) {
                    iInnerCount++;
                    if(!mapUpsertOppLineItem.containsKey(iCount)) {
                        mapUpsertOppLineItem.put(iCount, new list<OpportunityLineItem>());
                    }
                    //Create/update Opportunity Line Item
                    mapUpsertOppLineItem.get(iCount).add(generateOpportunityLineItem(mapNSOS.get(osID), mapNSLI.get(osliID), iInnerCount, mapUDAC));
                    
                    for(ID nroID : mapNSOSLI.get(osID).get(osliID)) {
                        if(!mapUpsertChildLine.containsKey(iInnerCount)) {
                            mapUpsertChildLine.put(iInnerCount, new list<National_Child_Lines__c>());
                        }
                        //Create/update National Chrild Records
                        mapUpsertChildLine.get(iInnerCount).add(generateNationalChildLines(mapNSLI.get(nroID)));
                    }
                }
            }
            
            if(lstUpsertOpp.size() > 0) {
                upsert lstUpsertOpp;
                set<Id> setAccount = new set<Id>();
                set<Id> setOpportunity = new set<id>();
                list<OpportunityLineItem> lstUpsertOPPLI = new list<OpportunityLineItem>();
                for(Opportunity iterator : lstUpsertOpp) {
                    setAccount.add(iterator.AccountId);
                    setOpportunity.add(iterator.Id);
                    if(mapUpsertOppLineItem.get(Integer.valueOf(iterator.Auto_Number__c)) != null) {
                        for(OpportunityLineItem iteratorOppLI : mapUpsertOppLineItem.get(Integer.valueOf(iterator.Auto_Number__c))) {
                            if(iteratorOppLI.Id == null) {
                                iteratorOppLI.OpportunityId = iterator.Id;
                            }
                            lstUpsertOPPLI.add(iteratorOppLI);
                        }
                    }
                }
                
                if(lstUpsertOPPLI.size() > 0) {
                    upsert lstUpsertOPPLI;
                    list<National_Child_Lines__c> lstUpsertNRO = new list<National_Child_Lines__c>();
                    for(OpportunityLineItem iterator : lstUpsertOPPLI) {
                        if(mapUpsertChildLine.get(Integer.valueOf(iterator.Auto_Number__c)) != null) {
                            for(National_Child_Lines__c iteratorNRO : mapUpsertChildLine.get(Integer.valueOf(iterator.Auto_Number__c))) {                               
                                iteratorNRO.Opportunity_Product__c = iterator.Id;
                                lstUpsertNRO.add(iteratorNRO);
                            }
                        }
                    }
                    if(lstUpsertNRO.size() > 0) {
                        upsert lstUpsertNRO;
                    }
                }
                
                system.debug('setAccount' + setAccount);
                system.debug('setOpportunity' + setOpportunity);
                if(setAccount.size() > 0 && setOpportunity.size() > 0) {
                   NationalOrderCreationHandler_V2.createNationalOrder(setOpportunity, setAccount);
                }
                
                if(lstNSOSConverted.size() > 0) {
                    update lstNSOSConverted;
                }
            }
        }*/
    }
    
    private static map<String, Id> getPriceBookEntryByUDAC(set<String> setUDAC) {
        list<PricebookEntry> lstPriceBookEntry = PriceBookEntrySOQLMethods.getPriceBookEntryByUDAC(setUDAC);
        map<String, Id> mapUDAC = new map<String, Id>();
        for(PricebookEntry iterator : lstPriceBookEntry) {
            mapUDAC.put(iterator.ProductCode, iterator.Id);
        }
        return mapUDAC;
    }
    
    private static Opportunity generateOpportunity(National_Staging_Order_Set__c objNSOS, Integer iCount, ID nationalOPPRTID) {
        Opportunity objOpportunity = new Opportunity(AccountId = objNSOS.CMR_Name__c, Auto_Number__c = iCount, StageName = CommonMessages.closedOwnStageOpp, 
            Name = objNSOS.CMR_Name__r.Name +' ' + System.today(), CloseDate = System.today(), Pricebook2Id = system.label.PricebookId,
            RecordTypeId = nationalOPPRTID);
        
        if(objNSOS.Opportunity__c != null) {
            objOpportunity.Id = objNSOS.Opportunity__c;
        }
        return objOpportunity;
    }
    
    private static OpportunityLineItem generateOpportunityLineItem(National_Staging_Order_Set__c objNSOS, National_Staging_Line_Item__c objNSLI, Integer iCount, map<String, ID> mapUDAC, Id opportunityId) {
        OpportunityLineItem objOPPLI = new OpportunityLineItem(Transaction_ID__c = objNSOS.Transaction_ID__c, Trans__c = objNSOS.TRANS_Code__c, Trans_Version__c = objNSOS.Transaction_Version__c, 
                                        Publication_Date__c = objNSOS.Publication_Date__c, 
                                        Directory__c = objNSOS.Directory__c, Directory_Edition__c = objNSOS.Directory_Edition__c, Directory_Section__c = objNSLI.Directory_Section__c, Directory_Heading__c = objNSLI.Directory_Heading__c, Date__c = objNSOS.Date__c, 
                                        Client_Number__c = objNSOS.Client_Number__c, Client_Name__c = objNSOS.Client_Name__c, CMR_Number__c = objNSOS.CMR_Number__c, CMR_Name__c = objNSOS.CMR_Name__c, 
                                        NAT__c = objNSOS.NAT__c, NAT_Client_ID__c = objNSOS.NAT_Client_Id__c, 
                                        Line_Number__c = objNSLI.Line_Number__c, PricebookEntryId = mapUDAC.get(objNSLI.UDAC__c), Action__c = objNSLI.Action__c, DAT__c = objNSLI.DAT__c,
                                        BAS__c = objNSLI.BAS__c, SPINS__c = objNSLI.SPINS__c, Advertising_Data__c = objNSLI.Advertising_Data__c, 
                                        Quantity = 1.0,  Type__c = objNSLI.Type__c, UnitPrice = objNSLI.Sales_Rate__c, Full_Rate__c = objNSLI.Full_Rate_f__c,
                                        Auto_Number__c = iCount, National_Staging_Line_Item__c = objNSLI.Id);
        
        if(opportunityId != null) {
            objOPPLI.opportunityId = opportunityId;
        }
        if(objNSLI.Opportunity_Product__r.size() > 0) {
            for(OpportunityLineItem iterator : objNSLI.Opportunity_Product__r) {
                objOPPLI.Id = iterator.Id;
            }
        }
        return objOPPLI;
    }
    
    private static National_Child_Lines__c generateNationalChildLines(National_Staging_Line_Item__c objNSLI) {
        National_Child_Lines__c objNRO = new National_Child_Lines__c(Type__c = objNSLI.Type__c, SPINS__c = objNSLI.SPINS__c, Line_Number__c = objNSLI.Line_Number__c, DAT__c = objNSLI.DAT__c, BAS__c = objNSLI.BAS__c, 
                                            Advertising_Data__c = objNSLI.Advertising_Data__c, Action__c = objNSLI.Action__c, National_Staging_Line_Item__c = objNSLI.Id);
        if(objNSLI.National_Reference_Object__r.size() > 0) {
            for(National_Child_Lines__c iterator : objNSLI.National_Reference_Object__r) {
                objNRO.Id = iterator.Id;
            }
        }
        return objNRO;
    }
}