@isTest
private class NationalStagingOrderSet_BAIUD_Test {

    static testMethod void NationalStagingOrderSetTestMethod() {
        Test.startTest();
       /* Directory__c dir = TestMethodsUtility.generateDirectory();
        dir.Months_for_National__c = 12;
        insert dir;*/
        Directory__c dir =TestMethodsUtility.createDirectory();  

        National_Staging_Order_Set__c newNSOS1 = TestMethodsUtility.generateNationalStagingOrderSet('Elite RT');
        newNSOS1.Directory__c = dir.Id;
        insert newNSOS1;
        National_Staging_Line_Item__c newNSLI1 = TestMethodsUtility.createNationalStagingLineItemCreation(newNSOS1);
        National_Staging_Order_Set__c newNSOS2 = TestMethodsUtility.generateNationalStagingOrderSet('Elite RT');
        newNSOS2.Directory__c = dir.Id;
        insert newNSOS2;
        National_Staging_Line_Item__c newNSLI2 = TestMethodsUtility.createNationalStagingLineItemCreation(newNSOS2);
        newNSOS1.CMR_Number__c = 'test';
        update newNSOS1;
        Test.stopTest();
    }
}