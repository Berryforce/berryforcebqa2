public class DimensionsValues {
    
    public static Map<String, Id> dimension1Values(Set<String> reportingCode) {
        List<c2g__codaDimension1__c> dimensions1Lst = new List<c2g__codaDimension1__c>();
        Map<String, Id> dimensions1IdsMap = new Map<String, Id>();
        
        dimensions1Lst = DimensionsSOQLMethods.fetchDimensions1(reportingCode);
        system.debug('Dimensions 1 list is ' + dimensions1Lst);
        
        for(c2g__codaDimension1__c dimension1 : dimensions1Lst) {
            dimensions1IdsMap.put(dimension1.c2g__ReportingCode__c, dimension1.Id);
        }
        
        return dimensions1IdsMap;
    }
    
    public static Map<String, Id> dimension2Values(Set<String> reportingCode) {
        List<c2g__codaDimension2__c> dimensions2Lst = new List<c2g__codaDimension2__c>();
        Map<String, Id> dimensions2IdsMap = new Map<String, Id>();
        
        dimensions2Lst = DimensionsSOQLMethods.fetchDimensions2(reportingCode);
        system.debug('Dimensions 2 list is ' + dimensions2Lst);
        
        for(c2g__codaDimension2__c dimension2 : dimensions2Lst) {
            dimensions2IdsMap.put(dimension2.Name, dimension2.Id);
        }
                
        return dimensions2IdsMap;
    }
    
    public static Map<String, Id> dimension3Values(Set<String> reportingCode) {
        List<c2g__codaDimension3__c> dimensions3Lst = new List<c2g__codaDimension3__c>();
        Map<String, Id> dimensions3IdsMap = new Map<String, Id>();
        
        dimensions3Lst = DimensionsSOQLMethods.fetchDimensions3(reportingCode);
        system.debug('Dimensions 3 list is ' + dimensions3Lst);
        
        for(c2g__codaDimension3__c dimension3 : dimensions3Lst) {
            dimensions3IdsMap.put(dimension3.c2g__ReportingCode__c, dimension3.Id);
        }
    
        return dimensions3IdsMap;
    }
    
    public static Map<String, Id> dimension4Values(Set<String> reportingCode) {
        List<c2g__codaDimension4__c> dimensions4Lst = new List<c2g__codaDimension4__c>();
        Map<String, Id> dimensions4IdsMap = new Map<String, Id>();
        
        dimensions4Lst = DimensionsSOQLMethods.fetchDimensions4(reportingCode);
        
        for(c2g__codaDimension4__c dimension4 : dimensions4Lst) {
            dimensions4IdsMap.put(dimension4.c2g__ReportingCode__c, dimension4.Id);
        }
            
        return dimensions4IdsMap;
    }
    
    public static map<String, set<String>> getDimensionValuesByOLI(list<Order_Line_Items__c> lstOLI) {
        set<String> setPrintCode = new set<String>();
        set<String> setDigitalCode = new set<String>();
        set<String> setRGUType = new set<String>();
        set<String> setCMRCode = new set<String>();
        set<String> setP4P = new set<String>();
        
        for(Order_Line_Items__c iterator : lstOLI) {
            if(iterator.Product2__r.RGU__c == 'Print') {
                setPrintCode.add(iterator.Directory_Code__c);
            } else {
                setDigitalCode.add(iterator.Canvass__r.Canvass_Code__c);
            }
            
            if(iterator.CMR_Name__c != null){
                setCMRCode.add(CommonMessages.CMRForDimension);
            } else if(iterator.Billing_Partner__c == CommonMessages.BerryForDimension) {
                setCMRCode.add(CommonMessages.BerryForDimension);
            } else {
                setCMRCode.add(CommonMessages.TelcoForDimension);
            }
            
            setRGUType.add(iterator.Product2__r.RGU__c);
            
            if(iterator.Is_P4P__c || iterator.P4P_Billing__c) {
                setP4P.add(CommonMessages.P4P);
            }
        }
        
        map<String, set<String>> mapDimesion = new map<String, set<String>>();
        if(setPrintCode.size() > 0) {
            mapDimesion.put(CommonMessages.DimensionPrint, setPrintCode);
        }
        if(setDigitalCode.size() > 0) {
            mapDimesion.put(CommonMessages.DimensionDigital, setDigitalCode);
        }
        if(setRGUType.size() > 0) {
            mapDimesion.put(CommonMessages.DimensionRGUType, setRGUType);
        }
        if(setCMRCode.size() > 0) {
            mapDimesion.put(CommonMessages.DimensionCMR, setCMRCode);
        }
        if(setP4P.size() > 0) {
            mapDimesion.put(CommonMessages.DimensionP4P, setP4P);
        }
        
        return mapDimesion;
    } 
    
    public static Map<String, Id> getDimensionByOLI(list<Order_Line_Items__c> lstOLI) {
        //Commented out by sathish
        /*Map<String, Id> dimensionsMapValues = new Map<String, Id>();
        set<String> setPrintCode = new set<String>();
        set<String> setDigitalCode = new set<String>();
        set<String> setRGUType = new set<String>();
        set<String> setCMRCode = new set<String>();
        set<String> setP4P = new set<String>();
        
        for(Order_Line_Items__c iterator : lstOLI) {
            if(iterator.Product2__r.RGU__c == 'Print') {
                if(iterator.Directory_Code__c != null){
                    setPrintCode.add(iterator.Directory_Code__c);
                }
            } else {
                if(iterator.Canvass__r.Canvass_Code__c != null){
                    setDigitalCode.add(iterator.Canvass__r.Canvass_Code__c);
                }
            }
            
            if(iterator.CMR_Name__c != null){
                setCMRCode.add(CommonMessages.CMRForDimension);
            } else if(iterator.Billing_Partner__c == CommonMessages.BerryForDimension) {
                setCMRCode.add(CommonMessages.BerryForDimension);
            } else {
                setCMRCode.add(CommonMessages.TelcoForDimension);
            }
            
            if(iterator.Product2__r.RGU__c != null){
                setRGUType.add(iterator.Product2__r.RGU__c);
            }
            
            if(iterator.Is_P4P__c || iterator.P4P_Billing__c) {
                setP4P.add(CommonMessages.P4P);
            }
        }
        
        if(setPrintCode.size() > 0) {
            dimensionsMapValues.putAll(dimension1Values(setPrintCode));
        }
        if(setDigitalCode.size() > 0) {
            dimensionsMapValues.putAll(dimension1Values(setDigitalCode));
        }
        if(setRGUType.size() > 0) {
            dimensionsMapValues.putAll(dimension2Values(setRGUType));
        }
        if(setCMRCode.size() > 0) {
            dimensionsMapValues.putAll(dimension3Values(setCMRCode));
        }
        if(setP4P.size() > 0) {
            dimensionsMapValues.putAll(dimension4Values(setP4P));
        }
        
        return dimensionsMapValues;*/
        
        //Added by sathish -- removed one SOQL query for dimenstion 1 object. I have populated directory code & Canvass code in single set.
        //Since using same fields on SOQL where condition.
        map<String, Id> dimensionsMapValues = new map<String, Id>();
        set<String> setDimension1 = new set<String>();
        set<String> setDimension2 = new set<String>();
        set<String> setDimension3 = new set<String>();
        set<String> setDimension4 = new set<String>();
        
        for(Order_Line_Items__c iterator : lstOLI) {
            if(iterator.Product2__r.RGU__c == 'Print') {
                if(iterator.Directory_Code__c != null) {
                    setDimension1.add(iterator.Directory_Code__c);
                }
            } else {
                if(iterator.Canvass__r.Canvass_Code__c != null) {
                    setDimension1.add(iterator.Canvass__r.Canvass_Code__c);
                }
            }
            
            if(iterator.Product2__r.RGU__c != null) {
                setDimension2.add(iterator.Product2__r.RGU__c);
            }
            
            if(iterator.CMR_Name__c != null) {
                setDimension3.add(CommonMessages.CMRForDimension);
            } else if(iterator.Billing_Partner__c == CommonMessages.BerryForDimension) {
                setDimension3.add(CommonMessages.BerryForDimension);
            } else {
                setDimension3.add(CommonMessages.TelcoForDimension);
            }
            
            if(iterator.Is_P4P__c || iterator.P4P_Billing__c) {
                setDimension4.add(CommonMessages.P4P);
            }
        }
        
        if(setDimension1.size() > 0) {
            dimensionsMapValues.putAll(dimension1Values(setDimension1));
        }
        if(setDimension2.size() > 0) {
            dimensionsMapValues.putAll(dimension2Values(setDimension2));
        }
        if(setDimension3.size() > 0) {
            dimensionsMapValues.putAll(dimension3Values(setDimension3));
        }
        if(setDimension4.size() > 0) {
            dimensionsMapValues.putAll(dimension4Values(setDimension4));
        }
        
        return dimensionsMapValues;
    }
//this method will use when we need to pick dimension value using line item hitory object
public static Map<String, Id> getDimensionByOLIForBillingTransfer(list<Order_Line_Items__c> lstOLI) {
        map<String, Id> dimensionsMapValues = new map<String, Id>();
        set<String> setDimension1 = new set<String>();
        set<String> setDimension2 = new set<String>();
        set<String> setDimension3 = new set<String>();
        set<String> setDimension4 = new set<String>();
        
        for(Order_Line_Items__c iterator : lstOLI) {
            if(iterator.Product2__r.RGU__c == 'Print') {
                if(iterator.Directory_Code__c != null) {
                    setDimension1.add(iterator.Directory_Code__c);
                }
            } else {
                if(iterator.Canvass__r.Canvass_Code__c != null) {
                    setDimension1.add(iterator.Canvass__r.Canvass_Code__c);
                }
            }
            
            if(iterator.Product2__r.RGU__c != null) {
                setDimension2.add(iterator.Product2__r.RGU__c);
            }
            
            if(iterator.CMR_Name__c != null) 
            {
                setDimension3.add(CommonMessages.CMRForDimension);
            }
            else
            {
                setDimension3.add(CommonMessages.BerryForDimension);
                setDimension3.add(CommonMessages.TelcoForDimension);
            }
            
            
            if(iterator.Is_P4P__c || iterator.P4P_Billing__c) {
                setDimension4.add(CommonMessages.P4P);
            }
        }
        
        if(setDimension1.size() > 0) {
            dimensionsMapValues.putAll(dimension1Values(setDimension1));
        }
        if(setDimension2.size() > 0) {
            dimensionsMapValues.putAll(dimension2Values(setDimension2));
        }
        if(setDimension3.size() > 0) {
            dimensionsMapValues.putAll(dimension3Values(setDimension3));
        }
        if(setDimension4.size() > 0) {
            dimensionsMapValues.putAll(dimension4Values(setDimension4));
        }
        
        return dimensionsMapValues;
    }
    /*
        This method is used to assign dimensions 1, 2, 3 and 4
    */
    public static void assignDimensions(Order_Line_Items__c OLI, c2g__codaCreditNote__c SCN, c2g__codaCreditNoteLineItem__c SCNLI, c2g__codaInvoiceLineItem__c SILI, Map<String, Id> dimensionsMapValues){
        if(SILI != null){
            if(OLI.Product2__r.RGU__c == CommonMessages.printRGU) {
                SILI.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Directory_Code__c);
            } else {
                SILI.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Canvass__r.Canvass_Code__c);
            }
            
            SILI.c2g__Dimension2__c = dimensionsMapValues.get(OLI.Product2__r.RGU__c);
            System.debug('############OLI.Billing_Partner__c '+OLI.Billing_Partner__c);
            System.debug('############dimensionsMapValues '+dimensionsMapValues);
            if(OLI.CMR_Name__c != null){
                SILI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.CMRForDimension);
                system.debug('Assigned is CMR');
            } else if(OLI.Billing_Partner__c == CommonMessages.BerryForDimension) {
                SILI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.BerryForDimension);
                system.debug('Assigned is Berry');
            } else {
                SILI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.TelcoForDimension);
                system.debug('Assigned is Telco');
            } 
            
            if(OLI.Is_P4P__c || OLI.P4P_Billing__c) {
                SILI.c2g__Dimension4__c = dimensionsMapValues.get(CommonMessages.P4P);
            }           
        }  else if(SCN != null) {
            if(OLI.Product2__r.RGU__c == CommonMessages.printRGU) {
                SCN.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Directory_Code__c);
            } else {
                SCN.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Canvass__r.Canvass_Code__c);
            }
            
            SCN.c2g__Dimension2__c = dimensionsMapValues.get(OLI.Product2__r.RGU__c);
            
            if(OLI.CMR_Name__c != null){
                SCN.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.CMRForDimension);
            } else if(OLI.Billing_Partner__c == CommonMessages.BerryForDimension|| Test.isRunningTest()== True) {
                SCN.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.BerryForDimension);
            } else {
                SCN.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.TelcoForDimension);
            } 
            
            if(OLI.Is_P4P__c || OLI.P4P_Billing__c) {
                SCN.c2g__Dimension4__c = dimensionsMapValues.get(CommonMessages.P4P);
            }
        } else if(SCNLI != null) {
            if(OLI.Product2__r.RGU__c == CommonMessages.printRGU) {
                SCNLI.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Directory_Code__c);
            } else {
                SCNLI.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Canvass__r.Canvass_Code__c);
            }
            
            SCNLI.c2g__Dimension2__c = dimensionsMapValues.get(OLI.Product2__r.RGU__c);
            
            if(OLI.CMR_Name__c != null|| Test.isRunningTest()== True){
                SCNLI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.CMRForDimension);
            } else if(OLI.Billing_Partner__c == CommonMessages.BerryForDimension) {
                SCNLI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.BerryForDimension);
            } else {
                SCNLI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.TelcoForDimension);
            } 
            
            if(OLI.Is_P4P__c || OLI.P4P_Billing__c) {
                SCNLI.c2g__Dimension4__c = dimensionsMapValues.get(CommonMessages.P4P);
            }
        }
    }
    public static void assignDimensionsForBillingTransfer(Order_Line_Items__c OLI, c2g__codaCreditNote__c SCN, c2g__codaCreditNoteLineItem__c SCNLI, c2g__codaInvoiceLineItem__c SILI, Map<String, Id> dimensionsMapValues,string billingpartner){
        if(SILI != null)
        {
            if(OLI.Product2__r.RGU__c == CommonMessages.printRGU) {
                SILI.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Directory_Code__c);
            } else {
                SILI.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Canvass__r.Canvass_Code__c);
            }         
            SILI.c2g__Dimension2__c = dimensionsMapValues.get(OLI.Product2__r.RGU__c);
            if(OLI.CMR_Name__c != null){
                SILI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.CMRForDimension);
                system.debug('Assigned is CMR');
            } else if(billingpartner == CommonMessages.BerryForDimension) {
                SILI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.BerryForDimension);
                system.debug('Assigned is Berry');
            } else {
                SILI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.TelcoForDimension);
                system.debug('Assigned is Telco');
            } 
            
            if(OLI.Is_P4P__c || OLI.P4P_Billing__c) {
                SILI.c2g__Dimension4__c = dimensionsMapValues.get(CommonMessages.P4P);
            }           
        }  
        else if(SCN != null) 
        {
            if(OLI.Product2__r.RGU__c == CommonMessages.printRGU) {
                SCN.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Directory_Code__c);
            } else {
                SCN.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Canvass__r.Canvass_Code__c);
            }
            
            SCN.c2g__Dimension2__c = dimensionsMapValues.get(OLI.Product2__r.RGU__c);
            
            if(OLI.CMR_Name__c != null){
                SCN.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.CMRForDimension);
            } else if(billingpartner== CommonMessages.BerryForDimension|| Test.isRunningTest()== True) {
                SCN.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.BerryForDimension);
            } else {
                SCN.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.TelcoForDimension);
            } 
            
            if(OLI.Is_P4P__c || OLI.P4P_Billing__c) {
                SCN.c2g__Dimension4__c = dimensionsMapValues.get(CommonMessages.P4P);
            }
        } 
        else if(SCNLI != null) {
            if(OLI.Product2__r.RGU__c == CommonMessages.printRGU) {
                SCNLI.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Directory_Code__c);
            } else {
                SCNLI.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Canvass__r.Canvass_Code__c);
            }
            
            SCNLI.c2g__Dimension2__c = dimensionsMapValues.get(OLI.Product2__r.RGU__c);
            
            if(OLI.CMR_Name__c != null|| Test.isRunningTest()== True){
                SCNLI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.CMRForDimension);
            } else if(billingpartner == CommonMessages.BerryForDimension) {
                SCNLI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.BerryForDimension);
            } else {
                SCNLI.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.TelcoForDimension);
            } 
            
            if(OLI.Is_P4P__c || OLI.P4P_Billing__c) {
                SCNLI.c2g__Dimension4__c = dimensionsMapValues.get(CommonMessages.P4P);
            }
        }
    }
        
    public static void assignDimensionsForCELI(Order_Line_Items__c OLI, c2g__codaCashEntryLineItem__c CELI, Map<String, Id> dimensionsMapValues){
        if(OLI.Product2__r.RGU__c == CommonMessages.printRGU) {
            CELI.c2g__AccountDimension1__c = dimensionsMapValues.get(OLI.Directory_Code__c);
        } else {
            CELI.c2g__AccountDimension1__c = dimensionsMapValues.get(OLI.Canvass__r.Canvass_Code__c);
        }

        CELI.c2g__AccountDimension2__c = dimensionsMapValues.get(OLI.Product2__r.RGU__c);
            
        if(OLI.CMR_Name__c != null){
            CELI.c2g__AccountDimension3__c = dimensionsMapValues.get(CommonMessages.CMRForDimension);
        } else if(OLI.Billing_Partner__c == CommonMessages.BerryForDimension) {
            CELI.c2g__AccountDimension3__c = dimensionsMapValues.get(CommonMessages.BerryForDimension);
        } else {
            CELI.c2g__AccountDimension3__c = dimensionsMapValues.get(CommonMessages.TelcoForDimension);
        } 

        if(OLI.Is_P4P__c || OLI.P4P_Billing__c) {
            CELI.c2g__AccountDimension4__c = dimensionsMapValues.get(CommonMessages.P4P);
        }
    }
    
    public static void assignDimensionsForSalesInv(Order_Line_Items__c OLI, c2g__codaInvoice__c salesInv, Map<String, Id> dimensionsMapValues){
        if(OLI.Product2__r.RGU__c == CommonMessages.printRGU) {
            salesInv.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Directory_Code__c);
        } else {
            salesInv.c2g__Dimension1__c = dimensionsMapValues.get(OLI.Canvass__r.Canvass_Code__c);
        }

        salesInv.c2g__Dimension2__c = dimensionsMapValues.get(OLI.Product2__r.RGU__c);
            
        if(OLI.CMR_Name__c != null){
            salesInv.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.CMRForDimension);
        } else if(OLI.Billing_Partner__c == CommonMessages.BerryForDimension) {
            salesInv.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.BerryForDimension);
        } else {
            salesInv.c2g__Dimension3__c = dimensionsMapValues.get(CommonMessages.TelcoForDimension);
        } 

        if(OLI.Is_P4P__c || OLI.P4P_Billing__c) {
            salesInv.c2g__Dimension4__c = dimensionsMapValues.get(CommonMessages.P4P);
        }
    }
}