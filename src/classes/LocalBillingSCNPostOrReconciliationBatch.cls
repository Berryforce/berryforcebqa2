global class LocalBillingSCNPostOrReconciliationBatch implements Database.Batchable<sObject>{    
    global Dummy_Object__c objDummy = new Dummy_Object__c();
    global String postOrRecncle;
    global String type=null;
	global Integer startNumber=null;
    global Integer endNumber=null;
    
    global LocalBillingSCNPostOrReconciliationBatch(Dummy_Object__c objDummy, String postOrRecncle) {
        this.objDummy = objDummy;
        this.postOrRecncle = postOrRecncle;
    }
    
    //Telco Reversal Sales Credit Note Posting Batch
    global LocalBillingSCNPostOrReconciliationBatch(String type,String postOrRecncle,Integer startNum,Integer endNum) {
    	this.type=type;
    	this.postOrRecncle=postOrRecncle;
    	this.startNumber=startNum;
    	this.endNumber=endNum;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
    	String strQuery=null;
        set<String> pymtMtds = new Set<String> {'Telco Billing', 'Statement'};
        Date fromDate = objDummy.From_Date__c;
        set<String> transTypes = new Set<String> {'FC - Frequency Change','TD - Billing Transfer Invoice'};
        if(type!='Telco Reversal Sales Credit Note'){
        	strQuery = 'SELECT Id, Reconcile__c FROM c2g__codaCreditNote__c where c2g__CreditNoteStatus__c = \'In Progress\' and SC_P4P__c = false '
                            + 'AND SC_Media_Type__c = \'Print\' AND Transaction_Type__c IN: transTypes AND SC_Payment_Method__c IN: pymtMtds '
                            + 'AND c2g__CreditNoteDate__c =: fromDate';
        }else if(type=='Telco Reversal Sales Credit Note'){
        	strQuery = 'Select id from c2g__codaCreditNote__c where c2g__CreditNoteStatus__c = \'In Progress\' and Telco_Reversal__c=true and ffps_berry__AutoPost__c=true';
        }
        if(startNumber!=null){
            strQuery +=' AND SCR_Posting_Number__c>=:startNumber ';
        }
        if(endNumber!=null){
            strQuery += ' AND SCR_Posting_Number__c<=:endNumber';    
        }
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext bc, List<c2g__codaCreditNote__c > listSI) {
        if(postOrRecncle == 'reconcile') {
            list<c2g__codaCreditNote__c> lstSCNUpdate = new list<c2g__codaCreditNote__c>();
            for(c2g__codaCreditNote__c iterator: listSI){
                lstSCNUpdate.add(new c2g__codaCreditNote__c (Id = iterator.Id, Reconcile__c = true));
            }
            update lstSCNUpdate;
        } else if(postOrRecncle == 'post') {
            set<Id> setSCNID = new set<Id>();
            for(c2g__codaCreditNote__c iterator: listSI){
                setSCNID.add(iterator.Id);
            }
            BillingWizardCommonController.postSalesCreditNote(BillingWizardCommonController.generateCODAAPICommonReference(setSCNID));
        }
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id =:BC.getJobId()];
        /*Map<String, LocalSISCNPostBatchEmails__c> allemails = LocalSISCNPostBatchEmails__c.getAll();
        Set<String> emailaddrSet=new Set<String>();
        emailaddrSet.add(a.CreatedBy.Email);*/
         Set<String> recipientIds = new Set<String>();
         recipientIds.addAll(User_Ids_For_Email__c.getInstance('SISCNPost').User_Ids__c.split(';'));
         recipientIds.add(UserInfo.getUserId());
        String emailaddr='';
        /*for(LocalSISCNPostBatchEmails__c iter : allemails.values()) {
            emailaddrSet.add(iter.email__c);
        }
        List<String> emailaddrLst=new List<String>();
        emailaddrLst.addall(emailaddrSet);*/
        String strErrorMessage = '';
        if(a.NumberOfErrors > 0) {
            strErrorMessage = a.ExtendedStatus;
        }
        for(String str : recipientIds) {
            CommonEmailUtils.sendHTMLEmailForTargetObject(str, 'Local Billing Sales Credit Note Reconciliation Batch  is ' + a.Status,'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'.');
        }
    }
}