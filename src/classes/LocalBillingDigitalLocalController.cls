public with sharing class LocalBillingDigitalLocalController {
    
    public Dummy_Object__c objDummy {get;set;}
    public Digital_Telco_Scheduler__c objDTS {get;set;}
    public list<SelectOption> lstBillingEntity {get;set;}    
    public String strBatchStatusTelco {get;set;}
    public String strSelectedBillingEntity {get;set;}
    public String strNoofAccountAndInvoice {get;set;}
    public String strNoofAccountAndSCN {get;set;}
    public String strSCNBatchStatus{get;set;}
    public String strInvBatchStatus{get;set;}
    public boolean bMIRT {get;set;}
    public boolean bReconciled {get;set;}
    public boolean bInvoicePresent {get;set;}  
    public boolean bPostAllInvoice {get;set;}
    public boolean bEnabledPoller {get;set;}
    public boolean bEnabledPollerForTelco {get;set;}
    public boolean bShowPostFF {get;set;}
    public boolean bShowCreateTelco {get;set;}
    public Decimal SILITotal {get;set;}
    public Id digitalInvoicePostReport {get;set;}
    public Id digitalSCNPostReport {get;set;}
    public String strSIReconcileBatchStatus {get;set;}
    public String strSCNReconcileBatchStatus {get;set;}
    public boolean bReconciliationEnabledPoller {get;set;}
    Set<id> SetSalesCreditnote;
    Set<id> setSIId;
    Map<String,Digital_Telco_Scheduler__c> mpTelcoScheBE;
    Map<id,c2g__codaInvoice__c> MapSalesinvoice;
    Map<id,c2g__codaCreditNote__c> MapofSalesCreditnote;
    Id SCNbatchId;
    Id batchId;
    Id SIReconciliationbatchId;
    Id SCNReconciliationbatchId;
    Public Id LOBReportId {get;set;}
    public Id DigitalSubsequentReconcileReportId{get;set;}
    
    Set<Id> setSIBatchIds = new Set<id>();
    Set<Id> setSCNBatchIds = new Set<id>();
    
    public LocalBillingDigitalLocalController() {
        objDummy = new Dummy_Object__c();
        clearDatas();
        mpTelcoScheBE = new Map<String,Digital_Telco_Scheduler__c>();
        Map<String, Local_Billing_Reports__c> reportIds = Local_Billing_Reports__c.getAll();        
        if(reportIds.get('Digital Invoice Posting')!=null) digitalInvoicePostReport = reportIds.get('Digital Invoice Posting').Report_Id__c;
        if(reportIds.get('Digital SCN Posting')!=null) digitalSCNPostReport = reportIds.get('Digital SCN Posting').Report_Id__c;   
        LOBReportId = Local_Billing_Reports__c.getInstance('DigitalLOBReport').Report_Id__c;
        if(reportIds.get('Digital Subsequent Reconcile Report')!=null) DigitalSubsequentReconcileReportId= reportIds.get('Digital Subsequent Reconcile Report').Report_Id__c;
        
    }    
    
    private void clearDatas() {
        strNoofAccountAndInvoice = concatenateString(0, 0);
        strNoofAccountAndSCN = concatenateSCNString(0, 0);
        strBatchStatusTelco = '';
        strSCNBatchStatus = '';
        strInvBatchStatus = '';
        bPostAllInvoice = false;
        bEnabledPollerForTelco = false;
        bInvoicePresent = false;
        bEnabledPoller = false;
        bShowPostFF = false;
        bShowCreateTelco = false;
        SetSalesCreditnote = new Set<Id>();
        setSIId = new set<Id>();
        strSIReconcileBatchStatus= '';
        strSCNReconcileBatchStatus= '';
        bReconciliationEnabledPoller =false;        
        MapSalesinvoice=new Map<id,c2g__codaInvoice__c>();
        MapofSalesCreditnote =new Map<id,c2g__codaCreditNote__c>();
    }
    
    private String concatenateString(Integer iAccCount, Integer iSICount) {
        return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Invoices for Selections';
    }
    
    private String concatenateSCNString(Integer iAccCount, Integer iSICount) {
        return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Credit Notes for Selections';
    }
    
    public void fetchDirectoryEdition() {
        System.debug('Testingg ');
        lstBillingEntity = new list<SelectOption>();
        if(objDummy.Date__c != null) {
            list<Digital_Telco_Scheduler__c> lstTelcoScheduler = [Select id,Name,Telco__r.Name,Billing_Entity__c,Invoice_To_Date__c,Invoice_From_Date__c,Telco__c,Telco_Receives_EFile__c,
                                                                    XML_Output_Total_Amount__c from Digital_Telco_Scheduler__c where Bill_Prep_Date__c =:objDummy.Date__c];
            if(lstTelcoScheduler.size()>0) {
                for(Digital_Telco_Scheduler__c iterator : lstTelcoScheduler) {
                    lstBillingEntity.add(new SelectOption(iterator.id, iterator.Billing_Entity__c));
                    mpTelcoScheBE.put(iterator.id, iterator);
                }
            }
        }
        System.debug('Testingg lstBillingEntity '+lstBillingEntity);
    }
    
    public void checkInvoicesforDigitalNew() {
        set<Id> setAccountID = new set<Id>();
        set<Id> setAccountSCNID = new set<Id>();
        clearDatas();
        
        if(String.isNotBlank(strSelectedBillingEntity)) {
            SILITotal = 0.0;
            objDTS = mpTelcoScheBE.get(strSelectedBillingEntity);            
            list<c2g__codaInvoice__c> lstSalesInvoice = [SELECT SI_Billing_Partner__c, Customer_Name__c,Reconcile__c,Name,c2g__NetTotal__c 
                                                        FROM c2g__codaInvoice__c 
                                                        where SI_P4P__c = false AND c2g__InvoiceStatus__c = 'In Progress' AND Billing_Entity__c=:objDTS.Billing_Entity__c 
                                                        AND SI_Media_Type__c='Digital' AND  c2g__InvoiceDate__c >=:objDTS.Invoice_From_Date__c AND c2g__InvoiceDate__c <=:objDTS.Invoice_To_Date__c  ];
            
            list<c2g__codaCreditNote__c> lstCreditNote = [Select Name, SC_Billing_Partner__c, c2g__Invoice__r.Name, c2g__NetTotal__c, Customer_Name__c, Id,Reconcile__c 
                                                         From c2g__codaCreditNote__c where Transaction_Type__c in ('TD - Billing Transfer Invoice','FC - Frequency Change') 
                                                         AND c2g__CreditNoteStatus__c='In Progress' and SC_P4P__c =false AND SC_Billing_Entity__c=:objDTS.Billing_Entity__c 
                                                         AND SC_Media_Type__c='Digital' AND  c2g__CreditNoteDate__c >=:objDTS.Invoice_From_Date__c AND c2g__CreditNoteDate__c <=:objDTS.Invoice_To_Date__c];
                        
            for(c2g__codaInvoice__c iterator : lstSalesInvoice) {                    
                if(!MapSalesinvoice.containskey(iterator.id)) {
                    MapSalesinvoice.put(iterator.id,new c2g__codaInvoice__c(id=iterator.Id,Reconcile__c=true));
                }
                if(iterator.SI_Billing_Partner__c != CommonMessages.BerryForDimension) {
                    SILITotal=SILITotal+iterator.c2g__NetTotal__c;
                    if(!setAccountID.contains(iterator.Customer_Name__c)) {                                                 
                        setAccountID.add(iterator.Customer_Name__c);
                    }
                    if(!setSIId.contains(iterator.id)) {
                        setSIId.add(iterator.id);          
                    }
                }
            }
            
            for(c2g__codaCreditNote__c iterator : lstCreditNote) {
                if(!MapofSalesCreditnote.containskey(iterator .id)) {
                     MapofSalesCreditnote.put(iterator.id,new c2g__codaCreditNote__c(id=iterator.Id,Reconcile__c=true));
                }                        
                if(iterator.SC_Billing_Partner__c != CommonMessages.BerryForDimension) {
                    if(!SetSalesCreditnote.contains(iterator.id)) {
                        SetSalesCreditnote.add(iterator.id);
                    }
                    if(!setAccountSCNID.contains(iterator.Customer_Name__c)) {
                        setAccountSCNID.add(iterator.Customer_Name__c);
                    }
                }
            }
        }
        
        System.debug('Testingg setAccountID '+setAccountID);
        strNoofAccountAndInvoice = concatenateString(setAccountID.size(), setSIId.size());
        strNoofAccountAndSCN = concatenateSCNString(setAccountSCNID.size(), SetSalesCreditnote.size());
        if(setSIId.size() > 0 || SetSalesCreditnote.size() > 0) {
            bInvoicePresent = true;
        }
    }
    
    public void updateSIwithReconciledCheckBox() {
        //Commented code by Mythili for ticket CC-2412
       /*   if(MapSalesinvoice.size() > 0) {
               if(!Test.isRunningTest()){ 
                  SIReconciliationbatchId=Database.executeBatch(new LocalBillingSIReconciliationBatch(MapSalesinvoice.keyset()));
               }
                    strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : Queued';
        } */
        //Added by Mythili for CC-2412
        //if(MapSalesinvoice.size() > 0) {
        	setSIBatchIds = new Set<id>();
        	setSCNBatchIds = new Set<id>();
        	Batch_Size__c reconcileBatchSize=Batch_Size__c.getValues('WizardSIReconcile');
            Batch_Size__c defaultBatchSize=Batch_Size__c.getValues('WizardSIPostWithoutSplit');
            List<SplitBatchJobs__c> reqdSplitBatchList=CommonMethods.fetchRequiredRecords('Digital Local');
            if(reqdSplitBatchList!=null && reqdSplitBatchList.size()>0){
                for(SplitBatchJobs__c splitBatch:reqdSplitBatchList){
                    setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Digital Local','reconcile',objDTS,Integer.valueOf(splitBatch.Start_Number__c),Integer.valueOf(splitBatch.End_Number__c),Integer.valueOf(splitBatch.Min_Line_Item_Count__c), Integer.valueOf(splitBatch.Max_Line_Item_Count__c)),Integer.valueOf(reconcileBatchSize.Size__c)));
                }
            }else{   
                setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Digital Local','reconcile',objDTS,null,null,null,null),Integer.valueOf(defaultBatchSize.Size__c)));
            }
            strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : Queued';
        //}
        
        //if(MapofSalesCreditnote.size()>0){
               if(!Test.isRunningTest()){ 
                    setSCNBatchIds.add(Database.executeBatch(new LocalBillingSCNReconciliationBatch(MapofSalesCreditnote.keyset())));
               }
               strSCNReconcileBatchStatus='Status of Reconciliation Credit Note Batch : Queued';    
        //}
          bReconciliationEnabledPoller =true;
        /* 
         update MapSalesinvoice.values();     
         update MapofSalesCreditnote.values();
         if(SetSalesCreditnote.size() > 0 || setSIId.size() > 0) {
            bShowPostFF = true;
        }
        */       
    }
    
    public void apexReconciliationJobStatus() {
    	boolean bStatusSI = LocalBillingCommonMethods.apexBatchJobStatus(setSIBatchIds);
    	if(bStatusSI) {
    		strSIReconcileBatchStatus = 'Status of Reconciliation Invoice Batch : : Completed';
        }
        else {
        	strSIReconcileBatchStatus = 'Status of Reconciliation Invoice Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        boolean bStatusSCN = LocalBillingCommonMethods.apexBatchJobStatus(setSCNBatchIds);
    	if(bStatusSCN) {
    		strSCNReconcileBatchStatus = 'Status of Reconciliation Credit Note Batch : : Completed';
        }
        else {
        	strSCNReconcileBatchStatus = 'Status of Reconciliation Credit Note Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        
        if(bStatusSI && bStatusSCN) {
            bReconciliationEnabledPoller = false;
            bShowPostFF=true;
        }
        /*
        Set<Id> setBatchIds = new Set<id>();
        Boolean bStatus = false;
        if(SIReconciliationbatchId != null) {
            setBatchIds.add(SIReconciliationbatchId);
        }
        if(SCNReconciliationbatchId != null) {
            setBatchIds.add(SCNReconciliationbatchId);
        } 
        Map<id,AsyncApexJob> mapBatchFFJob;
        if(setBatchIds.size() > 0) {
            mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
            if(mapBatchFFJob.containsKey(SIReconciliationbatchId)) {
             strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : ' + mapBatchFFJob.get(SIReconciliationbatchId).Status;
                if(mapBatchFFJob.get(SIReconciliationbatchId).Status == 'Completed') {
                    bStatus = true;
                }
            }
            else {
                bStatus = true;
            }
            if(mapBatchFFJob.containsKey(SCNReconciliationbatchId)) {
                strSCNReconcileBatchStatus= 'Status of Reconciliation Credit Note Batch : ' + mapBatchFFJob.get(SCNReconciliationbatchId).Status;
                if(bStatus && mapBatchFFJob.get(SCNReconciliationbatchId).Status == 'Completed') {
                    bStatus = true;
                }
                else {
                    bStatus = false;
                }
            }
            if(bStatus) {
                bReconciliationEnabledPoller = false;
                bShowPostFF=true;
            }
        }*/
    }
    
    public void postFFInvoice() {       
        if(bPostAllInvoice) {
        	setSIBatchIds = new Set<id>();
        	setSCNBatchIds = new Set<id>();
            if(SetSalesCreditnote.size() > 0) {
               FFCreditNotePostBatchController RefeCreditNotes=new FFCreditNotePostBatchController (SetSalesCreditnote);
               if(!Test.isRunningTest()) {
                    setSCNBatchIds.add(Database.executeBatch(RefeCreditNotes, Integer.valueOf(system.label.LocalBillingPostingBatchSize)));
               }
               bEnabledPoller = true;
               strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : Queued';
            }
            //Modified by Mythili for CC-2412
            if(setSIId.size() > 0) {               
              /*  FFSalesInvoicePostBatchController Refeinvoice= new FFSalesInvoicePostBatchController (setSIId);
                if(!Test.isRunningTest()) { 
                    batchId=Database.executeBatch(Refeinvoice, Integer.valueOf(system.label.LocalBillingPostingBatchSize));
                }
                bEnabledPoller = true;
                strInvBatchStatus = 'Status of Financial Force Invoice Batch : Queued';   */
                Batch_Size__c defaultBatchSize=Batch_Size__c.getValues('WizardSIPostWithoutSplit');
                List<SplitBatchJobs__c> reqdSplitBatchList=CommonMethods.fetchRequiredRecords('Digital Local');
                if(reqdSplitBatchList!=null && reqdSplitBatchList.size()>0){
                    for(SplitBatchJobs__c splitBatch:reqdSplitBatchList){
                        setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Digital Local','post',objDTS,Integer.valueOf(splitBatch.Start_Number__c),Integer.valueOf(splitBatch.End_Number__c),Integer.valueOf(splitBatch.Min_Line_Item_Count__c), Integer.valueOf(splitBatch.Max_Line_Item_Count__c)),Integer.valueOf(splitBatch.Batch_Size__c)));
                    }
                }else{
                    setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Digital Local','post',objDTS,null,null,null,null),Integer.valueOf(defaultBatchSize.Size__c)));
               }
               bEnabledPoller = true;  
               strInvBatchStatus = 'Status of Financial Force Invoice Batch : Queued';  
            }
        }
    }
    
    public void SendTelcoFile() {  
        if(objDTS != null) {
            Digital_Telco_Scheduler__c dts=new Digital_Telco_Scheduler__c(id=objDTS.id,Sent_Telco_File__c=true);
            bEnabledPollerForTelco = true;
            update dts;         
        }
    }
    
    public void apexJobStatus() {
    	boolean bStatusSI = LocalBillingCommonMethods.apexBatchJobStatus(setSIBatchIds);
    	if(bStatusSI) {
    		strInvBatchStatus = 'Status of Financial Force Invoice Batch : : Completed';
        }
        else {
        	strInvBatchStatus = 'Status of Financial Force Invoice Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        boolean bStatusSCN = LocalBillingCommonMethods.apexBatchJobStatus(setSCNBatchIds);
    	if(bStatusSCN) {
    		strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : : Completed';
        }
        else {
        	strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        
        if(bStatusSI && bStatusSCN) {
        	bEnabledPoller = false;
            bShowCreateTelco = true;
        }
        /*
        Set<Id> setBatchIds = new Set<id>();
        Boolean bStatus = false;
        if(batchId != null) {
            setBatchIds.add(batchId);
        }
        if(SCNbatchId != null) {
            setBatchIds.add(SCNbatchId);
        } 
        Map<id,AsyncApexJob> mapBatchFFJob;
        if(setBatchIds.size() > 0) {
            mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
            if(mapBatchFFJob.containsKey(batchId)) {
                strInvBatchStatus = 'Status of Financial Force Invoice Batch : ' + mapBatchFFJob.get(batchId).Status;
                if(mapBatchFFJob.get(batchId).Status == 'Completed') {
                    bEnabledPoller = false;
                    bShowCreateTelco = true;
                }
            }
            if(mapBatchFFJob.containsKey(SCNbatchId)) {
                strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : ' + mapBatchFFJob.get(SCNbatchId).Status;
                if(bStatus && mapBatchFFJob.get(SCNbatchId).Status == 'Completed') {
                    bEnabledPoller = false;
                    bShowCreateTelco = true;
                }
            }
        }*/
    }
    
    public void CheckTelcoOutput() {
        if(objDTS !=null) {
            strBatchStatusTelco = 'XML Telco Invoice Status : InProgress';
            fetchDTSForReport();
        }
    }
    
    public void fetchDTSForReport() {
        objDTS=[select id,Name,Invoice_From_Date__c ,Billing_Entity__c,Invoice_To_Date__c,Telco__c,Telco_Receives_EFile__c,XML_Output_Total_Amount__c from Digital_Telco_Scheduler__c where id=:strSelectedBillingEntity];
        if(objDTS.XML_Output_Total_Amount__c != null && objDTS.XML_Output_Total_Amount__c > 0) {
            strBatchStatusTelco = 'XML Telco Invoice Status : Completed';
            bEnabledPollerForTelco = false;
        }
    }
}