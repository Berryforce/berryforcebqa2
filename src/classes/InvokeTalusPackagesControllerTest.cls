@isTest
public class InvokeTalusPackagesControllerTest{
    static testmethod void classmet(){
        /*
        TalusOauth__c oauth=new TalusOauth__c();
        oauth.Name='Talus Oauth';
        oauth.verifier__c='59105594';
        oauth.tokenSecret__c='5sSadRRp6npSSLCs';
        oauth.Nigel_Url__c='https://nigel.stg.taluslabs.com/subscription/api/v2/accounts/';
        oauth.Package_Catalog_URL__c='https://nigel.stg.taluslabs.com/catalog/api/v2/packages/';
        oauth.Token__c='7fd69b390e9b43cc9e31b4fc9c17c678';
        oauth.ConsumerSecret__c='YpKzP7fS8PRCFeQy';
        oauth.ConsumerKey__c='62af72eae15444bfb1ee9678aadb4420';
        insert oauth;
        */
        Package_ID__c pk = new Package_ID__c(Name='PKG_IYP0001', Resource_URI__c='test');
        insert pk;
        
        Package_Product__c prdts = new Package_Product__c(Name='IYP0001_METAL', Package_ID__c=pk.id);
        insert prdts;
        
        test.startTest();
        
        MultiStaticResourceCalloutMock mock = new MultiStaticResourceCalloutMock();
        mock.setStaticResource(Label.Talus_Package_Catalog_URL+'q?param=1 & param=2', 'Talus_UDAC_Packages');
        mock.setStaticResource(Label.Talus_Package_Catalog_URL+'PKG_MCM0001/'+ pk.Resource_URI__c, 'Talus_UDAC_Products');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        //Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);  
        
        ApexPages.StandardController sc = new ApexPages.StandardController(pk);
        InvokeTalusPackagesController ITP = new InvokeTalusPackagesController(sc);
        ITP.invkPkgs();        
    
        test.stopTest();
    }
}