@IsTest(SeeAllData=true)
public class SalesInvoiceHandlerControllerTest {

    public static testMethod void SIHandlerControllerCoverage()
    {
    
        Canvass__c c=TestMethodsUtility.createCanvass();
       // Account newaccount = TestMethodsUtility.createaccount(c);
        Account newaccount = TestMethodsUtility.generateAccount();
        insert newaccount ; 
        Contact newContact = TestMethodsUtility.generateContact(newAccount.id);
        insert newContact ;
        Opportunity opp =TestMethodsUtility.createOpportunity(newaccount,newContact );
        insert opp;
         Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        /*Directory__c dir=new Directory__c(Name = 'testDirectory, KY', Directory_Code__c = '111111',Canvass__c=c.id,Telco_Provider__c=newTelco.id);
        insert dir;*/
        Directory__c dir=TestMethodsUtility.createDirectory();
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = dir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c=System.today().addMOnths(1);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE2';
        objDirE1.Directory__c = dir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS';
        objDirE1.Pub_Date__c=System.today().addMOnths(2);
        insert objDirE1;                  
        Directory_Section__c objDirSec = new Directory_Section__c();
        objDirSec.Directory__c = dir.id;
        objDirSec.Name = 'Test Dir Section';
        objDirSec.Section_Page_Type__c = 'WP';
        objDirSec.Section_Code__c = '111111';
        insert objDirSec; 
           
        
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
                Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Exclude_Dimension_1_3__c = true;
         
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print Graphic';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';  
        objProd1.Exclude_Dimension_1_3__c = true;
        
        insert objProd1;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
         list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
       // Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Successful_Payments__c=5,Directory_Section__c=objDirSec.id ,Billing_Partner__c='THE BERRY COMPAN',Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',Directory_Edition__c = objDirE1.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Billing_Frequency__c='Monthly',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMOnths(2));
         objOrderLineItem.Billing_Partner_Account__c=newaccount.id;
         objOrderLineItem.Quantity__c = 1;
         objOrderLineItem.Cancelation_Fee__c=50;
     
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c=newAccount.Id,Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Digital',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='Telco Billing',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
         objOrderLineItem1.Billing_Partner_Account__c=newaccount.id;
         objOrderLineItem1.Quantity__c = 1;
         objOrderLineItem1.Cancelation_Fee__c=50;
         
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c=newAccount.Id,Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Digital',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='Credit Card',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
         objOrderLineItem2.Billing_Partner_Account__c=newaccount.id;
         objOrderLineItem2.Quantity__c = 1;
         objOrderLineItem2.Cancelation_Fee__c=50;
         
         
     
        lstOrderLI.add(objOrderLineItem);
        lstOrderLI.add(objOrderLineItem1);
        lstOrderLI.add(objOrderLineItem2);

        insert lstOrderLI;
        c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(dir);
        c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd1);
        c2g__codaDimension3__c  dimension3=TestMethodsUtility.createDimension3(objOrderLineItem);
        
         list<c2g__codaInvoice__c> lstCodaInvoice = SalesInvoiceHandlerController.createSalesInvoiceByOLI(lstOrderLI,false);
      //   list<c2g__codaInvoice__c> lstCodaInvoice1 =SalesInvoiceHandlerController.createSalesInvoiceForCancel(lstOrderLI);
         list<c2g__codaInvoice__c> lstCodaInvoice2 =  SalesInvoiceHandlerController.createSalesInvoiceByOLIWithBundle(lstOrderLI);
        
    }
    
    
     public static testMethod void SIHandlerControllerCoverage2()
    {
        Canvass__c c=TestMethodsUtility.createCanvass();
       // Account newaccount = TestMethodsUtility.createaccount(c);
        Account newaccount = TestMethodsUtility.generateAccount();
        insert newaccount ; 
        
        Contact newContact = TestMethodsUtility.generateContact(newAccount.id);
        insert newContact ;
        Opportunity opp =TestMethodsUtility.createOpportunity(newaccount,newContact );
        insert opp;
         Telco__c newTelco = TestMethodsUtility.createTelco(newaccount.Id);
       /* Directory__c dir=new Directory__c(Name = 'testDirectory, KY', Directory_Code__c = '111111',Canvass__c=c.id,Telco_Provider__c=newTelco.id);
        insert dir;*/
         Directory__c dir=TestMethodsUtility.createDirectory();
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = dir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c=System.today().addMOnths(3);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE2';
        objDirE1.Directory__c = dir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS';
        objDirE1.Pub_Date__c=System.today().addMOnths(4);
        insert objDirE1;                  
        Directory_Section__c objDirSec = new Directory_Section__c();
        objDirSec.Directory__c = dir.id;
        objDirSec.Name = 'Test Dir Section';
        objDirSec.Section_Page_Type__c = 'WP';
        objDirSec.Section_Code__c = '111111';
        insert objDirSec; 
           
     
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
                Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Exclude_Dimension_1_3__c = true;
         
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print Graphic';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';  
        objProd1.Exclude_Dimension_1_3__c = true;
        
        insert objProd1;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
         list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
       // Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
      
     
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c=newAccount.Id,Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Digital',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='Credit Card',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
         objOrderLineItem1.Billing_Partner_Account__c=newaccount.id;
         objOrderLineItem1.Quantity__c = 1;
         objOrderLineItem1.Cancelation_Fee__c=50;
         
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c=newAccount.Id,Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Digital',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='Credit Card',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
         objOrderLineItem2.Billing_Partner_Account__c=newaccount.id;
         objOrderLineItem2.Quantity__c = 1;
         objOrderLineItem2.Cancelation_Fee__c=50;
         
         
     
        
        lstOrderLI.add(objOrderLineItem1);
        lstOrderLI.add(objOrderLineItem2);

        insert lstOrderLI;
        c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(dir);
        c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd1);
        c2g__codaDimension3__c  dimension3=TestMethodsUtility.createDimension3(objOrderLineItem1);
        
         list<c2g__codaInvoice__c> lstCodaInvoice = SalesInvoiceHandlerController.createSalesInvoiceByOLI(lstOrderLI,false);
        // list<c2g__codaInvoice__c> lstCodaInvoice1 =SalesInvoiceHandlerController.createSalesInvoiceForCancel(lstOrderLI);
         list<c2g__codaInvoice__c> lstCodaInvoice2 =  SalesInvoiceHandlerController.createSalesInvoiceByOLIWithBundle(lstOrderLI);
        
    }
    
    
     public static testMethod void SIHandlerControllerCoverage3()
    {
        Canvass__c c=TestMethodsUtility.createCanvass();
       // Account newaccount = TestMethodsUtility.createaccount(c);
        Account newaccount = TestMethodsUtility.generateAccount();
        insert newaccount ; 
        
        Contact newContact = TestMethodsUtility.generateContact(newAccount.id);
        insert newContact ;
        Opportunity opp =TestMethodsUtility.createOpportunity(newaccount,newContact );
        insert opp;
          Telco__c newTelco = TestMethodsUtility.createTelco(newaccount.Id);
        /*Directory__c dir=new Directory__c(Name = 'testDirectory, KY', Directory_Code__c = '111111',Canvass__c=c.id,Telco_Provider__c=newTelco.id);
        insert dir;*/
        Directory__c dir =TestMethodsUtility.createDirectory();
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = dir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c=System.today().addMOnths(5);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE2';
        objDirE1.Directory__c = dir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS';
        objDirE1.Pub_Date__c=System.today().addMOnths(6);
        insert objDirE1;                  
        Directory_Section__c objDirSec = new Directory_Section__c();
        objDirSec.Directory__c = dir.id;
        objDirSec.Name = 'Test Dir Section';
        objDirSec.Section_Page_Type__c = 'WP';
        objDirSec.Section_Code__c = '111111';
        insert objDirSec; 
           
       
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
                Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Exclude_Dimension_1_3__c = true;
         
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print Graphic';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';  
        objProd1.Exclude_Dimension_1_3__c = true;
        
        insert objProd1;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
         list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
       // Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
     
         
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c=newAccount.Id,Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Digital',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='ACH',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
         objOrderLineItem2.Billing_Partner_Account__c=newaccount.id;
         objOrderLineItem2.Quantity__c = 1;
         objOrderLineItem2.Cancelation_Fee__c=50;
         
         lstOrderLI.add(objOrderLineItem2);

        insert lstOrderLI;
        c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(dir);
        c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd1);
        c2g__codaDimension3__c  dimension3=TestMethodsUtility.createDimension3(objOrderLineItem2);
        
         list<c2g__codaInvoice__c> lstCodaInvoice = SalesInvoiceHandlerController.createSalesInvoiceByOLI(lstOrderLI,false);
        // list<c2g__codaInvoice__c> lstCodaInvoice1 =SalesInvoiceHandlerController.createSalesInvoiceForCancel(lstOrderLI);
         list<c2g__codaInvoice__c> lstCodaInvoice2 =  SalesInvoiceHandlerController.createSalesInvoiceByOLIWithBundle(lstOrderLI);
        
    }
    
    
    
    
    /*
    public static testMethod void SalesInvoiceHandlerControllerCoverage() {
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Product_Type__c = 'SEM';
            newProduct.Inventory_Tracking_Group__c = 'Spine';
            newProduct.RGU__c = 'SEM';
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        TestMethodsUtility.createDimension2(lstProduct[0]);
        
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        list<Canvass__c> lstCanvass = [Select Id, Name, Canvass_Code__c from Canvass__c where Id =:newAccount.Primary_Canvass__c];
        system.assertNotEquals(lstCanvass, null);
        TestMethodsUtility.createDimension1(lstCanvass[0].Name, lstCanvass[0].Canvass_Code__c);
        
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;
        insert newOpportunity;
        
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
            OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
            objOLI.PricebookEntryId = iterator.Id;
            objOLI.Billing_Duration__c = 12;
            objOLI.Full_Rate__c = 30.00;
            objOLI.UnitPrice = 30.00;
            objOLI.Package_ID__c = TestMethodsUtility.generateRandomString(5);
            objOLI.Billing_Partner__c = objOLI.Id;
            objOLI.OpportunityId = newOpportunity.Id;
            objOLI.Directory_Heading__c = objDH.Id;
            lstOLI.add(objOLI);
            mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        for(OpportunityLineItem iterator : lstOLI) {
            Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
            
            objOLI.Billing_Partner_Account__c=newAccount.id;
            objOLI.Package_ID__c = iterator.Package_ID__c;
            objOLI.Package_Item_Quantity__c = 1;
            objOLI.UnitPrice__c = iterator.UnitPrice;
            objOLI.Opportunity_line_Item_id__c = iterator.Id;
            objOLI.Media_Type__c = 'Digital';
            objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
            objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
            objOLI.Product_Inventory_Tracking_Group__c = 'Spine';
            objOLI.Payment_Method__c = newOpportunity.Payment_Method__c;
            objOLI.Quantity__c = 1;
            lstOrderLI.add(objOLI);
        }
        
        // Added by Jitender
      //  list<c2g__codaInvoice__c> lstCodaInvoice = SalesInvoiceHandlerController.createSalesInvoiceByOLI(lstOrderLI,false);
          
        
        Test.startTest();
        insert lstOrderLI;    
        
        c2g__codaDimension3__c  dimension3= TestMethodsUtility.generateDimension3(lstOrderLI[0]);
        insert dimension3;
        
        list<c2g__codaInvoice__c> lstCodaInvoice = SalesInvoiceHandlerController.createSalesInvoiceByOLI(lstOrderLI,false);
        list<c2g__codaInvoice__c> lstCodaInvoice11 = SalesInvoiceHandlerController.createSalesInvoiceForCancel(lstOrderLI);
           
        
    //  SalesInvoiceHandlerController.insertSalesInvoiceLineItemsForCancel();
                                    
        
        for(Order_Line_Items__c iterator : lstOrderLI) {
            iterator.Talus_Go_Live_Date__c = system.today();
        }
        update lstOrderLI;
        Test.stopTest();
    } */
}