global class OrderLineItemBatchCancelDM implements Database.Batchable<sObject>{
    
    //global string filtercondition;
    
    global OrderLineItemBatchCancelDM(){
        
    }
    
     global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'select isCanceled__c, Media_Type__c, id, createddate,Core_Opportunity_Line_ID__c,  Action_Code__c,Cutomer_Cancel_Date__c, Effective_Date__c from Order_Line_Items__c where Action_Code__c = \'Cancel\' and ((effective_date__c != null and Cutomer_Cancel_Date__c = null) or (media_type__c = \'Print\' and isCanceled__c = false)) and Core_Opportunity_Line_ID__c != null order by createddate desc ';
        return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext bc, List<Order_Line_Items__c> lstOLI) {
        List<Order_Line_Items__c> lstOLIUpdt = new List<Order_Line_Items__c>();
        for(Order_Line_Items__c objOLI : lstOLI) {
            if(objOLI.Core_Opportunity_Line_ID__c != null && objOLI.Action_Code__c == 'Cancel') {
                    objOLI.Cutomer_Cancel_Date__c = objOLI.Effective_Date__c;
                    if(objOLI.Effective_Date__c != null) {
                        if(objOLI.Effective_Date__c <= System.today()) {
                            objOLI.isCanceled__c = true;
                        }   
                    }
                    else {
                        if(objOLI.Media_Type__c.equals(CommonMessages.oliPrintProductType)) {
                            objOLI.isCanceled__c = true;
                        }
                    }
                }
            lstOLIUpdt.add(objOLI);
        }
        update lstOLIUpdt;
     } 
      
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Scoped Listing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception records for any other errors that might have occured while processing.');
    }
}