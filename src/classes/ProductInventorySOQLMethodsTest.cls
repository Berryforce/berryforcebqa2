@isTest(SeeAllData=True)
public class ProductInventorySOQLMethodsTest{
 static testmethod void ProductInventorySOQLMethodsTest() {
        Set<Id> setProductId = new Set<Id>();
        Set<Id> setOrLIId = new Set<Id>();
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.createproduct();
        setProductId.add(newProduct.Id);
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        setOrLIId.add(newOrLI.Id);
        Directory__c newDirectory = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c newDPM = TestMethodsUtility.createDirectoryProductMapping(newDirectory);
        Product_Inventory__c newPI = TestMethodsUtility.createProductInventory(newDPM, newProduct, newOrLI);
    }
    }