@isTest(seeAllData=true)
private class DeleteSubscriptionfromMolnControllerTest{

    static testMethod void manualMolnFulfld(){

        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'SpyderLink';
        insert prdt;
                
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.recordTypeId=Label.SpyderLink_RT_Id; dff.DFF_Product__c=prdt.Id; dff.Fulfillment_Submit_Status__c='Complete';
        insert dff;
                
        Modification_Order_Line_Item__c moln = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln.Digital_Product_Requirement__c=dff.Id; moln.Parent_ID__c='252525';
        insert mOln;        

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.recordTypeId=Label.SpyderLink_RT_Id; dff1.DFF_Product__c=prdt.Id; 
        insert dff1;
                
        Modification_Order_Line_Item__c mOln1 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln1.Digital_Product_Requirement__c=dff1.Id; moln1.Parent_ID_of_Addon__c='252525';
        insert moln1;
        
        Test.startTest();
               
        callPgRef(moln);
        
        Test.stopTest();
         
    }

    static testMethod void manualMoln(){

        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'SpyderLink';
        insert prdt;
                
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.recordTypeId=Label.SpyderLink_RT_Id; dff.DFF_Product__c=prdt.Id;
        insert dff;
                
        Modification_Order_Line_Item__c moln = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln.Digital_Product_Requirement__c=dff.Id; moln.Parent_ID__c='252525';
        insert mOln;        

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.recordTypeId=Label.SpyderLink_RT_Id; dff1.DFF_Product__c=prdt.Id; 
        insert dff1;
                
        Modification_Order_Line_Item__c mOln1 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln1.Digital_Product_Requirement__c=dff1.Id; moln1.Parent_ID_of_Addon__c='252525';
        insert moln1;
        
        Test.startTest();
        
        callPgRef(moln);
        
        Test.stopTest();
         
    }

    static testMethod void yodleMolnFulfld(){

        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'Yodle SEM';
        insert prdt;
                
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.recordTypeId=Label.Yodle_SEM_RT_Id; dff.DFF_Product__c=prdt.Id; dff.Fulfillment_Submit_Status__c='Complete';
        insert dff;
                
        Modification_Order_Line_Item__c moln = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln.Digital_Product_Requirement__c=dff.Id; moln.Parent_ID__c='252525'; moln.Status__c='New';
        insert mOln;        

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.recordTypeId=Label.Yodle_SEM_RT_Id; dff1.DFF_Product__c=prdt.Id;  
        insert dff1;
                
        Modification_Order_Line_Item__c mOln1 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln1.Digital_Product_Requirement__c=dff1.Id; moln1.Parent_ID_of_Addon__c='252525';
        insert moln1;
        
        Test.startTest();
        
        callPgRef(moln);
        
        Test.stopTest();
         
    }

    static testMethod void yodleMoln(){
    
        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'Yodle SEM';
        insert prdt;
                
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.recordTypeId=Label.Yodle_SEM_RT_Id; dff.DFF_Product__c=prdt.Id;
        insert dff;
                
        Modification_Order_Line_Item__c moln = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln.Digital_Product_Requirement__c=dff.Id; moln.Parent_ID__c='252525';
        insert mOln;        

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.recordTypeId=Label.Yodle_SEM_RT_Id; dff1.DFF_Product__c=prdt.Id; 
        insert dff1;
                
        Modification_Order_Line_Item__c mOln1 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln1.Digital_Product_Requirement__c=dff1.Id; moln1.Parent_ID_of_Addon__c='252525';
        insert moln1;
        
        Test.startTest();
        
        callPgRef(moln);
        
        Test.stopTest(); 
    }

    static testMethod void sptzrMolnFulfld(){
    
        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'Website';
        insert prdt;
                
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.recordTypeId=Label.DFF_Spotzer_RT_Id; dff.DFF_Product__c=prdt.Id; dff.Bundle__c='Spotzer BASE'; dff.Fulfillment_Submit_Status__c='Complete';
        insert dff;
                
        Modification_Order_Line_Item__c moln = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln.Digital_Product_Requirement__c=dff.Id; moln.Parent_ID__c='252525';
        insert mOln;        

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.recordTypeId=Label.DFF_Spotzer_RT_Id; dff1.DFF_Product__c=prdt.Id; dff1.Bundle__c='Spotzer Add-on';
        insert dff1;
                
        Modification_Order_Line_Item__c mOln1 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln1.Digital_Product_Requirement__c=dff1.Id; moln1.Parent_ID_of_Addon__c='252525';
        insert moln1;
        
        Test.startTest();
        
        callPgRef(moln);
        
        Test.stopTest(); 
    }

    static testMethod void sptzrMoln(){
    
        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'Website';
        insert prdt;
                
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.recordTypeId=Label.DFF_Spotzer_RT_Id; dff.DFF_Product__c=prdt.Id; dff.Bundle__c='Spotzer BASE';
        insert dff;
                
        Modification_Order_Line_Item__c moln = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln.Digital_Product_Requirement__c=dff.Id; moln.Parent_ID__c='252525';
        insert mOln;        

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.recordTypeId=Label.DFF_Spotzer_RT_Id; dff1.DFF_Product__c=prdt.Id; dff1.Bundle__c='Spotzer Add-on';
        insert dff1;
                
        Modification_Order_Line_Item__c mOln1 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln1.Digital_Product_Requirement__c=dff1.Id; moln1.Parent_ID_of_Addon__c='252525';
        insert moln1;
        
        Test.startTest();
        
        callPgRef(moln);
        
        Test.stopTest(); 
    }

    static testMethod void iYPMolnFulfld(){
    
        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'iYP_Bronze';
        insert prdt;
                
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.recordTypeId=Label.DFF_iYP_RecordType_Id; dff.DFF_Product__c=prdt.Id; dff.Fulfillment_Submit_Status__c='Complete';
        insert dff;
                
        Modification_Order_Line_Item__c moln = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln.Digital_Product_Requirement__c=dff.Id; moln.Parent_ID__c='252525';
        insert mOln;        

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.recordTypeId=Label.DFF_iYP_RecordType_Id; dff1.DFF_Product__c=prdt.Id;
        insert dff1;
                
        Modification_Order_Line_Item__c mOln1 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln1.Digital_Product_Requirement__c=dff1.Id; moln1.Parent_ID_of_Addon__c='252525';
        insert moln1;
        
        Test.startTest();
        
        callPgRef(moln);
        
        Test.stopTest(); 
    }

    static testMethod void iYPMoln(){
    
        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'iYP_Bronze';
        insert prdt;
                
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.recordTypeId=Label.DFF_iYP_RecordType_Id; dff.DFF_Product__c=prdt.Id;
        insert dff;
                
        Modification_Order_Line_Item__c moln = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln.Digital_Product_Requirement__c=dff.Id; moln.Parent_ID__c='252525';
        insert mOln;        

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.recordTypeId=Label.DFF_iYP_RecordType_Id; dff1.DFF_Product__c=prdt.Id;
        insert dff1;
                
        Modification_Order_Line_Item__c mOln1 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        moln1.Digital_Product_Requirement__c=dff1.Id; moln1.Parent_ID_of_Addon__c='252525';
        insert moln1;
        
        Test.startTest();
        
        callPgRef(moln);
        
        Test.stopTest(); 
    }

    static testMethod void talusMolnFulfld(){
    
        Product2 prdt1 = CommonUtility.createPrdtTest();
        prdt1.Product_Type__c = 'Facebook';
        insert prdt1;

        Product2 prdt2 = CommonUtility.createPrdtTest();
        prdt2.Product_Type__c = 'SEO';
        insert prdt2;
                        
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og1 = CommonUtility.createOGTest(a, ord, oppty);
        insert og1;

        Order_Group__c og2 = CommonUtility.createOGTest(a, ord, oppty);
        insert og2;

        Order_Group__c og3 = CommonUtility.createOGTest(a, ord, oppty);
        insert og3;
        
        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.recordTypeId=Label.DFF_Facebook_RT_Id; dff1.DFF_Product__c=prdt1.Id; dff1.Fulfillment_Submit_Status__c='Complete'; dff1.Bundle__c='Spotzer Add-on';
        insert dff1;
                
        Modification_Order_Line_Item__c mOln1= CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og1);
        mOln1.Digital_Product_Requirement__c=dff1.Id; mOln1.Parent_ID_of_Addon__c='252525'; mOln1.Talus_Subscription_ID__c='2525';
        insert mOln1;        

        Digital_Product_Requirement__c dff2 = CommonUtility.createDffTest();
        dff2.recordTypeId=Label.DFF_Boostability_RT_Id; dff2.DFF_Product__c=prdt2.Id; dff2.Fulfillment_Submit_Status__c='Complete';
        insert dff2;
                
        Modification_Order_Line_Item__c mOln2 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og2);
        mOln2.Digital_Product_Requirement__c=dff2.Id; mOln2.Parent_ID__c='252525'; mOln2.Talus_Subscription_ID__c='2525';
        insert mOln2;

        Digital_Product_Requirement__c dff3 = CommonUtility.createDffTest();
        dff3.recordTypeId=Label.DFF_Boostability_RT_Id; dff3.DFF_Product__c=prdt2.Id;
        insert dff3;
                
        Modification_Order_Line_Item__c mOln3 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og2);
        mOln3.Digital_Product_Requirement__c=dff3.Id; moln1.Parent_ID__c='252525'; mOln3.Talus_Subscription_ID__c='2525';
        insert mOln3;
                
        Test.startTest();
        
        callPgRef(mOln1);
        callPgRef(mOln2);
        callPgRef(mOln3);
        
        Test.stopTest(); 
    }
    
    public static void callPgRef(Modification_Order_Line_Item__c mOln){

        ApexPages.StandardController controller = new ApexPages.StandardController(mOln);
        DeleteSubscriptionfromMolnController delFromMoln = new DeleteSubscriptionfromMolnController(controller);
        delFromMoln.molnDffCancel();     

    }

}