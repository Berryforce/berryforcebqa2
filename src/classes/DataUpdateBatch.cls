global class DataUpdateBatch implements Database.Batchable<sObject>{
     
    global String strLabel;
    
    global DataUpdateBatch(){
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        String SOQL = 'Select id, DP_Incorrect_Setup_Excluded_XML_Reason__c, DP_Incorrect_Setup_Excluded_from_XML_F__c FROM Directory_Pagination__c where Order_Line_Item__c != null and (Banner__c = true or Billboard__c = true) and  Incorrect_Setup_Excluded_from_XML__c = true and DP_Incorrect_Setup_Excluded_from_XML_F__c = false';
       
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Directory_Pagination__c> scope){
        List<Directory_Pagination__c> lstUpdt = new List<Directory_Pagination__c>();
        for(Directory_Pagination__c objDP : scope) {
            
            Directory_Pagination__c objUpdtDP = new Directory_Pagination__c(id=objDP.id);
            if(objDP.DP_Incorrect_Setup_Excluded_XML_Reason__c == 'At least one of these values must be set to TRUE: Listing, Anchor, Extra Line Type Product, Display Ad, Space Ad, Coupon Ad, Leader Ad, Caption Header, Caption Member or TCAP Trademark Line;') {
                //bolError = true;
                objUpdtDP.Incorrect_Setup_Excluded_from_XML__c = objDP.DP_Incorrect_Setup_Excluded_from_XML_F__c;
                objUpdtDP.DP_Incorrect_Setup_Excluded_XML_Reason__c = null;
                lstUpdt.add(objUpdtDP);
            }
        }
            
        if(lstUpdt != null && lstUpdt.size() > 0) {
            update lstUpdt;
        }
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Batch Process Status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures.');
    }
}