/*******************************************************
Apex class to send Yodle and Manual DFFs for Fulfillment
Created By: Reddy(redd.yellanki@theberrycompany.com)
Updated Date: 09/21/2015
$Id$  
********************************************************/
public Class SubmitToYodleaAndGeneral {

    public static void updtDffforYodle(List < Digital_Product_Requirement__c > lstDffs) {

        List < Digital_Product_Requirement__c > ydlDffs = new List < Digital_Product_Requirement__c > ();
        List < Messaging.SingleEmailMessage > lstEmails = new List < Messaging.SingleEmailMessage > ();
        String mailBody;

        for (Digital_Product_Requirement__c Ydff: lstDffs) {
            if (Ydff.Status__c == 'Complete') {

                Ydff.Submit_To_Yodle__c = true;
                Ydff.Fulfillment_Submit_Status__c = 'Complete';
                Ydff.Submitted__c = true;
                Ydff.Fulfillment_Start_Date__c = datetime.now();
                Ydff.Final_Status__c = 'New';
                Ydff.OwnerId = Label.Yodle_Queue_Id;

                ydlDffs.add(Ydff);

                //Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List < String > yodleEmails = Label.Yodle_Email_Ids.split(',');
                List < String > yodleEmailsExternal = Label.Yodle_Email_Ids_External.split(',');
                //mail.setToAddresses(yodleEmails);
                //mail.setSubject('Yodle Fulfillment Information for' + Ydff.Name);
                if (Ydff.recordType.DeveloperName == 'Yodle_SEM') {
                    mailBody = semBody(Ydff);
                } else {
                    mailBody = wbsiteBody(Ydff);
                }
                //mail.setHtmlBody(mailBody);
                for (String trgtObjId: yodleEmails) {
                    lstEmails.add(CommonUtility.emailUsingTargetObjectId(trgtObjId, 'Yodle Fulfillment Information for' + Ydff.Name, mailBody));
                }
                lstEmails.add(CommonUtility.emailUsingEmailId(yodleEmailsExternal, 'Yodle Fulfillment Information for' + Ydff.Name, mailBody));
            }
        }

        database.update(ydlDffs, false);
        //System.debug('************Emails************' + ydlDffs);
        if (lstEmails.size() > 0) {
            Messaging.sendEmail(lstEmails);
        }

    }

    public static void updtDffforGeneral(List < Digital_Product_Requirement__c > lstDffs) {

        List < Digital_Product_Requirement__c > gnrlDffs = new List < Digital_Product_Requirement__c > ();

        //Queue Ids from custom settings
        Map < String, DFF_Manual_Queue_Ids__c > dffMnlQId = DFF_Manual_Queue_Ids__c.getAll();
        List < DFF_Manual_Queue_Ids__c > lstVals = dffMnlQId.Values();
        Map < String, String > mpVals = new Map < String, String > ();
        for (DFF_Manual_Queue_Ids__c iterator: lstVals) {
            mpVals.put(iterator.Name, iterator.queue_Id__c);
        }

        for (Digital_Product_Requirement__c Gdff: lstDffs) {
            if (Gdff.Status__c == 'Complete') {

                Gdff.Submitted__c = true;
                Gdff.Fulfillment_Submit_Status__c = 'Complete';
                Gdff.Final_Status__c = 'New';
                Gdff.Fulfillment_Start_Date__c = datetime.now();

                //Assigning owner to the queue
                if (mpVals.containsKey(Gdff.RecordType.DeveloperName)) {
                    Gdff.OwnerId = mpVals.get(Gdff.RecordType.DeveloperName);
                }

                gnrlDffs.add(Gdff);
            }
        }

        database.update(gnrlDffs, false);

    }

    //Method to form Yodle SEM Fulfillment (New/Update) Body
    public static String semBody(Digital_Product_Requirement__c dff) {

        String htmlBody = '<html><body>Dear Yodle Rep <br><br> Below is the Fulfillment Information for Yodle SEM Product <br><br>' +
            'Enterprise Customer Id: ' + dff.Enterprise_Customer_ID__c +
            '<br> Berry UDAC: ' + dff.UDAC__c +
            '<br> Advertised Business Name: ' + dff.business_name__c +
            '<br> Advertised Business URL: ' + dff.business_url__c +
            '<br> Advertised Business Email: ' + dff.business_email__c +
            '<br> Main Listed Number: ' + dff.business_phone_number_office__c +
            '<br> Address: ' + dff.business_address1__c + ', ' + dff.business_city__c + ', ' + dff.business_state__c + ', ' + dff.business_postal_code__c +
            '<br> Contact: ' + dff.contact_first_name__c + ' ' + dff.contact_last_name__c +
            '<br> Contact Phone Number Office: ' + dff.contact_Phone_number_office__c +
            '<br> Contact Email: ' + dff.contact_email__c +
            '<br> About Company: ' + dff.business_about__c +
            '<br> Destination Phone SEM: ' + dff.Destination_Phone_SEM__c +
            '<br> Destination Phone Website: ' + dff.Destination_Phone_Website__c +
            '<br> Geographic Service Area: ' + dff.Geographic_Service_Area__c +
            '<br> List 3 Selling Prepositions: ' + dff.selling_propositions__c +
            '<br> List 3 Services: ' + dff.services__c +
            '<br> Services to Use in Online Ad 1: ' + dff.List_3_Services_to_Use_in_Online_Ads_1__c +
            '<br> Services to Use in Online Ad 2: ' + dff.Services_to_Use_in_Online_Ads_2_c__c +
            '<br> Services to Use in Online Ad 3: ' + dff.Services_to_Use_in_Online_Ads_3_c__c +
            '<br> Desired Service Term: ' + dff.desired_service_term__c +
            '<br> Product Type: ' + dff.product_type__c +
            '<br> Hours of Operation: ' + '[Monday: ' + dff.MF__c + '--' + dff.MT__c + ', Tuesday: ' + dff.TF__c + '--' + dff.TT__c + ', Wednesday: ' + dff.WF__c + '--' + dff.WT__c + ', Thursday: ' + dff.ThF__c + '--' + dff.ThT__c +
            ', Friday: ' + dff.FF__c + '--' + dff.FT__c + ', Saturday: ' + dff.SF__c + '--' + dff.ST__c + ', Sunday: ' + dff.SnF__c + '--' + dff.SnT__c + ']' +
            '<br> Setup Notes: ' + dff.setup_notes__c +
            '<br> Certifications: ' + dff.certifications__c +
            '<br> Discounts, Specials, etc: ' + dff.Discounts_Specials_Free_Services__c +
            '<br> Payment Options: ' + dff.Payment_Options__c +
            '<br> Color Scheme: ' + dff.Color_Scheme__c +
            '<br> Keywords: ' + dff.keywords__c +
            '<br> Template Id/Color Schema: ' + dff.Template_ID__c +
            '<br> Tracking Phone SEM: ' + dff.Tracking_Phone_SEM__c +
            '<br> Omit Address: ' + dff.Omit_Address_Description__c +
            '<br> URL Tranfer Info: ' + dff.URL_Transfer_Info_if_Not_Berry_Owned__c +
            '<br> Advertise URL/Mirror URL: ' + dff.Adversite_URL_Mirror_URL__c +
            '<br> Accepted Payments: ' + dff.accepted_payments__c +
            '<br> Video to Embed or Link To: ' + dff.Video_to_Embed_or_Link_To__c +
            '<br> Budget Contracted: ' + dff.Budget_Contracted__c +
            '<br> Effective Date: ' + dff.Effective_Date__c +
            '<br> Customer Cancel Date: ' + dff.Customer_Cancel_Date__c +
            '<br><br><br><br> Thank you, <br><br> Berry Sales Rep';

        return htmlBody;

    }

    //Method to form Yodle Website Fulfillment (New/Update) Body
    public static String wbsiteBody(Digital_Product_Requirement__c dff) {

        String htmlBody = '<html><body>Dear Yodle Rep <br><br> Below is the Fulfillment Information for Yodle Website Product <br><br>' +
            'Enterprise Customer Id: ' + dff.Enterprise_Customer_ID__c +
            '<br> Berry UDAC: ' + dff.UDAC__c +
            '<br> Advertised Business Name: ' + dff.business_name__c +
            '<br> Advertised Business URL: ' + dff.business_url__c +
            '<br> Advertised Business Email: ' + dff.business_email__c +
            '<br> Main Listed Number: ' + dff.business_phone_number_office__c +
            '<br> Address: ' + dff.business_address1__c + ', ' + dff.business_city__c + ', ' + dff.business_state__c + ', ' + dff.business_postal_code__c +
            '<br> Contact: ' + dff.contact_first_name__c + ' ' + dff.contact_last_name__c +
            '<br> Contact Phone Number Office: ' + dff.contact_Phone_number_office__c +
            '<br> Contact Email: ' + dff.contact_email__c +
            '<br> About Company: ' + dff.business_about__c +
            '<br> Accepted Payments: ' + dff.accepted_payments__c +
            '<br> Template Id/Color Schema: ' + dff.Template_ID__c +
            '<br> Desired Service Term: ' + dff.desired_service_term__c +
            '<br> Product Type: ' + dff.product_type__c +
            '<br> Hours of Operation: ' + '[Monday: ' + dff.MF__c + '--' + dff.MT__c + ', Tuesday: ' + dff.TF__c + '--' + dff.TT__c + ', Wednesday: ' + dff.WF__c + '--' + dff.WT__c + ', Thursday: ' + dff.ThF__c + '--' + dff.ThT__c +
            ', Friday: ' + dff.FF__c + '--' + dff.FT__c + ', Saturday: ' + dff.SF__c + '--' + dff.ST__c + ', Sunday: ' + dff.SnF__c + '--' + dff.SnT__c + ']' +
            '<br> Setup Notes: ' + dff.setup_notes__c +
            '<br> Discounts, Specials, etc: ' + dff.Discounts_Specials_Free_Services__c +
            '<br> Destination Phone Website: ' + dff.Destination_Phone_Website__c +
            '<br> Heading: ' + dff.Heading_1__c +
            '<br> Certifications: ' + dff.certifications__c +
            '<br> Video to Embed or Link To: ' + dff.Video_to_Embed_or_Link_To__c +
            '<br> One Thing That Sets Company Apart: ' + dff.One_Thing_That_Sets_Company_Apart__c +
            '<br> Business Service Area: ' + dff.business_service_area__c +
            '<br> Payment Options: ' + dff.Payment_Options__c +
            '<br> Suppress Address on Website: ' + dff.Suppress_Address_on_Website_c__c +
            '<br> Color Scheme: ' + dff.Color_Scheme__c +
            '<br> Omit Address: ' + dff.Omit_Address_Description__c +
            '<br> URL Tranfer Info: ' + dff.URL_Transfer_Info_if_Not_Berry_Owned__c +
            '<br> Effective Date: ' + dff.Effective_Date__c +
            '<br> Customer Cancel Date: ' + dff.Customer_Cancel_Date__c +
            '<br><br><br><br> Thank you, <br><br> Berry Sales Rep';

        return htmlBody;

    }

    //Method to form Yodle SEM/Website Fulfillment (Cancel) Body
    public static String ydlCnclBody(Digital_Product_Requirement__c dff) {

        String htmlBody = '<html><body>Dear Yodle Rep <br><br> Below is the Fulfillment Cancellation Information for Yodle Product <br><br>' +
            'Enterprise Customer Id: ' + dff.Enterprise_Customer_ID__c +
            '<br> Berry UDAC: ' + dff.UDAC__c +
            '<br> Advertised Business Name: ' + dff.business_name__c +
            '<br> Advertised Business URL: ' + dff.business_url__c +
            '<br> Advertised Business Email: ' + dff.business_email__c +
            '<br> Main Listed Number: ' + dff.business_phone_number_office__c +
            '<br> Address: ' + dff.business_address1__c + ', ' + dff.business_city__c + ', ' + dff.business_state__c + ', ' + dff.business_postal_code__c +
            '<br> Effective Date: ' + dff.Effective_Date__c +
            '<br> Customer Cancel Date: ' + dff.Customer_Cancel_Date__c +
            '<br><br><br><br> Thank you, <br><br> Berry Sales Rep';

        return htmlBody;
    }
}