@isTest
public class MediaTraxServiceForUpdateClientTest{
    public static testmethod void MediaTraxUpdateClientBatchTest(){
        //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());
        
        List<MediaTrax_API_Configuration__c> lstMedia = new list<MediaTrax_API_Configuration__c>();
        MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='GetAuthToken',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/authentication.cfc',HEADER_SOAPACTION__C='GetAuthentication',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='GetAuthenticationResponse',RETURN_METHOD__C='GetAuthenticationReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='',OPERATION__C='');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='UpdateClients',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/clients.cfc',HEADER_SOAPACTION__C='UpdateClients',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='UpdateClientsResponse',RETURN_METHOD__C='UpdateClientsReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='clientIdList,clientNameList,clientNumberList,address1List,address2List,cityList,stateCodeList,zipCodeList,countryCodeList,languageList,phoneList,faxList,isYellowPageList,priceOverrideList,callRecordingList,mailMasterList,callTransferList,voiceMailList,whisperModeList,costPerCallList,callEmployeeCodeList,maxAdSpendTypeList,maxAdSpendList',OPERATION__C='Update');
        lstMedia.add(objMedia);
        insert lstMedia;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        Account newAccount = TestMethodsUtility.generateAccount('customer');
        newAccount.P4P_Spending_Cap__c = '12345';
        newAccount.P4P_Spending_Cap_Type__c = 'Y';
        newAccount.Media_Trax_Client_ID__c = '123';
        newAccount.Is_Changed__c = true;
        
        insert newAccount;  
        
        system.assertNotEquals(newAccount.ID, null);
        
        MediaTraxServiceForUpdateClientBatch objUCCallout = new MediaTraxServiceForUpdateClientBatch();
        database.executebatch(objUCCallout, 5);
    }
}