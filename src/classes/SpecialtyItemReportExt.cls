public class SpecialtyItemReportExt {
    public Id dirEditionId;
    List<Order_Line_Items__c> orderLineItemList = new List<Order_Line_Items__c>();
    public List<SpecialtyItemReportWrapper> specialtyItemReportWrapperList {get;set;}
    public String prevEditionCode {get;set;}
    public String nextEditionCode {get;set;}
    List<Directory_Edition__c> dirEditionList = new List<Directory_Edition__c>();
    Set<Id> setDirEditionIds = new Set<Id>();
    Map<Id, List<Order_Line_Items__c>> mapDirEdIdOLIList = new Map<Id, List<Order_Line_Items__c>>();
    
    public SpecialtyItemReportExt(){
    specialtyItemReportWrapperList = new List<SpecialtyItemReportWrapper>();
        dirEditionId = ApexPages.CurrentPage().getParameters().get('DEId');
        generateWrapperList();
    }
    
    public SpecialtyItemReportExt(ApexPages.StandardController controller) {
        specialtyItemReportWrapperList = new List<SpecialtyItemReportWrapper>();
        dirEditionId = controller.getId();               
        generateWrapperList(); 
    }
    
    public void generateWrapperList(){
        orderLineItemList = OrderLineItemSOQLMethods.getOLIForSpecialtyProductByDirEditionId(dirEditionId);                
        
        if(orderLineItemList.size() > 0){
            set<Id> directoryIds = new set<Id>();
            
            for(Order_Line_Items__c OLI : orderLineItemList){
                if(OLI.Directory__c != null){
                    directoryIds.add(OLI.Directory__c);
                }
            }
            system.debug('Directory Ids are ' + directoryIds);
            dirEditionList = DirectoryEditionSOQLMethods.getDirEditionByDirIds(directoryIds);
            
            if(dirEditionList.size() > 0){
                for(Directory_Edition__c DE : dirEditionList){
                    if(DE.Book_Status__c == CommonMessages.bots){
                        nextEditionCode = DE.Edition_Code__c;
                    }
                    if(DE.Book_Status__c == CommonMessages.botsMinus1){
                        prevEditionCode = DE.Edition_Code__c;
                    }
                }
            }
            
             for(Order_Line_Items__c OLI : orderLineItemList){
                 List<Line_Item_History__c> lineItemHisList = new List<Line_Item_History__c>();
                 lineItemHisList = OLI.Line_Item_Histories__r;
                 if(lineItemHisList.size() > 0){
                     for(Line_Item_History__c LIH : lineItemHisList){
                         if(LIH.Directory_Edition__c != null){
                             setDirEditionIds.add(LIH.Directory_Edition__c);
                         }
                     }
                 }                 
             }
             
             if(setDirEditionIds.size() > 0){
                 List<Order_Line_Items__c> listTempOLI = new List<Order_Line_Items__c>();
                 
                 listTempOLI = OrderLineItemSOQLMethods.getOLIFByDirEditionIds(setDirEditionIds);
                 
                 if(listTempOLI.size() > 0){
                     for(Order_Line_Items__c OLI : listTempOLI){
                         if(!mapDirEdIdOLIList.containsKey(OLI.Directory_Edition__c)){
                             mapDirEdIdOLIList.put(OLI.Directory_Edition__c, new List<Order_Line_Items__c>());
                         }
                         mapDirEdIdOLIList.get(OLI.Directory_Edition__c).add(OLI);
                     }
                 }
             }
            
            for(Order_Line_Items__c OLI : orderLineItemList){
                Decimal prevEdition = 0.0;
                Decimal nextEdition = 0.0;
                String handled = 'Yes';
                List<Line_Item_History__c> lineItemHistoryList = new List<Line_Item_History__c>();
                //List<Modification_Order_Line_Item__c> modifyOLIList = new List<Modification_Order_Line_Item__c>();
                
                lineItemHistoryList = OLI.Line_Item_Histories__r;
                //modifyOLIList = OLI.Modification_Order_Line_Items__r;
                //system.debug('MOLI List is ' + modifyOLIList);
                                            
                if(lineItemHistoryList.size() > 0){
                    Line_Item_History__c prevLIH = new Line_Item_History__c();
                    Line_Item_History__c nextLIH = new Line_Item_History__c();
                    for(Line_Item_History__c LIH : lineItemHistoryList){
                        if(LIH.Directory_Edition__r.Year__c  == String.valueOf(Integer.valueOf(OLI.Directory_Edition__r.Year__c) - 1)){
                            prevEdition = LIH.UnitPrice__c;
                            prevLIH = LIH;
                        }
                        if(LIH.Directory_Edition__r.Year__c == OLI.Directory_Edition__r.Year__c){
                            nextEdition = LIH.UnitPrice__c;
                            nextLIH = LIH;
                        }
                    }
                    if(nextEdition != null && prevEdition != null){
                        system.debug('Inside');
                        if(prevEdition == Decimal.valueOf('0') && nextEdition > Decimal.valueOf('0')){
                            handled = 'Yes';
                        } else if(prevEdition > Decimal.valueOf('0') && nextEdition > Decimal.valueOf('0')){
                            handled = 'Yes';
                        } else if(mapDirEdIdOLIList.containsKey(prevLIH.Directory_Edition__c)){
                            List<Order_Line_Items__c> listTempOLIs = new List<Order_Line_Items__c>();
                            listTempOLIs = mapDirEdIdOLIList.get(prevLIH.Directory_Edition__c);
                            for(Order_Line_Items__c ordeLinIt : listTempOLIs){
                                if(!ordeLinIt.IsCanceled__c){
                                    handled = 'No';
                                    break;
                                }
                            }
                            
                        } else {
                            handled = 'No';
                        }
                    }
                }
                system.debug('Account Number is ' + OLI.Account__r.Account_Number__c);
                specialtyItemReportWrapperList.add(new SpecialtyItemReportWrapper(OLI.Account__r.Account_Number__c, OLI.Account__r.Name, OLI.Directory_Edition__r.Name, OLI.Account__r.Phone, OLI.Name, OLI.Id, OLI.UDAC__c, OLI.Action_Code__c, OLI.Digital_Product_Requirement__r.Order_URN__c, prevEdition, nextEdition, handled));
            }
            Decimal totalPrevEdition = 0.0;
            Decimal totalNextEdition = 0.0;
            
            for(SpecialtyItemReportWrapper wrapper : specialtyItemReportWrapperList){
                totalPrevEdition += wrapper.previousEdition;
                totalNextEdition += wrapper.nextEdition;
            }
            
            specialtyItemReportWrapperList.add(new SpecialtyItemReportWrapper('Total', null, null, null, null, null, null, null, null, totalPrevEdition, totalNextEdition, null));
        }
    }
    
    /* Wrapper Class */
    public class SpecialtyItemReportWrapper {
        public String accountNumber {get;set;}
        public String accountName {get;set;}
        public String dirEditionName {get;set;}
        public String accountPhoneNumber {get;set;}
        public String orderLineItemName {get;set;}
        public String orderLineItemId {get;set;}
        public String UDAC {get;set;}
        public String orderlineitemaction {get;set;}
        public String dfforderurn {get;set;}
        public Decimal previousEdition {get;set;}
        public Decimal nextEdition {get;set;}
        public String handled {get;set;}
                
        public SpecialtyItemReportWrapper(String accountNumber, String accountName, String dirEditionName, String accountPhoneNumber, String orderLineItemName, String orderLineItemId, String UDAC, String orderlineitemaction, String dfforderurn, Decimal previousEdition, Decimal nextEdition, String handled){
            this.accountNumber = accountNumber;
            this.accountName = accountName;
            this.dirEditionName = dirEditionName;
            this.accountPhoneNumber = accountPhoneNumber;
            this.orderLineItemName = orderLineItemName;
            this.orderLineItemId = orderLineItemId;
            this.UDAC = UDAC;
            this.orderlineitemaction = orderlineitemaction;
            this.dfforderurn = dfforderurn;
            this.previousEdition = previousEdition;
            this.nextEdition = nextEdition;       
            this.handled = handled; 
        }
    }
    
    public PageReference exportAsExcel(){
        PageReference pg = new PageReference('/apex/SpecialtyItemReportExcelPage?DEId=' + dirEditionId);
        pg.setRedirect(true);
        return pg;
    }
}