/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Testvertex_RateTagPM {

    static testMethod void TestRateTagPM() 
    {
    Product2 objPGDisp=new Product2 ();
    objPGDisp.Name='GraphincDisp';
    objPGDisp.ProductCode='pro-001';
    objPGDisp.AllowX10Discount__c=true;
    objPGDisp.AllowX15Discount__c=true;
    objPGDisp.Allow_X20_Discount__c=true;
    objPGDisp.AllowX25Discount__c=true;
    objPGDisp.AllowX30Discount__c=true;
    objPGDisp.AllowX35Discount__c=true;
    objPGDisp.AllowX40Discount__c=true;
    objPGDisp.AllowX45Discount__c=true;
    objPGDisp.AllowX50Discount__c=true;
    objPGDisp.AllowX55Discount__c=true;
    objPGDisp.AllowX60Discount__c=true;
    objPGDisp.AllowX65Discount__c=true;
    objPGDisp.AllowX70Discount__c=true;
    objPGDisp.AllowX75Discount__c=true;
    objPGDisp.AllowX80Discount__c=true; 
    objPGDisp.AllowX85Discount__c=true;
    objPGDisp.AllowX90Discount__c=true;
    objPGDisp.AllowX95Discount__c=true;
    objPGDisp.LOY__c=true;
    objPGDisp.Product_Type__c ='Print Graphic';
    objPGDisp.Print_Product_Type__c='Display';
    objPGDisp.Description='WP';
    insert objPGDisp;
    
    /*Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;*/
    Directory__c objDir= TestMethodsUtility.createDirectory();
    
    Apexpages.currentPage().getParameters().put('objDirid',objDir.id);
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=Apexpages.currentPage().getParameters().get('objDirid');
    objDPM.Product2__c=objPGDisp.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=true;
    objDPM.National_Only__c=false;
    //objDPM.Grandfathered__c=true;
    objDPM.FullRate__c=500;
    insert objDPM;
    
   /* Directory_Product_Mapping__c objDPM1=new Directory_Product_Mapping__c();
    objDPM1.Directory__c=objDir.id;
    objDPM1.Product2__c=objPGDisp.id;
    objDPM1.is_Active__c=true;
    objDPM1.Local_Only__c=true;
    objDPM1.National_Only__c=false;
    //objDPM.Grandfathered__c=true;
    objDPM1.FullRate__c=500;
    insert objDPM1;*/
    
    vertex_RateTagPM  obj=new vertex_RateTagPM(new ApexPages.StandardController(objDPM));
    obj.DP=objDPM;
     Apexpages.currentPage().getParameters().put('objDirid',objDir.id);
    obj.cancel();
    obj.save();
    
    }
    }