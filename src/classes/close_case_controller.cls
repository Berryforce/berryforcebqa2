public with sharing class close_case_controller {
    
    public Case Caseobj{get; set;}
    public String caseID{get; set;}
    
    public close_case_controller(ApexPages.StandardController controller) {
        caseID = ApexPages.currentPage().getParameters().get('id');
    }
    
    public PageReference processSCN() {
        caseID = ApexPages.currentPage().getParameters().get('id');
        Caseobj = [SELECT  Id, Approved__c
                   FROM Case where Id = :caseID limit 1]; 
    
        PageReference pageRef = Null; 
        if(Caseobj.Approved__c == true){
            pageRef = new PageReference('/'+caseID+'/s?saveURL=%2Fapex%2FprocessSCN_V1?Id='+caseID+'&retURL=/'+caseID);
            //pageRef = new PageReference('/apex/processSCN?caseID='+caseID);
            pageRef.setRedirect(true);
        }    
        return pageRef;
    }
    
    
    
    
}