@isTest(seeallData=True)
public class BillingDateChangesControllerTest{
static testmethod void myunittest(){
Test.startTest();
string Acc = [select id from Account Limit 1].Id;
   //Apexpages.currentPage().getParameters().put('acid', Acc);
   //Apexpages.currentPage().getParameters().get('ccid', );
   
    
     
    
    
     
     
     
     //OLI creation
      Canvass__c newCanvass = TestMethodsUtility.createCanvass();
        Account newAccount = TestMethodsUtility.generateAccount('customer', false);
        newAccount.Primary_Canvass__c = newCanvass.Id;
        insert newAccount;
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
     Apexpages.currentPage().getParameters().put('acid', newAccount.ID);
    
    newOrLI.Order_Anniversary_Start_Date__c = system.TODAY();
   newOrLI.Talus_Go_Live_Date__c = system.today().adddays(2);
     insert newOrLI ;
     Order_Line_Items__c oldOrLI = newOrLI;
    newOrLI.Order_Anniversary_Start_Date__c = system.TODAY().adddays(5);
    update newOrLI;
    
     BillingDateChangesController objBillDateCont = new BillingDateChangesController();
     objBillDateCont.onLoad();
     objBillDateCont.ProrateLogic(newOrLI, oldOrLI);
     objBillDateCont.updateOrderBillingDate();
     objBillDateCont.cancel() ;
     
      Boolean B = objBillDateCont.validateBillingDate();
     system.assert(b);
     
     objBillDateCont.nextBillingDateChangeLogic(newOrLI,newOrLI);
     integer pdays = 10;
   //  objBillDateCont.prorateLogic(newOrLI,newOrLI);
     
     
Test.stopTest();
}
}