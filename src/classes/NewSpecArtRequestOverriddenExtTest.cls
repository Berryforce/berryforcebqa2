@isTest(seealldata=true)
public class NewSpecArtRequestOverriddenExtTest {
    static testMethod void testSpecArtReq() {
        Test.StartTest();
        
        Account newAccount = TestMethodsUtility.createAccount('cmr');
        
        Contact cnt = TestMethodsUtility.createContact(newAccount);
        
        Spec_Art_Request__c specArtReq = new Spec_Art_Request__c(Account__c = newAccount.Id);
        insert specArtReq;
        
        ApexPages.currentPage().getParameters().put('Test', 'Test2');
        
        NewSpecArtRequestOverriddenExt obj = new NewSpecArtRequestOverriddenExt(new ApexPages.StandardController(specArtReq));
        
        ApexPages.currentPage().getParameters().put('Test', 'Test2');
        
        obj.accountId = newAccount.Id;
        obj.onLoad();
        
        Test.stopTest();        
    }
}