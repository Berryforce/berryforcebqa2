global class BSure_ScheduleManagePaymentPlan implements Schedulable {
   public Interface BSure_ScheduleManagePaymentPlanInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('BSure_ScheduleManagePaymentPlanHndlr');
        if(targetType != null) {
            BSure_ScheduleManagePaymentPlanInterface obj = (BSure_ScheduleManagePaymentPlanInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}