public with sharing class OrderSetDFFsController {

    //getter, setters
    public String dffId {
        get;
        set;
    }
    public String orderSetId {
        get;
        set;
    }
    public String mOrderSetId {
        get;
        set;
    }
    public String rdId {
        get;
        set;
    }    
    public List < Digital_Product_Requirement__c > allDFFs {
        get;
        set;
    }
    
    private Digital_Product_Requirement__c dff;

    //constructor
    public OrderSetDFFsController(ApexPages.StandardController controller) {

        if (!Test.isRunningTest()) {
            List < String > flds = new List < String > {
                'Id', 'Name', 'Test_PackageId__c', 'business_url__c', 'Bundle__c', 'DFF_RecordType_Name__c', 'Fulfillment_Submit_Status__c', 'OrderLineItemID__r.Order_Group__c', 'ModificationOrderLineItem__r.Order_Group__c'
            };
            controller.addFields(flds);
        }

        this.dff = (Digital_Product_Requirement__c) Controller.getRecord();
        //System.debug('Testing (Digital_Product_Requirement__c)Controller.getRecord() ' + (Digital_Product_Requirement__c) Controller.getRecord());

    }

    public List < Digital_Product_Requirement__c > getDFFs() {

        dffId = dff.Id;
        System.debug('Testingg dffId ' + dffId);
        if (!Test.isRunningTest()) {
            orderSetId = dff.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
            //System.debug('************OrderSetId: ' + orderSetId + '************MOrderSetId: ' + mOrderSetId);

        } else {
            Digital_Product_Requirement__c dprRcd = [SELECT Id, OrderLineItemID__r.Order_Group__c, ModificationOrderLineItem__r.Order_Group__c, Test_PackageId__c from Digital_Product_Requirement__c where id = : dffId];
            orderSetId = dprRcd.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        }
        
        if(String.isNotBlank(orderSetId)){
            allDFFs = [SELECT id, Name, OrderLineItemID__c, OrderLineItemID__r.Order_Group__c, Account__r.Name, OrderLineItemID__r.Order_Group__r.Name, UDAC__c, business_url__c, Bundle__c, DFF_RecordType_Name__c, Changed_Dff_Field_Count__c,  
                OrderLineItemID__r.Description__c, DFF_Status__c, Status__c, OrderLineItemID__r.Status__c, OrderLineItemID__r.Product2__r.Description, Fulfillment_Status__c, ModificationOrderLineItem__r.Order_Line_Item__r.Status__c,
                OrderLineItemID__r.Talus_Go_Live_Date__c, ModificationOrderLineItem__c, OrderLineItemID__r.Product2__r.Product_or_Addon__c, ModificationOrderLineItem__r.Order_Line_Item__r.Talus_Go_Live_Date__c
                from Digital_Product_Requirement__c where OrderLineItemID__r.Order_Group__c =: orderSetId and ModificationOrderLineItem__c = null
                and UDAC__c != null and recordType.DeveloperName Not In('General_Add_on') order By Name Asc
            ];
            rdId = orderSetId;
        } else if(String.isNotBlank(mOrderSetId)){
            allDFFs = [SELECT id, Name, OrderLineItemID__c, OrderLineItemID__r.Order_Group__c, Account__r.Name, OrderLineItemID__r.Order_Group__r.Name, UDAC__c, business_url__c, Bundle__c, DFF_RecordType_Name__c, Changed_Dff_Field_Count__c, 
                OrderLineItemID__r.Description__c, DFF_Status__c, Status__c, OrderLineItemID__r.Status__c, OrderLineItemID__r.Product2__r.Description, Fulfillment_Status__c, ModificationOrderLineItem__r.Status__c,
                OrderLineItemID__r.Talus_Go_Live_Date__c, ModificationOrderLineItem__c, OrderLineItemID__r.Product2__r.Product_or_Addon__c, ModificationOrderLineItem__r.Talus_Go_Live_Date__c, ModificationOrderLineItem__r.Product2__r.Description
                from Digital_Product_Requirement__c where ModificationOrderLineItem__r.Order_Group__c = : morderSetId and recordType.DeveloperName Not In('General_Add_on') order By Name Asc
            ];        
            rdId = mOrderSetId;
        } else{
            CommonUtility.msgInfo(CommonUtility.noRcdLink);
        }

        //system.debug('************ALLDFFS*****************' + allDFFs);

        return allDFFs;

    }

}