public class DirectoryGuide
   {
       transient public Directory_Section__c lstDir {get;set;}
       public string DirSecId;
       public string TxtEnterGuide {get;set;}
       public string TxtDirecGuide {get;set;}
       public string TxtHeadDisclm {get;set;} 
       public string TxtDisclaimerText {get;set;}
       public Id TxtDirecGuideID {get;set;}
       public Id TxtHeadDisclmID {get;set;}
       public list<WrapobjSecHead> WrapobjSecHeadLst  {get;set;}
       public List<Section_Heading_Mapping__c> objSecHead  {get;set;}
       public List<Section_Heading_Mapping__c> objSecHead1  {get;set;}
       map<id,Section_Heading_Mapping__c> LstSecHeadMapping {get;set;}
       set<Id> setForDirectoryGuide;
       Id oldGuideId;

    public ApexPages.StandardSetController con{get; set;}
       
       public DirectoryGuide(ApexPages.StandardController controller)
       {
           DirSecId = Apexpages.currentPage().getParameters().get('Id');
           lstDir = new Directory_Section__c();
           lstDir = [SELECT Area_Codes_to_Suppress__c,Directory_Code__c,Directory_Name__c,Directory__c,Full_Section_ID__c,Guide__c,Headername__c,Id,Name,sectid__c,Section_Code__c,Section_Graphics__c,Section_Page_Type__c,Section_Pagination_Code__c,Sellable_EAS_Section__c,Sequence_in_Section__c,size__c,Type__c, (SELECT Appearance_Count__c,Directory_Guide__c,Directory_Heading__r.Name, Directory_Heading__r.Directory_Heading_Name__c, Directory_Section__c,Guide_Telltale__c,Heading_Code__c,Id,Name,Section_Code__c,Section_Heading_ID__c,Suppress__c,Heading_Disclaimer__c,Disclaimer_Text__c,Heading_Disclaimer__r.Name,Heading_Disclaimer__r.Disclaimer_Text__c,Force_Heading_To_Appear__c  FROM Section_Heading_Mappings__r) FROM Directory_Section__c where Id =: DirSecId limit 1];
           listingSecHeadLst(DirSecId);
       }

        public List<Section_Heading_Mapping__c> lstSecHeadMap
        {
            get {
            if(con != null)
            {
                  return (List<Section_Heading_Mapping__c>)con.getRecords();
            }
            else
            return null ;
            }   
            set;
        }
       
       public void listingSecHeadLst(Id DirSecId) {
           con = new ApexPages.StandardSetController(Database.query('SELECT Appearance_Count__c,Directory_Heading__r.Name, Directory_Heading__r.Directory_Heading_Name__c, Directory_Guide__c,Directory_Guide__r.Name,Directory_Heading__c,Directory_Section__c,Guide_Telltale__c,Heading_Code__c,Id,Name,Section_Code__c,Section_Heading_ID__c,Suppress__c,Heading_Disclaimer__c,Disclaimer_Text__c,Heading_Disclaimer__r.Name, Heading_Disclaimer__r.Disclaimer_Text__c, Force_Heading_To_Appear__c FROM Section_Heading_Mapping__c WHERE Directory_Section__c =: DirSecId ORDER BY Name ASC LIMIT 2000'));
           con.setPageSize(1000);
           createData();
       }
       public void createData() {
           WrapobjSecHeadLst = new list<WrapobjSecHead>();
            LstSecHeadMapping =new map<id,Section_Heading_Mapping__c>();
            List<Section_Heading_Mapping__c> LstSectionHeadingMapping = (List<Section_Heading_Mapping__c>)con.getRecords();
               for(Section_Heading_Mapping__c objSecHeadMap : LstSectionHeadingMapping) {
                   LstSecHeadMapping.put(objSecHeadMap.id,objSecHeadMap);
                   WrapobjSecHead objWrpSH = new WrapobjSecHead();
                   objWrpSH.objSecHMap = new Section_Heading_Mapping__c();
                   objWrpSH.objSecHMap = (Section_Heading_Mapping__c)objSecHeadMap;
                   WrapobjSecHeadLst.add(objWrpSH);
               }
       }

    public void UpdateDirGuides() {
        setForDirectoryGuide=new Set<Id>();
        for(Section_Heading_Mapping__c objSecHeadMap : [SELECT Directory_Guide__c FROM Section_Heading_Mapping__c WHERE Directory_Section__c =: DirSecId AND Directory_Guide__c != null ORDER BY Name ASC LIMIT 50000]) {
            setForDirectoryGuide.add(objSecHeadMap.Directory_Guide__c);
        }
        objSecHead = new list<Section_Heading_Mapping__c>();
        objSecHead1 = new list<Section_Heading_Mapping__c>();
        string oldComingDirectoryGuide = null;
        integer counter = 0;
        integer counter1 = 0;
        integer counter2 = 0;
        integer counter3 = 0;
        integer counter4 = 0;
        string newComingDirectoryGuide = null;
        for(WrapobjSecHead WrpSecHead : WrapobjSecHeadLst) {
            if(setForDirectoryGuide.contains(TxtDirecGuideID)) { 
                if(!WrpSecHead.ischecked && counter1 == 0) {                
                    newComingDirectoryGuide = WrpSecHead.objSecHMap.Directory_Guide__c;             
                    if(counter3 > 0 && counter2 == 0) {             
                        oldComingDirectoryGuide = newComingDirectoryGuide;
                        counter2++;                                         
                    }
                    counter4++;
                }
    
                if(WrpSecHead.ischecked && counter2 == 0) {
                    counter3++;
                    
                    if(String.isNotBlank(oldGuideId) && oldGuideId == TxtDirecGuideID && counter2 == 0 && counter4 == 0) {
                        oldComingDirectoryGuide = oldGuideId;
                        counter2++;
                    }
                }
                
                if(WrpSecHead.ischecked && TxtDirecGuideID == newComingDirectoryGuide) {
                    if(counter != 1) {
                        counter1++;
                        Id SecId = WrpSecHead.objSecHMap.id;
                        Section_Heading_Mapping__c sec = new Section_Heading_Mapping__c (id= SecId, Guide_Telltale__c = TxtEnterGuide,Directory_Guide__c = TxtDirecGuideID);
                        objSecHead.add(sec);
                    }
                    else {
                        objSecHead = new list<Section_Heading_Mapping__c>();
                        break;
                    }
                }
                else {
                    Id SecId = WrpSecHead.objSecHMap.id;
                    if(WrpSecHead.ischecked) {
                        Section_Heading_Mapping__c sec = new Section_Heading_Mapping__c (Id = SecId, Guide_Telltale__c = TxtEnterGuide,Directory_Guide__c = TxtDirecGuideID);
                        objSecHead1.add(sec);
                    }
                    if(counter1 > 0)
                    counter = 1;
                }
    
            }
            else {
                if(!WrpSecHead.ischecked ) {
                    newComingDirectoryGuide=WrpSecHead.objSecHMap.Directory_Guide__c;
                }
    
                if(WrpSecHead.ischecked) {
                    if(counter!=1) {
                        counter1++;
                        Id SecId = WrpSecHead.objSecHMap.id;
                        Section_Heading_Mapping__c sec = new Section_Heading_Mapping__c (id= SecId,  Guide_Telltale__c=TxtEnterGuide,Directory_Guide__c =TxtDirecGuideID);
                        objSecHead.add(sec);
                    }
                    else { 
                        objSecHead = new list<Section_Heading_Mapping__c>();
                        break;
                    }
                }
                else {
                    if(counter1>0)
                    counter=1;
                }
            }
        }
    
        if(oldComingDirectoryGuide == TxtDirecGuideID) {
            objSecHead.addAll(objSecHead1);
        }
    
        if(objSecHead.size()>0) {
            update objSecHead;
            TxtEnterGuide ='';
            TxtDirecGuide='';
        }
    
        else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Cannot select Guide which are not subsequent to each other');
            ApexPages.addMessage(myMsg);
        }
        listingSecHeadLst(DirSecId);
    }
       
       public void UpdateHeadingDisclaimer() {
           objSecHead = new list<Section_Heading_Mapping__c>();
           set<Id> setHeadingDisClm = new set<Id>();
           
           if(String.isNotBlank(TxtHeadDisclmID)) {
                   list<Heading_Disclaimer__c> lstHeadDiscClm = [Select Id, Disclaimer_Text__c from Heading_Disclaimer__c where Id =:TxtHeadDisclmID LIMIT 50000];
                       
                   
                   for(WrapobjSecHead WrpSecHead_Disc : WrapobjSecHeadLst) {
                       if(WrpSecHead_Disc.ischecked) {
                           Id SecId = WrpSecHead_Disc.objSecHMap.id;
                           TxtDisclaimerText = lstHeadDiscClm[0].Disclaimer_Text__c;
                           Section_Heading_Mapping__c sec = new Section_Heading_Mapping__c (id= SecId, Heading_Disclaimer__c = TxtHeadDisclmID, Disclaimer_Text__c = TxtDisclaimerText);
                           objSecHead.add(sec);
                       }
                   }
                   if(objSecHead.size()>0) {
                       update objSecHead;
                       TxtHeadDisclm = '';
                   }
           }
           listingSecHeadLst(DirSecId);
       }
       
       public string DeleteData() {
           Set<id> SecHeadIds = new Set<id>();
           list<Section_Heading_Mapping__c> SecHeadDel = new list<Section_Heading_Mapping__c>();
           for(WrapobjSecHead objBkWrap : WrapobjSecHeadLst) {
               if(objBkwrap.ischecked == true) {
                Id SecId = objBkWrap.objSecHMap.id;
                Section_Heading_Mapping__c sec = new Section_Heading_Mapping__c (id= SecId, Directory_Guide__c=null, Guide_Telltale__c='');
                SecHeadDel.add(sec);
               }
           }
           update SecHeadDel;
           TxtEnterGuide ='';
           TxtDirecGuide='';
           
           DirSecId = Apexpages.currentPage().getParameters().get('Id');
           lstDir = new Directory_Section__c();
           lstDir = [SELECT Area_Codes_to_Suppress__c,Directory_Code__c,Directory_Name__c,Directory__c,Full_Section_ID__c,Guide__c,Headername__c,Id,Name,sectid__c,Section_Code__c,Section_Graphics__c,Section_Page_Type__c,Section_Pagination_Code__c,Sellable_EAS_Section__c,Sequence_in_Section__c,size__c,Type__c, (SELECT Appearance_Count__c,Directory_Guide__c,Directory_Heading__r.Name, Directory_Heading__r.Directory_Heading_Name__c, Directory_Section__c,Guide_Telltale__c,Heading_Code__c,Id,Name,Section_Code__c,Section_Heading_ID__c,Suppress__c,Heading_Disclaimer__r.Name,Force_Heading_To_Appear__c FROM Section_Heading_Mappings__r) FROM Directory_Section__c where Id =: DirSecId limit 1];
           listingSecHeadLst(DirSecId);
           return null;
       }
       
       public string DeleteDisclaimerData()
       {
           Set<id> SecHeadIds = new Set<id>();
           list<Section_Heading_Mapping__c> SecHeadDel = new list<Section_Heading_Mapping__c>();
           for(WrapobjSecHead objBkWrap : WrapobjSecHeadLst)
           {
               if(objBkwrap.ischecked == true)
               {
                 Id SecId = objBkWrap.objSecHMap.id;
                 Section_Heading_Mapping__c sec = new Section_Heading_Mapping__c (id= SecId, Heading_Disclaimer__c=null);
                 SecHeadDel.add(sec);
               }
           }
           update SecHeadDel;
           TxtHeadDisclm = '';
           
           DirSecId = Apexpages.currentPage().getParameters().get('Id');
           lstDir = new Directory_Section__c();
           lstDir = [SELECT Area_Codes_to_Suppress__c,Directory_Code__c,Directory_Name__c,Directory__c,Full_Section_ID__c,Guide__c,Headername__c,Id,Name,sectid__c,Section_Code__c,Section_Graphics__c,Section_Page_Type__c,Section_Pagination_Code__c,Sellable_EAS_Section__c,Sequence_in_Section__c,size__c,Type__c, (SELECT Appearance_Count__c,Directory_Guide__c,Directory_Heading__r.Name, Directory_Heading__r.Directory_Heading_Name__c, Directory_Section__c,Guide_Telltale__c,Heading_Code__c,Id,Name,Section_Code__c,Heading_Disclaimer__c,Disclaimer_Text__c,Section_Heading_ID__c,Suppress__c,Heading_Disclaimer__r.Name,Force_Heading_To_Appear__c FROM Section_Heading_Mappings__r) FROM Directory_Section__c where Id =: DirSecId limit 1];
           listingSecHeadLst(DirSecId);
           return null;
       }
       
        public Boolean hasNext {
            get {
                return con.getHasNext();
            }
            set;
        }
     
        public Boolean hasPrevious {
            get {
                return con.getHasPrevious();
            }
            set;
        }
     
        public Integer pageNumber {
            get {
                return con.getPageNumber();
            }
            set;
        }

        public void first() {
            con.first();
            createData();
            oldGuideId = '';
        }
     

        public void last() {
            con.last();
            createData();
            oldGuideId = '';
        }
     

        public void previous() {
            con.previous();
            createData();
            oldGuideId = '';
        }
     

        public void next() {
            oldGuideId = WrapobjSecHeadLst.get(WrapobjSecHeadLst.size() - 1).objSecHMap.Directory_Guide__c;
            con.next();
            createData();
        }
     
        public pagereference cancel() {
            //con.cancel();
            return(new pagereference('/'+DirSecId).setredirect(true)) ; 
        }

       public class WrapobjSecHead
       {
           public boolean ischecked {get;set;}
           public Section_Heading_Mapping__c objSecHMap {get;set;}
       }
   }