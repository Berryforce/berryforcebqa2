public class ccexpireEmails { 
	
	/*
	This class finds credt cards that are about to expire and creates a case
	and then send a email to the card email address
	this class should be run on the first of the month.
	*/
	 
	public static Date TodayDate {get;set;}
	public static Date TempDate {get;set;}
	public List<Order_Line_Items__c> OLIList;
	
	public ccexpireEmails() {
		TodayDate = System.today();
		OLIList = [SELECT Id, Billing_Contact__c, Talus_Go_Live_Date__c FROM Order_Line_Items__c WHERE IsCanceled__c = False AND Talus_Go_Live_Date__c != Null];
	}
	
	//primary method
	public static void getmail() {
		// get open fulfilled line items
		list <Order_Line_Items__c> OLIs = [SELECT Id, Billing_Contact__c, Talus_Go_Live_Date__c FROM Order_Line_Items__c WHERE IsCanceled__c = False AND Talus_Go_Live_Date__c != Null];	
		pymt__Payment_Method__c PymtMethod = new pymt__Payment_Method__c();	
		list <Messaging.SingleEmailMessage> MessageArray = new list <Messaging.SingleEmailMessage>();
		
		if(OLIs.size() > 0) {
		
			//build a list of contacts with open fulfilled line items		
			set<Id> setContactId = new set<Id>();
			for (Order_Line_Items__c OLI:OLIs) {
				if(OLI.Billing_Contact__c != null) {
					setContactId.add(OLI.Billing_Contact__c);
				}
			}
	
			if(setContactId.size() > 0) {				
				system.debug('Contact ids are ' + setContactId);
			
				// what month is this and what year is it
				date testdate = system.today().addMonths(2);
				string month = string.valueOf(testdate.month());
				string year = string.valueOf(testdate.year());
				
				// get the payment methods that have cards expireing this month/year
				list<pymt__Payment_Method__c> PMs =  PaymentMethodSOQLMethods.getExpireSoonCreditCardDetails(setContactId, month, year);
				system.debug('Payment methods are ' + PMs);
				
				//build the cases
				//get the Billing Queue
				list<Group> lstQGP = CommonFunctions.getQueueByName(CommonMessages.queueBilling);
				system.debug('Group list is ' + lstQGP);
				
				RecordType CRT = CommonMethods.getRecordTypeDetailsByDeveloperName(CommonMessages.caseObjectName, CommonMessages.caseInternal);
				system.debug('Record type is ' + CRT);
				
				list <Case> Cases = new list <Case>();				
				
				if(!PMs.isEmpty()) {
					for(pymt__Payment_Method__c PM : PMs) {
						//create case for payment method
						Case CS = New Case(ContactId = PM.pymt__Contact__c, 
						AccountId = PM.pymt__Contact__r.Accountid, 
						SuppliedName = PM.pymt__Contact__r.Name, 
						SuppliedEmail = PM.pymt__Contact__r.Email, 
						SuppliedPhone  = PM.pymt__Billing_Phone__c, 
						Status = 'Pending Resp. - Email/Mail/Fax to Client', 
						Reason = 'Billing Credit Card Issue', 
						Subject  = 'Customer Credit Card Expireing on '+string.valueOf(testdate), 
						Type = 'Problem', Priority = 'High', 
						Case_Category__c = 'Non-Claim', 
						Case_Source__c = 'Client - System', 
						Received_From__c = 'Other', 
						Payment_Billing_Contact__c = PM.pymt__Contact__c, 
						Payment_Method__c = PM.Id);
						
						if(CRT != null) {
							CS.RecordTypeId = CRT.Id;
						}
						if(lstQGP != null) {
							CS.OwnerId = lstQGP[0].Id;
						}
						//add case to array
						Cases.add(CS);
					}					
					PymtMethod = PMs.get(0);
				}
				if(!Cases.isEmpty()) {
					system.debug('Cases are ' + Cases);
					insert Cases;
					
					
					//build the emails
					for (Case CS1 : Cases) {
						// add message to array	
						if(!Test.isRunningTest()) {
							Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
							mail.setTemplateId(Label.CC2MonthsExpiring);
							mail.setWhatId(PymtMethod.Id); 
							mail.setTargetObjectId(CS1.Id); // Id of Contact 
        					mail.setSaveAsActivity(false);
        					MessageArray.add(mail);
						}
					}
					if(!MessageArray.isEmpty()) {
						Messaging.sendEmail(MessageArray);
					}
				}
			}
		}
	}

	public static void ccExpireNotification(List <Order_Line_Items__c> OLIs) {
		/* Any change in this method should be done in ccExpireNotificationVF() also */
		set<Id> ContactIds = new set<Id>();
		Date TestDate;
		List<pymt__Payment_Method__c> PMs = new List<pymt__Payment_Method__c>();
		
		for (Order_Line_Items__c OLI:OLIs) {
			if(OLI.Billing_Contact__c != null) {
				ContactIds.add(OLI.Billing_Contact__c);
			}
		}		
		
		if(ContactIds.size() > 0) {				
			system.debug('Contact ids are ' + ContactIds);
			
			TestDate = ccexpireEmails.TodayDate.addMonths(2);			
			PMs =  PaymentMethodSOQLMethods.getExpireSoonCreditCardDetailsByExipiryDate(ContactIds, testDate);
			system.debug('Payment methods are ' + PMs);
			
			if(!PMs.isEmpty()) {
				sendingEmail(PMs, 2);
				PMs.clear();
			}
						
			TestDate = ccexpireEmails.TodayDate.addMonths(1);			
			PMs =  PaymentMethodSOQLMethods.getExpireSoonCreditCardDetailsByExipiryDate(ContactIds, testdate);
			system.debug('Payment methods are ' + PMs);
			
			if(!PMs.isEmpty()) {
				sendingEmail(PMs, 1);
				PMs.clear();
			}	
						
			TestDate = ccexpireEmails.TodayDate.addDays(7);			
			PMs =  PaymentMethodSOQLMethods.getExpireSoonCreditCardDetailsByExipiryDate(ContactIds, testDate);
			system.debug('Payment methods are ' + PMs);
			
			if(!PMs.isEmpty()) {
				ccexpireEmails.sendingEmail(PMs, 7);
			}					
		}		
	}
	
	public void ccExpireNotificationVF() {
		/* Any change in this method should be done in ccExpireNotification() also */
		system.debug('Today date is ' + TodayDate);
		set<Id> ContactIds = new set<Id>();
		Date TestDate;
		List<pymt__Payment_Method__c> PMs = new List<pymt__Payment_Method__c>();
		
		for (Order_Line_Items__c OLI : OLIList) {
			if(OLI.Billing_Contact__c != null) {
				ContactIds.add(OLI.Billing_Contact__c);
			}
		}		
		
		if(ContactIds.size() > 0) {				
			system.debug('Contact ids are ' + ContactIds);
			
			TestDate = ccexpireEmails.TodayDate.addMonths(2);	
			system.debug('Test Date for 2 months is ' + TestDate);		
			PMs =  PaymentMethodSOQLMethods.getExpireSoonCreditCardDetailsByExipiryDate(ContactIds, testDate);
			system.debug('Payment methods for 2 months are ' + PMs);
			
			if(!PMs.isEmpty()) {
				sendingEmail(PMs, 2);
				PMs.clear();
			}
						
			TestDate = ccexpireEmails.TodayDate.addMonths(1);	
			system.debug('Test Date for 1 month is ' + TestDate);			
			PMs =  PaymentMethodSOQLMethods.getExpireSoonCreditCardDetailsByExipiryDate(ContactIds, testdate);
			system.debug('Payment methods for 1 month are ' + PMs);
			
			if(!PMs.isEmpty()) {
				sendingEmail(PMs, 1);
				PMs.clear();
			}	
						
			TestDate = ccexpireEmails.TodayDate.addDays(7);		
			system.debug('Test Date for 1 week is ' + TestDate);		
			PMs =  PaymentMethodSOQLMethods.getExpireSoonCreditCardDetailsByExipiryDate(ContactIds, testDate);
			system.debug('Payment methods for 1 week are ' + PMs);
			
			if(!PMs.isEmpty()) {
				ccexpireEmails.sendingEmail(PMs, 7);
			}					
		}		
	}
	
	public static void sendingEmail(List<pymt__Payment_Method__c> PMs, Integer i) {
		/*
			Here Interger i defines
			2 - 2 months before notification has to be sent
			1 - 1 month before notification has to be sent
			7 - 1 week before notification has to be sent
		*/
		List <Messaging.SingleEmailMessage> Messages = new List <Messaging.SingleEmailMessage>();
		
		for(pymt__Payment_Method__c PymtMethod : PMs) {
			Messaging.SingleEmailMessage Message = new Messaging.SingleEmailMessage();
			if(i==2) {
				Message.setTemplateId(Label.CC2MonthsExpiring);
			} else if(i==1) {
				Message.setTemplateId(Label.CC1MonthExpiring);
			} else if(i==7) {
				Message.setTemplateId(Label.CC7DaysExpiring);
			}
			Message.setWhatId(PymtMethod.Id); 
			Message.setTargetObjectId(PymtMethod.pymt__Contact__c); // Id of Contact 
			Message.setSaveAsActivity(false);
			Messages.add(Message);
		}
		
		if(!Messages.isEmpty()) {
			system.debug('Messages are ' + Messages);
			Messaging.sendEmail(Messages);
		}
	}
	
	// Test Methods
	@IsTest(SeeAllData=true)
	static void ccexpireEmailsTest1() {
		ccexpireEmails Ctlr = new ccexpireEmails();
	}
}