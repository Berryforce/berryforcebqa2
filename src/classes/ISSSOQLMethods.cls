public class ISSSOQLMethods {
    public static map<Id, ISS__c> getISSByISSId(set<Id> setISSIds) {
        return new map<Id, ISS__c>([Select From_P__c, To_C__c, Trans__c, Trans_Version__c, Total_Tax__c, Total_Gross_Amount__c, Total_ADJ_Amount__c, TotalCommission__c, 
                                    To__c, State_City_Zip__c,Payment_Date__c, Publication_Code__c, Publication_Company__c, Publication_Company__r.Name, NetAmount__c, Name, Invoice_Date__c, Id, From__c, 
                                    CMR__c, CMR__r.Name, CMR_Number__c, CMR_Address__c, 
                                    (Select Product__c, Id, Name, Directory__c, Directory__r.Name, Directory_Edition__c, Directory_Edition__r.Edition_Code__c, 
                                    Directory_Edition__r.Name, Pubco_date__c, Client_Name__c, Client_Name__r.Name, Life__c, Inv_Type__c, Gross_Amount__c, Commission__c, ISS__c, 
                                    Directory_Code__c, Client_Number__c, Commission_Amount__c, Tax__c, Tax_Amount__c, ADJ_Amount__c, Net_Amount__c 
                                    From ISS_Line_Items__r) 
                                    From ISS__c 
                                    where Id IN :setISSIds]);
    }
    
    public static list<ISS__c> getISSByCMRId(set<Id> setCMRIDs) {
        return [Select From_P__c, To_C__c, Trans__c, Trans_Version__c, Total_Tax__c, Total_Gross_Amount__c, Total_ADJ_Amount__c, TotalCommission__c, 
                To__c, State_City_Zip__c, Payment_Date__c, Publication_Code__c, Publication_Company__c, Publication_Company__r.Name, NetAmount__c, Name, Invoice_Date__c, Id, From__c, 
                CMR__c, CMR__r.Name, CMR_Number__c, CMR_Address__c, 
                (Select Product__c, Id, Name, Directory__c, Directory__r.Name, Directory_Edition__c, Directory_Edition__r.Name, Pubco_date__c, 
                Client_Name__c, Client_Name__r.Name, Life__c, Inv_Type__c, Gross_Amount__c, Commission__c, ISS__c, 
                Directory_Code__c, Client_Number__c, Commission_Amount__c, Tax__c, Tax_Amount__c, ADJ_Amount__c, Net_Amount__c 
                From ISS_Line_Items__r) 
                From ISS__c 
                where CMR__c IN :setCMRIDs];
    }
    
    public static list<ISS__c> getISSByISS(list<ISS__c> lstISS) {
        return [SELECT name, CMR__c, CMR__r.name, From__c, To__c, Payment_Date__c, Total_ADJ_Amount__c, Trans__c, 
	            Invoice_Date__c, Total_Tax__c, Total_Gross_Amount__c, TotalCommission__c, NetAmount__c, CMR_Number__c 
	            FROM ISS__c WHERE id in :lstISS ORDER BY CMR_Number__c];
    }
    
    public static list<ISS__c> getISSByISSIds(set<Id> setISSIds) {
        return [SELECT Id, Name, CMR__c, CMR__r.name, From__c, To__c, Payment_Date__c, Total_ADJ_Amount__c, Trans__c, 
            Invoice_Date__c, Total_Tax__c, Total_Gross_Amount__c, TotalCommission__c, NetAmount__c, CMR__r.Id, CMR_Number__c 
            FROM ISS__c WHERE Id IN :setISSIds ORDER BY CMR_Number__c];
    }
}