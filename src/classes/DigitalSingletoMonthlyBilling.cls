global class DigitalSingletoMonthlyBilling implements Database.Batchable<sObject>{
    
    global string contractDate;
   
    global DigitalSingletoMonthlyBilling(date contractenddate){
        contractDate = String.valueOf(contractenddate);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String strRT = System.Label.TestOLIRTLocal;
       
        String query = 'SELECT Id,Contract_End_Date__c,Billing_Frequency__c FROM Order_Line_Items__c WHERE IsCanceled__c = false AND Next_Billing_Date__c='+contractDate+' AND Billing_Frequency__c= \'Single Payment\' AND RecordTypeId = \''+strRT+'\' AND Media_Type__c =\'Digital\'';
        system.debug('***Query***'+query);
        return Database.getQueryLocator(query);
    }  
    
    global void execute(Database.BatchableContext bc, List<Order_Line_Items__c> listOLI) {
        system.debug('***Query list oli size***'+listOLI.size());
        list<Order_Line_Items__c> lstOLIUpdate = new list<Order_Line_Items__c>();
        if(listOLI.size()> 0){
            for(Order_Line_Items__c iteratorOLI : listOLI){
                iteratorOLI.Billing_Frequency__c='Monthly';
                lstOLIUpdate.add(iteratorOLI);
            }
        }
        if(lstOLIUpdate.size()> 0){
            update lstOLIUpdate;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
     
    }
}