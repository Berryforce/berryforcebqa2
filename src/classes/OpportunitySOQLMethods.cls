public class OpportunitySOQLMethods {    
    public static list<Opportunity> getOpportunityForPendingItem(Id opportunityID) {
        return [Select RAS_ID__c, RAS_Date__c, Owner.Name, Owner.email, OwnerId ,isLocked__c, Name, IsWon, Signing_Method__c, Signing_Contact__c, 
        DocuSign_Received__c, IsClosed, Id, (Select Id From OpportunityLineItems) From Opportunity where Id =:opportunityID];
    }
    
    public static map<Id, Opportunity> getOpportunityCanvassById(set<Id> opportunityId) {
        return new map<Id, Opportunity>([Select Account_Primary_Canvass__r.Branch_Manager__c, Account_Primary_Canvass__r.Canvass__c, 
        Account_Primary_Canvass__r.Allow_Up_Front_Billing__c, Account_Primary_Canvass__r.Canvass_Code__c, Account_Primary_Canvass__r.Territory_Name__c, 
        Account_Primary_Canvass__r.IsActive__c, Account_Primary_Canvass__r.Billing_Partner__c, Account_Primary_Canvass__c, Id 
        From Opportunity where ID IN:opportunityId]);
    }
    
    public static list<Opportunity> getOpportunityForNational(set<Id> setOPPIDs) {
        return [Select Id, Name,Account_Primary_Canvass__r.Billing_Entity__c,AccountID, Account.Name, Account_Primary_Canvass__c, National_Staging_Order_Set__c, Transaction_Unique_ID__c,  
                (Select Seniority_Date__c,Discount,DAT__c, Trans_Version__c, PricebookEntry.Product2.ProductCode, PricebookEntry.Product2.Media_Type__c, PricebookEntry.Product2.Product_Type__c, UDAC__c, 
                Type__c,  Trans__c, To__c, Publication_Company__c, Publication_Code__c, Publication_Date__c, UnitPrice,
                National_Staging_Line_Item__r.Seniority_Date__c, National_Staging_Line_Item__r.National_Graphics_ID__c, National_Staging_Line_Item__c, 
                PricebookEntryId, OpportunityId, NAT__c, NAT_Client_ID__c, Directory_Heading__c, Line_Number__c, Id, Full_Rate__c, listing__c,
                From__c, Directory__c, Product_Type__c, Description, Date__c, Contact__c, Client_Number__c, Client_Name__c, CMR_Number__c, 
                CMR_Name__c,Trade_Mark_Parent_Id__c, Billing_Method__c, Billing_Duration__c, Advertising_Data__c, directory_edition__c, Action__c, Transaction_ID__c, Directory_Section__c, 
                Line_Unique_Id__c, Listing__r.Phone__c,OP_National_Graphics_ID__c
                From OpportunityLineItems) 
                from  Opportunity WHERE Id IN:setOPPIDs];
    }
    
    public static list<Opportunity> setOpportunityByIdForLockCloneInvoicePayment(set<Id> setOpportunityID) {
        return [SELECT modifyid__c, Late_Print_Order_Approved__c, Payment_Method__c, Billing_Contact__r.Contact_Role__c, Billing_Contact__r.Email, Override__c,
                Billing_Partner__c, Name,  Billing_Contact__r.Paperless__c,Net_Total__c, Billing_Contact__r.PrimaryBilling__c, OwnerId, DocuSign_Received__c, 
                Billing_Contact__r.Phone, Billing_Contact__r.MailingCountry, Billing_Contact__r.MailingPostalCode, Billing_Contact__r.MailingState, isLocked__c, IsWon,
                Billing_Contact__r.MailingCity, Billing_Contact__r.MailingStreet, Billing_Contact__c, Account.Delinquency_Indicator__c, Account.BillingCountry,
                Account.BillingPostalCode, Account.BillingState, Account.BillingCity, Account.BillingStreet, AccountId, Signing_Contact__c, RAS_ID__c, 
                RAS_Date__c, Owner.Name, Owner.email, IsClosed, Id, Signing_Method__c, Account.ACC_Prohibit_Telco_Billing__c, 
                (SELECT Id, OpportunityId, P4P_Billing__c, Is_P4P__c, UnitPrice, Original_Line_Item_ID__c, ProductCode__c, Directory__c, OPPLI_YPC_isModified__c, 
                PricebookEntryId, PricebookEntry.Product2Id, PricebookEntry.Product2.Inventory_Tracking_Group__c, Quantity, Renewals_Action__c, 
                Original_Line_Item_ID__r.Product2__c, Original_Line_Item_ID__r.Linvio_Payment_Method_Id__c,
                Section_Page_Type__c, Directory_Edition__r.Name,Original_Line_Item_ID__r.Package_ID_External__c, Parent_ID__c, Parent_ID_of_Addon__c,
                Directory_Heading__c, Requires_Inventory_Tracking__c, Directory_Section__c,Directory_Edition_Saleslockout_Date__c,Directory_Edition_Pub_Date__c 
                FROM OpportunityLineItems order by Parent_ID_of_Addon__c, Parent_ID__c desc) FROM Opportunity WHERE Id IN:setOpportunityID];
    }
    
    
    public static list<Opportunity> setOpportunityByIdForLockCloneCreateOrder(set<Id> setOpportunityID) {
        return [Select Account_Primary_Canvass__r.Billing_Entity__c,Close_Date__c,StageName,LastModifiedBy.Email,LastModifiedBy.ManagerId,Account.Name,Account.Berry_ID__c, Signing_Method__c, Owner.Name,Owner.LastName, Owner.email, Billing_Partner__c, modifyid__c, isLocked__c, Payment_Method__c, Name, Billing_Frequency__c, Billing_Contact__r.Contact_Role__c, Billing_Contact__r.Email, 
                Billing_Contact__r.Statement_Suppression__c, Billing_Contact__r.Phone, Billing_Contact__r.MailingCountry, Billing_Contact__r.MailingPostalCode, Billing_Contact__r.MailingState, 
                Billing_Contact__r.MailingCity, Billing_Contact__r.MailingStreet, Billing_Contact__c, Account.Delinquency_Indicator__c, Account.BillingCountry, Account.BillingPostalCode, Account.BillingState, Account.BillingCity, Account.BillingStreet, AccountId, Signing_Contact__c, Account.Phone, 
                Account.TalusAccountId__c, Billing_Contact__r.TalusContactId__c, OwnerId, Owner.FirstName, Owner.CompanyName, Owner.Fax, Owner.Phone, Owner.PostalCode, Owner.Street, Owner.city, Owner.State, Owner.Country, Owner.CommunityNickname,  
                Account.RecordTypeID,Account.Statement_Suppression__c,Account_Number__c, Account.Account_Number__c, Account_Manager__r.Email, Account_Manager__r.FirstName, Account_Manager__r.LastName, Account_Manager__c,
                Sig_Contact_Email__c,Account_Primary_Canvass__r.Primary_Telco__r.Sold_Letter_Logos__c,Account_Primary_Canvass__r.Primary_Telco__r.Document_Id__c,
                (Select Id, Opportunity.Owner.Email,Opportunity.Owner.FirstName, Opportunity.Owner.LastName, Opportunity.modifyid__c, OpportunityId, Full_Rate__c,SortOrder, PricebookEntryId, Quantity, Discount, Core_Migration_ID__c, 
                UnitPrice, ServiceDate, Description, Listing_Id__c, Listing__c, Renewals_Action__c, PricebookEntry.Product2.Inventory_Tracking_Group__c,
                Is_Anchor__c,Is_Caption__c,Is_Child__c, Canvass__c, PricebookEntry.Name, directory__c, PricebookEntry.Product2.Media_Type__c,
                PricebookEntry.Pricebook2Id, PricebookEntry.Product2Id, PricebookEntry.ProductCode, Parent_ID__c, PricebookEntry.Product2.Product_Type__c,  
                PricebookEntry.Product2.Addon_Type__c, Distribution_Area__c, Original_Line_Item_ID__r.Order_Group__c, PricebookEntry.Product2.Name,
                PricebookEntry.Product2.Is_IBUN_Bundle_Product__c, Package_ID_External__c, Original_Line_Item_ID__r.Directory_Edition__c,
                Digital_Product_Requirement__c,Effective_Date__c,Billing_Duration__c,Product_Type__c,Billing_Start_Date__c, Billing_End_Date__c, 
                isModify__c,Directory__r.Telco_Provider__c,Canvass__r.Primary_Telco__c, Package_ID__c, Parent_Line_Item__c, Geo_Type__c, Geo_Code__c,  
                is_p4p__c,Scope__c, Directory_Section__c, Status__c,Category__c, Directory_Heading__c, P4P_Price_Per_Click_Lead__c,P4P_Billing__c , 
                Directory_Edition__r.Bill_Prep__c, Auto_Number__c, Locality__c, Directory_Edition__c, Directory_Edition__r.New_Print_Bill_Date__c,
                Listing_Name__c,Pricing_Program__c, Parent_ID_of_Addon__c, Original_Line_Item_ID__c, strOppliCombo__c, Package_Item_Quantity__c,
                Package_Name__c,Display_Ad__c,PricebookEntry.Product2.Vendor__c, Original_Line_Item_ID__r.Digital_Product_Requirement__c,
                Original_Line_Item_ID__r.Digital_Product_Requirement__r.Data_Fulfillment_Form__c,Original_Line_Item_ID__r.FulfillmentDate__c,
                Original_Line_Item_ID__r.Effective_Date__c, UDAC__c,Seniority_Date_Reset__c,Original_OLI_Seniority_Date__c,
                Original_Line_Item_ID__r.Talus_Go_Live_Date__c,Original_Line_Item_ID__r.Sent_to_fulfillment_on__c, Directory_Edition__r.Name,
                Original_Line_Item_ID__r.Status__c, Original_Line_Item_ID__r.Directory__c, Original_Line_Item_ID__r.Listing__c,Original_Line_Item_ID__r.Talus_Subscription_ID__c, Original_Line_Item_ID__r.Talus_DFF_Id__c,
                Original_Line_Item_ID__r.ProductCode__c,Original_Line_Item_ID__r.Directory_Heading__c,Original_Line_Item_ID__r.Scoped_Caption_Header__c,Original_Line_Item_ID__r.Package_ID_External__c,
                Directory_Heading1__c,Directory_Heading2__c,Directory_Heading3__c,Directory_Heading4__c,BCC_Applied__c,BCC_Duration__c,BCC__c,BCC_Redeemed__c,
                Berry_Cares_Certificate_ID__c,OPPLI_Exception_Discount__c, Opportunity.Owner.CommunityNickname, Seniority_Date__c, Original_Line_Item_ID__r.P4P_Tracking_Number_ID__c,
                Original_Line_Item_ID__r.P4P_Tracking_Number_Status__c
                From OpportunityLineItems order by Parent_ID__c, Parent_ID_of_Addon__c desc)
                From Opportunity where Id IN:setOpportunityID and Account.RecordTypeID=:System.Label.TestAccountCustomerRT];
    }
    
    public static list<Opportunity> getOpportunityForLockCloneCreateOrder(List<Opportunity> lstOpportunity) {
        return [Select Account_Primary_Canvass__r.Billing_Entity__c,Account.Billing_Anniversary_Date__c,Close_Date__c,StageName,LastModifiedBy.Email,LastModifiedBy.ManagerId,Account.Name,Account.Berry_ID__c, Signing_Method__c, Owner.Name, Owner.email, Billing_Partner__c, modifyid__c, isLocked__c, Payment_Method__c, Name, Billing_Frequency__c, Billing_Contact__r.Contact_Role__c, Billing_Contact__r.Email, 
                Billing_Contact__r.Statement_Suppression__c, Billing_Contact__r.Phone, Billing_Contact__r.MailingCountry, Billing_Contact__r.MailingPostalCode, Billing_Contact__r.MailingState, 
                Billing_Contact__r.MailingCity, Billing_Contact__r.MailingStreet, Billing_Contact__c, Account.Delinquency_Indicator__c, Account.BillingCountry, Account.BillingPostalCode, Account.BillingState, Account.BillingCity, Account.BillingStreet, AccountId, Signing_Contact__c, Account.Phone, 
                Account.TalusAccountId__c, Billing_Contact__r.TalusContactId__c, OwnerId, Owner.FirstName,Owner.LastName, Owner.CompanyName, Owner.Fax, Owner.Phone, Owner.PostalCode, Owner.Street, Owner.city, Owner.State, Owner.Country, Owner.CommunityNickname, 
                Account.Statement_Suppression__c,Account_Number__c, Account.Account_Number__c, Account_Manager__r.Email, Account_Manager__r.FirstName, Account_Manager__r.LastName, Account_Manager__c,
                (Select Id, Original_Line_Item_ID__r.Package_ID_External__c, Opportunity.Owner.Email,Opportunity.Owner.FirstName, Opportunity.Owner.LastName, Opportunity.modifyid__c, OpportunityId, Full_Rate__c,SortOrder, PricebookEntryId, Quantity, Discount, Core_Migration_ID__c, 
                UnitPrice, ServiceDate, Description, Listing_Id__c, Listing__c, Renewals_Action__c, PricebookEntry.Product2.Inventory_Tracking_Group__c,
                Is_Anchor__c,Is_Caption__c,Is_Child__c, Canvass__c, PricebookEntry.Name, directory__c, PricebookEntry.Product2.Media_Type__c,
                PricebookEntry.Pricebook2Id, PricebookEntry.Product2Id, PricebookEntry.ProductCode, Parent_ID__c, PricebookEntry.Product2.Product_Type__c,  
                PricebookEntry.Product2.Addon_Type__c, Distribution_Area__c, Original_Line_Item_ID__r.Order_Group__c,  PricebookEntry.Product2.Name,
                PricebookEntry.Product2.Is_IBUN_Bundle_Product__c, Package_ID_External__c, Original_Line_Item_ID__r.Directory_Edition__c,
                Digital_Product_Requirement__c,Effective_Date__c,Billing_Duration__c,Product_Type__c,Billing_Start_Date__c, Billing_End_Date__c, 
                isModify__c,Directory__r.Telco_Provider__c,Canvass__r.Primary_Telco__c, Package_ID__c, Parent_Line_Item__c, Geo_Type__c, Geo_Code__c,  
                is_p4p__c,Scope__c, Directory_Section__c, Status__c,Category__c, Directory_Heading__c, P4P_Price_Per_Click_Lead__c,P4P_Billing__c , 
                Directory_Edition__r.Bill_Prep__c, Auto_Number__c, Locality__c, Directory_Edition__c, Directory_Edition__r.New_Print_Bill_Date__c,
                Listing_Name__c,Pricing_Program__c, Parent_ID_of_Addon__c, Original_Line_Item_ID__c, strOppliCombo__c, Package_Item_Quantity__c,
                Package_Name__c,Display_Ad__c,PricebookEntry.Product2.Vendor__c, Original_Line_Item_ID__r.Digital_Product_Requirement__c,Original_Line_Item_ID__r.Digital_Product_Requirement__r.Data_Fulfillment_Form__c,
                Original_Line_Item_ID__r.Effective_Date__c, UDAC__c,Seniority_Date_Reset__c,Original_OLI_Seniority_Date__c,
                Original_Line_Item_ID__r.Talus_Go_Live_Date__c,Original_Line_Item_ID__r.Sent_to_fulfillment_on__c,Original_Line_Item_ID__r.Status__c,Original_Line_Item_ID__r.Talus_Subscription_ID__c, Original_Line_Item_ID__r.Talus_DFF_Id__c,
                Original_Line_Item_ID__r.Directory__c, Original_Line_Item_ID__r.Listing__c, Directory_Edition__r.Name,
                Original_Line_Item_ID__r.ProductCode__c,Original_Line_Item_ID__r.Directory_Heading__c,Original_Line_Item_ID__r.Scoped_Caption_Header__c,
                Directory_Heading1__c,Directory_Heading2__c,Directory_Heading3__c,Directory_Heading4__c, BCC_Applied__c,BCC_Duration__c,BCC__c,BCC_Redeemed__c,Berry_Cares_Certificate_ID__c,OPPLI_Exception_Discount__c,
                Opportunity.Owner.CommunityNickname, Seniority_Date__c, Original_Line_Item_ID__r.P4P_Tracking_Number_ID__c, Original_Line_Item_ID__r.P4P_Tracking_Number_Status__c
                From OpportunityLineItems order by Parent_ID__c, Parent_ID_of_Addon__c)                
                From Opportunity where Id IN:lstOpportunity];
    }
        
    public static list<Opportunity> getOpportunityByAcctIds(set<Id> accountIDs){
        return [Select Name, Id, AccountId, Account_Primary_Canvass__c, Account_Primary_Canvass__r.Name
        From Opportunity WHERE AccountId IN : accountIDs];
    }
}