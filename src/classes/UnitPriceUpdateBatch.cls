global class UnitPriceUpdateBatch implements Database.Batchable<sObject> {
    Date tody;
    global UnitPriceUpdateBatch(Date natRatDueDate) {
        tody = natRatDueDate;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {    
        Id nationalRecTypId = CommonMethods.getRedordTypeIdByName(CommonMessages.nationaOLIRT, CommonMessages.OLIObjectName);  
        String SOQL = 'SELECT Id FROM National_Staging_Line_Item__c WHERE National_Staging_Header__r.Directory_Edition__r.National_Rates_Due__c = : tody AND National_Staging_Header__r.Directory_Edition__r.Book_Status__c = \'NI\'';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List <National_Staging_Line_Item__c> lstNSLI) {
        update lstNSLI;
        List<Order_Line_Items__c> lstOLI = [select id, UnitPrice__c, National_Staging_Line_Item__r.Sales_Rate__c, National_Staging_Line_Item__c FROM Order_Line_Items__c WHERE National_Staging_Line_Item__c in : lstNSLI];
        for(Order_Line_Items__c OLI : lstOLI) {
            OLI.UnitPrice__c = OLI.National_Staging_Line_Item__r.Sales_Rate__c;
        }           
        update lstOLI;
    }
    
    global void finish(Database.BatchableContext bc) {
    }
}