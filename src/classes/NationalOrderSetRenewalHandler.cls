/***************************************
* Class Name     : NationalOrderSetRenewalHandler
* Created By     : Ankit Nigam
* Created Date   : 7/22/2014
* Description    : Renew National Order Set and Lines 
***************************************/

public class NationalOrderSetRenewalHandler
{
    /***************************************
    * Method Name    : RenewNationalOrder
    * Created By     : Ankit Nigam
    * Created Date   : 7/22/2014
    * Description    : Cloning National Order Set and Lines maintaining the relationship between new records
    * Parameters     : List of Directory Edition with National Staging Order Set 
    ***************************************/
   public static Map<String,String> chidlObjAPIName_FieldAPIName = new Map<String,String>{};
   public static void RenewNationalOrder(List<Directory_Edition__c> ListDE)
   {
      List<National_Staging_Order_Set__c> NationalOrderSetList = new List<National_Staging_Order_Set__c>();
      Map<id,National_Staging_Order_Set__c> MapNSOS=new Map<id,National_Staging_Order_Set__c>();
      Map<Id,Map<Id,National_Staging_Line_Item__c>> MapNSLI=new Map<Id,Map<Id,National_Staging_Line_Item__c>>();
      
      //Variables for Renewal Start here
      Map<Id,Map<Id,Opportunity>> MapNSOSParentOppClonedOpp=new Map<Id,Map<Id,Opportunity>>();
      Map<Id,Map<Id,Order_Group__c>> MapNSOSParentOrderSetClonedOrderSet=new Map<Id,Map<Id,Order_Group__c>>();
      Map<Id,Map<Id,Order__c>> MapNSOSParentOrderClonedOrder=new Map<Id,Map<Id,Order__c>>();
      List<Opportunity> ClonedOppList=new List<Opportunity>();
      List<Order__c> ClonedOrderList=new List<Order__c>();
      List<Order_Group__c> ClonedOrderGroupList=new List<Order_Group__c>();
      set<Id> setNSLI=new set<Id>();
      set<Id> setOpportunity=new set<Id>();
      set<Id> setOrder=new set<Id>();
      //Variable for renewal ends here
      
      Map<Id,National_Staging_Line_Item__c> mapIDSNLI =new Map<Id,National_Staging_Line_Item__c>();
      List<National_Staging_Line_Item__c> NationalChilLineList=new List<National_Staging_Line_Item__c >();
      Set<Id> DirectoryIds=new Set<Id>();
      Set<Id> DEIds=new Set<Id>();
      Set<Id> NSOSIds=new Set<Id>();
      Map<id,Directory_Edition__c> MapNewDE=new Map<id,Directory_Edition__c>();
      Map<id,List<National_Staging_Order_Set__c>> DENSOSMap = new Map<id,List<National_Staging_Order_Set__c>>();
      for(Directory_Edition__c DEIterator :ListDE)
      {
         DEIds.add(DEIterator.Id);
         DirectoryIds.add(DEIterator.Directory__c);
         for(National_Staging_Order_Set__c  nsoIterator : DEIterator.National_Staging_Order_Sets__r)
         {
               NSOSIds.add(nsoIterator.id);
               
         }
      }
      
      for(Directory__c dirIterator :[Select id,(Select id,Book_Status__c,Directory__c,Pub_Date__c,Edition_Code__c,LSA_Directory_Version__c from Directory_Editions__r where Book_Status__c='NI' Order By CreatedDate DESC Limit 1) from Directory__c where id in:DirectoryIds])
      {
          for(Directory_Edition__c DEIterator : dirIterator.Directory_Editions__r)
          {
              MapNewDE.put(DEIterator.Directory__c,DEIterator);
              
          }
      }
      
      for(National_Staging_Order_Set__c nsosIterator : [SELECT Auto_Number__c,ChildReadyForProcess__c,Child_Total__c,
      Client_Name_Text__c,Client_Name__c,Client_Number__c,CMRClientCombo__c,CMR_Name__c,CMR_Notice__c,CMR_Number__c,
      CORE_Migration_ID__c,Date__c,Directory_Edition_Number__c,Directory_Edition__c,
      Directory_Number__c,Directory_Version__c,Directory__c,From_Number__c,From_Type__c,Header_Error_Description__c,
      Id,Informatica_Unique_ID__c,IsDeleted,Is_Converted__c,Is_Ready__c,
      Name,NAT_Client_Id__c,NAT__c,Non_Elite_CMR__c,Number_of_Graphics__c,Opportunity__c,OwnerId,
      Publication_Company_Code_c__c,Publication_Date__c,RAC_Date__c,Ready_for_Process__c,RecordTypeId,
      Reference_Date__c,SAC_Date__c,Standing_Order__c,State__c,SystemModstamp,To_Number__c,To_Type__c,Transaction_ID__c,
      Transaction_Unique_ID__c,Transaction_Version_ID__c,Transaction_Version__c,TRANS_Code__c,
      (SELECT Action__c,Advertising_Data__c,Auto_Number__c,BAS__c,Caption_Display_Text__c,Caption_Header__c,
      Caption_Indent_Level__c,Caption_Indent_Order__c,Caption_Phone_Number__c,
      CORE_Migration_ID__c,DAT__c,Delete__c,Directory_Code__c,Directory_Heading__c,
      Directory_Section__c,Directory__c,Discount__c,Full_Rate_f__c,Id,Is_Changed__c,Is_Processed__c,
      Line_Error_Description__c,Line_Number__c,Listing__c,Migration_Full_Rate__c,Name,National_Graphics_ID__c,
      National_Pricing__c,National_Staging_Header__c,Needs_Review__c,New_Transaction__c,Parent_ID__c,
      Processed_Order_Line_Item__c,Product2__c,RAC_Date__c,Ready_for_Processing__c,Row_Type__c,Seniority_Date__c,
      SAC_Date__c,Sales_Rate__c,Sales_Rate_Rounded__c,Scoped_Listing__c,SEQ__c,SPINS__c,Standing_Order__c,
      SystemModstamp,Transaction_Id__c,Transaction_Line_Id__c,Transaction_Unique_ID__c,Transaction_Version_ID__c,
      Type__c,UDAC__c FROM National_Staging_Line_Items__r where Standing_Order__c=true AND Action__c!='O'),
      (SELECT AccountId,Account_Manager__c,Account_Number__c,Account_Primary_Canvass__c,Allow_Upfront_Payment__c,Amount,Approval_Status__c,Auto_Number__c,BigMachines__Line_Items__c,Billing_Contact__c,billing_defaults_set__c,Billing_Frequency__c,Billing_Locked__c,Billing_Partner__c,BMI_Quote_Exists__c,c2g__CODAUnitOfWork__c,CampaignId,Cancellation_Letter_Sent__c,Client_Tier__c,CloseDate,Closed_Lost_Reason__c,Close_Date__c,cmr_Number__c,Converted_From_Lead__c,CORE_Migration_ID_temp__c,CORE_Migration_ID__c,Co_Op_Ads_and_Brands__c,Credit_Check__c,DB_Competitor__c,Delinquency_Status__c,Description,Directory_Number__c,DocuSign_Received__c,Enterprise_Customer_ID__c,ExpectedRevenue,Fiscal,FiscalQuarter,FiscalYear,ForecastCategory,ForecastCategoryName,Guaranteed_Calls__c,Guaranteed_End_Date__c,Guaranteed_Start_Date__c,HasOpportunityLineItem,Id,IsClosed,isLocked__c,IsPrivate,IsWon,Late_Print_Order_Approved__c,Late_Print_Order_Description__c,Late_Print_Order_Rejected__c,LeadSource,Main_Listed_City__c,Main_Listed_Phone__c,Main_Listed_State__c,Main_Listed_Street__c,Main_Listed_Zip_Code__c,modifyid__c,Name,National_Staging_Order_Set__c,Net_Total__c,NextStep,NotProcessed__c,Number_of_Cancelled_Opportunity_Products__c,Number_of_Opportunity_Products__c,Opportunity_Owner_Email__c,Opportunity_Owner_Phone__c,Override__c,OwnerId,P4P_Price_Quote__c,Payment_Method__c,PIA__c,Prepayment_Required__c,Pricebook2Id,Probability,pymt__Balance__c,pymt__Currency_ISO_Code__c,pymt__Frequency__c,pymt__Invoice_Number__c,pymt__Number_of_Payments_Made__c,pymt__Occurrences__c,pymt__Paid_Off__c,pymt__Payments_Made__c,pymt__Period__c,pymt__PO_Number__c,pymt__Recurring_Amount__c,pymt__Shipping__c,pymt__SiteQuote_Expiration__c,pymt__SiteQuote_Recurring_Setup__c,pymt__Site_Name__c,pymt__Subscription_Start_Date__c,pymt__Tax__c,pymt__Total_Amount__c,RAS_Date__c,RAS_ID__c,RecordTypeId,Requires_Late_Print_Order_Approval__c,Send_Cancellation_Letter__c,Signing_Contact__c,Signing_Method__c,Sig_Contact_Email__c,Sig_Contact_First__c,Sig_Contact_FullName__c,Sig_Contact_Last__c,Sig_Contact_Title__c,StageName,Telco__c,TmpOppty_Total_Price__c,TotalOpportunityQuantity,TotalSinglePaymentAmount__c,Transaction_Unique_ID__c,Type,Website__c From Opportunities__r),
      (SELECT Id,All_Line_Items_Cancelled__c,Canvass__c,Order__r.Name,CMR_Client__c,Commission_Ready__c,InvoiceGenerationStatus__c,IsActive__c,Name,National_Staging_Order_Set__c,oli_all_count__c,oli_cancelled_count__c,oli_count__c,Opportunity__c,Order_Account__c,Order_Set_Selected_Cancel__c,Order__c,OS_Delinquency_Indicator__c,OS_Directory_Editions__c,OS_Mailing_Address__c,OS_No_of_Digital_OLI__c,OS_No_of_Print_OLI__c,OS_Phone_Number__c,RecordTypeId,selected__c,OS_Total_OLI_Value__c,Transaction_ID__c,Type__c,Worked__c FROM Order_Sets__r) 
      FROM National_Staging_Order_Set__c where id in : NSOSIds AND Is_Ready__c=true AND Is_Converted__c=true and TRANS_Code__c!='D'])
      {
          National_Staging_Order_Set__c newNSOS= nsosIterator.clone(false,true);
          newNSOS.Is_Converted__c=true;
          newNSOS.Directory_Edition__c=MapNewDE.get(newNSOS.Directory__c).Id;
          newNSOS.Directory_Version__c=MapNewDE.get(newNSOS.Directory__c).LSA_Directory_Version__c;
          newNSOS.Directory_Edition_Number__c=MapNewDE.get(newNSOS.Directory__c).Edition_Code__c;
          newNSOS.Publication_Date__c=MapNewDE.get(newNSOS.Directory__c).Pub_Date__c;
          //newNSOS.Transaction_Unique_ID__c=nsosIterator.CMR_Number__c+nsosIterator.Client_Number__c+nsosIterator.Directory_Number__c+MapNewDE.get(newNSOS.Directory__c).LSA_Directory_Version__c;
          newNSOS.Transaction_ID__c=nsosIterator.CMR_Number__c+nsosIterator.Client_Number__c+nsosIterator.Directory_Number__c+MapNewDE.get(newNSOS.Directory__c).LSA_Directory_Version__c;
          NationalOrderSetList.add(newNSOS);
          Map<Id,National_Staging_Line_Item__c> tempMap=new Map<Id,National_Staging_Line_Item__c>();
          for(National_Staging_Line_Item__c NSLI : nsosIterator.National_Staging_Line_Items__r)
          {
              setNSLI.add(NSLI.Id);
              tempMap.put(NSLI.id,NSLI);
          }
          for(Opportunity iterator: nsosIterator.Opportunities__r){
              Opportunity clonedOpp= iterator.clone(false,true);
              if(!MapNSOSParentOppClonedOpp.containskey(iterator.National_Staging_Order_Set__c)){
                   map<Id,Opportunity> tempMapOpp=new map<Id,Opportunity>();
                   tempMapOpp.put(iterator.Id,clonedOpp);
                   MapNSOSParentOppClonedOpp.put(iterator.National_Staging_Order_Set__c,tempMapOpp);
              }
              else{
                  MapNSOSParentOppClonedOpp.get(iterator.National_Staging_Order_Set__c).put(iterator.Id,clonedOpp);
              }
         }
          
          for(Order_Group__c iterator: nsosIterator.Order_Sets__r){
              
              if(iterator.Order__c !=null){
                  Order__c iteratorOrder=new Order__c(Id=iterator.Order__c);
                  Order__c clonedOrder= iteratorOrder.clone(false,true);
                  clonedOrder.Account__c=iterator.Order_Account__c;
                  clonedOrder.Name=iterator.Order__r.Name;
                  if(!MapNSOSParentOrderClonedOrder.containskey(iterator.National_Staging_Order_Set__c)){
                   map<Id,Order__c > tempMapOrder=new map<Id,Order__c >();
                   tempMapOrder.put(iterator.Order__c,clonedOrder);
                   MapNSOSParentOrderClonedOrder.put(iterator.National_Staging_Order_Set__c,tempMapOrder);
                  }
                  else{
                  MapNSOSParentOrderClonedOrder.get(iterator.National_Staging_Order_Set__c).put(iterator.Order__c,clonedOrder);
                  }
                  ClonedOrderList.add(clonedOrder);
              }
              Order_Group__c clonedOrderSet= iterator.clone(false,true);
              if(!MapNSOSParentOrderSetClonedOrderSet.containskey(iterator.National_Staging_Order_Set__c)){
                   map<Id,Order_Group__c > tempMapOpp=new map<Id,Order_Group__c >();
                   tempMapOpp.put(iterator.Id,clonedOrderSet);
                   MapNSOSParentOrderSetClonedOrderSet.put(iterator.National_Staging_Order_Set__c,tempMapOpp);
              }
              else{
                  MapNSOSParentOrderSetClonedOrderSet.get(iterator.National_Staging_Order_Set__c).put(iterator.Id,clonedOrderSet);
              }
              
          }
          MapNSOS.put(nsosIterator.Id,newNSOS);
          MapNSLI.put(nsosIterator.Id,tempMap);
      }
      
      if(NationalOrderSetList.size()>0) insert NationalOrderSetList;
      
      if(MapNSOS.size()>0){
          for(Id nsosIteraror: MapNSOS.keyset()){
              for(Id OriginalOppiterator: MapNSOSParentOppClonedOpp.get(nsosIteraror).keyset()){
                  Opportunity Clonedopp= MapNSOSParentOppClonedOpp.get(nsosIteraror).get(OriginalOppiterator);
                  Clonedopp.National_Staging_Order_Set__c=MapNSOS.get(nsosIteraror).Id;
                  ClonedOppList.add(Clonedopp);
              }
       }
      if(ClonedOppList.size()>0) insert ClonedOppList;
      if(ClonedOrderList.size()>0) insert ClonedOrderList;
      }
      
      
      for(Id nsosIteraror: MapNSOS.keyset()){
          for(Id OriginalSetiterator: MapNSOSParentOrderSetClonedOrderSet.get(nsosIteraror).keyset()){
               Order_Group__c clonedOrderSet=MapNSOSParentOrderSetClonedOrderSet.get(nsosIteraror).get(OriginalSetiterator);
               if(MapNSOSParentOrderClonedOrder.get(nsosIteraror).containskey(clonedOrderSet.Order__c)){
                   clonedOrderSet.Order__c=MapNSOSParentOrderClonedOrder.get(nsosIteraror).get(clonedOrderSet.Order__c).Id;
                   clonedOrderSet.National_Staging_Order_Set__c=MapNSOS.get(nsosIteraror).Id;
               }
               if(MapNSOSParentOppClonedOpp.get(nsosIteraror).containskey(clonedOrderSet.Opportunity__c)){
               clonedOrderSet.Opportunity__c=MapNSOSParentOppClonedOpp.get(nsosIteraror).get(clonedOrderSet.Opportunity__c).Id;
               }
               ClonedOrderGroupList.add(clonedOrderSet);
          }
      }
      
      if(ClonedOrderGroupList.size()>0) insert ClonedOrderGroupList;
      
      map<Id,map<Id,OpportunityLineItem>> mapNSLIOppLi=new map<Id,map<Id,OpportunityLineItem>>();
      map<Id,map<Id,Order_Line_Items__c>> mapNSLIOLi=new map<Id,map<Id,Order_Line_Items__c>>();
      set<id> setOLIIds=new set<Id>();
      set<Id> setDFFIds=new set<Id>();
      List<OpportunityLineItem> ListClonedOppLI=new List<OpportunityLineItem>();
      List<Order_Line_Items__c> ListClonedOLI=new List<Order_Line_Items__c>();
      map<string,Order_Line_Items__c> MapAnchor=new map<string,Order_Line_Items__c>();    
      Map<Id,Id> mapOriginalOLIIdDFFId= new Map<Id,Id>();
      map<Id,Order_Line_Items__c> mapOriginalOLIClonedOLI=new map<Id,Order_Line_Items__c>();
      map<Id, Order_Line_Items__c> mapOLIByNSLI = new map<Id, Order_Line_Items__c>();
      map<Id,Digital_Product_Requirement__c> mapOldDFFIdClonedDFF=new map<Id,Digital_Product_Requirement__c>();
      
      if(setNSLI.size()>0){
          
          for(National_Staging_Line_Item__c nsliiter : [SELECT Action__c,Advertising_Data__c,Auto_Number__c,BAS__c,
          Caption_Display_Text__c,Caption_Header__c,Caption_Indent_Level__c,Caption_Indent_Order__c,Caption_Phone_Number__c,
      CORE_Migration_ID__c,DAT__c,Delete__c,Directory_Code__c,Directory_Heading__c,
      Directory_Section__c,Directory__c,Discount__c,Full_Rate_f__c,Id,Is_Changed__c,Is_Processed__c,
      Line_Error_Description__c,Line_Number__c,Listing__c,Migration_Full_Rate__c,Name,National_Graphics_ID__c,
      National_Pricing__c,National_Staging_Header__c,Needs_Review__c,New_Transaction__c,Parent_ID__c,
      Processed_Order_Line_Item__c,Product2__c,RAC_Date__c,Ready_for_Processing__c,Row_Type__c,Seniority_Date__c,
      SAC_Date__c,Sales_Rate__c,Sales_Rate_Rounded__c,Scoped_Listing__c,SEQ__c,SPINS__c,Standing_Order__c,
      Transaction_Id__c,Transaction_Line_Id__c,Transaction_Unique_ID__c,Transaction_Version_ID__c,Type__c,UDAC__c,
          (SELECT Action__c,Advertising_Data__c,Anchor_Ad__c,Auto_Number__c,BAS__c,BCC_Applied__c,BCC_Duration__c,BCC_Name__c,BCC_Redeemed__c,BCC__c,Berry_Cares_Certificate_ID__c,BigMachines__Origin_Quote__c,BigMachines__Synchronization_Id__c,Billing_Duration__c,Billing_End_Date__c,Billing_Method__c,Billing_Partner__c,Billing_Start_Date__c,c2g__CODAUnitOfWork__c,Cancel_Date__c,Canvass__c,Category__c,Client_Name__c,Client_Number__c,CMR_Name__c,CMR_Number__c,Contact__c,CORE_Migration_ID__c,Date__c,DAT__c,Description,Digital_Product_Requirement__c,Directory_Edition_Pub_Date__c,Directory_Edition_Saleslockout_Date__c,Directory_Edition__c,Directory_Heading1__c,Directory_Heading2__c,Directory_Heading3__c,Directory_Heading4__c,Directory_Heading__c,Directory_Number__c,Directory_Section__c,Directory__c,Discount,Display_Ad__c,Distribution_Area__c,DoNotConvert__c,Duration__c,Effective_Date__c,Exception_Discount_Applied__c,From__c,Full_Rate__c,Geo_Code__c,Geo_Name__c,Geo_Type__c,Heading_Code__c,Heading__c,Id,IsActive__c,IsCancel__c,isModify__c,Is_Anchor__c,Is_Caption__c,Is_Child__c,Is_P4P__c,Line_Number__c,Line_Unique_Id__c,Listing_Id__c,Listing_Name__c,Listing__c,ListPrice,Locality__c,Margin__c,Migration_ID__c,National_Staging_Line_Item__c,NAT_Client_ID__c,NAT__c,OPPLI_Exception_Discount__c,OPPLI_Modification_Order_Line_Item__c,OpportunityId,OP_National_Graphics_ID__c,Original_Line_Item_ID__c,Original_OLI_Directory_Heading__c,Original_OLI_Geo_Code__c,Original_OLI_Seniority_Date__c,Original_OLI_UDAC_Group__c,Original_OLI_UDAC__c,P4P_Billing__c,P4P_Price_Per_Click_Lead__c,Package_ID_External__c,Package_ID__c,Package_Item_Quantity__c,Package_Name__c,Parent_ID_of_Addon__c,Parent_ID__c,Parent_Line_Item__c,Previous_End_Date__c,Previous_Start_Date__c,PricebookEntryId,Pricing_Program__c,ProductCode__c,Product_Type__c,Publication_Code__c,Publication_Company__c,Publication_Date__c,Quantity,Renewals_Action__c,Renewal_Directory_Heading_Change__c,Renewal_Geo_Code_Change__c,Renewal_UDAC_Change__c,Requires_Inventory_Tracking__c,Scope__c,Section_Code__c,Section_Page_Type__c,Seniority_Date_Reset__c,Seniority_Date__c,ServiceDate,Single_Payment_Total__c,SortOrder,SPINS__c,Start_Date__c,Status__c,strOppliCombo__c,strOpplineProductCombo__c,Subtotal,ToPubco_Number__c,To_Pubco__c,To__c,Trade_Mark_Parent_Id__c,Transaction_ID__c,TransIDLineNo__c,Trans_Version__c,Trans__c,Type__c,UDAC_Group__c,UDAC__c,UnitPrice from Opportunity_Product__r),
          (SELECT Product2__r.Is_Anchor__c,Digital_Product_Requirement__r.National_Graphics_ID__c ,Account__c,Action_Code__c,Action__c,Addon_Type__c,Advertised_Phone_Number__c,Advertising_Data__c,Advice_Query__c,Anchor_Ad__c,Anchor_Caption_Header_Record_ID__c,Anchor_Listing_Caption_Header__c,BAS__c,BillingChangeNoofDays__c,BillingChangeProrateCreditDays__c,BillingChangesPayment__c,BillingTransferedInfo__c,BillingTransfered__c,Billing_Change_Prorate_Credit__c,Billing_Close_Date__c,Billing_Contact__c,Billing_End_Date__c,Billing_Frequency_Change_Date__c,Billing_Frequency_Change__c,Billing_Frequency__c,Billing_Partner_Account__c,Billing_Partner_Change__c,Billing_Partner_Transferred__c,Billing_Partner__c,Billing_Start_Date__c,Billing_Transfer_Type__c,Call_Tracking_Phone_Number__c,Cancelation_Fee__c,Cancellation_Reason1__c,Cancellation__c,Cancel_Fee_Sales_Invoice_Created__c,Canvass__c,Caption_Or_Extra_Line_Text__c,Case__c,Category__c,checked__c,Client_Number__c,CMR_Name__c,CMR_Number_Client_Number__c,CMR_Number__c,Comments__c,Continious_Billing__c,Contract_End_Date__c,Contract_Start_Date__c,Core_Opportunity_Line_ID__c,Current_Billing_Period_Days2__c,Current_Billing_Period_Days__c,Current_Daily_Prorate__c,Current_Invoice__c,Current_Month_for_P4P__c,Current_Rate__c,Custom_Domain_Name__c,Cutomer_Cancel_Date__c,Date_of_Transfer__c,Date__c,DAT__c,Days_Between_OLI_Created_and_Go_Live__c,Days_B_W__c,Days_For_Task_Notification__c,Days_From_Creation_to_Fulfillment__c,Days_from_Submit_to_Fulfillment__c,Days_from_Submit_to_Fulfilmment__c,Description__c,Destination_Phone_Number__c,Digital_Product_Requirement__c,Directory_Code__c,Directory_Edition_Book_Status__c,Directory_Edition_Name__c,Directory_Edition_Renewal_Date__c,Directory_Edition__c,Directory_Heading__c,Directory_Name__c,Directory_Section__c,Directory__c,Discount_Amount__c,Discount__c,Display_Ad__c,Distribution_Area__c,DM_isTriggerExecuted__c,Domain_Email_Addresses__c,Edition_Code__c,Effective_Date__c,Enhance_Scoped_Caption_Header_Product__c,error_description__c,Evergreen_Payments__c,Expiration_Date__c,Facebook_Admin__c,Facebook_URL__c,From_C__c,From__c,Fulfilled_on__c,FulfillmentDate__c,Fulfillment_Status__c,Geo_Code__c,Geo_Type__c,Has_Anchor__c,Id,Indent_Order__c,isCanceled__c,IsDeleted,Issue_SCN__c,Is_Anchor__c,Is_Billing_Cycle__c,Is_Caption__c,Is_Child__c,Is_Handled__c,Is_P4P__c,Is_Paid__c,Is_Trademark_Finding_Line__c,L2N__c,Landing_Page_URL__c,LastActivityDate,Last_Billing_Date__c,Last_Invoice__c,Last_successful_settlement__c,line_color_id__c,Line_Number__c,Line_Status__c,Linvio_Payment_Method_Id__c,Listing_Name__c,Listing__c,ListPrice__c,Locality__c,Media_Type__c,Migration_Directory_Heading1__c,Migration_Directory_Heading2__c,Migration_Directory_Heading3__c,Migration_Directory_Heading4__c,Mobile_URL__c,N2L__c,Name,national_order_Status__c,National_Staging_Line_Item__c,national_status__c,NAT_Client_ID__c,NAT__c,Next_Billing_Date__c,Number_of_Months_Transferred__c,OLI_Account_Dashboard_Pending__c,OLI_BCC_Applied__c,OLI_BCC_Duration__c,OLI_BCC_Name__c,OLI_BCC_Redeemed__c,OLI_Berry_Cares_Certificate_ID__c,OLI_DFFCaptionMember__c,OLI_DMF_Audit_BOTS_Text__c,OLI_DMF_Audit_NI_Text__c,OLI_Exception_Discount__c,OLI_Seniority_Date_Combo__c,OL_Print_Specialty_Product_Type__c,Opportunity_Close_Date__c,Opportunity_line_Item_id__c,Opportunity__c,Order_Anniversary_Start_Date__c,Order_Billing_Date_Changed__c,Order_Group__c,Order_Line_Item_Locality__c,Order_Line_Total__c,Order__c,OriginalBillingDate__c,Original_Order_Line_Item__c,P4P_Billing__c,P4P_Current_Billing_Clicks_Leads__c,P4P_Current_Billing_Period_Clicks_Leads__c,P4P_Current_Months_Clicks_Leads__c,P4P_Price_Per_Click_Lead__c,P4P_RCF_Order_Line_Item__c,P4P_Tracking_Number_ID__c,P4P_Tracking_Number_Status__c,Package_ID_External__c,Package_ID__c,Package_Item_Quantity__c,Package_Name__c,Page_Number__c,Parent_ID_of_Addon__c,Parent_ID__c,Parent_Line_Item__c,Payments_Remaining__c,Payment_Duration__c,Payment_Method__c,Pending_Disconnect__c,Previous_Directory_Edition__c,Previous_End_Date__c,Previous_Start_Date__c,Pricing_Program__c,Print_First_Bill_Date__c,Print_Product_Type__c,Product2__c,ProductCode__c,ProductType__c,Product_Code__c,Product_Inventory_Tracking_Group__c,Product_Is_IBUN_Bundle_Product__c,Product_Type__c,Prorate_Credit_Days__c,Prorate_Credit__c,Prorate_Stored_Value__c,Proration_Billing_Period__c,Proxy_URL__c,Publication_Code__c,Publication_Company__c,Publication_Date__c,Quantity__c,Quote_Signed_Date__c,Quote_signing_method__c,RCF_Tracked__c,ReadyForProcess__c,Reason_for_Transfer__c,RecordTypeId,Requires_Scoped_Suppression_Processing__c,Scoped_Caption_Header__c,Scoped_Suppression_Processing_Complete__c,Scope__c,Section_Page_Type__c,Selected_Cancel_Date__c,Selected_Cancel__c,Seniority_Date__c,Sent_to_fulfillment_on__c,Service_End_Date__c,Service_Start_Date__c,Sort_As_Check_Test__c,Sort_As_FORMULA__c,Sort_As__c,SPINS__c,Spotzer_Bundle__c,Statement_Suppression__c,Status__c,strOLICombo__c,strOppliCombo__c,strSeniorityDateCombo__c,strSuppressionCheck__c,Subscription_ID__c,Successful_Payments__c,SystemModstamp,Talus_Cancel_Date__c,Talus_DFF_Id__c,Talus_Fulfillment_Date__c,Talus_Go_Live_Date__c,Talus_OrderLineItem__c,Talus_Subscription_ID__c,Tear_Page_PDF_Path__c,Telco_Invoice_Date__c,Telco__c,Total_Billing_Amount_Transfered__c,Total_Number_Free__c,Total_Prorated_Days_for_contract__c,Total_Prorate__c,To_P__c,To__c,Trademark_Display_Text__c,Trademark_Finding_Line_OLI__c,Trademark_OLI_Grouping__c,Trademark_Product__c,Transaction_ID__c,Transferred_from__c,Transferred_To__c,Transfer_From_Account__c,TransIDLineNo__c,Trans_Version__c,Trans__c,Type__c,UDAC_Group__c,UDAC__c,UnitPrice__c,User_who_initiated_the_transfer__c,Vendor_Product_Key__c,Website_URL__c,WP_Logo_Header__c,WP_Logo_Product__c,Zero_Cancel_Fee_Approved__c,zero_Cancel_Fee_Requested__c from Order_Line_Items__r) 
          from National_Staging_Line_Item__c where Id in: setNSLI]){
              if(nsliiter.Opportunity_Product__r.size()>0){
                  for(OpportunityLineItem oppliiterator : nsliiter.Opportunity_Product__r){
                     OpportunityLineItem Clonedoppli=oppliiterator.clone(false,true);
                     if(MapNewDE.containskey(Clonedoppli.Directory__c)) Clonedoppli.Directory_Edition__c=MapNewDE.get(Clonedoppli.Directory__c).Id;
                     if(!mapNSLIOppLi.containskey(nsliiter.id)){
                        map<Id,OpportunityLineItem> tempMapOppLi=new map<Id,OpportunityLineItem>();
                        tempMapOppLi.put(oppliiterator.Id,Clonedoppli);
                        mapNSLIOppLi.put(nsliiter.Id,tempMapOppLi);
                     }
                     else{
                        mapNSLIOppLi.get(nsliiter.Id).put(oppliiterator.Id,Clonedoppli);
                     }
                  }
              }
              if(nsliiter.Order_Line_Items__r.size()>0){
                  for(Order_Line_Items__c oliiterator: nsliiter.Order_Line_Items__r){
                    setOLIIds.add(oliiterator.Id);
                    setDFFIds.add(oliiterator.Digital_Product_Requirement__c);
                    mapOriginalOLIIdDFFId.put(oliiterator.Id,oliiterator.Digital_Product_Requirement__c);
                    Order_Line_Items__c clonedOLI=oliiterator.clone(false,true);
                    if(MapNewDE.containskey(clonedOLI.Directory__c)) clonedOLI.Directory_Edition__c=MapNewDE.get(clonedOLI.Directory__c).Id;
                    if(!mapNSLIOLi.containskey(nsliiter.id)){
                        map<Id,Order_Line_Items__c> tempMapOLi=new map<Id,Order_Line_Items__c>();
                        tempMapOLi.put(oliiterator.Id,clonedOLI);
                        mapNSLIOLi.put(nsliiter.Id,tempMapOLi);
                     }
                     else{
                        mapNSLIOLi.get(nsliiter.Id).put(oliiterator.Id,clonedOLI);
                     }
                     
                    if(oliiterator.Product2__r.Is_Anchor__c){
                        if(String.isNotBlank(oliiterator.strOLICombo__c)){
                            MapAnchor.put(oliiterator.strOLICombo__c,oliiterator);
                        }
                    }
                    
                    mapOLIByNSLI.put(oliiterator.National_Staging_Line_Item__c, oliiterator);
                  }
              }
           }
      }
      for(Id nsosIterator : MapNSOS.keyset())
      {
          if(MapNSLI.containskey(nsosIterator))
          {
              for(Id nsliiterator : MapNSLI.get(nsosIterator).keyset())
              {
                 National_Staging_Line_Item__c NewSNLI = MapNSLI.get(nsosIterator).get(nsliiterator).clone(false,true);
                 NewSNLI.Is_Processed__c=true;
                 NewSNLI.Is_Changed__c=true;
                 NewSNLI.Seniority_Date__c =MapNSLI.get(nsosIterator).get(nsliiterator).Seniority_Date__c;
                 NewSNLI.National_Staging_Header__c=MapNSOS.get(nsosIterator).Id;
                 NewSNLI.Transaction_Line_Id__c=MapNSOS.get(nsosIterator).Transaction_ID__c+'_'+NewSNLI.Line_Number__c;
                 NewSNLI.Transaction_ID__c=MapNSOS.get(nsosIterator).Transaction_ID__c;
                 mapIDSNLI.put(nsliiterator,NewSNLI);
              }
          } 
      }
      
      if(mapIDSNLI.size()>0) insert mapIDSNLI.values();
      
      for(Id nsosIterator : MapNSOS.keyset())
      {
          if(MapNSLI.containskey(nsosIterator))
          {
              for(Id nsliiterator : MapNSLI.get(nsosIterator).keyset())
              {
                  if(mapIDSNLI.containskey(nsliiterator))
                  { 
                       if(mapIDSNLI.get(nsliiterator).Parent_ID__c==nsliiterator)
                       {
                           National_Staging_Line_Item__c TempSNLI=new National_Staging_Line_Item__c (id=mapIDSNLI.get(nsliiterator).Id);
                           TempSNLI.Parent_ID__c=mapIDSNLI.get(nsliiterator).Id;
                           NationalChilLineList.add(TempSNLI);
                       }
                       
                       if(mapNSLIOppLi.containskey(nsliiterator)){
                         for(Id oppliIterator : mapNSLIOppLi.get(nsliiterator).keyset()){
                              OpportunityLineItem clonedOppLi= mapNSLIOppLi.get(nsliiterator).get(oppliIterator);
                              clonedOppLi.National_Staging_Line_Item__c=mapIDSNLI.get(nsliiterator).Id;
                              if(MapNSOSParentOppClonedOpp.get(nsosIterator).containskey(clonedOppLi.OpportunityId)){
                              clonedOppLi.OpportunityId=MapNSOSParentOppClonedOpp.get(nsosIterator).get(clonedOppLi.OpportunityId).Id;
                              }
                              ListClonedOppLI.add(clonedOppLi);
                         }
                       }
                       
                       if(mapNSLIOLi.containskey(nsliiterator)){
                         for(Id oliIterator : mapNSLIOLi.get(nsliiterator).keyset()){
                              Order_Line_Items__c  clonedOLi= mapNSLIOLi.get(nsliiterator).get(oliIterator);
                              clonedOLi.National_Staging_Line_Item__c=mapIDSNLI.get(nsliiterator).Id;
                              clonedOLi.Scoped_Suppression_Processing_Complete__c=false;
                              if(MapNSOSParentOrderClonedOrder.get(nsosIterator).containskey(clonedOLi.Order__c)){
                                    clonedOLi.Order__c=MapNSOSParentOrderClonedOrder.get(nsosIterator).get(clonedOLi.Order__c).Id;
                              }
                              if(MapNSOSParentOppClonedOpp.get(nsosIterator).containskey(clonedOLi.Opportunity__c)){
                                    clonedOLi.Opportunity__c=MapNSOSParentOppClonedOpp.get(nsosIterator).get(clonedOLi.Opportunity__c).Id;
                              }
                              if(MapNSOSParentOrderSetClonedOrderSet.get(nsosIterator).containskey(clonedOLi.Order_Group__c)){
                                    clonedOLi.Order_Group__c=MapNSOSParentOrderSetClonedOrderSet.get(nsosIterator).get(clonedOLi.Order_Group__c).Id;
                              }
                              mapOriginalOLIClonedOLI.put(oliIterator,clonedOLi);
                              ListClonedOLI.add(clonedOLi);
                         }
                      }
                  }
              }
           }
      }
      if(NationalChilLineList.size()>0) update NationalChilLineList;
      if(ListClonedOppLI.size()>0) insert ListClonedOppLI;
      
      if(ListClonedOLI.size()>0) insert ListClonedOLI;
       
      if(setOLIIds.size()>0){
            List<Digital_Product_Requirement__c> listClonedDFF=new List<Digital_Product_Requirement__c>();
            String strQuery = CommonMethods.getCreatableFieldsSOQL('Digital_Product_Requirement__c');
            strQuery+=' where OrderLineItemID__c in :setOLIIds';      
            List<Digital_Product_Requirement__c> DFFList = Database.query(strQuery); 
                 
            for(Digital_Product_Requirement__c DFFIterator : DFFList){
                 Digital_Product_Requirement__c clonedDFF=DFFIterator.clone(false,true);
                 if(MapNewDE.containskey(clonedDFF.DFF_Directory__c)){
                     clonedDFF.Directory_Edition__c=MapNewDE.get(clonedDFF.DFF_Directory__c).Id;
                     clonedDFF.Issue_Number__c=MapNewDE.get(clonedDFF.DFF_Directory__c).Edition_Code__c;
                 }
                 clonedDFF.Proof__c='NO';
                 if(mapOriginalOLIClonedOLI.containskey(DFFIterator.OrderLineItemID__c)){
                 clonedDFF.OrderLineItemID__c =mapOriginalOLIClonedOLI.get(DFFIterator.OrderLineItemID__c).Id;
                 clonedDFF.OpportunityId__c =mapOriginalOLIClonedOLI.get(DFFIterator.OrderLineItemID__c).Opportunity__c;
                 }
                 mapOldDFFIdClonedDFF.put(DFFIterator.Id,clonedDFF);
                 listClonedDFF.add(clonedDFF);
            }
            
            if(listClonedDFF.size()>0) insert listClonedDFF;
      }
      
      
      if(ListClonedOLI.size()>0){
             List<Order_Line_Items__c> OLIUpdateList=new List<Order_Line_Items__c>();
             //For OLI Update Trademark_Finding_Line_OLI__c
             for(Order_Line_Items__c oliIterator: ListClonedOLI){
                 if(oliIterator.Trademark_Finding_Line_OLI__c!=null){
                     if(mapOriginalOLIClonedOLI.containskey(oliIterator.Trademark_Finding_Line_OLI__c))
                     oliIterator.Trademark_Finding_Line_OLI__c=mapOriginalOLIClonedOLI.get(oliIterator.Trademark_Finding_Line_OLI__c).Id;
                 }
                 if(oliIterator.Anchor_Listing_Caption_Header__c!=null && mapOriginalOLIClonedOLI.containskey(oliIterator.Anchor_Listing_Caption_Header__c)){
                     oliIterator.Anchor_Listing_Caption_Header__c=mapOriginalOLIClonedOLI.get(oliIterator.Anchor_Listing_Caption_Header__c).Id;
                 }
                 if(mapOldDFFIdClonedDFF.containskey(oliIterator.Digital_Product_Requirement__c))
                     oliIterator.Digital_Product_Requirement__c=mapOldDFFIdClonedDFF.get(oliIterator.Digital_Product_Requirement__c).Id;
                     
                 OLIUpdateList.add(oliIterator);
            }
            
            if(OLIUpdateList.size()>0) update OLIUpdateList;
      }
      /*
      if(NationalOrderSetList!=null) {
      System.debug('######NationalOrderSetList####'+NationalOrderSetList);
        for(National_Staging_Order_Set__c nsos : NationalOrderSetList)
        { 
            HeaderIds.add(nsos.id);
        }
        
       // if(HeaderIds.size()>0) NationalOpportunityCreationHandler_v3.NationalOrderProcessByManually(HeaderIds);
      }
      */
   }
}