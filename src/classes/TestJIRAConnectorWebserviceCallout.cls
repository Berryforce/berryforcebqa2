@isTest
private class TestJIRAConnectorWebserviceCallout {
     @isTest static void testCallout() {
        // Set mock callout class 
        Case obj_case=new case();
        obj_case.Status='New';
        insert obj_case;
        CaseComment obje=new CaseComment ();
        obje.CommentBody='testtest';
        obje.ParentId=obj_case.id;
        insert obje;
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        String jiraURL='https://cscberryteam.atlassian.net';
        String systemID='1';
        String objectType='CASE';
        String projectKey='ProjectKey1';
        String IssueType='Issue';
        String objectId=String.ValueOf(obj_case.id);
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
         
          JIRAConnectorWebserviceCallout.createIssue( jiraURL,systemId,objectType,objectId,projectKey, issueType); 
        
        // Verify response received contains fake values
      
    }
}