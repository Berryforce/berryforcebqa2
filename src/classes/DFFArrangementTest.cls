@isTest(SeeAllData=true)
public class DFFArrangementTest
{
    public static testMethod void DFFArrangementTest () 
    {   
        Test.startTest();

        PageReference pageRef = Page.DFFArrangement;
        Test.setCurrentPage(pageRef);
        Digital_Product_Requirement__c dpr= TestMethodsUtility.generateDataFulfillmentForm();
        insert dpr;
        ApexPages.StandardController dffctrl = new ApexPages.standardController(dpr);
        Apexpages.currentPage().getParameters().put('id',dpr.id);
        DFFArrangement dff=new DFFArrangement(dffctrl);
        dff.uplisting_order();
        dff.downlisting_order();
        dff.activeedit(); 
        dff.increaseindentlevel();
        dff.decreaseindentlevel();
        dff.cancelbtn();
        dff.savebtn();
        dff.GenerateNewListing();
        dff.PopulateDLCaptionHeaders();
        dff.getSelected();
        dff.ReplaceNewDLHeader();

        Test.stopTest();  
    }

}