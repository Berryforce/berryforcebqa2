global class OrderLineSuppresionUpdt implements Database.Batchable<sObject>{

    
    /*global OrderLineItemTriggerForDataMigrBatch(){
    }
*/    
    global OrderLineSuppresionUpdt(){
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
         String query = 'Select id,Product2__r.Print_Product_Type__c, Listing__c, Is_Child__c, isCanceled__c From Order_Line_Items__c where (Product2__r.Print_Product_Type__c = \'Listing\' or Product2__r.Print_Product_Type__c = \'In-Column\') and isCanceled__c = FALSE  and Is_Child__c = FALSE and Listing__c != null and Requires_Scoped_Suppression_Processing__c = true order by Listing__c';
        return Database.getQueryLocator(query);
    }  
    
    global void execute(Database.BatchableContext bc, List<Order_Line_Items__c> listOLI) {
        Savepoint sp = Database.setSavepoint();
        List<Order_Line_Items__c> listUpdtOLI = new List<Order_Line_Items__c>();
        Map<string,List<Id>> mapListing = new Map<string,List<Id>>();
        try{
            
            for(Order_Line_Items__c oli:listOLI){
                
                if(!mapListing.containsKey(oli.Listing__c)) {
                    //setListingId.add(oli.Listing__c);
                    mapListing.put(oli.Listing__c, new List<id>());
                }
                mapListing.get(oli.Listing__c).add(oli.id);
            }
            AggregateResult[] ARs = [select count(id) countLst, listing__c from Directory_listing__c where listing__c in : mapListing.keyset() group by listing__c];
            for(AggregateResult ar : ARs) {
                if((Integer) ar.get('countLst') > 0) {
                    for(id idOLI : mapListing.get((String) ar.get('listing__c'))) {
                        listUpdtOLI.add(new Order_Line_Items__c(id = idOLI, Requires_Scoped_Suppression_Processing__c = true));
                    }
                }
            }

            update listUpdtOLI;
        }catch(Exception e){
            System.debug('The exception is'+e);
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch update of migrated Opportunity');
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}