global class PaymentMethodUpdate Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id, Payment_Method__c FROM Opportunity';
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
    	set<Id> setOpptyIds = new set<Id>();
		list<Order_Line_Items__c> listOLI = new list<Order_Line_Items__c>(); 
		list<Modification_Order_Line_Item__c> listMOLI = new list<Modification_Order_Line_Item__c>(); 
		Map<Id, Opportunity> mapOppty = new Map<Id, Opportunity>();
		
		for(Opportunity oppty : scope) {
			setOpptyIds.add(oppty.Id);
			mapOppty.put(oppty.Id, oppty);
		}
		
		listOLI = [SELECT Opportunity__c, Id, Payment_Method__c FROM Order_Line_Items__c WHERE Opportunity__c IN: setOpptyIds];
		listMOLI = [SELECT Opportunity__c, Id, MOLI_Payment_Method__c FROM Modification_Order_Line_Item__c WHERE Opportunity__c IN: setOpptyIds];
		
		if(listOLI.size() > 0) {
			for(Order_Line_Items__c OLI: listOLI) {
				OLI.Payment_Method__c = mapOppty.get(OLI.Opportunity__c).Payment_Method__c;
			}
			update listOLI;
		}
		
		if(listMOLI.size() > 0) {
			for(Modification_Order_Line_Item__c MOLI: listMOLI) {
				MOLI.MOLI_Payment_Method__c = mapOppty.get(MOLI.Opportunity__c).Payment_Method__c;
			}
			update listMOLI;
		}
    }

    global void finish(Database.BatchableContext bc) {
    }
}