public class MassUpdateLeadListings2{

    private String Id;
    public Lead lead {get; set;}
    public Listing__c listing {get;set;}
    public set<Id> lstngIds{get;set;}
    public List<Listing__c> SelectedForUpdate{get;set;}
    public List<wrapData> listingActLst{get;set;}
    
    public MassUpdateLeadListings2(ApexPages.StandardController controller){
      Id = System.currentPageReference().getParameters().get('id');
      lead = ( Lead ) controller.getRecord();
      listing = new Listing__c();
      listingActLst = new List<wrapdata>();
      if(listingActLst!= null){
        listingsData();
      }
    }
    public List<wrapData> listingsData(){
        List<Listing__c> objListingLst = new List<Listing__c>();    
        objListingLst = [Select Id,Name,Lst_Last_Name_Business_Name__c,Phone__c,Listing_State__c,Listing_City__c,Service_Order__c,Requires_LCN_Telco_Notification__c,Listing_Street_Number__c ,Listing_PO_Box__c, Listing_Country__c , Listing_Postal_Code__c ,Listing_Street__c, Changes_Authorized_By__c, Customer_Advised_Charges_May_Apply__c  FROM Listing__c WHERE Lead__c = :Id];
        for(Listing__c objLst :objListingLst ){
            WrapData objWD = new WrapData();
            objWD.objL = objLst;
            objWD.ischecked = false;
            listingActLst.add(objWD);
        }   
        return listingActLst;
    }
    public pagereference SaveSelected(){
       Integer Count = 0;
       SelectedForUpdate = new list<Listing__c>();
        for (WrapData cw : listingActLst){
            if (cw.ischecked){
                count++;
                System.debug('*********'+cw.objL );
                if(listing.Lst_Last_Name_Business_Name__c != null && listing.Lst_Last_Name_Business_Name__c != ''){
                cw.objL.Name = listing.Lst_Last_Name_Business_Name__c;
                }
                system.debug('*******Listing DataPHONE******'+ listing.Phone__c);
                if(listing.Phone__c != null && listing.Phone__c != ''){
                cw.objL.Phone__c = listing.Phone__c;
                }
                if(listing.Listing_Street_Number__c != null && listing.Listing_Street_Number__c != ''){
                cw.objL.Listing_Street_Number__c = listing.Listing_Street_Number__c;
                }
                else{
                cw.objL.Listing_Street_Number__c = '';
                }
                if(listing.Listing_Street__c != null && listing.Listing_Street__c != ''){
                cw.objL.Listing_Street__c = listing.Listing_Street__c;
                }
                else{
                cw.objL.Listing_Street__c = '';
                }
                if(listing.Listing_PO_Box__c != null && listing.Listing_PO_Box__c != ''){
                cw.objL.Listing_PO_Box__c = listing.Listing_PO_Box__c;
                }
                else{
                cw.objL.Listing_PO_Box__c = '';
                }
                if(listing.Listing_City__c != null && listing.Listing_City__c != ''){
                cw.objL.Listing_City__c = listing.Listing_City__c;
                }
                else{
                cw.objL.Listing_City__c = '';
                }
                if(listing.Listing_State__c != null && listing.Listing_State__c != ''){
                cw.objL.Listing_State__c = listing.Listing_State__c;
                }
                else{
                cw.objL.Listing_State__c = '';
                }
                if(listing.Listing_Postal_Code__c != null &&listing.Listing_Postal_Code__c != ''){
                cw.objL.Listing_Postal_Code__c = listing.Listing_Postal_Code__c;
                }
                else{
                cw.objL.Listing_Postal_Code__c = '';
                }
                if(listing.Service_Order__c != null &&listing.Service_Order__c != ''){
                cw.objL.Service_Order__c = listing.Service_Order__c;
                }
                else{
                cw.objL.Service_Order__c = '';
                }
                if(listing.Customer_Advised_Charges_May_Apply__c != false &&listing.Customer_Advised_Charges_May_Apply__c != false){
                cw.objL.Customer_Advised_Charges_May_Apply__c = listing.Customer_Advised_Charges_May_Apply__c;
                }
                else{
                cw.objL.Customer_Advised_Charges_May_Apply__c = false;
                }
                if(listing.Changes_Authorized_By__c != null &&listing.Changes_Authorized_By__c != ''){
                cw.objL.Changes_Authorized_By__c = listing.Changes_Authorized_By__c;
                }
                else{
                cw.objL.Changes_Authorized_By__c = '';
                }
                if(listing.Requires_LCN_Telco_Notification__c == false){
                   cw.objL.Requires_LCN_Telco_Notification__c = true;
                }
                
                system.debug('********Wrap Listing Data Phone******'+ cw.objL.Phone__c );
                SelectedForUpdate.add(cw.objL);
                system.debug('*******sELECTED RECORDS *********'+SelectedForUpdate);
            }
           
        }
        if(count == 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select atleast one Record!!!!!');
            apexPages.addMessage(myMsg);
            return null;
       }
       if(SelectedForUpdate.size()>0){
       	    CommonVariables.LeadListingAdrRecursive = true;
       		update SelectedForUpdate;
       }
       return(new pagereference('/apex/MassUpdateLeadListings2?id='+Id).setredirect(true)) ;
    }
    public PageReference redirect(){
      PageReference leadpage= new ApexPages.StandardController(lead).view();
      leadpage.setRedirect(true); 
      return leadpage;
    }
    public class WrapData{
        public boolean ischecked{get;set;}
        public Listing__c objL{get;set;}
    }
    
}