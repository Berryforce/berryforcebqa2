@isTest
private class AddSubscriptionProductControllerTest {
	
    static testmethod void addSubPrdtTest() {

        Map <String, String> mapPrdt = new Map<String, String>{'LocalVox' => Label.LocalVox_RT_Id, 'Yodle SEM' => Label.Yodle_SEM_RT_Id, 'Marketing Messenger' => Label.Marketing_Messenger_RT_Id, 'iYP Bronze' => Label.YPC_Record_Type_Id};
        List<Product2> lstPrdts = new List<Product2>();
        List<Order_Line_Items__c> lstOLns = new List<Order_Line_Items__c>();
        List<Digital_Product_Requirement__c> lstDffs = new List<Digital_Product_Requirement__c>();

        Test.startTest();
        
        Package_ID__c pkg = CommonUtility.createPkgTest();
        insert pkg;

        Package_Product__c pkPrdt = CommonUtility.createPkgPrdtTest(pkg);
        insert pkPrdt;

        instPrpts(pkPrdt.Id);

        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'Facebook';
        insert prdt;

        Canvass__c c = TestMethodsUtility.createCanvass();

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;

        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c ordGrp = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp;

        Order_Line_Items__c OLI = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
        OLI.Product2__c = prdt.Id;
        OLI.Effective_Date__c = system.Today();
        OLI.Parent_ID__c = TestMethodsUtility.generateRandomString();
        insert OLI;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.UDAC__c = 'FB';
        dff.Talus_Subscription_Id__c = '2525';
        dff.Talus_DFF_Id__c = '';
        dff.recordTypeId = Label.DFF_Facebook_RT_Id;
        dff.OrderLineItemID__c = OLI.Id;
        dff.Account__c = a.Id;
        dff.DFF_Product__c = prdt.Id;
        dff.Status__c = '';
        dff.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
        dff.Bundle__c = 'Spotzer Add-on';
        insert dff;

        callPgRef(dff);

        Order_Group__c ordGrp1 = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp1;

        Order_Line_Items__c OLI1 = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp1);
        OLI1.Product2__c = prdt.Id;
        OLI1.Effective_Date__c = system.Today();
        OLI1.Parent_ID__c = TestMethodsUtility.generateRandomString();
        insert OLI1;

        Modification_Order_Line_Item__c mOLI = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, ordGrp1);
        mOLI.Order_Line_Item__c = OLI1.Id;
        insert mOLI;

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.UDAC__c = 'FB';
        dff1.Talus_Subscription_Id__c = '2525';
        dff1.Talus_DFF_Id__c = '';
        dff1.recordTypeId = Label.DFF_Facebook_RT_Id;
        dff1.Account__c = a.Id;
        dff.ModificationOrderLineItem__c = mOLI.Id;
        dff1.DFF_Product__c = prdt.Id;
        dff1.Status__c = '';
        dff1.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
        dff1.Bundle__c = 'Spotzer Add-on';
        insert dff1;

        callPgRef(dff1);

        Test.stopTest();

    }

    public static void callPgRef(Digital_Product_Requirement__c dff){

        ApexPages.StandardController sc = new ApexPages.StandardController(dff);
        AddSubscriptionProductController addSubPrdt = new AddSubscriptionProductController(sc);
        addSubPrdt.sbrprdt();
        addSubPrdt.rtnDff();

    }

    public static void instPrpts(Id pkPrdt) {

        List < Product_Properties__c > prptsLst = new List < Product_Properties__c > ();
        Product_Properties__c prdtPrpt = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'business_name', Type__c = 'string');
        prptsLst.add(prdtPrpt);
        Product_Properties__c prdtPrpt1 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'affiliations', Type__c = 'stringarray');
        prptsLst.add(prdtPrpt1);
        Product_Properties__c prdtPrpt2 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'hours', Type__c = 'hoursofoperation');
        prptsLst.add(prdtPrpt2);
        Product_Properties__c prdtPrpt3 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'years_in_business', Type__c = 'int');
        prptsLst.add(prdtPrpt3);
        Product_Properties__c prdtPrpt4 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'seniority_date', Type__c = 'date');
        prptsLst.add(prdtPrpt4);
        Product_Properties__c prdtPrpt5 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'graphic_pao', Type__c = 'jsonobj');
        prptsLst.add(prdtPrpt5);
        Product_Properties__c prdtPrpt6 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'coupons', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt6);
        Product_Properties__c prdtPrpt7 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'business_email', Type__c = 'email');
        prptsLst.add(prdtPrpt7);
        Product_Properties__c prdtPrpt8 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'show_location_on_lp', Type__c = 'bool');
        prptsLst.add(prdtPrpt8);
        Product_Properties__c prdtPrpt9 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'cost', Type__c = 'decimal');
        prptsLst.add(prdtPrpt9);
        Product_Properties__c prdtPrpt10 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'graphic_banner', Type__c = 'jsonobj');
        prptsLst.add(prdtPrpt10);
        Product_Properties__c prdtPrpt11 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'graphic_logo', Type__c = 'jsonobj');
        prptsLst.add(prdtPrpt11);
        Product_Properties__c prdtPrpt12 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'additional_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt12);
        Product_Properties__c prdtPrpt13 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'additional_urls', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt13);
        Product_Properties__c prdtPrpt14 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'after_hours_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt14);
        Product_Properties__c prdtPrpt15 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'emergency_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt15);
        Product_Properties__c prdtPrpt16 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'fax_numbers', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt16);
        Product_Properties__c prdtPrpt17 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'mobile_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt17);
        Product_Properties__c prdtPrpt18 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'pager_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt18);
        Product_Properties__c prdtPrpt19 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'toll_free_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt19);
        Product_Properties__c prdtPrpt20 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'vanity_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt20);
        Product_Properties__c prdtPrpt21 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'additional_emails', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt21);

        insert prptsLst;

    }

}