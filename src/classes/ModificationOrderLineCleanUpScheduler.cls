global class ModificationOrderLineCleanUpScheduler implements Schedulable {
   public Interface ModificationOrderLineCleanUpSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('ModificationOrderLCleanUpSchedulerHndlr');
        if(targetType != null) {
            ModificationOrderLineCleanUpSchedulerInterface obj = (ModificationOrderLineCleanUpSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}