/**
 * How to run it:
 * Id ruleId = 'a0I240000022Ium'; //Integreation Rule Id
 * Integer scopeSize = 1; //number of records created on each chunck
 * Integer postScopeSize = 1; //number of records posted on each chunck
 * Boolean postAllInvoices = false; //do I want to post all SINV or just those previously created
 * A_CreateSINVBatch createSINV = new A_CreateSINVBatch(ruleId, postScopeSize, postAllInvoices);
 * Database.executeBatch(createSINV, scopeSize);
 */

public class A_CreateSINVBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private Id integrationRuleId;
    
    public Integer postScopeSize;
    public Boolean postAllInvoices;
        
    public List<Id> targetObjectIds;
    public List<Id> targetObjectIdsForEmail;
    public List<String> errorMessages;
    public List<String> successfulMessages;
    
    public A_CreateSINVBatch(Id ruleId, Integer postSSize, Boolean postAll)
    {
        postScopeSize = postSSize;
        postAllInvoices = postAll;

        integrationRuleId = ruleId;

        errorMessages = new List<String>();
        successfulMessages = new List<String>();
        
        targetObjectIds = new List<Id>();
        targetObjectIdsForEmail = new List<Id>();
    }

    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        Boolean processed = true;
        
        //String qry = 'Select Id From Open_AR__c Where Invoice_Processed__c != :processed';
        String qry = 'Select Id From Open_AR__c Where Invoice_Processed__c != :processed limit 100000';

        return Database.getQueryLocator(qry);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.Savepoint sp = Database.setSavepoint();
        
        targetObjectIdsForEmail = new List<Id>();
        List<Id> sourceIds = new List<Id>();
        List<c2g.CODAAPICommon.Reference> refs = new List<c2g.CODAAPICommon.Reference>();
        
        if(scope != null)
            successfulMessages.add('Number of Open AR records to process = ' + scope.size());
        else
            successfulMessages.add('No Open AR records to process.');

        try
        {
            for(SObject so : scope)
            {
                sourceIds.add(so.Id);
            }

            targetObjectIds.addAll(ffirule.RuleService.run(sourceIds, integrationRuleId));
            targetObjectIdsForEmail.addAll(targetObjectIds);
            
             if(targetObjectIds != null)
                successfulMessages.add(targetObjectIdsForEmail.size() + ' SINV has been created.');
            else
                successfulMessages.add('No sales invoices created');
        }
        catch (Exception e)
        {
            Database.rollback(sp);

            errorMessages.add( 'Error during Bulk Create and Post Sales Invoices in chain ' + scope + '; Exception: ' + e.getMessage() + '\n\n');
        }
    }

    public void finish(Database.BatchableContext BC)
    {
        if(!targetObjectIds.isEmpty() && targetObjectIds.size() > 0)
        {
            A_PostSINVBatch postSINV;
            
            if(postAllInvoices)
            {
               postSINV = new A_PostSINVBatch(errorMessages,successfulMessages);
            }
            else
            {            
               postSINV = new A_PostSINVBatch(errorMessages, successfulMessages, targetObjectIds);
            }                
            
            Database.executeBatch(postSINV, postScopeSize);
        }
    }
}