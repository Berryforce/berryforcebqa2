global class TestBatch implements Database.Batchable<sObject>{
    
    
    global TestBatch(){
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        //Set<Id> setUserId = new set<Id>{'005K0000001yKP3IAM'};
        
        //String SOQL = 'select id, DP_Caption_Header__c, DP_Caption_Member__c, Caption_Header__c, Caption_Member__c from Directory_Pagination__c where (Caption_Header__c = false and DP_Caption_Header__c = true) or (Caption_Member__c = false and DP_Caption_Member__c = true)';
        String SOQL;
        
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Directory_Pagination__c> lstDP){
        /*List<Directory_Pagination__c> lstDPUpdt = new List<Directory_Pagination__c>();
        for(Directory_Pagination__c objDP : lstDP) {
            objDP.DP_Caption_Member__c = objDP.Caption_Member__c;
            objDP.DP_Caption_Header__c= objDP.Caption_Header__c;
            lstDPUpdt.add(objDP);
        }
        Update lstDPUpdt;*/
    }
    
    global void finish(Database.BatchableContext bc){
        String strErrorMessage;
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email, 'msureshkuma2@csc.com'};
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Test Batch Process Completed' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems +
          ' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage);
    }
}