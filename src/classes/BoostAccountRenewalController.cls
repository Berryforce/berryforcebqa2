public class BoostAccountRenewalController{
    public static void BoostAccountRenewal(List<Order_Line_Items__c> lstOLI) {
        map<Id, list<Order_Line_Items__c>> mapTempOLI = new map<Id, list<Order_Line_Items__c>>();
        set<Id> setRemovedAccIds = new set<Id>();
        set<Id> setAccIds = new set<Id>();
        set<Id> setownerId = new set<Id>();
        List<Order_Line_Items__c> objOliLst = new List<Order_Line_Items__c>();
        map<Id, map<ID, list<Order_Line_Items__c>>> mapAccDEOLIBoostRenewal = new map<Id, map<ID, list<Order_Line_Items__c>>>();
        for(Order_Line_Items__c iterator : lstOLI){
            if(iterator.Account__r.Delinquency_Indicator__c == false && iterator.Product2__r.Print_Product_Type__c != 'Specialty' && iterator.Is_P4P__c == false && iterator.Account__r.Open_or_Closed_Claim_Past_18_Months__c == false){
                if(!setRemovedAccIds.contains(iterator.Account__c)) {
                    if(!mapTempOLI.containsKey(iterator.Account__c)) {
                        mapTempOLI.put(iterator.Account__c, new list<Order_Line_Items__c>());
                    }
                    mapTempOLI.get(iterator.Account__c).add(iterator);
                }
            }
            else{
                setRemovedAccIds.add(iterator.Account__c);
                if(mapTempOLI.containsKey(iterator.Account__c)) {
                    mapTempOLI.remove(iterator.Account__c);                    
                }
            }            
        }
        List<Order_Line_Items__c> lstOLIBoostRenewal = new List<Order_Line_Items__c>();
        set<Id> setOliId = new set<Id>();
        if(mapTempOLI.size() > 0) {
            for(Id accId :  mapTempOLI.keySet()) {
                if(mapTempOLI.get(accId) != null) {
                    for(Order_Line_Items__c iterator :  mapTempOLI.get(accId)) {
                        //lstOLIBoostRenewal.add(iterator);
                        setOliId.add(iterator.Id);
                    }
                }
            }
        }
        if(setOliId.size()>0){
            lstOLIBoostRenewal=OrderLineItemSOQLMethods.getOLIByID(setOliId);
        }
        if(lstOLIBoostRenewal.size()>0){
            for(Order_Line_Items__c iterator : lstOLIBoostRenewal) {
                setAccIds.add(iterator.Account__c);
                setownerId.add(iterator.Account__r.OwnerId);
                //setOliId.add(iterator.Id);
                if(!mapAccDEOLIBoostRenewal.containsKey(iterator.Account__c)) {
                    mapAccDEOLIBoostRenewal.put(iterator.Account__c, new map<Id, list<Order_Line_Items__c>>());
                }
                if(!mapAccDEOLIBoostRenewal.get(iterator.Account__c).containsKey(iterator.Directory_Edition__c)) {
                    mapAccDEOLIBoostRenewal.get(iterator.Account__c).put(iterator.Directory_Edition__c, new list<Order_Line_Items__c>());
                }
                mapAccDEOLIBoostRenewal.get(iterator.Account__c).get(iterator.Directory_Edition__c).add(iterator);
            }
        }
        map<Id, Account> mapAccount = new map<Id, Account>([Select Id,Name,OwnerId from Account where Id IN:setAccIds]);
        map<Id,Id> mapUserManagerId = new map<Id,Id>();
        List<User> userLst = [Select Id,managerId,Email from User where Id IN : setownerId];
        for(User objUser : userLst){
            mapUserManagerId.put(objUser.Id,objUser.managerId);
        }
        map<Id,list<string>> mapBoostRenewal = new map<Id,list<string>>();
        list<String> toAddresses = new list<String>();
        for(Id accId : mapAccDEOLIBoostRenewal.keyset()){
            Id actOwnerId ;
            if(mapAccount.get(accId) != null){
                actOwnerId=mapAccount.get(accId).OwnerId;
            }
            system.debug('**********Account Owner Id***********'+actOwnerId);
            if(mapAccDEOLIBoostRenewal.get(accId) != null){
                for(Id objDEId : mapAccDEOLIBoostRenewal.get(accId).keyset()){
                    String pdfString = '';
                    if(mapAccDEOLIBoostRenewal.get(accId).get(objDEId) != null){
                    //String pdfString = '';
                    pdfString += '<html><body>';
                    pdfString += generateAccountDEString(mapAccDEOLIBoostRenewal.get(accId).get(objDEId));
                        for(Order_Line_Items__c objOli : mapAccDEOLIBoostRenewal.get(accId).get(objDEId)){
                            pdfString += generateDirOlitemsString(objOli);
                        }
                    pdfString += generateBottomString();
                    }
                    if(actOwnerId != null){
                        if(!mapBoostRenewal.containsKey(actOwnerId)){
                            mapBoostRenewal.put(actOwnerId,new list<string>());
                        }
                        mapBoostRenewal.get(actOwnerId).add(pdfString);
                    }
                }
            }
        }
        if(mapBoostRenewal.size()>0){
            sendEmail(mapBoostRenewal,mapUserManagerId);
        }
        
        if(setRemovedAccIds.size()>0){
            updateBoostFallOutFlag(setRemovedAccIds);
        }
       
        if(lstOLIBoostRenewal.size()>0){
            createNewOLI(lstOLIBoostRenewal);
        }
    }
    
     public static void updateBoostFallOutFlag(set<Id> actIds){
        List<Account> objActFalloutLst = [Select Id,Name,Boost_Fallout_Flag__c from Account where Id IN : actIds];
        if(objActFalloutLst.size()>0){
            for(Account objActLRF : objActFalloutLst){
                if(objActLRF.Boost_Fallout_Flag__c == false){
                    objActLRF.Boost_Fallout_Flag__c = true;
                }
               
            } 
            if(objActFalloutLst.size()>0){
                update objActFalloutLst;
            }
        }
    }
    public static void createNewOLI(List<Order_Line_Items__c> lstOLI) {
        map<Id,list<Order_Line_Items__c>> mapOptyOliLst = new map<Id,list<Order_Line_Items__c>>();
        set<Id> setOrderSetId = new set<Id>();
        map<Id,Order_Group__c> mapnewobjOrderSet = new map<Id,Order_Group__c>();
        map<Id,opportunity> mapnewobjOpp = new map<Id,opportunity>();
        set<Id> setOpptyId = new set<Id>();
        map<string,Directory_Product_Mapping__c> mapDPM = new map<string,Directory_Product_Mapping__c>();
        set<Id> setProdId = new set<Id>();
        set<Id> setDirId = new set<Id>();
        
        for(Order_Line_Items__c Oli : lstOLI) {
            if(Oli.Cutomer_Cancel_Date__c == null){
                if(!mapOptyOliLst.containskey(Oli.Opportunity__c)){
                    mapOptyOliLst.put(Oli.Opportunity__c,new list<Order_Line_Items__c>());
                }
                mapOptyOliLst.get(Oli.Opportunity__c).add(Oli);
                setOpptyId.add(Oli.Opportunity__c);
                setOrderSetId.add(Oli.Order_Group__c);
                if(Oli.Product2__c != null) {
                    setProdId.add(Oli.Product2__c);
                }
                if(Oli.Directory__c != null) {
                    setDirId.add(Oli.Directory__c);
                }
            }
        }
       
        if(setOpptyId.size()>0){
            mapnewobjOpp = createNewOpty(setOpptyId);
        }
        if(mapnewobjOpp.size()>0 && CommonVariables.inFutureContext == false){
            CommonVariables.inFutureContext  = true;
            insert mapnewobjOpp.values();
        }
        system.debug('********Order Set Id******' + setOrderSetId.size());
        if(setOrderSetId.size()> 0 && mapnewobjOpp.size()> 0){
            mapnewobjOrderSet = createNewOrderSet(setOrderSetId,mapnewobjOpp);
        }
        if(mapnewobjOrderSet.size()>0){
            insert mapnewobjOrderSet.values();
        }
        List<Directory_Product_Mapping__c> objDirPM = DirectoryProductMappingSOQLMethods.getDirProdMapByProdIdAndDirId(setProdId,setDirId);
        
        if(objDirPM.size()> 0){
            for(Directory_Product_Mapping__c objDPM : objDirPM){
                string strDirProd = objDPM.Directory__c+''+objDPM.Product2__c ;
                if(!mapDPM.containskey(strDirProd)){
                    mapDPM.put(strDirProd,objDPM);
                }
            }
        }
        
        list<directory_edition__c> dirEditionLst = new list<directory_edition__c>();
        map<Id,directory_edition__c> mapOliDir = new map<Id,directory_edition__c>();
        
        if(setDirId.size()> 0){
            dirEditionLst = DirectoryEditionSOQLMethods.getBOTSDEByDirectory(setDirId);
        }
        if(dirEditionLst.size()>0){
            for(directory_edition__c objDirEdition : dirEditionLst){
                if(!mapOliDir.containskey(objDirEdition.Directory__c)){
                    mapOliDir.put(objDirEdition.Directory__c,objDirEdition);
                }
            }
        }
        map<Id,Order_Line_Items__c> mapnewOLI = new map<Id,Order_Line_Items__c>();
        map<Id,Id> mapOldNewOli = new map<Id,Id>(); 
        list<Order_Line_Items__c> lstOroliUpdate = new list<Order_Line_Items__c>();
        for(Order_Line_Items__c OliIterator : lstOLI){
            if(mapnewobjOpp.size()>0 && mapnewobjOrderSet.size()>0){
                //create a New Order Line Item
                //mapnewOLILst  = NewOrderLineItem(OliIterator,mapnewobjOpp,mapnewobjOrderSet,mapnewOLI);
                decimal modifiedUnitPrice;
                string strOliDirProd;
                if(OliIterator.Directory__c != null && OliIterator.Product2__c != null) {
                    strOliDirProd = OliIterator.Directory__c+''+OliIterator.Product2__c;
                }
                if(mapDPM.get(strOliDirProd) != null){
                    if(mapDPM.get(strOliDirProd).FullRate__c != null && OliIterator.UnitPrice__c != mapDPM.get(strOliDirProd).FullRate__c && OliIterator.Discount__c != null){
                        modifiedUnitPrice = (mapDPM.get(strOliDirProd).FullRate__c-((mapDPM.get(strOliDirProd).FullRate__c * OliIterator.Discount__c)/100)).setScale(2, RoundingMode.HALF_UP);
                    }
                }
                Order_Line_Items__c objNewOrdL = new Order_Line_Items__c();
                objNewOrdL.Order__c = OliIterator.Order__c;
                objNewOrdL.Order_Group__c = mapnewobjOrderSet.get(OliIterator.Order_Group__c).Id;
                if(mapnewobjOpp.get(OliIterator.Opportunity__c) != null){
                    objNewOrdL.Opportunity__c = mapnewobjOpp.get(OliIterator.Opportunity__c).Id;
                }
                objNewOrdL.Original_Order_Line_Item__c = OliIterator.Id;
                objNewOrdL.Product2__c = OliIterator.Product2__c;
                //objNewOrdL.Product_Code__c = OliIterator.ProductCode__c;
                objNewOrdL.FulfillmentDate__c = system.today();
                objNewOrdL.Canvass__c = OliIterator.Canvass__c;
                //objNewOrdL.Account_Manager__c = OliIterator.Account_Manager__c;
                objNewOrdL.Quote_signing_method__c = OliIterator.Quote_signing_method__c;
                objNewOrdL.Account__c = OliIterator.Account__c;
                objNewOrdL.Product_Type__c = OliIterator.Product_Type__c;
                //objNewOrdL.Parent_ID__c = OliIterator.Parent_ID__c;
                objNewOrdL.Package_ID__c = OliIterator.Package_ID__c;        
                objNewOrdL.Directory_Heading__c= OliIterator.Directory_Heading__c;
                objNewOrdL.Parent_Line_Item__c = OliIterator.Parent_Line_Item__c;
                objNewOrdL.Geo_Type__c  = OliIterator.Geo_Type__c ;
                objNewOrdL.Geo_Code__c = OliIterator.Geo_Code__c;
                objNewOrdL.Scope__c = OliIterator.Scope__c ;
                objNewOrdL.Directory_Section__c = OliIterator.Directory_Section__c;
                objNewOrdL.Status__c = OliIterator.Status__c;
                objNewOrdL.Directory_Heading__c = OliIterator.Directory_Heading__c;
                //objNewOrdL.Order_Line_Item__c = OliIterator.Id;
                objNewOrdL.Directory__c = OliIterator.Directory__c;
                if(mapOliDir.get(OliIterator.Directory__c) != null){
                    objNewOrdL.Directory_Edition__c = mapOliDir.get(OliIterator.Directory__c).Id;
                }
                objNewOrdL.Statement_Suppression__c = OliIterator.Statement_Suppression__c;
                objNewOrdL.Order_Anniversary_Start_Date__c = OliIterator.Order_Anniversary_Start_Date__c;
                objNewOrdL.Quantity__c = OliIterator.Quantity__c;
                objNewOrdL.ListPrice__c= OliIterator.ListPrice__c;
                objNewOrdL.Listing__c = OliIterator.Listing__c;
                objNewOrdL.Description__c = OliIterator.Description__c;            
                objNewOrdL.Discount__c = OliIterator.Discount__c;
                objNewOrdL.Billing_End_Date__c = OliIterator.Billing_End_Date__c;
                objNewOrdL.Billing_Start_Date__c = OliIterator.Billing_Start_Date__c;
                objNewOrdL.Billing_Frequency__c = OliIterator.Billing_Frequency__c;
                objNewOrdL.Payment_Method__c = OliIterator.Payment_Method__c;
                objNewOrdL.Payment_Duration__c = OliIterator.Payment_Duration__c ;
                objNewOrdL.Billing_Partner__c = OliIterator.Billing_Partner__c;
                objNewOrdL.Billing_Contact__c = OliIterator.Billing_Contact__c;
                objNewOrdL.Billing_Partner_Account__c = OliIterator.Billing_Partner_Account__c;
                objNewOrdL.Payments_Remaining__c = 12;
                objNewOrdL.Successful_Payments__c = 0;
                //objNewOrdL.Order_Line_Total__c = OliIterator.Order_Line_Total__c;
                objNewOrdL.Line_Status__c = OliIterator.Line_Status__c;
                objNewOrdL.Media_Type__c = OliIterator.Media_Type__c;
                objNewOrdL.RecordTypeId = OliIterator.RecordTypeId;
                objNewOrdL.Seniority_Date__c = OliIterator.Seniority_Date__c;
                if(mapOliDir.get(OliIterator.Directory__c) != null) {
                    objNewOrdL.Telco_Invoice_Date__c = mapOliDir.get(OliIterator.Directory__c).Bill_Prep__c;
                    objNewOrdL.Print_First_Bill_Date__c = mapOliDir.get(OliIterator.Directory__c).New_Print_Bill_Date__c;
                    if(objNewOrdL.Payment_Method__c == 'Telco Billing'){
                        objNewOrdL.Effective_Date__c = mapOliDir.get(OliIterator.Directory__c).Pub_Date__c.adddays(-65);
                    }   
                    else{
                        objNewOrdL.Effective_Date__c = OliIterator.Last_Billing_Date__c;
                    }
                }
                //objNewOrdL.ModificationOrderLineItem_Status__c = 'Renewed';
                if(modifiedUnitPrice != null){
                    objNewOrdL.UnitPrice__c = modifiedUnitPrice;  
                }
                else {
                    system.debug('************Enter Here if no Rate*********');
                    objNewOrdL.UnitPrice__c = OliIterator.UnitPrice__c;
                }
                 //As per ATP-4375
                if(OliIterator.Anchor_Listing_Caption_Header__c != null){
               		objNewOrdL.Anchor_Listing_Caption_Header__c = OliIterator.Anchor_Listing_Caption_Header__c;
                }
                if(OliIterator.Trademark_Finding_Line_OLI__c != null){
                	objNewOrdL.Trademark_Finding_Line_OLI__c = OliIterator.Trademark_Finding_Line_OLI__c;
                }
                if(OliIterator.Scoped_Caption_Header__c != null){
                	objNewOrdL.Scoped_Caption_Header__c = OliIterator.Scoped_Caption_Header__c;
                }
                if(!mapnewOLI.containskey(OliIterator.Id)){
                    mapnewOLI.put(OliIterator.Id, objNewOrdL);
                }
                OliIterator.Is_Handled__c = true;
                lstOroliUpdate.add(OliIterator);
            }
        }
        if(mapnewOLI.size()>0){
            insert mapnewOLI.values();
            for(Order_Line_Items__c iteratorNewOLI :mapnewOLI.values()){
            	mapOldNewOli.put(iteratorNewOLI.Original_Order_Line_Item__c,iteratorNewOLI.Id);
            }
            if(mapOldNewOli.size()> 0){
            	for(Order_Line_Items__c iteratorNewOLI :mapnewOLI.values()){
            		if(iteratorNewOLI.Anchor_Listing_Caption_Header__c != null){
            			iteratorNewOLI.Anchor_Listing_Caption_Header__c = mapOldNewOli.get(iteratorNewOLI.Anchor_Listing_Caption_Header__c);
            			lstOroliUpdate.add(iteratorNewOLI);
            		}
            		else if(iteratorNewOLI.Trademark_Finding_Line_OLI__c != null){
            			iteratorNewOLI.Trademark_Finding_Line_OLI__c = mapOldNewOli.get(iteratorNewOLI.Trademark_Finding_Line_OLI__c);
            			lstOroliUpdate.add(iteratorNewOLI);
            		}
            	}
            }
            if(lstOroliUpdate.size()> 0){
                update lstOroliUpdate;
            }
            createDFFClone(mapnewOLI,mapOliDir);
        }
        
    }
    
   
    public static string generateAccountDEString(list<Order_Line_Items__c> lstOLIDE) {
        Order_Line_Items__c objOliDEAct = lstOLIDE[0];
        String strOLIDEACT = '';
        strOLIDEACT += 'A Boost Renewal notification for '+objOliDEAct.Account__r.Name+'<br/> <br/><br> Please see details below: <br/>';
        strOLIDEACT += '<table cellpadding="0" cellspacing="0" width="100%"><tr><td style="font-weignt:bold;text-align:left;">Account Name:</td><td>'+objOliDEAct.Account__r.Name+'</td></tr><tr><td style="font-weignt:bold;text-align:left;">Account Owner Name:</td><td>'+objOliDEAct.Account__r.Owner.Name+'</td></tr><tr><td style="font-weignt:bold;text-align:left;">Directory Edition Name:</td><td>'+objOliDEAct.Directory_Edition__r.Name+'</td></tr></table>';
        strOLIDEACT += '<table width="100%"><tr><td border="0" align="left" width="40%">OL Name</td><td border="0" align="center" width="30%">UDAC</td><td border="0" align="right" width="30%"> Unit Price</td></tr></table><br>';
        return strOLIDEACT;
    }
    
    public static string generateDirOlitemsString(Order_Line_Items__c iterator){
        String oliDirString = '';
        oliDirString += '<table width="100%"><tr><td border="0" align="left" width="40%">'+iterator.Name+'</td><td border="0" align="center" width="30%">'+iterator.Udac__c+'</td><td border="0" align="right" width="30%">$'+iterator.UnitPrice__c.setscale(2)+'</td></tr></table>'; 
        //oliDirString += '</body></html>';
        return oliDirString;
    }
    public static string generateBottomString(){
        String oliBtString = '';
        oliBtString += '<br><br> Regards,<br><br> The Berry Company <br> '; 
        oliBtString += '</body></html>';
        return oliBtString;
    }
    
    public static void sendEmail(map<Id, List<String>> mapBoostRenewal,map<Id,Id> mapUserManagerId) {
        List<Messaging.SingleEmailMessage> lstEmail= new list<Messaging.SingleEmailMessage>();
        for(Id ownerId : mapBoostRenewal.keySet()) {
            for(integer i=0;i<mapBoostRenewal.get(ownerId).size();i++){
            //Initializing for Sending Mails(Written Single because of in a loop)
            system.debug('************** mapBoostRenewal LIST NO:'+i);
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                List<String> toAddresses = new List<String>();
                //toAddresses.add('smallela4@csc.com');
                //toAddresses.add(mapUserManagerId.get(ownerId));
                email.setTargetObjectId(ownerId);
                //email.setToAddresses(toAddresses);
                email.setSubject('Boost Account Renewal');
                email.setHtmlBody(mapBoostRenewal.get(ownerId)[i]);
                email.saveAsActivity = false;
                lstEmail.add(email);
            if(mapUserManagerId.get(ownerId) != null){
            system.debug('*********Enter here for Mangers Email*****');
                Messaging.SingleEmailMessage manEmail = new Messaging.SingleEmailMessage();
                manEmail.setTargetObjectId(mapUserManagerId.get(ownerId));
                //email.setToAddresses(toAddresses);
                manEmail.setSubject('Boost Account Renewal');
                manEmail.setHtmlBody(mapBoostRenewal.get(ownerId)[i]);
                manEmail.saveAsActivity = false;
                lstEmail.add(manEmail );    
            }
            }
            
        }
        Messaging.sendEmail(lstEmail);
    }
   
    public static map<Id,Opportunity> createNewOpty(set<Id> setOpptyId){
        List<Opportunity> OptyrecordLst = new List<Opportunity>();
        map<Id,Opportunity> mapnewOpty = new map<Id,Opportunity>();    
        OptyrecordLst = [Select Id,Name,AccountId,Account_Manager__c,Billing_Contact__c,Signing_Contact__c,Signing_Method__c,Billing_Partner__c,Payment_Method__c,Billing_Frequency__c  from Opportunity where Id = :setOpptyId ];
        
        if(OptyrecordLst.size()>0){
            for(Opportunity OldOpty :OptyrecordLst){
               
                Opportunity newOpty = new Opportunity();
                newOpty =  OldOpty.clone(false);
                newOpty.StageName = 'Auto Renewal';
                newOpty.Billing_Partner__c = OldOpty.Billing_Partner__c;
                newOpty.billing_defaults_set__c = true;
                //newOpty.isLocked__c = true;
                newOpty.RecordTypeId = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityLockRTName, CommonMessages.opportunityObjectName);       
                newOpty.CloseDate = system.today();
                
                if(!mapnewOpty.containsKey(OldOpty.Id)) {
                    mapnewOpty.put(OldOpty.Id, newOpty);
                }
            }
        }
        return mapnewOpty;
       
    }
    public static map<Id,Order_Group__c> createNewOrderSet(set<Id> setOrdsetId, map<Id,Opportunity> mapnewobjOpp){
        List<Order_Group__c> OrdersetLst = new List<Order_Group__c>();
        map<Id,Order_Group__c> mapnewOrderSet = new map<Id,Order_Group__c>();    
        OrdersetLst = [Select Id,Name,Order__c,IsActive__c,Order_Account__c,Opportunity__c,selected__c,Transaction_ID__c,Type__c from Order_Group__c where Id = :setOrdsetId ];
        
        if(OrdersetLst.size()>0){
            for(Order_Group__c OldOrderSet :OrdersetLst){
                Order_Group__c newOrderSet = new Order_Group__c();
                newOrderSet =  OldOrderSet.clone(false);
                if(mapnewobjOpp.get(OldOrderSet.Opportunity__c) != null){
                    newOrderSet.Opportunity__c = mapnewobjOpp.get(OldOrderSet.Opportunity__c).Id;
                }
                if(!mapnewOrderSet.containsKey(OldOrderSet.Id)) {
                    mapnewOrderSet.put(OldOrderSet.Id, newOrderSet);
                }
            }
        }
        return mapnewOrderSet;
    }
    public static void createDFFClone(map<Id,Order_Line_Items__c> mapnewobjOLI,map<Id,directory_edition__c> mapOliDirEdition){
        set<Id> oldOLIId = new set<Id>();
        list<Digital_Product_Requirement__c> DFFList = new list<Digital_Product_Requirement__c> ();
        map<Id, Digital_Product_Requirement__c> OLIDFFMap = new map<Id, Digital_Product_Requirement__c>();
        list<Digital_Product_Requirement__c> DFFInsertList = new list<Digital_Product_Requirement__c> ();
        list<order_line_items__c> lstNewoliDFF = new list<order_line_items__c>();
        for(Id OldId : mapnewobjOLI.keyset()){
            oldOLIId.add(OldId);
        }
        String strQuery = CommonMethods.getCreatableFieldsSOQL('Digital_Product_Requirement__c');
        strQuery+='where OrderLineItemID__c in :oldOLIId';
        DFFList = Database.query(strQuery);
        if(DFFList.size()>0){
            for(Digital_Product_Requirement__c DFF : DFFList){
                OLIDFFMap.put(DFF.OrderLineItemID__c, DFF);
            }
        }
        if(OLIDFFMap.size()>0){
           for(Id OLIId: mapnewobjOLI.keyset()){
                Digital_Product_Requirement__c DFF = new Digital_Product_Requirement__c();
                if(OLIDFFMap.get(OLIId) != null){
                    DFF = OLIDFFMap.get(OLIId).clone(false, false, false, false);
                }
                DFF.Core_Opportunity_Line_ID__c = '';
                DFF.OrderLineItemID__c = mapnewobjOLI.get(OLIId).Id;
                DFF.Directory_Edition__c = mapnewobjOLI.get(OLIId).Directory_edition__c;
                DFF.Issue_Number__c = mapOliDirEdition.get(DFF.DFF_Directory__c).Edition_Code__c;
                DFFInsertList.add(DFF);
            }
        }
        if(DFFInsertList.size() > 0 ){
            insert DFFInsertList;
        }
        
        for(Digital_product_requirement__c objDFFNew : DFFInsertList){
            order_line_items__c objOLINewDFF = new Order_line_items__c(Id = objDFFNew.OrderLineItemID__c);
            objOLINewDFF.Digital_Product_Requirement__c = objDFFNew.Id;
            lstNewoliDFF.add(objOLINewDFF);
        }
        if(lstNewoliDFF.size()>0){
            update lstNewoliDFF;
        }
    }
}