@isTest(seealldata=true)
public class NationalBillingSendRemittanceTest {
    public static testmethod void NationalBillingSendRemittanceTest(){
       /* Directory__c objDir = TestMethodsUtility.generateDirectory();
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        date myDate = date.newInstance(2014, 12, 17);
        objDirEd.Pub_Date__c = myDate;
        insert objDirEd;
        
        National_Commission__c objNC = new National_Commission__c(Rate__c=10);
        insert objNC;
        
        Account objPubcoAcct = TestMethodsUtility.generateAccount('publication');
        objPubcoAcct.Publication_Code__c = '0012';
        objPubcoAcct.National_Commission__c = objNC.id;
        insert objPubcoAcct;
        Account objCMRAcct = TestMethodsUtility.generateCMRAccount();
        objCMRAcct.CMR_Number__c= '1234';
        objCMRAcct.National_Commission__c = objNC.id;
        objCMRAcct.AccountNumber = '1234';
        insert objCMRAcct;
        Account objClientAcct = TestMethodsUtility.generateCustomerAccount();
        objClientAcct.Client_Number__c='2345';
        objClientAcct.National_Commission__c = objNC.id;
        insert objClientAcct;
        
        ISS__c objISS = TestMethodsUtility.generateISS();
        objISS.Invoice_Date__c = System.today();
        objISS.CMR__c = objCMRAcct.id;
        objISS.Publication_Company__c = objPubcoAcct.id;
        insert objISS;
        ISS_Line_Item__c objISSLine = new ISS_Line_Item__c(ISS__c = objISS.id, Commission__c = 10, Client_Name__c = objClientAcct.id, Directory__c = objDir.id, Inv_Type__c = 'IR', Product__c = CommonMessages.nationalProductID);
        objISSLine.Directory_Edition__c = objDirEd.id;
        insert objISSLine;
        
        National_Billing_Status__c natBillStatus = TestMethodsUtility.generateNatBillStatus(objPubcoAcct.Id, objDirEd.Id);
        natBillStatus.RSAP_Send_Remittance_Started__c = true;
        insert natBillStatus;
        
        Test.startTest();
        NationalBillingSendRemittance NBSR = new NationalBillingSendRemittance();
        NBSR.chkRemittanceComplete = true;
        NBSR.chkFFInvBalanced = true;
        NBSR.strPubCodeSendRemittance = '0012';
        NBSR.strEditionCodeSendRemittance = '1412';
        NBSR.fetchISSForRemittance();
        NBSR.generateRemitPDFandFF();
        NBSR.addTrailingSpace('');
        NBSR.updateStatusTable();
        NBSR.sendToElite();
        NBSR.postInvToFF();
        NBSR.PollingBatchProcess();
        Test.stopTest();
    }
}