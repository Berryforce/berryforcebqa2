global class ListingBatch implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
    String query='Select id,Phone__c,Listing_Phone_Unformatted_Substitute__c from Listing__c';
    return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext bc, List<Listing__c> listingList) {
        try{
        for(Listing__c iteratorLst:listingList){
                if(iteratorLst.Phone__c!=null && String.isNotBlank(iteratorLst.Phone__c)){
                        iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Phone__c.remove('(');
                        iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Listing_Phone_Unformatted_Substitute__c.remove(')');
                        iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Listing_Phone_Unformatted_Substitute__c.remove('-');
                        iteratorLst.Listing_Phone_Unformatted_Substitute__c=iteratorLst.Listing_Phone_Unformatted_Substitute__c.remove(' ');
                    }
                    }
                    update listingList;
                    }catch(Exception e){
            System.debug('The exception is'+e);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Listing batch fail ');
        }
    }
    
    global void finish(Database.BatchableContext bc) {
    }
}