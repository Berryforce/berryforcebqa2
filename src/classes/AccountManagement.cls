public with sharing class AccountManagement {
    private Account objAccount {get;set;}
    public Account objChildAccount {get;set;}
    public Contact objContact {get;set;}
    public String accountName {get;set;}
    
    public list<AccountWrapperClass> lstAccountWrapper {get;set;}
    public list<Account> lstUpdatedAccount {get;set;}
    
    public AccountManagement(Apexpages.Standardcontroller controller) {
        objAccount = (Account)controller.getRecord();
        objAccount = AccountSOQLMethods.getAccountByOpportunityAccountID(new set<Id>{objAccount.Id})[0];
        system.debug('Account : '+ objAccount);
        objChildAccount = new Account(ParentId = objAccount.Id, RecordTypeId = objAccount.RecordtypeId);
    }
    
    public void onLoad() {
        lstAccountWrapper = new list<AccountWrapperClass>();
    }
    
    public void accountSearch() {
        accountName = '%'+accountName+'%';
        list<Account> lstAccount = AccountSOQLMethods.getAccountByAccountName(accountName, new set<Id>{objAccount.Id});
        for(Account iteratorAccount : lstAccount) {
            lstAccountWrapper.add(new AccountWrapperClass(false, iteratorAccount));
        }
    }
    
    public PageReference createChildAccount() {
      return Page.CreateChildAccount;
    }
    
    public PageReference saveAccount() {
        if(insertOperation()) {
            return new Pagereference('/'+objChildAccount.Id);
        }
        return null;
    }
    
    private boolean insertOperation() {
        boolean status = false;
        try {
            objChildAccount.Billing_Anniversary_Date__c=objAccount.Billing_Anniversary_Date__c;
            insert objChildAccount;
            status = true;
        }
        catch(System.DmlException ex) {
            status = false;
            CommonMethods.addError(ex.getDmlMessage(0));
        }
        
        if(status) {            
            Order__c newOrder = new Order__c();
            newOrder = createOrder(objChildAccount);
            insert newOrder;
        }
        return status;
    }
    
    public PageReference saveAccountandCreateContact() {
        if(insertOperation()) {
            system.debug('Account id is ' + objChildAccount.Id);
            List<Account> TempAccts = [SELECT ID, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode FROM Account WHERE ID = : objChildAccount.Id];
            system.debug('Accounts are ' + TempAccts);
            Account TempAcct = new Account();
            TempAcct = TempAccts.get(0);
            system.debug('Account is ' + TempAcct);
                    
            objContact = new Contact(AccountId = objChildAccount.Id, MailingStreet = TempAcct.BillingStreet, MailingCity = TempAcct.BillingCity, MailingCountry = TempAcct.BillingCountry, MailingState = TempAcct.BillingState, MailingPostalCode = TempAcct.BillingPostalCode);
            String url = '/003/e?accid='+ objChildAccount.Id +'&retURL=%2F'+objChildAccount.Id;
            //return new Pagereference(url);
            return Page.CreateContact;
        }
        return null;
    }
    
    public PageReference saveContact() {
        if(insertContact()) {
            return new Pagereference('/'+objAccount.Id);
        }
        return null;
    }
    
    public PageReference saveContactAndNew() {
        if(insertContact()) {
            objContact = new Contact(AccountId = objChildAccount.Id);
            return Page.CreateContact;
        }
        return null;
    }
    
    private boolean insertContact() {
        boolean status = false;
        try {
            objContact.IsActive__c = true;
            insert objContact;
            status = true;
        }
        catch(System.DmlException ex) {
            status = false;
            CommonMethods.addError(ex.getDmlMessage(0));
        }
        return status;
    }
    
    public PageReference cancelChildAccount() {
      return Page.AccountManagementPage;
    }
    
     public PageReference cancelContact() {
      return new Pagereference('/'+objAccount.Id);
    }
    
    public Pagereference convertExistingAccountToChildAccount() {
        list<Account> lstAccount = new list<Account>();
        list<Order__c> orderList = new list<Order__c>();
        
        if(validateSelectedData()) {
            for(AccountWrapperClass iterator : lstAccountWrapper) {
                if(iterator.bFlag) {
                    iterator.objAccount.ParentId = objAccount.Id;
                    if(iterator.objAccount.Billing_Anniversary_Date__c==null){
                    	iterator.objAccount.Billing_Anniversary_Date__c=objAccount.Billing_Anniversary_Date__c;
                    }
                    lstAccount.add(iterator.objAccount);
                }
            }
            
            if(lstAccount.size() > 0) {
                update lstAccount;
                Set<Id> acctIds = new Set<Id>();
                list<Order__c> orderListForMap = new list<Order__c>();
                Map<Id, Order__c> acctIdOrderMap = new Map<Id, Order__c>();
                
                for(Account acct : lstAccount){
                    acctIds.add(acct.Id);
                }
                                
                orderListForMap = OrderSOQLMethods.getOrderListByAccountID(acctIds);
                
                for(Order__c ordr : orderListForMap){
                    acctIdOrderMap.put(ordr.Account__c, ordr);
                }
                
                for(Account acct : lstAccount){
                    if(!acctIdOrderMap.containsKey(acct.Id)){
                        Order__c newOrder = new Order__c();
                        newOrder = createOrder(acct);
                        orderList.add(newOrder);
                    }
                }
                
                if(orderList.size() > 0){
                    insert orderList;
                }
                
                lstUpdatedAccount = lstAccount;
                return Page.ViewUpdatedAccount;
            }
        }
        return null;
    }
    
    public Pagereference cancel() {     
        return new Pagereference('/'+objAccount.Id);
    }
    
    private boolean validateSelectedData() {
        boolean bFlag = false;
        for(AccountWrapperClass iterator : lstAccountWrapper) {
            if(iterator.bFlag) {
                bFlag = true;
            }
        }
        
        if(!bFlag) {
            CommonMethods.addError(CommonMessages.invoiceCheckbox);
            return false;
        }
        return true;
    }
    
    public class AccountWrapperClass {
        public boolean bFlag {get;set;}
        public Account objAccount {get;set;}
        
        public AccountWrapperClass(boolean bFlag, Account objAccount) {
            this.bFlag = bFlag;
            this.objAccount = objAccount;
        }
    }
    
    public Order__c createOrder(Account acct){
        List<Order__c> orderList = new List<Order__c>();
        orderList = objAccount.Orders__r;
        Order__c newOrder = new Order__c();
        newOrder.Name =  'Order-' + acct.Name;
        newOrder.Account__c = acct.Id;
        if(orderList.size() > 0){
            newOrder.Billing_Anniversary_Date__c = orderList.get(0).Billing_Anniversary_Date__c;
        }
        return newOrder;
    }
    
    public List<Schema.FieldSetMember> getAccFields() {
        return SObjectType.Account.FieldSets.CustomerAccountInformation.getFields();
    }
    
    public List<Schema.FieldSetMember> getMgrFields() {
        return SObjectType.Account.FieldSets.CustomerAccountManager.getFields();
    }
    
    public List<Schema.FieldSetMember> getAddFields() {
        return SObjectType.Account.FieldSets.AdditionalInformation.getFields();
    }
    
    public List<Schema.FieldSetMember> getConAddressFields() {
        return SObjectType.Contact.FieldSets.AddressInformation.getFields();
    }
    
    public List<Schema.FieldSetMember> getConInfoFields() {
        return SObjectType.Contact.FieldSets.ContactInformation.getFields();
    }
    
    public List<Schema.FieldSetMember> getConFlagFields() {
        return SObjectType.Contact.FieldSets.Flags.getFields();
    }
    
    public List<Schema.FieldSetMember> getConAddFields() {
        return SObjectType.Contact.FieldSets.AdditionalInformation.getFields();
    }
}