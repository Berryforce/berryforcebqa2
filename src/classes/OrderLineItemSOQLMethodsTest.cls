@isTest(SeeAllData=True)
public class OrderLineItemSOQLMethodsTest{

    public static testMethod void OrderLineItemSOQLMethodsTest() {
        test.startTest();
        
        Account objAccount = TestMethodsUtility.generateAccount('customer');
        insert objAccount;
        
        Contact objContact = TestMethodsUtility.createContact(objAccount);
        insert objContact;
        
        Opportunity objOpportunity = TestMethodsUtility.createOpportunity(objAccount, objContact);
        insert objOpportunity;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        
        insert objProd;
        
        Listing__c objMListing = TestMethodsUtility.generateListing('Main Listing');
        objMListing.Account__c = objAccount.Id;
        objMListing.Primary_Canvass__c = objAccount.Primary_Canvass__c;
        insert objMListing;
        
        Listing__c objListing = TestMethodsUtility.generateListing('Additional Listing');
        objListing.Account__c = objAccount.Id;
        objListing.Primary_Canvass__c = objAccount.Primary_Canvass__c;
        objListing.Main_Listing__c = objMListing.id;
        insert objListing;
        
        List<OpportunityLineItem> LstOppLI = TestMethodsUtility.selectOpportunityLineItem(objOpportunity);
        insert LstOppLI;
             
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;
        
        Order_Group__c objOrderGroup = new Order_Group__c (Name = 'Unit Testing', selected__c = true, Order_Account__c = objAccount.ID, Opportunity__c = objOpportunity.ID);                                            
        insert objOrderGroup ;
        
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id);
        objOrderLineItem.Listing__c = objListing.Id;
        insert objOrderLineItem ;
        
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='Order_Line_Items__c' and isActive=true];
        map<string,RecordType>RecordTypeMap = new map<string,RecordType>();
        for(RecordType rectype : rtypes)
        {
            RecordTypeMap.put(rectype.name, rectype);
        }
        
        Product2 objProduct = TestMethodsUtility.createProduct2();  
        insert objProduct;
        
           Telco__c newTelco = TestMethodsUtility.createTelco(objAccount.Id);
        
        /*Directory__c objDir = new Directory__c(Name = 'Test Dir1',Directory_Code__c='2829', EBD__c = System.Today().addDays(1), DCR_Close__c = System.Today().addDays(5), BOC__c = System.Today().addDays(2), Final_Service_Order_Due_to_Berry__c = System.Today().addDays(1), Sales_Lockout__c = System.Today(), Book_Extract_YP__c = System.Today().addDays(3), Ship_Date__c = System.Today().addDays(-2), Pub_Date__c = System.Today().addDays(4), Publication_Company__c = objAccount.Id,Telco_Provider__c=newTelco.id);
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();
        Directory_Edition__c objDirEdition = new Directory_Edition__c(Name = 'Test Dir Edition', Directory__c = objDir.Id, Book_Status__c = 'BOTS');
        insert objDirEdition;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        
        
        Set<ID> setOLI = new Set<ID>();
        setOLI.add(objOrderLineItem.id);
        set<String> sepOPPLI = new set<String>();
        OpportunityLineItem objOppLI;
        Set<ID> setOrderGroupId = new Set<ID>();
        setOrderGroupId.add(objOrderGroup.id);
        
        if(LstOppLI != null && LstOppLI.size() > 0){
            objOppLI = LstOppLI[0];
            sepOPPLI.add(objOppLI.Id);
        }      
        
        
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateManualNSRT();
        objNSOS.CMR_Name__c = objAccount.id;
        objNSOS.Is_Ready__c = true;
        objNSOS.Directory__c = objDir.id;
        objNSOS.Directory_Edition__c =objDirEdition.Id;
        objNSOS.Is_Converted__c=true;
        insert objNSOS;
        
        List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Ready_for_Processing__c = true;
        objNSLI.UDAC__c='SS';
        objNSLI.Standing_Order__c=true;
        objNSLI.National_Staging_Header__c = objNSOS.id;
        objNSLI.Is_Changed__c=true;
        objNSLI.Product2__c=objProduct.Id;
        ListNSLI.add(objNSLI);
        
        National_Staging_Line_Item__c objNSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI1.Ready_for_Processing__c = true;
        objNSLI1.UDAC__c='SS';
        objNSLI1.Standing_Order__c=true;
        objNSLI1.National_Staging_Header__c = objNSOS.id;
        objNSLI1.Is_Changed__c=true;
        objNSLI1.Product2__c=objProduct.Id;
        ListNSLI.add(objNSLI1);
        
        insert ListNSLI;
        
        objOrderLineItem.National_Staging_Line_Item__c=objNSLI1.id;
        upsert objOrderLineItem;
        
        Canvass__c c = CommonUtility.createCanvasTest();
      insert c;
                  
        
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOrderLineItemByAccountID(new set<Id>{objAccount.Id}));
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOrderLineItemByAccountIDOrderByOrder(setOrderGroupId));
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOrderLineItemByAccount(new set<Id>{objAccount.Id}));
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOLIByNationalOrderSet(setOrderGroupId));
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOLIByNationalOrderSet(setOrderGroupId));
        //OrderLineItemSOQLMethods.getOrderLineItemForStatementCronJob(new set<String>{'Print'}, Date.today());
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOrderLineItemandMoliByID(setOLI));
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOrderLineItemByID(setOLI));
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOLIForNationalDFF(setOLI));
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOLIByID(setOLI));
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOLIForSalesInvoicebyIDs(setOLI));
        //System.assertNotEquals(null, OrderLineItemSOQLMethods.getOLIForStatementByIds(setOLI, Date.today(), Date.today()));
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOLIForBillingTransfer(setOLI));
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOLIForSpecialtyProductByDirEditionId(objDirEdition.id));
        OrderLineItemSOQLMethods.getOLIForSalesInvoicebyPackageIDs(new set<String>{'P001'});
        System.assertNotEquals(null, OrderLineItemSOQLMethods.getOrderLineItemByAccountIDs(new set<Id>{objAccount.Id}));
        OrderLineItemSOQLMethods.getOrderLineItemByComboValueForSeniorityDate(new Set<String> {'C123'});
        OrderLineItemSOQLMethods.getOLIFByDirEditionIds(new Set<Id> {objDirEdition.id});
        OrderLineItemSOQLMethods.getOpportunityByAcctIdsforCS(new set<Id>{objAccount.Id});
        OrderLineItemSOQLMethods.getOLIByTransUniqueCombo(objDir.Directory_Code__c, objDirEdition.Edition_Code__c, 'test', 'test');
        OrderLineItemSOQLMethods.getOLIByIDForP4P(new set<Id> {objOrderLineItem.Id});
        OrderLineItemSOQLMethods.getOLIByListingIds(new set<Id> {objListing.Id});
        OrderLineItemSOQLMethods.getOLIByNationalOrderSet_V3(objAccount.Publication_Code__c, objDirEdition.Edition_Code__c, objDir.Id);
        OrderLineItemSOQLMethods.getOLIByNationalOrderSetWithoutInv(objAccount.Publication_Code__c, objDirEdition.Edition_Code__c, objDir.Id);
       
        
        OrderLineItemSOQLMethods.getOLIbyNSLI(new set<ID>{objNSLI1.id});
        OrderLineItemSOQLMethods.getOLIByCavassAndAcct(c.id,objAccount.id);
        OrderLineItemSOQLMethods.getOliById(objOrderLineItem.id);
        OrderLineItemSOQLMethods.getOLIByIdsForTradMarkFindLine(new set<ID>{objOrderLineItem.id});
        OrderLineItemSOQLMethods.getOLIbyunsuppressedSL(new set<ID>{objOrderLineItem.id});
        OrderLineItemSOQLMethods.getOLINotHandledinModification(objDirEdition.id,UserInfo.getUserId());
        OrderLineItemSOQLMethods.getOrderLineItemForSeniorityDateCombo(new set<string>{'test'});
        OrderLineItemSOQLMethods.getOrderLineItemForSeniorityDate(new set<id>{objAccount.id},new set<ID> {objDir.id},new set<id> {objDH.id}, new set<id> {objProd.id});
       test.stopTest();
   
    }    
}