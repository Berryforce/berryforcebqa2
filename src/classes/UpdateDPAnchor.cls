global class UpdateDPAnchor implements Database.Batchable<sObject>{
   
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator('select ID, Order_Line_Item__c, Order_Line_Item__r.Anchor_Listing_Caption_Header__c, DP_OLI_Anchor_Caption_Header__c from Directory_Pagination__c where Order_Line_Item__r.Anchor_Listing_Caption_Header__c != null and Order_Line_Item__c != null and DP_OLI_Anchor_Caption_Header__c = null');
   }

   global void execute(Database.BatchableContext BC, List<Directory_Pagination__c> scope){
     for(Directory_Pagination__c s : scope){
         s.DP_OLI_Anchor_Caption_Header__c = s.Order_Line_Item__r.Anchor_Listing_Caption_Header__c;
     }
     update scope;
    }

   global void finish(Database.BatchableContext BC){
   }
}