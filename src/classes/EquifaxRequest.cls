public with sharing class EquifaxRequest {
	
	public String strBusinessName1{get;set;}
	public String strPostalCode1{get;set;}
	public String strState1{get;set;}
	public String strCity1{get;set;}
	public String strAddressLine11{get;set;}
	
	public String ConSubjectSSN1{get;set;}
	public String ConSubjectBirthDate1{get;set;}
	public String ConFirstName1{get;set;}
	public String ConLastName1{get;set;}
	public String ConStreetName1{get;set;}
	public String ConCity1{get;set;}
	public String ConState1{get;set;}
	public String ConPostalCode1{get;set;}
	
	public String Reqresultconsumer{get;set;}
	public String Resresultconsumer{get;set;}
	
	public String ReqresultCommercial{get;set;}
	public String ResresultCommercial{get;set;}
	
	Public String BSureC_EquifaxURL='';
	public map<String, String> mapXmlBook=new map<String, String>();
	public  EquifaxRequest(){
	}
	/*public map<string,string> getEquifaxCommercialReport(Vertex_Berry.Bsure_commonUtilityFunctioncall.customerinfobean objCustInfo)
	{
		
		 return null;
	}*/
	public void click()
	{
		Vertex_Berry.Bsure_commonUtilityFunctioncall.ConsumerInfoBean objConsumerInfo = new Vertex_Berry.Bsure_commonUtilityFunctioncall.ConsumerInfoBean();
		objConsumerInfo.ConSubjectSSN=ConSubjectSSN1;
		objConsumerInfo.ConSubjectBirthDate=ConSubjectBirthDate1;
		objConsumerInfo.ConFirstName=ConFirstName1;
		objConsumerInfo.ConLastName=ConLastName1;
		objConsumerInfo.ConStreetName=ConStreetName1;
		objConsumerInfo.ConCity=ConCity1;
		objConsumerInfo.ConState=ConState1;
		objConsumerInfo.ConPostalCode=ConPostalCode1;
		
		
		Vertex_Berry.Bsure_commonUtilityFunctioncall.customerinfobean objCustInfo = new Vertex_Berry.Bsure_commonUtilityFunctioncall.customerinfobean();
		objCustInfo.strAddressLine1 = strAddressLine11;
		objCustInfo.strCity = strCity1;
		objCustInfo.strState = strState1;
		objCustInfo.strPostalCode = strPostalCode1;
		objCustInfo.strBusinessName = strBusinessName1;
		map<String,String> mapEfxResult = new map<String,String>();
		mapEfxResult = getEquifaxCommercialReport(objCustInfo,objConsumerInfo);
		System.debug('mapEfxResult=========='+mapEfxResult);
	}
	public map<string,string> getEquifaxCommercialReport(Vertex_Berry.Bsure_commonUtilityFunctioncall.customerinfobean objCustInfo,Vertex_Berry.Bsure_commonUtilityFunctioncall.ConsumerInfoBean objConsInfo)
	{
		 //system.debug('Hello'); 
		 String EnquiryString=''; 
		 BSureC_EquifaxURL='';
		 String StrBSureC_EquifaxURL='EquifaxCommercialURL';
		 String strPOST='POST';
		 map<String, String> mapResultBook =new map<String, String>();
		 map<String,String> mapAlias=new map<string,string>();
		 map<String,String> mapProductCodes=new map<string,string>();
		 map<String,String> mapConsumerAlias=new map<string,string>();
		 set<String> setScores=new set<String>();
		 try{
			 for(Vertex_Berry__BsureEquifax__c objBsureEq:Vertex_Berry__BsureEquifax__c.getAll().values())
			 {
			 	if(objBsureEq!=null && objBsureEq.Vertex_Berry__RequestType__c!=null && objBsureEq.Vertex_Berry__RequestType__c!='' && objBsureEq.Vertex_Berry__RequestType__c=='Commercial')
			 	{
				 	if(objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__AliasName__c!=null && objBsureEq.Vertex_Berry__AliasName__c!='')
				 	{
				 		mapAlias.put(objBsureEq.Vertex_Berry__NodeName__c,objBsureEq.Vertex_Berry__AliasName__c);
				 	}
				 	if(objBsureEq.Vertex_Berry__ReportName__c!='' && objBsureEq.Vertex_Berry__ReportName__c!='' && objBsureEq.Vertex_Berry__ReportCode__c!=null && objBsureEq.Vertex_Berry__ReportCode__c!='')
				 	{
				 		mapProductCodes.put(objBsureEq.Vertex_Berry__ReportName__c,objBsureEq.Vertex_Berry__ReportCode__c);
				 	}
			 	}else if(objBsureEq!=null && objBsureEq.Vertex_Berry__RequestType__c!=null && objBsureEq.Vertex_Berry__RequestType__c!='' && objBsureEq.Vertex_Berry__RequestType__c=='Consumer')
			 	{
				 	if(objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__AliasName__c!=null && objBsureEq.Vertex_Berry__AliasName__c!='')
					 	{
					 		mapConsumerAlias.put(objBsureEq.Vertex_Berry__NodeName__c,objBsureEq.Vertex_Berry__AliasName__c);
					 	}
			 	}
			 }
			 //BSureC_EquifaxEnquiry objEnquiry=new BSureC_EquifaxEnquiry();
			 
			 /*Building XML Request String*/
	    	 EnquiryString = getEnquiryString( objCustInfo, mapProductCodes);
	    	 
	    	 /*Get Endpoint Url from Custom Setting*/
	    	 BSureC_EquifaxURL= getConfigurationValues(StrBSureC_EquifaxURL).get(0);
			 /*BSure_Configuration_Settings__c objConfigSetting=BSure_Configuration_Settings__c.getValues(StrBSureC_EquifaxURL);
			 if(objConfigSetting.Vertex_Berry__Parameter_Value__c!=null)
			 {
				BSureC_EquifaxURL=objConfigSetting.Vertex_Berry__Parameter_Value__c;
			 }*/
			 ReqresultCommercial = EnquiryString;
			 HttpRequest req = new HttpRequest(); 
			 req.setEndpoint(BSureC_EquifaxURL);
			 req.setHeader('Content-Type', 'text/xml');
			 req.setMethod(strPOST);
			 req.setTimeout(100000);
			 //system.debug(EnquiryString);
			 req.setBody(EnquiryString);
			 Http http = new Http();
			 XmlStreamReader reader;
			 if(!Test.isRunningTest()){
				 HttpResponse res = http.send(req);
				 
				 system.debug('res===Commercial============'+res.getBody());
				 ResresultCommercial = res.getBody();
				 // Generate the HTTP response as an XML stream
				 reader = res.getXmlStreamReader();
				 System.debug('##########XML DATA##########'+res.getXmlStreamReader());
			 }
		     // Read through the XML
		     mapResultBook = parseBooks(reader,mapAlias);
		     
		     getEquifaxConsumerReport(objConsInfo,mapResultBook,mapConsumerAlias);
			   //  return null;
		     //}else{
		     	 return mapResultBook;
		     //}
		 }catch(System.CalloutException e)
		 { 
		 	System.debug('ERROR:Commercial=====' + e.getMessage());
		 	mapXmlBook.put('CommercialTimeout','Equifax commercial server error : '+e.getMessage());
		 	notifyDevelopersOf(e);  

		 	return mapXmlBook;
		 }
	}
	
	//THIS METHOD IS OVERIDDEN BELOW
	public map<string,string> getEquifaxConsumerReport(Vertex_Berry.Bsure_commonUtilityFunctioncall.customerinfobean objCustInfo,map<string,string> mapResultBook)
	{
		return null;
	}
	public map<string,string> getEquifaxConsumerReport(Vertex_Berry.Bsure_commonUtilityFunctioncall.ConsumerInfoBean objConsInfo,map<string,string> mapResultBook, map<string,string> mapConsumerAlias)
	{
	try{
		String strEquifaxConsumerURL='EquifaxConsumerURL';
		String strPOST='POST';
		if(mapConsumerAlias.isEmpty())
		{
			for(Vertex_Berry__BsureEquifax__c objBsureEq:Vertex_Berry__BsureEquifax__c.getAll().values())
				 {
				 	if(objBsureEq!=null && objBsureEq.Vertex_Berry__RequestType__c!=null && objBsureEq.Vertex_Berry__RequestType__c!='' && objBsureEq.Vertex_Berry__RequestType__c=='Consumer')
				 	{
					 	if(objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__AliasName__c!=null && objBsureEq.Vertex_Berry__AliasName__c!='')
						 	{
						 		mapConsumerAlias.put(objBsureEq.Vertex_Berry__NodeName__c,objBsureEq.Vertex_Berry__AliasName__c);
						 	}
				 	}
				 }
		}
		 
		String ConsumerEnquiryString='';
		ConsumerEnquiryString=getConsumerEnquiryString( objConsInfo);
	     //String newXML='<?xml version="1.0" encoding="UTF-8"?>  <EfxReceive tranID="XDIA" customerNumber="999ZZ57881" securityCode="@U2">  <CustomerInfo>  <CustomerNumber>999ZZ57881</CustomerNumber>  <SecurityCode>@U2</SecurityCode>  <CustomerCode>ISTS</CustomerCode>  <OutputFormat>02</OutputFormat>  <MultipleFileIndicator>F</MultipleFileIndicator>  <RawFileAppendIndicator>N</RawFileAppendIndicator>  </CustomerInfo>  <EfxRequest requestNumber="1">  <Subject>  <CustomerReferenceNumber></CustomerReferenceNumber>  <ECOAInquiryType>I</ECOAInquiryType>  <SubjectSSN>666650010</SubjectSSN>  <SubjectName>  <LastName>KXPYLWW</LastName>  <MiddleName>A</MiddleName>  <FirstName>LEWIS</FirstName>  </SubjectName>  </Subject>  <OptionalFeatures>  <BeaconFlag>Y</BeaconFlag>  <PaymentHistory24MonthFlag>Y</PaymentHistory24MonthFlag>  </OptionalFeatures>  <Address>  <StreetAddress>  <StreetNumber>401</StreetNumber>  <StreetName>BURNT TREE</StreetName>  <StreeType>WAY</StreeType>  </StreetAddress>  <City>ORANGE</City>  <State>VA</State>  <PostalCode>22960</PostalCode>  </Address>  <ModelData>  <ModelInfo modelNumber="5127" numberOfModelFields="0"/>  </ModelData>  </EfxRequest>  </EfxReceive>';
		 //String newTest='<?xml version="1.0" encoding="UTF-8"?><EfxReceive tranID="XDIA" customerNumber="999ZZ57881" securityCode="@U2"><CustomerInfo><CustomerNumber>999ZZ57881</CustomerNumber><SecurityCode>@U2</SecurityCode><CustomerCode>ISTS</CustomerCode><OutputFormat>02</OutputFormat><MultipleFileIndicator>F</MultipleFileIndicator><RawFileAppendIndicator>N</RawFileAppendIndicator></CustomerInfo><EfxRequest requestNumber="1"><Subject><CustomerReferenceNumber>XML TEST 1</CustomerReferenceNumber><ECOAInquiryType>I</ECOAInquiryType><SubjectSSN>666131862</SubjectSSN><SubjectName><LastName>KKCNW</LastName><MiddleName>RAYMOND</MiddleName><FirstName>JACK</FirstName></SubjectName></Subject><OptionalFeatures><BeaconFlag>Y</BeaconFlag><OnLineDirectoryFlag>Y</OnLineDirectoryFlag><ConsumerReferralLocationFlag>Y</ConsumerReferralLocationFlag><AlertContactFlag>Y</AlertContactFlag></OptionalFeatures><Address><StreetAddress><StreetNumber>1521</StreetNumber><StreetName>HIGHLAND</StreetName><StreeType>DR</StreeType></StreetAddress><City>TALLAHASSEE</City><State>FL</State><PostalCode>32317</PostalCode></Address><ModelData><ModelInfo modelNumber="5127" numberOfModelFields="0"/></ModelData></EfxRequest></EfxReceive>';
		// String NewString='<?xml version="1.0" encoding="UTF-8"?><EfxReceive version="6.0" tranID="XDIA" customerNumber="999ZZ57881" securityCode="@U2"><CustomerInfo><CustomerNumber>999ZZ57881</CustomerNumber><SecurityCode>@U2</SecurityCode><CustomerCode>ISTS</CustomerCode><OutputFormat>02</OutputFormat><MultipleFileIndicator>1</MultipleFileIndicator><RawFileAppendIndicator>N</RawFileAppendIndicator><PrintImageCDATAInclude>N</PrintImageCDATAInclude></CustomerInfo><EfxRequest requestNumber="1"><RequestInfo><CustomerReferenceNumber>XML TEST 1</CustomerReferenceNumber><ECOAInquiryType>I</ECOAInquiryType></RequestInfo><ConsumerSubjects><ConsumerSubject relationshipIdentifier="Primary"><PersonName><NamePrefix/><LastName>KKCNW</LastName><FirstName>JACK</FirstName><MiddleName>RAYMOND</MiddleName></PersonName><DateOfBirth/><SocialSecurityNumber>666131862</SocialSecurityNumber></ConsumerSubject></ConsumerSubjects><OptionalFeatures><BeaconFlag>Y</BeaconFlag><OnLineDirectoryFlag>Y</OnLineDirectoryFlag><ConsumerReferralLocationFlag>Y</ConsumerReferralLocationFlag><AlertContactFlag>Y</AlertContactFlag></OptionalFeatures><USAddress><StreetNumber>1521</StreetNumber><StreetName>HIGHLAND</StreetName><StreetType>DR</StreetType><City>TALLAHASSEE</City><State>FL</State><PostalCode>32317</PostalCode></USAddress><ModelData><ModelInfo modelNumber="5127" numberOfModelFields="0"/></ModelData></EfxRequest></EfxReceive>';
		  //ConsumerEnquiryString=NewString;
		 
	     //system.debug('ConsumerEnquiryString==='+ConsumerEnquiryString);
		/*Get Endpoint Url from Custom Setting*/
		BSureC_EquifaxURL= getConfigurationValues(strEquifaxConsumerURL).get(0);
		/* BSure_Configuration_Settings__c objConfigSetting=BSure_Configuration_Settings__c.getValues(strEquifaxConsumerURL);
		 if(objConfigSetting.Parameter_Value__c!=null)
		 {
			BSureC_EquifaxURL=objConfigSetting.Parameter_Value__c;
		 }*/
		//String strUsername='15900';String strPassword='ypPdqDLTadxgmZb6t9';
		String strEquifaxConUsername='EquifaxConUsername';
		String strEquifaxConPassword='EquifaxConPassword';
		String strEquifaxConSiteId='EquifaxConSiteId';
		String strEquifaxConServiceName='EquifaxConServiceName';
		String strUsername= getConfigurationValues(strEquifaxConUsername).get(0);
		String strPassword= getConfigurationValues(strEquifaxConPassword).get(0);
		String strSiteId= getConfigurationValues(strEquifaxConSiteId).get(0);
		String strServiceName= getConfigurationValues(strEquifaxConServiceName).get(0);
		HttpRequest reqCon = new HttpRequest();  
		//ConsumerEnquiryString=ConsumerEnquiryString.replace(' ','');
		String strBSureC_EquifaxURL=BSureC_EquifaxURL+'?site_id='+strSiteId+'&service_name='+strServiceName;
		Blob headerValue = Blob.valueOf(strUsername+':'+strPassword);
		String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
		reqCon.setHeader('Authorization', authorizationHeader);
		reqCon.setHeader('Content-Type', 'text/xml');
		String ContentLen=String.valueof(ConsumerEnquiryString.length());
		Reqresultconsumer = ContentLen;
		reqCon.setEndpoint(strBSureC_EquifaxURL);
		reqCon.setMethod(strPOST);
		reqCon.setTimeout(100000);
		reqCon.setHeader('Content-Length', ContentLen);
		reqCon.setBody(ConsumerEnquiryString);
		//ConsumerEnquiryString
		//system.debug('reqCon==='+reqCon);
		XmlStreamReader ConsumerReader;
		if(!Test.isRunningTest()){
			Http httpCon = new Http();
			HttpResponse resCon = httpCon.send(reqCon);
			ConsumerReader=resCon.getXmlStreamReader();
			system.debug('resCon===consumer='+resCon.getBody());
			Resresultconsumer = resCon.getBody();
		}
		
		mapResultBook=parseBooks(ConsumerReader, mapConsumerAlias);
		return mapResultBook;
		}catch(System.CalloutException e)
		 { 
		 	//System.debug('ERROR:' + e);
		 	String strException=e.getMessage();
			mapXmlBook.put('ConsumerTimeout','Equifax consumer server error : '+e.getMessage());
			notifyDevelopersOf(e);  
		 	return mapXmlBook;
		 }
	}
	public static void notifyDevelopersOf(Exception e) {
      try{
         String message = e.getMessage();
         String stacktrace = e.getStackTraceString();
         String exType = '' + e.getTypeName();
         String theTime = '' + System.now();
         String body = String.format('Hi,\n\nTime: {0}\nMessage: {1}\nStacktrace: {2}\n\n\nRegards,\nBsure Admin', new List<String>{ theTime, message, stacktrace });
         Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
         String[] toAddresses = new String[] {'kishorekumar.a@vertexcs.com'};
         mail.setToAddresses(toAddresses);
         mail.setSenderDisplayName('Apex error message');
         mail.setSubject('Error from Org : ' + UserInfo.getOrganizationName());
         mail.setPlainTextBody(body);
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      }Catch(Exception ex)
      {
        
      }
    }
	public String getConsumerEnquiryString(Vertex_Berry.Bsure_commonUtilityFunctioncall.ConsumerInfoBean objConsInfo)
      {
        String strBSureC_EquifaxConsumerMemberId='BSureC_EquifaxConsumerMemberId';
        String strEquifaxCustomerCode='EquifaxCustomerCode';
        String strConsumerMemberId='';
        String strConsumerSecurityCode='';
        Vertex_Berry__BSure_Configuration_Settings__c objConfigSetting=Vertex_Berry__BSure_Configuration_Settings__c.getValues(strBSureC_EquifaxConsumerMemberId);
        if(objConfigSetting.Vertex_Berry__Parameter_Key__c!=null && objConfigSetting.Vertex_Berry__Parameter_Key__c!='')
            strConsumerMemberId=objConfigSetting.Vertex_Berry__Parameter_Key__c;
        if(objConfigSetting.Vertex_Berry__Parameter_Value__c!=null && objConfigSetting.Vertex_Berry__Parameter_Value__c!='')
            strConsumerSecurityCode=objConfigSetting.Vertex_Berry__Parameter_Value__c;
        String EquifaxCustomerCode= getConfigurationValues(strEquifaxCustomerCode).get(0);
        
        String ConsumerEnquiryString='<?xml version="1.0" encoding="UTF-8"?>'+
            '<EfxReceive tranID="XDIA" customerNumber="'+strConsumerMemberId+'" securityCode="'+strConsumerSecurityCode+'">'+
            '<CustomerInfo>'+
            '<CustomerNumber>'+strConsumerMemberId+'</CustomerNumber>'+
            '<SecurityCode>'+strConsumerSecurityCode+'</SecurityCode>'+
            '<CustomerCode>'+EquifaxCustomerCode+'</CustomerCode>'+
            '<OutputFormat>02</OutputFormat>'+
            '<MultipleFileIndicator>F</MultipleFileIndicator>'+
            '<RawFileAppendIndicator>N</RawFileAppendIndicator>'+
            '</CustomerInfo>'+
                '<EfxRequest requestNumber="1">'+
                '<Subject>'+
                    '<CustomerReferenceNumber>XML TEST 1</CustomerReferenceNumber>'+
                    '<ECOAInquiryType>I</ECOAInquiryType>'+
                    '<SubjectSSN>'+objConsInfo.ConSubjectSSN+'</SubjectSSN>'+
                    '<SubjectName>'+
                        '<LastName>'+objConsInfo.ConLastName+'</LastName>'+
                        '<MiddleName>'+objConsInfo.ConMiddleName+'</MiddleName>'+
                        '<FirstName>'+objConsInfo.ConFirstName+'</FirstName>'+
                    '</SubjectName>'+
                    '<SubjectBirthDate>'+objConsInfo.ConSubjectBirthDate+'</SubjectBirthDate>'+
                '</Subject>'+
                    '<OptionalFeatures>'+
                    '<BeaconFlag>Y</BeaconFlag>'+
                    '<OnLineDirectoryFlag>Y</OnLineDirectoryFlag>'+
                    '<ConsumerReferralLocationFlag>Y</ConsumerReferralLocationFlag>'+
                    '<AlertContactFlag>Y</AlertContactFlag>'+
                '</OptionalFeatures>'+
                '<Address>'+
                    '<StreetAddress>'+
                        '<StreetNumber>'+objConsInfo.ConStreetNumber+'</StreetNumber>'+
                        '<StreetName>'+objConsInfo.ConStreetName+'</StreetName>'+
                        '<StreeType>'+objConsInfo.ConStreeType+'</StreeType>'+
                    '</StreetAddress>'+
                    '<City>'+objConsInfo.ConCity+'</City>'+
                    '<State>'+objConsInfo.ConState+'</State>'+
                    '<PostalCode>'+objConsInfo.ConPostalCode+'</PostalCode>'+
                '</Address>'+
                '<ModelData>'+
                '<ModelInfo modelNumber="5127" numberOfModelFields="0"/>'+
                '</ModelData>'+
                '</EfxRequest>'+
            '</EfxReceive>';
            
            return ConsumerEnquiryString;
      }
    public static Map<String, List<String>> configSettingsMap {
        get
        {
            if (configSettingsMap == null || configSettingsMap.size() ==0)
            {
                configSettingsMap  = new Map<String, List<String>>();
                for( BSure_Configuration_Settings__c configSetting : BSure_Configuration_Settings__c.getAll().values() )
                {
                    if(configSetting.Parameter_Key__c != null)
                        {
                            List<String> valuesList = configSettingsMap.get(configSetting.Parameter_Key__c.toLowerCase());
                            
                            if(valuesList == null) 
                            {
                                valuesList = new List<String>();                
                            } 
                            valuesList.add(configSetting.Parameter_Value__c);
                            configSettingsMap.put(configSetting.Parameter_Key__c.toLowerCase(), valuesList);
                        }
                }
            }
            return configSettingsMap;
        }        
    }
	public static List<String> getConfigurationValues(String ParameterName)
    {        
        if (ParameterName == null)
        {
            return new List<string>();
        }
        if (configSettingsMap.get(ParameterName.tolowercase()) != null)
        {
            return configSettingsMap.get(ParameterName.tolowercase());
        }
        return new List<string>();
    }
    public String getEnquiryString(Vertex_Berry.Bsure_commonUtilityFunctioncall.customerinfobean objCustInfo, map<string,string> mapProductCodes)
      {
        ///set<String> setProductCodes=objCustInfo.setProductCodes;
        String strAddressLine1=objCustInfo.strAddressLine1;
        String strCity=objCustInfo.strCity;
        String strState=objCustInfo.strState;
        String strPostalCode =objCustInfo.strPostalCode;
        String strBusinessName=objCustInfo.strBusinessName;
        String strTaxid=objCustInfo.strTaxid;
        String EnquiryString='';
        String strEqMemberId='';
        String strEqSecurityCode='';
        String ProductTags='';
        String strBSureC_EquifaxMemberId='BSureC_EquifaxMemberId';
        Vertex_Berry__BSure_Configuration_Settings__c objConfigSetting= Vertex_Berry__BSure_Configuration_Settings__c.getValues(strBSureC_EquifaxMemberId);
        if(objConfigSetting.Vertex_Berry__Parameter_Key__c!=null && objConfigSetting.Vertex_Berry__Parameter_Key__c!='')
            strEqMemberId=objConfigSetting.Vertex_Berry__Parameter_Key__c;
        if(objConfigSetting.Vertex_Berry__Parameter_Value__c!=null && objConfigSetting.Vertex_Berry__Parameter_Value__c!='')
            strEqSecurityCode=objConfigSetting.Vertex_Berry__Parameter_Value__c;
        ProductTags=getProductCodeTags(mapProductCodes);
        
        if(strEqSecurityCode!=null && strEqSecurityCode!='' && strEqMemberId!=null && strEqMemberId!=''){
            EnquiryString='<?xml version="1.0" encoding="UTF-8"?>'
                            +'<EfxCommercialRequest version="5.0" securityCode="'+strEqSecurityCode+'" customerNumber="'+strEqMemberId+'" tranID="XSOF" efxInternalTranId="XXXXXXXXX" customerReference="HTTP Test" serviceCode="SB1">'
                            +'<CustomerSecurityInfo>'
                            + ProductTags   
                            +'<Channel>'
                                +'<IdNumber>3</IdNumber>'
                            +'</Channel>'
                            +'</CustomerSecurityInfo>'
                            +'<StandardRequest>'
                                +'<Folder>'
                                    +'<IdTrait>'
                                                +'<AddressTrait>';
                                                
                                                        if(strAddressLine1!=null && strAddressLine1!='')
                                                         EnquiryString += '<AddressLine1>' +strAddressLine1+ '</AddressLine1>';
                                                        if(strCity!=null && strCity!='')    
                                                         EnquiryString += '<City>' +strCity+ '</City>';
                                                        if(strState!=null && strState!='')
                                                         EnquiryString += '<State>' +strState+ '</State>';
                                                        if(strPostalCode!=null && strPostalCode!='')
                                                         EnquiryString += '<PostalCode>' +strPostalCode+ '</PostalCode>';
                                                         
                                        EnquiryString +='</AddressTrait>'
                                        +'<CompanyNameTrait>';
                                            if(strBusinessName!=null && strBusinessName!='')
                                            EnquiryString += '<BusinessName>' +strBusinessName+ '</BusinessName>'
                                        +'</CompanyNameTrait>'
                                    +'</IdTrait>';
                             if(strTaxid!=null && strTaxid!=''){
                                EnquiryString += '<PrincipalTrait>'
                                                    +'<Folder>'
                                                        +'<IdTrait>'
                                                            +'<IdNumberTrait>'
                                                                +'<IdNumber>'+strTaxid+'</IdNumber>'
                                                                +'<IdNumberIndicator>Tax ID</IdNumberIndicator>'
                                                            +'</IdNumberTrait>'
                                                        +'</IdTrait>'
                                                     +'</Folder>'
                                                +'</PrincipalTrait>';
                            }
                            EnquiryString += '</Folder>'
                            +'</StandardRequest>' 
                            +'</EfxCommercialRequest>';
           }
         //system.debug('EnquiryString===='+EnquiryString);               
         return EnquiryString;
      } 
    public String getProductCodeTags(map<string,string> mapProductCodes)
      {
        String strProductCode='';
        String ProductTag='';
        String strSCR='SCR';
        String strRPT='RPT';
        
        //setProductCodes.add('Business Delinquency Score');
        for(String strReportName:mapProductCodes.keyset())
        {   
            /*BSure_Configuration_Settings__c objConfigSetting=BSure_Configuration_Settings__c.getValues(str);
            if(objConfigSetting.Parameter_Value__c!=null)
            strProductCode=objConfigSetting.Parameter_Value__c;
            //strProductCode = BSureC_CommonUtil.getConfigurationValues(str); */
            strProductCode=mapProductCodes.get(strReportName);
            if(strProductCode!=null && strProductCode.startsWith(strSCR))
            {
                strProductCode = strProductCode.substring(3);
                if(strProductCode!=null && strProductCode!='')
                {
                    ProductTag += '<ProductCode code="'+strProductCode+'" name="SCR">'+strReportName+'</ProductCode>';
                
                }
            } else if(strProductCode!=null &&  strProductCode.startsWith(strRPT))
            {
                strProductCode = strProductCode.substring(3);
                if(strProductCode!=null && strProductCode!='')
                {
                    ProductTag += '<ProductCode code="'+strProductCode+'" name="RPT">'+strReportName+'</ProductCode>';
                
                }
            }
            
        }
        return ProductTag;
      }  
    public map<string,string> parseBooks(XmlStreamReader reader, map<string,string> mapAlias) 
	{
	  try{
	     Integer i=1,j=1,k=1;
	     String AlertCodeDescription='';
	     String AlertKey='';
	     String ErrorCode='';
	     String ApplicationError='';
	     while(reader.hasNext() ) 
	     {
	     	String strNewKey='';
	     	if (reader.getEventType() == XmlTag.START_ELEMENT) 
	     	{
	     		if('ProcessingError' == reader.getLocalName())
	     		{
	     			ErrorCode = reader.getAttributeValue(null, 'code')+':'+reader.getAttributeValue(null, 'description');
	     			mapXmlBook.put('ProcessingError',ErrorCode);
	     			break;
	     		}
	     		else{
	     			String CurrentTag=reader.getLocalName();
		     		if(mapAlias.containskey(CurrentTag))
		     		{
			     	  	for(String strEq:mapAlias.keyset())
			     	  	{
			     	  		if (strEq == reader.getLocalName())
			         	    {
			         	    	String Value ='';
			         	    	if ('AlertCode' == strEq || 'ErrorCode'==strEq) 
			            		{
			            			Value = getValueFromTag(reader);
			            			if(j==1){
			            				AlertCodeDescription=Value;
			            			}else{
			            				AlertCodeDescription +='\n'+Value;
			            			}
				                    j++;
				                    break;
			            	    }else if ('AlertDescription' == strEq || 'ErrorDescription'==strEq) 
			            		{
			            			AlertKey=mapAlias.get(strEq);
			            			Value = getValueFromTag(reader);
			            			AlertCodeDescription +=':'+Value;
				                    i++;
				                    break;
			            	     }else if ('ScoreData' == strEq) 
			            		 {  
			            		 	String ScoreType='';
			            		 	String strCreditScoreKey='';
			            			ScoreType = reader.getAttributeValue(null, 'scoreName');
			            			strCreditScoreKey=mapAlias.get(ScoreType);
			            			if(strCreditScoreKey!='' && strCreditScoreKey!=null)
			            			{
				            			Value='';
				            			Value = reader.getAttributeValue(null, 'score');
				            			if((Value!='' && Value!=null && !Value.isNumericSpace()) || Value=='' || Value==null )
				            			{
				            				Value='0';
				            			}
					                    mapXmlBook.put(strCreditScoreKey,Value);
					                    
					                    //Test Values for PCS
					                    /*if(Value!=null && Value!=''){
						                    Integer intValue=Integer.valueOf(Value)+300;
						                    mapXmlBook.put('PCS',intValue+'');
					                    }else{
											mapXmlBook.put('PCS',Value);			                    	
					                    }*/
					                    Value='';
					                    Value = reader.getAttributeValue(null, 'riskLevel');
					                    mapXmlBook.put(strCreditScoreKey+'riskLevel',Value);
			            			}
				                    break;
			            	     }else if ('ScoreAttribute' == strEq) 
			            		 {  
			            		 	String AttriType='';
			            			AttriType = reader.getAttributeValue(null, 'Name');
			            			AttriType = AttriType.replace(' ','');
			            			Value='';
			            			strNewKey=mapAlias.get(AttriType);
			            			if(strNewKey!=null && strNewKey!='')
			            	     	{
				            			Value = getValueFromTag(reader);
					                    mapXmlBook.put(AttriType,Value);
			            	     	}
				                    break;
			            	     }
			            	     else if ('ScoreReason' == strEq || 'RejectCode'==strEq || 'Error'==strEq || 'ErrorType'==strEq || 'HitCode'==strEq) 
			            		 {
			            		 	String ReasonCode='';
			            		 	String ReasonDescription='';
			            		 	ReasonCode = reader.getAttributeValue(null, 'code');
			            		 	ReasonDescription = reader.getAttributeValue(null, 'description');
			            		 	strNewKey=mapAlias.get(strEq);
			            		 	Value=ReasonCode+':'+ReasonDescription;
			            		 	if(!mapXmlBook.containsKey(strNewKey))
			            		 	{
			            		 		mapXmlBook.put(strNewKey,Value);
			            		 	}else{
			            				Value +='\n'+mapXmlBook.get(strNewKey);
			            				mapXmlBook.put(strNewKey,Value);
			            			}
			            			 
			            			break;
			            		 }else{
			            	     	strNewKey=mapAlias.get(strEq);
			            	     	if(strNewKey!=null && strNewKey!='' && !mapXmlBook.containsKey(strNewKey) )
			            	     	{
						                Value = getValueFromTag(reader);
						                mapXmlBook.put(strNewKey,Value);
			            	     	}
					                break;
			            		}
			                } 
			     	    }
		     		}
		     	    
		     	}
	     	 }
	     	 reader.next();
	     }
	     if(AlertCodeDescription!=null && AlertCodeDescription!='' && AlertKey!='' && AlertKey!=null)
	     {
	     	mapXmlBook.put(AlertKey,AlertCodeDescription);
	     }
	     if(mapXmlBook.ContainsKey('PCS') && !mapXmlBook.IsEmpty())
	     {
	     	String Value=mapXmlBook.get('PCS');
	     	if((Value!='' && Value!=null && !Value.isNumericSpace()) || Value=='' || Value==null)
			{
				Value='0';
				mapXmlBook.put('PCS',Value);
			}
	     	
	     }
	     //System.debug(ApplicationError+'ObjCreditBook==='+mapXmlBook);
	     	return mapXmlBook;
     
	}catch(XmlException e)
	{
		//System.debug(e.getStackTraceString());
		mapXmlBook.put('XmlParsingException','Exception during Xml parsing error : '+e.getMessage());
		notifyDevelopersOf(e);  
		return mapXmlBook;
		
	}
   }
   public  String getValueFromTag(XmlStreamReader reader) 
	 {
    	String returnvalue='';
        reader.setCoalescing(true);
        while(reader.hasNext()) 
        {
            if (reader.getEventType() == XmlTag.END_ELEMENT) 
            {
               	break;
            } else if (reader.getEventType() == XmlTag.CHARACTERS) 
            {
              //system.debug('reader####'+reader.getText());
                     returnvalue=reader.getText();// reader.getAttributeValue('', 'Location');
              //system.debug('returnvalue=====@@@@@========'+returnvalue);
            }
        reader.next();
        }
      	//system.debug('returnvalue==========='+returnvalue);
         return returnvalue;
       }
}