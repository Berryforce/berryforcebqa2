global class BillingSubsequentPrintScheduler Implements Schedulable {
   public Interface BillingSubsequentPrintSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('BillingSubsequentPrintSchedulerHndlr');
        if(targetType != null) {
          BillingSubsequentPrintSchedulerInterface obj = (BillingSubsequentPrintSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}