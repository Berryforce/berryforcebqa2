@IsTest
private Class lockCloneCreateOrderControllerTest
{

  static testMethod void testlockCloneCreateOrderController()
  {
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.New_Print_Bill_Date__c=System.Today();
        objDirEd.Bill_Prep__c=System.Today();
        insert objDirEd;
        
     //   Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
     //   insert objDirEd1;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
             newProduct.Product_Type__c = CommonMessages.printMediaType;
            newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
              newProduct.Family='Print';
            if(x==0){
                newProduct.Family='Digital';
            }
          
      newProduct.Vendor__c =CommonMessages.prdVendorYP;
      newProduct.ProductCode='UDAC';
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        
        list<PricebookEntry> lstPBE = new list<PricebookEntry>();
        for(Product2 iterator : lstProduct) {
        
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstPBE.add(pbe);
        }
        insert lstPBE;
                
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        //list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        for(Product2 iterator : lstProduct) {
            Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
            objDPM.Product2__c = iterator.Id; 
        }
        insert lstDPM;
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;      
        newOpportunity.StageName='Closed UTC';
        insert newOpportunity;
    
    Listing__c newListing=TestMethodsUtility.generateListing();
    insert newListing;
    
      Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
      newOrder.Billing_Anniversary_Date__c=System.Today();
      update newOrder;
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
             newOrLI.Parent_ID__c='2434-4545';
             newOrLI.Listing__c=newListing.Id;
        List<Order_Line_Items__c> listnewOrli = new List<Order_Line_Items__c>();
        listnewOrli.add(newOrLI);
     //   insert listnewOrli;    
        
        
    List<Modification_Order_Line_Item__c> MOLIList = new List<Modification_Order_Line_Item__c>();
    Modification_Order_Line_Item__c mol = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI.id);
   // mol.Directory_Section__c = objDS.Id;
    mol.Order_Group__c = newOrderSet.Id;
    mol.Action_Code__c = 'Renew';
     mol.Product_Type__c='SEO';
    mol.Product2__c =lstProduct[0].id;
    MOLIList.add(mol);
       insert MOLIList;
    
    
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        Integer i=0;
        for(PricebookEntry iterator : lstPBE) {
            i++;
            OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
            objOLI.PricebookEntryId = iterator.Id;
            objOLI.Billing_Duration__c = 12;
            objOLI.Directory__c = objDir.Id;
            objOLI.Directory_Edition__c = objDirEd.Id;
            objOLI.Full_Rate__c = 30.00;
            objOLI.UnitPrice = 30.00;
            objOLI.Package_ID__c = '123456';
            objOLI.Billing_Partner__c = objOLI.Id;
            objOLI.OpportunityId = newOpportunity.Id;
            objOLI.Directory_Heading__c = objDH.Id;
            objOLI.Directory_Section__c = objDS.Id;
            objOLI.Renewals_Action__c=CommonMessages.renewRenewalAction;
            objOLI.Parent_ID__c ='2014-8235_tst'+i;
            //objOLI.Parent_ID_of_Addon__c='2014-8235_tst1';
            objOLI.Effective_Date__c = System.Today();
            objOLI.Is_P4P__c =true;
            lstOLI.add(objOLI);
            mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;
        
        pymt__PaymentX__c initialpayment = new pymt__PaymentX__c(name='Initial Payment');
        insert initialpayment;
    
     Test.startTest();
        ApexPages.currentPage().getParameters().put('Id', newOpportunity.Id);
        ApexPages.currentPage().getParameters().put('paymentid', initialpayment.Id);
        ApexPages.StandardController cont = new ApexPages.StandardController(newOpportunity);
        lockCloneCreateOrderController lockOrderCreate = new lockCloneCreateOrderController(cont);
        PageReference orderpage31 = lockOrderCreate.onLoad();
        
        lockCloneCreateModifyOrderController lockOrderModify = new lockCloneCreateModifyOrderController(cont);
        //added by Mythili 
     //   lockOrderModify.newOLI =listnewOrli;
       lockOrderModify.renewMOLI=MOLIList;
        //till here
       // lockOrderModify.modifyOLIProcess(newOpportunity,lstOLI,newOrder,newOrderSet);
      //  lockOrderModify.newOLIProcess(newOpportunity,lstOLI,newOrder,newOrderSet,listnewOrli);
        
        PageReference orderpage1 = lockOrderModify.onLoad();
        
        Order__c order= lockCloneHandlerController.newOrder(newOpportunity);
       
      Test.stopTest();
      
  }

  static testMethod void testlockCloneCreateOrderControllerV1()
  {
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account(Statement_Suppression__c=true);
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;
        
     //   Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
      //  insert objDirEd1;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
         Berry_Cares_Certificate__c bcc=new Berry_Cares_Certificate__c();
                bcc.Is_Redeemed__c=true;
                bcc.Redeemable__c=false;
                bcc.Account__c=lstAccount[1].Id;
                insert bcc;
        
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
             if(x==0){
                newProduct.Name='Berry Cares Certificate';
            }
            newProduct.Product_Type__c = 'SEO';
            newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
      newProduct.Vendor__c =CommonMessages.prdVendorYP;
      //added by Mythili
      newProduct.ProductCode='UDAC';
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        
        //Pricebook2 newPriceBook = getStandardPricebookId();
        list<PricebookEntry> lstPBE = new list<PricebookEntry>();
        for(Product2 iterator : lstProduct) {
        
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstPBE.add(pbe);
        }
        insert lstPBE;
        
        /*list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        
        for(Product2 iterator : lstProduct) {
            Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
            objDPM.Product2__c = iterator.Id;
        }
        insert lstDPM;*/
        
        Opportunity newOpportunity1 = TestMethodsUtility.generateOpportunity('new');
        newOpportunity1.AccountId = newAccount.Id;
        newOpportunity1.Pricebook2Id = newPriceBook.Id;
        newOpportunity1.Billing_Contact__c = newContact.Id;
        newOpportunity1.Signing_Contact__c = newContact.Id;
        newOpportunity1.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity1.Payment_Method__c = CommonMessages.telcoPaymentMethod;      
        newOpportunity1.StageName='Closed UTC';
        insert newOpportunity1;
    
      Order__c newOrder1 = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet1 = TestMethodsUtility.createOrderSet(newAccount, newOrder1, newOpportunity1);
        Order_Line_Items__c newOrLI1 = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity1, newOrder1, newOrderSet1);
        
        List<Order_Line_Items__c> listnewOrli = new List<Order_Line_Items__c>();
        listnewOrli.add(newOrLI1);
        
    List<Modification_Order_Line_Item__c> MOLIList1 = new List<Modification_Order_Line_Item__c>();
    Modification_Order_Line_Item__c mol1 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI1.id);
   // mol1.Directory_Section__c = objDS.Id;
    mol1.Order_Group__c = newOrderSet1.Id;
    mol1.Parent_ID__c='2014-8235_tst11';
    mol1.Parent_ID_of_Addon__c='2015-8235_tst12';
    mol1.Action_Code__c = 'Renew';
    mol1.Product_Type__c='SEO';
    mol1.Product_code__c='4345235';
    mol1.Product2__r=lstProduct[0];
    
    //mol1.Parent_ID_of_Addon__c='2014-8235_tst1';
   // mol1.Directory__c=objDir.id;
    MOLIList1.add(mol1);
    insert MOLIList1; 
    
    
        list<OpportunityLineItem> lstOLI1 = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE1 = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
            OpportunityLineItem objOLI1 = TestMethodsUtility.generateOpportunityLineItem();
            objOLI1.PricebookEntryId = iterator.Id;
            objOLI1.Billing_Duration__c = 12;
            objOLI1.Directory__c = objDir.Id;
            objOLI1.Directory_Edition__c = objDirEd.Id;
            objOLI1.Full_Rate__c = 30.00;
            objOLI1.UnitPrice = 30.00;
            objOLI1.Package_ID__c = '123456';
            objOLI1.Billing_Partner__c = objOLI1.Id;
            objOLI1.OpportunityId = newOpportunity1.Id;
            objOLI1.Directory_Heading__c = objDH.Id;
            objOLI1.Directory_Section__c = objDS.Id;
      objOLI1.Renewals_Action__c='New';
      //objOLI1.Parent_ID__c ='2014-8235_tst2';
      objOLI1.Parent_ID_of_Addon__c='2014-8235_tst1';
      objOLI1.Effective_Date__c = System.Today().addDays(2);
      objOLI1.Original_Line_Item_ID__c = newOrLI1.id;
     
      objOLI1.P4P_Billing__c = true;
            lstOLI1.add(objOLI1);
            mapPBE1.put(iterator.Id, iterator);
        }
        OpportunityLineItem objOLINew=lstOLI1[0];
        objOLINew.BCC__c=bcc.Id;
        objOLINew.Core_Migration_ID__c='32452345';
        objOLINew.Effective_Date__c=System.Today();
        lstOLI1.set(0,objOLINew);
        insert lstOLI1;
        
        
        
        System.debug('listnewOrli[0].Opportunity_line_Item_id__c '+listnewOrli[0].Opportunity_line_Item_id__c);
        System.debug('lstOLI1[0].Id '+lstOLI1[0].Id);
        listnewOrli[0].Opportunity_line_Item_id__c = lstOLI1[0].Id;
        pymt__PaymentX__c initialpayment = new pymt__PaymentX__c(name='Initial Payment');
        insert initialpayment;
         
        List<Digital_Product_Requirement__c> lstDFF = new List<Digital_Product_Requirement__c >();
        Digital_Product_Requirement__c DFF = TestMethodsUtility.generateDataFulfillmentForm('Print Graphic');
        DFF.Account__c = lstAccount[1].Id;
        DFF.Contact__c = newContact.Id;
        DFF.Proof_Contact__c = newContact.Id;
        
        DFF.Heading_1__c = objDH.Id;
        DFF.Heading_2__c = objDH.Id;
        DFF.Heading_3__c = objDH.Id;
        DFF.Heading_4__c = objDH.Id;
        DFF.Heading_5__c = objDH.Id;
        DFF.OrderLineItemID__c=newOrLI1.Id;
        DFF.OpportunityID__c=newOpportunity1.Id;
        DFF.Competitor_1__c = 'test';
        DFF.Competitor_2__c = 'test';
        DFF.Competitor_3__c = 'test';
        DFF.Employee_1__c = 'test';
        DFF.Employee_2__c = 'test';
        DFF.Employee_3__c = 'test';   
        DFF.Affiliation_1__c = 'test';
        DFF.Affiliation_2__c = 'test';   
        DFF.Affiliation_3__c = 'test';
        DFF.Affiliation_4__c = 'test';         
        DFF.Also_Known_As_aka_1__c = 'test';
        DFF.Also_Known_As_aka_2__c = 'test';
        DFF.Also_Known_As_aka_3__c = 'test';   
        DFF.Also_Known_As_aka_4__c = 'test';
        DFF.Also_Known_As_aka_5__c = 'test'; 
        DFF.CpnDesc_CstLnkUrl_CstLnkTxt__c = true;
        DFF.add_ons__c = 'YPCP';
        DFF.coupon_description__c = 'test';
        DFF.UDAC__c='test';
        DFF.ModificationOrderLineItem__c=MOLIList1[0].Id;
        lstDFF.add(DFF);
        insert lstDFF;
     
     Date dtEffectiveDate = Date.Today();
     
        
         Test.startTest();
        ApexPages.currentPage().getParameters().put('Id', newOpportunity1.Id);
        ApexPages.currentPage().getParameters().put('paymentid', initialpayment.Id);
        ApexPages.StandardController cont = new ApexPages.StandardController(newOpportunity1);
        lockCloneCreateOrderController lockOrderCreate1 = new lockCloneCreateOrderController(cont);
        PageReference orderpage32 = lockOrderCreate1.onLoad();
        
        lockCloneCreateModifyOrderController lockOrderModify1 = new lockCloneCreateModifyOrderController(cont);
        lockOrderModify1.modifyOLIProcess(newOpportunity1,lstOLI1,newOrder1,newOrderSet1, new Map<String,String>());
        
       List<Modification_Order_Line_Item__c> moli=[Select m.zero_Cancel_Fee_Requested__c, m.zero_Cancel_Fee_Approved__c, m.line_Status__c, 
        m.Vendor_Product_Key__c, m.UnitPrice__c, m.Total_Prorated_Days_for_contract__c, m.Total_Prorate__c, m.Telco__c, m.Talus_Subscription_ID__c, 
        m.Talus_Go_Live_Date__c, m.Talus_DFF_Id__c, m.SystemModstamp, m.Subtotal__c, m.Status__c, m.Sent_to_fulfillment_on__c, 
        m.Seniority_Date__c, m.Quote_Signing_Method__c, m.Quote_Signed_Date__c, m.Quantity__c, m.Prorate_Stored_Value__c, m.Prorate_Credit__c,
        m.Prorate_Credit_Days__c, m.Product_Type__c, m.Product_Code__c, m.Product2__c, m.MOLI_Payment_Method__c, m.Payment_Duration__c, 
        m.Patch_Complete_Date__c, m.Parent_Line_Item__c, m.Parent_ID_of_Addon__c, m.Parent_ID__c, m.Package_ID__c, m.Package_ID_External__c, 
        m.P4P_Price_Per_Click_Lead__c, m.P4P_Billing__c, m.Order__c, m.Order_Line_Total__c, m.Order_Line_Item__c, m.Order_Group__c,
        m.Order_Anniversary_Start_Date__c, m.Opportunity__c, m.Name, m.Migration_Directory_Heading4__c, m.Migration_Directory_Heading3__c, 
        m.Migration_Directory_Heading2__c, m.Migration_Directory_Heading1__c, m.Media_Type__c, m.MOLI_DM_Opportunity_Close_Date__c, m.MOLI_Berry_Cares_Certificate_ID__c, 
        m.MOLI_BCC_Redeemed__c, m.MOLI_BCC_Name__c, m.MOLI_BCC_Duration__c, m.MOLI_BCC_Applied__c, m.Listing_Section__c, m.Listing_Heading__c,
        m.ListPrice__c, m.LastModifiedDate, m.LastModifiedById, m.Is_P4P__c, m.IsUDACChange__c, m.IsDeleted, m.Inactive__c, m.Id, m.Geo_Type__c, m.Geo_Code__c, 
        m.FulfillmentDate__c, m.Effective_Date__c, m.Distribution_Area__c, m.Discount__c, m.Discount_Rate__c, m.Directory_Listing__c, m.Directory_Heading__c, m.Digital_Product_Requirement__c, 
        m.Description__c, m.Current_Rate__c, m.CreatedDate, m.CreatedById, m.Core_Opportunity_Line_ID__c, m.Completed__c, m.Canvass__c, m.Cancellation__c, m.Cancelation_Fee__c, 
        m.Billing_Start_Date__c, m.Billing_Partner__c, m.Billing_Frequency__c, m.Billing_End_Date__c, m.Billing_Contact__c, m.Action_Code__c,
        m.Account__c,m.Product2__r.Product_Type__c,(Select id,ModificationOrderLineItem__r.Product2__r.Product_Type__c,ModificationOrderLineItem__r.Product2__r.ProductCode,Data_Fulfillment_Form__c,Plan_ID__c,cost__c,budget_total__c,setup_fee__c,Clicks_Contracted__c,Product_Type__c,product_type_id__c,desired_service_term__c from Data_Fulfillment_Forms__r) From Modification_Order_Line_Item__c m where id IN :MOLIList1]; 
        
        lockOrderModify1.createModifyOLIDFF(MOLIList1,newOpportunity1);
        lockCloneCreateModifyOrderController.DFFClone(listnewOrli,lstOLI1,new map<Id, Id>());
        PageReference Modifyorderpage = lockOrderModify1.onLoad();
        YPC_AddOn__c objYPCAddon = lockOrderModify1.newYPCAddOn('New',lstDFF[0].id,dtEffectiveDate,'CSTLK','');
        
        lockCloneCreateModifyOrderController.FindAddOnProductForDFFAndCreateDFF1(moli,newOpportunity1,'TAFSDFSDGG');
        lockCloneCreateModifyOrderController.cancelProcess(lstOLI1,listnewOrli);
        
      Test.stopTest();
      
  }
  
  static testMethod void testlockCloneCreateOrderControllerV2()
  {
    system.debug('Entering testlockCloneCreateOrderControllerV2');
        list<Account> lstAccount3 = new list<Account>();
        lstAccount3.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount3.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount3.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount3;  
        Account newAccount3 = new Account();
        Account newPubAccount3 = new Account();
        Account newTelcoAccount3 = new Account();
        for(Account iterator : lstAccount3) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount3 = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount3 = iterator;
            }
            else {
                newTelcoAccount3 = iterator;
            }
        }
        
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount3.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Contact newContact = TestMethodsUtility.createContact(newAccount3.Id);
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount3.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount3.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;
        
      //  Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
       // insert objDirEd1;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Product_Type__c = CommonMessages.digitalMediaType;
            newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
            newProduct.Vendor__c =CommonMessages.prdVendorYP;
             
            if(x==0){
                newProduct.Family='Digital';
                newProduct.ProductCode='ABC';
                newProduct.Print_Specialty_Product_Type__c=CommonMessages.ELProd;
            }
            if(x==1){
                newProduct.Product_Type__c='Spotzer Website';
            }
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        
        list<PricebookEntry> lstPBE = new list<PricebookEntry>();
        for(Product2 iterator : lstProduct) {
        
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstPBE.add(pbe);
        }
        insert lstPBE;
        
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
       // list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
      
        for(Product2 iterator : lstProduct) {
            Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
            objDPM.Product2__c = iterator.Id;
        }
        insert lstDPM;
        
        Opportunity newOpportunity3 = TestMethodsUtility.generateOpportunity('new');
        newOpportunity3.AccountId = newAccount3.Id;
        newOpportunity3.Pricebook2Id = newPriceBook.Id;
        newOpportunity3.Billing_Contact__c = newContact.Id;
        newOpportunity3.Signing_Contact__c = newContact.Id;
        newOpportunity3.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity3.Payment_Method__c = CommonMessages.telcoPaymentMethod;      
        newOpportunity3.StageName='Closed UTC';
        insert newOpportunity3;
           
      Order__c newOrder3 = TestMethodsUtility.createOrder(newAccount3.Id);
        Order_Group__c newOrderSet3 = TestMethodsUtility.createOrderSet(newAccount3, newOrder3, newOpportunity3);
        Order_Line_Items__c newOrLI3 = TestMethodsUtility.createOrderLineItem(newAccount3, newContact, newOpportunity3, newOrder3, newOrderSet3);
        Order_Line_Items__c newOrLI4 = TestMethodsUtility.createOrderLineItem(newAccount3, newContact, newOpportunity3, newOrder3, newOrderSet3);
        newOrLI4.Product2__c =lstProduct[1].Id;
        List<Order_Line_Items__c> oliList=new List<Order_Line_Items__c>();
       newOrLI3.Product2__c =lstProduct[0].Id;
       newOrLI3.Parent_ID_of_Addon__c='5465476578';
       newOrLI3.Parent_ID__c='354657667';
       oliList.add(newOrLI3);
       oliList.add(newOrLI4);
      //  insert oliList;
        
    List<Modification_Order_Line_Item__c> MOLIList3 = new List<Modification_Order_Line_Item__c>();
    Modification_Order_Line_Item__c mol3 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI3.id);
    //mol3.Directory_Section__c = objDS.Id;
    mol3.Order_Group__c = newOrderSet3.Id;
    mol3.Parent_ID__c='2014-8235_tst11';
   // mol3.Parent_ID_of_Addon__c='2015-8235_tst12'; 
   mol3.Action_code__c='Renew';
    MOLIList3.add(mol3);
    insert MOLIList3;
    
     List<Modification_Order_Line_Item__c> MOLIList3New = new List<Modification_Order_Line_Item__c>();
    Modification_Order_Line_Item__c mol4 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI4.id);
    //mol3.Directory_Section__c = objDS.Id;
    mol4.Order_Group__c = newOrderSet3.Id;
   // mol3.Parent_ID__c='2014-8235_tst11';
   // mol3.Parent_ID_of_Addon__c='2015-8235_tst12'; 
   mol4.Action_code__c='Renew';
    MOLIList3New.add(mol4);
    insert MOLIList3New;
    
    
        list<OpportunityLineItem> lstOLI3 = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE3 = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
            OpportunityLineItem objOLI3 = TestMethodsUtility.generateOpportunityLineItem();
            objOLI3.PricebookEntryId = iterator.Id;
            objOLI3.Billing_Duration__c = 12;
            objOLI3.Directory__c = objDir.Id;
            objOLI3.Directory_Edition__c = objDirEd.Id;
            objOLI3.Full_Rate__c = 30.00;
            objOLI3.UnitPrice = 30.00;
            objOLI3.Package_ID__c = '123456';
            objOLI3.Billing_Partner__c = objOLI3.Id;
            objOLI3.OpportunityId = newOpportunity3.Id;
            objOLI3.Directory_Heading__c = objDH.Id;  
            objOLI3.Directory_Section__c = objDS.Id;
            objOLI3.Renewals_Action__c=CommonMessages.newRenewalAction;
            //objOLI3.Parent_ID__c ='2014-8235_tst3';
            objOLI3.Parent_ID_of_Addon__c='2014-8235_tst1';
            objOLI3.Effective_Date__c=Date.newInstance(2014, 2, 1);
            lstOLI3.add(objOLI3);
            mapPBE3.put(iterator.Id, iterator);
        }
        insert lstOLI3;
        
        pymt__PaymentX__c initialpayment = new pymt__PaymentX__c(name='Initial Payment');
        insert initialpayment;
    
     Test.startTest();
        ApexPages.currentPage().getParameters().put('Id', newOpportunity3.Id);
        ApexPages.currentPage().getParameters().put('paymentid', initialpayment.Id);
        ApexPages.StandardController cont = new ApexPages.StandardController(newOpportunity3);
        lockCloneCreateOrderController lockOrderCreate3 = new lockCloneCreateOrderController(cont);
        PageReference orderpage33 = lockOrderCreate3.onLoad();
        
        lockCloneCreateModifyOrderController lockOrderModify3 = new lockCloneCreateModifyOrderController(cont);
    lockOrderModify3.modifyOLIProcess(newOpportunity3,lstOLI3,newOrder3,newOrderSet3,new Map<String,String>());
   lockOrderModify3.newMOLI=MOLIList3New;
    lockOrderModify3.newOLIProcess(newOpportunity3,lstOLI3,newOrder3,newOrderSet3,oliList);
     lockCloneCreateModifyOrderController.FindAddOnProductForDFFAndCreateDFF1(MOLIList3,newOpportunity3,'sdgfsdgrwg');
       lockCloneCreateModifyOrderController.FindAddOnProductForDFFAndCreateDFF1(MOLIList3New,newOpportunity3,'dfwefwret');
        PageReference orderpage1 = lockOrderModify3.onLoad();
        
        List<Order_Line_Items__c> oliListNew=[Select Id, Account__c, Order_Anniversary_Start_Date__c,Directory_Heading__r.Directory_Heading_Name__c, Account__r.phone,Account__r.BillingState,Account__r.BillingPostalCode,Account__r.BillingStreet,Account__r.BillingCity,
                Opportunity__r.StageName, Addon_Type__c, Billing_Contact__c, Billing_Contact__r.Name, Billing_Contact__r.Phone, Directory_Section__r.Section_Page_Type__c, P4P_Tracking_Number_ID__c,
                Billing_End_Date__c, Billing_Frequency__c,Opportunity__r.Billing_Contact__r.Email,Opportunity__r.Billing_Contact__r.HasOptedOutOfEmail, Billing_Partner__c, Anchor_Ad__c, Display_Ad__c,
                Billing_Start_Date__c, Canvass__c, Category__c, Continious_Billing__c, Opportunity__r.Billing_Contact__c,RecordTypeId, Core_Opportunity_Line_ID__c, OL_Print_Specialty_Product_Type__c, 
                Description__c, Directory__c, Directory_Edition__c, Discount__c, Distribution_Area__c, Product2__r.Name, Product2__r.Inventory_Tracking_Group__c, Migration_Directory_Heading1__c, 
                Migration_Directory_Heading2__c, Migration_Directory_Heading3__c, Migration_Directory_Heading4__c, Product_Is_IBUN_Bundle_Product__c, Product2__r.Print_Product_Type__c,
                Effective_Date__c, FulfillmentDate__c, Geo_Code__c, Geo_Type__c, Is_Caption__c, Directory_Section__r.Section_Code__c,Media_Type__c,Cutomer_Cancel_Date__c, Product2__r.Is_Anchor__c,  
                Is_Child__c, Is_P4P__c, Line_Status__c, Linvio_Payment_Method_Id__c, Directory_Heading__c, Directory_Heading__r.Name, Directory_Heading__r.code__c,  
                Locality__c, UnitPrice__c, Name, Seniority_Date__c, Directory_Section__r.Columns__c,ListPrice__c,Last_Billing_Date__c,Account__r.OwnerId,Account__r.Owner.Email,Account__r.Owner.Manager.Email,
                Opportunity__c, Opportunity_line_item_id__c,Directory__r.Branch_Office__c, Order__c, Order_Group__c, Order_Line_Total__c, P4P_Billing__c,Is_Handled__c, 
                P4P_Price_Per_Click_Lead__c, Package_ID__c, Parent_ID__c, Parent_ID_of_Addon__c, Parent_Line_Item__c, Payment_Duration__c, Payment_Method__c,isCanceled__c,
                Payments_Remaining__c, Pricing_Program__c, Print_First_Bill_Date__c,Package_Name__c, Product_Type__c, Product2__c, Product2__r.Vendor__c, ProductCode__c, Quantity__c, 
                Quote_signing_method__c, Scope__c, Directory_Section__c, Statement_Suppression__c, Status__c, Product2__r.Family, Product2__r.RGU__c, Product2__r.Is_IBUN_Bundle_Product__c,
                Directory_Section__r.Name,Directory__r.Pub_Date__c, Section_Page_Type__c, strOLICombo__c, Product_Inventory_Tracking_Group__c, Opportunity_Close_Date__c,Listing__c,
                Account__r.Name,Account__r.Owner.Name,Directory_Edition__r.Name,Directory__r.Name,UDAC__c,Successful_Payments__c, Talus_Go_Live_Date__c, Telco_Invoice_Date__c, Directory_Code__c, 
                Edition_Code__c, Canvass__r.Canvass_Code__c,Product2__r.Product_Type__c, Listing__r.Phone__c, Listing__r.Area_Code__c, Opportunity__r.Name, Billing_Partner_Account__c, 
                Listing__r.Name, Listing__r.Listing_City__c, Listing__r.Listing_Country__c, Listing__r.Listing_PO_Box__c, Listing__r.Listing_Postal_Code__c, Listing__r.Listing_State__c, 
                Listing__r.Listing_Street__c, Listing__r.Listing_Street_Number__c,Directory_Edition__r.Book_Status__c,Directory_Edition__r.Pub_Date__c,Directory_Edition__r.Directory__c,
                Product2__r.Print_Specialty_Product_Type__c,Opportunity__r.Owner.Email, Opportunity__r.Owner.FirstName, Opportunity__r.Owner.LastName,
                Product2__r.ProductCode 
                from Order_Line_Items__c where Id IN:oliList];
                
                lockCloneHandlerController.FindAddOnProductForDFFAndCreateDFF(oliListNew,newOpportunity3, new map<id, id>());
                Test.stopTest();
      
  }
}