@isTest
private Class DFFMOLICleanUpSchedulerTest
{



static testMethod Void CleanupDataFulFillmentForm()
{
  Digital_Product_Requirement__c DFF = TestMethodsUtility.generateDataFulfillmentForm('Print Graphic');
  insert DFF; 
  Test.StartTest();
    DataFulFillmentCleanUpScheduler datafulfilmentcleanup = new DataFulFillmentCleanUpScheduler ();
     String schString = '0  00 1 3 * ?';
    String datafulfilmentcleanupjobID = system.schedule('Data Fulfillment job',  schString, datafulfilmentcleanup);
  Test.StopTest();
}

static testMethod void CleanUpModificationOrderLineItem()
{
    Opportunity opp=new Opportunity(name='rtretr');
    opp.StageName='Closed Won';
    opp.CloseDate=Date.parse('4/24/2015');
    insert opp;
    Order_Group__c orderGrp=new Order_Group__c(name='test');
    orderGrp.Opportunity__c=opp.Id;
      insert orderGrp;
    Modification_Order_Line_Item__c mol = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly');
    mol.Order_Group__c=orderGrp.Id;
    insert mol;
    Test.StartTest();
        ModificationOrderLineCleanUpScheduler MOLIcleanup = new ModificationOrderLineCleanUpScheduler ();
        String schString = '0  00 1 3 * ?';
        String MOLIcleanupjobID = system.schedule('Modification Order line Item job',  schString, MOLIcleanup);
    Test.StopTest();
   
}


}