@isTest(seeAllData=True)
public with sharing class AccountWithDirectoriesTest {
    static testMethod void testAccountWithDir() {
        Test.StartTest();
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.createAccount('cmr');
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        insert og;
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        insert oln; 
        
        Directory__c dir = TestMethodsUtility.createDirectory();
        
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        
        DE.Book_Status__c = 'NI';
        update DE;
        
        Line_Item_History__c LIH = new Line_Item_History__c(Order_Line_Total__c = 900, Order_Line_Item__c = oln.Id, Directory_Edition__c = DE.Id);
        insert LIH;     
        
        AccountWithDirectories AWD = new AccountWithDirectories(new ApexPages.StandardController(acct));
        
        Test.StopTest();
    }
}