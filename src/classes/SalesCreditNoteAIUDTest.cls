@IsTest(SeeAllData=true)
public class SalesCreditNoteAIUDTest {
    static testMethod void InsertSalesCreditNote() {
        List<c2g__codaDimension1__c> lstDimension1 = new List<c2g__codaDimension1__c>();
        Canvass__c newCanvass = TestMethodsUtility.createCanvass();
        Account newAccount = TestMethodsUtility.generateAccount('customer', false);
        newAccount.Primary_Canvass__c = newCanvass.Id;
        insert newAccount;
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Test.startTest();
        Case newCase = TestMethodsUtility.createCase('CS Claim', newAccount);
        Test.stopTest();
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        Directory__c newDirectory = TestMethodsUtility.createDirectory();
        c2g__codaInvoice__c newSalesInvoice = TestMethodsUtility.createSalesInvoice(newAccount, newOpportunity);
        c2g__codaCreditNote__c newSalesInvoiceCreditNote1 = TestMethodsUtility.createSalesCreditNote(newSalesInvoice, newAccount);
        c2g__codaCreditNote__c newSalesInvoiceCreditNote2 = TestMethodsUtility.generateSalesCreditNote(newSalesInvoice, newAccount);
        newSalesInvoiceCreditNote2.Case__c = newCase.Id;
        insert newSalesInvoiceCreditNote2;
        system.debug(newSalesInvoiceCreditNote1+'....................1'+newSalesInvoiceCreditNote1.Case__c);
        system.debug(newSalesInvoiceCreditNote2+'....................2'+newSalesInvoiceCreditNote2.Case__c);
        list<c2g__codaCreditNoteLineItem__c> CNLIlist = new list<c2g__codaCreditNoteLineItem__c>();
        c2g__codaCreditNoteLineItem__c newSCNLI2 = TestMethodsUtility.generateSalesCreditNoteLineItem(newSalesInvoiceCreditNote2, newProduct);
        newSCNLI2.c2g__TaxRate1__c = null;
        newSCNLI2.c2g__TaxRate2__c = null;
        newSCNLI2.c2g__TaxRate3__c = null;
        newSCNLI2.c2g__Dimension1__c = TestMethodsUtility.createDimension1(newDirectory).Id;
        newSCNLI2.c2g__Dimension2__c = TestMethodsUtility.createDimension2(newProduct).Id;
        c2g__codaDimension3__c  dimension3 = [SELECT Id FROM c2g__codaDimension3__c WHERE c2g__ReportingCode__c =: newOrLI.Billing_Partner__c];
        newSCNLI2.c2g__Dimension3__c = dimension3.id;
        newSCNLI2.Billing_Frequency__c = CommonMessages.singlePayment;
        CNLIlist.add(newSCNLI2);
        insert CNLIlist;
    }
}