public with sharing class DirectoryMappingHandlerController {
    public static void onBeforeInsert(list<Directory_Mapping__c> objDMLst) {
        DirectoryMappingBeforeInsertUpdateLogic(objDMLst,null);
    }
    public static void onBeforeUpdate(list<Directory_Mapping__c> objDMLst,map<Id,Directory_Mapping__c> mapOldDM) {
        DirectoryMappingBeforeInsertUpdateLogic(objDMLst,mapOldDM);
    }
    
    public static void DirectoryMappingBeforeInsertUpdateLogic(list<Directory_Mapping__c> objDMLst,map<Id,Directory_Mapping__c> mapOldDM) {
        set<Id> setDirId = new set<Id>();
        map<Id,Directory__c> mapObjDir = new map<Id,Directory__c>();
        for(Directory_Mapping__c iteratorDM :objDMLst){
            if(mapOldDM != null) {
                Directory_Mapping__c oldDM = mapOldDM.get(iteratorDM.Id);
                if(iteratorDM.Directory__c != null && (iteratorDM.Directory__c != oldDM.Directory__c || iteratorDM.Canvass__c != oldDM.Canvass__c)) {
                    setDirId.add(iteratorDM.Directory__c);
                }
            }
            else {
                if(iteratorDM.Directory__c != null) {
                    setDirId.add(iteratorDM.Directory__c);
                }
            }
        }
        if(setDirId.size()>0) {
            mapObjDir=DirectorySOQLMethods.getCanvassByDirId(setDirId);
            if(mapObjDir.size()> 0){
                for(Directory_Mapping__c iterator :objDMLst) {
                    if(iterator.Directory__c != null && mapObjDir.get(iterator.Directory__c) != null) {
                        iterator.Canvass__c =  mapObjDir.get(iterator.Directory__c).Canvass__c;
                    }
                }
            }
        }
        
    }
  
}