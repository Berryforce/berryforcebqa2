/**
*/
public with sharing class A_AppContext
{
    /* //Check with different APIs but leave the latest one.
    private static c2g.CODAAPICommon_6_0.Context apiV6Context = null;

    public static c2g.CODAAPICommon_6_0.Context getv6ApiContext()
    {
        if( apiv6Context == null )
        {
            String apiToken = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', Blob.valueOf('0000000000000000C000000000000046' + UserInfo.getUserId() + 'PROBABLEMENTE EL MEJOR SOFTWARE DE CONTABILIDAD EN EL MUNDO')));
            apiv6Context = new c2g.CODAAPICommon_6_0.Context();
            apiv6Context.Token = apiToken;
        }

        return apiv6Context;
    }

    private static c2g.CODAAPICommon_7_0.Context apiV7Context = null;

    public static c2g.CODAAPICommon_7_0.Context getv7ApiContext()
    {
        if( apiv7Context == null )
        {
            String apiToken = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', Blob.valueOf('0000000000000000C000000000000046' + UserInfo.getUserId() + 'PROBABLEMENTE EL MEJOR SOFTWARE DE CONTABILIDAD EN EL MUNDO')));
            apiv7Context = new c2g.CODAAPICommon_7_0.Context();
            apiv7Context.Token = apiToken;
        }

        return apiv7Context;
    }*/

    private static c2g.CODAAPICommon_8_0.Context apiV8Context = null;

    public static c2g.CODAAPICommon_8_0.Context getv8ApiContext()
    {
        if( apiv8Context == null )
        {
            String apiToken = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', Blob.valueOf('0000000000000000C000000000000046' + UserInfo.getUserId() + 'PROBABLEMENTE EL MEJOR SOFTWARE DE CONTABILIDAD EN EL MUNDO')));
            apiv8Context = new c2g.CODAAPICommon_8_0.Context();
            apiv8Context.Token = apiToken;
        }

        return apiv8Context;
    }
}