@isTest(seealldata=true)
public class BillingTransferReportControllerTest{
  static testmethod void myunitTest()
    {
        Test.startTest();
        Canvass__c c=TestMethodsUtility.createCanvass();
        c = [SELECT Name, Canvass_Code__c, Id FROM Canvass__c WHERE Id =: c.Id];
        Account newaccount = new Account(Primary_Canvass__c = c.Id, BillingState = 'AB', BillingCountry = 'US', BillingCity = 'California', Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');
        insert newaccount ;
        Contact newContact = TestMethodsUtility.createContact(newAccount);
        insert newContact ;
        Opportunity opp =TestMethodsUtility.createOpportunity(newaccount,newContact );
        insert opp;
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = dir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c =Date.today().addMonths(1);
        insert objDirE;
        
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE2';
        objDirE1.Directory__c = dir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS';
        objDirE1.Pub_Date__c =Date.today();
        
        insert objDirE1;                  
        
        Directory_Section__c objDirSec = new Directory_Section__c();
        objDirSec.Directory__c = dir.id;
        objDirSec.Name = 'Test Dir Section';
        objDirSec.Section_Page_Type__c = 'WP';
        objDirSec.Section_Code__c = '111111';
        insert objDirSec; 
           
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        newProduct.RGU__c = 'Print';
        insert newProduct;
                Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
    objProd.RGU__c = 'Print';         
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print Graphic';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        objProd1.RGU__c = 'Print';
        insert objProd1;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
       // Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Successful_Payments__c=5,Directory_Section__c=objDirSec.id ,Billing_Partner__c='THE BERRY COMPANY',Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',Directory_Edition__c = objDirE1.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Billing_Frequency__c='Monthly',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMOnths(2));
        insert objOrderLineItem;
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c = 'THE BERRY COMPANY', Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='Telco Billing',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
        insert objOrderLineItem1;
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c = 'THE BERRY COMPANY1', Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='Telco Billing',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
        insert objOrderLineItem2;
         Set<Id> oliListIds=new Set<Id>();
        oliListIds.add(objOrderLineItem.id);
        oliListIds.add(objOrderLineItem1.id);
        Map<Id, Order_Line_Items__c> mapOLI = new map<Id, Order_Line_Items__c>([SELECT Account__c,Action_Code__c,Action__c,Addon_Type__c,Advertised_Phone_Number__c,
                           Advertising_Data__c,Anchor_Caption_Header_Record_ID__c,Anchor_Listing_Caption_Header__c,BAS__c,
                           BillingChangeNoofDays__c,BillingChangeProrateCreditDays__c,BillingChangesPayment__c,BillingTransferedInfo__c,
                           BillingTransfered__c,Billing_Change_Prorate_Credit__c,Billing_Close_Date__c,Billing_Contact__c,Billing_End_Date__c,
                           Billing_Frequency_Change_Date__c,Billing_Frequency_Change__c,Billing_Frequency__c,Billing_Partner_Account__c,
                           Billing_Partner_Change__c,Billing_Partner_Transferred__c,Billing_Partner__c,Billing_Start_Date__c,
                           Billing_Transfer_Type__c,Call_Tracking_Phone_Number__c,Cancelation_Fee__c,Cancellation__c,Canvass__c,
                           Caption_Or_Extra_Line_Text__c,Case__c,Category__c,checked__c,Client_Number__c,CMR_Name__c, Product2__r.RGU__c,
                           CMR_Number_Client_Number__c,CMR_Number__c,Comments__c,Continious_Billing__c,
                           Core_Opportunity_Line_ID__c,CreatedById,CreatedDate,Current_Billing_Period_Days2__c,
                           Current_Billing_Period_Days__c,Current_Daily_Prorate__c,Current_Invoice__c,Current_Month_for_P4P__c,
                           Current_Rate__c,Custom_Domain_Name__c,Cutomer_Cancel_Date__c,Date_of_Transfer__c,Date__c,DAT__c,
                           Days_B_W__c,Days_For_Task_Notification__c,Days_From_Creation_to_Fulfillment__c, Canvass__r.Canvass_Code__c,
                           Days_from_Submit_to_Fulfilmment__c,Description__c,Destination_Phone_Number__c,Digital_Product_Requirement__c,
                           Directory_Code__c,Directory_Edition_Book_Status__c,Directory_Edition_Name__c,Directory_Edition_Renewal_Date__c,
                           Directory_Edition__c,Directory_Heading__c,Directory_Name__c,Directory_Section__c,Directory__c,Discount__c,
                           Distribution_Area__c,Domain_Email_Addresses__c,Edition_Code__c,Effective_Date__c,Evergreen_Payments__c,
                           Expiration_Date__c,Facebook_Admin__c,Facebook_URL__c,From_C__c,From__c,Fulfilled_on__c,FulfillmentDate__c,
                           Fulfillment_Status__c,Geo_Code__c,Geo_Type__c,Id,Indent_Order__c,isCanceled__c,IsDeleted,Is_Billing_Cycle__c,
                           Is_Caption__c,Is_Child__c,Is_P4P__c,Is_Trademark_Finding_Line__c,L2N__c,Landing_Page_URL__c,LastActivityDate,
                           LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Last_Billing_Date__c,Last_Invoice__c,
                           Last_successful_settlement__c,line_color_id__c,Line_Number__c,Line_Status__c,Linvio_Payment_Method_Id__c,
                           Listing_Name__c,Listing__c,ListPrice__c,Locality__c,Media_Type__c,Migration_Directory_Heading1__c,
                           Migration_Directory_Heading2__c,Migration_Directory_Heading3__c,Migration_Directory_Heading4__c,Mobile_URL__c,
                           N2L__c,Name,National_Staging_Line_Item__c,NAT_Client_ID__c,NAT__c,Next_Billing_Date__c,
                           Number_of_Months_Transferred__c,Opportunity_Close_Date__c,Opportunity_line_Item_id__c,Opportunity__c,
                           Order_Anniversary_Start_Date__c,Order_Billing_Date_Changed__c,Order_Group__c,Order_Line_Item_Locality__c,
                           Order_Line_Total__c,Order__c,OriginalBillingDate__c,Original_Order_Line_Item__c,P4P_Billing__c,
                           P4P_Current_Billing_Clicks_Leads__c,P4P_Current_Months_Clicks_Leads__c,P4P_Price_Per_Click_Lead__c,
                           P4P_Tracking_Number_ID__c,P4P_Tracking_Number_Status__c,Package_ID_External__c,
                           Package_ID__c,Package_Item_Quantity__c,Package_Name__c,Page_Number__c,Parent_ID_of_Addon__c,Parent_ID__c,
                           Parent_Line_Item__c,Payments_Remaining__c,Payment_Duration__c,Payment_Method__c,Pending_Disconnect__c,
                           Previous_Directory_Edition__c,Previous_End_Date__c,Previous_Start_Date__c,Pricing_Program__c,
                           Print_First_Bill_Date__c,Print_Product_Type__c,Product2__c,ProductCode__c,Product_Inventory_Tracking_Group__c,
                           Product_Is_IBUN_Bundle_Product__c,Product_Type__c,Prorate_Credit_Days__c,Prorate_Credit__c,Prorate_Stored_Value__c,
                           Proxy_URL__c,Publication_Code__c,Publication_Company__c,Publication_Date__c,Quantity__c,Quote_Signed_Date__c,
                           Quote_signing_method__c,RCF_Tracked__c,ReadyForProcess__c,Reason_for_Transfer__c,RecordTypeId,
                           Requires_Scoped_Suppression_Processing__c,Scoped_Caption_Header__c,
                           Scoped_Suppression_Processing_Complete__c,Scope__c,Section_Page_Type__c,Selected_Cancel_Date__c,Seniority_Date__c,
                           Sent_to_fulfillment_on__c,Service_End_Date__c,Service_Start_Date__c,Sort_As_Check_Test__c,Sort_As_FORMULA__c,
                           Sort_As__c,SPINS__c,Statement_Suppression__c,Status__c,strOLICombo__c,strSeniorityDateCombo__c,
                           strSuppressionCheck__c,Subscription_ID__c,Successful_Payments__c,SystemModstamp,Talus_Cancel_Date__c,
                           Talus_DFF_Id__c, Directory_Edition__r.Directory__c, Directory_Edition__r.Book_Status__c, Directory_Edition__r.Name,
                           Talus_Fulfillment_Date__c,Talus_Go_Live_Date__c,Talus_OrderLineItem__c,Talus_Subscription_ID__c,Tear_Page_PDF_Path__c,
                           Telco_Invoice_Date__c,Telco__c,Timestamp_Update__c,Total_Billing_Amount_Transfered__c,Total_Number_Free__c,Total_Prorated_Days_for_contract__c,
                           Total_Prorate__c,To_P__c,To__c,Trademark_Display_Text__c,Trademark_Finding_Line_OLI__c,Transaction_ID__c,Transferred_from__c,Transferred_To__c,
                           TransIDLineNo__c,Trans_Version__c,Trans__c,Type__c,UDAC_Group__c,UDAC__c,UnitPrice__c,User_who_initiated_the_transfer__c,Vendor_Product_Key__c,
                           Website_URL__c,Zero_Cancel_Fee_Approved__c,zero_Cancel_Fee_Requested__c FROM Order_Line_Items__c where Id IN : oliListIds]);
    objOrderLineItem = mapOLI.get(objOrderLineItem.id);                           
    objOrderLineItem1 = mapOLI.get(objOrderLineItem1.id);
        c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(dir);
        c2g__codaDimension1__c  dimension1ForCanvass = TestMethodsUtility.createDimension1(c.Name, c.Canvass_Code__c);
        c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd1);        
        //Modified the code  1/19/2015        
        c2g__codaDimension3__c  dimension3= TestMethodsUtility.createDimension3(objOrderLineItem2);
        dimension3.c2g__ReportingCode__c =  CommonMessages.BerryForDimension;
        //c2g__codaDimension3__c  dimension3 = [SELECT Id FROM c2g__codaDimension3__c WHERE c2g__ReportingCode__c =: CommonMessages.BerryForDimension];        
        c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
        c2g__codaPeriod__c period=[select id from c2g__codaPeriod__c limit 1 ];
        c2g__codaInvoice__c invoice =TestMethodsUtility.createSalesInvoice(newAccount,opp);
        invoice.Transaction_Type__c='TD - Billing Transfer Invoice';
        update invoice;
        c2g__codaAccountingCurrency__c currencyrec=[SELECT Id from c2g__codaAccountingCurrency__c WHERE name='USD' limit 1];
        c2g__codaInvoiceLineItem__c invoiceLi=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItem1,invoice, objProd1,dimension1,dimension2,dimension3,dimension4); 
        
        c2g__codaCreditNote__c  creditnote=TestMethodsUtility.createSalesCreditNote(invoice,newAccount);
        creditnote.Transaction_Type__c='TD - Billing Transfer Invoice';
        update creditnote;        
        c2g__codaCreditNoteLineItem__c  creditnoteLI=TestMethodsUtility.createSalesCreditNoteLineItem(creditnote,objProd1,objOrderLineItem1,dimension1,dimension2,dimension3,dimension4);
        
        Test.stopTest();
        
        ApexPages.CurrentPage().getparameters().put('id', newaccount.Id);
        list<account> lstaccount = new list<account>();
        lstaccount.add(newaccount);
        ApexPages.StandardController sc = new ApexPages.standardController(lstaccount[0]);
        BillingTransferReportController obj = new BillingTransferReportController(sc);
        obj.GetReportInfo();
    }
}