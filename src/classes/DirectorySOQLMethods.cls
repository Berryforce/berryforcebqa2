public class DirectorySOQLMethods {
    public static map<Id, Directory__c> getNationalCommissionById(set<Id> dirIDs) {
        return new map<Id, Directory__c>([Select Id, National_Commission__c, National_Commission__r.Rate__c from Directory__c where Id IN :dirIDs]);
    }
    
    public static list<Directory__c> getDirectoryByCode(set<String> setDirCode) {
        return [Select Id, name, Directory_Code__c, Publication_Company__c, Publication_Company_Code__c, state__c, (Select Id,Name, Pub_Date__c, National_Close_Date__c, Edition_Code__c, 
        Directory_Edition_Code__c, LSA_Directory_Version__c from Directory_Editions__r where Book_Status__c = 'NI') from Directory__c where Directory_Code__c IN:setDirCode];
    }
    
    public static list<Directory__c> getDirectoriesByCanvassId(String canvassId){
        return [SELECT Name, Id, directory_code__c FROM Directory__c WHERE Canvass__c = : canvassId];
    }
    
    public static list<Directory__c> getDirectoryForMediaTrax() {
        return [Select Id, Directory_Code__c, Name, Media_Trax_Publisher_ID__c, Media_Trax_Directory_ID__c, Telco_Provider_Code__c from Directory__c 
                where Directory_Code__c != null and Media_Trax_Publisher_ID__c != null and Telco_Provider_Code__c != null and Media_Trax_Directory_ID__c = null limit 500];
    }
    
    public static list<Directory__c> getDirectoryUpdateMediaTrax() {
        return [Select Is_Changed__c, Id, Directory_Code__c, Name, Media_Trax_Publisher_ID__c, Media_Trax_Directory_ID__c, Telco_Provider_Code__c from Directory__c where Directory_Code__c != null and Media_Trax_Publisher_ID__c != null and Telco_Provider_Code__c != null and Media_Trax_Directory_ID__c != null and Is_Changed__c = true limit 500];
    }
    
    public static list<Directory__c> getDirectoryDirsectionById(set<Id> ObjDirId){
        return [Select Id,Name,Toll_Free_Phrase__c,(Select Id,Name,Toll_Free_Phrase__c from Directory_Sections__r) from Directory__c where Id IN : ObjDirId];
    }
    
    public static map<Id, Directory__c> getCanvassByDirId(set<Id> dirIDs) {
        return new map<Id, Directory__c>([Select Id,Canvass__c  from Directory__c where Id IN :dirIDs]);
    }
    
    public static list<Directory__c> getDirectoryDEById(set<Id> setDirIds) {
        return [Select Id, Telco_Provider__c, (Select Id from Directory_Editions__r where Book_Status__c = 'NI') from Directory__c where id IN:setDirIds];
    }
    
    public static map<Id, Directory__c> getDirectoryDEByDirId(set<Id> setDirIds) {
         return new map<Id, Directory__c>([Select Id,(Select Id,Book_Status__c,Pub_Date__c,ALPHA_PAGES_DUE__c,Directory__c,Final_Service_Order_Due_to_Berry__c  from Directory_Editions__r where Book_Status__c = 'NI' OR Book_Status__c = 'BOTS') from Directory__c where Id IN :setDirIds]);
    }
}