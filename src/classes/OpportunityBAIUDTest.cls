@isTest
public class OpportunityBAIUDTest {
    static testMethod void opptyTest() {
        Test.startTest();
        List<Directory_Edition__c> lstDE = new List<Directory_Edition__c>();
        Pricebook2 pricebook = TestMethodsUtility.createpricebook();
        Account telcoAcct = TestMethodsUtility.createAccount('telco');
        Account Acc1 = TestMethodsUtility.generateAccount('customer');
        Acc1.Telco_Partner__c = telcoAcct.Id;
        insert Acc1;
        Telco_Billing_Defaults__c TBD = TestMethodsUtility.createTelcoBillingDefaults(telcoAcct);
        Contact newContact = TestMethodsUtility.generateContact(Acc1.Id);
        newContact.PrimaryBilling__c = True;
        newContact.IsActive__c = True;
        insert newContact;
        Opportunity oppty = TestMethodsUtility.createOpportunity('new', Acc1);
        oppty.StageName = CommonMessages.closedOwnStageOpp;
        update oppty;
        delete oppty;
        Test.stopTest();
    }
}