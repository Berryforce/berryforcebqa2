@isTest
public class NationalBillingStatusSOQLMethodsTest {
    static testMethod void NBSSTest() {
        Account pubAcct = TestMethodsUtility.generatePublicationAccount();
        insert pubAcct;
       /* Directory__c dir = TestMethodsUtility.generateDirectory();
        dir.Publication_Company__c = pubAcct.Id;
        insert dir;*/
        Directory__c dir=TestMethodsUtility.createDirectory(); 
        Directory_Edition__c DE = TestMethodsUtility.generateDirectoryEdition(dir);
        DE.Pub_Date__c = system.today();
        insert DE;
        National_Billing_Status__c natBillStatus = TestMethodsUtility.createNatBillStatus(pubAcct.Id, DE.Id); 
        NationalBillingStatusSOQLMethods.getNatBillByPubEditionDirCode(pubAcct.Publication_Code__c, DE.Edition_Code__c, dir.Directory_Code__c);
        NationalBillingStatusSOQLMethods.getNatBillByPubEditionCode(pubAcct.Publication_Code__c, DE.Edition_Code__c);
        NationalBillingStatusSOQLMethods.getNatBillByDirEdLSACodeDirCode(new Set<String> {DE.LSA_Directory_Version__c}, new Set<String> {DE.Directory_Code__c});
        NationalBillingStatusSOQLMethods.getNatBillByDirEdIds(new Set<Id> {DE.Id});
    }
}