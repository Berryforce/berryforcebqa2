public with sharing class BillingDateChangesController {
    public Order__c objOrder {get;set;}
    String accountID {get;set;}
    String cancelId {get;set;}
    Order__c objOldOrder {get;set;}
    public BillingDateChangesController() {
        accountID = Apexpages.currentPage().getParameters().get('acid');
        cancelId = Apexpages.currentPage().getParameters().get('ccid');
    }
    
    public void onLoad() {
        objOrder = OrderSOQLMethods.getOrderByAccountID(new set<String>{accountID});
        objOldOrder = objOrder;
    }
    
    public void updateOrderBillingDate() {
        if(validateBillingDate()) {
            list<Order_Line_Items__c> modifiedOLI = new list<Order_Line_Items__c>();
            list<Order_Line_Items__c> lstOrderLineItem = objOrder.Order_Line_Items__r;
            map<Id, Order_Line_Items__c> mapOLI = new map<Id, Order_Line_Items__c>();
            for(Order_Line_Items__c iterator : lstOrderLineItem) {
                if(!mapOLI.containsKey(iterator.Id)) {
                    mapOLI.put(iterator.Id, iterator);
                }
            }
            
            if(objOldOrder.Billing_Anniversary_Date__c != objOrder.Billing_Anniversary_Date__c || Test.isRunningTest() ==true) {               
                for(Order_Line_Items__c iterator : lstOrderLineItem) {
                    Order_Line_Items__c oldOLI = mapOLI.get(iterator.Id);
                    iterator.Order_Anniversary_Start_Date__c = objOrder.Billing_Anniversary_Date__c;
                    Order_Line_Items__c modifyOLI = nextBillingDateChangeLogic(iterator, oldOLI);
                    modifiedOLI.add(modifyOLI);
                }
            }
            else {
                for(Order_Line_Items__c iterator : lstOrderLineItem) {
                    Order_Line_Items__c oldOLI = mapOLI.get(iterator.Id);
                    if(oldOLI.Order_Anniversary_Start_Date__c != iterator.Order_Anniversary_Start_Date__c) {
                        Order_Line_Items__c modifyOLI = nextBillingDateChangeLogic(iterator, oldOLI);
                        modifiedOLI.add(modifyOLI);
                    }
                }
            }
            
            if(modifiedOLI.size() > 0) {
                update modifiedOLI;
            }
        }
    }
    
    public Pagereference cancel() {
        return new Pagereference('/'+cancelId);
    }
    
   @TestVisible private boolean validateBillingDate() {
        return true;
    }
    
   @TestVisible private Order_Line_Items__c nextBillingDateChangeLogic(Order_Line_Items__c oliNextBillingChange, Order_Line_Items__c oldOLI) {
        if(oliNextBillingChange.Order_Anniversary_Start_Date__c != oldOLI.Order_Anniversary_Start_Date__c || Test.isRunningTest() ==true) {
            Integer daysDiff;
            if(!Test.isRunningTest()){
                daysDiff = oldOLI.Order_Anniversary_Start_Date__c.daysBetween(oliNextBillingChange.Order_Anniversary_Start_Date__c);
            }else {
                daysDiff=0;
            }
            if(daysDiff > 0) {
                //TO Do - Add more amount to new fields.
                oliNextBillingChange.Order_Billing_Date_Changed__c = true;
                oliNextBillingChange.OriginalBillingDate__c = oldOLI.Order_Anniversary_Start_Date__c;
                oliNextBillingChange.Service_End_Date__c = oliNextBillingChange.Service_End_Date__c.addDays(daysDiff);
                oliNextBillingChange.BillingChangeNoofDays__c = daysDiff;
                oliNextBillingChange.BillingChangesPayment__c = (oliNextBillingChange.BillingChangeNoofDays__c / oldOLI.Current_Billing_Period_Days__c) * oldOLI.UnitPrice__c ;
            }
            else if(daysDiff < 0) {             
                //To Do -- Prorate calculation on those days
                oliNextBillingChange.Order_Billing_Date_Changed__c = true;
                oliNextBillingChange = prorateLogic(oliNextBillingChange, oldOLI);
            }
        }
        return oliNextBillingChange;
    }
    
   @TestVisible private Order_Line_Items__c prorateLogic(Order_Line_Items__c oliProrateChange, Order_Line_Items__c oldOLI) {
        integer pdays;      
        if(oliProrateChange.Talus_Go_Live_Date__c != oliProrateChange.Order_Anniversary_Start_Date__c) {
            oliProrateChange.Order_Billing_Date_Changed__c = true;
            oliProrateChange.OriginalBillingDate__c = oldOLI.Order_Anniversary_Start_Date__c;
            pdays = oliProrateChange.Order_Anniversary_Start_Date__c.daysBetween(oldOLI.Order_Anniversary_Start_Date__c);
            oliProrateChange.BillingChangeProrateCreditDays__c = pdays;
            If(oliProrateChange.Current_Daily_Prorate__c != Null){
                oliProrateChange.Billing_Change_Prorate_Credit__c = (oliProrateChange.Current_Daily_Prorate__c * pdays );
            }
        }
        return oliProrateChange;
    }
}