public class Listing_AUTrigger {

    public static void fieldDupe(set<Id> LID) { 
        list <Order_Line_Items__c> OLI= new list <Order_Line_Items__c>();
        
        OLI = [SELECT Id, Sort_As_FORMULA__c, Sort_As__c FROM Order_Line_Items__c
        WHERE Listing__c IN : LID];
        
        if(OLI.size()>0 ) {
            for(Order_Line_Items__c OlineRec : OLI){
                OlineRec.Sort_As__c = OlineRec.Sort_As_FORMULA__c;
            
            }
            update OLI;
        
        }   
    }
    
    public static void updateSortAs(List<listing__c> listListing, Map<Id, listing__c> oldMap) {
       Set<Id> listingIds = new Set<Id>();
        
       for(Listing__c lis : listListing) {
            Listing__c tempListing = oldMap.get(lis.Id);
            if(lis.Normalized_Last_Name_Business_Name__c != tempListing.Normalized_Last_Name_Business_Name__c || lis.Normalized_First_Name__c != tempListing.Normalized_First_Name__c || lis.Manual_Sort_As_Override__c != tempListing.Manual_Sort_As_Override__c || lis.Telco_Sort_Order__c != tempListing.Telco_Sort_Order__c || lis.Listing_Street_Number__c != tempListing.Listing_Street_Number__c || lis.Listing_Street__c != tempListing.Listing_Street__c || lis.Listing_City__c != tempListing.Listing_City__c || lis.Phone__c != tempListing.Phone__c) {
                listingIds.add(lis.Id);
            }
        }

        if(listingIds.size() > 0) {
            List<Order_Line_Items__c> listOLI = OrderLineItemSOQLMethods.getOLIByListingIds(listingIds);
            
            if(listOLI.size() > 0) {
                for(Order_Line_Items__c OLI : listOLI) {
                    OLI.Sort_As__c = OLI.Sort_As_FORMULA__c;
                }
            }
             
            update listOLI;
        }       
    }
    
    public static void ListingDisconnectValidation(list<listing__c> lstList,map<id,listing__c> lstOldmap){
       Set<Id> setLstId = new Set<Id>();
       map<Id,listing__c> mapObjLst = new map<Id,Listing__c>();
       set<Id> setDLDisconId = new set<Id>();
       for(Listing__c lis : lstList) {
            Listing__c OldListing = lstOldmap.get(lis.Id);
            if(lis.Disconnected__c != OldListing.Disconnected__c && lis.Disconnected__c == true ) {
                setLstId.add(lis.Id);
            }
        }
        if(setLstId.size()>0){
            mapObjLst = ListingSOQLMethods.fetchListing(setLstId);
        }
       if(mapObjLst.size()>0){
           for(Listing__c objLst : lstList){
               if(mapObjLst.get(objLst.Id) != null){
                   for(directory_listing__c objDirLst : mapObjLst.get(objLst.Id).Reference_Listings__r){
                   		if(objDirLst.Disconnected__c == false){
                   			//setDLDisconId.add(objLst.Id);
                   			if(setDLDisconId.size()> 0 && setDLDisconId.contains(objLst.Id)){
                   				setDLDisconId.remove(objLst.Id);
                   			}
                   		}
                   		else{
                   			setDLDisconId.add(objLst.Id);
                   		}
                        if(!setDLDisconId.contains(objLst.Id)){
                           objLst.addError('You must establish a new Caption Header record, and link the Caption Members to the new Caption Header using the Under Caption field.');
                       }
                   }
               }
           }
       }
    }
}