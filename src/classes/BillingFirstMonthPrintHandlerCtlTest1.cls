@isTest(SeeAllData=True)
public class BillingFirstMonthPrintHandlerCtlTest1 {
    public static testmethod void eachtest(){
       // BillingFirstMonthPrintHandlerCtl  bmph = new BillingFirstMonthPrintHandlerCtl();
        
        //skeleton
        Account acct = TestMethodsUtility.createAccount('cmr');
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;

        //creating second directory
        /*Directory__c objDir1 = TestMethodsUtility.generateDirectory();
        objDir1.Telco_Provider__c = objTelco.Id;
        objDir1.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir1.Publication_Company__c = newPubAccount.Id;
        objDir1.Division__c = objDiv.Id;
        insert objDir1;*/


        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;

        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.Pub_Date__c=System.today().addMOnths(1);
        insert objDirEd;

        Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd1.Pub_Date__c=System.today().addMOnths(2);
        insert objDirEd1;

        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;

        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();  
        Product2 newProduct;    
        for(Integer x=0; x<3;x++)
        {
         newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        lstProduct.add(newProduct);
        }
        //insert lstProduct;

        //creating another product to use in dimensions
        Product2 newProduct2;    
        for(Integer x=0; x<3;x++)
        {
         newProduct2 = TestMethodsUtility.generateproduct();
        newProduct2.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct2.Inventory_Tracking_Group__c = 'YP Leader Ad';
        lstProduct.add(newProduct2);
        }
        insert lstProduct;

        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        for(Product2 iterator : lstProduct) {
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDPM.Product2__c = iterator.Id;
        }
        insert lstDPM;

        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        insert newOpportunity;

        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
        OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
        objOLI.PricebookEntryId = iterator.Id;
        objOLI.Billing_Duration__c = 12;
        objOLI.Directory__c = objDir.Id;
        objOLI.Directory_Edition__c = objDirEd.Id;
        objOLI.Full_Rate__c = 30.00;
        objOLI.UnitPrice = 30.00;
        objOLI.Package_ID__c = '123456';
        objOLI.Billing_Partner__c = objOLI.Id;
        objOLI.OpportunityId = newOpportunity.Id;
        objOLI.Directory_Heading__c = objDH.Id;
        objOLI.Directory_Section__c = objDS.Id;
        lstOLI.add(objOLI);
        mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;

        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        for(OpportunityLineItem iterator : lstOLI) {
        Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
        objOLI.Package_ID__c = iterator.Package_ID__c;
        objOLI.Package_Item_Quantity__c = 2;
        objOLI.Directory__c = iterator.Directory__c;
        objOLI.Successful_Payments__c = 0;
        objOLI.Payments_Remaining__c = 1;
        objOLI.Directory_Edition__c = iterator.Directory_Edition__c;
        objOLI.UnitPrice__c = iterator.UnitPrice;
        objOLI.Opportunity_line_Item_id__c = iterator.Id;
        objOLI.Media_Type__c = CommonMessages.oliPrintProductType;
        objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
        objOLI.Directory_Section__c = iterator.Directory_Section__c;
        objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
        objOLI.Product_Inventory_Tracking_Group__c = 'YP Leader Ad';
        objOLI.Payment_Method__c = 'Credit Card';
        objOLI.Quantity__c=10;
        lstOrderLI.add(objOLI);
        }
        
        insert lstOrderLI;
        Order_Line_Items__c firstOLI = lstOrderLI[0];
        firstOLI.Talus_Go_Live_Date__c = Null; //system.today();
        firstOLI.Telco_Invoice_Date__c = system.today();
        firstOLI.Directory_Edition__c = objDirEd1.Id;
        firstOLI.CMR_Name__c = acct.Id;
        //added by praveen
        firstOLI.Print_First_Bill_Date__c = system.today();
        firstOLI.Order_Anniversary_Start_Date__c = system.today();
        update firstOLI;
        Order_Line_Items__c secondOLI = lstOrderLI[1];
        secondOLI.Talus_Go_Live_Date__c = system.today();
        update secondOLI;

        firstOLI.Talus_Cancel_Date__c = system.today();
        firstOLI.Status__c = 'Cancelation Requested';
        update firstOLI;

        //for first OLI
        c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDir);
        c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(newProduct);
        system.debug('#########################'+dimension2.id);
        c2g__codaDimension3__c  dimension3=TestMethodsUtility.createDimension3(firstOLI);
            
        //for Second OLI
        c2g__codaDimension1__c  dimension11=TestMethodsUtility.generateDimension1(objDir);
        dimension11.c2g__ReportingCode__c = 'test1'; 
        insert dimension11;
        c2g__codaDimension2__c  dimension12=TestMethodsUtility.generateDimension2(newProduct2);
        dimension12.c2g__ReportingCode__c = '1234456';  
        insert dimension12;
        system.debug('*************************'+dimension12.id); 
        c2g__codaDimension3__c  dimension13=TestMethodsUtility.generateDimension3(secondOLI);
        dimension13.c2g__ReportingCode__c = 'test2'; 
        insert dimension13;
             

        list<Order_Line_Items__c > oli = new list<Order_Line_Items__c >();
        Order_Line_Items__c oln = [SELECT Id, account__c, Directory_Code__c, Canvass__r.Canvass_Code__c, Product2__r.RGU__c, CMR_Name__c, 
                Billing_Partner__c, Is_P4P__c, P4P_Billing__c, Directory_Edition__c, Order_Group__c, 
                Order_Anniversary_Start_Date__c, Print_First_Bill_Date__c, Talus_Go_Live_Date__c, Payment_Duration__c, 
                Payment_Method__c, Product2__r.Family, Product2__r.Name, P4P_Current_Billing_Clicks_Leads__c,
                Quantity__c, Product2__c, Directory__c, Order_Line_Total__c, Successful_Payments__c, UnitPrice__c,
                Prorate_Credit_Days__c, Billing_Frequency__c              
                FROM Order_Line_Items__c WHERE Id = : firstOLI.Id];
                
                insert oli;
        oli.add(oln);
        //end
        
        
        
        Test.startTest();
        BillingFirstMonthPrintHandlerCtl.processTelcoInvoiceAndFirstPrintBillDateBilling(oli);
        Test.stopTest();
        }
        
        public static testmethod void eachtest1(){
       // BillingFirstMonthPrintHandlerCtl  bmph = new BillingFirstMonthPrintHandlerCtl();
        
        //skeleton
        Account acct = TestMethodsUtility.createAccount('cmr');
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;

        //creating second directory
        /*Directory__c objDir1 = TestMethodsUtility.generateDirectory();
        objDir1.Telco_Provider__c = objTelco.Id;
        objDir1.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir1.Publication_Company__c = newPubAccount.Id;
        objDir1.Division__c = objDiv.Id;
        insert objDir1;*/


        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;

        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.Pub_Date__c=System.today().addMOnths(3);
        insert objDirEd;

        Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd1.Pub_Date__c=System.today().addMOnths(4);
        insert objDirEd1;

        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;

        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();  
        Product2 newProduct;    
        for(Integer x=0; x<3;x++)
        {
         newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        lstProduct.add(newProduct);
        }
        //insert lstProduct;

        //creating another product to use in dimensions
        Product2 newProduct2;    
        for(Integer x=0; x<3;x++)
        {
         newProduct2 = TestMethodsUtility.generateproduct();
        newProduct2.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct2.Inventory_Tracking_Group__c = 'YP Leader Ad';
        lstProduct.add(newProduct2);
        }
        insert lstProduct;

        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        for(Product2 iterator : lstProduct) {
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDPM.Product2__c = iterator.Id;
        }
        insert lstDPM;

        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        insert newOpportunity;

        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
        OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
        objOLI.PricebookEntryId = iterator.Id;
        objOLI.Billing_Duration__c = 12;
        objOLI.Directory__c = objDir.Id;
        objOLI.Directory_Edition__c = objDirEd.Id;
        objOLI.Full_Rate__c = 30.00;
        objOLI.UnitPrice = 30.00;
        objOLI.Package_ID__c = '123456';
        objOLI.Billing_Partner__c = objOLI.Id;
        objOLI.OpportunityId = newOpportunity.Id;
        objOLI.Directory_Heading__c = objDH.Id;
        objOLI.Directory_Section__c = objDS.Id;
        lstOLI.add(objOLI);
        mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;

        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        for(OpportunityLineItem iterator : lstOLI) {
        Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
        objOLI.Package_ID__c = iterator.Package_ID__c;
        objOLI.Package_Item_Quantity__c = 2;
        objOLI.Directory__c = iterator.Directory__c;
        objOLI.Directory_Edition__c = iterator.Directory_Edition__c;
        objOLI.UnitPrice__c = iterator.UnitPrice;
        objOLI.Opportunity_line_Item_id__c = iterator.Id;
        objOLI.Media_Type__c = CommonMessages.oliPrintProductType;
        objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
        objOLI.Directory_Section__c = iterator.Directory_Section__c;
        objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
        objOLI.Product_Inventory_Tracking_Group__c = 'YP Leader Ad';
        objOLI.Payment_Method__c = 'Credit Card';
        objOLI.Quantity__c=10;
        lstOrderLI.add(objOLI);
        }
        
        insert lstOrderLI;
        Order_Line_Items__c firstOLI = lstOrderLI[0];
        firstOLI.Talus_Go_Live_Date__c = system.today();
        firstOLI.Telco_Invoice_Date__c = system.today();
        firstOLI.Directory_Edition__c = objDirEd1.Id;
        firstOLI.CMR_Name__c = acct.Id;
        firstOLI.Order_Anniversary_Start_Date__c = null;
        firstOLI.Print_First_Bill_Date__c = null;
        update firstOLI;
        Order_Line_Items__c secondOLI = lstOrderLI[1];
        secondOLI.Talus_Go_Live_Date__c = system.today();
        update secondOLI;

        firstOLI.Talus_Cancel_Date__c = system.today();
        firstOLI.Status__c = 'Cancelation Requested';
        update firstOLI;

        //for first OLI
        c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDir);
        c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(newProduct);
        system.debug('#########################'+dimension2.id);
        c2g__codaDimension3__c  dimension3=TestMethodsUtility.createDimension3(firstOLI);
            
        //for Second OLI
        c2g__codaDimension1__c  dimension11=TestMethodsUtility.generateDimension1(objDir);
        dimension11.c2g__ReportingCode__c = 'test1'; 
        insert dimension11;
        c2g__codaDimension2__c  dimension12=TestMethodsUtility.generateDimension2(newProduct2);
        dimension12.c2g__ReportingCode__c = '1234456';  
        insert dimension12;
        system.debug('*************************'+dimension12.id); 
        c2g__codaDimension3__c  dimension13=TestMethodsUtility.generateDimension3(secondOLI);
        dimension13.c2g__ReportingCode__c = 'test2'; 
        insert dimension13;
             

        list<Order_Line_Items__c > oli = new list<Order_Line_Items__c >();
        Order_Line_Items__c oln = [SELECT Id, account__c, Directory_Code__c, Canvass__r.Canvass_Code__c, Product2__r.RGU__c, CMR_Name__c, 
                Billing_Partner__c, Is_P4P__c, P4P_Billing__c, Directory_Edition__c, Order_Group__c, 
                Order_Anniversary_Start_Date__c, Print_First_Bill_Date__c, Talus_Go_Live_Date__c, 
                Payment_Method__c, Product2__r.Family, Product2__r.Name, P4P_Current_Billing_Clicks_Leads__c,
                Quantity__c, Product2__c, Directory__c, Order_Line_Total__c, Successful_Payments__c, UnitPrice__c,Payment_Duration__c,
                Prorate_Credit_Days__c, Billing_Frequency__c              
                FROM Order_Line_Items__c WHERE Id = : firstOLI.Id];
                
                insert oli;
        oli.add(oln);
        //end
        
        
        
        Test.startTest();
        BillingFirstMonthPrintHandlerCtl.processTelcoInvoiceAndFirstPrintBillDateBilling(oli);
        Test.stopTest();
        }
        
        
        }