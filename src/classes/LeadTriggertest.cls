// LeadTrigger Trigger Testclass
@isTest(SeeAllData=True)
public class LeadTriggertest{
    static testmethod void lttest(){
        Test.starttest();        
        Account telcoAcc = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcc.Id);
		Canvass__c objCanvass = TestMethodsUtility.createCanvass(telco);
        Account acc = TestMethodsUtility.createAccount('cmr');
        Lead objLead = new Lead(FirstName = 'Test', LastName = 'Test Unit', Company = 'Test Berry', Primary_Canvass__c = objCanvass.Id, Phone = '9999988888', Status = 'Open');
        insert objLead; 
        objLead.LeadSource = 'Sales Rep';
        objLead.Sales_Rep__c = UserInfo.getUserId();
        update objLead;       
        
        objLead.LeadSource = 'Leads List';
        objLead.Sales_Rep__c = null;
        update objLead;     
        
        List<Listing__c> listListing = new List<Listing__c>();
        Listing__c listing1 = TestMethodsUtility.generateListing();
        listing1.Lead__c = objLead.Id;
        listListing.add(listing1);
        Listing__c listing2 = TestMethodsUtility.generateListing();
        listing2.Account__c = acc.Id; 
        listListing.add(listing2);
        insert listListing;
        
        map<Id,Listing__c> oldMap = new map<Id,Listing__c>();
        oldMap.put(listing1.Id,listing1);
        oldMap.put(listing2.Id,listing2);
        ListingHandlerController.ListingafterInsertUpdateLogic(listListing,oldMap,null);
        
        Id LeadId = objLead.Id;
        
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(LeadId);
        lc.setDoNotCreateOpportunity(true);
        lc.setConvertedStatus('Qualified');             
        Database.LeadConvertResult lcr = Database.convertLead(lc);                
                
        Test.stoptest();
    }
}