global class tstBatch implements Database.Batchable<sObject>{
    global tstBatch(){
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        String SOQL = 'select DP_Edition_Year__c, Directory_Edition__r.Year__c from Directory_Pagination__c where DP_Edition_Year__c = null';            
        
        return Database.getQueryLocator(SOQL);
    }
    global void execute(Database.BatchableContext bc, List<Directory_Pagination__c> lstDP){
    
    
List<Directory_Pagination__c> lstDPNew = new List<Directory_Pagination__c>();
for(Directory_Pagination__c iterator : lstDP) {
    iterator.DP_Edition_Year__c = iterator.Directory_Edition__r.Year__c;
    lstDPNew.add(iterator);
}

update lstDPNew;
        
    }
    
    global void finish(Database.BatchableContext bc){
    }

}