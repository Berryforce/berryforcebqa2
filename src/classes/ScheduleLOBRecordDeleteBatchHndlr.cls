global class ScheduleLOBRecordDeleteBatchHndlr implements ScheduleLOBRecordDeleteBatch.ScheduleLOBRecordDeleteBatchInterface {
   global void execute(SchedulableContext sc) {
        LOBRecordDeleteBatch obj = new LOBRecordDeleteBatch();
        Database.executeBatch(obj);
    }  
}