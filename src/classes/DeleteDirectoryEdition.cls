global class DeleteDirectoryEdition implements Database.Batchable<sobject>, Database.Stateful{
    global set<Id> setDirectoryID = new set<Id>();
    global DeleteDirectoryEdition(Set<Id> setDirId){
        setDirectoryID = setDirId;
    }
    
	global Database.QueryLocator start(Database.BatchableContext bc) {
        set<String> setStatus = new set<String>{'NI'};
        String str = 'Select Id from Directory_Edition__c where Directory__c IN:setDirectoryID and Book_Status__c IN:setStatus';
        return Database.getQueryLocator(str);
    }
    
    global void execute(Database.BatchableContext bc, List<Directory_Edition__c> lstDE) {
        set<Id> setDE = new set<Id>();
        for(Directory_Edition__c iterator : lstDE) {
            setDE.add(iterator.Id);
        }
        list<Budget__c> lstBudget = [Select ID from Budget__c where Directory_Edition__c IN:setDE];
        if(lstBudget.size() > 0) {
            delete lstBudget;
        }
        list<National_Billing_Status__c> lstNational = [Select Id from National_Billing_Status__c where Directory_Edition__c IN:setDE];
        if(lstNational.size() > 0) {
            delete lstNational;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        set<String> setStatus = new set<String>{'NI'};
        list<Directory_Edition__c> lstDEDelete = [Select Id from Directory_Edition__c where Directory__c IN:setDirectoryID and Book_Status__c IN:setStatus];
        delete lstDEDelete;
    }
}