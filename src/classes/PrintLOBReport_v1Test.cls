@isTest(SeeAllData=true)
public class PrintLOBReport_v1Test {

    public static testMethod void testPrintLOBReportv1() {
        PageReference pageRef = Page.PrintLOBReport_v1;
        Test.setCurrentPage(pageRef);
        
        Canvass__c c=TestMethodsUtility.generateCanvass();
          c.Billing_Entity__c='CENTURY';
          insert c;
        list < Account > lstAccount = new list < Account > ();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for (Account iterator: lstAccount) {
            if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            } else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            } else {
                newTelcoAccount = iterator;
            }
        }
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;

        List < String > telcosList1 = new List < String > ();
        List < String > telcosList = new List < String > ();

        for (Telco__c Telcolst: [Select Id, Name from Telco__c where Account__c = : newTelcoAccount.Id]) {
            if (Telcolst.Id != null)
                telcosList.add(String.valueof(Telcolst.get('Id')));

        }
        system.assertNotEquals(newTelcoAccount.ID, null);

        Division__c objDiv = TestMethodsUtility.createDivision();

        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;

        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.Family = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        insert  objProd;
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        
        Test.startTest();
        
        List<Order_Line_Items__c> ListOLI=new List<Order_Line_Items__c >();
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Partner__c='Frontier Communications1',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1);
        ListOLI.add(objOrderLineItem);
       
       /* 
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Billing_Partner__c='Hawaiian Telcom1',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1);
        ListOLI.add(objOrderLineItem1);
        
        
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Billing_Partner__c='THE BERRY COMPANY1',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1);
        
        ListOLI.add(objOrderLineItem2);
        
        */
        
        insert ListOLI;
        
        
        
        c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount,newOpportunity);
        invoice.Customer_Name__c=newAccount.id;
        invoice.c2g__InvoiceStatus__c='In Progress';
        insert invoice;
        
       // c2g__codaAccountingCurrency__c currencyrec=new c2g__codaAccountingCurrency__c (Id='a0DG000000KYXszMAH');
        
        c2g__codaAccountingCurrency__c currencyrec=[SELECT Id from c2g__codaAccountingCurrency__c WHERE name='USD' limit 1];
        
        c2g__codaCreditNote__c SCN=TestMethodsUtility.generateSalesCreditNote(invoice, newAccount);
        SCN.c2g__InvoiceDate__c=date.today();
        insert SCN;
        
        c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDir);
        c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd);
        c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
        c2g__codaDimension3__c  dimension3=TestMethodsUtility.createDimension3(objOrderLineItem);
        //c2g__codaDimension3__c  dimension31=TestMethodsUtility.createDimension3(objOrderLineItem1);
        //c2g__codaDimension3__c  dimension32=TestMethodsUtility.createDimension3(objOrderLineItem2);
        
        Test.stopTest();
        c2g__codaInvoiceLineItem__c invoiceLi=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItem ,invoice,objProd,dimension1,dimension2,dimension3,dimension4 );
       // c2g__codaInvoiceLineItem__c invoiceLi1=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItem1 ,invoice,objProd,dimension1,dimension2,dimension31,dimension4 );
       // c2g__codaInvoiceLineItem__c invoiceLi2=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItem2 ,invoice,objProd,dimension1,dimension2,dimension32,dimension4 );
        
       
        c2g__codaCreditNoteLineItem__c scliDigital=TestMethodsUtility.generateSalesCreditNoteLineItem(SCN,objProd);
        scliDigital.Sales_Invoice_Line_Item__c=invoiceLi.id;
        scliDigital.c2g__Dimension1__c=dimension1.id;
        scliDigital.c2g__Dimension2__c=dimension2.id;
        scliDigital.c2g__Dimension3__c= dimension3.id;
        scliDigital.c2g__Dimension4__c=dimension4.id;
        insert scliDigital;
        
      /*  c2g__codaCreditNoteLineItem__c scliDigital1=TestMethodsUtility.generateSalesCreditNoteLineItem(SCN,objProd);
        scliDigital1.Sales_Invoice_Line_Item__c=invoiceLi.id;
        scliDigital1.c2g__Dimension1__c=dimension1.id;
        scliDigital1.c2g__Dimension2__c=dimension2.id;
        scliDigital1.c2g__Dimension3__c= dimension31.id;
        scliDigital1.c2g__Dimension4__c=dimension4.id;
        insert scliDigital1;
        
        c2g__codaCreditNoteLineItem__c scliDigital2=TestMethodsUtility.generateSalesCreditNoteLineItem(SCN,objProd);
        scliDigital2.Sales_Invoice_Line_Item__c=invoiceLi.id;
        scliDigital2.c2g__Dimension1__c=dimension1.id;
        scliDigital2.c2g__Dimension2__c=dimension2.id;
        scliDigital2.c2g__Dimension3__c= dimension32.id;
        scliDigital2.c2g__Dimension4__c=dimension4.id;
        insert scliDigital2;
     */
        //positive test
        String[] BillingPartners=new String[]{'Frontier Communications1','Hawaiian Telcom1','THE BERRY COMPANY1'};
        Apexpages.currentpage().getparameters().put('telcoProduct', String.valueof(objTelco.id));
        Apexpages.currentpage().getparameters().put('directoryId', String.valueof(objDir.id));
        Apexpages.currentpage().getparameters().put('Edition', String.valueof(objDirEd.id));
        Apexpages.currentpage().getparameters().put('IsNational', 'true');
        Apexpages.currentpage().getparameters().put('BillingPartner','Frontier Communications1,Hawaiian Telcom1,THE BERRY COMPANY1');
        
        PrintLOBReport_v1 PLOBR_P = new PrintLOBReport_v1();
        PLOBR_P.doGenerateCSV_email(BillingPartners, String.valueof(objDir.id), String.valueof(objDirEd.id));
        //Negative
        PLOBR_P.doGenerateCSV_email(null, String.valueof(objDir.id), String.valueof(objDirEd.id));
        PLOBR_P.doGenerateCSV_email(telcosList1, '', '');
        List<PrintLOBReport_v1.FinalWrapper> finalWrapList=new List<PrintLOBReport_v1.FinalWrapper>();
        finalWrapList.add(new PrintLOBReport_v1.FinalWrapper('Aest','2343453456','AT-01245',500,null,'Monthly','Berry','9900999898',null,'Test',null,'Test','Test','12234544'));
        finalWrapList.add(new PrintLOBReport_v1.FinalWrapper('Test','2343453456','AT-01245',500,null,'Monthly','Berry','9900999898',null,'Test',null,'Test','Test','12234544'));
        finalWrapList.add(new PrintLOBReport_v1.FinalWrapper('Xest','2343453456','AT-01245',500,null,'Monthly','Berry','9900999898',null,'Test',null,'Test','Test','12234544'));
        finalWrapList.add(new PrintLOBReport_v1.FinalWrapper('Test','2343453456','AT-01245',500,null,'Monthly','ABerry','9900999898',null,'Test',null,'Test','Test','12234544'));
        finalWrapList.sort();
        PLOBR_P.cancel();
        PLOBR_P.ExportReport();
        
        PrintLOBReport_v1.BillSummaryWrapper psw=new PrintLOBReport_v1.BillSummaryWrapper('THE BERRY COMPANY',500,'',5);
        PrintLOBReport_v1.UDACWrapper ud=new PrintLOBReport_v1.UDACWrapper('THE BERRY COMPANY','WLCSH',500,500,5);
    }

}