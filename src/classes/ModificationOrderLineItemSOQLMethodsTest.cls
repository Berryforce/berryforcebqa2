@IsTest
public class ModificationOrderLineItemSOQLMethodsTest {
    @isTest static void ModificationOrderLineItemSOQLMethodsCoverage() {

    Date effectiveDate;
    set<Id> accountIds = new set<id>();
    
    Account newAccount = TestMethodsUtility.createAccount('customer');
    accountIds.add(newAccount.id);
    
    System.assertNotEquals(null,ModificationOrderLineItemSOQLMethods.getMOLIBYEffectiveDate(system.today()));
    System.assertNotEquals(null,ModificationOrderLineItemSOQLMethods.getMOLIBYAccountIds(accountIds));
    }    
}