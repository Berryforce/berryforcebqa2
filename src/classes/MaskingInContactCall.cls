global class MaskingInContactCall{
    //preventing recursive future callouts while calling from trigger
    global static boolean stoprecursion=False;
    webService static String MaskingCallOut()
    {
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
    
        req.setEndpoint('https://home-c9.incontact.com/inContact/Manage/Scripts/Spawn.aspx?scriptName=SpawnSignal&bus_no=4593768&scriptId=2594515&skill_no=802591&p1='+UserInfo.getUserEmail()+'&p2=mask&Guid=00d3ab74-4dca-4dae-92cd-f9f2c992b8b5');
        req.setMethod('GET');
    
        req.setTimeout(100000);
        req.setHeader('Content-Type', 'application/soap+xml; charset=utf-8');
        req.setCompressed(false);
        req.setBody('');
    
        try {
            if(!Test.isRunningTest()){
                res = http.send(req);
            }
        }catch(System.CalloutException e){
            System.debug('Callout error: '+ e);
        }

        System.debug('Masking In controller class'+res);
        return null;
    }
    
    @future(callout=true)
    global static void UnMaskingCallOut()
    {
        stoprecursion=True;
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
    
        req.setEndpoint('https://home-c9.incontact.com/inContact/Manage/Scripts/Spawn.aspx?scriptName=SpawnSignal&bus_no=4593768&scriptId=2594515&skill_no=802591&p1='+UserInfo.getUserEmail()+'&p2=unmask&Guid=00d3ab74-4dca-4dae-92cd-f9f2c992b8b5');
        req.setMethod('GET');
    
        req.setTimeout(100000);
        req.setHeader('Content-Type', 'application/soap+xml; charset=utf-8');
        req.setCompressed(false);
        req.setBody('');
    
        try {
            if(!Test.isRunningTest()){
                res = http.send(req);
            }
        }catch(System.CalloutException e){
            System.debug('Callout error: '+ e);
        }

        System.debug('UnMasking In controller class'+res);
    }
}