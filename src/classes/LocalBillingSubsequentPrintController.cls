public with sharing class LocalBillingSubsequentPrintController {
    public Dummy_Object__c objDummy {get;set;} 
    public boolean bReconciled {get;set;}
    public boolean bPostAllInvoice {get;set;}
    public String strBatchStatus {get;set;}
    public String strSCNBatchStatus {get;set;}
    public boolean bEnabledPoller {get;set;}
    public boolean bShowLOB {get;set;}
    public boolean bShowFFPost {get;set;}
    public String strSIReconcileBatchStatus {get;set;}
    public String strSCNReconcileBatchStatus {get;set;}
    public boolean bReconciliationEnabledPoller {get;set;}
    public String strNoofAccountAndInvoice {get;set;}
    public String strNoofAccountAndSCN {get;set;}
    set<Id> setSIId;
    set<Id> setSCNId;
    @testvisible Id batchId; 
    @testvisible Id SCNbatchId;
    @testvisible Id SIReconciliationbatchId;
    @testvisible Id SCNReconciliationbatchId;
    
    //Report Ids Custom Settings
    public Id LOBMatchingReportId{get;set;}
    public Id goLiveReportId {get;set;}
    public Id UnReconciledSLIOLIReportID{get;set;}
    public Id EverythingGoneLivewithInvoice{get;set;}
    public Id SubsequentReconciliationReportID{get;set;}
    public Id rptPostingInvoice {get;set;}
    public Id rptPostingSCN {get;set;}
    
    Set<Id> setSIBatchIds = new Set<id>();
    Set<Id> setSCNBatchIds = new Set<id>();
    
    public LocalBillingSubsequentPrintController() {
        objDummy = new Dummy_Object__c();
        clearDatas();
        Map<String, Local_Billing_Reports__c> reportIds = Local_Billing_Reports__c.getAll();
        if(reportIds.get('Go Live')!=null) goLiveReportId = reportIds.get('Go Live').Report_Id__c;
        if(reportIds.get('EverythingGoneLivewithInvoice')!=null) EverythingGoneLivewithInvoice=reportIds.get('EverythingGoneLivewithInvoice').Report_Id__c;
        if(reportIds.get('LOB Matching Report')!=null) LOBMatchingReportId= reportIds.get('LOB Matching Report').Report_Id__c;
        if(reportIds.get('UnReconciled Sales Invoice OLI')!=null) UnReconciledSLIOLIReportID=reportIds.get('UnReconciled Sales Invoice OLI').Report_Id__c;
        if(reportIds.get('Subsequent Invoice Posting Report')!=null) rptPostingInvoice=reportIds.get('Subsequent Invoice Posting Report').Report_Id__c;
        if(reportIds.get('Subsequent SCN Posting Report')!=null) rptPostingSCN=reportIds.get('Subsequent SCN Posting Report').Report_Id__c;        
        if(reportIds.get('Print Subsequent Reconcile Report')!=null) SubsequentReconciliationReportID=reportIds.get('Print Subsequent Reconcile Report').Report_Id__c; 
    }
    
    private void clearDatas() {
        strNoofAccountAndInvoice = concatenateString(0, 0);
        strNoofAccountAndSCN = concatenateSCNString(0, 0);
        setSIId = new set<Id>();
        setSCNId =new set<Id>();
        strBatchStatus = '';
        strSCNBatchStatus = '';
        bPostAllInvoice = false;
        bEnabledPoller = false;
        bShowLOB = false;
        bShowFFPost = false;
        strSIReconcileBatchStatus= '';
        strSCNReconcileBatchStatus= '';
        bReconciliationEnabledPoller =false;
    }
    
    public void clickGo() {
        if(objDummy.From_Date__c != null) {           
            bShowLOB = true;
            bShowFFPost = true;
        }
    }
     /*
    public void updateSIwithReconciledCheckBox() {
        if(setSIId.size() > 0 && bReconciled) {
                if(!Test.isRunningTest()) 
                    SIReconciliationbatchId=Database.executeBatch(new LocalBillingSIReconciliationBatch(setSIId));
                    strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : Queued';
                
        }
        if(setSCNId.size()>0 && bReconciled){
                if(!Test.isRunningTest()) 
                    SCNReconciliationbatchId=Database.executeBatch(new LocalBillingSCNReconciliationBatch(setSCNId));
                    strSCNReconcileBatchStatus='Status of Reconciliation Credit Note Batch : Queued';
                    
        }
          bReconciliationEnabledPoller = true;
    }
    */
    public void callReconcileUpdateBatch() {
     /*   if(!Test.isRunningTest()) 
        SIReconciliationbatchId = Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch(objDummy, 'reconcile'), 50);
        strSIReconcileBatchStatus = 'Status of Reconciliation Invoice Batch : Queued';
        if(!Test.isRunningTest()) 
        SCNReconciliationbatchId = Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(objDummy, 'reconcile'), 40);
        strSCNReconcileBatchStatus = 'Status of Reconciliation Credit Note Batch : Queued';
        
        bReconciliationEnabledPoller = true; */
        Batch_Size__c reconcileBatchSize=Batch_Size__c.getValues('WizardSIReconcile');
        Batch_Size__c defaultBatchSize=Batch_Size__c.getValues('WizardSIPostWithoutSplit');
        List<SplitBatchJobs__c> reqdSplitBatchList=CommonMethods.fetchRequiredRecords('Subsequent Print');
        setSIBatchIds = new Set<id>();
        setSCNBatchIds = new Set<id>();
        if(!Test.isRunningTest()) {
	        if(reqdSplitBatchList!=null && reqdSplitBatchList.size()>0) {
	            for(SplitBatchJobs__c splitBatch:reqdSplitBatchList){
	                setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Subsequent Print','reconcile',objDummy,Integer.valueOf(splitBatch.Start_Number__c),Integer.valueOf(splitBatch.End_Number__c),Integer.valueOf(splitBatch.Min_Line_Item_Count__c), Integer.valueOf(splitBatch.Max_Line_Item_Count__c)),Integer.valueOf(reconcileBatchSize.Size__c)));
	            }
	        }else{   
	            setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Subsequent Print','reconcile',objDummy,null,null,null,null), Integer.valueOf(defaultBatchSize.Size__c)));
	        }
        }
        strSIReconcileBatchStatus = 'Status of Reconciliation Invoice Batch : Queued';
        if(!Test.isRunningTest()) {
        	setSCNBatchIds.add(Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(objDummy, 'reconcile'), 40));
        }
        strSCNReconcileBatchStatus = 'Status of Reconciliation Credit Note Batch : Queued';
        bReconciliationEnabledPoller = true; 
    }
    
    public void callPostFFBatch() {  
        try {
            if(!Test.isRunningTest()) {
             	setSIBatchIds = new Set<id>();
             	Batch_Size__c defaultBatchSize=Batch_Size__c.getValues('WizardSIPostWithoutSplit');
             	List<SplitBatchJobs__c> reqdSplitBatchList=CommonMethods.fetchRequiredRecords('Subsequent Print');
		        if(reqdSplitBatchList!=null && reqdSplitBatchList.size()>0) {
		            for(SplitBatchJobs__c splitBatch:reqdSplitBatchList) {
		                setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Subsequent Print','post',objDummy,Integer.valueOf(splitBatch.Start_Number__c),Integer.valueOf(splitBatch.End_Number__c), Integer.valueOf(splitBatch.Min_Line_Item_Count__c), Integer.valueOf(splitBatch.Max_Line_Item_Count__c)), Integer.valueOf(splitBatch.Batch_Size__c)));
		            }
		        }else{   
		            setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Subsequent Print','post',objDummy,null,null,null,null), Integer.valueOf(defaultBatchSize.Size__c)));
		        }
                strBatchStatus = 'Status of Financial Force Invoice Batch : Queued';        
            }            
        }
        catch(Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Error Occured while posting Invoice. Error : ' + ex);
            ApexPages.addMessage(myMsg);
        }

        try {
            if(!Test.isRunningTest()) {
            	setSCNBatchIds = new Set<id>();
                setSCNBatchIds.add(Database.executeBatch(new LocalBillingSCNPostOrReconciliationBatch(objDummy, 'post'), 2));
                strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : Queued';         
            }
        }
        catch (Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Error Occurred while posting Sales Credit Notes. Error : ' + ex);
            ApexPages.addMessage(myMsg);
        }
        bEnabledPoller = true;
    }
    
    public void apexJobStatus() {
    	boolean bStatusSI = LocalBillingCommonMethods.apexBatchJobStatus(setSIBatchIds);
    	if(bStatusSI) {
    		strBatchStatus = 'Status of Financial Force Invoice Batch : : Completed';
        }
        else {
        	strBatchStatus = 'Status of Financial Force Invoice Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        boolean bStatusSCN = LocalBillingCommonMethods.apexBatchJobStatus(setSCNBatchIds);
    	if(bStatusSCN) {
    		strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : : Completed';
        }
        else {
        	strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        
        if(bStatusSI && bStatusSCN) {
        	bEnabledPoller = false;
        }
        
        /*
        Set<Id> setBatchIds = new Set<id>();
        if(batchId != null) {
            setBatchIds.add(batchId);
        }
        if(SCNbatchId != null) {
            setBatchIds.add(SCNbatchId);
        } 
        Map<id,AsyncApexJob> mapBatchFFJob;
        if(setBatchIds.size() > 0) {
            mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
            if(mapBatchFFJob.get(batchId) != null) {
                strBatchStatus = 'Status of Financial Force Invoice Batch : ' + mapBatchFFJob.get(batchId).Status;
                if(mapBatchFFJob.get(batchId).Status == 'Completed') {
                    bEnabledPoller = false;
                }
            }
            if(mapBatchFFJob.get(SCNbatchId) != null) { 
                strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : ' + mapBatchFFJob.get(SCNbatchId).Status;
                if(mapBatchFFJob.get(SCNbatchId).Status == 'Completed') {
                    bEnabledPoller = false;
                }
            }
        }*/
    }
    
    public void apexReconciliationJobStatus() {
    	boolean bStatusSI = LocalBillingCommonMethods.apexBatchJobStatus(setSIBatchIds);
    	if(bStatusSI) {
    		strSIReconcileBatchStatus = 'Status of Reconciliation Invoice Batch : : Completed';
        }
        else {
        	strSIReconcileBatchStatus = 'Status of Reconciliation Invoice Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
    	
    	boolean bStatusSCN = LocalBillingCommonMethods.apexBatchJobStatus(setSCNBatchIds);
    	if(bStatusSCN) {
    		strSCNReconcileBatchStatus = 'Status of Reconciliation Credit Note Batch : : Completed';
        }
        else {
        	strSCNReconcileBatchStatus = 'Status of Reconciliation Credit Note Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        
        if(bStatusSI && bStatusSCN) {
        	bReconciliationEnabledPoller = false;
            bShowFFPost =true;
        }
    	/*
        Set<Id> setBatchIds = new Set<id>();
        Boolean bStatus = false;
        if(SIReconciliationbatchId != null) {
            setBatchIds.add(SIReconciliationbatchId);
        }
        if(SCNReconciliationbatchId != null) {
            setBatchIds.add(SCNReconciliationbatchId);
        } 
        Map<id,AsyncApexJob> mapBatchFFJob;
        if(setBatchIds.size() > 0) {
            mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
            if(mapBatchFFJob.containsKey(SIReconciliationbatchId)) {
             strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : ' + mapBatchFFJob.get(SIReconciliationbatchId).Status;
                if(mapBatchFFJob.get(SIReconciliationbatchId).Status == 'Completed') {
                    bStatus = true;
                }
            }
            else {
                bStatus = true;
            }
            if(mapBatchFFJob.containsKey(SCNReconciliationbatchId)) {
                strSCNReconcileBatchStatus= 'Status of Reconciliation Credit Note Batch : ' + mapBatchFFJob.get(SCNReconciliationbatchId).Status;
                if(bStatus && mapBatchFFJob.get(SCNReconciliationbatchId).Status == 'Completed') {
                    bStatus = true;
                }
                else {
                    bStatus = false;
                }
            }
            if(bStatus) {
                bReconciliationEnabledPoller = false;
                bShowFFPost =true;
            }
        }*/
    }
    /*
    public void postFFInvoice() {
        if(bPostAllInvoice) {
            if(setSIId.size() > 0) {
                try{
                 if(!Test.isRunningTest()) 
                    batchId = Database.executeBatch(new FFSalesInvoicePostBatchController(setSIId), Integer.valueOf(system.label.LocalBillingPostingBatchSize));
                    strBatchStatus = 'Status of Financial Force Invoice Batch : Queued';                    
                }
                catch(Exception ex){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Error Occured while posting Invoice. Error : '+ex);
                    ApexPages.addMessage(myMsg);
                }
                bEnabledPoller = true;
            }
            if(setSCNId.size() > 0) {
                try{
                 if(!Test.isRunningTest()) 
                    SCNbatchId = Database.executeBatch(new FFCreditNotePostBatchController(setSCNId), Integer.valueOf(system.label.LocalBillingPostingBatchSize));
                    strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : Queued';
                    
                }
                catch (Exception ex){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Error Occurred while posting Sales Credit Notes. Error : '+ex);
                    ApexPages.addMessage(myMsg);
                }
                bEnabledPoller = true;
            }                
        }
    }
      */
   

    private String concatenateString(Integer iAccCount, Integer iSICount) {
        return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Invoices for Selections';
    }
    
    private String concatenateSCNString(Integer iAccCount, Integer iSICount) {
        return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Credit Notes for Selections';
    }
}