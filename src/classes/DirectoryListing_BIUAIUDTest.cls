//DirectoryListingTrigger Test class
@isTest(SeeAllData=True)
public class DirectoryListing_BIUAIUDTest {
    static testmethod void DLTTest1() {
        Account newTelcoAccount = TestMethodsUtility.generateTelcoAccount();
        insert newTelcoAccount;
        Telco__c newTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = newTelco.Id;
        insert objDir;
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Directory_Heading__c dirHeading = Testmethodsutility.generateDirectoryHeading(); 
        dirHeading.Directory_Heading_Name__c=Testmethodsutility.generateRandomString(100);
        insert dirHeading;
        Directory_Heading__c dirHeading1 = Testmethodsutility.generateDirectoryHeading(); 
        dirHeading1.Directory_Heading_Name__c=Testmethodsutility.generateRandomString(101);
        insert dirHeading1;
        Listing__c  listing = Testmethodsutility.createListing('Main Listing');
        Directory_Listing__c DL1 = Testmethodsutility.generateDirectoryListing();
        DL1.Directory__c=objDir.Id;
        DL1.Listing__c=listing.Id;
        DL1.Directory_Section__c=objDS.Id;
        DL1.Directory_Heading__c = dirHeading1.Id;
        DL1.Telco_Provider__c = newTelco.Id;
        insert DL1;
        test.startTest();

        DL1.Directory_Heading__c=dirHeading1.Id;
        List<Directory_Listing__c> dList = new List<Directory_Listing__c>();
        dList.add(DL1);
        DL1.Directory_Heading__c=dirHeading1.Id;
        Map<Id,Directory_Listing__c> newMap = new Map<Id,Directory_Listing__c>();
        newMap.put(DL1.id,DL1);
        system.debug('test is'+newMap+DL1);
        //DirectoryListingHandlerController.UnSuppressingScopedOncancelOrderLineItem(dList,newMap);
        Test.stopTest();
    }
}