@isTest
public class BillingAmntPopulationOnDMIGRPrintOLITest {
	static testMethod void billAmtTest() {
        Canvass__c canvass = TestMethodsUtility.createCanvass();      
        Account acct = TestMethodsUtility.createAccount('cmr');         
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);    
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        insert og;
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Mapping__c dirMap = TestMethodsUtility.createDirectoryMapping(telco);
        Directory_Edition__c DE = TestMethodsUtility.generateDirectoryEdition(dir.Id, canvass.Id, telco.Id, dirMap.Id);
        DE.Book_Status__c = 'BOTS';
        insert DE;
        Product2 product2 = TestMethodsUtility.createproduct2();
        product2.Product_Type__c = 'Print';
        insert product2;  
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og); 
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Product2__c = product2.Id;
        oln.isCanceled__c = false;
        oln.RecordTypeId = Label.TestOLIRTLocal;
        insert oln; 
		Test.StartTest();
		BillingAmountPopulationOnDMIGRPrintOLI objexecuteTrigger = new BillingAmountPopulationOnDMIGRPrintOLI();
        database.executebatch(objexecuteTrigger);
        Test.StopTest();
	}
}