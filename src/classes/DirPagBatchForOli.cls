global class DirPagBatchForOli implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
    String query='Select id,Under_Caption__c,DP_Under_Caption_F__c from Directory_Pagination__c where Order_Line_Item__c!=null and Under_Caption__c=null and DP_Under_Caption_F__c !=null';
    return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext bc, List<Directory_Pagination__c> DPList) {
        try{
        for(Directory_Pagination__c objDP:DPList){
        
                    objDP.Under_Caption__c=objDP.DP_Under_Caption_F__c ;  
                   
                    }
                    update DPList;
                    }catch(Exception e){
            System.debug('The exception is'+e);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Listing batch fail ');
        }
    }
    
    global void finish(Database.BatchableContext bc) {
    }
}