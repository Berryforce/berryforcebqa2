@isTest(seeAllData = true)
private class OpportunityLineItemHandlerTest {

    static testMethod void OpportunityLineItemTrigger() {
        List<Directory_Edition__c> lstDE = new List<Directory_Edition__c>();
        Test.startTest();
        Pricebook2 pricebook = TestMethodsUtility.createpricebook();
        Account Acc1 = TestMethodsUtility.createAccount('customer');
        Opportunity Opp1 = TestMethodsUtility.createOpportunity('new', Acc1);
        Product2 product = TestMethodsUtility.createproduct();      
        PriceBookEntry PBE = TestMethodsUtility.createpricebookentry(product.Id, pricebook.Id);
        Directory_Heading__c directoryHeading = TestMethodsUtility.createDirectoryHeading();
        Directory__c Directory = TestMethodsUtility.createDirectory();
        Telco__c Telco = TestMethodsUtility.createTelco(Acc1.Id);
        Directory_Mapping__c DirectoryMapping = TestMethodsUtility.createDirectoryMapping(Telco);
        Directory_Edition__c DirectoryEdition1 = TestMethodsUtility.generateDirectoryEdition(Directory.Id, Acc1.Primary_Canvass__c, Telco.Id, DirectoryMapping.Id);
        DirectoryEdition1.Book_Status__c = 'NI';
        DirectoryEdition1.Pub_Date__c=System.today().addMOnths(1);
        lstDE.add(DirectoryEdition1);
        Directory_Edition__c DirectoryEdition2 = TestMethodsUtility.generateDirectoryEdition(Directory.Id, Acc1.Primary_Canvass__c, Telco.Id, DirectoryMapping.Id);
        DirectoryEdition2.Book_Status__c = 'BOTS';
        DirectoryEdition2.Pub_Date__c=System.today().addMOnths(2);
        lstDE.add(DirectoryEdition2);
        insert lstDE;
        OpportunityLineItem OLI = TestMethodsUtility.generateOpportunityLineItem();
        OLI.PricebookEntryId = PBE.Id;
        OLI.OpportunityId = Opp1.Id;
        OLI.Heading__c = directoryHeading.Name;
        OLI.Package_ID__c = 'TestPackage';
        OLI.Directory__c = Directory.Id;
        insert OLI;
        Test.stopTest();
    }
    
    static testMethod void OpportunityLineItemTriggerUpdate() {
        List<Directory_Edition__c> lstDE = new List<Directory_Edition__c>();
        Test.startTest();
        Pricebook2 pricebook = TestMethodsUtility.createpricebook();
        Account Acc1 = TestMethodsUtility.createAccount('customer');
        Opportunity Opp1 = TestMethodsUtility.createOpportunity('new', Acc1);
        Opp1.Late_Print_Order_Approved__c = true;
        update Opp1;
        Product2 product = TestMethodsUtility.createproduct();      
        PriceBookEntry PBE = TestMethodsUtility.createpricebookentry(product.Id, pricebook.Id);
        Directory_Heading__c directoryHeading = TestMethodsUtility.createDirectoryHeading();
        Directory__c Directory = TestMethodsUtility.createDirectory();
        Telco__c Telco = TestMethodsUtility.createTelco(Acc1.Id);
        Directory_Mapping__c DirectoryMapping = TestMethodsUtility.createDirectoryMapping(Telco);
        Directory_Edition__c DirectoryEdition1 = TestMethodsUtility.generateDirectoryEdition(Directory.Id, Acc1.Primary_Canvass__c, Telco.Id, DirectoryMapping.Id);
        DirectoryEdition1.Book_Status__c = 'NI';
        DirectoryEdition1.Pub_Date__c=System.today().addMOnths(3);
        lstDE.add(DirectoryEdition1);
        Directory_Edition__c DirectoryEdition2 = TestMethodsUtility.generateDirectoryEdition(Directory.Id, Acc1.Primary_Canvass__c, Telco.Id, DirectoryMapping.Id);
        DirectoryEdition2.Book_Status__c = 'BOTS';
        DirectoryEdition2.Pub_Date__c=System.today().addMOnths(4);
        lstDE.add(DirectoryEdition2);
        insert lstDE;
        OpportunityLineItem OLI = TestMethodsUtility.generateOpportunityLineItem();
        OLI.PricebookEntryId = PBE.Id;
        OLI.OpportunityId = Opp1.Id;
        OLI.Heading__c = directoryHeading.Name;
        OLI.Package_ID__c = 'TestPackage';
        OLI.Directory__c = Directory.Id;
        insert OLI;
        update OLI;
        Test.stopTest();
    }
}