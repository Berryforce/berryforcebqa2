@IsTest (SeeAllData=true)
public class LetterRenewalControllerTest{
     
     static testMethod void letterRenewalTest(){
        
        Profile p = TestMethodsUtility.getSysAdminProfile();
        User Us = TestMethodsUtility.generateUser(p.Id,null);
        Us.LastName='Renewal';
        Us.FirstName ='Letter';
        Us.CommunityNickname ='LR';
        insert Us;
        
        Id RecordTypeId = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityLockRTName, CommonMessages.opportunityObjectName);    
        Database.BatchableContext bc;
        //system.runas(objUser){
            Account newAccountLR = TestMethodsUtility.generateAccount('customer');
            newAccountLR.Open_claim__c = false;
            newAccountLR.OwnerId = Us.Id;
            newAccountLR.Delinquency_Indicator__c= false;
            newAccountLR.Letter_Renewal_Sequence__c = '1';
            insert newAccountLR;
           
            
            Contact newContactLR = TestMethodsUtility.generateContact(newAccountLR.Id);        
            insert newContactLR;
          
            Opportunity newOpportunityLR = TestMethodsUtility.createOpportunity('new', newAccountLR);
           
            Directory__c newDirectory = TestMethodsUtility.createDirectory();
            
            List<Directory_Edition__c> lstDE = new List<Directory_Edition__c>();
            
            Directory_Edition__c newDE = TestMethodsUtility.generateDirectoryEdition(newDirectory.Id, newAccountLR.Primary_Canvass__c, null, null);
            newDE.Book_Status__c = 'NI';
            newDE.Letter_Renewal_Stage_1__c = System.today();
            newDE.Pub_Date__c = system.today().adddays(2);
            newDE.Bill_Prep__c= system.today().adddays(8);
            newDE.New_Print_Bill_Date__c= system.today().adddays(2);
            insert newDE;
            lstDE.add(newDE);
            
            Directory_Edition__c newDE1= TestMethodsUtility.generateDirectoryEdition(newDirectory.Id, newAccountLR.Primary_Canvass__c, null, null);
            newDE1.Book_Status__c = 'BOTS';
            insert newDE1;
            
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Print_Product_Type__c = 'Test';
            insert newProduct;
            
            Directory_Product_Mapping__c objDPM = new Directory_Product_Mapping__c();
            objDPM.Directory__c = newDirectory.Id;
            objDPM.Product2__c = newProduct.Id;
            objDPM.FullRate__c = 10;
            insert objDPM;
            
            Order__c newOrder = TestMethodsUtility.createOrder(newAccountLR.Id);
            
            Order_Group__c newOG = TestMethodsUtility.createOrderSet(newAccountLR, newOrder, newOpportunityLR);
            
            List<Order_Line_Items__c> lstOLI = new List<Order_Line_Items__c>();
            Order_Line_Items__c newOLI1LR = TestMethodsUtility.generateOrderLineItem(newAccountLR, newContactLR, newOpportunityLR, newOrder, newOG);
            newOLI1LR.Product2__c=newProduct.Id;
            newOLI1LR.is_p4p__c=false;
            newOLI1LR.Directory_Edition__c = newDE1.Id;
            newOLI1LR.UnitPrice__c=100;
            newOLI1LR.Media_type__c = 'Print';
            newOLI1LR.Order_Anniversary_Start_Date__c = date.today();
            newOLI1LR.Last_Billing_Date__c = system.today();
            newOLI1LR.Is_Handled__c = false;
            newOLI1LR.Cutomer_Cancel_Date__c=null;
            insert newOLI1LR;
            
        
            lstOLI=[Select Id,Name,Account__r.BillingStreet,Account__r.BillingCity,Account__r.BillingState,Account__r.BillingPostalCode,Cutomer_Cancel_Date__c,Account__r.Delinquency_Indicator__c,Account__r.Open_Claim__c,Account__r.Phone,Product_Type__c,Product2__r.Print_Product_Type__c,Billing_Contact__c,Billing_Contact__r.Name,UnitPrice__c,Order_Line_Total__c,Effective_Date__c,Is_P4P__c,Last_Billing_Date__c,Billing_Contact__r.Email,Directory_Edition__c,Discount__c,
                  Directory_Edition__r.Name,Listing__r.Phone__c,Listing__r.Name,Directory__r.Name,Directory_Edition__r.Letter_Renewal_Stage_1__c,Directory_Edition__r.Letter_Renewal_Stage_2__c,Account__c,Account__r.Name,Account__r.Letter_Renewal_Sequence__c,Product2__r.Name,Order_Anniversary_Start_Date__c,Is_Handled__c,Opportunity__c,Media_Type__c from Order_Line_Items__c where Id=:newOLI1LR.Id];
           system.assertequals(null,lstOLI[0].Cutomer_Cancel_Date__c);
            
            map<Id, list<Order_Line_Items__c>> mapTempOLI = new map<Id, list<Order_Line_Items__c>>();
           
            Test.startTest();
            
            LetterRenewalBatchController batchLR = new LetterRenewalBatchController();
            batchLR.start(bc);
            batchLR.execute(bc,lstDE);
            
            LetterRenewalReportScheduler lrRS= new LetterRenewalReportScheduler();
            schedulableContext sc;
            lrRS.execute(sc);
            
            LetterRenewalController_v1.letterRenewal(lstOLI);
            DirectoryEditionLetterRenewal.DELetterrenewl(newDE1.Id);
            LetterRenewalReportManualSchedule lrms = new LetterRenewalReportManualSchedule();
            lrms.doRun();
            Test.stopTest();
          //}
    }
  
}