@isTest
public class MediaTraxServiceForAddDirectoryTest{
    public static testmethod void MediaTraxAddDirectoryBatchTest(){
        //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());
        List<MediaTrax_API_Configuration__c> lstMedia = new list<MediaTrax_API_Configuration__c>();
        MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='GetAuthToken',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/authentication.cfc',HEADER_SOAPACTION__C='GetAuthentication',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='GetAuthenticationResponse',RETURN_METHOD__C='GetAuthenticationReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='',OPERATION__C='');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='AddDirectories',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/directories.cfc',HEADER_SOAPACTION__C='AddDirectories',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='AddDirectoriesResponse',RETURN_METHOD__C='AddDirectoriesReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='publisherIds,directoryNames,directoryNumbers,directoryNumber2s,telcoRecoNumbers,timeZoneIds',OPERATION__C='');     
        lstMedia.add(objMedia);
        insert lstMedia;
                
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        Account objAcc = TestMethodsUtility.generateAccount('customer');
        lstAccount.add(objAcc);
        objAcc = TestMethodsUtility.generateAccount('publication');
        objAcc.Media_Trax_Publisher_ID__c = '123';
        lstAccount.add(objAcc);
        
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        system.assertNotEquals(objTelco.Telco_Code__c, null);
        System.debug('Testingg objTelco '+objTelco);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        List<Directory__c> lstDir = [Select Id, Directory_Code__c, Name, Media_Trax_Publisher_ID__c, Media_Trax_Directory_ID__c, Telco_Provider_Code__c from Directory__c 
                where Directory_Code__c != null and Media_Trax_Publisher_ID__c != null and Telco_Provider_Code__c != null and Media_Trax_Directory_ID__c = null];
        System.debug('Testingg lstDir '+lstDir);
        
        lstDir = [Select Id, Directory_Code__c, Name, Media_Trax_Publisher_ID__c, Media_Trax_Directory_ID__c, Telco_Provider_Code__c from Directory__c];
        System.debug('Testingg lstDir '+lstDir);
        
        system.assertNotEquals(null, lstDir[0].Directory_Code__c);
        system.assertNotEquals(null, lstDir[0].Media_Trax_Publisher_ID__c);
        system.assertNotEquals(null, lstDir[0].Telco_Provider_Code__c);
        system.assertEquals(null, lstDir[0].Media_Trax_Directory_ID__c);
        
        MediaTraxServiceForAddDirectoryBatch objADCallout = new MediaTraxServiceForAddDirectoryBatch();
        database.executebatch(objADCallout );
    }
}