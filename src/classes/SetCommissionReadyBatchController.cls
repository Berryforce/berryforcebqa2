Global class SetCommissionReadyBatchController implements Database.Batchable<sObject>,Database.Stateful,Schedulable
{
    Global SetCommissionReadyBatchController(){}
   
    Global void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new SetCommissionReadyBatchController(), 2000);   
    }     
    Global Database.QueryLocator start(Database.BatchableContext bc){
       // set<Id> setOSID = new set<Id>();
       // setOSID.add('a26Z0000001CeRR');
      //string query='select RecordTypeId,id,Order_Group__c,Order_Group__r.Commission_Ready__c,Product2__r.Family,Product2__r.Product_Type__c, (Select RecordTypeId,id,Submitted__c,Miles_Status__c,Fulfillment_Submit_Status__c from Digital_Product_Requirements__r) from Order_Line_Items__c where isCanceled__c=False AND  Order_Group__r.Commission_Ready__c=FALSE order by Order_Group__c' ;
      //string query='select RecordTypeId,id,Order_Group__c,Order_Group__r.Commission_Ready__c,Product2__r.Family,Product2__r.Product_Type__c, (Select RecordTypeId,id,Submitted__c,Miles_Status__c,Fulfillment_Submit_Status__c from Digital_Product_Requirements__r) from Order_Line_Items__c where isCanceled__c=False AND  Order_Group__r.Commission_Ready__c=FALSE and Order_Group__c IN:setOSID order by Order_Group__c' ;
      Set<String> MOLIActionCodes = new Set<String> {CommonMessages.newRenewalAction, CommonMessages.renewRenewalAction, CommonMessages.modifyRenewalAction};
      string query = 'SELECT Id, Commission_Ready__c, (SELECT Id, Digital_Product_Requirement__r.Status__c, Digital_Product_Requirement__r.Id FROM ' +
      					'Order_Line_Items__r WHERE isCanceled__c = False), (SELECT Id, Action_Code__c, Digital_Product_Requirement__r.Status__c, ' + 
      					'Digital_Product_Requirement__c FROM Modification_Order_Line_Items__r WHERE Action_Code__c IN: MOLIActionCodes) FROM Order_Group__c ' + 
      					'WHERE Commission_Ready__c = FALSE';
      return database.getQuerylocator(query);
    }
    Global void execute(Database.BatchableContext bc, List<Order_Group__c> lstOS) {
        List<Order_Group__c> orderSetListForUpdate=new List<Order_Group__c> ();
        list<Order_Line_Items__c> tempOLIList = new list<Order_Line_Items__c>();
        list<Modification_Order_Line_Item__c> tempMOLIList = new list<Modification_Order_Line_Item__c>();
        Set<Id> DFFIds = new Set<Id>();
        Map<Id, List<Track_DFF_Update__c>> mapDFFIdListTrackDFFUpdate = new Map<Id, List<Track_DFF_Update__c>>();
        
        for(Order_Group__c objOS:lstOS) {
	        if(tempMOLIList.size() > 0) {
	        	tempMOLIList = objOS.Modification_Order_Line_Items__r;
	        	for(Modification_Order_Line_Item__c tempMOLI : tempMOLIList) {
	        		if(tempMOLI.Action_Code__c == CommonMessages.modifyRenewalAction) {
	        			DFFIds.add(tempMOLI.Digital_Product_Requirement__c);
	        		}
	        	}
	        }
        }
        
        if(DFFIds.size() > 0) {
        	List<Track_DFF_Update__c> listTrackDFFUpdate = new List<Track_DFF_Update__c>();
        	listTrackDFFUpdate = TrackDFFUpdateSOQLMethods.fetchTrackDFFUpdatesByDFFIds(DFFIds);
        	
        	if(listTrackDFFUpdate.size() > 0) {
        		for(Track_DFF_Update__c objTrackDFF : listTrackDFFUpdate) {
        			if(!mapDFFIdListTrackDFFUpdate.containsKey(objTrackDFF.Id)) {
        				mapDFFIdListTrackDFFUpdate.put(objTrackDFF.Id, new List<Track_DFF_Update__c>());
        			}
        			mapDFFIdListTrackDFFUpdate.get(objTrackDFF.Id).add(objTrackDFF);
        		}
        	}
        }
        
        for(Order_Group__c objOS:lstOS) {
            integer count=0;
            integer countForDFF=0;
            system.debug(objOS);
            tempOLIList = objOS.Order_Line_Items__r;
            tempMOLIList = objOS.Modification_Order_Line_Items__r;
            Boolean bFlag = true;
            /*if(tempOLIList.size() > 0) {
                for(Order_Line_Items__c tempOLI : tempOLIList) {
                    if((tempOLI.Digital_Product_Requirement__r.Status__c != 'Complete')) {
                        count++;
                        break;
                    }
                    else {
                        countForDFF++;
                    }
                    if(count > 0) {
                        break;
                    }
                }
            }
            if(count == 0 && countForDFF > 0) {
                orderSetListForUpdate.add(new Order_Group__c(Id=objOS.id,Commission_Ready__c = true));
            }*/
            
            if(tempOLIList.size() > 0) {
            	for(Order_Line_Items__c tempOLI : tempOLIList) {
            		if(tempOLI.Digital_Product_Requirement__r.Status__c != 'Complete') {
            			bFlag = false;
            			break;
            		}
            	}
            	
            	if(bFlag) {
            		if(tempMOLIList.size() > 0) {
	            		for(Modification_Order_Line_Item__c tempMOLI : tempMOLIList) {
	            			if(tempMOLI.Action_Code__c == CommonMessages.newRenewalAction || tempMOLI.Action_Code__c == CommonMessages.renewRenewalAction) {
	            				if(tempMOLI.Digital_Product_Requirement__r.Status__c != 'Complete') {
	            					bFlag = false;
	            					break;
	            				}
	            			} else if(tempMOLI.Action_Code__c == CommonMessages.modifyRenewalAction) {
	            				if(!mapDFFIdListTrackDFFUpdate.containsKey(tempMOLI.Digital_Product_Requirement__c)) {
	            					bFlag = false;
	            					break;
	            				}
	            			}
	            		}
            		}
            	}
            }
            if(bFlag && (tempOLIList.size() > 0 || tempMOLIList.size() > 0)) {
            	orderSetListForUpdate.add(new Order_Group__c(Id=objOS.id,Commission_Ready__c = true));
            }
        }
        if(orderSetListForUpdate.size()>0) {
            update orderSetListForUpdate;
        }     
    }
    Global void finish(Database.BatchableContext bc){}
}