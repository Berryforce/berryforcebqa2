/****************************************************
Apex Class to Schedule YpcAddonPatch Batch Job
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 08/27/2015
$Id$
*****************************************************/
global class ScheduleYpcAddonPatchHndlr implements ScheduleYpcAddonPatch.ScheduleYpcAddonPatchInterface {
    //public static String sched = '0 00 23 * * ?'; //batch will run every night

    //global static String scheduleBatch() {
          //ScheduleYpcAddonPatch SC = new ScheduleYpcAddonPatch();
          //return System.schedule('Ypc Addon Patch Job', sched, SC);
    //}

    global void execute(SchedulableContext sc) {

        //Batch size from custom settings
        Batch_Size__c objBatch = Batch_Size__c.getInstance('YpcAddon'); 

        //Query to pick all YPC add-on records and pass it to YpcAddonPatch class
        String query = 'SELECT Id, Name, udac__c, udac_points__c, add_ons__c, vid_add_on__c, coupon_description__c, coupon_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c, cslt_url__c, cslt_text__c, recordType.DeveloperName, Account__r.TalusAccountId__c, Talus_Subscription_Id__c, Talus_DFF_Id__c, OrderLineItemID__r.Effective_Date__c, (SELECT Id, Name, DFF__c, DFF_Id__c, Validation__c, Effective_Date__c, Action_Type__c, coupon_description__c, coupon_url__c, cslt_text__c, cslt_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c from YPC_AddOns__r) from Digital_Product_Requirement__c where Account__r.TalusAccountId__c != null and Talus_Subscription_Id__c != null and Id IN(SELECT DFF__c from YPC_AddOn__c)';
        //System.debug('************StringQuery************' + query);
        
        YpcAddonPatch obj = new YpcAddonPatch(query);
        ID batchprocessid = database.executebatch(obj, Integer.valueOf(objBatch.Size__c));

    }
}