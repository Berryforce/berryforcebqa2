public class SubsequentReconcilationReport 
{
public string rtype{get;set;}
public string sdate{get;set;}
public string edate{get;set;}
public string isCreditCard{get;set;}
public String isStatement{get;set;}
public String billingEntity{get;set;}
public String RenderType{get;set;}
String Reporttype;
public boolean showurlexport{get;set;}
public boolean showexport{get;set;}
public Boolean showblock{get;set;}
transient public List<MonthlyToSingleWrap> M2SWrapper {get;set;}
transient public List<SingleToMonthlyWrap> S2MWrapper {get;set;}
//transient public List<AllInvoiceWrap> AllInvoiceWrapper {get;set;}
public boolean showReport{get;set;}
public boolean showSearchPanel{get;set;}

public SubsequentReconcilationReport()
{
      rtype=Apexpages.currentpage().getparameters().get('rtype');
      sdate=Apexpages.currentpage().getparameters().get('sdate');
      edate=Apexpages.currentpage().getparameters().get('edate');
      isCreditCard=Apexpages.currentpage().getparameters().get('isCreditCard');
      isStatement=Apexpages.currentpage().getparameters().get('isStatement');
      billingEntity=Apexpages.currentpage().getparameters().get('billingEntity');
      System.debug(rtype+'###########'+sdate+'##########'+edate);
      M2SWrapper=new List<MonthlyToSingleWrap>();
      S2MWrapper=new List<SingleToMonthlyWrap>();
      //AllInvoiceWrapper =new List<AllInvoiceWrap>();
      RenderType='text/html';
      showurlexport=false;
      showexport=false;
      showblock=true;
      if(rtype!=null && sdate!=null && edate!=null)
      {
        generateSubsequentReport(rtype,sdate,edate);
        showSearchPanel=false;
        showReport=true;
        showurlexport=true;
      }
      else
      {
          showReport=false;
          showSearchPanel=true;
      }
}
public List<SelectOption> getReporttypeOptions() 
{
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('0','--None--'));
    options.add(new SelectOption('Digital','Digital'));
    options.add(new SelectOption('Print','Print'));
    return options;
} 
public String getReporttype() 
{
    return Reporttype;
} 
public void setReporttype(String Reporttype) 
{
    this.Reporttype= Reporttype;
    rtype=Reporttype;
            
} 
public void generateReport()
{
   if(Reporttype!=null && sdate!=null && edate!=null)
   {
       generateSubsequentReport(Reporttype,sdate,edate);
   }
}
public void generateSubsequentReport(String Rtype,String sdate, String edate)
{
    List<c2g__codaInvoiceLineItem__c> IterativeILI=new List<c2g__codaInvoiceLineItem__c>();
    M2SWrapper=new List<MonthlyToSingleWrap>();
    S2MWrapper=new List<SingleToMonthlyWrap>();
    //AllInvoiceWrapper =new List<AllInvoiceWrap>();
    Integer startday=Date.parse(sdate).day();
    Integer endday=Date.parse(edate).day();
    Map<Id,Order_Line_Items__c> MapOLI =new Map<Id,Order_Line_Items__c>();
    Date startDate=Date.parse(sdate).addDays(-30);
    Date endDate=Date.parse(sdate);
    
    system.debug('ssssssssss'+startDate);
    system.debug('eeeeeeeeeee'+endDate);
    if(Rtype!='Print'){
            if(isCreditCard!=null && isCreditCard=='true'){
                MapOLI =new Map<Id,Order_Line_Items__c> ([Select id,Account__r.Account_Number__c,Canvass__r.Name,Total_Prorate__c,Prorate_Credit__c,Directory__r.Name,Directory_Edition__r.Name,Account__r.Name,Name,Prorate_Credit_Days__c,Number_of_Months_Transferred__c,Total_Prorated_Days_for_contract__c,Billing_Contact__r.Phone,UnitPrice__c,Description__c,Order_Line_Total__c,Product2__r.Name,Service_End_Date__c,Service_Start_Date__c,Talus_Go_Live_Date__c,ProductCode__c,Product2__c,Billing_Frequency__c,Line_Number__c,Product2__r.ProductCode,Payment_Method__c,Successful_Payments__c,Payments_Remaining__c,RecordtypeId,Billing_Frequency_Change__c,Billing_Frequency_Change_Date__c,Payment_Duration__c,Account__r.Last_Billing_Transfer_Date__c,Last_Invoice__c,Current_Invoice__c,(SELECT c2g__NetValue__c,c2g__Invoice__r.Name,c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,c2g__Invoice__c,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM Sales_Invoice_Line_Items__r where  c2g__Invoice__r.c2g__InvoiceDate__c>=:Date.parse(sdate)  AND c2g__Invoice__r.c2g__InvoiceDate__c<=:Date.parse(edate) AND c2g__Product__r.ProductCode!='SFEE1') from Order_Line_Items__c  where Product2__r.Media_Type__c=:Rtype AND Service_Start_Date__c>=:Date.parse(sdate) AND Service_Start_Date__c<=:Date.parse(edate) AND isCanceled__c=false AND Is_P4P__c=false AND Payment_Method__c='Credit Card' AND (Billing_Frequency_Change_Date__c!=null OR (Successful_Payments__c>=1 AND (Payments_Remaining__c>0 OR (Payments_Remaining__c=0 AND Continious_Billing__c=true ))))]);
            }
            else if(isStatement!=null && isStatement=='true'){
                MapOLI =new Map<Id,Order_Line_Items__c> ([Select id,Account__r.Account_Number__c,Canvass__r.Name,Total_Prorate__c,Prorate_Credit__c,Directory__r.Name,Directory_Edition__r.Name,Account__r.Name,Name,Prorate_Credit_Days__c,Number_of_Months_Transferred__c,Total_Prorated_Days_for_contract__c,Billing_Contact__r.Phone,UnitPrice__c,Description__c,Order_Line_Total__c,Product2__r.Name,Service_End_Date__c,Service_Start_Date__c,Talus_Go_Live_Date__c,ProductCode__c,Product2__c,Billing_Frequency__c,Line_Number__c,Product2__r.ProductCode,Payment_Method__c,Successful_Payments__c,Payments_Remaining__c,RecordtypeId,Billing_Frequency_Change__c,Billing_Frequency_Change_Date__c,Payment_Duration__c,Account__r.Last_Billing_Transfer_Date__c,Last_Invoice__c,Current_Invoice__c,(SELECT c2g__NetValue__c,c2g__Invoice__r.Name,c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,c2g__Invoice__c,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM Sales_Invoice_Line_Items__r where  c2g__Invoice__r.c2g__InvoiceDate__c>=:Date.parse(sdate)  AND c2g__Invoice__r.c2g__InvoiceDate__c<=:Date.parse(edate) AND c2g__Product__r.ProductCode!='SFEE1') from Order_Line_Items__c  where Product2__r.Media_Type__c=:Rtype AND Service_Start_Date__c>=:Date.parse(sdate) AND Service_Start_Date__c<=:Date.parse(edate) AND isCanceled__c=false AND Is_P4P__c=false AND Payment_Method__c='Statement' AND (Billing_Frequency_Change_Date__c!=null OR (Successful_Payments__c>=1 AND (Payments_Remaining__c>0 OR (Payments_Remaining__c=0 AND Continious_Billing__c=true ))))]);
            }
            else{
                System.debug('@@@@@@@@@@@@@@'+billingEntity);
                if(billingEntity!=null)
                 MapOLI =new Map<Id,Order_Line_Items__c> ([Select id,Account__r.Account_Number__c,Canvass__r.Name,Total_Prorate__c,Prorate_Credit__c,Directory__r.Name,Directory_Edition__r.Name,Account__r.Name,Name,Prorate_Credit_Days__c,Number_of_Months_Transferred__c,Total_Prorated_Days_for_contract__c,Billing_Contact__r.Phone,UnitPrice__c,Description__c,Order_Line_Total__c,Product2__r.Name,Service_End_Date__c,Service_Start_Date__c,Talus_Go_Live_Date__c,ProductCode__c,Product2__c,Billing_Frequency__c,Line_Number__c,Product2__r.ProductCode,Payment_Method__c,Successful_Payments__c,Payments_Remaining__c,RecordtypeId,Billing_Frequency_Change__c,Billing_Frequency_Change_Date__c,Payment_Duration__c,Account__r.Last_Billing_Transfer_Date__c,Last_Invoice__c,Current_Invoice__c,(SELECT c2g__NetValue__c,c2g__Invoice__r.Name,c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,c2g__Invoice__c,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM Sales_Invoice_Line_Items__r where  c2g__Invoice__r.c2g__InvoiceDate__c>=:Date.parse(sdate)  AND c2g__Invoice__r.c2g__InvoiceDate__c<=:Date.parse(edate) AND c2g__Product__r.ProductCode!='SFEE1' AND c2g__Invoice__r.Billing_Entity__c=:billingEntity) from Order_Line_Items__c  where Product2__r.Media_Type__c=:Rtype AND Service_Start_Date__c>=:Date.parse(sdate) AND Service_Start_Date__c<=:Date.parse(edate) AND Opportunity__r.Account_Primary_Canvass__r.Billing_Entity__c=:billingEntity AND isCanceled__c=false AND Billing_Partner__c!='THE BERRY COMPANY' AND (Billing_Frequency_Change_Date__c!=null OR (Successful_Payments__c>=1 AND (Payments_Remaining__c>0 OR (Payments_Remaining__c=0 AND Continious_Billing__c=true ))))]);
                else
                 MapOLI =new Map<Id,Order_Line_Items__c> ([Select id,Account__r.Account_Number__c,Canvass__r.Name,Total_Prorate__c,Prorate_Credit__c,Directory__r.Name,Directory_Edition__r.Name,Account__r.Name,Name,Prorate_Credit_Days__c,Number_of_Months_Transferred__c,Total_Prorated_Days_for_contract__c,Billing_Contact__r.Phone,UnitPrice__c,Description__c,Order_Line_Total__c,Product2__r.Name,Service_End_Date__c,Service_Start_Date__c,Talus_Go_Live_Date__c,ProductCode__c,Product2__c,Billing_Frequency__c,Line_Number__c,Product2__r.ProductCode,Payment_Method__c,Successful_Payments__c,Payments_Remaining__c,RecordtypeId,Billing_Frequency_Change__c,Billing_Frequency_Change_Date__c,Payment_Duration__c,Account__r.Last_Billing_Transfer_Date__c,Last_Invoice__c,Current_Invoice__c,(SELECT c2g__NetValue__c,c2g__Invoice__r.Name,c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,c2g__Invoice__c,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM Sales_Invoice_Line_Items__r where  c2g__Invoice__r.c2g__InvoiceDate__c>=:Date.parse(sdate)  AND c2g__Invoice__r.c2g__InvoiceDate__c<=:Date.parse(edate) AND c2g__Product__r.ProductCode!='SFEE1') from Order_Line_Items__c  where Product2__r.Media_Type__c=:Rtype AND Service_Start_Date__c>=:Date.parse(sdate) AND Service_Start_Date__c<=:Date.parse(edate) AND isCanceled__c=false AND Billing_Partner__c!='THE BERRY COMPANY' AND (Billing_Frequency_Change_Date__c!=null OR (Successful_Payments__c>=1 AND (Payments_Remaining__c>0 OR (Payments_Remaining__c=0 AND Continious_Billing__c=true ))))]);
            }
        }
        else{
            
            //Modified by Mythili for ticket ATP-4315
            //MapOLI =new Map<Id,Order_Line_Items__c> ([Select id,Account__r.Account_Number__c,Canvass__r.Name,Total_Prorate__c,Prorate_Credit__c,Directory__r.Name,Directory_Edition__r.Name,Account__r.Name,Name,Prorate_Credit_Days__c,Number_of_Months_Transferred__c,Total_Prorated_Days_for_contract__c,Billing_Contact__r.Phone,UnitPrice__c,Description__c,Order_Line_Total__c,Product2__r.Name,Service_End_Date__c,Service_Start_Date__c,Talus_Go_Live_Date__c,ProductCode__c,Product2__c,Billing_Frequency__c,Line_Number__c,Product2__r.ProductCode,Payment_Method__c,Successful_Payments__c,Payments_Remaining__c,RecordtypeId,Billing_Frequency_Change__c,Billing_Frequency_Change_Date__c,Payment_Duration__c,Account__r.Last_Billing_Transfer_Date__c,Last_Invoice__c,Current_Invoice__c,(SELECT c2g__NetValue__c,c2g__Invoice__r.Name,c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,c2g__Invoice__c,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM Sales_Invoice_Line_Items__r where c2g__Invoice__r.c2g__InvoiceDate__c=:Date.parse(sdate) AND c2g__Product__r.ProductCode!='SFEE1') from Order_Line_Items__c  where Product2__r.Media_Type__c=:Rtype AND Service_Start_Date__c =:Date.parse(sdate) AND isCanceled__c=false AND (Billing_Frequency_Change_Date__c!=null OR (Successful_Payments__c>1 AND (Payments_Remaining__c>0 OR (Payments_Remaining__c=0 AND Contract_End_Date__c>=TODAY AND Billing_Frequency__c='Monthly'))))]);
            MapOLI =new Map<Id,Order_Line_Items__c> ([Select id,Account__r.Account_Number__c,Canvass__r.Name,Total_Prorate__c,Prorate_Credit__c,Directory__r.Name,Directory_Edition__r.Name,Account__r.Name,Name,Prorate_Credit_Days__c,Number_of_Months_Transferred__c,Total_Prorated_Days_for_contract__c,Billing_Contact__r.Phone,UnitPrice__c,Description__c,Order_Line_Total__c,Product2__r.Name,Service_End_Date__c,Service_Start_Date__c,Talus_Go_Live_Date__c,ProductCode__c,Product2__c,Billing_Frequency__c,Line_Number__c,Product2__r.ProductCode,Payment_Method__c,Successful_Payments__c,Payments_Remaining__c,RecordtypeId,Billing_Frequency_Change__c,Billing_Frequency_Change_Date__c,Payment_Duration__c,Account__r.Last_Billing_Transfer_Date__c,Last_Invoice__c,Current_Invoice__c,(SELECT c2g__NetValue__c,c2g__Invoice__r.Name,c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,c2g__Invoice__c,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM Sales_Invoice_Line_Items__r where c2g__Invoice__r.c2g__InvoiceDate__c=:Date.parse(sdate) AND c2g__Product__r.ProductCode!='SFEE1') from Order_Line_Items__c  where Product2__r.Media_Type__c=:Rtype AND Service_Start_Date__c =:Date.parse(sdate) AND isCanceled__c=false AND Billing_Frequency_Change_Date__c!=null AND Billing_Frequency_Change_Date__c>:startDate AND Billing_Frequency_Change_Date__c<:endDate AND Contract_End_Date__c>=TODAY]);
            system.debug('***********'+MapOLI);
        }
    /*
    
    if(endday>=startday){
        if(Rtype!='Print'){
            MapOLI =new Map<Id,Order_Line_Items__c> ([Select id,Account__r.Account_Number__c,Canvass__r.Name,Total_Prorate__c,Prorate_Credit__c,Directory__r.Name,Directory_Edition__r.Name,Account__r.Name,Name,Prorate_Credit_Days__c,Number_of_Months_Transferred__c,Total_Prorated_Days_for_contract__c,Billing_Contact__r.Phone,UnitPrice__c,Description__c,Order_Line_Total__c,Product2__r.Name,Service_End_Date__c,Service_Start_Date__c,Talus_Go_Live_Date__c,ProductCode__c,Product2__c,Billing_Frequency__c,Line_Number__c,Product2__r.ProductCode,Payment_Method__c,Successful_Payments__c,Payments_Remaining__c,RecordtypeId,Billing_Frequency_Change__c,Billing_Frequency_Change_Date__c,Payment_Duration__c,Account__r.Last_Billing_Transfer_Date__c,Last_Invoice__c,Current_Invoice__c,(SELECT c2g__NetValue__c,c2g__Invoice__r.Name,c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,c2g__Invoice__c,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM Sales_Invoice_Line_Items__r where c2g__Invoice__r.c2g__InvoiceDate__c>=:Date.parse(sdate)  AND c2g__Invoice__r.c2g__InvoiceDate__c<=:Date.parse(edate) AND c2g__Product__r.ProductCode!='SFEE1') from Order_Line_Items__c  where Product2__r.Media_Type__c=:Rtype AND DAY_IN_MONTH(Account__r.Billing_Anniversary_Date__c) <=: endday AND DAY_IN_MONTH(Account__r.Billing_Anniversary_Date__c) >=: startday AND isCanceled__c=false AND (Billing_Frequency_Change_Date__c!=null OR (Successful_Payments__c>=1 AND (Payments_Remaining__c>0 OR (Payments_Remaining__c=0 AND Continious_Billing__c=true))))]);
        }
        else{
            MapOLI =new Map<Id,Order_Line_Items__c> ([Select id,Account__r.Account_Number__c,Canvass__r.Name,Total_Prorate__c,Prorate_Credit__c,Directory__r.Name,Directory_Edition__r.Name,Account__r.Name,Name,Prorate_Credit_Days__c,Number_of_Months_Transferred__c,Total_Prorated_Days_for_contract__c,Billing_Contact__r.Phone,UnitPrice__c,Description__c,Order_Line_Total__c,Product2__r.Name,Service_End_Date__c,Service_Start_Date__c,Talus_Go_Live_Date__c,ProductCode__c,Product2__c,Billing_Frequency__c,Line_Number__c,Product2__r.ProductCode,Payment_Method__c,Successful_Payments__c,Payments_Remaining__c,RecordtypeId,Billing_Frequency_Change__c,Billing_Frequency_Change_Date__c,Payment_Duration__c,Account__r.Last_Billing_Transfer_Date__c,Last_Invoice__c,Current_Invoice__c,(SELECT c2g__NetValue__c,c2g__Invoice__r.Name,c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,c2g__Invoice__c,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM Sales_Invoice_Line_Items__r where c2g__Invoice__r.c2g__InvoiceDate__c>=:Date.parse(sdate)  AND c2g__Invoice__r.c2g__InvoiceDate__c<=:Date.parse(edate) AND c2g__Product__r.ProductCode!='SFEE1') from Order_Line_Items__c  where Product2__r.Media_Type__c=:Rtype AND DAY_IN_MONTH(Account__r.Billing_Anniversary_Date__c) <=: endday AND DAY_IN_MONTH(Account__r.Billing_Anniversary_Date__c) >=: startday AND isCanceled__c=false AND (Billing_Frequency_Change_Date__c!=null OR (Successful_Payments__c>1 AND Payments_Remaining__c>0))]);
        }
    }
    else{
        Integer monthsBetween = Date.parse(sdate).monthsBetween(Date.parse(edate));
        if(monthsBetween >1){
            if(Rtype!='Print'){
                MapOLI =new Map<Id,Order_Line_Items__c> ([Select id,Account__r.Account_Number__c,Canvass__r.Name,Total_Prorate__c,Prorate_Credit__c,Directory__r.Name,Directory_Edition__r.Name,Account__r.Name,Name,Prorate_Credit_Days__c,Number_of_Months_Transferred__c,Total_Prorated_Days_for_contract__c,Billing_Contact__r.Phone,UnitPrice__c,Description__c,Order_Line_Total__c,Product2__r.Name,Service_End_Date__c,Service_Start_Date__c,Talus_Go_Live_Date__c,ProductCode__c,Product2__c,Billing_Frequency__c,Line_Number__c,Product2__r.ProductCode,Payment_Method__c,Successful_Payments__c,Payments_Remaining__c,RecordtypeId,Billing_Frequency_Change__c,Billing_Frequency_Change_Date__c,Payment_Duration__c,Account__r.Last_Billing_Transfer_Date__c,Last_Invoice__c,Current_Invoice__c,(SELECT c2g__NetValue__c,c2g__Invoice__r.Name,c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,c2g__Invoice__c,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM Sales_Invoice_Line_Items__r where c2g__Invoice__r.c2g__InvoiceDate__c>=:Date.parse(sdate)  AND c2g__Invoice__r.c2g__InvoiceDate__c<=:Date.parse(edate) AND c2g__Product__r.ProductCode!='SFEE1') from Order_Line_Items__c  where Product2__r.Media_Type__c=:Rtype AND DAY_IN_MONTH(Account__r.Billing_Anniversary_Date__c) <=: startday AND DAY_IN_MONTH(Account__r.Billing_Anniversary_Date__c) >=: endday AND isCanceled__c=false AND (Billing_Frequency_Change_Date__c!=null OR (Successful_Payments__c>=1 AND (Payments_Remaining__c>0 OR (Payments_Remaining__c=0 AND Continious_Billing__c=true))))]);
            }
            else{
                MapOLI =new Map<Id,Order_Line_Items__c> ([Select id,Account__r.Account_Number__c,Canvass__r.Name,Total_Prorate__c,Prorate_Credit__c,Directory__r.Name,Directory_Edition__r.Name,Account__r.Name,Name,Prorate_Credit_Days__c,Number_of_Months_Transferred__c,Total_Prorated_Days_for_contract__c,Billing_Contact__r.Phone,UnitPrice__c,Description__c,Order_Line_Total__c,Product2__r.Name,Service_End_Date__c,Service_Start_Date__c,Talus_Go_Live_Date__c,ProductCode__c,Product2__c,Billing_Frequency__c,Line_Number__c,Product2__r.ProductCode,Payment_Method__c,Successful_Payments__c,Payments_Remaining__c,RecordtypeId,Billing_Frequency_Change__c,Billing_Frequency_Change_Date__c,Payment_Duration__c,Account__r.Last_Billing_Transfer_Date__c,Last_Invoice__c,Current_Invoice__c,(SELECT c2g__NetValue__c,c2g__Invoice__r.Name,c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,c2g__Invoice__c,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM Sales_Invoice_Line_Items__r where c2g__Invoice__r.c2g__InvoiceDate__c>=:Date.parse(sdate)  AND c2g__Invoice__r.c2g__InvoiceDate__c<=:Date.parse(edate) AND c2g__Product__r.ProductCode!='SFEE1') from Order_Line_Items__c  where Product2__r.Media_Type__c=:Rtype AND DAY_IN_MONTH(Account__r.Billing_Anniversary_Date__c) <=: startday AND DAY_IN_MONTH(Account__r.Billing_Anniversary_Date__c) >=: endday AND isCanceled__c=false AND (Billing_Frequency_Change_Date__c!=null OR (Successful_Payments__c>1 AND Payments_Remaining__c>0))]);
            }
        }
    }
    
    */
    
    if(MapOLI.size()>0){
            
            for(Order_Line_Items__c oliIterator : MapOLI.values()){
                    if(oliIterator.UnitPrice__c==null) oliIterator.UnitPrice__c=0;
                    if(oliIterator.Successful_Payments__c==null) oliIterator.Successful_Payments__c=0;
                    if(oliIterator.Payments_Remaining__c==null) oliIterator.Payments_Remaining__c=0;
                    if(oliIterator.Payment_Duration__c==null) oliIterator.Payment_Duration__c=0;
                    if(oliIterator.Last_Invoice__c==null) oliIterator.Last_Invoice__c=0.00;
                    if(oliIterator.Current_Invoice__c==null) oliIterator.Current_Invoice__c=0.00;
                    if(oliIterator.Prorate_Credit_Days__c==null) oliIterator.Prorate_Credit_Days__c=0;
                    if(oliIterator.Prorate_Credit__c==null) oliIterator.Prorate_Credit__c = 0;
                    Double ExpectedInvoice=oliIterator.UnitPrice__c;
                    Double OLICurrentValue=0.00;
                    boolean IsInvoiced=false;
                    if(oliIterator.Sales_Invoice_Line_Items__r.size()>0){
                           IsInvoiced=true;
                           for(c2g__codaInvoiceLineItem__c sili: oliIterator.Sales_Invoice_Line_Items__r)
                           {
                               if(sili.c2g__UnitPrice__c>0) 
                               {
                                 OLICurrentValue=OLICurrentValue+sili.c2g__UnitPrice__c;
                               }
                               else{
                                OLICurrentValue=OLICurrentValue-sili.c2g__UnitPrice__c;
                               }
                           }   
                           
                           if(rtype!='Print'){
                               if(oliIterator.Payments_Remaining__c==10 && oliIterator.Successful_Payments__c==2 && oliIterator.Prorate_Credit__c>0){
                                    ExpectedInvoice=(oliIterator.UnitPrice__c-oliIterator.Prorate_Credit__c)+oliIterator.UnitPrice__c;
                                    oliIterator.Current_Invoice__c=oliIterator.Current_Invoice__c+oliIterator.Last_Invoice__c;
                               }
                               else if(oliIterator.Payments_Remaining__c>0 && oliIterator.Successful_Payments__c==2){
                                    ExpectedInvoice=oliIterator.UnitPrice__c-oliIterator.Prorate_Credit__c;
                               }
                               else if(oliIterator.Payments_Remaining__c==0 && oliIterator.Successful_Payments__c==1){
                                   ExpectedInvoice = (oliIterator.UnitPrice__c - oliIterator.Prorate_Credit__c) * oliIterator.Payment_Duration__c;
                               }
                               else{
                                   ExpectedInvoice = oliIterator.UnitPrice__c;
                               }
                           }
                    }
                    
                    if(IsInvoiced){
                            
                        if(oliIterator.Billing_Frequency_Change_Date__c!=null && oliIterator.Service_Start_Date__c!=null)
                        {
                           // if(oliIterator.Billing_Frequency_Change_Date__c>oliIterator.Service_Start_Date__c.adddays(-30) && oliIterator.Billing_Frequency_Change_Date__c<oliIterator.Service_Start_Date__c)
                          //  if(oliIterator.Billing_Frequency_Change_Date__c>Date.parse(sdate).adddays(-30) && oliIterator.Billing_Frequency_Change_Date__c<Date.parse(sdate))
                           // {
                                if(oliIterator.Billing_Frequency__c=='Single Payment')
                                {
                                    if(rtype=='Print') ExpectedInvoice=(oliIterator.Payment_Duration__c-oliIterator.Successful_Payments__c+1)*oliIterator.UnitPrice__c;
                                    oliIterator.Number_of_Months_Transferred__c=oliIterator.Payments_Remaining__c;
                                    //Build Wrapper for Monthly to Single
                                    M2SWrapper.add(new MonthlyToSingleWrap(String.valueof(oliIterator.Name),String.valueof(oliIterator.ProductCode__c),String.valueof(oliIterator.Description__c),
                                    Double.valueof(oliIterator.UnitPrice__c),Integer.valueof(oliIterator.Successful_Payments__c),Integer.valueof(oliIterator.Payments_Remaining__c),
                                    Integer.valueof(oliIterator.Payment_Duration__c),Integer.valueof(oliIterator.Number_of_Months_Transferred__c),Integer.valueof(oliIterator.Prorate_Credit_Days__c),Double.valueof(OLICurrentValue),
                                    'Monthly','Single',ExpectedInvoice,Double.valueof(oliIterator.Current_Invoice__c),oliIterator));
                                    showReport=true;
                                }
                                else if(oliIterator.Billing_Frequency__c=='Monthly')
                                {
                                    oliIterator.Number_of_Months_Transferred__c=1;
                                    //Build Wrapper for Single to Monthly
                                    S2MWrapper.add(new SingleToMonthlyWrap(String.valueof(oliIterator.Name),String.valueof(oliIterator.ProductCode__c),String.valueof(oliIterator.Description__c),
                                    Double.valueof(oliIterator.UnitPrice__c),Integer.valueof(oliIterator.Successful_Payments__c),Integer.valueof(oliIterator.Payments_Remaining__c),
                                    Integer.valueof(oliIterator.Payment_Duration__c),Integer.valueof(oliIterator.Number_of_Months_Transferred__c),Integer.valueof(oliIterator.Prorate_Credit_Days__c),Double.valueof(OLICurrentValue),
                                    'Single','Monthly',ExpectedInvoice,Double.valueof(oliIterator.Current_Invoice__c),oliIterator));
                                    showReport=true;
                                }
                                else
                                {
                                   // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'No Result Found.');
                                   // ApexPages.addMessage(myMsg);
                                   /* //Build Wrapper for All Invoice 
                                    AllInvoiceWrapper.add(new AllInvoiceWrap(String.valueof(oliIterator.Name),String.valueof(oliIterator.ProductCode__c),String.valueof(oliIterator.Description__c),
                                    Double.valueof(oliIterator.UnitPrice__c),Integer.valueof(oliIterator.Successful_Payments__c),Integer.valueof(oliIterator.Payments_Remaining__c),
                                    Integer.valueof(oliIterator.Payment_Duration__c),Integer.valueof(oliIterator.Number_of_Months_Transferred__c),Integer.valueof(oliIterator.Prorate_Credit_Days__c),Double.valueof(OLICurrentValue),
                                    '','',ExpectedInvoice,Double.valueof(oliIterator.Current_Invoice__c),oliIterator));
                                    showReport=true;
                                  */
                                }
                           // }
                           // else
                           // {
                                  //  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'No Result Found.');
                                  //  ApexPages.addMessage(myMsg);
                                    //Build Wrapper for All Invoice 
                                  /*  AllInvoiceWrapper.add(new AllInvoiceWrap(String.valueof(oliIterator.Name),String.valueof(oliIterator.ProductCode__c),String.valueof(oliIterator.Description__c),
                                    Double.valueof(oliIterator.UnitPrice__c),Integer.valueof(oliIterator.Successful_Payments__c),Integer.valueof(oliIterator.Payments_Remaining__c),
                                    Integer.valueof(oliIterator.Payment_Duration__c),Integer.valueof(oliIterator.Number_of_Months_Transferred__c),Integer.valueof(oliIterator.Prorate_Credit_Days__c),Double.valueof(OLICurrentValue),
                                    '','',ExpectedInvoice,Double.valueof(oliIterator.Current_Invoice__c),oliIterator));
                                    showReport=true;
                                  */
                            //}
                        }
                        else
                        {
                               // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'No Result Found.');
                                //ApexPages.addMessage(myMsg);
                                //Build Wrapper for All Invoice 
                               /* AllInvoiceWrapper.add(new AllInvoiceWrap(String.valueof(oliIterator.Name),String.valueof(oliIterator.ProductCode__c),String.valueof(oliIterator.Description__c),
                                Double.valueof(oliIterator.UnitPrice__c),Integer.valueof(oliIterator.Successful_Payments__c),Integer.valueof(oliIterator.Payments_Remaining__c),
                                Integer.valueof(oliIterator.Payment_Duration__c),Integer.valueof(oliIterator.Number_of_Months_Transferred__c),Integer.valueof(oliIterator.Prorate_Credit_Days__c),Double.valueof(OLICurrentValue),
                                '','',ExpectedInvoice,Double.valueof(oliIterator.Current_Invoice__c),oliIterator));
                                showReport=true;
                               */
                        }
                    }
                    else{
                              //  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'No Result Found.');
                              //  ApexPages.addMessage(myMsg);
                                //Build Wrapper for All Invoice 
                               /* AllInvoiceWrapper.add(new AllInvoiceWrap(String.valueof(oliIterator.Name),String.valueof(oliIterator.ProductCode__c),String.valueof(oliIterator.Description__c),
                                Double.valueof(oliIterator.UnitPrice__c),Integer.valueof(oliIterator.Successful_Payments__c),Integer.valueof(oliIterator.Payments_Remaining__c),
                                Integer.valueof(oliIterator.Payment_Duration__c),Integer.valueof(oliIterator.Number_of_Months_Transferred__c),Integer.valueof(oliIterator.Prorate_Credit_Days__c),Double.valueof(OLICurrentValue),
                                '','',ExpectedInvoice,Double.valueof(0),oliIterator));
                                showReport=true;
                              */
                    }
            }
            
            /*if(AllInvoiceWrapper.size()>0)
                showexport=true;
           */
           
    }
    else
    {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'No Result Found.');
        ApexPages.addMessage(myMsg);
    }
}

public void ExportReport(){
    
      M2SWrapper=new List<MonthlyToSingleWrap>();
      S2MWrapper=new List<SingleToMonthlyWrap>();
     // AllInvoiceWrapper =new List<AllInvoiceWrap>();
      RenderType='text/html';
      if(rtype!=null && sdate!=null && edate!=null)
      {
        generateSubsequentReport(rtype,sdate,edate);
        showSearchPanel=false;
        showReport=true;
        showblock=false;
        showurlexport=true;
        RenderType='application/vnd.ms-excel#SubsequentReconciliationReport.xls';
      }
      else
      {
        System.debug('##########'+Reporttype+'%%%%%%%%%%%%%%%%'+sdate+'##########'+edate);
        generateSubsequentReport(Reporttype,sdate,edate);
        showSearchPanel=false;
        showblock=false;
        RenderType='application/vnd.ms-excel#SubsequentReconciliationReport.xls';
      }
   
    
}
public class MonthlyToSingleWrap
{
    public string OLI{get;set;}
    public string ProductCode{get;set;}
    public string LineDesc{get;set;}
    public Double OLIAmount{get;set;}
    public integer installments{get;set;}
    public integer PaymentsRemaining{get;set;}
    public integer PaymentDuration{get;set;}
    public integer MonthsTranferred{get;set;}
    public integer ProrateDays{get;set;}
    public double OLICurrentValue{get;set;} 
    public Double OLITotal{get;set;}
    public string FCfrom{get;set;}
    public string FCto{get;set;}
    public Double LastInvoice{get;set;}
    public Double RaisedInvoiceAmt{get;set;}
    public double VarianceAmt{get;set;}
    public Order_Line_Items__c oliIterator{get;set;}
   public MonthlyToSingleWrap(String OLI,String ProductCode,string LineDesc,Double OLIAmount,Integer installments,Integer PaymentsRemaining,Integer PaymentDuration,integer MonthsTranferred,integer ProrateDays,Double OLICurrentValue,String FCfrom,String FCto,Double LastInvoice,Double RaisedInvoiceAmt,Order_Line_Items__c oliIterator)
   {
        this.OLI=OLI;
        this.ProductCode=ProductCode;
        this.LineDesc=LineDesc;
        this.MonthsTranferred=MonthsTranferred;
        this.ProrateDays=ProrateDays;
        this.OLIAmount=OLIAmount;
        this.installments=installments;
        this.PaymentsRemaining=PaymentsRemaining;
        this.PaymentDuration=PaymentDuration;
        this.OLICurrentValue=OLICurrentValue;
        this.OLITotal=PaymentDuration*OLIAmount;
        this.FCfrom=FCfrom;
        this.FCto=FCto;
        this.LastInvoice=LastInvoice;
        this.RaisedInvoiceAmt=RaisedInvoiceAmt;
        this.VarianceAmt=RaisedInvoiceAmt-LastInvoice;
         this.oliIterator=oliIterator;
   }   
   
}
public class SingleToMonthlyWrap
{
    public string OLI{get;set;}
    public string ProductCode{get;set;}
    public string LineDesc{get;set;}
    public Double OLIAmount{get;set;}
    public integer installments{get;set;}
    public integer PaymentsRemaining{get;set;}
    public integer PaymentDuration{get;set;}
    public integer MonthsTranferred{get;set;}
    public integer ProrateDays{get;set;}
    public double OLICurrentValue{get;set;} 
    public Double OLITotal{get;set;}
    public string FCfrom{get;set;}
    public string FCto{get;set;}
    public Double LastInvoice{get;set;}
    public Double RaisedInvoiceAmt{get;set;}
    public double VarianceAmt{get;set;}
    public Order_Line_Items__c oliIterator{get;set;}
   public SingleToMonthlyWrap(String OLI,String ProductCode,string LineDesc,Double OLIAmount,Integer installments,Integer PaymentsRemaining,Integer PaymentDuration,integer MonthsTranferred,integer ProrateDays,Double OLICurrentValue,String FCfrom,String FCto,Double LastInvoice,Double RaisedInvoiceAmt,Order_Line_Items__c oliIterator)
   {
        this.OLI=OLI;
        this.ProductCode=ProductCode;
        this.LineDesc=LineDesc;
        this.MonthsTranferred=MonthsTranferred;
        this.ProrateDays=ProrateDays;
        this.OLIAmount=OLIAmount;
        this.installments=installments;
        this.PaymentsRemaining=PaymentsRemaining;
        this.PaymentDuration=PaymentDuration;
        this.OLICurrentValue=OLICurrentValue;
        this.OLITotal=PaymentDuration*OLIAmount;
        this.FCfrom=FCfrom;
        this.FCto=FCto;
        this.LastInvoice=LastInvoice;
        this.RaisedInvoiceAmt=RaisedInvoiceAmt;
        this.VarianceAmt=RaisedInvoiceAmt-LastInvoice;
        this.oliIterator=oliIterator;
   }   
   
}
/*
public class AllInvoiceWrap
{
    public string OLI{get;set;}
    public string ProductCode{get;set;}
    public string LineDesc{get;set;}
    public Double OLIAmount{get;set;}
    public integer installments{get;set;}
    public integer PaymentsRemaining{get;set;}
    public integer PaymentDuration{get;set;}
    public integer MonthsTranferred{get;set;}
    public integer ProrateDays{get;set;}
    public double OLICurrentValue{get;set;} 
    public Double OLITotal{get;set;}
    public string FCfrom{get;set;}
    public string FCto{get;set;}
    public Double LastInvoice{get;set;}
    public Double RaisedInvoiceAmt{get;set;}
    public double VarianceAmt{get;set;}
    public Order_Line_Items__c oliIterator{get;set;}
     public AllInvoiceWrap(String OLI,String ProductCode,String LineDesc,Double OLIAmount,Integer installments,Integer PaymentsRemaining,Integer PaymentDuration,integer MonthsTranferred,integer ProrateDays,Double OLICurrentValue,String FCfrom,String FCto,Double LastInvoice,Double RaisedInvoiceAmt,Order_Line_Items__c oliIterator)
   {
        this.OLI=OLI;
        this.ProductCode=ProductCode;
        this.LineDesc=LineDesc;
        this.OLIAmount=OLIAmount;
        this.MonthsTranferred=MonthsTranferred;
        this.ProrateDays=ProrateDays;
        this.installments=installments;
        this.PaymentsRemaining=PaymentsRemaining;
        this.PaymentDuration=PaymentDuration;
        this.OLICurrentValue=OLICurrentValue;
        this.OLITotal=PaymentDuration*OLIAmount;
        this.FCfrom=FCfrom;
        this.FCto=FCto;
        this.LastInvoice=LastInvoice;
        this.RaisedInvoiceAmt=RaisedInvoiceAmt;
        this.VarianceAmt=RaisedInvoiceAmt-LastInvoice;
         this.oliIterator=oliIterator;
   }   
   
}
*/
}