public class ISSLineItemSOQLMethods {        
    public static list<ISS_Line_Item__c> getISSLIForSendRemittanceWithoutRemSheet(String strPubCode, String strDirEditionCode) {
        return [select ISS__r.From_P__c, ISS__r.To_C__c, ISS__r.Trans__c, ISS__r.Trans_Version__c, ISS__r.Total_Tax__c, ISS__r.Total_Gross_Amount__c, 
            ISS__r.Total_ADJ_Amount__c, ISS__r.TotalCommission__c, ISS__r.To__c, ISS__r.State_City_Zip__c,ISS__r.Payment_Date__c, 
            ISS__r.Publication_Code__c, ISS__r.Publication_Company__c, ISS__r.Publication_Company__r.Name, ISS__r.NetAmount__c, 
            ISS__r.Name, ISS__r.Invoice_Date__c, ISS__r.Id, ISS__r.From__c, ISS__r.CMR__c, ISS__r.CMR__r.Name, ISS__r.CMR_Number__c, 
            ISS__r.CMR_Address__c, Product__c, Id, Name, Directory__c, Directory__r.Name, Directory_Edition__c, Directory_Edition__r.Edition_Code__c, 
            Directory_Edition__r.Name, Pubco_date__c, Client_Name__c, Client_Name__r.Name, Life__c, Inv_Type__c, Gross_Amount__c, Commission__c, ISS__c, 
            Directory_Code__c, Client_Number__c, Commission_Amount__c, Tax__c, Tax_Amount__c, ADJ_Amount__c, Net_Amount__c 
            from ISS_Line_Item__c where ISS__r.Publication_Code__c =:strPubCode and Directory_Edition_Code__c =:strDirEditionCode and ISS__r.Remittance_Sheet_created__c = false];
    }
    
    public static list<ISS_Line_Item__c> getISSLIForSendRemittanceWithRemSheet(String strPubCode, String strDirEditionCode) {
        return [select ISS__r.From_P__c, ISS__r.To_C__c, ISS__r.Trans__c, ISS__r.Trans_Version__c, ISS__r.Total_Tax__c, ISS__r.Total_Gross_Amount__c, 
            ISS__r.Total_ADJ_Amount__c, ISS__r.TotalCommission__c, ISS__r.To__c, ISS__r.State_City_Zip__c,ISS__r.Payment_Date__c, 
            ISS__r.Publication_Code__c, ISS__r.Publication_Company__c, ISS__r.Publication_Company__r.Name, ISS__r.NetAmount__c, 
            ISS__r.Name, ISS__r.Invoice_Date__c, ISS__r.Id, ISS__r.From__c, ISS__r.CMR__c, ISS__r.CMR__r.Name, ISS__r.CMR_Number__c, 
            ISS__r.CMR_Address__c, Product__c, Id, Name, Directory__c, Directory__r.Name, Directory_Edition__c, Directory_Edition__r.Edition_Code__c, 
            Directory_Edition__r.Name, Pubco_date__c, Client_Name__c, Client_Name__r.Name, Life__c, Inv_Type__c, Gross_Amount__c, Commission__c, ISS__c, 
            Directory_Code__c, Client_Number__c, Commission_Amount__c, Tax__c, Tax_Amount__c, ADJ_Amount__c, Net_Amount__c 
            from ISS_Line_Item__c where ISS__r.Publication_Code__c =:strPubCode and Directory_Edition_Code__c =:strDirEditionCode and ISS__r.Remittance_Sheet_created__c = true];
    }
    
    public static list<ISS_Line_Item__c> getISSLIForSendISSWithRemSheet(String strPubCode, String strDirEditionCode) {
        return [SELECT ISS__c, ISS__r.Total_Tax__c, ISS__r.NetAmount__c, ISS__r.Total_Gross_Amount__c, ISS__r.TotalCommission__c, ISS__r.Total_ADJ_Amount__c 
            FROM ISS_Line_Item__c WHERE ISS__r.Publication_Code__c =: strPubCode AND Directory_Edition_Code__c =: strDirEditionCode 
            AND ISS__r.Sent_to_Elite__c = false AND ISS__r.Remittance_Sheet_created__c = true];
    }
    
    public static list<ISS_Line_Item__c> getISSLIForSendISSWithoutRemSheet(String strPubCode, String strDirEditionCode) {
        return [SELECT ISS__c, ISS__r.Total_Tax__c, ISS__r.NetAmount__c, ISS__r.Total_Gross_Amount__c, ISS__r.TotalCommission__c, ISS__r.Total_ADJ_Amount__c 
            FROM ISS_Line_Item__c WHERE ISS__r.Publication_Code__c =: strPubCode AND Directory_Edition_Code__c =: strDirEditionCode 
            AND ISS__r.Sent_to_Elite__c = false AND ISS__r.Remittance_Sheet_created__c = false];
    }
    
    public static list<ISS_Line_Item__c> getISSLIByIds(set<Id> setISSLiIds) {
        return [SELECT Id, Commission__c, Rate_for_Commission__c FROM ISS_Line_Item__c WHERE Id IN :setISSLiIds];
    }
}