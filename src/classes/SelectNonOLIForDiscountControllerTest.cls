@isTest
public with sharing class SelectNonOLIForDiscountControllerTest {
    static testMethod void selNonOLITest() {        
        Test.startTest();
        apexpages.currentpage().getparameters().put('mode', 'true');
        Account acct = TestMethodsUtility.createAccount('cmr');
        RecordType RT = [SELECT ID FROM RecordType WHERE DeveloperName='Telco_Partner' LIMIT 1];
        acct.RecordTypeId =RT.id;
        update acct;
        Case cas = TestMethodsUtility.generateCase('CS Claim');
        cas.Contracted_Total_Value__c = 90;
        insert cas;
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 prod = TestMethodsUtility.createproduct();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = canvass.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c = system.today();
        oln.Print_First_Bill_Date__c = system.today();
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        oln.Directory__c = dir.Id;
        oln.Product2__c = prod.Id;
        oln.Directory_Edition__c = DE.Id;
        oln.Billing_Partner__c = CommonMessages.BerryForDimension;
        oln.Payment_Method__c = CommonMessages.ACHPaymentMethod;
        oln.Billing_Frequency__c = CommonMessages.singlePayment;
        oln.Successful_Payments__c = 9;
        //oln.CMR_Name__c = acct.Id;
        insert oln;         
        oln = [SELECT Id, Directory_Code__c, Canvass__r.Canvass_Code__c, Product2__r.RGU__c, CMR_Name__c, 
                Billing_Partner__c, Is_P4P__c, P4P_Billing__c, Directory_Edition__c, Order_Group__c, 
                Order_Anniversary_Start_Date__c, Print_First_Bill_Date__c, Talus_Go_Live_Date__c, 
                Payment_Method__c, Product2__r.Family, Product2__r.Name, P4P_Current_Billing_Clicks_Leads__c,
                Quantity__c, Product2__c, Directory__c, Order_Line_Total__c, Successful_Payments__c, UnitPrice__c,
                Prorate_Credit_Days__c, Billing_Frequency__c              
                FROM Order_Line_Items__c WHERE Id = : oln.Id];
        c2g__codaDimension3__c  dimension3 = TestMethodsUtility.generateDimension3(oln);
        dimension3.Name = 'Local';
        insert dimension3;
        
        c2g__codaDimension1__c  dimension1 = TestMethodsUtility.generateDimension1(dir);
        dimension1.Type__c='Print';
        dimension1.Directory__c=dir.id;
        insert dimension1;
        
        system.debug('-----prod type---'+dimension1.ProdType__c);
        
        c2g__codaDimension2__c dimension2 = TestMethodsUtility.createDimension2(prod);
        dimension2.Type__c='Print';
        //dimension2.Canvass__c=null;
        upsert dimension2;
        
       
       //system.debug('-----prod type---'+dimension2.ProdType__c);
        
        PageReference pageRef = Page.SelectNonOLIForDiscountPage;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getparameters().put('id', cas.id);
        
        
        ApexPages.StandardController controller = new ApexPages.StandardController(cas);
        SelectNonOLIForDiscountController obj = new SelectNonOLIForDiscountController(controller);
        
         
        
        obj.objOLID.Dimension_3__c = dimension3.Id;
        
            
        
        
        obj.getItems();
        obj.getTwelveItems(); 
        obj.SaveNONAndReturn();
        
        cas.Case_Source__c = 'SALES';
        cas.Case_Reason__c = 'test';
        update cas; 
        
         ApexPages.StandardController controller1 = new ApexPages.StandardController(cas);
        SelectNonOLIForDiscountController obj1 = new SelectNonOLIForDiscountController(controller1);
        
        obj1.objOLID.Discount_Per_period__c = 5;
        obj1.objOLID.Number_of_Billing_Periods_to_discount__c=1;
        obj1.objOLID.Product_Non_OLI__c = null;
        obj1.objOLID.Dimension_1__c = null;
        obj1.objOLID.Dimension_2__c = null;
        obj1.objOLID.Dimension_3__c = null;
        obj1.objOLID.Directory_Edition__c = null; 
        //obj1.objOLID.Dimension_2__r.Name = 'Print';
        
        obj1.SaveNONAndReturn();
        
        
        cas.Case_Source__c = 'Other';
        cas.Case_Reason__c = 'test';
        update cas; 
        
        
        ApexPages.StandardController controller2 = new ApexPages.StandardController(cas);
        SelectNonOLIForDiscountController obj2 = new SelectNonOLIForDiscountController(controller2);
        
        obj2.objOLID.Discount_Per_period__c = 4;
        obj2.objOLID.Number_of_Billing_Periods_to_discount__c=5;
        obj2.objOLID.Product_Non_OLI__c='Print';
        obj2.objOLID.Dimension_1__c = dimension1.id;
        obj2.objOLID.Dimension_2__c = dimension2.id;
        obj2.objOLID.Dimension_3__c = dimension3.id;
        obj2.objOLID.Total_BCC__c= '3';
       obj2.objOLID.Telco__c=acct.id;
        
        
        obj2.SaveNONAndReturn();
        
        
        
        
        Test.stopTest();
    }
}