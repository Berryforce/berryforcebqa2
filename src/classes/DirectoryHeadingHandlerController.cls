public class DirectoryHeadingHandlerController {	
	public static void onBeforeInsert(list<Directory_Heading__c> lstDirHead) {
		populateName(lstDirHead, null);
	}
	
	public static void onBeforeUpdate(list<Directory_Heading__c> lstDirHead, Map<Id, Directory_Heading__c> dirHeadOldMap) {
		populateName(lstDirHead, dirHeadOldMap);
	}
	
	private static void populateName(list<Directory_Heading__c> lstDirHead, Map<Id, Directory_Heading__c> dirHeadOldMap) {		
		if(dirHeadOldMap == null) {
			for(Directory_Heading__c DH : lstDirHead) {
				if(DH.Directory_Heading_Name__c != null) {
					if(DH.Directory_Heading_Name__c.length() > 79) {
						DH.Name = DH.Directory_Heading_Name__c.subString(80);
					} else {
						DH.Name = DH.Directory_Heading_Name__c;
					}			 
				}
			}				
		} else {
			for(Directory_Heading__c DH : lstDirHead) {
				Directory_Heading__c oldDirHead = dirHeadOldMap.get(DH.Id);
				if(DH.Directory_Heading_Name__c != oldDirHead.Directory_Heading_Name__c) {
					if(DH.Directory_Heading_Name__c != null) {
						if(DH.Directory_Heading_Name__c.length() > 79) {
							DH.Name = DH.Directory_Heading_Name__c.subString(80);
						} else {
							DH.Name = DH.Directory_Heading_Name__c;
						}
					}
				} 
			}				
		}
	}
}