public with sharing class bSure_CustomSelectContact {

  public Account account {get;set;} 
  public Contact contact {get;set;}
  public List<Contact> results{get;set;}
  public List<Account> LstAcc; 
  public string searchString{get;set;} 
  public String InputHiddenIds{get;set;}  
  public string choice {get;set;}
  public String acid;
  public String acids {get;set;}
  public String opId;
  public String AccId;
  public Boolean refreshPage {get; set;} 
  public string fullFileURL  {get; set;}
  
    public Bsure_CustomSelectContact () {
    //account = new Account();
    // get the current search string
    refreshPage = false;
    fullFileURL = URL.getSalesforceBaseUrl().toExternalForm()+'/';
    system.debug('fullFileURL:'+fullFileURL);
    acid = System.currentPageReference().getParameters().get('acid');
    opId = System.currentPageReference().getParameters().get('opID');
    AccId = System.currentPageReference().getParameters().get('AccId');
    acids = System.currentPageReference().getParameters().get('AccId');
    system.debug('acids:'+acids);
    
    system.debug('acid========'+acid+'===='+System.currentPageReference().getParameters().get('sId'));
    
    if(System.currentPageReference().getParameters().get('sId')!=null && System.currentPageReference().getParameters().get('sId')!='')
    {
        searchString = System.currentPageReference().getParameters().get('sId');
    }
     
    runSearch();  
    }
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
  
    // prepare the query and issue the search command
  Public void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    system.debug('searchString=====11111111====='+searchString);
    results = performSearch(searchString);               
  }
  
   // run the search and return the records found. 
  Public List<Contact> performSearch(string searchString) 
  {
     system.debug('searchString======='+searchString+'====opId==='+AccId);
     String strSearch='%'+searchString+'%';
   system.debug('searchString======='+searchString+'==='+strSearch);
   
     //Opportunity opt = new Opportunity ();
     //list<Opportunity> lstOpt = new list<Opportunity>();
     //lstOpt=[SELECT id,name,Account.Id from Opportunity where id=:opId];
     
     Account Acc = new account();
     LstAcc = new list<Account>(); 
     //LstAcc=[select id,Name,Primary_Contact__c, AR_Balance__c,Open_Claim__c,Collection_Status__c,Total_Past_Due__c from account where id=:AccId];
     LstAcc=[select id,Name,AR_Balance__c,Open_Claim__c,Collection_Status__c,Total_Past_Due__c from account where id=:AccId];     
     String soql = 'select id, name,FirstName,LastName,Email from Contact WHERE Email !=NULL AND IsActive__c=True';
     if(searchString != '' && searchString != null)
     soql = soql +  ' AND name LIKE :strSearch ';
     
     System.debug('LstAcc::::::::::'+LstAcc); 
     if(LstAcc!=null && LstAcc.size()>0)
     {
         //opt=lstOpt[0];
         soql = soql +' AND AccountId=\''+LstAcc[0].Id+'\'';
         System.debug('soql::::::::::::'+soql);
     }
  
    soql = soql + ' limit 25';
    System.debug('soqlLimit:'+soql);
    return database.query(soql); 
 
  }
  
  public PageReference ContactCancel() {
    system.debug(acids);
    PageReference Ref = new PageReference('/'+acids);
    Ref.setRedirect(true);
    return Ref;
  }
   // save the new account record 
  public PageReference saveAccount() {
    system.debug('choice:'+choice);
    
    if(choice != null && choice != '') {
        system.debug('choice:'+choice);
        system.debug('List:'+LstAcc[0]);
        if(LstAcc[0]!=null)
        {
        
        /*Account ObjAccount =LstAcc[0];
        ObjAccount.Primary_Contact__c=choice;
        update ObjAccount;
        system.debug('List:'+ObjAccount);*/
        
        String strAccountId= String.valueof(LstAcc[0].Id).substring(0,15);
        system.debug('strAccountId:::::::::::'+strAccountId);
        
        list<Vertex_Berry__BSureC_Customer_Basic_Info__c> lstcust = new list<Vertex_Berry__BSureC_Customer_Basic_Info__c>();
        //Vertex_Berry__BSureC_Customer_Basic_Info__c CustInfo = [select id,Vertex_Berry__Customer_Contact__c,Vertex_Berry__Customer_Name__c,Vertex_Berry__Email_address__c from Vertex_Berry__BSureC_Customer_Basic_Info__c where Vertex_Berry__ExternalId__c =:strAccountId];
        lstcust = [select Id,Vertex_Berry__Customer_Contact__c,Vertex_Berry__Customer_Name__c,Vertex_Berry__Email_address__c from Vertex_Berry__BSureC_Customer_Basic_Info__c where Vertex_Berry__ExternalId__c =:strAccountId];
        Contact ObjCont = [select id, name,email from contact where id=:choice];
        system.debug('choice=============='+choice);
        System.debug('lstcust=============1====='+lstcust);
        String strId;
        if(lstcust != null && lstcust.size() > 0){
        	for(Vertex_Berry__BSureC_Customer_Basic_Info__c CustInfo : lstcust){
        		strId = CustInfo.id;
	        	CustInfo.Vertex_Berry__Customer_Contact__c=choice;
	        	CustInfo.Vertex_Berry__Customer_Contact__c=ObjCont.Name;
	        	CustInfo.Vertex_Berry__Email_address__c = ObjCont.Email;
        	}	
        	system.debug('lstcust===========2======'+lstcust);
        	//system.debug('jShrs[0].AccessLevel=========='+lstcust.get(0).AccessLevel);
        	//update lstcust;
        	
        	
        }
        try{
        	Database.SaveResult[] lsr = Database.update(lstcust,false);
        	String strAccName = LstAcc[0].Name;
        	if(strAccName != null && strAccName.contains('&lt;'))
            {
                strAccName = strAccName.replaceAll('&lt;', '<');                    
            }
            if(strAccName != null && strAccName.contains('&gt;'))
            {
                strAccName = strAccName.replaceAll('&gt;', '>');                    
            }
            if(strAccName != null && strAccName.contains('&quot;'))
            {
                strAccName = strAccName.replaceAll('&quot;', '"');                  
            }
            if(strAccName != null && strAccName.contains('\n'))
            {
                strAccName = strAccName.replace('\n', '');                  
            }
            if(strAccName != null && strAccName.contains('#'))
            {
                strAccName = strAccName.replace('#', '%23');                  
            }
        	for(Database.SaveResult sr : lsr){
	            if(sr.isSuccess()){
	            	system.debug('lstcust====1======='+lstcust);
	                System.debug('strAccName================'+strAccName);
			        //PageReference Ref = new PageReference('/apex/Vertex_Berry__BSure_CreatePaymentPlan'+'?CustId='+LstAcc[0].Id+'&RId='+LstAcc[0].Name+'&Arbl='+LstAcc[0].AR_Balance__c+'&RecId='+strId+'&CusN='+LstAcc[0].Name+'&OType=Account');
			        String strPageUrl = '/apex/Vertex_Berry__BSure_CreatePaymentPlan'+'?CustId='+LstAcc[0].Id+'&RId='+strAccName+'&Arbl='+LstAcc[0].AR_Balance__c+'&RecId='+strId+'&CusN='+strAccName+'&OType=Account';
			        PageReference Ref = new PageReference(strPageUrl);
			        system.debug('Reference'+Ref);
			        Ref.setRedirect(true);
			        //return Ref;
			        return Ref;
	            }	
	            else
	            {
	            	system.debug('lstcust===2========'+lstcust);
			        PageReference Ref = new PageReference('/apex/Vertex_Berry__BSure_CreatePaymentPlan'+'?CustId='+LstAcc[0].Id+'&RId='+strAccName+'&Arbl='+LstAcc[0].AR_Balance__c+'&RecId='+strId+'&CusN='+strAccName+'&OType=Account');
			        system.debug('Reference'+Ref);
			        Ref.setRedirect(true);
			        //return Ref;
			        return Ref;
	            }
        	}
        }
        catch(Exception ex){
        	String strAccName = LstAcc[0].Name;
        	system.debug('lstcust==========='+lstcust);
	        PageReference Ref = new PageReference('/apex/Vertex_Berry__BSure_CreatePaymentPlan'+'?CustId='+LstAcc[0].Id+'&RId='+strAccName+'&Arbl='+LstAcc[0].AR_Balance__c+'&RecId='+strId+'&CusN='+strAccName+'&OType=Account');
	        system.debug('Reference'+Ref);
	        Ref.setRedirect(true);
	        //return Ref;
	        return Ref;
        }
        return null;
        /*
        system.debug('CustInfo::::::::::::'+CustInfo);
        system.debug('choice=============='+choice);
        CustInfo.Vertex_Berry__Customer_Contact__c=choice;
        
        Contact ObjCont = [select id, name,email from contact where id=:choice];
        system.debug('ObjCont:'+ObjCont);
        system.debug('choice:'+choice);
        CustInfo.Vertex_Berry__Customer_Contact__c=ObjCont.Name;
        CustInfo.Vertex_Berry__Email_address__c = ObjCont.Email;
        system.debug('CustInfo:+contact++Email+++++++'+CustInfo.Vertex_Berry__Customer_Contact__c+CustInfo.Vertex_Berry__Email_address__c);
        //CustInfo.Vertex_Berry__Email_address__c;
        System.debug('CustInfo==****=='+CustInfo);
        //update CustInfo;
        Database.Saveresult db = Database.update(CustInfo);
        if(db.isSuccess()){
	        PageReference Ref = new PageReference('/apex/Vertex_Berry__BSure_CreatePaymentPlan'+'?CustId='+LstAcc[0].Id+'&RId='+LstAcc[0].Name+'&Arbl='+LstAcc[0].AR_Balance__c+'&RecId='+CustInfo.id+'&CusN='+LstAcc[0].Name+'&OType=Account');
	        system.debug('Reference'+Ref);
	        Ref.setRedirect(true);
	        //return Ref;
	        return Ref;
        }
        return null;    */
        
        }
    }
    else {
        // TO Do display message
        showErrorMessage('Please select atleast one contact from the below list.');
    }
    return null;
 }
 
    public pageReference showErrorMessage(String msg)
    {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,msg);
        ApexPages.addMessage(errormsg);
        return null;
    }
 
   
}