global class ScheduleSortAsPopulationBatch implements Schedulable {
   public Interface ScheduleSortAsPopulationBatchInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('ScheduleSortAsPopulationBatchHndlr');
        if(targetType != null) {
            ScheduleSortAsPopulationBatchInterface obj = (ScheduleSortAsPopulationBatchInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}