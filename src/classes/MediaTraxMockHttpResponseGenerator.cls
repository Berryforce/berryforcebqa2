@isTest
global class MediaTraxMockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
       // System.assertEquals('https://cscberryteam.atlassian.net/rest/customware/connector/1.0/1/CASE/500Z00000073EOl/issue/synchronize.json', req.getEndpoint());
       // System.assertEquals('GET', req.getMethod());
        System.debug('Testing mock req '+req);
        System.debug('Testing mock req.getMethod() '+req.getMethod());
        
        System.debug('Testing mock req.getEndpoint() '+req.getEndpoint());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><Response><Return><GetHeadings><action>GetHeadings</action><headings><heading><headingid>93</headingid><headingname>TestName</headingname></heading></headings><total>1</total></GetHeadings></Return></Response><Response xmlns="https://ct.mediatrax.com/api/"><Return xsi:type="ns1:Document"xmlns:ns1="http://xml.apache.org/xml-soap"><GetDirectories xmlns="http://calltraks.dev.local/api/"><directories><directory><directorynumber>101753</directorynumber><directoryid>1974</directoryid></directory></directories><total>2</total></GetDirectories></Return></Response></soapenv:Body></soapenv:Envelope>');
        res.setStatusCode(200);
        return res;
    }
}