global class LOBReportGenerationBatch Implements Database.Batchable <sObject>, Database.Stateful {
    Id selectedDirId;
    Id selectedDirEdId; 
    List<String> listSelBillingPartners = new List<String>();
    Map<Id, String> mapAcctIdPrimConPhone = new Map<Id, String>();
    String reportType;
    String invFromDate;
    String invToDate;  
    String selectedEntity;
    
    global LOBReportGenerationBatch(Id selectedDirId, Id selectedDirEdId, List<String> listSelBillingPartners, String reportType, String invFromDate, String invToDate, String selectedEntity) {
        this.selectedDirId = selectedDirId;
        this.selectedDirEdId = selectedDirEdId;
        this.listSelBillingPartners = listSelBillingPartners;
        system.debug('Testss : '+ listSelBillingPartners);
        this.reportType = reportType;
        this.invFromDate = invFromDate;
        this.invToDate = invToDate;
        this.selectedEntity = selectedEntity;
    }
    
    global Database.queryLocator start(Database.BatchableContext bc) {      
    	String SOQL = 'SELECT Id FROM Directory_Edition__c LIMIT 1'; 
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<Directory_Edition__c> dirEdList) {
        List<List_of_Business__c> listLOB = new List<List_of_Business__c>();
        Set<Id> acctIds = new Set<Id>();
        
        /*if(reportType == 'print') {
            List<Order_Line_Items__c> listOLI;
            Map<Id, Map<String, Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>>> mapAccBPBFPhoneLisNamOLI = new Map<Id, Map<String, Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>>>();     
            RecordType  objRT = CommonMethods.getRecordTypeDetailsByName('Order_Line_Items__c','National Order Line Item');         
            if(listSelBillingPartners.size() > 0) {        
                listOLI = [SELECT Listing__r.Phone__c, Account__r.X3l_External_Id__c, Account__r.Parent_3L_External_Id__c,Id,
                        Digital_Product_Requirement__r.business_phone_number_office__c,UnitPrice__c, Account__r.Account_Number__c,
                        Account__r.Parent.Account_Number__c, Telco__r.Name,Listing_Name__c,Listing__r.Name,
                        Billing_Partner__c,Billing_Frequency__c, Canvass__c, Directory_Code__c,
                        Edition_Code__c, Media_Type__c, Telco_Invoice_Date__c, Service_End_Date__c, Service_Start_Date__c FROM Order_Line_Items__c WHERE 
                        RecordtypeId !=: objRT.Id AND isCanceled__c = false AND Product2__r.Media_Type__c='Print' AND Billing_Partner__c in : listSelBillingPartners 
                        AND Directory__c=: selectedDirId AND Directory_Edition__c =: selectedDirEdId AND Billing_Partner__c != null ORDER BY Billing_Partner__c, Account__r.Name];
            } else {
                listOLI = [SELECT Listing__r.Phone__c, Account__r.X3l_External_Id__c, Account__r.Parent_3L_External_Id__c, Id, 
                        Digital_Product_Requirement__r.business_phone_number_office__c,UnitPrice__c, Account__c,Account__r.Account_Number__c,
                        Account__r.Parent.Account_Number__c, Telco__r.Name,Listing_Name__c,Listing__r.Name, 
                        Billing_Partner__c,Billing_Frequency__c, Canvass__c, Directory_Code__c,
                        Edition_Code__c, Media_Type__c, Telco_Invoice_Date__c, Service_End_Date__c, Service_Start_Date__c FROM Order_Line_Items__c WHERE 
                        RecordtypeId !=: objRT.Id AND isCanceled__c = false AND Product2__r.Media_Type__c = 'Print' AND Directory__c =: selectedDirId AND 
                        Directory_Edition__c =: selectedDirEdId AND Billing_Partner__c != null ORDER BY Billing_Partner__c,Account__r.Name];
            }  
            if(listOLI.size() > 0) {
                for(Order_Line_Items__c OLI : listOLI) {                
                    String AdvPhone = OLI.Digital_Product_Requirement__r.business_phone_number_office__c;
                    String ListingName = String.valueof(OLI.Listing__r.Name + OLI.Listing__r.Phone__c).replaceAll('\\s+','');
                    acctIds.add(OLI.Account__c);
                    if(!mapAccBPBFPhoneLisNamOLI.containsKey(OLI.Account__c)) {
                        mapAccBPBFPhoneLisNamOLI.put(OLI.Account__c, new Map<String, Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>>());
                    } 
                    if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).containsKey(OLI.Billing_Partner__c)) {
                        mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).put(OLI.Billing_Partner__c, new Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>());
                    }
                    if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).containsKey(OLI.Billing_Frequency__c)) {
                        mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).put(OLI.Billing_Frequency__c, new Map<String, Map<String, List<Order_Line_Items__c>>>());
                    }
                    if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).containsKey(AdvPhone)) {
                        mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).put(AdvPhone, new Map<String, List<Order_Line_Items__c>>());
                    }
                    if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).get(AdvPhone).containsKey(ListingName)) {
                        mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).get(AdvPhone).put(ListingName, new List<Order_Line_Items__c>());
                    }
                    mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).get(AdvPhone).get(ListingName).add(OLI);
                }
                
                fetchBillingContact(acctIds);               
                
                for(Id acctId : mapAccBPBFPhoneLisNamOLI.keySet()) {
                    for(String BP : mapAccBPBFPhoneLisNamOLI.get(acctId).keySet()) {
                        for(String BF : mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).keySet()) {
                            for(String advPhone : mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).get(BF).keySet()) {
                                for(String listName : mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).get(BF).get(advPhone).keySet()) {
                                    Double billAmount = 0;
                                    List<Order_Line_Items__c> listTempOLI = mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).get(BF).get(advPhone).get(listName);
                                    if(listTempOLI != null) {
                                        Order_Line_Items__c tempOLI = listTempOLI.get(0);
                                        String OLIIds = '';
                                        set<Id> setOLIIds = new set<Id>();
                                        for(Order_Line_Items__c OLI : listTempOLI) {
                                            billAmount += OLI.UnitPrice__c;
                                            OLIIds += OLI.Id + ';';
                                            setOLIIds.add(OLI.Id);
                                        }           
                    					List_of_Business__c lob = new List_of_Business__c();   
                    					lob = newLOB(tempOLI, billAmount, OLIIds.removeEnd(';'), setOLIIds.size());   
                    					lob.LOB_Billing_Telephone__c = mapAcctIdPrimConPhone.get(tempOLI.Account__c);   
                            			listLOB.add(lob);          
                                    }
                                }
                            }
                        }
                    }
                }    
            }  
        } else */if(reportType == 'digital') {
            map<String,List<c2g__codaInvoiceLineItem__c>> mapStrCompareInvoices=new map<String,List<c2g__codaInvoiceLineItem__c>>();
            set<String> transTypes = new set<String>();
            transTypes.addAll(System.Label.NonTDAndFCTransType.split(','));
            for(c2g__codaInvoiceLineItem__c InvoiceIterator: [SELECT Order_Line_Item__r.Account__r.Primary_Contact__r.Phone,
            Order_Line_Item__r.Account__r.Primary_Contact__r.Name, Order_Line_Item__r.Edition_Code__c, Order_Line_Item__r.Media_Type__c, 
            c2g__NetValue__c,Order_Line_Item__r.Account__c,Order_Line_Item__r.Account__r.X3l_External_ID__c, Order_Line_Item__r.Telco_Invoice_Date__c, 
            Order_Line_Item__r.Account__r.Parent_3L_External_ID__c, Order_Line_Item__r.Account__r.Account_Number__c,
            Order_Line_Item__r.Account__r.Parent.Account_Number__c, c2g__Invoice__r.Name,
            c2g__Invoice__r.Transaction_Type__c,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,Order_Line_Item__r.Listing__r.Name,
            c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,Order_Line_Item__r.Billing_Contact__r.Phone,Order_Line_Item__r.UnitPrice__c,
            Order_Line_Item__r.name,c2g__Invoice__r.Customer_Name__r.Name,c2g__Invoice__r.c2g__Account__c, c2g__Invoice__r.c2g__Account__r.Berry_ID__c,
            c2g__Invoice__r.c2g__Account__r.Billing_Phone__c,Order_Line_Item__r.Service_Start_Date__c,Order_Line_Item__r.Product2__r.Name,
            Order_Line_Item__r.Service_End_Date__c,Order_Line_Item__r.Talus_Go_Live_Date__c,Order_Line_Item__r.ProductCode__c,Order_Line_Item__r.Product2__c,
            Order_Line_Item__r.Canvass__r.Name,Order_Line_Item__r.Listing_Name__c, Order_Line_Item__r.Directory_Code__c,
            Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.Billing_Frequency__c,Order_Line_Item__r.Line_Number__c,Order_Line_Item__r.Telco__r.Name,
            Order_Line_Item__r.Product2__r.ProductCode,Order_Line_Item__r.Payment_Method__c, c2g__Invoice__c,
            Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c,Order_Line_Item__r.Digital_Product_Requirement__r.business_phone_number_office__c,  
            Order_Line_Item__r.Account__r.ParentId, Order_Line_Item__r.Listing__r.Phone__c FROM c2g__codaInvoiceLineItem__c 
            WHERE c2g__Invoice__r.c2g__InvoiceDate__c >=: Date.parse(invFromDate)  AND c2g__Invoice__r.c2g__InvoiceDate__c <=: Date.parse(invToDate) AND 
            Order_Line_Item__r.Product2__r.Media_Type__c = 'Digital' AND Order_Line_Item__r.Canvass__r.Billing_Entity__c = :selectedEntity
            AND Order_Line_Item__r.Billing_Partner__c!='THE BERRY COMPANY' AND Offset__c=false AND Is_Manually_Created__c = false AND c2g__Invoice__r.Telco_Reversal__c = false AND c2g__Invoice__r.Transaction_Type__c IN: transTypes 
            Order By Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.Account__r.Name ]) {
            String strCompare='';            
            acctIds.add(InvoiceIterator.Order_Line_Item__r.Account__c);
            if(InvoiceIterator.Order_Line_Item__r.Account__r.Account_Number__c != null)  
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.Account_Number__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Account__r.Parent.Account_Number__c != null)  
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.Parent.Account_Number__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Account__r.Primary_Contact__r.Phone != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.Primary_Contact__r.Phone).trim();
            if(InvoiceIterator.Order_Line_Item__r.Billing_Partner__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Billing_Partner__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Canvass__r.Name != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Canvass__r.Name).trim();
            if(InvoiceIterator.c2g__Invoice__r.c2g__InvoiceDate__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.c2g__Invoice__r.c2g__InvoiceDate__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Billing_Frequency__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Billing_Frequency__c).trim();
            if(InvoiceIterator.c2g__Invoice__r.Transaction_Type__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.c2g__Invoice__r.Transaction_Type__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Account__r.X3l_External_ID__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.X3l_External_ID__c).trim();
            if(InvoiceIterator.Order_Line_Item__r.Account__r.Parent_3L_External_ID__c != null) 
            strCompare=strCompare+String.valueof(InvoiceIterator.Order_Line_Item__r.Account__r.Parent_3L_External_ID__c).trim();
        
            if(!mapStrCompareInvoices.containskey(strCompare)) {
                 mapStrCompareInvoices.put(strCompare,new list<c2g__codaInvoiceLineItem__c>());
            }
            mapStrCompareInvoices.get(strCompare).add(InvoiceIterator);
            }
            fetchBillingContact(acctIds);
            if(mapStrCompareInvoices.size() > 0) {
                for(String strIterator : mapStrCompareInvoices.keyset()) {
                    Double BillingAmount=0.0;
                    List<c2g__codaInvoiceLineItem__c> listSILI = new List<c2g__codaInvoiceLineItem__c>();
                    listSILI = mapStrCompareInvoices.get(strIterator);
                    if(listSILI.size() > 0) {
                        String OLIIds = '';
                        set<Id> setOLIIds = new set<Id>();
                        for(c2g__codaInvoiceLineItem__c InvoiceIterator : mapStrCompareInvoices.get(strIterator)) {
                            BillingAmount = BillingAmount + InvoiceIterator.c2g__NetValue__c;
                            setOLIIds.add(InvoiceIterator.Order_Line_Item__c);
                        }
                        if(BillingAmount > 0) {
                            for(Id OLIId : setOLIIds) {
                                OLIIds += OLIId + ';';
                            }
                            List_of_Business__c lob = new List_of_Business__c();   
        					lob = newLOB(listSILI.get(0), BillingAmount, OLIIds.removeEnd(';'), setOLIIds.size());    
        					lob.LOB_Billing_Entity__c = selectedEntity;              
                    		lob.LOB_Billing_Telephone__c = mapAcctIdPrimConPhone.get(listSILI.get(0).Order_Line_Item__r.Account__c);         
                            listLOB.add(lob);    
                        }
					}
                }
            }  
        }
        if(listLOB.size() > 0) {
            insert listLOB;
        }  
    }

    global voId finish(Database.BatchableContext bc) {
        BillingAmountPopulationOnOLIFromLOBBatch obj = new BillingAmountPopulationOnOLIFromLOBBatch();
        Database.executeBatch(obj, Integer.valueOf(system.label.OLI_Bill_Amount_Batch_Size));
    }
    
    public static List_of_Business__c newLOB(Order_Line_Items__c OLI, Double billAmount, String OLIIds, Integer OLICount) {
        return new List_of_Business__c(LOB_X3L_External_ID__c = OLI.Account__r.X3l_External_Id__c,
                    LOB_Account_Name__c = OLI.Account__c,
                    LOB_Account_Number__c = OLI.Account__r.Account_Number__c,
                    LOB_Billing_Amount__c = billAmount,
                    LOB_Billing_Frequency__c = OLI.Billing_Frequency__c,
                    LOB_Billing_Partner__c = OLI.Billing_Partner__c,
                    LOB_Canvass__c = OLI.Canvass__c,
                    LOB_Directory_Code__c = OLI.Directory_Code__c,
                    LOB_Directory_Name__c = OLI.Directory_Name__c,
                    LOB_Edition_Code__c = OLI.Edition_Code__c,
                    LOB_Edition_Name__c = OLI.Directory_Edition_Name__c,
                    LOB_Invoice_Date__c = OLI.Telco_Invoice_Date__c,
                    LOB_Listing_Name__c = OLI.Listing__c,
                    LOB_Listing_Phone_Number__c = OLI.Listing__r.Phone__c,
                    LOB_Media_Type__c = OLI.Media_Type__c,
                    LOB_OLI_Count__c = OLICount,
                    LOB_OLI_Ids__c = OLIIds,
                    LOB_Parent_3L_External_ID__c = OLI.Account__r.Parent_3L_External_Id__c,
                    LOB_Parent_Account__c = OLI.Account__r.ParentId,
                    LOB_Parent_Account_Number__c = OLI.Account__r.Parent.Account_Number__c,
                    LOB_Service_End_Date__c = OLI.Service_End_Date__c,
                    LOB_Service_Start_Date__c = OLI.Service_Start_Date__c,
                    LOB_Telco_Name__c = OLI.Telco__r.Name,
                    LOB_Transaction_Type__c = '');
    }
    
    public static List_of_Business__c newLOB(c2g__codaInvoiceLineItem__c SILI, Double billAmount, String OLIIds, Integer OLICount) {
        return new List_of_Business__c(LOB_X3L_External_ID__c = SILI.Order_Line_Item__r.Account__r.X3l_External_ID__c,
                    LOB_Account_Name__c = SILI.Order_Line_Item__r.Account__c,
                    LOB_Account_Number__c = SILI.Order_Line_Item__r.Account__r.Account_Number__c,
                    LOB_Billing_Amount__c = billAmount,
                    LOB_Billing_Frequency__c = SILI.Order_Line_Item__r.Billing_Frequency__c,
                    LOB_Billing_Partner__c = SILI.Order_Line_Item__r.Billing_Partner__c,
                    LOB_Canvass__c = SILI.Order_Line_Item__r.Canvass__c,
                    LOB_Directory_Code__c = SILI.Order_Line_Item__r.Directory_Code__c,
                    LOB_Edition_Code__c = SILI.Order_Line_Item__r.Edition_Code__c,
                    LOB_Invoice_Date__c = SILI.c2g__Invoice__r.c2g__InvoiceDate__c,
                    LOB_Listing_Name__c = SILI.Order_Line_Item__r.Listing__c,
                    LOB_Listing_Phone_Number__c = SILI.Order_Line_Item__r.Listing__r.Phone__c,
                    LOB_Media_Type__c = SILI.Order_Line_Item__r.Media_Type__c,
                    LOB_OLI_Count__c = OLICount,
                    LOB_OLI_Ids__c = OLIIds,
                    LOB_Parent_3L_External_ID__c = SILI.Order_Line_Item__r.Account__r.Parent_3L_External_ID__c,
                    LOB_Parent_Account__c = SILI.Order_Line_Item__r.Account__r.ParentId,
                    LOB_Parent_Account_Number__c = SILI.Order_Line_Item__r.Account__r.Parent.Account_Number__c,
                    LOB_Service_End_Date__c = SILI.Order_Line_Item__r.Service_End_Date__c,
                    LOB_Service_Start_Date__c = SILI.Order_Line_Item__r.Service_Start_Date__c,
                    LOB_Telco_Name__c = SILI.Order_Line_Item__r.Telco__r.Name,
                    LOB_Transaction_Type__c = SILI.c2g__Invoice__r.Transaction_Type__c);
    }
    
    void fetchBillingContact(Set<Id> acctIds) {
        List<Contact> listContact = new List<Contact>();
        listContact = ContactSOQLMethods.getPrimaryBillContactByAccountID(acctIds); 
        if(listContact.size() > 0) {
            for(Contact con : listContact) {
                mapAcctIdPrimConPhone.put(con.AccountId, con.Phone);
            }
        }
    }  
     
}