global class LOBRecordDeleteBatch Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id FROM List_Of_Business__c';
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<List_Of_Business__c> listLOB) {
    	delete listLOB;
    }

    global void finish(Database.BatchableContext bc) {
    }
}