public with sharing class PendingItemToWinOpportunityController {
	public PendingItemToWinOpportunityController(Apexpages.Standardcontroller controller){
		Opportunity objOpp = (Opportunity)controller.getRecord();
		list<Opportunity> lstOpportunity = OpportunitySOQLMethods.getOpportunityForPendingItem(objOpp.Id);
		list<BigMachines__Quote__c> lstBMIQuote = BMIQuoteSOQLMethods.getBMIQuoteByOpportunityID(new set<Id>{objOpp.Id});
		if(lstOpportunity.size() > 0){
			if(lstBMIQuote == null || lstBMIQuote.size() <= 0) {
				//TO DO :
				//There is BMI Quote
				CommonMethods.addINFO(CommonMessages.opportunityBMIQuote);
			}
			if(lstBMIQuote != null && lstBMIQuote.size() > 0) {
				if(lstBMIQuote[0].BigMachines__BigMachines_Quote_Products__r.size() <= 0) {
					//TO DO:
					//There is no BMI Quote Product
					CommonMethods.addINFO(CommonMessages.opportunityBMIQuoteLineItem);
				}
			}
			
			boolean bFlag = false;
			for(BigMachines__Quote__c iterator : lstBMIQuote) {
				if(iterator.BigMachines__Is_Primary__c) {
					bFlag = true;
				}
			}
			
			if(!bFlag) {
				CommonMethods.addINFO(CommonMessages.opportunityPrimaryBMIQuote);
			}
			
			if(lstOpportunity[0].Signing_Contact__c == null) {
				CommonMethods.addINFO(CommonMessages.opportunitySigningContact);
			}
			
			if(String.isNotEmpty(lstOpportunity[0].Signing_Method__c)) {
				if(lstOpportunity[0].Signing_Method__c.equals('RAS')) {
					if(lstOpportunity[0].RAS_ID__c == null) {
						//TO DO
						//RAS ID should not empty
						CommonMethods.addINFO(CommonMessages.opportunitySigningMethodRSADateID);
					}
					if(lstOpportunity[0].RAS_Date__c == null) {
						//TO DO
						//RAS Date should not empty
						CommonMethods.addINFO(CommonMessages.opportunitySigningMethodRSADateID);
					}
				}
				else {
					if(!lstOpportunity[0].DocuSign_Received__c) {
						//TO DO
						//Quote signed documnet yet to recive from customer
						CommonMethods.addINFO(CommonMessages.opportunitySigningDocument);
					}
				}
			}
			else {
				CommonMethods.addINFO(CommonMessages.opportunitySigningMethod);
			}
			
			if(!CommonMethods.checkOpportunityHasClosedWon(lstOpportunity[0])) {
				CommonMethods.addINFO(CommonMessages.opportunityStage);
			}
		}
	}
}