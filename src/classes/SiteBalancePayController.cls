public class SiteBalancePayController {

    public String contactPhone { get; set; }

    public String contactEmail { get; set; }
    public String contactlastname { get; set; }
    public String contactfirstname { get; set; }
    public string accountNumber { get; set; }
    public pymt__PaymentX__c payment;
    public string contactId { get; set; }
    public decimal payAmount { get; set; }
    public Account acct { get; set; }
    public boolean createContact { get; set; }
    private pymt__Processor_Connection__c processor;
    private String processorName;
    private String currencyISOCode;
    public AccessControllerWithoutSharing_Berry acwos;
    public decimal invoiceSum {get; set; }
    private string cancelURL = '/sitebalancePay?acctId=';
    private string finishURL = 'http://www.theberrycompany.com/';
    
    public SiteBalancePayController() {


    this.accountNumber = ApexPages.CurrentPage().getParameters().get('acctId');
     
      this.acwos = new AccessControllerWithoutSharing_Berry();
      this.invoiceSum = 0;
      this.payAmount = 0;
      this.createContact = false;


      Id defaultProcessorConnection = pymt.PaymentX.getSiteDefaultProcessorConnection(Site.getName());
          if (defaultProcessorConnection == null) {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The default processor connection for this site has not been configured.'));            
                return;
            }
       else {
               string soql = 'Select id, pymt__default_connection__c, pymt__processor_id__c, pymt__test_mode__c, pymt__Default_Currency__c, pymt__PP_Merchant_Id__c '+
               'from pymt__Processor_Connection__c where isDeleted = false and id = \''+ String.escapeSingleQuotes(defaultProcessorConnection) + '\'';
                    
                 pymt__Processor_Connection__c[] validConnections = this.acwos.dbQuery(soql);
                 if (validConnections.size() > 0) {
                        this.processor = validConnections[0];
                        this.currencyISOCode = validConnections[0].pymt__Default_Currency__c;
                 }
                 else {
                         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The default processor connection for this site can not be found.'));            
                        return;
                 }
        }


    }
    
    public List<SelectOption> contactOptionList {
        get {
                        List<SelectOption> options = new List<SelectOption>();
                        if (this.acct != null) {
                                for (Contact c : this.acct.contacts) {
                                        options.add(new SelectOption(c.id, c.name));
                                }
                                options.add(new SelectOption('Create Contact','Create Contact'));
                                return options;
                        }
                        else { return null; }
                }
        set {}
    }
    
    
        public Boolean setupPaymentRecord() {        
            
            // Setup Payment record
            if (this.payment == null) {
                this.payment = new pymt__PaymentX__c();
            }
            try {
                this.payment.name = 'Payment';
                this.payment.pymt__amount__c = this.payAmount;
                if(!pymt.Util.isNullOrEmpty(contactId))
                this.payment.pymt__contact__c = this.contactId;
                this.payment.pymt__status__c = 'In Process';
                this.payment.pymt__date__c = System.today();
                this.payment.pymt__currency_ISO_code__c = this.currencyISOCode;
                this.payment.pymt__Payment_Type__c = 'Credit Card';
                this.payment.pymt__Invoice_Number__c = String.valueOf(Math.Random()).substring(2,9);
                 this.payment.name = 'Payment ' + this.payment.pymt__Invoice_Number__c;
                /*
               this.payment.pymt__Billing_First_Name__c = this.billingFirstName;
                this.payment.pymt__Billing_Company__c = this.companyName;
                this.payment.pymt__Billing_Last_Name__c = this.billingLastName;
                this.payment.pymt__Billing_Street__c = this.billingStreet;
                this.payment.pymt__Billing_City__c = this.billingCity;
                this.payment.pymt__Billing_State__c = this.billingState;
                this.payment.pymt__Billing_Postal_Code__c = this.billingPostalCode;
                this.payment.pymt__Billing_Country__c = this.billingCountry;
                this.payment.pymt__Billing_Email__c = this.emailAddress;

           
                */
                this.payment.pymt__payment_processor__c = this.processor.pymt__processor_id__c;
                this.payment.pymt__processor_connection__c = this.processor.id;
                this.payment.pymt__account__c = this.acct.id;
                this.payment.pymt__Is_Test_Transaction__c = this.processor.pymt__Test_Mode__c;
                this.payment.pymt__log__c = ' ++++ Partial Site Payment toward FinancialForce Invoices:\n'+ 'Online payment in the amount of '+
                                          this.payment.pymt__Amount__c + this.payment.pymt__Currency_ISO_Code__c;
    
               // if (this.payment.pymt__amount__c < this.totalInvoiceAmount) {       
                 //       this.payment.name += ' (Balance Due)';
               // }                       
                if (this.payment.id == null) {
                    this.acwos.dbInsert(this.payment); 
                } else {
                    this.acwos.dbUpdate(this.payment);
                }   
            }
            catch(system.exception ex) {
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred creating a Payment Record. ' + ex.getMessage()));   
                return false;
            }
            return true;
    }

    
    public PageReference payNow() {
        PageReference nextPage;
        try {
            System.Debug('Contact Id of newly created Contact after clicking PAYNOW : ' + this.contactId);
            if(this.contactId == 'Create Contact'){
                if (pymt.Util.isNullOrEmpty(this.contactfirstname) || pymt.Util.isNullOrEmpty(this.contactlastname)) {
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Cannot create Contact.  First or Last Name is missing.'));
                  return null;
                }
                if (pymt.Util.isNullOrEmpty(this.contactphone) || this.contactPhone.length() != 12) {
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Cannot create Contact.  Phone number is required to be in the following format:  xxx-xxx-xxxx'));
                  return null;
                }
                if (findContactByEmail(this.contactEmail) == true) {
                  System.Debug('Contact Id of newly created Contact after clicking create : ' + this.contactId);
                  System.Debug('Account Id of newly created Contact after clicking create : ' + this.acct.Id);
                  this.createContact = false;
                  string soql;
                  list<account> acctList;
                  soql = 'Select Account_number__c, id, name, (select name, id from Contacts) from Account where id = \''+String.escapeSingleQuotes(this.acct.id)+'\'';
                  acctList = this.acwos.dbQuery(soql);
                  acct = acctList[0];  
                }            
            }

            if (pymt.Util.isNullOrEmpty(contactId)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please Select a Billing Contact'));
                this.createContact = false;
                return null;
            }
            
            if (this.payAmount == null || this.payAmount <= 0) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please Enter an amount greater than 0.00'));
                return null;
            }

            if (!this.setupPaymentRecord()) {
                        return null;
            }
            nextPage = Page.pymt__SiteCheckout;
            nextPage.getParameters().put('pid', this.payment.id);
            this.cancelURL = this.cancelURL + this.acct.account_Number__C;
            if (!pymt.Util.isNullOrEmpty(this.cancelURL)) nextPage.getParameters().put('cancel_url', EncodingUtil.urlEncode(this.cancelURL,'UTF-8'));        
            if (!pymt.Util.isNullOrEmpty(this.finishURL)) nextPage.getParameters().put('finish_url', EncodingUtil.urlEncode(this.finishURL,'UTF-8'));   
        }
        catch(exception ex) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'An error occurred:  ' + ex.getMessage() + ' Line Number:  ' + ex.getLineNumber()));
             return  null;
        }
        return nextPage;
        
        
}


    public PageReference searchBalance() {
    string soql;
       try {
                if (pymt.Util.isNullOrEmpty(this.accountNumber)) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please Enter an Account Number'));
                    return null;
                }
               
                 else if (this.accountNumber.length() < 5) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Account Number must be at least 5 digits'));
                    return null;
                }
              
                
                else if (this.accountNumber.startswith('1')) {
                    soql = 'Select Account_number__c, id, name, (select name, id from Contacts) from Account where X3l_External_ID__c = ' + '\'' +String.escapeSingleQuotes(this.accountNumber)+'\'';
                }
                else  {
                   soql = 'Select Account_number__c, id, name, (select name, id from Contacts) from Account where Account_Number__c like \'%'+String.escapeSingleQuotes(this.accountNumber)+'\'';
                    
                }
                
                
             
                 Account[] acctList = this.acwos.dbQuery(soql);          
                     
             if (acctList.size() > 0) {
                this.acct = new Account();
                this.acct = acctList[0];
                if (acct.contacts.size() == 0) {
                    this.contactId = 'Create Contact';
                    this.createContact();
                }
                if (acct.contacts.size() == 1) {
                         this.contactId = acct.contacts[0].id;

                 }
             }                                 
             else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Could not locate Account.  Please verify the Account Number.'));
                 return null;
             }
             
            soql = 'Select c2g__OutstandingValue__c From c2g__codaInvoice__c  where ' +
                   'Payment_Type__c != \'ECheck\' and c2g__Opportunity__r.Billing_Partner__c != \'THE BERRY COMPANY\' and c2g__PaymentStatus__c != \'Paid\' ' +
                   'AND c2g__Account__c = \''+String.escapeSingleQuotes(this.acct.id)+'\'';   
                              
               List<c2g__codaInvoice__c > invList = this.acwos.dbQuery(soql);
               
               
                for (c2g__codaInvoice__c  tmpInv : invList) {
                   this.invoiceSum = this.invoiceSum + tmpInv.c2g__OutstandingValue__c;  
                }
         }
     catch(exception ex) {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error retrieving Account or Invoice Information:' + soql + ex.getMessage() + '. Line Number: '  + ex.getLineNumber()));
             return null;
       }  
       return null;
       }

  public boolean findContactbyEmail(string tmpEmail) {
    Contact updatedContact;
        try {
                
    
                        updatedContact = new Contact();
                        updatedContact.firstname = this.contactfirstname;
                        updatedContact.lastname = this.contactlastname;
                        if (!pymt.Util.IsNullOrEmpty(this.contactEmail)) {
                            updatedContact.email = this.contactEmail;
                        }
                        updatedContact.accountId = this.acct.id;
                        updatedContact.phone = this.contactPhone;
                        acwos.dbInsert(updatedContact);   
                        
                   
                    this.contactId = updatedContact.id;
                    return true;
        }
        catch(exception ex) {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'An error occurred creating contact  ' + ex.getMessage() + ' Line Number:  ' + ex.getLineNumber()));
            return false;
        }
    }
    
    
    public PageReference cancelCreateContact() {
        this.createContact = false;
        this.contactEmail = '';
        this.contactlastname = '';
        this.contactfirstname = '';
        this.contactphone = '';
    
        return null;
    }

    public PageReference findContact() {
        try {
 
          if (pymt.Util.isNullOrEmpty(this.contactfirstname) || pymt.Util.isNullOrEmpty(this.contactlastname)) {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Cannot create Contact.  First or Last Name is missing.'));
              return null;
          }
          if (pymt.Util.isNullOrEmpty(this.contactphone) || this.contactPhone.length() != 12) {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Cannot create Contact.  Phone number is required to be in the following format:  xxx-xxx-xxxx'));
              return null;
          }
          
          if (findContactByEmail(this.contactEmail) == true) {
              System.Debug('Contact Id of newly created Contact after clicking create : ' + this.contactId);
              System.Debug('Account Id of newly created Contact after clicking create : ' + this.acct.Id);
              this.createContact = false;
              string soql;
              list<account> acctList;
               soql = 'Select Account_number__c, id, name, (select name, id from Contacts) from Account where id = \''+String.escapeSingleQuotes(this.acct.id)+'\'';
               acctList = this.acwos.dbQuery(soql);
               acct = acctList[0];  
          }
        }
        catch(exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage() + ' Line:  ' + ex.getLineNumber()));
        }
        return null;
    }
    
    public PageReference createContact() {
        System.Debug('createContact launched : ' + contactId);
        if(this.contactId == 'Create Contact'){
          this.createContact = true;  
        }
        if(this.contactId != 'Create Contact'){
          cancelCreateContact();
        }

        return null;
    }

                        
}