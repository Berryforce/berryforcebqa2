global class UpdateHeading implements Database.Batchable<sobject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String SOQL = 'Select ID, Name, Directory_Heading_Name__c from Directory_Heading__c where Directory_Heading_Name__c = null';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List< Directory_Heading__c > listDirPagination) {
        for(Directory_Heading__c itr : listDirPagination) {
            itr.Directory_Heading_Name__c = itr.Name;
        }
        
        update listDirPagination;
    }
    
     global void finish(Database.BatchableContext bc){
     }
}