/**********************************************************************
Apex class to create a Case, with info coming via SOAP WebServices call
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 06/01/2015
$Id$
***********************************************************************/
global class Email2Case_SOAP_Webservice {

    public static Set<String> csRsn1 = new Set<String>{'Domain issue', 'Video Issue', 'YPC Issue', 'Vendor Fulfillment Issue', 'YPC Issue – Invalid Data', 'YPC Issue – Misc.', 'Cancellation Issue'};
    public static Set<String> csRsn2 = new Set<String>{'General Query', 'Invalid DFF Data', 'YPC Issue – Limited Inventory', 'Missing info on DFF', 'Website Issue'};

    //Webservices method to accept subject, notes, dffId, tableIdItemId, and patchId to create a new Case
    webService static Id createTalusCase(String subject, String notes, String dffId, String tableIdItemId, String caseReason) {

        //Query DFF to get OwnerId, in order to assign Case to that DFF
        Digital_Product_Requirement__c dff = [SELECT Id, Name, CreatedById, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.Email, OwnerId, Account__r.Id, OrderLineItemID__r.Billing_Contact__r.Id, OrderLineItemID__c, (SELECT Id, Name, Status__c, Digital_Product_Requirement__c from Order_Line_Items__r where Digital_Product_Requirement__c =: dffId) from Digital_Product_Requirement__c where id = : dffId];

        //System.debug('************DFF************' + dff);

        //Create new Case record with Talus_Case_RT recordtype assigned
        Case c = new Case();
        if(csRsn1.contains(caseReason)){
        c.OwnerId = Label.Berry_Digital_Support_Queue_Id;
        } else if(csRsn2.contains(caseReason)){
        c.OwnerId = dff.CreatedById;
        }
        c.AccountId = dff.Account__r.Id;
        c.ContactId = dff.OrderLineItemID__r.Billing_Contact__r.Id;
        c.Received_From__c = 'Other';
        c.RecordTypeId = Label.Case_RecordType_Id;
        c.Case_Category__c = 'Query';
        c.Reason = caseReason;
        c.Status = 'New';
        c.Origin = 'Email';
        c.Subject = Subject;
        c.Description = Notes;
        c.Digital_Product_Requirement__c = dffId;
        c.Talus_DffId__c = dff.Id;
        c.TeamTrack_tableIdItemId__c = tableIdItemId;
        c.DFF_Owner_Email__c = dff.CreatedBy.Email;

        insert c;

        //return Case Id as a success response
        return c.id;

    }

    //Webservice method for TeamTrack user to be able to reopen SFDC Case
    webService static String reOpenCase(String subject, String notes, String caseId, String caseReason) {

        String csRspns;
        Id dffOwnrId;
        //Query Case record that needs to be reopned
        Case c = [SELECT Id, Digital_Product_Requirement__c, AccountId, ContactId, Reason, Subject, Description, Link_To_Case__c, DFF_CreatedBy_Id__c, DFF_Owner_Email__c, DFF_Owner_FullName__c from Case where id=:caseId];

        //Update case status to reopen
        if(csRsn1.contains(caseReason)){
        c.OwnerId = Label.Berry_Digital_Support_Queue_Id;
        } else if(csRsn2.contains(caseReason)){
        c.OwnerId = c.DFF_CreatedBy_Id__c;
        }        
        c.Subject = subject;
        c.Description = notes;
        c.Reason = caseReason;
        c.Status = 'Reopened';

        try{
            update c;
            csRspns = 'Case status is successfully updated to reopen';

            //Send email to Case/DFF Owner only if it is a need more info related
            if(c.OwnerId != Label.Berry_Digital_Support_Queue_Id){

            String mailBody = 'Dear ' + c.DFF_Owner_FullName__c + '<br><br>Case has been reopened for one of your Data Fulfillment Forms, please review the following Case<br><br>Link To Case Record: ' + c.Link_To_Case__c + '<br><br><br><br>Thank you';

                List<String> lstToAddress = new List<String>{c.DFF_CreatedBy_Id__c};
                List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();

                for (String trgtObjId: lstToAddress) {
                    lstMsgsToSend.add(CommonUtility.emailUsingTargetObjectId(trgtObjId, ' Case Created for Data Fulfillment Form: ' + c.Digital_Product_Requirement__c, mailBody));
                }

                Messaging.sendEmail(lstMsgsToSend);                

                /*
                List<String> lstToAddress = new List<String>{c.DFF_Owner_Email__c};
                List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage snglMail = new Messaging.SingleEmailMessage();
                snglMail.setToAddresses(lstToAddress);
                snglMail.setSubject(' Case Created for Data Fulfillment Form: ' + c.Digital_Product_Requirement__c);
                snglMail.setHTMLBody(mailBody);
                lstMsgsToSend.add(snglMail);
                Messaging.sendEmail(lstMsgsToSend);
                */
            }

        } catch(exception e){
            csRspns = 'Case update is failed dude to: ' + e.getMessage();
        }

        return csRspns;

    }

}