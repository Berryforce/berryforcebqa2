public with sharing class NationalStagingOrderSetHandlerController {
    public static void onBeforeInsert(list<National_Staging_Order_Set__c> lstNSOS) {
        //NationalErrorValidationHandlerController.HeaderLevelValidation(lstNSOS);
        ID eliteRTId = CommonMethods.getRedordTypeIdByName(CommonMessages.eliteNSRT, CommonMessages.nationalStagingOrderSetObjectName);
        populateCMRAccount(lstNSOS, eliteRTId);
        populateClientAccount(lstNSOS, eliteRTId);
        duplicateStandingOrderSetForITrans(lstNSOS);
    }
    
    private static void populateCMRAccount(list<National_Staging_Order_Set__c> lstNSOS, ID eliteRTId) {
        set<String> setCMRNumber = new set<String>();
        map<String, Account> mapCMRAccount = new map<String, Account>();
        for(National_Staging_Order_Set__c iterator : lstNSOS) {
            if(iterator.RecordTypeId == eliteRTId) {
                if(String.isNotBlank(iterator.CMR_Number__c)) {
                    setCMRNumber.add(iterator.CMR_Number__c);
                }
            }       
        }
        
        if(setCMRNumber.size() > 0) {
            list<Account> lstAccount = AccountSOQLMethods.getCMRAccountByNumber(setCMRNumber);
            for(Account iterator : lstAccount) {
                if(!mapCMRAccount.containsKey(iterator.AccountNumber)) {
                    mapCMRAccount.put(iterator.AccountNumber, iterator);
                }
            }
        }
        
        if(mapCMRAccount.size() > 0) {
            for(National_Staging_Order_Set__c iterator : lstNSOS) {
                if(mapCMRAccount.containsKey(iterator.CMR_Number__c)) {
                    iterator.CMR_Name__c = mapCMRAccount.get(iterator.CMR_Number__c).Id;
                }
            }
        }       
    }
   
    private static void populateClientAccount(list<National_Staging_Order_Set__c> lstNSOS, ID eliteRTId) {
        set<String> setCMRClientNumber = new set<string>();
        map<String, Account> mapClientAccount = new map<String, Account>();
        string strCMRClientNumber;
        for(National_Staging_Order_Set__c iterator : lstNSOS) {
        	System.debug('Inside iterator.RecordTypeId '+iterator.RecordTypeId);
        	System.debug('Inside eliteRTId '+eliteRTId);
            if(iterator.RecordTypeId == eliteRTId) {
                strCMRClientNumber = iterator.CMR_Number__c+'-'+iterator.Client_Number__c;
                setCMRClientNumber.add(strCMRClientNumber);
            }
        }
        
        if(setCMRClientNumber.size()>0) {
            list<Account> lstAccount = AccountSOQLMethods.getClientAccountByNumber(setCMRClientNumber);
            
            for(Account iterator : lstAccount) {
                if(!mapClientAccount.containsKey(iterator.CMR_Number_Client_Number__c)) {
                    mapClientAccount.put(iterator.CMR_Number_Client_Number__c, iterator);
                }
            }
        }
        
        //set<String> setCheckDuplicate = new set<String>();
        list<Account> lstInsertAccount = new list<Account>();
        string strCMRClientNumberNS;
        //if(mapClientAccount.size() > 0) {           
            for(National_Staging_Order_Set__c iterator : lstNSOS) {
                if(iterator.RecordTypeId == eliteRTId) {
                	strCMRClientNumberNS = iterator.CMR_Number__c+'-'+iterator.Client_Number__c;
	                if(mapClientAccount.size() > 0 && mapClientAccount.get(strCMRClientNumberNS) != null){
	                    iterator.Client_Name__c = mapClientAccount.get(strCMRClientNumberNS).Id;
	                }
	                else{
	                    System.debug('#######Inside Else');
	                    lstInsertAccount.add(generateNewClientAccount(iterator));
	                }
                }
            }
       // }
       
        if(lstInsertAccount.size() > 0) {
            insert lstInsertAccount;
         }
         set<Id> setNewActId = new set<Id>();
         for(account actNew : lstInsertAccount){
            setNewActId.add(actNew.Id);
         }
         list<account> NewInsertActsLst = AccountSOQLMethods.getAccountsByAcctId(setNewActId);
         map<string,Account> mapClientNewAccount = new map<String,Account>();
         for(Account iterator : NewInsertActsLst) {
             if(!mapClientNewAccount.containsKey(iterator.CMR_Number_Client_Number__c)) {
                mapClientNewAccount.put(iterator.CMR_Number_Client_Number__c, iterator);
            }
         }
        
         if(mapClientNewAccount.size() > 0) {
            for(National_Staging_Order_Set__c iterator : lstNSOS) {
                string strCMRClientNumberNSFinal = iterator.CMR_Number__c+'-'+iterator.Client_Number__c;
                if(mapClientNewAccount.get(strCMRClientNumberNSFinal) != null) {
                    iterator.Client_Name__c = mapClientNewAccount.get(strCMRClientNumberNSFinal).Id;
                }
            }
         }
    
    }
    
    private static Account generateNewClientAccount(National_Staging_Order_Set__c iterator) {
       return new Account (Name = iterator.Client_Name_Text__c+' '+iterator.Client_Number__c, AccountNumber = iterator.Client_Number__c, 
                                     RecordTypeId = CommonMessages.clientAccountRecordTypeID, ParentId = iterator.CMR_Name__c);      
    }
    
    private static void duplicateStandingOrderSetForITrans(List<National_Staging_Order_Set__c> lstNSOS) {
        set<String> setClientNumber = new set<String>();
        set<String> setCMRNumber = new set<String>();
        List<National_Staging_Order_Set__c> listNSOS = new List<National_Staging_Order_Set__c>();
        Map<String, National_Staging_Order_Set__c> mapCMRClientComboNSOS = new Map<String, National_Staging_Order_Set__c>();
        
        for(National_Staging_Order_Set__c iterator : lstNSOS) {
            if(iterator.TRANS_Code__c == 'I') {
                if(String.isNotBlank(iterator.CMR_Number__c)) {
                    setCMRNumber.add(iterator.CMR_Number__c);
                }
                
                if(String.isNotBlank(iterator.Client_Number__c)) {
                    setClientNumber.add(iterator.Client_Number__c);
                }
            }
        }
        
        if(setCMRNumber.size() > 0 && setClientNumber.size() > 0) {
            listNSOS = NationalStagingOrderSetSOQLMethods.getNSOSByCMRAndClientNumberForTransI(setCMRNumber, setClientNumber);
            
            if(listNSOS.size() > 0){
                for(National_Staging_Order_Set__c iterator : listNSOS) {
                    mapCMRClientComboNSOS.put(iterator.CMRClientCombo__c, iterator);
                }
                
                for(National_Staging_Order_Set__c iterator : lstNSOS) {
                    if(mapCMRClientComboNSOS.containsKey(String.valueOf(iterator.CMR_Number__c) + String.valueOf(iterator.Client_Number__c))) {
                        iterator.addError('Standing Order already present - ' + mapCMRClientComboNSOS.get(String.valueOf(iterator.CMR_Number__c) + String.valueOf(iterator.Client_Number__c)).Name);
                    }
                }
            }
        }
    }
}