@IsTest(SeeAlldata=true)
public with sharing class LocalBillingCreateTelcoControllerTest {

  public static testMethod void TestLocalBillingCreatePrintTelcoController(){
            Canvass__c c=TestMethodsUtility.createCanvass();
            
            list<Account> lstAccount1 = new list<Account>();
            lstAccount1.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount1[0].Billing_Anniversary_Date__c=date.today();
            insert lstAccount1;  
            Account newAccount1 = new Account();
            for(Account iterator : lstAccount1) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount1 = iterator;
                }
           }
            Telco__c objTelco1 =TestMethodsUtility.createTelco(newAccount1.Id);
            objTelco1.Telco_Code__c = 'Test';
            update objTelco1;
            system.assertNotEquals(newAccount1.ID, null);
            Contact newContact1 = TestMethodsUtility.createContact(newAccount1.Id);
            Pricebook2 newPriceBook1 = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv1 = TestMethodsUtility.createDivision();
        
            /*Directory__c objDir1 = TestMethodsUtility.generateDirectory();
            objDir1.Telco_Recives_Electronice_File__c=true;
            objDir1.Telco_Provider__c = objTelco1.Id;
            objDir1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDir1.Publication_Company__c = newAccount1.Id;
            objDir1.Division__c = objDiv1.Id;
            objDir1.Directory_Code__c = '100000';
            insert objDir1;*/
            Directory__c objDir1 =TestMethodsUtility.createDirectory(); 
            
            Directory_Heading__c objDH1 = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS1 = TestMethodsUtility.createDirectorySection(objDir1);
            Section_Heading_Mapping__c objSHM1 = TestMethodsUtility.generateSectionHeadingMapping(objDS1.Id, objDH1.Id);
            insert objSHM1;
            
            Directory__c objDirNew1 = TestMethodsUtility.generateDirectory();
            objDirNew1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDirNew1.Publication_Company__c = newAccount1.Id;
            objDirNew1.Division__c = objDiv1.Id;
            objDirNew1.Directory_Code__c = '100001';
            insert objDirNew1;
            
            Directory_Edition__c objDirEdNew2 = TestMethodsUtility.generateDirectoryEdition(objDirNew1);
            objDirEdNew2.XML_Output_Total_Amount__c=200;
            objDirEdNew2.Pub_Date__c =Date.today();
            objDirEdNew2.Bill_Prep__c=date.today();
            insert objDirEdNew2;
            
            Product2 newProduct3 = TestMethodsUtility.generateproduct();
            newProduct3.Family = 'Print';
            insert newProduct3;
            
            Product2 objProd3 = new Product2();
            objProd3.Name = 'Test';
            objProd3.Product_Type__c = 'Print';
            objProd3.ProductCode = 'WLCSH';
            objProd3.Print_Product_Type__c='Display';
            insert  objProd3;
            
            Product2 objProd4 = new Product2();
            objProd4.Name = 'Test';
            objProd4.Product_Type__c = 'Print';
            objProd4.ProductCode = 'GC50';
            objProd4.Print_Product_Type__c='Specialty';          
            insert objProd4;
            
            Opportunity newOpportunity2 = TestMethodsUtility.generateOpportunity('new');
            newOpportunity2.AccountId = newAccount1.Id;
            newOpportunity2.Pricebook2Id = newPriceBook1.Id;
            insert newOpportunity2;
            
            Order__c newOrder1 = TestMethodsUtility.createOrder(newAccount1.Id);
            
            Order_Group__c newOrderSet1 = TestMethodsUtility.createOrderSet(newAccount1, newOrder1, newOpportunity2);
            
          
            c2g__codaCompany__c companyObj =TestMethodsUtility.generateCompany();
            
            Order_Line_Items__c objOrderLineItemStmtOdd = new Order_Line_Items__c(Billing_Partner__c='Berry123',Account__c=newAccount1.Id, 
            Billing_Contact__c=newContact1.id, Opportunity__c=newOpportunity2.id, Order_Group__c=newOrderSet1.id,
            Order__c=newOrder1.id,Product2__c=objProd4.Id,is_p4p__c=false,media_type__c='Print',
            Directory_Edition__c = objDirEdNew2 .Id,Directory__c=objDirNew1.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Statement',
            Package_ID__c='pkgid_12',Payments_Remaining__c=10,Successful_Payments__c=1);
            insert objOrderLineItemStmtOdd;
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount1,newOpportunity2);
            invoice.Customer_Name__c=newAccount1.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.c2g__OwnerCompany__c = companyObj.id;
            invoice.c2g__Account__c= newAccount1.id;
            invoice.Is_Statement_Generated__c=false;
            invoice.SI_Payment_Method__c='Statement'; 
            insert invoice;
            
            c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDirNew1);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd4);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            c2g__codaDimension3__c  dimension3_ss=TestMethodsUtility.createDimension3(objOrderLineItemStmtOdd);
            c2g__codaInvoiceLineItem__c invoiceLiStmt=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItemStmtOdd,invoice,objProd4,dimension1,dimension2,dimension3_ss,dimension4 );
       
            invoice.SI_Successful_Payments__c=1;
            invoice.SI_P4P__c=false;
            invoice.SI_Media_Type__c='Print';
            invoice.c2g__InvoiceStatus__c='Complete';
            invoice.SI_Directory_Code__c='100001';
            invoice.SI_Directory_Edition_Code__c=[Select Edition_Code__c from Directory_Edition__c where id=:objDirEdNew2.Id].Edition_Code__c ;
            invoice.SI_Payment_Method__c='Telco Billing';
            update invoice;
            
            Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today(),From_Date__c = date.newinstance(2012, 3, 17),To_Date__c =date.newinstance(2015, 1, 16));
            insert objDummy;
            
            Digital_Telco_Scheduler__c dts=new Digital_Telco_Scheduler__c ();
            dts.Telco__c=objTelco1.Id;
            dts.Billing_Entity__c='CENTURYNEW';
            dts.Invoice_To_Date__c=date.today();
            dts.Invoice_From_Date__c=date.today().adddays(-30);
            dts.Telco_Receives_EFile__c=false;
            dts.XML_Output_Total_Amount__c=200;
            dts.Bill_Prep_Date__c =date.today();
            insert dts;
                                          
            Test.startTest();
            LocalBillingCreateTelcoController  crTelco = new LocalBillingCreateTelcoController ();
            crTelco.strPrintDigital='Print';
            crTelco.objDummy = objDummy;
            crTelco.fetchDE();
            crTelco.strSelectedDirectoryEdition=objDirEdNew2.Id;
            crTelco.fetchDirectoryEditionForReportPrint();
            crTelco.createTelcoFilePrint();
            crTelco.checkTelcoXMLOutputPrint();
            crTelco.clickGoPrint();
            Test.stopTest();
  }
  
  
  public static testMethod void TestLocalBillingCreatePrintDigitalTelcoController(){
           Canvass__c c=TestMethodsUtility.createCanvass();
           c.Billing_Entity__c='CENTURYNEW';
           update c;
           
            list<Account> lstAccount1 = new list<Account>();
            lstAccount1.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount1[0].Billing_Anniversary_Date__c=date.today();
            insert lstAccount1;  
            Account newAccount1 = new Account();
            for(Account iterator : lstAccount1) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount1 = iterator;
                }
           }
            Telco__c objTelco1 =TestMethodsUtility.createTelco(newAccount1.Id);
            objTelco1.Telco_Code__c = 'Test';
            update objTelco1;
            system.assertNotEquals(newAccount1.ID, null);
            Contact newContact1 = TestMethodsUtility.createContact(newAccount1.Id);
            Pricebook2 newPriceBook1 = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv1 = TestMethodsUtility.createDivision();
        
            /*Directory__c objDir1 = TestMethodsUtility.generateDirectory();
            objDir1.Telco_Recives_Electronice_File__c=true;
            objDir1.Telco_Provider__c = objTelco1.Id;
            objDir1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDir1.Publication_Company__c = newAccount1.Id;
            objDir1.Division__c = objDiv1.Id;
            objDir1.Directory_Code__c = '100000';
            insert objDir1;*/
            Directory__c objDir1=TestMethodsUtility.createDirectory(); 
            
            Directory_Heading__c objDH1 = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS1 = TestMethodsUtility.createDirectorySection(objDir1);
            Section_Heading_Mapping__c objSHM1 = TestMethodsUtility.generateSectionHeadingMapping(objDS1.Id, objDH1.Id);
            insert objSHM1;
            
            Directory__c objDirNew1 = TestMethodsUtility.generateDirectory();
            objDirNew1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDirNew1.Publication_Company__c = newAccount1.Id;
            objDirNew1.Division__c = objDiv1.Id;
            objDirNew1.Directory_Code__c = '100001';
            insert objDirNew1;
            Directory_Edition__c objDirEdNew2 = TestMethodsUtility.generateDirectoryEdition(objDirNew1);
            objDirEdNew2.XML_Output_Total_Amount__c=200;
            objDirEdNew2.Pub_Date__c =Date.today().addMonths(4);
            objDirEdNew2.Bill_Prep__c=date.today();
            insert objDirEdNew2;
            
            Product2 objProd4 = new Product2();
            objProd4.Name = 'Test';
            objProd4.Product_Type__c = 'Digital';
            objProd4.ProductCode = 'GC50';
            objProd4.Print_Product_Type__c='Specialty';          
            insert objProd4;
            
            Opportunity newOpportunity2 = TestMethodsUtility.generateOpportunity('new');
            newOpportunity2.AccountId = newAccount1.Id;
            newOpportunity2.Pricebook2Id = newPriceBook1.Id;
            newOpportunity2.Account_Primary_Canvass__c=c.Id;
            insert newOpportunity2;
            
            Order__c newOrder1 = TestMethodsUtility.createOrder(newAccount1.Id);
            
            Order_Group__c newOrderSet1 = TestMethodsUtility.createOrderSet(newAccount1, newOrder1, newOpportunity2);
            
          
            c2g__codaCompany__c companyObj =TestMethodsUtility.generateCompany();
            
            Order_Line_Items__c objOrderLineItemStmtOdd = new Order_Line_Items__c(Billing_Partner__c='Berry123',Account__c=newAccount1.Id, 
            Billing_Contact__c=newContact1.id, Opportunity__c=newOpportunity2.id, Order_Group__c=newOrderSet1.id,
            Order__c=newOrder1.id,Product2__c=objProd4.Id,is_p4p__c=false,media_type__c='Digital',
            Directory_Edition__c = objDirEdNew2 .Id,Directory__c=objDirNew1.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Statement',
            Package_ID__c='pkgid_12',Payments_Remaining__c=10,Successful_Payments__c=1);
            insert objOrderLineItemStmtOdd;
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount1,newOpportunity2);
            invoice.Customer_Name__c=newAccount1.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.c2g__OwnerCompany__c = companyObj.id;
            invoice.c2g__Account__c= newAccount1.id;
            invoice.Is_Statement_Generated__c=false;
            invoice.SI_Payment_Method__c='Statement'; 
            insert invoice;
            
            c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDirNew1);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd4);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            c2g__codaDimension3__c  dimension3_ss=TestMethodsUtility.createDimension3(objOrderLineItemStmtOdd);
            c2g__codaInvoiceLineItem__c invoiceLiStmt=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItemStmtOdd,invoice,objProd4,dimension1,dimension2,dimension3_ss,dimension4 );
       
            invoice.SI_Successful_Payments__c=1;
            invoice.SI_P4P__c=false;
            invoice.SI_Media_Type__c='Digital';
            invoice.c2g__InvoiceStatus__c='Complete';
            invoice.SI_Directory_Code__c='100001';
            string pubmonth=String.valueof(Date.today().month()).length()<2 ? '0'+String.valueof(Date.today().month()) : String.valueof(Date.today().month());
            invoice.SI_Directory_Edition_Code__c=String.valueof(Date.today().year()).substring(2,4)+pubmonth;
            update invoice;
            
            Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today(),From_Date__c = date.newinstance(2012, 3, 17),To_Date__c =date.newinstance(2015, 1, 16));
            insert objDummy;
            
            Digital_Telco_Scheduler__c dts=new Digital_Telco_Scheduler__c ();
            dts.Telco__c=objTelco1.Id;
            dts.Billing_Entity__c='CENTURYNEW';
            dts.Invoice_To_Date__c=date.today();
            dts.Invoice_From_Date__c=date.today().adddays(-30);
            dts.Telco_Receives_EFile__c=false;
            dts.XML_Output_Total_Amount__c=200;
            dts.Bill_Prep_Date__c =date.today();
            insert dts;
                                          
            Test.startTest();
            LocalBillingCreateTelcoController  crTelco = new LocalBillingCreateTelcoController ();
            crTelco.strPrintDigital='Digital';
            crTelco.objDummy = objDummy;
            crTelco.fetchDE();
            crTelco.strSelectedBillingEntity=dts.Id;
            crTelco.objDTS=dts;
            crTelco.CheckTelcoOutputDigital();
            crTelco.SendTelcoFileDigital();
            crTelco.clickGoDigital();
            
            Test.stopTest();
  }
}