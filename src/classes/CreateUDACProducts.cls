/*****************************************************************
Batch class to create Product records for each Fulfillment Package
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 10/07/2014
$Id$
******************************************************************/
global class CreateUDACProducts implements Database.Batchable < sObject > , Database.AllowsCallouts {

    global String query;

    global CreateUDACProducts(String query) {
        this.query = query;
    }

    global Database.Querylocator start(Database.BatchableContext BC) {
        //System.debug('************The Query************' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Package_ID__c > scope) {

        List < Package_Product__c > lstpcksprs = new List < Package_Product__c > ();
        List < Package_Product__c > packageproduct = new List < Package_Product__c > ();

        for (Package_ID__c pkgs: scope) {
            //Invoke pkg prdts creation class
            packageproduct = CreateUDACPackageProducts_2.createPkgPrdt(pkgs.id);

            if (packageproduct.size() > 0) {
                for (Package_Product__c prdts: packageproduct) {
                    lstpcksprs.add(prdts);
                }
            }
        }

        if (lstpcksprs.size() > 0) {

            insert lstpcksprs;

        }

    }

    global void finish(Database.BatchableContext BC) {

        //Call next batch class to invoke properties creation for each product
        String query = 'Select id, Name from Package_ID__c where Name!=null';
        CreateUDACProperties obj = new CreateUDACProperties(query);
        database.executebatch(obj, 10);

    }

}