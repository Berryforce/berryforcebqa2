global class DataLoadBatchClass2 implements Database.Batchable<sObject>{
    
    global DataLoadBatchClass2(){
    }
    
     global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'Select id, Follows_Listing_Record2__c From Directory_Listing__c where Follows_Listing_Record2__c != \'True\'';
        return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext bc, List<Directory_Listing__c> lstListing) {
        Savepoint sp = Database.setSavepoint();
        //try{
            if(lstListing!=null && lstListing.size()>0){
                
                //AppearanceCountSHM.IncreaseDecreaseAppearancecount(lstListing,null);
                List<Directory_Listing__c> lstListingUpdt = new List<Directory_Listing__c>();
                for(Directory_Listing__c objListing :lstListing){
                    objListing.Follows_Listing_Record2__c = 'True';
                    lstListingUpdt.add(objListing);
                }
                if(lstListingUpdt.size() > 0) {
                    update lstListingUpdt;
                }
            }
        /*}catch(Exception e){
            System.debug('The exception is'+e);
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch update of Listing name');
        } */
     } 
      
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Listing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception records for any other errors that might have occured while processing.');
    }
}