global class testBatch2 implements Database.Batchable<sObject>{
    global testBatch2(){
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        //String SOQL = 'select DP_Edition_Year__c, Directory_Edition__r.Year__c from Directory_Pagination__c where DP_Edition_Year__c = null';            
        
        //String SOQL = 'SELECT id FROM Listing__c where createdbyid=\'005G0000005hhf4\'';
        
        String SOQL = 'select DFF_Directory__c,DFF_Directory_Section__c , OrderLineItemID__r.directory__c, OrderLineItemID__r.Directory_Section__c  from Digital_Product_Requirement__c where DFF_Directory__c = null or DFF_Directory_Section__c = null';
        
        //String SOQL = 'SELECT id FROM Directory_Pagination__c where createddate > 2015-01-05T00:00:00Z and createdbyid=\'005K0000002MSSp\'';
        
        return Database.getQueryLocator(SOQL);
    }
    global void execute(Database.BatchableContext bc, List<Digital_Product_Requirement__c> scope){
    
        List<Digital_Product_Requirement__c> lstUpdtDFF = new List<Digital_Product_Requirement__c>();
        for(Digital_Product_Requirement__c  dffIterator : scope) {
            Digital_Product_Requirement__c objDFFNew = new Digital_Product_Requirement__c(id = dffIterator.id);
            if(dffIterator.DFF_Directory__c == null) {
                if(dffIterator.OrderLineItemID__c != null) {
                    objDFFNew.DFF_Directory__c = dffIterator.OrderLineItemID__r.Directory__c;
                }
                else if(dffIterator.ModificationOrderLineItem__c != null) {
                   // objDFFNew.DFF_Directory__c = dffIterator.ModificationOrderLineItem__r.Directory__c;
                }
            }
            if(dffIterator.DFF_Directory_Section__c == null) {
                if(dffIterator.OrderLineItemID__c != null) {
                    objDFFNew.DFF_Directory_Section__c = dffIterator.OrderLineItemID__r.Directory_Section__c;
                }
                else if(dffIterator.ModificationOrderLineItem__c != null) {
                 //   objDFFNew.DFF_Directory_Section__c = dffIterator.ModificationOrderLineItem__r.Directory_Section__c;
                }
            }
            lstUpdtDFF.add(objDFFNew);
        }
        
        update lstUpdtDFF;
    
        //delete scope;
    
        /*List<Directory_Pagination__c> lstDPNew = new List<Directory_Pagination__c>();
        for(Directory_Pagination__c iterator : lstDP) {
            iterator.DP_Edition_Year__c = iterator.Directory_Edition__r.Year__c;
            lstDPNew.add(iterator);
        }
        
        update lstDPNew;*/
        
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Batch Process Status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures.');
    }

}