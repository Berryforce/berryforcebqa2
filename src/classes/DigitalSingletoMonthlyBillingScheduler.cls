global class DigitalSingletoMonthlyBillingScheduler implements Schedulable {
   global void execute(SchedulableContext sc) {
      DigitalSingletoMonthlyBilling  b = new DigitalSingletoMonthlyBilling(date.today()); 
      database.executebatch(b);
   }
}