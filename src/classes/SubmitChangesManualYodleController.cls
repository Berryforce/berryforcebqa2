/******************************************************
Apex Class to submit changes from Manual and Yodle Dffs
Created By: Reddy(redd.yellanki@theberrycompany.com)
Updated Date: 09/21/2015
$Id$
******************************************************/
public with sharing class SubmitChangesManualYodleController {

    //Getters and setters
    public String dffId {
        get;
        set;
    }
    public String prflId {
        get;
        set;
    }
    public String prflNm {
        get;
        set;
    }

    public static String trackInfo;
    public Digital_Product_Requirement__c dff;
    public Order_Line_Items__c ol;
    public Modification_Order_Line_Item__c mOL;
    public static List < Track_DFF_Update__c > tDffUpdts = new List < Track_DFF_Update__c > ();

    String gnralRcdTyp = Label.ManualRecordTypes;
    String yodleRcdTyp = Label.YodleRecordTypes;

    public static List < Messaging.SingleEmailMessage > lstEmails = new List < Messaging.SingleEmailMessage > ();

    //Constructor
    public SubmitChangesManualYodleController(ApexPages.StandardController controller) {

        dffId = controller.getId();
        Id prflId = userInfo.getProfileId();
        Profile prfl = [SELECT Name from Profile where id = : prflId];
        prflNm = prfl.Name;

    }

    public void sbmtChngs() {

        dff = [SELECT Id, Name, OrderLineItemID__c, ModificationOrderLineItem__c, Submit_To_Yodle__c, Submit_Yodle_Cancel__c, Submit_Yodle_Update__c, RecordType.DeveloperName, CreatedById, Contact__c, contact_first_name__c, contact_last_name__c, Customer_Cancel_Date__c, Effective_Date__c,
            OwnerId, Fulfillment_Submit_status__c, RecordTypeId, FSD_Status__c, Final_Status__c, OrderLineItemID__r.Id, OrderLineItemID__r.Name, OrderLineItemID__r.Status__c, OrderLineItemID__r.Talus_Go_Live_Date__c, OrderLineItemID__r.Talus_Cancel_Date__c,
            ModificationOrderLineItem__r.Id, ModificationOrderLineItem__r.Name, ModificationOrderLineItem__r.Digital_Product_Requirement__c, ModificationOrderLineItem__r.Status__c, ModificationOrderLineItem__r.Talus_Go_Live_Date__c, List_3_Services_to_Use_in_Online_Ads_1__c, 
            Enterprise_Customer_ID__c, UDAC__c, business_name__c, business_url__c, business_email__c, business_phone_number_office__c, business_address1__c, business_city__c, business_state__c, business_postal_code__c, contact_phone_number_office__c, Contact_email__c, business_about__c,
            Destination_Phone_SEM__c, Destination_Phone_Website__c, Geographic_Service_Area__c, selling_propositions__c, services__c, Services_to_Use_in_Online_Ads_2_c__c, Services_to_Use_in_Online_Ads_3_c__c, desired_service_term__c, product_type__c,
            MF__c, MT__c, TF__c, TT__c, WF__c, WT__c, ThF__c, ThT__c, FF__c, FT__c, SF__c, ST__c, SnF__c, SnT__c, setup_notes__c, certifications__c, Discounts_Specials_Free_Services__c, Payment_Options__c, Color_Scheme__c, keywords__c, Template_ID__c, Tracking_Phone_SEM__c, Omit_Address_Description__c,
            URL_Transfer_Info_if_Not_Berry_Owned__c, Adversite_URL_Mirror_URL__c, accepted_payments__c, Video_to_Embed_or_Link_To__c, Budget_Contracted__c, Heading_1__c, One_Thing_That_Sets_Company_Apart__c, business_service_area__c, Suppress_Address_on_Website_c__c, (SELECT Id, Name, Type__c, Field_Value__c, Data_Fulfillment_Form__c From ChangedDFFFields__r)
            from Digital_Product_Requirement__c where id = : dffId
        ];


        //System.debug('************dff************' + dff);

        if (dff.OrderLineItemID__c != null) {

            if (gnralRcdTyp.contains(dff.RecordType.DeveloperName)) {

                manualDFFChngs(dff, dff.ChangedDFFFields__r);

            } else if(yodleRcdTyp.contains(dff.RecordType.DeveloperName)){

                yodleDFFChngs(dff, dff.ChangedDFFFields__r);

            }

        } else {

            if (gnralRcdTyp.contains(dff.RecordType.DeveloperName)) {

                manualMDFFChngs(dff, dff.ChangedDFFFields__r);

            } else if(yodleRcdTyp.contains(dff.RecordType.DeveloperName)){

                yodleMDFFChngs(dff, dff.ChangedDFFFields__r);

            }

        }
    }

    public static void manualDFFChngs(Digital_Product_Requirement__c dff, List < ChangedDFFField__c > cDffs) {

        //Queue Ids from custom settings
        Map < String, DFF_Manual_Queue_Ids__c > dffMnlQId = DFF_Manual_Queue_Ids__c.getAll();
        List < DFF_Manual_Queue_Ids__c > lstVals = dffMnlQId.Values();
        Map < String, String > mpVals = new Map < String, String > ();
        for (DFF_Manual_Queue_Ids__c iterator: lstVals) {
            mpVals.put(iterator.Name, iterator.queue_Id__c);
        }

        if (dff.OrderLineItemID__r.Status__c != 'Cancelled') {

            if (dff.Fulfillment_Submit_Status__c == 'Complete') {

                if (cDffs.size() > 0) {

                    for (ChangedDFFField__c iterator: cDffs) {


                        if (String.isBlank(trackInfo)) {
                            if (iterator.Name == 'UDAC') {
                                trackInfo = iterator.Name + ': ' + iterator.Field_Value__c;
                                trackInfo += '\n' + 'Effective Date: ' + iterator.Field_Value__c;
                            }
                            trackInfo = iterator.Name + ': ' + iterator.Field_Value__c;
                        } else {
                            if (iterator.Name == 'UDAC') {
                                trackInfo += '\n' + iterator.Name + ': ' + iterator.Field_Value__c;
                                trackInfo += '\n' + 'Effective Date: ' + iterator.Field_Value__c;
                            }
                            trackInfo += '\n' + iterator.Name + ': ' + iterator.Field_Value__c;
                        }

                    }

                    tDffUpdts.add(new Track_DFF_Update__c(Data_Fulfillment_Form__c = dff.Id, Updated_Info__c = trackInfo, Status__c = 'In Progress', Sent_Date__c = System.Today()));

                    if (dff.OwnerId == dff.CreatedById) {

                        //Assigning owner to the queue
                        if (mpVals.containsKey(dff.RecordType.DeveloperName)) {
                            dff.OwnerId = mpVals.get(dff.RecordType.DeveloperName);
                        }
                        update dff;

                    }

                    if (tDffUpdts.size() > 0) {
                        if (!Test.isRunningTest()) {
                            insert tDffUpdts;
                        }
                    }

                    CommonUtility.msgConfirm(CommonUtility.cngsSSbmtd);

                    delete cDffs;

                } else {

                    CommonUtility.msgInfo(CommonUtility.dffAlrdyAsgnQueue);

                }

            } else {

                CommonUtility.msgFatal(CommonUtility.cngsCntSbmt);

            }

        } else if (dff.OrderLineItemID__r.Status__c == 'Cancelled') {

            CommonUtility.msgFatal(CommonUtility.updtCntSnd);

        }

    }

    public static void manualMDFFChngs(Digital_Product_Requirement__c dff, List < ChangedDFFField__c > cDffs) {

        //Queue Ids from custom settings
        Map < String, DFF_Manual_Queue_Ids__c > dffMnlQId = DFF_Manual_Queue_Ids__c.getAll();
        List < DFF_Manual_Queue_Ids__c > lstVals = dffMnlQId.Values();
        Map < String, String > mpVals = new Map < String, String > ();
        for (DFF_Manual_Queue_Ids__c iterator: lstVals) {
            mpVals.put(iterator.Name, iterator.queue_Id__c);
        }

        if (dff.ModificationOrderLineItem__r.Status__c != 'Cancelled') {

            if (dff.Fulfillment_Submit_Status__c == 'Complete') {

                if (cDffs.size() > 0) {

                    for (ChangedDFFField__c iterator: cDffs) {


                        if (String.isBlank(trackInfo)) {
                            if (iterator.Name == 'UDAC') {
                                trackInfo = iterator.Name + ': ' + iterator.Field_Value__c;
                                trackInfo += '\n' + 'Effective Date: ' + iterator.Field_Value__c;
                            }
                            trackInfo = iterator.Name + ': ' + iterator.Field_Value__c;
                        } else {
                            if (iterator.Name == 'UDAC') {
                                trackInfo += '\n' + iterator.Name + ': ' + iterator.Field_Value__c;
                                trackInfo += '\n' + 'Effective Date: ' + iterator.Field_Value__c;
                            }
                            trackInfo += '\n' + iterator.Name + ': ' + iterator.Field_Value__c;
                        }

                    }

                    tDffUpdts.add(new Track_DFF_Update__c(Data_Fulfillment_Form__c = dff.Id, Updated_Info__c = trackInfo, Status__c = 'In Progress', Sent_Date__c = System.Today()));

                    if (dff.OwnerId == dff.CreatedById) {

                        //Assigning owner to the queue
                        if (mpVals.containsKey(dff.RecordType.DeveloperName)) {
                            dff.OwnerId = mpVals.get(dff.RecordType.DeveloperName);
                        }

                        update dff;

                    }

                    if (tDffUpdts.size() > 0) {
                        if (!Test.isRunningTest()) {
                            insert tDffUpdts;
                        }
                    }

                    CommonUtility.msgConfirm(CommonUtility.cngsSSbmtd);

                    delete cDffs;

                } else {

                    CommonUtility.msgInfo(CommonUtility.dffAlrdyAsgnQueue);

                }

            } else {

                CommonUtility.msgFatal(CommonUtility.cngsCntSbmt);

            }

        } else if (dff.ModificationOrderLineItem__r.Status__c == 'Cancelled') {

            CommonUtility.msgFatal(CommonUtility.updtCntSnd);

        }

    }

    public void yodleDFFChngs(Digital_Product_Requirement__c dff, List < ChangedDFFField__c > cDffs) {
        String mailBody;
        if (dff.Fulfillment_Submit_Status__c == 'Complete') {

            if (dff.Submit_Yodle_Cancel__c == false && dff.OrderLineItemID__r.Status__c != 'Cancelled') {

                if (cDffs.size() > 0) {

                    for (ChangedDFFField__c iterator: cDffs) {


                        if (String.isBlank(trackInfo)) {
                            if (iterator.Name == 'UDAC') {
                                trackInfo = iterator.Name + ': ' + iterator.Field_Value__c;
                                trackInfo += '\n' + 'Effective Date: ' + iterator.Field_Value__c;
                            }
                            trackInfo = iterator.Name + ': ' + iterator.Field_Value__c;
                        } else {
                            if (iterator.Name == 'UDAC') {
                                trackInfo += '\n' + iterator.Name + ': ' + iterator.Field_Value__c;
                                trackInfo += '\n' + 'Effective Date: ' + iterator.Field_Value__c;
                            }
                            trackInfo += '\n' + iterator.Name + ': ' + iterator.Field_Value__c;
                        }

                    }

                    tDffUpdts.add(new Track_DFF_Update__c(Data_Fulfillment_Form__c = dff.Id, Updated_Info__c = trackInfo, Status__c = 'In Progress', Sent_Date__c = System.Today()));

                    if (dff.OwnerId == dff.CreatedById) {
                        dff.OwnerId = Label.Yodle_Queue_Id;
                        dff.Submit_Yodle_Update__c = true;
                        update dff;
                    }

                    if (dff.OrderLineItemID__r.Status__c == 'New') {
                        ol = dff.OrderLineItemID__r;
                        ol.Status__c = 'In Progress';
                        update ol;
                    }

                    //Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    List < String > yodleEmails = Label.Yodle_Email_Ids.split(',');
                    List < String > yodleEmailsExternal = Label.Yodle_Email_Ids_External.split(',');
                    //mail.setToAddresses(yodleEmails);
                    //mail.setToAddresses(yodleEmails);
                    //mail.setSubject('Yodle Fulfillment Update Information for' + dff.Name);
                    if (dff.recordType.DeveloperName == 'Yodle_SEM') {
                        mailBody = SubmitToYodleaAndGeneral.semBody(dff);
                    } else {
                        mailBody = SubmitToYodleaAndGeneral.wbsiteBody(dff);
                    }
                    //mail.setHtmlBody(mailBody);
                    for(String trgtObjId: yodleEmails){
                        lstEmails.add(CommonUtility.emailUsingTargetObjectId(trgtObjId, 'Yodle Fulfillment Update Information for' + dff.Name, mailBody));
                    }
                    lstEmails.add(CommonUtility.emailUsingEmailId(yodleEmailsExternal, 'Yodle Fulfillment Update Information for' + dff.Name, mailBody));

                    if (!Test.isRunningTest()) {
                        //Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                            //mail
                        //});
                        
                        Messaging.sendEmail(lstEmails);

                        if (tDffUpdts.size() > 0) {
                            insert tDffUpdts;
                        }
                    }
                    delete cDffs;
                    CommonUtility.msgConfirm(CommonUtility.ydlDffSbmtd);

                } else {

                    CommonUtility.msgInfo(CommonUtility.ydlNoChngsToSbmt);

                }

            } else if (dff.Submit_Yodle_Cancel__c == true && dff.OrderLineItemID__r.Status__c == 'Cancelled') {

                CommonUtility.msgFatal(CommonUtility.ydlPrdtCncld);

            }
        } else {

            CommonUtility.msgFatal(CommonUtility.ydlIntlFlmnt);

        }

    }

    public void yodleMDFFChngs(Digital_Product_Requirement__c dff, List < ChangedDFFField__c > cDffs) {
        String mailBody;
        if (dff.Fulfillment_Submit_Status__c == 'Complete') {

            if (dff.Submit_Yodle_Cancel__c == false && dff.ModificationOrderLineItem__r.Status__c != 'Cancelled') {

                if (cDffs.size() > 0) {

                    for (ChangedDFFField__c iterator: cDffs) {


                        if (String.isBlank(trackInfo)) {
                            if (iterator.Name == 'UDAC') {
                                trackInfo = iterator.Name + ': ' + iterator.Field_Value__c;
                                trackInfo += '\n' + 'Effective Date: ' + iterator.Field_Value__c;
                            }
                            trackInfo = iterator.Name + ': ' + iterator.Field_Value__c;
                        } else {
                            if (iterator.Name == 'UDAC') {
                                trackInfo += '\n' + iterator.Name + ': ' + iterator.Field_Value__c;
                                trackInfo += '\n' + 'Effective Date: ' + iterator.Field_Value__c;
                            }
                            trackInfo += '\n' + iterator.Name + ': ' + iterator.Field_Value__c;
                        }

                    }

                    tDffUpdts.add(new Track_DFF_Update__c(Data_Fulfillment_Form__c = dff.Id, Updated_Info__c = trackInfo, Status__c = 'In Progress', Sent_Date__c = System.Today()));

                    if (dff.OwnerId == dff.CreatedById) {
                        dff.Ownerid = Label.Yodle_Queue_Id;
                        dff.Submit_Yodle_Update__c = true;
                        update dff;
                    }
                    if (dff.ModificationOrderLineItem__r.Status__c == 'New') {
                        mOL = dff.ModificationOrderLineItem__r;
                        mOL.Status__c = 'In Progress';
                        update mOL;
                    }

                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    List < String > yodleEmails = Label.Yodle_Email_Ids.split(',');
                    mail.setToAddresses(yodleEmails);
                    mail.setToAddresses(yodleEmails);
                    mail.setSubject('Yodle Fulfillment Update Information for' + dff.Name);
                    if (dff.recordType.DeveloperName == 'Yodle_SEM') {
                        mailBody = SubmitToYodleaAndGeneral.semBody(dff);
                    } else {
                        mailBody = SubmitToYodleaAndGeneral.wbsiteBody(dff);
                    }
                    mail.setHtmlBody(mailBody);
                    if (!Test.isRunningTest()) {
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                            mail
                        });

                        if (tDffUpdts.size() > 0) {
                            insert tDffUpdts;
                        }
                    }
                    delete cDffs;
                    CommonUtility.msgConfirm(CommonUtility.ydlDffSbmtd);

                } else {

                    CommonUtility.msgInfo(CommonUtility.ydlNoChngsToSbmt);

                }

            } else if (dff.Submit_Yodle_Cancel__c == true && dff.ModificationOrderLineItem__r.Status__c == 'Cancelled') {

                CommonUtility.msgFatal(CommonUtility.ydlPrdtCncld);

            }
        } else {

            CommonUtility.msgFatal(CommonUtility.ydlIntlFlmnt);

        }

    }
}