/*
* Class Name: futureCreateErrorLog
* Class Description: Create records in Exceptions/Errors object.
* 
*/

global class futureCreateErrorLog {
     @future
     public static void createErrorRecord(final string strExpMessage, final string strStackTraceString, final string strRealtedTo){
         Exception_Error__c objErrorRecord = new Exception_Error__c();
         objErrorRecord.Exception_Message__c = strExpMessage;
         objErrorRecord.StackTraceString__c = strStackTraceString;
         objErrorRecord.Related_to__c = strRealtedTo;
         Database.insert(objErrorRecord,false);
     }
     
     @future
     public static void MediaTraxStatus(final string message, final Integer intStatusCode, final String strDoc, final string strRealtedTo) {
        Exception_Error__c objErrorRecord = new Exception_Error__c();
        objErrorRecord.Status_Code__c = string.valueof(intStatusCode);
        objErrorRecord.StackTraceString__c = strDoc;
        objErrorRecord.Related_to__c = strRealtedTo;
        Database.insert(objErrorRecord,false);
    }
    
     public static void createErrorRecordBatch(final string strExpMessage, final string strStackTraceString, final string strRealtedTo){
         Exception_Error__c objErrorRecord = new Exception_Error__c();
         objErrorRecord.Exception_Message__c = strExpMessage;
         objErrorRecord.StackTraceString__c = strStackTraceString;
         objErrorRecord.Related_to__c = strRealtedTo;
         Database.insert(objErrorRecord,false);
     }
     
     public static void createErrorRecordBatch(final string strExpMessage, final string strStackTraceString, final string strRealtedTo, final string strRecordId){
         Exception_Error__c objErrorRecord = new Exception_Error__c();
         objErrorRecord.Exception_Message__c = strExpMessage;
         objErrorRecord.StackTraceString__c = strStackTraceString;
         objErrorRecord.Record_Id__c = strRecordId;
         objErrorRecord.Related_to__c = strRealtedTo;
         Database.insert(objErrorRecord,false);
     }
     
     public static void MediaTraxStatusBatch(final string message, final Integer intStatusCode, final String strDoc, final string strRealtedTo) {
        Exception_Error__c objErrorRecord = new Exception_Error__c();
        objErrorRecord.Status_Code__c = string.valueof(intStatusCode);
        objErrorRecord.StackTraceString__c = strDoc;
        objErrorRecord.Related_to__c = strRealtedTo;
        Database.insert(objErrorRecord,false);
    }
     // Test Methods
  @IsTest(SeeAllData=true)
  static void futureCreateErrorLogTest() {
    Test.startTest();
    futureCreateErrorLog FEL = new futureCreateErrorLog();
    futureCreateErrorLog.MediaTraxStatusBatch('test', 1000007, 'testdoc', 'nothing');
    Test.stopTest();
  }   
}