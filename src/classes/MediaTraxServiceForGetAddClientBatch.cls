global class MediaTraxServiceForGetAddClientBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global String SOQL = '';
    global String strAccRTId;
    global Database.QueryLocator start(Database.BatchableContext bc){
        strAccRTId = CommonMethods.getRedordTypeIdByName('Customer Account', 'Account');
        SOQL = 'SELECT Name, Account_Number__c, Client_Number__c, BillingCity, BillingCountry, Phone, BillingState, BillingStreet, BillingPostalCode, P4P_Spending_Cap__c, P4P_Spending_Cap_Type__c, Fax, Media_Trax_Client_ID__c FROM Account where Media_Trax_Client_ID__c = null and Account_Number__c != null and Name != null and RecordTypeId = \''+strAccRTId+'\' and BillingCity != null  and BillingCountry != null and Phone != null and BillingState != null and BillingStreet != null and BillingPostalCode != null and P4P_Spending_Cap__c != null and P4P_Spending_Cap_Type__c != null';
        System.debug('Testingg SOQL MediaTraxServiceForGetAddClientBatch  '+SOQL);
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> lstAccount){
        
        MediaTraxHeadingCalloutController_V1 objMediaTraxService = new MediaTraxHeadingCalloutController_V1();
        objMediaTraxService.ProcessMediaTraxForGetAddClientScope(lstAccount);
    }
    
    global void finish(Database.BatchableContext bc){
    }
}