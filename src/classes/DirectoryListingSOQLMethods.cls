public with sharing class DirectoryListingSOQLMethods {
    public static list<Directory_Mapping__c> getDirectoryListingByCanvassID(set<Id> setCanvassId) {
        return [SELECT Id, Name, Canvass__c, Directory__c,
                Telco__c, Default_Directory_Telco__c, Default_Canvass_Telco__c, 
                Telco__r.Account__c, Telco__r.Telco_Account_Name__c FROM Directory_Mapping__c
                WHERE Default_Canvass_Telco__c = true
                AND 
                Canvass__c IN :setCanvassId];//
    }
    
    public static list<Directory_listing__c> getMatchedDirectoryListings(set<string> setMatchedDL,set<Id> setDirId){
        return [select Id,Listing__c,Directory__c,SL_strScopedlistingMatched__c,StrBOCDisconnect__c,ABD_Pending_Review__c,DataFeedType__c,
                Disconnected_Via_BOC_Purge__c,Bus_Res_Gov_Indicator__c,Disconnected__c,Created_by_Batch__c,Phone_Number__c from directory_listing__c 
                where SL_strScopedlistingMatched__c IN : setMatchedDL AND Directory__c IN :setDirId];
    }
    
    public static list<Directory_listing__c> getYPCMscopedlistings(set<Id> setSLId){
        return [select Id,Under_caption__c,Under_caption__r.Service_order_stage__c from directory_listing__c 
                where Id  IN : setSLId];
    }
    
    public static list<Directory_listing__c> getYPCHscopedlistings(set<Id> setSLCHSOId){
        return [select Id,Under_caption__c,Service_order_stage__c from directory_listing__c 
                where Service_order_stage__c  IN : setSLCHSOId AND Section_page_type__c ='YP'];
    }  
    
    public static list<Directory_listing__c> fetchExistingDL(Set<string> setExistDL){
        return [Select id, Phone_Number__c,Directory_Section__c,Directory__c,SL_StrPhoneDLDS__c,strDLcombo__c,Directory_Section__r.Section_Page_Type__c from 
                Directory_Listing__c where Directory_Section__r.Section_Page_Type__c = 'WP' And SL_StrPhoneDLDS__c IN:setExistDL];       
    }
    
    public static list<Directory_listing__c> getDirListingByIds(Set<Id> dirListingIds){
        return [SELECT Id, Product__c, Original_Product__c, Caption_Header_Enhanced_By__c, Enhanced_By_OLI_Art_URN__c, Enhanced_By_OLI_Con__c
                 FROM Directory_Listing__c WHERE Id IN: dirListingIds];       
    }
   
    public static list<Directory_listing__c> getDlsbyUndercaption(Set<Id> dirListingIds){
        return [SELECT Id,Disconnected__c,Under_caption__c FROM Directory_Listing__c WHERE Under_caption__c IN: dirListingIds];       
    }
    
    public static Map<Id,Directory_listing__c> getDirListingMapByIds(Set<Id> dirLisTIds){
        return new Map<Id,Directory_listing__c>([select id,Directory_Section__c,Directory_Section__r.Section_Page_Type__c,Directory_Section__r.Section_Code__c,
                                                Product__c,Product__r.Print_Specialty_Product_Type__c,Product__r.ProductCode,Directory_Heading__r.Name,
                                                Directory_Heading__r.code__c,Caption_Member__c,Product__r.Print_Product_Type__c,Directory_Section__r.Telco_Drives_WP_Sorting__c,
                                                Under_Caption__r.Normalized_Last_Name_Business_Name__c,Under_Caption__r.Telco_Sort_Order__c,Under_Caption__r.Sort_As__c,Under_Caption__r.L_Street_Number__c,Under_Caption__r.L_Street__c,
                                                Under_Caption__r.L_City__c,Under_Caption__r.Listing__c,Under_Caption__r.Listing__r.Phone_Unformatted__c,Under_Caption__c,Listing__r.Phone_Unformatted__c,Listing__c from Directory_Listing__c where id IN :dirLisTIds]);
    }
    
    public static list<Directory_listing__c> getYPSL(set<string> setPhone,set<Id> setDirId){
        return [Select Id,Name,SL_Last_Name_Business_Name__c,First_Name__c,Listing__c,Listing_Postal_Code__c,Listing_State__c,Listing_City__c,Listing_PO_Box__c,Listing_Street__c,
                Listing_Street_Number__c,Caption_Header__c,Caption_member__c,Bus_Res_Gov_Indicator__c,Phone_Number__c,Under_caption__c,Section_page_type__c,
                ABD_Pending_Review__c,Directory__c,Disconnected_Via_BOC_Purge__c,Disconnected__c from Directory_listing__c where Phone_Number__c IN : setPhone 
                and Directory__c IN :setDirId and Section_Page_Type__c ='YP' ];
    }
    
    public static list<Directory_listing__c> getScopedListingsByAreaExchDirDirSec(set<string> setAreaCodes, set<string> setExchangeCodes, set<Id> setDirIds,
                                                                                    set<Id> setDirSecIds, Set<String> setBusGovIndcs) {
        return [SELECT Id, ABD_Pending_Review__c,Address_Override__c, Area_Code_WF__c, Bus_Res_Gov_Indicator__c, 
                Listing_City__c, Listing_Country__c, Caption_Display_Text__c, Caption_Header__c, Caption_Header_Enhanced_By__c, Caption_Member__c, 
                Listing_PO_Box__c, Listing_Postal_Code__c, Listing_State__c, Listing_Street__c, Listing_Street_Number__c, CASESAFEID__c, Name,SL_Last_Name_Business_Name__c, 
                CLEC_Provider__c, CORE_Caption_Migration_Group_ID__c, CORE_Migration_ID__c, Created_by_Batch__c, Cross_Reference_Text__c, 
                DataFeedType__c, Data_Feed_Type_2__c, Designation__c, Directory__c, Directory_Heading__c, Directory_Listing_External_ID__c, 
                Directory_Section__c, Disconnected__c, Disconnected_Parent_Listing__c, Disconnected_Via_BOC_Purge__c, Disconnect_Reason__c, 
                DL_DM_isTriggerExecuted__c, Do_Not_Create_YP__c, Effective_Date__c, Enhanced_By_OLI_Art_URN__c, Enhanced_By_OLI_Con__c, Exchange__c,
                Exchange_WF__c, First_Name__c, Follows_Listing_Record2__c, Free_WP_Directory_Listing__c, Area_Code__c,
                Honorary_Title__c, Include_Monitoring_Symbol__c, Indent_Level__c, Indent_Order__c, IsAddress__c, IsName__c, IsPhoneNumber__c, 
                Left_Telephone_Phrase__c, Lineage_Title__c, Line_WF__c, Listing__c, Listing_Locality__c, Bus_Main_or_Additional__c, 
                Multi_Scoped_Appearance__c, Normalized_Designation__c, Normalized_First_Name__c, Normalized_Honorary_Title__c, SL_Is_Multi_Scoped__c,
                Normalized_Last_Name_Business_Name__c, Normalized_Lineage_Title__c, Normalized_Listing_City__c, Normalized_Listing_PO_Box__c, 
                Normalized_Listing_Postal_Code__c, Normalized_Listing_Street_Name__c, Normalized_Listing_Street_Number__c, Normalized_Phone__c, 
                Normalized_Secondary_Surname__c, NOSO_CAIN_Sort_As__c, Omit_Address_OAD__c, Omit_Phone_OTN__c, Order_Line_Item_Caption_Header__c, 
                Original_Product__c, Phone_Number__c, Phone_Override__c, Phone_Type__c, Product__c, Right_Telephone_Phrase__c, Salutation__c, 
                Secondary_Surname__c, Section_Heading_Mapping_Not_Found__c, Section_Page_Type_Lookup__c, Service_Order__c, Service_Order_Stage__c, 
                SL_StrPhoneDLDS__c, SL_strScopedlistingMatched__c, Suppressed__c, Suppressed_By_OLI__c, Telco_Provider__c, Telco_Sort_Order__c, 
                Under_Caption__c, Under_Sub_Caption__c, Updated_From__c, Workflow_Fire_Field__c, WP_Logo_Product__c, Caption_Type__c, 
                SL_Original_Scoped_Listing_Id__c, Manual_Sort_As_Override__c, RecordTypeId
                FROM Directory_listing__c WHERE Directory_Section__c IN : setDirSecIds AND Directory__c IN :setDirIds AND Area_Code__c IN: setAreaCodes AND 
                Exchange__c IN: setExchangeCodes AND Bus_Res_Gov_Indicator__c IN: setBusGovIndcs AND Disconnected__c = false];
    }
    public static list<Directory_listing__c> getScopedListingsByListingIds(set<Id> setLstId){
        return [Select Id, Telco_Provider__c, Service_Order__c, Phone_Type__c, Phone_Override__c,Phone_Number__c, Omit_Address_OAD__c, Normalized_Phone__c,
                Normalized_Listing_Street_Number__c, Normalized_Listing_Street_Name__c,Normalized_Last_Name_Business_Name__c, Normalized_Designation__c, Name,SL_Last_Name_Business_Name__c, 
                Manual_Sort_As_Override__c, Lineage_Title__c, Left_Telephone_Phrase__c,ABD_Pending_Review__c,Honorary_Title__c, First_Name__c, Effective_Date__c, Disconnected__c,Disconnected_Via_BOC_Purge__c, 
                Disconnect_Reason__c, Cross_Reference_Text__c, CLEC_Provider__c,Bus_Res_Gov_Indicator__c,Follows_Listing_Record2__c, Bus_Main_or_Additional__c,
                Listing__c From Directory_Listing__c Where Listing__c IN :setLstId AND Follows_Listing_Record2__c='True'];                                                                                  
    }
    public static list<Directory_listing__c> getCMSlsByListingIdsforNormalization(set<Id> setLstId){
        return [Select Id,Follows_Listing_Record2__c,Normalized_Phone__c,Normalized_Listing_Street_Number__c, Normalized_Listing_Street_Name__c,Normalized_Last_Name_Business_Name__c, Normalized_Designation__c,Normalized_Honorary_Title__c,Normalized_Lineage_Title__c,Normalized_Secondary_Surname__c,Name,
                Listing__c From Directory_Listing__c Where Listing__c IN :setLstId AND Follows_Listing_Record2__c='True' AND RecordtypeId =:Label.TestScopedListingCaptionMemberRT];                                                                                  
    }
}