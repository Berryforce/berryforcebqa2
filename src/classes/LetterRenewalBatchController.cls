global class LetterRenewalBatchController implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext bc) {        
        Date TodayDate = Date.today();
        string BookStatus = 'NI';
        String soql = 'Select Id,Book_Status__c,Letter_Renewal_Stage_1__c,Letter_Renewal_Stage_2__c,Directory__c from Directory_Edition__c where Book_Status__c = : BookStatus '+' AND (Letter_Renewal_Stage_1__c=:'+'TodayDate '+' OR Letter_Renewal_Stage_2__c =:'+'TodayDate)';
        return Database.getQueryLocator(soql);
    }
   
    global void execute(Database.BatchableContext bc, List<Directory_edition__c> DirEditionLst) {
        List<User> objUserLst = [SELECT Id,Name FROM User where Name = 'Letter Renewal' Limit 1];
        String UserId = objUserLst[0].Id;
        set<Id> objDirId = new set<Id>();
        for(Directory_Edition__c objDE : DirEditionLst){
            objDirId.add(objDE.Directory__c);
        }
        System.debug('*****Directory Id****'+objDirId);
        set<Id> dirEditionId = new set<Id>();
        
        list<directory_edition__c> direditionBOTSLst = new list<directory_edition__c>();
        if(objDirId.size() >0){
            direditionBOTSLst = [Select Id,Book_Status__c,Directory__c from directory_edition__c where Directory__c IN :objDirId AND Book_Status__c ='BOTS']; 
        }
        if(direditionBOTSLst.size()>0){
            for(directory_edition__c objDirE : direditionBOTSLst ){
                dirEditionId.add(objDirE.Id);
            }
        }
        list<Order_Line_Items__c> oliLst = new list<Order_Line_Items__c>();
        oliLst = [Select Id,Seniority_Date__c ,Name,Account__r.BillingStreet,Account__r.BillingCity,Account__r.BillingState,Account__r.BillingPostalCode,Cutomer_Cancel_Date__c,Account__r.Delinquency_Indicator__c,Account__r.Open_Claim__c,Account__r.Phone,Product_Type__c,Product2__r.Print_Product_Type__c,Billing_Contact__c,Billing_Contact__r.Name,UnitPrice__c,Order_Line_Total__c,Effective_Date__c,Is_P4P__c,Last_Billing_Date__c,Billing_Contact__r.Email,Directory_Edition__c,Discount__c,
                  Directory_Edition__r.Name,Listing__r.Phone__c,Listing__r.Name,Directory__r.Name,Directory_Edition__r.Letter_Renewal_Stage_1__c,Directory_Edition__r.Letter_Renewal_Stage_2__c,Account__c,Account__r.Name,Account__r.Letter_Renewal_Sequence__c,Product2__r.Name,Order_Anniversary_Start_Date__c,Is_Handled__c,Opportunity__c,Media_Type__c from Order_Line_Items__c where Is_Handled__c=false and Account__r.Letter_Renewal_Sequence__c != null and Account__r.OwnerId =:UserId and Directory_edition__c IN :dirEditionId];
        System.debug('*****OLI lIST****'+oliLst);
        if(oliLst.size() >0){    
            LetterRenewalController_v1.letterRenewal(oliLst);
        }     
    }
   
    global void finish(Database.BatchableContext bc)
    {
    }
}