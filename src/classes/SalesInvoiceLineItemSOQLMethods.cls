public with sharing class SalesInvoiceLineItemSOQLMethods {
    
  public static list<c2g__codaInvoiceLineItem__c> getInvoiceLineItemByOLI(set<Id> setOLI) {
        return [Select c2g__UnitPrice__c,Billing_Frequency__c,Successful_Payments__c,Payments_Remaining__c , c2g__Quantity__c, 
        c2g__Product__c, c2g__NetValue__c, c2g__LineNumber__c, c2g__Invoice__r.Customer_Name__c, c2g__LineDescription__c, 
        c2g__Invoice__r.c2g__NetTotal__c,c2g__Invoice__r.Service_End_Date__c,c2g__Invoice__r.Service_Start_Date__c,c2g__Invoice__r.Telco_Reversal__c,c2g__Invoice__r.c2g__Transaction__c, c2g__Invoice__r.c2g__PaymentStatus__c, c2g__Invoice__r.c2g__Opportunity__c,   
        c2g__Invoice__r.c2g__NumberOfPayments__c, c2g__Invoice__r.c2g__InvoiceTotal__c, c2g__Invoice__r.c2g__InvoiceStatus__c, c2g__Invoice__r.c2g__Period__c,
        c2g__Invoice__r.c2g__InvoiceCurrency__c, c2g__Invoice__r.c2g__InvoiceDate__c,c2g__Invoice__r.c2g__DueDate__c, 
        c2g__Invoice__r.c2g__Account__c,c2g__Invoice__r.c2g__Account__r.name, c2g__Invoice__r.Name, c2g__Invoice__c, 
        c2g__Invoice__r.c2g__Dimension1__c,c2g__Invoice__r.c2g__Dimension2__c,c2g__Invoice__r.c2g__Dimension3__c,c2g__Invoice__r.SI_Payments_Remaining__c,c2g__Invoice__r.SI_Successful_Payments__c, Order_Line_Item__r.CMR_Name__c,
        c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c, Order_Line_Item__r.Product2__r.RGU__c, Order_Line_Item__r.Directory_Code__c, 
        Order_Line_Item__r.Edition_Code__c, Order_Line_Item__r.Canvass__r.Canvass_Code__c,
        Order_Line_Item__c, Name, Id, Order_Line_Item__r.UnitPrice__c, Order_Line_Item__r.Talus_Go_Live_Date__c, Order_Line_Item__r.ProductCode__c, 
        Directory__c, Directory_Edition__c, c2g__Invoice__r.Total_Month_Invoice__c, Order_Line_Item__r.Billing_Partner__c, Order_Line_Item__r.Is_P4P__c, Order_Line_Item__r.P4P_Billing__c,
        Order_Line_Item__r.Payments_Remaining__c,c2g__Invoice__r.CreatedDate,c2g__Invoice__r.c2g__InvoiceDescription__c, Order_Line_Item__r.Canvass__c, Order_Line_Item__r.Name, 
        Order_Line_Item__r.Successful_Payments__c,Order_Line_Item__r.Directory__c,Order_Line_Item__r.Directory_Edition__c, 
        Total_Credit_Amount__c,c2g__TaxCode1__c,c2g__TaxCode2__c,c2g__TaxCode3__c,c2g__TaxValue1__c,c2g__TaxValue2__c,c2g__TaxValue3__c
        From c2g__codaInvoiceLineItem__c where Order_Line_Item__c IN :setOLI AND OffSet__c=false AND Is_Manually_Created__c = false AND
        (c2g__Invoice__r.c2g__InvoiceStatus__c = 'In Progress' OR c2g__Invoice__r.c2g__InvoiceStatus__c = 'Complete') AND c2g__Invoice__r.Telco_Reversal__c=false order by c2g__Invoice__r.c2g__InvoiceDate__c ASC] ;   
        //From c2g__codaInvoiceLineItem__c where Order_Line_Item__c IN :setOLI and c2g__Invoice__r.c2g__PaymentStatus__c = 'Unpaid' and c2g__Invoice__r.c2g__InvoiceStatus__c = 'Reversed' order by Order_Line_Item__c];
    }
    
   
}