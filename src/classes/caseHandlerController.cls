Public class caseHandlerController {
    public static void onBeforeInsert(list<Case> lstCase) {
        RecordType  objRTNC = CommonMethods.getRecordTypeDetailsByName('Case','National Claim');
        Id rtId = objRTNC.Id;
        //createTaskwithDFFOwner(lstCase);
        populatePANinCase(lstCase,rtId);
        updateCaseFields(lstCase);                
    }
    
    public static void onAfterInsert(list<Case> lstCase) {
    	Map<String, JIRAConnector__c> mapJIRAConnector = JIRAConnector__c.getAll();
        RecordType  objRTClaim = CommonMethods.getRecordTypeDetailsByName('Case','CS Claim');
        Id Case_RId = objRTClaim.Id;
        updateOpenorClosedClaiminAccount(lstCase);
        UpdateAccountOpenClaim(lstCase, Case_RId);     
         
		for(Case c : lstCase) {
			if(mapJIRAConnector.containsKey(c.RecordTypeId)) {
				JIRAConnectorWebserviceCallout.createIssue(String.valueOf(c.Id), String.valueOf(c.RecordTypeId));
			}
		} 
    }
    
    public static void onAfterUpdate(list<Case> lstCase, Map<Id, Case> oldMap) {
    	Map<String, JIRAConnector__c> mapJIRAConnector = JIRAConnector__c.getAll();
        RecordType  objRTClaim = CommonMethods.getRecordTypeDetailsByName('Case','CS Claim');
        Id Case_RId = objRTClaim.Id;
        updateOpenorClosedClaiminAccount(lstCase);
        UpdateAccountOpenClaim(lstCase, Case_RId);
        updateOrderLineItemsOnZeroCancelFeeApproved(lstCase,oldMap);
        populateCustomerCancelDateOnOrderLineItem(lstCase,oldMap);
        
		String userEmailid = UserInfo.getUserEmail();
        for(Case c : lstCase) {
            if(mapJIRAConnector.containsKey(c.RecordTypeId)) {
                if(userEmailid <> mapJIRAConnector.get(c.RecordTypeId).Skipping_Email_Address__c) {
                    JIRAConnectorWebserviceCallout.synchronizeWithJIRAIssue(String.valueOf(c.Id), String.valueOf(c.RecordTypeId));
                }
            }
        }
    }
    
    /*public static void createTaskwithDFFOwner(list<case> caseLst) {
        String dffId;
        String Subj;
        List<String> lst;
        List<Task> nwTsk = new List<Task>();
        List<Digital_Product_Requirement__c> dffLst = new List<Digital_Product_Requirement__c>();
        
        dffLst = [Select Id, Name, Talus_DFF_Id__c, CreatedById, Owner.Id from Digital_Product_Requirement__c  where Approved_DFF__c = True LIMIT 250];
        
        for(Case c1: caseLst) {
            if(c1.Subject != null) {
                if(dffLst.size()>0) {
                    for(Digital_Product_Requirement__c dff1: dffLst) {
                        
                        String Body= c1.Description;
                        system.debug('BODY'+Body);
                        
                        //parse the case subject to get the DossierID from the string
                        subj = c1.subject;
                        lst = subj.split('--');
                        
                        system.debug('LIST of STRING' + lst);
                        if(lst != null){
                            dffId = String.valueof(lst.get(lst.size()-1));
                        }
                        system.debug('DFF Id:' + dffId);
                        
                        system.debug('SUBSCRIPTIONID:'+dff1.Talus_DFF_Id__c);
                        
                        //check if the parsed Id value is same as the "Dossier_ID__c" field on DFF
                        if(dffId != null){
                            if(String.valueof(dffId) == dff1.Talus_DFF_Id__c) {
                                //assign the current case record to matching DFF record
                                c1.Digital_Product_Requirement__c=dff1.id;
                                //create a new task to the owner of DFF(Salesrep), to let him know the issues with DFF record
                                Task tsk = new Task();
                                tsk.Subject = 'Please Review the Digital Product Requirement:'+dff1.Name;
                                tsk.ownerId = dff1.CreatedById;
                                tsk.Status = 'Not Started';
                                tsk.Priority = 'Low';
                                tsk.ActivityDate = system.today()+7;
                                tsk.whatId = dff1.id;
                                
                                nwTsk.add(tsk);                   
                            }
                        }
                    }                    
                }       
            }
        }
        if(nwTsk.size() > 0) {
            try{
                system.debug('TASKCREATED:'+nwTsk);
                insert nwTsk;
            }catch(exception e){
                system.debug('TASK FAILED');
            }
        }       
        
    }*/
    
    public static void populatePANinCase(list<case> csLst,Id NcRtId) {
        Integer iCount = 0;
        String FiveZeros = '00000';
        String FourZeros = '0000';
        String ThreeZeros = '000';
        String TwoZeros = '00';
        String oneZeros = '0';
        String strToUpdate ='';
        
        list<Case> csNcLst = new list<Case>();
        if(NcRtId != null) {
            csNcLst  = [Select Id,PAN1__c from Case where RecordtypeId =:NcRtId AND PAN1__c!=null order by PAN1__c desc limit 1];
        }
        
        system.debug('*****Case PAN lIST****'+csNcLst);
        
        for(Case Cs : csNcLst) {
            if(Cs.PAN1__c != null) {    
                iCount = integer.valueof(Cs.PAN1__c) ;
                system.debug('Icount value-------' + iCount);
            }
        }
      
        for(Case csPan : csLst) {
        system.debug('******Enter here *******');
            if(csPan.RecordtypeId != null && csPan.RecordtypeId == NcRtId) {
                if(Test.isRunningTest()) {
                    iCount = integer.valueof(csPan.PAN1__c) + 1;
                    system.debug('Icount value 2-------' + iCount);
                }
                else {
                    iCount  = iCount  + 1;
                    system.debug('Icount value 3-------' + iCount);
                } 
                if(iCount  >0 && iCount  <=9)
                strToUpdate  = FiveZeros + iCount;
                else
                if(iCount  >=10 && iCount  <=99)
                strToUpdate  = FourZeros + iCount;
                else
                if(iCount  >=100 && iCount  <=999)
                strToUpdate  = ThreeZeros + iCount;
                else
                if(iCount  >=1000 && iCount  <=9999)
                strToUpdate  = TwoZeros + iCount;
                else
                if(iCount  >=10000 && iCount  <=99999)
                strToUpdate  = oneZeros + iCount;
                else
                if(iCount  >=100000 && iCount  <=999999)
                strToUpdate  = string.valueof(iCount);
                
                system.debug('Updated String-----' + strToUpdate);
                
                csPan.PAN1__c= strToUpdate;
                system.debug('PAN Number-----' + csPan.PAN1__c);
            }
         }
    }
    
    public static void UpdateAccountOpenClaim(List<Case> lstCase, Id Case_RID) {
        list<account> AccountList = new list<Account>();
        Set<ID>  AccountID_Set=new Set<ID>();
        Set<ID>  AccountID_ClosedSet=new Set<ID>();     
        
        for(Case objCase : lstCase) {
            system.debug('*****Case Record Type is *****'+objCase.RecordtypeId);
            if(objCase.RecordtypeId == Case_RId && objCase.AccountId != null && objCase.IsClosed==false) {
                AccountID_Set.add(objCase.AccountId);
            }
            if(objCase.RecordTypeId==Case_RId && objCase.AccountId != null && objCase.IsClosed==true) {
                AccountID_ClosedSet.add(objCase.AccountId);
            }
        }
            
        if(AccountID_Set.size()>0) {           
            System.debug('@@@@@@@@@@@@@@'+Limits.getQueries());
            List<Account> acctList = AccountSOQLMethods.getAccountsByAcctId(AccountID_Set);            
            for(Account acc : acctList) {
                if(acc.Open_Claim__c == false) {
                    acc.Open_Claim__c=true;
                }
                AccountList.add(acc);
            }
        }  
        
        if(AccountID_ClosedSet.size()>0) {
            List<Account> acctList = AccountSOQLMethods.getAccountsByAcctIdAndCSClaimRT(AccountID_ClosedSet, Case_RId);
            for(Account acc :acctList){
                if(acc.Cases.size()>0){
                      acc.Open_Claim__c=true;
                      AccountList .add(acc);
                } else {
                     acc.Open_Claim__c=false;
                     AccountList .add(acc);
                }
            }                    
        }
        
        if(AccountList .size()>0) {            
            update AccountList ; 
        }
    }
    
    public static void updateOpenorClosedClaiminAccount(list<case> caseclaimLst) {
        Datetime pastDate;
        set<Id> actId = new set<Id>();
        List<Account> actCaseLst = new List<Account>();
        
        pastDate = system.today().addMonths(-18);

        for(Case cs : caseclaimLst) {
            if(cs.AccountId != null && (cs.CreatedDate != null || cs.ClosedDate != null) ) {
                if(cs.CreatedDate > pastDate || cs.ClosedDate > pastDate){
                actId.add(cs.AccountId);
                }
            }
        }       
        
        if(actId.size()>0) {
            for(Account act : [Select Id,Name,Open_or_Closed_Claim_Past_18_Months__c from Account where Id IN : actId]) {
                if(act.Open_or_Closed_Claim_Past_18_Months__c == false){
                    act.Open_or_Closed_Claim_Past_18_Months__c = true;
                    actCaseLst.add(act);
                }                
            }
        }
        
        if(actCaseLst.size()>0) {
            update actCaseLst;
        }
    }

    public static void updateCaseFields(List<Case> lstCs){

        for(Case iterator: lstCs){ 
            //logic to set Due Date to created date plus 8, excluding sat and sun           
            if(iterator.Digital_Product_Requirement__c != null){
                DateTime myDate = system.Now();
                DateTime myDateTime = (DateTime)myDate;
                String dayOfWeek = myDateTime.format('E');
                if(dayofWeek == 'Sat'){
                    iterator.Due_Date__c = system.Today() + 11;
                } 
                if(dayofWeek == 'Sun'){
                    iterator.Due_Date__c = system.Today() + 10;
                } 
                if(dayofWeek == 'Mon') {
                    iterator.Due_Date__c = system.Today() + 10;
                }
                if(dayofWeek == 'Tue') {
                    iterator.Due_Date__c = system.Today() + 10;
                }
                if(dayofWeek == 'Wed') {
                    iterator.Due_Date__c = system.Today() + 12;
                }
                if(dayofWeek == 'Thu') {
                    iterator.Due_Date__c = system.Today() + 12;
                }
                if(dayofWeek == 'Fri') {
                    iterator.Due_Date__c = system.Today() + 12;
                }                                                                
            }    
        } 

    }
  
 public static void populateCustomerCancelDateOnOrderLineItem(list<Case> lstCase, Map<Id, Case> oldMap)
    {
         List<order_Line_Items__c> orderLineItemForUpdate=new List<order_Line_Items__c>();
         order_Line_Items__c objOLI = new order_Line_Items__c();
         Set<Id> caseIds=new Set<Id>();
         List<Messaging.SingleEmailMessage> emailList=new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> lstmail = new List<Messaging.SingleEmailMessage>();
        EmailTemplate objEmailTemplate = new EmailTemplate();
        Map<id,List<order_line_items__c>> mapOfOrderSet=new Map<id,List<order_line_items__c>>();
        objEmailTemplate = [select id,name,DeveloperName from EmailTemplate where DeveloperName = 'Cancellation_Opty_Contract_Confirmation'];         
         Map<String, Date> mpP4PCancelDate = new Map<String, Date>();
       for(Case iterator:[select id,contact.id,contact.email,Recordtype.name,IsClosed,Zero_Cancel_Fee_Requested__c,Zero_Cancel_Fee_Approved__c,Zero_Cancel_Fee_Request_Rejected__c,(select id,order_group__c,P4P_Tracking_Number_Status__c,P4P_Tracking_Number_ID__c,P4P_Billing__c,Is_P4P__c,Selected_Cancel_Date__c,Cutomer_Cancel_Date__c,zero_Cancel_Fee_Requested__c,Zero_Cancel_Fee_Approved__c from Order_Line_Items__r ) from Case where Id IN :lstCase])
        {
                        System.debug('######### '+iterator.isClosed);
                        System.debug('######### '+!oldMap.get(iterator.id).isClosed);
                            System.debug('#########iterator.Recordtype.name '+iterator.Recordtype.name);
            if(iterator.isClosed  && !oldMap.get(iterator.id).isClosed &&  iterator.Recordtype.name=='Cancellation Closed')
            {
                caseIds.add(iterator.id);
                for(order_Line_Items__c childIterator:iterator.Order_Line_Items__r)
                {
                    if(childIterator.Selected_Cancel_Date__c!=null) 
                    {                   
                        objOLI = new order_Line_Items__c(Id=childIterator.Id,Cutomer_Cancel_Date__c=childIterator.Selected_Cancel_Date__c,Selected_Cancel_Date__c=null);
                        orderLineItemForUpdate.add(objOLI);
                        
                        //added by Mythili - populating map to send oli list to Media Trax system
                        if(childIterator.P4P_Billing__c || childIterator.Is_P4P__c){
                            mpP4PCancelDate.put(childIterator.P4P_Tracking_Number_ID__c, childIterator.Selected_Cancel_Date__c);                            
                            System.debug('Testingg Adding to map - track no mpP4PCancelDate ');
                        }  
                         if(!mapOfOrderSet.containsKey(childIterator.order_group__c))
                            mapOfOrderSet.put(childIterator.order_group__c,new List<Order_Line_Items__c>{childIterator});
                        else
                            mapOfOrderSet.get(childIterator.order_group__c).add(childIterator);
                   }                    
                }
                System.debug('###########mapOfOrderSet '+mapOfOrderSet);
                if(iterator.contact.email!=null && iterator.contact.email!='')
                {
                   for(Id tempId:mapOfOrderSet.keySet())
                    {
                            order_line_items__c tempOLI=mapOfOrderSet.get(tempId)[0];               
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setTemplateId(objEmailTemplate.Id);
                            mail.setTargetObjectId(iterator.contact.id);
                            mail.setWhatId(iterator.id);
                            mail.setSaveAsActivity(true); 
                            lstmail.add(mail);          
                    }
                }
           }
        }
        System.debug('###########caseIds '+caseIds.size() + ' @@@@ '+lstmail.size());
        List<Attachment> attlistForCancellationHtmlBody=new List<Attachment>();
        if(caseIds.size()>0) {
          CancellationLetterController obj=new CancellationLetterController();
          for(Id caseIdIterator:caseIds)
          {
              String mybody=obj.generateHtmlBody(caseIdIterator);
              Attachment a=new Attachment ();
              string filename='Cancellation Email Letter -'+ Date.today();
              a.parentId=caseIdIterator;
              a.Name = fileName + '.pdf';
              a.contenttype= 'application/pdf';
              a.body = Blob.toPdf(mybody); 
              attlistForCancellationHtmlBody.add(a);          
          }
        }
        System.debug('########### '+attlistForCancellationHtmlBody.size());
        /*if(attlistForCancellationHtmlBody.size()>0)
            insert attlistForCancellationHtmlBody;*/
        if(caseIds.size()>0 )
            InsertCancellationLetterToCase(caseIds,userInfo.getSessionId(),URL.getSalesforceBaseUrl().toExternalForm());
        System.debug('########### '+lstmail);
        
        if(lstmail.size()>0 && !Test.isRunningTest())
             Messaging.sendEmail(lstmail);
        if(orderLineItemForUpdate.size()>0)
            update orderLineItemForUpdate;
        
        //added by Mythili - calling Media Trax system to deactivate P4P_Tracking_Number for oliList
        if(mpP4PCancelDate != null && mpP4PCancelDate.size() > 0 ){
            System.debug('Testingg Calling deactivation service');
            deactivateTrackingNo(mpP4PCancelDate);
         }
    }  
    @future(callout=true)
    public static void InsertCancellationLetterToCase(Set<Id> caseIds, string userSessionId,string host)
    {
        System.debug('############### '+userSessionId);
        System.debug('###############host '+host);
        //Replace below URL with your Salesforce instance host
        String addr = host.toLowercase()+'/services/apexrest/attachCancellationLetter';
        HttpRequest req = new HttpRequest();
        req.setEndpoint( addr );
        req.setMethod('POST');
        req.setHeader('Authorization', 'OAuth ' + userSessionId);

        //req.setHeader('Authorization','Bearer '+UserInfo.getSessionId());
        req.setHeader('Content-Type','application/json');
        Map<String,set<Id>> postBody = new Map<String,set<Id>>();
        postBody.put('caseIdList',CaseIds);
        String reqBody = JSON.serialize(postBody);
        req.setBody(reqBody);
        Http http = new Http();
        if(!Test.isRunningtest())
            HttpResponse response = http.send(req);
    }
    /* This method calls the Media Trax system to deactivate the P4P_Tracking_No for the cancelled olis */
    @future (callout = true)
    public static void deactivateTrackingNo(Map<String, Date> mpP4PCancelDate) {
      
            list<MediaTrax_API_Configuration__c> lstMediaConf = MediaTraxAPIConfigSOQLMethods.getMediaTraxOnAction(new set<String>{'DeactivateTrackingNumber'});
            List<String> lstTrackId = new List<String>();
            MediaTrax_API_Configuration__c objMedia;
            Map<Id,Order_Line_Items__c> mpOLIUpdt = new Map<Id,Order_Line_Items__c>();
            
            List<Order_Line_Items__c> oliListToUpdate=new List<Order_Line_Items__c>();
            System.debug('B4 Ading media enrty  to list and mpP4PCancelDate '+mpP4PCancelDate);
            if(lstMediaConf!= null && lstMediaConf.size() > 0){
                objMedia = lstMediaConf[0];
                System.debug('Ading media enrty  to list');
            }
            if(!Test.isRunningtest()){
                for(String strTrackNo : mpP4PCancelDate.keySet()){
                    DOM.Document objDOMResult = MediaTraxHTTPCalloutHandlerController_V1.TrackingInMediaTraxCallDeActivate(strTrackNo, mpP4PCancelDate.get(strTrackNo), objMedia);
                    System.debug('Testingg objDOMResult '+objDOMResult);
                    if(objDOMResult != null) {                  
                        map<String, dom.XmlNode> mapNodeResult = MediaTraxHTTPCalloutHandlerController_V1.parseDirectoryXML(objDOMResult, objMedia.SOAP_URL__c, objMedia.API_URL__c, objMedia.Return_Method__c, objMedia.Response_Method__c);
                        System.debug('Testingg mapNodeResult '+mapNodeResult);
                        if(mapNodeResult.size() > 0) {
                            if(mapNodeResult.get('result') != null) {
                                for(dom.XmlNode iterator1: mapNodeResult.get('result').getChildElements()) {
                                    if(iterator1.getName() == 'result' && iterator1.getText() == 'Deactivated') {
                                        //mpTrackNoCancelDate.put(strTrackNo,mpP4PCancelDate.get(strTrackNo));
                                        lstTrackId.add(strTrackNo);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if(lstTrackId != null && lstTrackId.size() > 0){
                List<Order_Line_Items__c> lstOLIs = [select id, P4P_Tracking_Number_Status__c, P4P_Tracking_Number_ID__c from Order_Line_Items__c where P4P_Tracking_Number_ID__c in :lstTrackId ];               
                if(lstOLIs!=null && lstOLIs.size()>0){
                    for(Order_Line_Items__c objOLITrackNoUpdt : lstOLIs){
                        objOLITrackNoUpdt.P4P_Tracking_Number_ID__c = null;
                        objOLITrackNoUpdt.P4P_Tracking_Number_Status__c = 'Deactivated';
                        oliListToUpdate.add(objOLITrackNoUpdt);
                          
                    }
                }
               
            }
            if(oliListToUpdate != null && oliListToUpdate.size() > 0){
                update oliListToUpdate;
            }
      
    }   
    
    public static void updateOrderLineItemsOnZeroCancelFeeApproved(list<Case> lstCase, Map<Id, Case> oldMap)
    {
        List<order_Line_Items__c> orderLineItemForUpdate=new List<order_Line_Items__c>();
        List<Messaging.SingleEmailMessage> lstmail = new List<Messaging.SingleEmailMessage>();
       order_Line_Items__c objOLI = new order_Line_Items__c();
        for(Case iterator:[select id,CaseNumber,owner.email,owner.name,Zero_Cancel_Fee_Requested__c,Zero_Cancel_Fee_Approved__c,Zero_Cancel_Fee_Request_Rejected__c,(select id,P4P_Tracking_Number_Status__c,Is_P4P__c,P4P_Billing__c,P4P_Tracking_Number_ID__c,Selected_Cancel_Date__c,Cutomer_Cancel_Date__c,zero_Cancel_Fee_Requested__c,Zero_Cancel_Fee_Approved__c from Order_Line_Items__r ) from Case where Id IN :lstCase])
        {
            if(iterator.Zero_Cancel_Fee_Approved__c && !oldMap.get(iterator.id).Zero_Cancel_Fee_Approved__c)
            {
                for(order_Line_Items__c childIterator:iterator.Order_Line_Items__r)
                {
                    if(childIterator.Selected_Cancel_Date__c!=null) {                   
                        objOLI = new order_Line_Items__c(Id=childIterator.Id,Cutomer_Cancel_Date__c=childIterator.Selected_Cancel_Date__c,Selected_Cancel_Date__c=null);
                       //orderLineItemForUpdate.add(objOLI);
                     }
                }
                }
            else if(iterator.Zero_Cancel_Fee_Request_Rejected__c && !oldMap.get(iterator.id).Zero_Cancel_Fee_Request_Rejected__c)
            {
                for(order_Line_Items__c childIterator:iterator.Order_Line_Items__r)
                {
                    if(childIterator.Selected_Cancel_Date__c!=null)
                    orderLineItemForUpdate.add(new order_Line_Items__c(Id=childIterator.Id,Selected_Cancel_Date__c=null));
                }
                String[] toAddresses = new String[] {iterator.owner.email};
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(toAddresses);
                mail.setSubject('Case '+iterator.CaseNumber +' Rejected by Approver');
                mail.setHTMLBody('Hi '+iterator.owner.name+',<br> Your case '+ iterator.CaseNumber + ' has been rejected.<br>Thanks');
                mail.setSaveAsActivity(true); 
                lstmail.add(mail);  
                                             
            }
        }
        Messaging.sendEmail(lstmail);   
        if(orderLineItemForUpdate.size()>0)
        {
            update orderLineItemForUpdate;
        }
    }  
}