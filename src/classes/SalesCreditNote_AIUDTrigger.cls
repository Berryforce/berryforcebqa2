public class SalesCreditNote_AIUDTrigger {
	public static void creditNoteAmountCalculation(List<c2g__codaCreditNote__c> lstSalesCreditNotes) {
		Set<Id> accountIds = new Set<Id>();
		List<c2g__codaCreditNote__c> salesCreditNotesList = new List<c2g__codaCreditNote__c>();
		Map<Id, List<c2g__codaCreditNote__c>> accountSalesCreditNotesListMap = new Map<Id, List<c2g__codaCreditNote__c>>();	
		List<Account> accountList = new List<Account>();
    	List<Account> accountUpdateList = new List<Account>();	
		
		for(c2g__codaCreditNote__c salesCreditNote : lstSalesCreditNotes) {
    		accountIds.add(salesCreditNote.c2g__Account__c);
    	}
    	
    	salesCreditNotesList = [SELECT Id, c2g__Account__c, c2g__CreditNoteTotal__c FROM c2g__codaCreditNote__c WHERE c2g__Account__c IN : accountIds];
    	
    	for(c2g__codaCreditNote__c salesCreditNote : salesCreditNotesList) {
    		if(!accountSalesCreditNotesListMap.containsKey(salesCreditNote.c2g__Account__c)) {
    			accountSalesCreditNotesListMap.put(salesCreditNote.c2g__Account__c, new List<c2g__codaCreditNote__c>());
    		}
    		accountSalesCreditNotesListMap.get(salesCreditNote.c2g__Account__c).add(salesCreditNote);
    	}	
    	
    	accountList = [SELECT Id, Credit_Note_Amount__c FROM Account WHERE ID IN : accountIds];
    	
    	for(Account acct : accountList) {    	
    		if(accountSalesCreditNotesListMap.containsKey(acct.Id)) {
    			Double creditNoteAmount = 0;
    			List<c2g__codaCreditNote__c> tempSalesCreditNotesList = new List<c2g__codaCreditNote__c>();
    			tempSalesCreditNotesList = accountSalesCreditNotesListMap.get(acct.Id);
    			for(c2g__codaCreditNote__c salesCreditNote : tempSalesCreditNotesList) {
    				creditNoteAmount = creditNoteAmount + salesCreditNote.c2g__CreditNoteTotal__c;
    			}
    			acct.Credit_Note_Amount__c = creditNoteAmount;
    			accountUpdateList.add(acct);
    		}	
    	}
    	if(!accountUpdateList.isEmpty()) {
    		update accountUpdateList;
    	}				
	}
}