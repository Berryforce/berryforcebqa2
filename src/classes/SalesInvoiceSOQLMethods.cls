public class SalesInvoiceSOQLMethods {
    
    public static map<Id, c2g__codaInvoice__c> getMapSalesInvoice(set<Id> accountID) {
        return new map<Id, c2g__codaInvoice__c>([Select c2g__Account__c, Id, Name, c2g__Account__r.Name, c2g__Opportunity__c, CreatedDate, c2g__InvoiceDate__c, c2g__InvoiceTotal__c, c2g__Period__c, c2g__InvoiceCurrency__c, c2g__DueDate__c From c2g__codaInvoice__c where c2g__Account__c IN :accountID order by c2g__InvoiceDate__c desc]);
    }
    
    public static map<Id, c2g__codaInvoice__c> getMapSalesInvoice(set<Id> accountID, Date fromDate , Date toDate) {
        return new map<Id, c2g__codaInvoice__c>([Select c2g__Account__c, Id, Name, c2g__Account__r.Name, c2g__Opportunity__c, CreatedDate, 
                                        c2g__InvoiceDate__c, c2g__InvoiceTotal__c, c2g__Period__c, c2g__InvoiceCurrency__c, c2g__DueDate__c 
                                        From c2g__codaInvoice__c where c2g__Account__c IN :accountID and (c2g__InvoiceDate__c >= :fromDate and c2g__InvoiceDate__c <= :toDate) 
                                        order by c2g__InvoiceDate__c desc]);
    }
    //Adding a Method to fetch Invoice,InvoiceLineItems and Payments
    public static map<Id, c2g__codaInvoice__c> getMapSalesInvoiceLineItemsPaymentsbyInvoiceId(set<Id> InvoiceId){
        return new map<ID, c2g__codaInvoice__c>([Select c2g__TaxTotal__c, c2g__DueDate__c, c2g__Account__c, c.Name, Id, c2g__Opportunity__c,c2g__NetTotal__c,
                                      (Select Id, Name, pymt__Amount__c, pymt__Authorization_Id__c,pymt__Check_Number__c, pymt__Contact__r.Name, pymt__Date__c, pymt__Last_4_Digits__c, pymt__Payment_Type__c, pymt__Status__c,pymt__Card_Type__c From p2f__Payments__r), 
                                      (Select Id,Order_Line_Item__r.Account__c,c2g__Invoice__c, c2g__LineDescription__c, c2g__LineNumber__c, c2g__UnitPrice__c,c2g__Product__r.Name,c2g__codaInvoiceLineItem__c.c2g__NetValue__c 
                                       From c2g__InvoiceLineItems__r) 
                                      From c2g__codaInvoice__c c where Id IN: InvoiceId]);
    }
    
    public static List<c2g__codaInvoice__c> getMapSalesInvoiceLineItemsPaymentsbyInvoiceIds(set<Id> salesInvoiceIds){
        return new List<c2g__codaInvoice__c>([SELECT c2g__Account__r.Name, c2g__Account__r.BillingStreet, c2g__Account__r.BillingCity, c2g__Account__r.BillingState, 
                                              c2g__Account__r.BillingCountry, c2g__Account__r.BillingPostalCode, c2g__Account__r.AccountNumber, c2g__Account__r.Phone,
                                              Service_Start_Date__c, Service_End_Date__c, c2g__DueDate__c,
                                              (Select Id, Name, pymt__Amount__c, pymt__Authorization_Id__c,pymt__Check_Number__c, pymt__Contact__r.Name, pymt__Date__c, 
                                              pymt__Last_4_Digits__c, pymt__Payment_Type__c, pymt__Status__c,pymt__Card_Type__c From p2f__Payments__r), 
                                              (Select Id,Order_Line_Item__r.Account__c,c2g__Invoice__c, c2g__LineDescription__c, c2g__LineNumber__c, c2g__UnitPrice__c,c2g__Product__r.Name,
                                              Order_Line_Item__r.Package_ID__c, c2g__codaInvoiceLineItem__c.c2g__NetValue__c From c2g__InvoiceLineItems__r) 
                                              From c2g__codaInvoice__c c where Id IN: salesInvoiceIds]);
    } 
    
    // add method to get non monthly auto pay invoices for an account
    public static list <c2g__codaInvoice__c> getNonAutoSalesInvoicesByAccount(String AccId){
    list <c2g__codaInvoice__c> retVal =[SELECT Id,  IsDeleted, Name,  c2g__Account__c, c2g__CustomerReference__c, c2g__Dimension1__c, c2g__Dimension2__c, 
                    c2g__Dimension3__c, c2g__Dimension4__c, c2g__DiscardReason__c, c2g__DueDate__c, c2g__ExternalId__c, c2g__FirstDueDate__c, 
                    c2g__GeneralLedgerAccount__c, c2g__IncomeScheduleGroup__c, c2g__IncomeSchedule__c, c2g__Interval__c, c2g__InvoiceCurrency__c, 
                    c2g__InvoiceDate__c, c2g__InvoiceDescription__c, c2g__InvoiceGroup__c, c2g__InvoiceStatus__c, c2g__InvoiceTotal__c, 
                    c2g__NumberOfPayments__c, c2g__NumberofJournals__c, c2g__Opportunity__c, c2g__OutstandingValue__c, c2g__OwnerCompany__c, 
                    c2g__PaymentSchedule__c, c2g__PaymentStatus__c, c2g__PeriodInterval__c, c2g__Period__c, c2g__PrintStatus__c, 
                    c2g__PrintedText1AllowEdit__c, c2g__PrintedText1Heading__c, c2g__PrintedText1TextDefinitionName__c, c2g__PrintedText1Text__c, 
                    c2g__PrintedText2AllowEdit__c, c2g__PrintedText2Heading__c, c2g__PrintedText2TextDefinitionName__c, c2g__PrintedText2Text__c, 
                    c2g__PrintedText3AllowEdit__c, c2g__PrintedText3Heading__c, c2g__PrintedText3TextDefinitionName__c, c2g__PrintedText3Text__c, 
                    c2g__PrintedText4AllowEdit__c, c2g__PrintedText4Heading__c, c2g__PrintedText4TextDefinitionName__c, c2g__PrintedText4Text__c, 
                    c2g__PrintedText5AllowEdit__c, c2g__PrintedText5Heading__c, c2g__PrintedText5TextDefinitionName__c, c2g__PrintedText5Text__c, 
                    c2g__ShippingMethod__c, c2g__StartDate__c, c2g__TaxCode1__c, c2g__TaxCode2__c, c2g__TaxCode3__c, c2g__TaxTotal__c, 
                    c2g__Transaction__c, c2g__UnitOfWork__c, c2g__Usepartperiods__c, c2g__Year__c, c2g__NetTotal__c, c2g__Tax1Total__c, 
                    c2g__Tax2Total__c, c2g__Tax3Total__c, ffbext__Approved__c, ffbilling__BillingAddress__c, ffbilling__CopyAccountValues__c, 
                    ffbilling__CopyDefaultPrintedTextDefinitions__c, ffbilling__CreatingRecurringInvoice__c, ffbilling__DeriveCurrency__c, 
                    ffbilling__DeriveDueDate__c, ffbilling__DerivePeriod__c, ffbilling__ShippingAddress__c, ffbilling__PaymentScheduleTotal__c, 
                    p2f__Account_Billing_Contact__c, Order_Set__c, Auto_Number__c, Customer_Name__c, Billing_Contact__c, ISS_Number__c, 
                    CMR_Number__c, Total_Gross_Amnt__c, Total_ADJ_Amnt__c, Total_Commission__c, Payment_Type__c, ISS_Number_Text__c, 
                    Berry_Transaction_Type__c, Service_Start_Date__c, Service_End_Date__c, Total_Month_Invoice__c from c2g__codaInvoice__c 
                    WHERE Payment_Type__c != 'Credit Card' 
                    AND Payment_Type__c != 'ECheck' 
                    AND  c2g__PaymentStatus__c !='Paid'
                    AND c2g__Account__c =: AccId ];
    return retVal;
    }
    
     // add method to get all unpaid invoices for an account
    public static list <c2g__codaInvoice__c> getAllUnpaidSalesInvoicesByAccount(String AccId){
    list <c2g__codaInvoice__c> retVal =[SELECT Id,  IsDeleted, Name,  c2g__Account__c, c2g__CustomerReference__c, c2g__Dimension1__c, c2g__Dimension2__c, 
                    c2g__Dimension3__c, c2g__Dimension4__c, c2g__DiscardReason__c, c2g__DueDate__c, c2g__ExternalId__c, c2g__FirstDueDate__c, 
                    c2g__GeneralLedgerAccount__c, c2g__IncomeScheduleGroup__c, c2g__IncomeSchedule__c, c2g__Interval__c, c2g__InvoiceCurrency__c, 
                    c2g__InvoiceDate__c, c2g__InvoiceDescription__c, c2g__InvoiceGroup__c, c2g__InvoiceStatus__c, c2g__InvoiceTotal__c, 
                    c2g__NumberOfPayments__c, c2g__NumberofJournals__c, c2g__Opportunity__c, c2g__OutstandingValue__c, c2g__OwnerCompany__c, 
                    c2g__PaymentSchedule__c, c2g__PaymentStatus__c, c2g__PeriodInterval__c, c2g__Period__c, c2g__PrintStatus__c, 
                    c2g__PrintedText1AllowEdit__c, c2g__PrintedText1Heading__c, c2g__PrintedText1TextDefinitionName__c, c2g__PrintedText1Text__c, 
                    c2g__PrintedText2AllowEdit__c, c2g__PrintedText2Heading__c, c2g__PrintedText2TextDefinitionName__c, c2g__PrintedText2Text__c, 
                    c2g__PrintedText3AllowEdit__c, c2g__PrintedText3Heading__c, c2g__PrintedText3TextDefinitionName__c, c2g__PrintedText3Text__c, 
                    c2g__PrintedText4AllowEdit__c, c2g__PrintedText4Heading__c, c2g__PrintedText4TextDefinitionName__c, c2g__PrintedText4Text__c, 
                    c2g__PrintedText5AllowEdit__c, c2g__PrintedText5Heading__c, c2g__PrintedText5TextDefinitionName__c, c2g__PrintedText5Text__c, 
                    c2g__ShippingMethod__c, c2g__StartDate__c, c2g__TaxCode1__c, c2g__TaxCode2__c, c2g__TaxCode3__c, c2g__TaxTotal__c, 
                    c2g__Transaction__c, c2g__UnitOfWork__c, c2g__Usepartperiods__c, c2g__Year__c, c2g__NetTotal__c, c2g__Tax1Total__c, 
                    c2g__Tax2Total__c, c2g__Tax3Total__c, ffbext__Approved__c, ffbilling__BillingAddress__c, ffbilling__CopyAccountValues__c, 
                    ffbilling__CopyDefaultPrintedTextDefinitions__c, ffbilling__CreatingRecurringInvoice__c, ffbilling__DeriveCurrency__c, 
                    ffbilling__DeriveDueDate__c, ffbilling__DerivePeriod__c, ffbilling__ShippingAddress__c, ffbilling__PaymentScheduleTotal__c, 
                    p2f__Account_Billing_Contact__c, Order_Set__c, Auto_Number__c, Customer_Name__c, Billing_Contact__c, ISS_Number__c, 
                    CMR_Number__c, Total_Gross_Amnt__c, Total_ADJ_Amnt__c, Total_Commission__c, Payment_Type__c, ISS_Number_Text__c, 
                    Berry_Transaction_Type__c, Service_Start_Date__c, Service_End_Date__c, Total_Month_Invoice__c from c2g__codaInvoice__c 
                    WHERE c2g__PaymentStatus__c !='Paid'
                    AND c2g__Account__c =: AccId ];
    return retVal;
    }
    
    
    public static list<c2g__codaInvoice__c> getSalesInvoiceBYOpportunityID(set<Id> opportunityIDs) {
        return [SELECT Id,c2g__Opportunity__c FROM c2g__codaInvoice__c WHERE c2g__Opportunity__c IN:opportunityIDs order by createddate desc];  
    }
    
    public static list<c2g__codaInvoice__c> getSalesInvoiceByInvoiceID(set<Id> InvoiceId) {
        return [Select c2g__InvoiceStatus__c, c2g__PaymentStatus__c,c2g__InvoiceDate__c, c2g__Opportunity__c, c2g__NetTotal__c, c2g__InvoiceTotal__c, c2g__Interval__c, c2g__Account__c, Order_Set__c,c2g__Period__c,c2g__DueDate__c ,c2g__InvoiceCurrency__c, c2g__PrintStatus__c,Name, Id, Customer_Name__c, c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c From c2g__codaInvoice__c where Id IN :InvoiceId];
    }
    
    public static list<c2g__codaInvoice__c> getSalesInvoiceByInvoice(list<c2g__codaInvoice__c> lstFFInvoice) {
        return [select Total_Commission__c, Total_Gross_Amnt__c, c2g__TaxTotal__c, c2g__NetTotal__c from c2g__codaInvoice__c where id in :lstFFInvoice];
    }
    
    public static list<c2g__codaInvoice__c> getSalesInvoiceByISSIds(set<Id> ISSIds) {
        return [SELECT c2g__TaxTotal__c, c2g__NetTotal__c, Total_Gross_Amnt__c, Total_Commission__c FROM c2g__codaInvoice__c 
        		WHERE ISS_Number__c IN :ISSIds];
    }
}