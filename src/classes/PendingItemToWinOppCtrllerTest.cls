@IsTest
public with sharing class PendingItemToWinOppCtrllerTest{
	public static testmethod void PendingItemToWinOpportunityControllerTest() {
		Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        
        Account objAccount = TestMethodsUtility.createAccount('cmr');
        
        Contact objContact = TestMethodsUtility.createContact(objAccount);
        insert objContact;
        
        Opportunity objOpportunity = TestMethodsUtility.createOpportunity(objAccount, objContact);
        insert objOpportunity;
        
        BigMachines__Quote__c objBMI = TestMethodsUtility.createBMIQuote(objAccount, objOpportunity);
	    objBMI.BigMachines__Is_Primary__c = true;
	    insert objBMI;
        Apexpages.Standardcontroller ctl = new Apexpages.Standardcontroller(objOpportunity);
        PendingItemToWinOpportunityController pendingItem = new PendingItemToWinOpportunityController(ctl);
        
        objOpportunity.Signing_Method__c = 'RAS';
        update objOpportunity;
        pendingItem = new PendingItemToWinOpportunityController(ctl);
        
        objOpportunity.Signing_Method__c = 'DocuSign';
        update objOpportunity;
        pendingItem = new PendingItemToWinOpportunityController(ctl);
	}
}