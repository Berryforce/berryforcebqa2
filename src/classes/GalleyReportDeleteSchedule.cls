global class GalleyReportDeleteSchedule implements Schedulable {
   public Interface GalleyReportDeleteScheduleInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('GalleyReportDeleteScheduleHndlr');
        if(targetType != null) {
            GalleyReportDeleteScheduleInterface obj = (GalleyReportDeleteScheduleInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}