public class ClientInvoiceValidationController {
    public String pubCode {get;set;}
    public String editionCode {get;set;}
    public Id dirId {get;set;}
    public String strDirName {get;set;}
    public List<SelectOption> dirList {get;set;}
    public Decimal OLITotal{get;set;}
    public Decimal nationalInvLITotal{get;set;} 
    public Integer intOSCount{get;set;}
    public Integer intNationalOSCount{get;set;}
    public List<Order_Line_Items__c> OLIListForInvCalculation = new List<Order_Line_Items__c>();
    map<Id, String> mapDirIdName = new map<Id, String>();
    map<String, Id> mapDirNameId = new map<String, Id>();
    public Id pagNumValReportId {get;set;}
    public Id taxNatClientReportId {get;set;}
    public Id printLOBReportId {get;set;}
    public Id clInvReportId {get;set;}
    map<Id, Id> mapOSWithNSOS = new map<Id, Id>();
    set<Id> setNSOS = new set<Id>();    
    public Boolean displayBool {get;set;}
    public list<NationalInvoice__c> listNatInv {get;set;}
    public boolean displayPopup {get; set;}
    public boolean natInvLinkBool {get; set;}
    public Integer ISSCount{get;set;}
    public Integer dirCount{get;set;}
    public Integer dirVerCount{get;set;}
    public Integer stateCount{get;set;}
    public Integer CMRCount{get;set;}    
    public Integer dirNameCount{get;set;}
    public Integer clientCount{get;set;}
    public Integer clientNameCount{get;set;}
    public Integer pubDateCount{get;set;}
    public Boolean validateOrdersBool {get;set;}
    public Boolean createInvoicesBool {get;set;}
    public Boolean validateTaxBool {get;set;}
    public Boolean validateCMRBool {get;set;}
    public Boolean createInvoiceBool {get;set;}
    public Boolean validateHeadersBool {get;set;}
    public Boolean pageNumberValidationBool {get;set;}
    public String selectedDirCode {get;set;}
    public String selectedDEId {get;set;}
    public String selectedDEName {get;set;}
    Map<Id, String> mapDirIdDirCode = new Map<Id, String>();
    National_Billing_Status__c natBilling = new National_Billing_Status__c();
    List<National_Billing_Status__c> listNatBill = new List<National_Billing_Status__c>();
    public list<NationalInvoice__c> listNatInvoices {get;set;}
    public Decimal totalInvTaxAmount{get;set;}
    public Decimal totalInvAmount{get;set;}
    public Decimal totalInvNetAmount{get;set;} 
    public Decimal totalInvGrossAmount{get;set;}
    public Id eBillingXMLReportId{get;set;}
    
    public ClientInvoiceValidationController() {
        validateOrdersBool = false;
        createInvoicesBool = false;
        createInvoiceBool = false;
        validateTaxBool = false;
        validateCMRBool = false;
        validateHeadersBool = false;
        pageNumberValidationBool = false;
        dirList = new List<SelectOption>();
        displayBool = false;
        displayPopup = false;
        natInvLinkBool = false;
        listNatInv = new list<NationalInvoice__c>();
        listNatInvoices = new list<NationalInvoice__c>();
        pagNumValReportId = National_Billing_Reports__c.getInstance('PageNumValReport').Report_Id__c;
        printLOBReportId = National_Billing_Reports__c.getInstance('PrintLOBReport').Report_Id__c;
        taxNatClientReportId = National_Billing_Reports__c.getInstance('TaxClientReport').Report_Id__c; 
        clInvReportId = National_Billing_Reports__c.getInstance('ClientInvReport').Report_Id__c;
        eBillingXMLReportId = National_Billing_Reports__c.getInstance('EBillingXMLReport').Report_Id__c;
    }
    
    public void fetchDirs() {
        displayBool = false;
        dirList = new List<SelectOption>();
        if(String.isNotBlank(pubCode) && String.isNotBlank(editionCode)) {
            list<Directory_Edition__c> dirEditionList = new list<Directory_Edition__c>(); 
            dirEditionList = DirectoryEditionSOQLMethods.getDirEdByDirPubCodeAndEdCode(pubCode, editionCode);     
            system.debug('Directory Edition list is ' + dirEditionList);             
            if(dirEditionList.size() > 0) {
                mapDirNameId = new map<String, Id>();
                mapDirIdName = new map<Id, String>();
                for(Directory_Edition__c DE : dirEditionList) {
                    mapDirNameId.put(DE.Directory__r.Name, DE.Directory__c);
                    mapDirIdName.put(DE.Directory__c, DE.Directory__r.Name);
                    mapDirIdDirCode.put(DE.Directory__c, DE.Directory__r.Directory_Code__c);
                }
                List<String> dirNames = new List<String>(mapDirNameId.keySet());
                dirNames.sort();
                for(String str : dirNames) {
                    dirList.add(new SelectOption(mapDirNameId.get(str), str));
                }                  
            }
        }
    } 
    
    public void calculateOrderTotal() {  
        validateOrdersBool = false;
        createInvoicesBool = false;
        createInvoiceBool = false;
        validateTaxBool = false;
        validateCMRBool = false;
        validateHeadersBool = false;
        pageNumberValidationBool = false;
        displayBool = false;
        displayPopup = false;
        natInvLinkBool = false;
        listNatInv = new list<NationalInvoice__c>();
        ISSCount = 0;
        dirCount = 0;
        dirVerCount = 0;
        stateCount = 0;
        CMRCount = 0;   
        dirNameCount = 0;
        clientCount = 0;
        clientNameCount = 0;
        pubDateCount = 0;
        selectedDirCode = mapDirIdDirCode.get(dirId);             
        fetchNationalBilling();
        if(listNatBill.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No record found in Status Table'));
            displayBool = false;           
        } else {       
            refresh();
            assignValuesFromNatBill(); 
            validateOLIAndNITotal();
        }         
    }           
    
    public void refresh() {
        intOSCount = 0;
        intNationalOSCount = 0;
        OLITotal = 0.00;
        nationalInvLITotal = 0.00;
        displayBool = true;                
        
        list<National_Staging_Order_Set__c> listNSOS = NationalStagingOrderSetSOQLMethods.getNSOSByDirIdPubCodeEditionCodeWithoutActionO(pubCode, editionCode, dirId);
        system.debug('Directory Id is ' + dirId);
        
        for(National_Staging_Order_Set__c NSOS : listNSOS) {
            /*List<Order_Group__c> listOrdSet = new List<Order_Group__c>();
            listOrdSet = NSOS.Order_Sets__r;
            if(listOrdSet != null && listOrdSet.size() > 0) {
                Order_Group__c orderSet = listOrdSet.get(0);
                if(!orderSet.InvoiceGenerationStatus__c) {
                    intNationalOSCount += 1;
                    nationalInvLITotal += calculateNSLITotal(NSOS.National_Staging_Line_Items__r);
                }
            } else {
                intNationalOSCount += 1;
                nationalInvLITotal += calculateNSLITotal(NSOS.National_Staging_Line_Items__r);
            }*/     
            intNationalOSCount += 1;
            nationalInvLITotal += calculateNSLITotal(NSOS.National_Staging_Line_Items__r);                 
        }
        
        OLIListForInvCalculation = OrderLineItemSOQLMethods.getOLIByNationalOrderSet_V3(pubCode, editionCode, dirId);
        system.debug('OLI list size is ' + OLIListForInvCalculation.size());
        Set<ID> setOS = new Set<ID>();
        Set<ID> setNationalOS = new Set<ID>();
        strDirName = mapDirIdName.get(dirId);
        if(OLIListForInvCalculation.Size() > 0){
            for(Order_Line_Items__c OLI : OLIListForInvCalculation) {
                if(!setOS.contains(OLI.Order_Group__c)) {
                    setOS.add(OLI.Order_Group__c);
                }
                
                if(OLI.UnitPrice__c != null) {
                    OLITotal += OLI.UnitPrice__c;                    
                }
            }
            if(setOS != null && setOS.size() > 0) {
                intOSCount = setOS.size();
            }            
        }
        populateHeadersCheck();
        validateOLIAndNITotal();
    }
    
    public void createNationalInv() {
        List<Order_Line_Items__c> OLIList = OrderLineItemSOQLMethods.getOLIByNationalOrderSetWithoutInv(pubCode, editionCode, dirId);
        if(OLIList.Size() > 0) {
            system.debug('OLI creation list size is ' + OLIList.size());
            generateNationalInvoice(OLIList);
            populateHeadersCheck();
            natInvLinkBool = true;
            createInvoicesBool = true;
            createInvoiceBool = true;
            updateStatusTable();
        }
    }
    
    public void generateNationalInvoice(list<Order_Line_Items__c>  lstOLI) {
        map<Id, list<NationalInvoiceLineItem__c>> mapNILI = new map<Id, list<NationalInvoiceLineItem__c>>();
        map<Id, list<NationalInvoiceLineItem__c>> mapNILIRef = new map<Id, list<NationalInvoiceLineItem__c>>();
        map<Id, list<Order_Line_Items__c>> mapOS = new map<Id, list<Order_Line_Items__c>>();
        set<Id> setOSIds = new set<Id>();
        set<id> setCMRId = new set<id>();
        set<id> setDirId = new set<id>();
        map<id,Decimal> mpOLIIdTax = new map<id,Decimal>();
        map<id,list<id>> mpCMROLI = new map<id,list<id>>();
        map<id,list<id>> mpDirOLI = new map<id,list<id>>();
        Map<Id, String> mapOLIIdCMRNumber = new Map<Id, String>();
        Map<Id, String> mapOLIIdPubCode = new Map<Id, String>();
        listNatInv = new list<NationalInvoice__c>();
         
        for(Order_Line_Items__c iterator : lstOLI) {
            mapOLIIdCMRNumber.put(iterator.Id, iterator.CMR_Number__c);
            mapOLIIdPubCode.put(iterator.Id, iterator.Publication_Code__c);
            
            if(!mapOS.containsKey(iterator.Order_Group__c)) {
                mapOS.put(iterator.Order_Group__c, new list<Order_Line_Items__c>());
            }
            
            mapOS.get(iterator.Order_Group__c).add(iterator);
            setCMRId.add(iterator.CMR_Name__c);
            if(!mpCMROLI.containsKey(iterator.CMR_Name__c)){
                mpCMROLI.put(iterator.CMR_Name__c, new list<id>());
            }
            mpCMROLI.get(iterator.CMR_Name__c).add(iterator.id);
            setDirId.add(iterator.Directory__c);
            if(!mpDirOLI.containsKey(iterator.Directory__c)) {
                mpDirOLI.put(iterator.Directory__c, new list<id>());
            }
            mpDirOLI.get(iterator.Directory__c).add(iterator.id);
            setNSOS.add(iterator.Order_Group__r.National_Staging_Order_Set__c);
            mapOSWithNSOS.put(iterator.Order_Group__r.National_Staging_Order_Set__c, iterator.Order_Group__c);
        }
        
        list<National_Tax__c> lstNationalTax = [SELECT Tax__c, CMR__c, Directory__c FROM National_Tax__c WHERE CMR__c in :setCMRId or Directory__c in :setDirId];
        for(National_Tax__c objNatTax : lstNationalTax) {
            if(mpCMROLI != null && mpCMROLI.size() > 0){
                if(objNatTax.CMR__c != null) {
                    if(mpCMROLI.containsKey(objNatTax.CMR__c)) {
                        for(id idOLI : mpCMROLI.get(objNatTax.CMR__c)) {
                            if(mpOLIIdTax.containsKey(idOLI)) {
                                if(objNatTax.Tax__c > mpOLIIdTax.get(idOLI)) {
                                    mpOLIIdTax.put(idOLI, objNatTax.Tax__c);
                                }
                            } else {
                                mpOLIIdTax.put(idOLI, objNatTax.Tax__c);
                            }
                        }
                    }
                }
            }
            
            if(mpDirOLI != null && mpDirOLI.size() > 0) {
                if(objNatTax.Directory__c != null) {
                    if(mpDirOLI.containsKey(objNatTax.Directory__c)) {
                        for(id idOLI : mpDirOLI.get(objNatTax.Directory__c)) {
                            if(mpOLIIdTax.containsKey(idOLI)) {
                                if(objNatTax.Tax__c > mpOLIIdTax.get(idOLI)) {
                                    mpOLIIdTax.put(idOLI, objNatTax.Tax__c);
                                }
                            } else {
                                mpOLIIdTax.put(idOLI, objNatTax.Tax__c);
                            }
                        }   
                    }
                }
            }
            
        }
        
        for(ID osID : mapOS.keySet()) {
            list<Order_Line_Items__c> lstOLIPerOS = mapOS.get(osID);
            if(lstOLIPerOS != null && lstOLIPerOS.size() > 0) {
                Order_Line_Items__c objOLI = lstOLIPerOS[0];                
                listNatInv.add(generateInvoice(objOLI, mpOLIIdTax, mapOLIIdCMRNumber, mapOLIIdPubCode));
                mapNILI.put(osID, generateInvoiceLineItem(lstOLIPerOS));
            }
        }
        
        list<National_Staging_Order_Set__c> lstNationalOS = NationalStagingOrderSetSOQLMethods.getNationalReferenceRecordByNationalOrderId(setNSOS);
        system.debug('NSOS are ' + lstNationalOS);
        for(National_Staging_Order_Set__c iterator : lstNationalOS) {
            Id osID = mapOSWithNSOS.get(iterator.Id);
            if(iterator.National_Staging_Line_Items__r != null) {
                list<National_Staging_Line_Item__c> lstNSLI = iterator.National_Staging_Line_Items__r;
                if(lstNSLI.size() > 0) {
                    mapNILIRef.put(osID, generateInvoiceLineItemForReference(lstNSLI));
                }
            }
        }
        
        if(listNatInv.size() > 0) {
            insert listNatInv;            
            list<NationalInvoiceLineItem__c> listNatInvLI = new list<NationalInvoiceLineItem__c>();
            list<Order_Group__c> updateOS = new list<Order_Group__c>();
            for(NationalInvoice__c iteratorNI : listNatInv) {
                updateOS.add(new Order_Group__c(Id = iteratorNI.OrderSet__c, InvoiceGenerationStatus__c = true));
                for(NationalInvoiceLineItem__c iteratorNILI : mapNILI.get(iteratorNI.OrderSet__c)) {
                    iteratorNILI.NationalInvoice__c = iteratorNI.Id;
                    listNatInvLI.add(iteratorNILI);
                }
                if(mapNILIRef.containsKey(iteratorNI.OrderSet__c)){
                    for(NationalInvoiceLineItem__c iteratorNILI : mapNILIRef.get(iteratorNI.OrderSet__c)) {
                        iteratorNILI.NationalInvoice__c = iteratorNI.Id;
                        listNatInvLI.add(iteratorNILI);
                    }
                }
            }
            if(listNatInvLI.size() > 0) {
                insert listNatInvLI;
            }
            
            /*if(updateNI.size() > 0) {
                update updateNI;
            }*/
            
            if(updateOS.size() > 0) {
                update updateOS;
            }
        }
    }
    
    private NationalInvoice__c generateInvoice(Order_Line_Items__c objOLI, map<id,Decimal> mpOLIIdTax, Map<Id, String> mapOLIIdCMRNumber, Map<Id, String> mapOLIIdPubCode) {
        
        NationalInvoice__c objNI = new NationalInvoice__c(Trans__c = 'IR', 
                                    Trans_Version__c = objOLI.Trans_Version__c, 
                                    To__c = mapOLIIdCMRNumber.get(objOLI.Id), 
                                    Publication_Date__c = objOLI.Publication_Date__c, 
                                    Publication_Company__c = objOLI.Publication_Company__c, 
                                    OrderSet__c = objOLI.Order_Group__c, 
                                    NAT__c = objOLI.NAT__c, 
                                    NAT_Client_ID__c  = objOLI.NAT_Client_ID__c,
                                    Invoice_Date__c = objOLI.Directory_Edition__r.National_Billing_Date__c,  
                                    From__c = mapOLIIdPubCode.get(objOLI.Id), 
                                    Directory__c = objOLI.Directory__c, 
                                    Source_Directory__c = objOLI.Directory__c,
                                    Directory_Edition__c = objOLI.Directory_Edition__c, 
                                    SourceDirectoryEdition__c = objOLI.Directory_Edition__c, 
                                    Client_Name__c = objOLI.Account__c, 
                                    CMR__c = objOLI.CMR_Name__c,
                                    Life__c = '1200',
                                    Tax__c = mpOLIIdTax.get(objOLI.id),
                                    IsNew__c = true ); 
        return objNI;
    }
    
    private static list<NationalInvoiceLineItem__c> generateInvoiceLineItem(list<Order_Line_Items__c> lstOLI) {
        list<NationalInvoiceLineItem__c> lstNILI = new list<NationalInvoiceLineItem__c>();
        for(Order_Line_Items__c objOLI : lstOLI) {
            NationalInvoiceLineItem__c objNILI = new NationalInvoiceLineItem__c(SPINS__c = objOLI.National_Staging_Line_Item__r.Spins__c , 
                                                    Product2__c = objOLI.Product2__c , 
                                                    OrderLineItem__c = objOLI.Id , 
                                                    NationalInvoice__c = null , 
                                                    Gross_Amount__c = objOLI.UnitPrice__c, 
                                                    BAS__c = objOLI.National_Staging_Line_Item__r.BAS__c , DAT__c = objOLI.DAT__c, 
                                                    Advertising_Data__c = objOLI.Advertising_Data__c , 
                                                    ADJ_Amount__c = 0,
                                                    Page_Number__c = objOLI.Page_Number__c,
                                                    Line_Number__c = objOLI.Line_Number__c);
            lstNILI.add(objNILI);
        }
        return lstNILI;
    }
    
    private static list<NationalInvoiceLineItem__c> generateInvoiceLineItemForReference(list<National_Staging_Line_Item__c> lstNSLI) {
        list<NationalInvoiceLineItem__c> lstNILI = new list<NationalInvoiceLineItem__c>();
        for(National_Staging_Line_Item__c objOLI : lstNSLI) {
            NationalInvoiceLineItem__c objNILI = new NationalInvoiceLineItem__c(SPINS__c = objOLI.SPINS__c , 
                                                    NationalInvoice__c = null , 
                                                    BAS__c = objOLI.BAS__c , DAT__c = objOLI.DAT__c, 
                                                    Advertising_Data__c = objOLI.Advertising_Data__c,
                                                    Line_Number__c = objOLI.Line_Number__c);
            lstNILI.add(objNILI);
        }
        return lstNILI;
    }
    
    public void showNatInv() {
        displayPopup = true;
        listNatInvoices = NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeDirId(pubCode, editionCode, dirId); 
        calcualteTotalForInv();
    }
    
    public void closePopup() {
        displayPopup = false;     
    }
    
    private Double calculateNSLITotal(List<National_Staging_Line_Item__c> listNSLI) {
        Double nationalInvLITotal = 0;
        if(listNSLI != null) {
            for(National_Staging_Line_Item__c NSLI : listNSLI) {
                nationalInvLITotal += NSLI.Sales_Rate__c;
            }
        }
        return nationalInvLITotal;
    }
    
    private void populateHeadersCheck() {
        listNatInv = NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeDirId(pubCode, editionCode, dirId); 
        if(listNatInv.size() > 0) {
            ISSCount = 0;
            dirCount = 0;
            dirVerCount = 0;
            stateCount = 0;
            CMRCount = 0;   
            dirNameCount = 0;
            clientCount = 0;
            clientNameCount = 0;
            pubDateCount = 0;
            for(NationalInvoice__c NI : listNatInv) {
                if(NI.ISS__c == null) {
                    ISSCount = ISSCount + 1;
                }           
                if(NI.Directory__c == null) {
                    dirCount = dirCount + 1;
                    dirNameCount = dirNameCount + 1;
                }
                if(NI.State__c == null) {
                    stateCount = stateCount + 1;
                }
                if(NI.Publication_Date__c == null) {
                    pubDateCount = pubDateCount + 1;
                }
                if(NI.CMR__c == null) {
                    CMRCount = CMRCount + 1;
                }
                if(NI.Client_Name__c == null) {
                    clientNameCount = clientNameCount + 1;
                    clientCount = clientCount + 1;
                }
                if(NI.Directory_Version__c == null) {
                    dirVerCount = dirVerCount + 1;
                }           
            }
        }
    }
    
    public void updateStatusTable() {
        natBilling.IV_VALIDATE_ORDER__c = validateOrdersBool;
        natBilling.IV_CREATE_INVOICE__c = createInvoicesBool; 
        natBilling.IV_VALIDATE_TAX__c = validateTaxBool;
        natBilling.IV_VALIDATE_CMR_BILLING_RESPONSIBILITY__c = validateCMRBool;
        natBilling.IV_VALIDATE_HEADER__c = validateHeadersBool;
        natBilling.IV_VALIDATE_PAGE_NUMBER__c = pageNumberValidationBool;
        natBilling.IV_Create_National_Client_Invoice__c = createInvoiceBool;
        natBilling.IV_Create_Client_Invoice_Link__c = natInvLinkBool;
        
        update natBilling;
    }
    
    public void fetchNationalBilling() {
        listNatBill = NationalBillingStatusSOQLMethods.getNatBillByPubEditionDirCode(pubCode, editionCode, selectedDirCode);
        if(listNatBill.size() > 0) {
            natBilling = listNatBill.get(0);
        }        
    }
    
    public void assignValuesFromNatBill() {
        validateOrdersBool = natBilling.IV_VALIDATE_ORDER__c;
        createInvoicesBool = natBilling.IV_CREATE_INVOICE__c;
        validateTaxBool = natBilling.IV_VALIDATE_TAX__c;
        validateCMRBool = natBilling.IV_VALIDATE_CMR_BILLING_RESPONSIBILITY__c;
        validateHeadersBool = natBilling.IV_VALIDATE_HEADER__c;
        pageNumberValidationBool = natBilling.IV_VALIDATE_PAGE_NUMBER__c;
        createInvoiceBool = natBilling.IV_Create_National_Client_Invoice__c;
        natInvLinkBool = natBilling.IV_Create_Client_Invoice_Link__c;
    }
    
    public void enableCreateInv() {
        if(validateOrdersBool) {
            createInvoiceBool = false;
        } else {
            createInvoiceBool = true;
        }
        updateStatusTable();
    }                    
    
    public void calcualteTotalForInv() {
        totalInvTaxAmount = 0;
        totalInvAmount = 0;
        totalInvNetAmount = 0;
        totalInvGrossAmount = 0;
        
        if(listNatInvoices.size() > 0) {
            for(NationalInvoice__c NI : listNatInvoices) {
                if(NI.Tax_Amount_New__c != null) {
                    totalInvTaxAmount += NI.Tax_Amount_New__c;
                }
                
                if(NI.Total_Amount__c != null) {
                    totalInvAmount += NI.Total_Amount__c;
                }
                
                if(NI.Net_Amount__c != null) {
                    totalInvNetAmount += NI.Net_Amount__c;
                }
                
                if(NI.TotalGrossAmount__c != null) {
                    totalInvGrossAmount += NI.TotalGrossAmount__c;
                }
            }
        }
    }
    
    private void validateOLIAndNITotal() {
        if(OLITotal == nationalInvLITotal && OLITotal > 0 && nationalInvLITotal > 0) {
            validateOrdersBool = true;
            createInvoiceBool = false;
        } else {
            createInvoiceBool = true;
        }  
    }
}