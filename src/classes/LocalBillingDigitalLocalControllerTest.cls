@IsTest(SeeAlldata=true)
public with sharing class LocalBillingDigitalLocalControllerTest {

  public static testMethod void TestLocalBillingDigitalLocalControllerController(){
            Canvass__c c=TestMethodsUtility.createCanvass();
            c.Billing_Entity__c='ACS';
            update c;
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('telco'));
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount.add(TestMethodsUtility.generateAccount('publication'));
            insert lstAccount;  
            Account newAccount = new Account();
            Account newPubAccount = new Account();
            Account newTelcoAccount = new Account();
            for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
            newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
            newPubAccount = iterator;
            }
            else {
            newTelcoAccount = iterator;
            }
            }
            Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
            objTelco.Telco_Code__c = 'Test';
            update objTelco;
            system.assertNotEquals(newTelcoAccount.ID, null);
            Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
            Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv = TestMethodsUtility.createDivision();
        
            /*Directory__c objDir = TestMethodsUtility.generateDirectory();
            objDir.Telco_Recives_Electronice_File__c=true;
            objDir.Telco_Provider__c = objTelco.Id;
            objDir.Canvass__c = newAccount.Primary_Canvass__c;        
            objDir.Publication_Company__c = newPubAccount.Id;
            objDir.Division__c = objDiv.Id;
            objDir.Directory_Code__c = '100000';
            insert objDir;*/
            
            Directory__c objDir=TestMethodsUtility.createDirectory();  
            
            Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
            Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
            insert objSHM;
            
            date billDate= date.today();
            Digital_Telco_Scheduler__c objDigitalTelcoScheduler = new Digital_Telco_Scheduler__c();
            objDigitalTelcoScheduler.Billing_Entity__c='ACS';
            objDigitalTelcoScheduler.Bill_Prep_Date__c=date.today().adddays(4);
            objDigitalTelcoScheduler.Invoice_From_Date__c=billDate.addDays(10);
            objDigitalTelcoScheduler.Invoice_To_Date__c=billDate.addDays(50);
            //objDigitalTelcoScheduler.Sent_Telco_File__c=
            objDigitalTelcoScheduler.Telco__c=objTelco.id;
            objDigitalTelcoScheduler.Telco_Bill_Date__c=billDate;
            //objDigitalTelcoScheduler.Telco_Receives_EFile__c
            //objDigitalTelcoScheduler.XML_Output_Total_Amount__c;
            insert objDigitalTelcoScheduler;
            
            
            Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
            objDirEd .New_Print_Bill_Date__c=date.today();
            objDirEd .Bill_Prep__c=date.parse('01/01/2013');
            objDirEd.XML_Output_Total_Amount__c=100;
            objDirEd.Pub_Date__c =Date.today().addMonths(1);
            insert objDirEd;
            
            Directory_Edition__c objDirE = new Directory_Edition__c();
            objDirE.Name = 'Test DirE1';
            objDirE.Directory__c = objDir.Id;
            objDirE.Letter_Renewal_Stage_1__c = system.today();
            objDirE.Sales_Lockout__c=Date.today().addDays(30);
            objDirE.book_status__c='NI';
            objDirE.Pub_Date__c =Date.today().addMonths(2);
            insert objDirE;
            
            Directory_Edition__c objDirE1 = new Directory_Edition__c();
            objDirE1.Name = 'Test DirE2';
            objDirE1.Directory__c = objDir.Id;
            objDirE1.Letter_Renewal_Stage_1__c = system.today();
            objDirE1.Sales_Lockout__c=Date.today().addDays(30);
            objDirE1.book_status__c='BOTS';
            objDirE1.Pub_Date__c =Date.today().addMonths(3);
            insert objDirE1;     
            
            Directory__c objDirNew = TestMethodsUtility.generateDirectory();
            objDirNew .Canvass__c = newAccount.Primary_Canvass__c;        
            objDirNew .Publication_Company__c = newPubAccount.Id;
            objDirNew .Division__c = objDiv.Id;
            objDirNew.Directory_Code__c = '100001';
            insert objDirNew ;
            Directory_Edition__c objDirEdNew = TestMethodsUtility.generateDirectoryEdition(objDirNew );
            objDirEdNew.XML_Output_Total_Amount__c=200;
            objDirEdNew.Pub_Date__c =Date.today().addMonths(4);
            insert objDirEdNew;
            
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Family = 'Print';
            insert newProduct;
            
            Product2 objProd = new Product2();
            objProd.Name = 'Test';
            objProd.Product_Type__c = 'Print';
            objProd.ProductCode = 'WLCSH';
            objProd.Print_Product_Type__c='Display';
            insert  objProd;
            
            Product2 objProd1 = new Product2();
            objProd1.Name = 'Test';
            objProd1.Product_Type__c = 'Print';
            objProd1.ProductCode = 'GC50';
            objProd1.Print_Product_Type__c='Specialty';          
            insert objProd1;
            
            Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
            newOpportunity.AccountId = newAccount.Id;
            newOpportunity.Pricebook2Id = newPriceBook.Id;
            newOpportunity.Account_Primary_Canvass__c=c.id;
            insert newOpportunity;
            
            Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
            
            Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);

               
            c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDir);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd1);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount,newOpportunity);
            LocalBillingDigitalLocalController lbdlc1 = new LocalBillingDigitalLocalController();
            invoice.Customer_Name__c=newAccount.id;
            invoice.SI_P4P__c = false;
            invoice.SI_Media_Type__c='Digital';
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.SI_Payment_Method__c='Statement'; 
            invoice.c2g__InvoiceDate__c=date.today().adddays(15);
            insert invoice;
          
           //invoice=[select Billing_Entity__c from  c2g__codaInvoice__c where id=:invoice.id];
           
          // system.debug('BillingEntity'+invoice.Billing_Entity__c);
    
           c2g__codaCreditNote__c SCN = TestMethodsUtility.generateSalesCreditNote(invoice,newAccount);
           SCN.Transaction_Type__c='TD - Billing Transfer Invoice';
           SCN.c2g__CreditNoteStatus__c='In Progress';
          SCN.Customer_Name__c=newAccount.id;
           SCN.SC_P4P__c =false;
           SCN.SC_Billing_Entity__c='ACS';
           SCN.SC_Media_Type__c='Digital';
           SCN.c2g__CreditNoteDate__c=date.today().adddays(15);
           insert SCN;
           
           list< c2g__codaCreditNote__c>  lstSCN = new  list< c2g__codaCreditNote__c>();
           c2g__codaCreditNote__c SCN1 = TestMethodsUtility.generateSalesCreditNote(invoice,newAccount);
           SCN1.Transaction_Type__c='TD - Billing Transfer Invoice';
           SCN1.c2g__CreditNoteStatus__c='In Progress';
           SCN1.SC_P4P__c =false;
           SCN1.SC_Billing_Entity__c='ACS';
           SCN1.SC_Media_Type__c='Digital';
           SCN1.c2g__CreditNoteDate__c=date.today().adddays(15);           
           lstSCN.add(SCN1);
           //lstSCN.add(TestMethodsUtility.generateSalesCreditNote(invoice,newAccount));             
           insert lstSCN;
         
            Dummy_Object__c objDummy= new Dummy_Object__c(Date__c=date.today().adddays(4),From_Date__c = date.newinstance(2012, 2, 17),To_Date__c =date.newinstance(2015, 2, 2));
            insert objDummy;
            
            
          
             /* First Month Print Start Here */     
            //Telco Billing
            Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
            Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
            Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
            Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1);
            insert objOrderLineItem;
            
            c2g__codaDimension3__c dimension3=TestMethodsUtility.createDimension3(objOrderLineItem);
           // c2g__codaInvoiceLineItem__c invoiceLi=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItem,invoice,objProd1,dimension1,dimension2,dimension3,dimension4 );
            
            c2g__codaInvoiceLineItem__c invoiceLi= new c2g__codaInvoiceLineItem__c(Order_Line_Item__c =objOrderLineItem.Id,c2g__Quantity__c = 1, c2g__UnitPrice__c = 100, c2g__Invoice__c = invoice.Id, c2g__Product__c = objProd1.Id,c2g__Dimension1__c=dimension1.id,c2g__Dimension2__c=dimension2.id,c2g__Dimension3__c= dimension3.id,c2g__Dimension4__c=dimension4.id);
            invoiceLi.Directory_Edition__c=objDirEd.Id;
            invoiceLi.Directory__c=Objdir.Id;
            invoiceLi.Billing_Frequency__c='Monthly';
            insert invoiceLi;
       
            //Berry Billing Partner
            Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Billing_Partner__c='Berry',
            Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
            Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
            Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,
            Payment_Method__c='ACH',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=11,
            Successful_Payments__c=1 );
            insert objOrderLineItem1;
            
            
           // c2g__codaInvoiceLineItem__c invoiceLi1=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItem1,invoice,objProd1,dimension1,dimension2,dimension3,dimension4 );
            
            c2g__codaInvoiceLineItem__c invoiceLi1= new c2g__codaInvoiceLineItem__c(Order_Line_Item__c =objOrderLineItem1.Id,c2g__Quantity__c = 1, c2g__UnitPrice__c = 100, c2g__Invoice__c = invoice.Id, c2g__Product__c = objProd1.Id,c2g__Dimension1__c=dimension1.id,c2g__Dimension2__c=dimension2.id,c2g__Dimension3__c= dimension3.id,c2g__Dimension4__c=dimension4.id);
            invoiceLi1.Directory_Edition__c=objDirEd.Id;
            invoiceLi1.Directory__c=Objdir.Id;
            invoiceLi1.Billing_Frequency__c='Monthly';
            insert invoiceLi1;
    
            
    
        Test.startTest();
        //Dummy_Object__c objDummy =new Dummy_Object__c(Name='Test',Date__c=date.today().adddays(4));
       LocalBillingDigitalLocalController lbdlc = new LocalBillingDigitalLocalController();
       lbdlc.objDummy = objDummy;
       lbdlc.strSelectedBillingEntity=objDigitalTelcoScheduler.id;  
       
       lbdlc.fetchDirectoryEdition();
       lbdlc.checkInvoicesforDigitalNew();    
       
     
 
       
       lbdlc.SendTelcoFile();
       lbdlc.bReconciled = true;
       lbdlc.updateSIwithReconciledCheckBox();
       lbdlc.bPostAllInvoice=true;
       lbdlc.postFFInvoice();
       lbdlc.apexJobStatus();
       lbdlc.apexReconciliationJobStatus(); 
       lbdlc.CheckTelcoOutput();
       //lbdlc.fetchDTSForReport();
       
    Test.stopTest();
    
  
  }


}