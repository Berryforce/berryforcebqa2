global class DirectoryFlipBookUpdatesSchedule implements Schedulable {
   public Interface DirectoryFlipBookUpdatesScheduleInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('DirectoryFlipBookUpdatesScheduleHndlr');
        if(targetType != null) {
            DirectoryFlipBookUpdatesScheduleInterface obj = (DirectoryFlipBookUpdatesScheduleInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}