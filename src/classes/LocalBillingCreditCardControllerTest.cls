@IsTest(SeeAlldata=true)
public with sharing class LocalBillingCreditCardControllerTest {

  public static testMethod void testLocalBillingCreditCardController(){
    
    // Test Data creation
    
    Canvass__c c=TestMethodsUtility.createCanvass();
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('telco'));
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount.add(TestMethodsUtility.generateAccount('publication'));
            insert lstAccount;  
            Account newAccount = new Account();
            Account newPubAccount = new Account();
            Account newTelcoAccount = new Account();
            for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
            newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
            newPubAccount = iterator;
            }
            else {
            newTelcoAccount = iterator;
            }
            }
            Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
            objTelco.Telco_Code__c = 'Test';
            update objTelco;
            system.assertNotEquals(newTelcoAccount.ID, null);
            Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
            Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv = TestMethodsUtility.createDivision();
        
           /* Directory__c objDir = TestMethodsUtility.generateDirectory();
            objDir.Telco_Recives_Electronice_File__c=true;
            objDir.Telco_Provider__c = objTelco.Id;
            objDir.Canvass__c = newAccount.Primary_Canvass__c;        
            objDir.Publication_Company__c = newPubAccount.Id;
            objDir.Division__c = objDiv.Id;
            objDir.Directory_Code__c = '100000';
            insert objDir;*/
            
            Directory__c objDir = TestMethodsUtility.createDirectory(); 
            
            Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
            Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
            insert objSHM;
            
            Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
            objDirEd .New_Print_Bill_Date__c=date.today();
            objDirEd .Bill_Prep__c=date.parse('01/01/2013');
            objDirEd.XML_Output_Total_Amount__c=100;
            objDirEd.Pub_Date__c =Date.today().addMonths(1);
            insert objDirEd;
            
            Directory_Edition__c objDirE = new Directory_Edition__c();
            objDirE.Name = 'Test DirE1';
            objDirE.Directory__c = objDir.Id;
            objDirE.Letter_Renewal_Stage_1__c = system.today();
            objDirE.Sales_Lockout__c=Date.today().addDays(30);
            objDirE.book_status__c='NI';
            objDirE.Pub_Date__c =Date.today().addMonths(2);
            insert objDirE;
            
            Directory_Edition__c objDirE1 = new Directory_Edition__c();
            objDirE1.Name = 'Test DirE2';
            objDirE1.Directory__c = objDir.Id;
            objDirE1.Letter_Renewal_Stage_1__c = system.today();
            objDirE1.Sales_Lockout__c=Date.today().addDays(30);
            objDirE1.book_status__c='BOTS';
            objDirE1.Pub_Date__c =Date.today().addMonths(3);
            insert objDirE1;     
            
            Directory__c objDirNew = TestMethodsUtility.generateDirectory();
            objDirNew .Canvass__c = newAccount.Primary_Canvass__c;        
            objDirNew .Publication_Company__c = newPubAccount.Id;
            objDirNew .Division__c = objDiv.Id;
            objDirNew.Directory_Code__c = '100001';
            insert objDirNew ;
            Directory_Edition__c objDirEdNew = TestMethodsUtility.generateDirectoryEdition(objDirNew );
            objDirEdNew.XML_Output_Total_Amount__c=200;
            objDirEdNew.Pub_Date__c =Date.today().addMonths(4);
            insert objDirEdNew;
            
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Family = 'Print';
            insert newProduct;
            
            Product2 objProd = new Product2();
            objProd.Name = 'Test';
            objProd.Product_Type__c = 'Print';
            objProd.ProductCode = 'WLCSH';
            objProd.Print_Product_Type__c='Display';
            insert  objProd;
            
            Product2 objProd1 = new Product2();
            objProd1.Name = 'Test';
            objProd1.Product_Type__c = 'Print';
            objProd1.ProductCode = 'GC50';
            objProd1.Print_Product_Type__c='Specialty';          
            insert objProd1;
            
            Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
            newOpportunity.AccountId = newAccount.Id;
            newOpportunity.Pricebook2Id = newPriceBook.Id;
            insert newOpportunity;
            
            Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
            
            Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);

            c2g__codaCompany__c companyObj =TestMethodsUtility.generateCompany();
            
            Order_Line_Items__c objOrderLineItemStmtOdd = new Order_Line_Items__c(Billing_Partner__c='Berry123',
            Account__c=newAccount.Id, 
            Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,
            Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=true,media_type__c='Print',
            Directory_Edition__c = objDirEdNew.Id,Directory__c=objDirNew.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Statement',
            Package_ID__c='pkgid_12',Payments_Remaining__c=10,Successful_Payments__c=2);
            insert objOrderLineItemStmtOdd;
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount,newOpportunity);
            invoice.Customer_Name__c=newAccount.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.SI_Payment_Method__c='Statement'; 
            insert invoice;
            
             c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDirNew);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            c2g__codaDimension3__c  dimension3_ss=TestMethodsUtility.createDimension3(objOrderLineItemStmtOdd);
            c2g__codaInvoiceLineItem__c invoiceLiStmt=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItemStmtOdd,invoice,objProd,dimension1,dimension2,dimension3_ss,dimension4 );
       
             Dummy_Object__c objDummy= new Dummy_Object__c(From_Date__c = date.newinstance(2012, 2, 17),To_Date__c =date.newinstance(2015, 1, 18));
            insert objDummy;       
    
       LocalBillingCreditCardController lbccc = new LocalBillingCreditCardController();
       lbccc.objDummy = objDummy;
       lbccc.getPaymentMethods();
       lbccc.clickGoNew();
       lbccc.clickGo();
       lbccc.bReconciled = true;
       lbccc.updateSIwithReconciledCheckBox();
       
       Test.startTest();
       Id SIReconciliationbatchId= Database.executeBatch(new LocalBillingSIReconciliationBatch(new Set<Id>{invoice.Id})); 
       Test.stopTest();
       lbccc.SIReconciliationbatchId=SIReconciliationbatchId;
       lbccc.SCNReconciliationbatchId=SIReconciliationbatchId;
       lbccc.apexReconciliationJobStatus();
       lbccc.apexJobStatus();
       lbccc.postFFInvoice();      
  }
   
  public static testMethod void testPositiveLocalBillingCreditCardController(){
    
    // Test Data creation
    
    Canvass__c c=TestMethodsUtility.createCanvass();
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('telco'));
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount.add(TestMethodsUtility.generateAccount('publication'));
            insert lstAccount;  
            Account newAccount = new Account();
            Account newPubAccount = new Account();
            Account newTelcoAccount = new Account();
            for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
            newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
            newPubAccount = iterator;
            }
            else {
            newTelcoAccount = iterator;
            }
            }
            Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
            objTelco.Telco_Code__c = 'Test';
            update objTelco;
            system.assertNotEquals(newTelcoAccount.ID, null);
            Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
            Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv = TestMethodsUtility.createDivision();
        
            /*Directory__c objDir = TestMethodsUtility.generateDirectory();
            objDir.Telco_Recives_Electronice_File__c=true;
            objDir.Telco_Provider__c = objTelco.Id;
            objDir.Canvass__c = newAccount.Primary_Canvass__c;        
            objDir.Publication_Company__c = newPubAccount.Id;
            objDir.Division__c = objDiv.Id;
            objDir.Directory_Code__c = '100000';
            insert objDir;*/
             
            Directory__c objDir = TestMethodsUtility.createDirectory(); 
            
            Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
            Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
            insert objSHM;
            
            Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
            objDirEd .New_Print_Bill_Date__c=date.today();
            objDirEd .Bill_Prep__c=date.parse('01/01/2013');
            objDirEd.XML_Output_Total_Amount__c=100;
            objDirEd.Pub_Date__c =Date.today().addMonths(1);
            insert objDirEd;
            
            Directory_Edition__c objDirE = new Directory_Edition__c();
            objDirE.Name = 'Test DirE1';
            objDirE.Directory__c = objDir.Id;
            objDirE.Letter_Renewal_Stage_1__c = system.today();
            objDirE.Sales_Lockout__c=Date.today().addDays(30);
            objDirE.book_status__c='NI';
            objDirE.Pub_Date__c =Date.today().addMonths(2);
            insert objDirE;
            
            Directory_Edition__c objDirE1 = new Directory_Edition__c();
            objDirE1.Name = 'Test DirE2';
            objDirE1.Directory__c = objDir.Id;
            objDirE1.Letter_Renewal_Stage_1__c = system.today();
            objDirE1.Sales_Lockout__c=Date.today().addDays(30);
            objDirE1.book_status__c='BOTS';
            objDirE1.Pub_Date__c =Date.today().addMonths(3);
            insert objDirE1;     
            
            Directory__c objDirNew = TestMethodsUtility.generateDirectory();
            objDirNew .Canvass__c = newAccount.Primary_Canvass__c;        
            objDirNew .Publication_Company__c = newPubAccount.Id;
            objDirNew .Division__c = objDiv.Id;
            objDirNew.Directory_Code__c = '100001';
            insert objDirNew ;
            Directory_Edition__c objDirEdNew = TestMethodsUtility.generateDirectoryEdition(objDirNew );
            objDirEdNew.XML_Output_Total_Amount__c=200;
            objDirEdNew.Pub_Date__c =Date.today().addMonths(4);
            insert objDirEdNew;
            
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Family = 'Print';
            insert newProduct;
            
            Product2 objProd = new Product2();
            objProd.Name = 'Test';
            objProd.Product_Type__c = 'Print';
            objProd.ProductCode = 'WLCSH';
            objProd.Print_Product_Type__c='Display';
            insert  objProd;
            
            Product2 objProd1 = new Product2();
            objProd1.Name = 'Test';
            objProd1.Product_Type__c = 'Print';
            objProd1.ProductCode = 'GC50';
            objProd1.Print_Product_Type__c='Specialty';          
            insert objProd1;
            
            Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
            newOpportunity.AccountId = newAccount.Id;
            newOpportunity.Pricebook2Id = newPriceBook.Id;
            insert newOpportunity;
            
            Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
            
            Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);

            c2g__codaCompany__c companyObj =TestMethodsUtility.generateCompany();
            
            Order_Line_Items__c objOrderLineItemStmtOdd = new Order_Line_Items__c(Billing_Partner__c='Berry123',
            Account__c=newAccount.Id, 
            Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,
            Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=true,media_type__c='Print',
            Directory_Edition__c = objDirEdNew.Id,Directory__c=objDirNew.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Payment_Method__c='Statement',
            Package_ID__c='pkgid_12',Payments_Remaining__c=10,Successful_Payments__c=2);
            insert objOrderLineItemStmtOdd;
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount,newOpportunity);
            invoice.Customer_Name__c=newAccount.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.SI_Payment_Method__c='Statement'; 
            insert invoice;
            
             c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDirNew);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            c2g__codaDimension3__c  dimension3_ss=TestMethodsUtility.createDimension3(objOrderLineItemStmtOdd);
            c2g__codaInvoiceLineItem__c invoiceLiStmt=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItemStmtOdd,invoice,objProd,dimension1,dimension2,dimension3_ss,dimension4 );
       
             Dummy_Object__c objDummy= new Dummy_Object__c(From_Date__c = date.newinstance(2012, 2, 17),To_Date__c =date.newinstance(2015, 1, 18));
            insert objDummy;
            
       LocalBillingCreditCardController lbccc = new LocalBillingCreditCardController();
       lbccc.objDummy = objDummy;
       lbccc.getPaymentMethods();
       lbccc.clickGoNew();
       lbccc.clickGo();
       lbccc.bReconciled = true;
       lbccc.updateSIwithReconciledCheckBox();
       lbccc.bPostAllInvoice=true;
       Test.startTest();
       Id batchId = Database.executeBatch(new FFSalesInvoicePostBatchController(new Set<Id>{invoice.Id}));
       lbccc.batchId =batchId ;
       lbccc.SCNbatchId =batchId ;
       lbccc.postFFInvoice();
       lbccc.apexJobStatus();
       Test.stopTest();
  }

}