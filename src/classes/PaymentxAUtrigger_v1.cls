public class PaymentxAUtrigger_v1 
{
    public static string attachmentName{get;set;}
    public static string istest{get;set;}
    public void makePDF(set<ID> invoiceID)
    {
        pymt__PaymentX__c   objPayment = new pymt__PaymentX__c();
        Account objAccount = new Account();
        Contact objContact = new Contact();
        Canvass__c objCanvass = new Canvass__c();
        c2g__codaInvoice__c objInvoice=new c2g__codaInvoice__c ();
        string errortext;
        Date servEnd ;
        list<c2g__codaInvoiceLineItem__c> lstILI =new list<c2g__codaInvoiceLineItem__c>();
        boolean isCreditCard;
        boolean isECheck;
        Opportunity objOpportunity = new Opportunity();
        map<Id, Opportunity> mapOpportunity = new map<Id, Opportunity>();
        map<Id, Contact> mapContact = new map<Id, Contact>();
        list<pymt__PaymentX__c> lstPayment = new list<pymt__PaymentX__c>();  
        List <Messaging.SingleEmailMessage> Messages = new List <Messaging.SingleEmailMessage>();       
        set<Id> accountID = new set<Id>();
        Map<Id, c2g__codaInvoice__c> mapInvoice = SalesInvoiceSOQLMethods.getMapSalesInvoiceLineItemsPaymentsbyInvoiceId(invoiceID);
        for(c2g__codaInvoice__c iteratorInvoice : mapInvoice.values()) 
        {
            accountID.add(iteratorInvoice.c2g__Account__c);
        }
       
        map<Id, Account> mapAccount = AccountSOQLMethods.getContactOpportunityByAccountId(accountID);
        for(c2g__codaInvoice__c iteratorInvoice : mapInvoice.values()) 
        {
            objInvoice = mapInvoice.get(iteratorInvoice.Id);
            if(objInvoice.c2g__InvoiceLineItems__r.isEmpty()){
            errortext ='<br/>FATAL no line items fount for invoice '+invoiceID+' <br/>';
	        }
	        else
	        {
	            lstILI = objInvoice.c2g__InvoiceLineItems__r;
	        }
	            //  lstPayment = objInvoice.p2f__Payments__r;
	        Map<Id,pymt__PaymentX__c> mapPayment=new Map<Id,pymt__PaymentX__c>(); 
	            
	        for(pymt__PaymentX__c  payment : objInvoice.p2f__Payments__r)
	        {
	            mapPayment.put(payment.Id,payment);
	        }
	            
	        for(pymt__PaymentX__c pymt:mapPayment.values())
	        {
	            objPayment=mapPayment.get(pymt.id);
	        }
	            
	        //For ACH AND Credit Card Billing
	            
	        pymt__PaymentX__c paymentX=[Select pymt__Payment_Type__c from pymt__PaymentX__c where id=:objPayment.id];
	         
	        if(paymentX.pymt__Payment_Type__c =='Credit Card')
	        {
	            isCreditCard=true;
	            isECheck=false;
	        }
	        else if(paymentX.pymt__Payment_Type__c =='ECheck')
	        {
	            isCreditCard=false;
	            isECheck=true;
	        }
	        else
	        {}
	                           
	        //Updation for ACH & Credit Card End Here
	        if(iteratorInvoice.c2g__Account__c == NULL){
	        errortext ='<br/>FATAL no records returned for account id '+iteratorInvoice.c2g__Account__c+' <br/>';
	        }
	        else
	        {
	        objAccount = mapAccount.get(iteratorInvoice.c2g__Account__c);
	        }
	        if(objAccount.Primary_Canvass__c == null){
	         errortext ='<br/>FATAL account primary canvass Id missing OR no records returned for canvass id '+objAccount.Primary_Canvass__c+' <br/>';
	        }
	        else
	        {
	        objCanvass = new Canvass__c(Id = objAccount.Primary_Canvass__c, Name = objAccount.Primary_Canvass__r.Name,  
	                                   Logo__c = objAccount.Primary_Canvass__r.Logo__c, 
	                                   Invoice_logo__c = objAccount.Primary_Canvass__r.Invoice_Logo__c, 
	                                   Terms_and_Conditions__c = objAccount.Primary_Canvass__r.Terms_and_Conditions__c, 
	                              co_brand_name__c = objAccount.Primary_Canvass__r.co_brand_name__c, 
	                                   Territory_Name__c = objAccount.Primary_Canvass__r.Territory_Name__c, 
	                                   IsActive__c = objAccount.Primary_Canvass__r.IsActive__c, 
	                                   Billing_Partner__c  = objAccount.Primary_Canvass__r.Billing_Partner__c);
	        
	         }
	                        
	        if(objAccount.Opportunities.size() > 0) 
	        {
	            for(Opportunity iteratorOpp : objAccount.Opportunities) 
	            {
	                mapOpportunity.put(iteratorOpp.Id, iteratorOpp);
	            }
	        }
	        
	        objOpportunity = mapOpportunity.get(objInvoice.c2g__Opportunity__c);
	        
	        if(objAccount.Contacts.size() > 0) 
	        {
	            for(Contact iteratorContact : objAccount.Contacts) 
	            {
	                mapContact.put(iteratorContact.Id, iteratorContact);
	            }
	        }
            servEnd = objInvoice.c2g__DueDate__c.addMonths(1)-1;
            objContact = mapContact.get(objOpportunity.Billing_Contact__c);
            Messages.add(CreatePDFAttachment(objAccount,objContact ,objPayment,objInvoice,lstILI,servEnd,isCreditCard,isECheck));
            
        }
        Messaging.sendEmail(Messages); 
    }
    public Messaging.SingleEmailMessage CreatePDFAttachment(Account objAccount,Contact objContact ,pymt__PaymentX__c objPayment,c2g__codaInvoice__c  objInvoice,list<c2g__codaInvoiceLineItem__c> lstILI,Date servEnd,boolean isCreditCard,boolean isECheck)
    {
            List<Attachment > attList=new List<Attachment>();
            String pdfContent;
            string invoicestr='<table cellpadding="0" cellspacing="0" border="0" style="font-size:8pt;width:100%"><tr><td style="font-weight:bold;padding-left:10px">Product</td><td style="padding-left:10px;font-weight:bold">Service Dates</td><td style="padding-left:10px;font-weight:bold" width="14%"> Amount</td></tr>';
            DateTime InvoiceDate=DateTime.newInstance(objInvoice.c2g__DueDate__c.year(), objInvoice.c2g__DueDate__c.month(),objInvoice.c2g__DueDate__c.day());
            String formattedInvoiceDate=InvoiceDate.format('MM/dd/yyyy');
            
            for(c2g__codaInvoiceLineItem__c ILIIterator: lstILI )
            {
               if(ILIIterator.c2g__UnitPrice__c>0)
               {
                 
                 DateTime dtDue=DateTime.newInstance(objInvoice.c2g__DueDate__c.year(), objInvoice.c2g__DueDate__c.month(),objInvoice.c2g__DueDate__c.day());
                 String formattedDDate=dtDue.format('MM/dd/yyyy');
                 DateTime dtServEnd=DateTime.newInstance(servEnd.year(), servEnd.month(),servEnd.day());
                 String formattedEDate=dtServEnd.format('MM/dd/yyyy');
                 invoicestr=invoicestr+'<tr><td>'+ILIIterator.c2g__Product__r.Name+'</td><td>';
                 invoicestr= invoicestr+formattedDDate+'&nbsp;&nbsp;-'+formattedEDate+'&nbsp;&nbsp;<td>&nbsp;$&nbsp;'+ILIIterator.c2g__UnitPrice__c+'</td></tr>';
              }
              else
              {
                 invoicestr=invoicestr+'<tr><td>'+ILIIterator.c2g__Product__r.Name+'</td><td>'+ILIIterator.c2g__LineDescription__c+'&nbsp;&nbsp;';
                 invoicestr=invoicestr+'<td>&nbsp;$&nbsp;'+ILIIterator.c2g__UnitPrice__c+'</td></tr>';
              }
            }
            invoicestr=invoicestr+'</table>';
            
            string totalstr='<table cellspacing="0" cellpadding="0" border="0" width="100%" style="padding-left:10px"><tr valign="top"><th style="font-size:8pt;">Taxes/Fees/Other Charges</th><th style="font-size:8pt;text-align:center;">&nbsp;&nbsp;</th><th style="font-size:8pt;text-align:center;">&nbsp;&nbsp;</th></tr><tr valign="top"><td style="font-size:8pt;width:200px;">Total Tax</td><td style="font-size:8pt;width:10px;">:</td><td style="font-size:8pt;">$&nbsp;'+objInvoice.c2g__TaxTotal__c+'</td></tr><tr nowrap="true" valign="top"><td style="font-size:8pt;width:200px;">Grand Total</td><td style="font-size:8pt;width:10px;">:</td><td style="font-size:8pt;" >$'+objInvoice.c2g__NetTotal__c+'</td></tr><tr valign="top"><td style="font-size:8pt;width:200px;">Total Payment Process</td><td style="border-bottom: 0;font-size:8pt;width:10px;">:</td><td style="font-size:8pt;">$'+objPayment.pymt__Amount__c+'</td></tr></table>';
           
            string pymnstr='<table border="0"> <tr valign="top"><td colspan="2" style="font-size:8pt;text-align:center"></td></tr><tr style="height:10px;"></tr><tr valign="top"><td colspan="2" style="font-size:8pt;">';
            
            if(isCreditCard)
            {
              pymnstr=pymnstr+' <table border="0"><tr valign="top"><td style="font-size:8pt;width:200px;">Name On Card</td><td style="border-bottom: 0;font-size:8pt;width:10px;">:</td><td style="font-size:8pt;">'+objPayment.pymt__Contact__r.name+'</td></tr><tr valign="top"><td style="font-size:8pt;">Card Type</td><td style="font-size:8pt;width:10px;">:</td><td style="font-size:8pt;">'+objPayment.pymt__Card_Type__c+'</td></tr><tr valign="top"><td style="font-size:8pt;">Card Number</td><td style="font-size:8pt;width:10px;">:</td><td style="border-bottom: 0;font-size:8pt;">'+objPayment.pymt__Last_4_Digits__c+'</td></tr><tr valign="top"><td style="font-size:8pt;">Approval #</td><td style="font-size:8pt;width:10px;">:</td><td style="font-size:8pt;">'+objPayment.pymt__Authorization_Id__c+'</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr style="height:10px;"></tr><tr valign="top"><td style="font-size:8pt;"><b>Terms and Conditions</b></td><td style="font-size:8pt;width:10px;">:</td><td style="font-size:8pt;">See your advertising order for information related to Berrys Terms and Conditions </td></tr></table>';
            }
            if(isECheck)
            {
              pymnstr=pymnstr+ '<table border="0"><tr valign="top"><td style="font-size:8pt;width:200px;">Name On Check</td><td style="font-size:8pt;width:10px;">:</td><td style="font-size:8pt;">'+objPayment.pymt__Contact__r.name+'</td></tr><tr valign="top"><td style="font-size:8pt;">Check Number</td><td style="font-size:8pt;width:10px;">:</td><td style="font-size:8pt;">'+objPayment.pymt__Check_Number__c+'</td></tr><tr valign="top"><td style="font-size:8pt;"></td><td style="font-size:8pt;width:10px;">:</td><td style="font-size:8pt;"></td></tr><tr valign="top"><td style="font-size:8pt;">Approval #</td><td style="font-size:8pt;width:10px;">:</td><td style="font-size:8pt;">'+objPayment.pymt__Authorization_Id__c+'</td></tr><tr style="height:10px;"></tr><tr valign="top"><td style="font-size:8pt;"><br/><b>Terms and Conditions</b></td><td style="font-size:8pt;width:10px;">:</td><td style="font-size:8pt;"><br/>See your advertising order for information related to Berrys Terms and Conditions </td></tr></table>';
            }
            
            pymnstr=pymnstr+'<table border="0" width="380"><tr valign="top"><td style="border-bottom:1px;"><span style="border-bottom:1px solid black;font-weight:bold;text-decoration:underline;font-size:8pt;">How to Reach Us</span></td><td></td></tr><tr valign="top"><td style="font-size:8pt;"><b>Customer Service</b> : &nbsp;</td><td><div style="width:100px"><span  style="color:red">1-877-877-0475</span>:&nbsp; &nbsp; 8:00AM to 5:00PM EST, Monday through Friday<br/>Email: customer.relations@theberrycompany.com</div></td></tr><tr valign="top"><td style="font-size:8pt;"><b>On-Line</b>:</td><td><div>Website:  http://www.BerryMeansBusiness.com/</div></td></tr></table>';
            pymnstr=pymnstr+'</td></tr></table>';
            
            string pdfhead='<html><body><div style="width:550px;">';
            string pdfheaderImages='<table width="550" cellpadding="0" cellspacing="0" border="0"><tr><td colspan="2"><img src="https://c.cs11.content.force.com/servlet/servlet.ImageServer?id=015Z00000006QIu&oid=00DZ0000001C48o&lastMod=1384862853000" width="200"></td><td><img width="90" src="https://c.cs11.content.force.com/servlet/servlet.ImageServer?id=015Z00000006QIz&oid=00DZ0000001C48o&lastMod=1384862875000"></td></tr></table>';
            string pdfMainTable='<table width="550" cellpadding="0" cellspacing="0" style="margin:0px auto;" border="0"><tr>';
            string pdfContentMainTableTd1='<td valign="top" style="padding:0px;" colspan="2"><div style=""><table border="0" cellpadding="0" cellspacing="0" style="margin:0px auto;width:500px"><tr valign="top"><td colspan="4" style="font-size:10pt;"><table border="0" width="100%"><tr valign="top"><td style="font-size:10pt;"><b>Receipt for Advertising Payment</b></td><td style="font-size:9pt;text-align:right;padding-right:10px"><b>Date:</b>&nbsp;<span>'+formattedInvoiceDate+'</span></td></tr></table><table border="0"><tr valign="top"><td nowrap="true" style="font-size:9pt;"><b>For:</b><br><span>'+objAccount.Name+'</span><br><span>'+objAccount.BillingStreet+'</span><br><span>'+objAccount.BillingCity+'</span>,&nbsp;<span>'+objAccount.BillingState+'</span>,&nbsp;<span>'+objAccount.BillingPostalCode+'</span><br><br><b>Phone Number:</b>&nbsp;<span>'+objAccount.Phone+'</span><br><b>Customer Number:</b>&nbsp;<span>'+objAccount.Account_Number__c+'</span><br><br></td></tr><tr><td colspan="5" valign="top"><b style="font-size:12pt;">Detail</b></td></tr></table></td></tr></table><br/><div>'+invoicestr+'</div><div>'+totalstr+'</div><div>'+pymnstr+'</div></td>';
            string pdfContentMainTableTd2='<td valign="top" style="padding:0px;"><div style="font-size:8pt;font-family:Calibiri;text-align:center;">Visit us at <br/>BerryMeansBusiness.com <br/>to learn about products and<br/> services Berry provides to deliver<br/> leads to your business!<br/><img src="https://c.cs11.content.force.com/servlet/servlet.ImageServer?id=015Z00000006Q4i&oid=00DZ0000001C48o&lastMod=1384760548000" width="88" height="76"/></div><div style="text-align:center;font-size:8pt;font-family:arial">Follow Berry on<br/>Facebook and Twitter&nbsp;&nbsp;<br/>for small business tips,&nbsp;webinars,<br/> recycling info and more! <br/><br/> <table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px" width="100"><tr><td><img src="https://c.cs11.content.force.com/servlet/servlet.ImageServer?id=015Z00000006Q4n&oid=00DZ0000001C48o&lastMod=1384760603000" width="25" height="25"/></td><td><img src="https://c.cs11.content.force.com/servlet/servlet.ImageServer?id=015Z00000006Q4s&oid=00DZ0000001C48o&lastMod=1384760633000" width="25" height="25"/></td><tr><td colspan="2">BerryMeansBiz</td></tr></tr></table></div><br/><span style="margin:0px auto;width:100px;color:red;font-size:8pt;padding-left:30px">Note:Please see comment box for info on 800#</span></div></td>';
            string pdfMainTableEnd='</tr></table>';
            string footertable='<table cellspacing="0" cellpadding="0" border="0" width="100%"><tr><td style="font-size:18pt;text-align:center;"><span style="font-family: Brush Script MT;font-style: italic;">Thank you for your business!</span></td></tr></table>';
            string pdfend='</div></body></html>';
            pdfContent=pdfhead+pdfMainTable+pdfContentMainTableTd1+pdfContentMainTableTd2+pdfMainTableEnd+footertable+pdfend;
            
            Messaging.SingleEmailMessage Message = new Messaging.SingleEmailMessage();
            Message.setTargetObjectId(objContact.id); // Id of Contact 
           // Message.setTargetObjectId('003Z000000UXGB6');
            Message.setWhatId(objPayment.id);
            Message.setSaveAsActivity(false);
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName('Invoice Receipt.pdf');
            efa.setBody(Blob.toPDF(pdfContent));
            Message.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            Message.setTemplateId(Label.ETID_Initial_Payment);
           // Message.setHTMLBody('Please find the Invoice Receipt..Thanks Berry Team');
            
            return Message;
            
    }
       
    public static void declineCCNotification(List<pymt__PaymentX__c> PaymentList) {
        Set<Id> ContactIds = new Set<Id>();
        Map<Id, Id> ContPymtMap = new Map<Id, Id>();
        Map<Id, Contact> contactMap = new Map<Id, Contact>();
    	List<Case> caseList = new List<Case>();
        
        for(pymt__PaymentX__c Payment : PaymentList) {
            if(Payment.pymt__Status__c == CommonMessages.pymtStatusDecline) {
            	if(String.isNotBlank(Payment.pymt__Contact__c)){
	                ContactIds.add(Payment.pymt__Contact__c);
	                ContPymtMap.put(Payment.pymt__Contact__c, Payment.Id);
            	}
            }
        }   
        if(!ContactIds.isEmpty()) {
        	contactMap = ContactSOQLMethods.getContactByContactID(ContactIds);
            List <Messaging.SingleEmailMessage> Messages = new List <Messaging.SingleEmailMessage>();
            for(ID ContId : ContactIds) {
                Messaging.SingleEmailMessage Message = new Messaging.SingleEmailMessage();
                Message.setTemplateId(Label.CCDeclined);
                Message.setTargetObjectId(ContId); // Id of Contact 
                Message.setWhatId(ContPymtMap.get(ContId));
                Message.setSaveAsActivity(false);
                Messages.add(Message);                                
            }
            Messaging.sendEmail(Messages);
        }
        /* Creating case for declined payment */
        /*for(pymt__PaymentX__c Payment : PaymentList) {
            if(Payment.pymt__Status__c == CommonMessages.pymtStatusDecline) {
                Case cas = new Case();
                cas.ContactId = Payment.pymt__Contact__c;
                cas.Status = CommonMessages.newCase;
                cas.Reason = CommonMessages.creditCardIssueReason;
                cas.Case_Reason__c = CommonMessages.creditCardIssueReason;
                cas.Received_From__c = CommonMessages.creditCardProcessorRecdFrom;    
                cas.Case_Category__c = CommonMessages.billingCaseType;
                if(contactMap.containsKey(Payment.pymt__Contact__c)){
                	cas.AccountId = contactMap.get(Payment.pymt__Contact__c).AccountId;
                	cas.Main_Listed_Phone_Number__c = contactMap.get(Payment.pymt__Contact__c).Phone;
                }                
                cas.RecordTypeId = CommonMethods.getRedordTypeIdByName(CommonMessages.billingCaseType, CommonMessages.caseObjectName);                  
                cas.Payment__c = Payment.Id;
                if(String.isNotBlank(Payment.pymt__Payment_Type__c)){
                	cas.Description = Payment.pymt__Payment_Type__c + '\n';
                }
                if(String.isNotBlank(Payment.pymt__Payment_Type__c)){
                	cas.Description += Payment.pymt__Amount__c + '\n';
                }
                if(String.isNotBlank(Payment.pymt__Payment_Method__c)){
                	cas.Description += Payment.pymt__Payment_Method__c + '\n';
                }
                if(String.isNotBlank(Payment.pymt__Card_Type__c)){
					cas.Description += Payment.pymt__Card_Type__c + '\n';
                }
                if(String.isNotBlank(Payment.pymt__Last_4_Digits__c)){
                	 cas.Description += Payment.pymt__Last_4_Digits__c;
                }    
                if(String.isNotBlank(Payment.pymt__Log__c)){
                	 cas.Description += Payment.pymt__Log__c;
                }  
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.useDefaultRule = true;
                dmo.EmailHeader.TriggerUserEmail = true;
    			cas.setOptions(dmo);
                caseList.add(cas);
            }
        }
        if(caseList.size() > 0){
        	insert caseList;
        }*/
    }
    
    public static void generatePDF(set<ID> salesInvoiceIDs){
    	List<c2g__codaInvoice__c> salesInvoiceList = new List<c2g__codaInvoice__c>();
    	salesInvoiceList = SalesInvoiceSOQLMethods.getMapSalesInvoiceLineItemsPaymentsbyInvoiceIds(salesInvoiceIDs); 
    	String pdfContent = ''; 
    	
    	for(c2g__codaInvoice__c SI : salesInvoiceList){
    		pdfContent = createHeaderForPDF(SI);
    		pdfContent += createBodyForPDF(SI);
    		pdfContent += createFooterForPDF(SI.p2f__Payments__r.get(0));
    	}  	
    }
    
    public static String createHeaderForPDF(c2g__codaInvoice__c salesInvoice){
    	String content;
    	/* First row of the Table */
    	content = '<html><table align="center"><tr><td colspan = "3"><h1>Receipt for Advertising Payment</h1></td>';
    	content += '<td><b>Date</b></td><td>' + salesInvoice.c2g__DueDate__c + '</td></tr>';
    	/* Second row of the Table */
    	content += '<tr><td colspan = "5"><b>For:</b></td></tr>';
    	/* Third row of the Table */
    	content += '<tr><td colspan = "5">' + salesInvoice.c2g__Account__r.Name + '</td></tr>';    	
    	/* Fourth row of the Table */
    	content += '<tr><td colspan = "5">' + salesInvoice.c2g__Account__r.BillingStreet + '</td></tr>';
    	/* Fifth row of the Table */
    	content += '<tr><td colspan = "5">' + salesInvoice.c2g__Account__r.BillingCity + ', ' + salesInvoice.c2g__Account__r.BillingState + ', ' + salesInvoice.c2g__Account__r.BillingCountry + ', ' + salesInvoice.c2g__Account__r.BillingPostalcode + '</tr>';
    	/* Sixth row of the Table */
    	content += '<tr><td>Phone Number:</td><td colspan = "4">' + salesInvoice.c2g__Account__r.Phone + '</td></tr>';
    	/* Sixth row of the Table */
    	content += '<tr><td>Account Number:</td><td colspan = "4">' + salesInvoice.c2g__Account__r.AccountNumber + '</td></tr>';
    	return content;
    }
    
    public static String createBodyForPDF(c2g__codaInvoice__c salesInvoice){
    	String content;
    	Map<String, List<c2g__codaInvoiceLineItem__c>> packageIdSalesInvLineItemListMap = new Map<String, List<c2g__codaInvoiceLineItem__c>>();
    	for(c2g__codaInvoiceLineItem__c SILI : salesInvoice.c2g__InvoiceLineItems__r){
			if(!packageIdSalesInvLineItemListMap.containsKey(SILI.Order_Line_Item__r.Package_ID__c)){
				packageIdSalesInvLineItemListMap.put(SILI.Order_Line_Item__r.Package_ID__c, new List<c2g__codaInvoiceLineItem__c>());
			}
			packageIdSalesInvLineItemListMap.get(SILI.Order_Line_Item__r.Package_ID__c).add(SILI);
    	}
    	
    	content = '<tr><td colspan="5">Detail</td></tr><tr><td>Product</td><td>Service Dates</td><td colspan="3"></td></tr>';
    	
    	for(String packageId : packageIdSalesInvLineItemListMap.keySet()){
    		Double unitPriceTotal = 0;
    		content += '<tr><td>Temp</td><td>' + salesInvoice.Service_Start_Date__c + ' - ' + salesInvoice.Service_End_Date__c +'</td>';
    		List<c2g__codaInvoiceLineItem__c> salesInvLineItemList = new List<c2g__codaInvoiceLineItem__c>();
    		salesInvLineItemList = packageIdSalesInvLineItemListMap.get(packageId);
    		for(c2g__codaInvoiceLineItem__c SILI : salesInvLineItemList){
    			unitPriceTotal += SILI.c2g__UnitPrice__c;
    		}
    		content += '<td>unitPriceTotal</td><td></td></tr>';
    	}
    	return content;
    }    
    
    public static String createFooterForPDF(pymt__PaymentX__c payment){
    	String content;
    	if(payment.pymt__Payment_Type__c == CommonMessages.ccPaymentType){
    		content += '<tr><td>Name on Credit Card (ACH):</td>' + payment.pymt__Contact__r.Name + '</td></tr>';
    		content += '<tr><td>Card Type:</td><td colspan="4">' + payment.pymt__Card_Type__c + '</td></tr>';
    		content += '<tr><td>Card Number:</td><td colspan="4">' + payment.pymt__Last_4_Digits__c + '</td></tr>';
    		content += '<tr><td>Approval #</td><td colspan="4">' + payment.pymt__Authorization_Id__c + '</td></tr>';
    	} else if(payment.pymt__Payment_Type__c == CommonMessages.ACHPaymentType){
    		content += '<tr><td>Name On Check:</td>' + payment.pymt__Contact__r.Name + '</td></tr>';
    		content += '<tr><td>Check Number:</td><td colspan="4">' + payment.pymt__Check_Number__c + '</td></tr>';
    		content += '<tr><td>Approval #</td><td colspan="4">' + payment.pymt__Authorization_Id__c + '</td></tr>';
    	}
    	content += '<tr><td><b>Terms and Conditions</b></td><td colspan = "4">See your advertising order for information related to Berry\'s Terms & Conditions</td></tr>';
    	content += '<tr><td colspan = "5"><b><u>How To Reach Us</u></b></td></tr>';
    	content += '<tr><td><b>Customer Service</b></td><td colspan = "4"><p style="color:red;">1-877-877-0475</p> 8:00AM to 5:00PM EST, Monday through Friday</td></tr>';
    	content += '<tr><td></td><td colspan = "4">Email: customer.relations@theberrycompany.com</td></tr>';
    	content += '<tr><td><b>Online</b></td><td colspan = "4">Website: www.BerryMeansBusiness.com</td></tr>';
    	content += '<tr><td><h1 style="font-family: Brush Script MT;font-style: italic;">Thank you for your business!</h1></td></tr>';
    	content += '</table></html>';
    	return content;
    }
    
}