@IsTest (SeeAllData=true)
public class InternerBundleAppearancesTest{
    public static testmethod void InternetBundleTest(){
        
        Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        //insert objCanvass;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account1';
        objAccount.Phone = '(988) 239-0999';
        objAccount.Primary_Canvass__c = objCanvass.Id;
        
        insert objAccount;
        
        Contact objContact = TestMethodsUtility.createContact(objAccount);
        insert objContact;
        Opportunity objOpportunity = TestMethodsUtility.createOpportunity(objAccount,objContact);
        insert objOpportunity;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print Listing Content';
        objProd.Print_Product_Type__c = 'Listing';
        objProd.Print_Specialty_Product_Type__c = 'Internet Bundle One Website';
        objProd.ProductCode = 'IBUN';
        
        insert objProd;
        
         Directory__c objDirect = new Directory__c();
            objDirect.Name = 'Wind Telco Test12';
            objDirect.Canvass__c = objCanvass .id;
            objDirect.Directory_Code__c = '103370';
            insert objDirect;
        
         Directory_Section__c objDirSec = new Directory_Section__c();
            objDirSec.Name = 'Test Dir Section';
            objDirSec.Section_Page_Type__c = 'YP';
            objDirSec.Directory__c = objDirect.Id;
            objDirSec.Section_Code__c = '103370';
            insert objDirSec;      
        
        Directory_Heading__c objDirHeading = new Directory_Heading__c();
            objDirHeading.Name = 'Test Dir Heading';
             
            insert objDirHeading;
            
        Section_Heading_Mapping__c objSH = new Section_Heading_Mapping__c();
            objSH.Name = 'Test SH name';
            objSH.Directory_Section__c = objDirSec.Id;
            objSH.Directory_Heading__c = objDirHeading.Id;
            
            insert objSH;
        
        User u= [Select id, phone, Email from user where isActive=true and phone!=null and Email!=null limit 1];
        
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;
        
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id);
        insert objOrderGroup ;
        
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Contact__c=objContact.id, Canvass__c=objCanvass.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,Directory_Section__c=objDirSec.Id,Directory_Heading__c=objDirHeading.Id,Media_Type__c = 'Print',Product_Is_IBUN_Bundle_Product__c= true);
        //Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Account_Manager__c=u.id, Billing_Contact__c=objContact.id, Canvass__c=objCanvass.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,Directory_Section__c=objDirSec.Id,Directory_Heading__c=objDirHeading.Id,Media_Type__c = 'Print',Product_Is_IBUN_Bundle_Product__c= true,isCanceled__c=true);
        list<order_line_items__c> lstOli = new list<order_line_items__c>();
        lstOli.add(objOrderLineItem);
        //lstOli.add(objOrderLineItem1);
        insert lstOli ;
        
        map<Id, Order_Line_Items__c> oldMap = new map<Id, Order_Line_Items__c>();
        oldmap.put(lstOli[0].id,lstOli[0]);
        
        Test.startTest();
        InternetBundleAppearances.IncreaseDecreaseIBAppearancesNew(lstOli,oldmap);
        
         Test.stopTest();
    }
    public static testmethod void InternetBundleTes1(){
        
        Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        //insert objCanvass;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account1';
        objAccount.Phone = '(988) 239-0999';
        objAccount.Primary_Canvass__c = objCanvass.Id;
        
        insert objAccount;
        
        Contact objContact = TestMethodsUtility.createContact(objAccount);
        insert objContact;
        Opportunity objOpportunity = TestMethodsUtility.createOpportunity(objAccount,objContact);
        insert objOpportunity;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print Listing Content';
        objProd.Print_Product_Type__c = 'Listing';
        objProd.Print_Specialty_Product_Type__c = 'Internet Bundle One Website';
        objProd.ProductCode = 'IBUN';
        
        insert objProd;
        
         Directory__c objDirect = new Directory__c();
            objDirect.Name = 'Wind Telco Test12';
            objDirect.Canvass__c = objCanvass .id;
            objDirect.Directory_Code__c = '103370';
            insert objDirect;
        
         Directory_Section__c objDirSec = new Directory_Section__c();
            objDirSec.Name = 'Test Dir Section';
            objDirSec.Section_Page_Type__c = 'YP';
            objDirSec.Directory__c = objDirect.Id;
            objDirSec.Section_Code__c = '103370';
            insert objDirSec;      
        
        Directory_Heading__c objDirHeading = new Directory_Heading__c();
            objDirHeading.Name = 'Test Dir Heading';
             
            insert objDirHeading;
            
        Section_Heading_Mapping__c objSH = new Section_Heading_Mapping__c();
            objSH.Name = 'Test SH name';
            objSH.Directory_Section__c = objDirSec.Id;
            objSH.Directory_Heading__c = objDirHeading.Id;
            
            insert objSH;
        
        User u= [Select id, phone, Email from user where isActive=true and phone!=null and Email!=null limit 1];
        
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;
        
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id);
        insert objOrderGroup ;
        
        //Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Account_Manager__c=u.id, Billing_Contact__c=objContact.id, Canvass__c=objCanvass.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,Directory_Section__c=objDirSec.Id,Directory_Heading__c=objDirHeading.Id,Media_Type__c = 'Print',Product_Is_IBUN_Bundle_Product__c= true);
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Billing_Contact__c=objContact.id, Canvass__c=objCanvass.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,Directory_Section__c=objDirSec.Id,Directory_Heading__c=objDirHeading.Id,Media_Type__c = 'Print',Product_Is_IBUN_Bundle_Product__c= true,isCanceled__c=true);
        list<order_line_items__c> lstOli = new list<order_line_items__c>();
        //lstOli.add(objOrderLineItem);
        lstOli.add(objOrderLineItem1);
        insert lstOli ;
       // map<Id, Order_Line_Items__c> oldMap = new map<Id, Order_Line_Items__c>();
        //oldmap.put(objOrderLineItem.id,objOrderLineItem);
        
        Test.startTest();
        InternetBundleAppearances.IncreaseDecreaseIBAppearancesNew(lstOli,null);
        
         Test.stopTest();
    }
}