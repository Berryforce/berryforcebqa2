@isTest(SeeAllData=true)
public class SubsequentReconcilationReportTest{

public static testMethod void testCommonLines(){
        
        Test.StartTest();
        PageReference pageRef = Page.SubsequentReconciliationReport;
        Test.setCurrentPage(pageRef);
        SubsequentReconcilationReport sRR=new SubsequentReconcilationReport();
        sRR.rtype='Digital';
        sRR.sdate=Date.today().adddays(-180).format();
        sRR.edate=Date.today().format();
        sRR.getReporttypeOptions();
        sRR.setReporttype('Digital');
        sRR.getReporttype();
        sRR.generateReport();
        sRR.ExportReport();
        
        
        PageReference pageRefNew = Page.SubsequentReconciliationReport;
        Test.setCurrentPage(pageRefNew);
        
        SubsequentReconcilationReport sRRNew=new SubsequentReconcilationReport();
        Apexpages.currentpage().getparameters().put('rtype','Print');
        Apexpages.currentpage().getparameters().put('sdate',Date.today().adddays(-180).format());
        Apexpages.currentpage().getparameters().put('edate',Date.today().format());
        sRRNew.rtype='Print';
        sRRNew.sdate=Date.today().adddays(-180).format();
        sRRNew.edate=Date.today().format();
        sRRNew.generateReport();
        sRRNew.ExportReport();
        sRRNew.generateSubsequentReport('Print',Date.today().adddays(-180).format(),Date.today().format());
        List<SubsequentReconcilationReport.MonthlyToSingleWrap> ListM2SWrap=new List<SubsequentReconcilationReport.MonthlyToSingleWrap>();
        ListM2SWrap.add(new SubsequentReconcilationReport.MonthlyToSingleWrap('OLI-1234','ABCD','TEST',200,1,11,12,1,0,200,'Monthly','Single',200,200,new Order_Line_Items__c()));
        
        List<SubsequentReconcilationReport.SingleToMonthlyWrap> ListS2MWrap=new List<SubsequentReconcilationReport.SingleToMonthlyWrap>();
        ListS2MWrap.add(new SubsequentReconcilationReport.SingleToMonthlyWrap('OLI-1234','ABCD','TEST',200,1,11,12,1,0,200,'Monthly','Single',200,200,new Order_Line_Items__c()));
        Test.StopTest();
}

}