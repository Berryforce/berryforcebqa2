global class ScheduleDummyObjRecordDeletionBatchHndlr implements ScheduleDummyObjRecordDeletionBatch.ScheduleDummyObjRecordDeletionBatchInterface {
     global void execute(SchedulableContext sc) {
        DummyObjRecordDeletionBatch   obj = new DummyObjRecordDeletionBatch();
        Database.executeBatch(obj);
    }   
}