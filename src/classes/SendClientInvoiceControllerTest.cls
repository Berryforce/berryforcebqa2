@isTest(seeAllData=true)
public with sharing class SendClientInvoiceControllerTest {
    public static testMethod void sendClInvTest() {
        Test.StartTest();
        Account pubAcct = TestMethodsUtility.generatePublicationAccount();
        insert pubAcct;
        Contact cnt = TestMethodsUtility.createContact(pubAcct.Id);
        Directory__c dir = TestMethodsUtility.generateDirectory();
        dir.Publication_Company__c = pubAcct.Id;
        insert dir;
        Directory_Edition__c DE = TestMethodsUtility.generateDirectoryEdition(dir);
        DE.Pub_Date__c = system.today();
        insert DE;
        Order__c ord = TestMethodsUtility.createOrder(pubAcct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(pubAcct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(pubAcct, ord, oppty);
        og.selected__c = true;
        og.InvoiceGenerationStatus__c = false;
        insert og;
        NationalInvoice__c natInv = TestMethodsUtility.generateNationalInvoice(og);
        natInv.Directory__c = dir.Id;
        natInv.Directory_Edition__c = DE.Id;
        natInv.Publication_Company__c = pubAcct.Id;
        insert natInv;
        DE = [SELECT Id, Edition_Code__c, Directory__c, Directory__r.Directory_Code__c, Directory__r.Name FROM Directory_Edition__c WHERE Id = : DE.Id];
        National_Billing_Status__c natBillStatus = TestMethodsUtility.createNatBillStatus(pubAcct.Id, DE.Id);  
        SendClientInvoiceController SCIC = new SendClientInvoiceController();
        SCIC.pubCode = pubAcct.Publication_Code__c;
        SCIC.editionCode = DE.Edition_Code__c;
        SCIC.dirId = dir.Id;
        SCIC.fetchDirs();
        SCIC.calculateTotalForSendInv();
        SCIC.closePopup();
        SCIC.showNatInv();
        SCIC.sendToElite();
        SCIC.updateStatusTable();
        Test.StopTest();
    }
}