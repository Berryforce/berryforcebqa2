/*************************************************************************
Wrapper class to form serialized JSON data for creating package id records 
Created By: Reddy(pyellanki2@csc.com)
Updated Date: 10/08/2014
$Id$
**************************************************************************/
public class TalusPackageIdWrapper {

    public class Objects {
        public String description;
        public String end_date;
        public Integer id;
        public String name;
        public String resource_uri;
        public String sku;
        public String start_date;
        public String status;
    }

    public Meta meta;
    public List < Objects > objects;

    public class Meta {
        //public Integer limit;
        public Object next;
        public Integer offset;
        public Object previous;
        public Integer total_count;
    }

}