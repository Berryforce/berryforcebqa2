@IsTest(SeeAllData=True)
public class DirectoryProductMappingSOQLMethodsTest {
    @isTest static void DirectoryProductMappingSOQLMethodsCoverage() {
        User u = TestMethodsUtility.generateStandardUser();
        System.runAs(u) {
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('telco'));
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount.add(TestMethodsUtility.generateAccount('publication'));
            insert lstAccount;  
            Account newAccount = new Account();
            Account newPubAccount = new Account();
            Account newTelcoAccount = new Account();
            for(Account iterator : lstAccount) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                    newAccount = iterator;
                }
                else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                    newPubAccount = iterator;
                }
                else {
                    newTelcoAccount = iterator;
                }
            }
            system.assertNotEquals(newAccount.ID, null);
            system.assertNotEquals(newPubAccount.ID, null);
            system.assertNotEquals(newAccount.Primary_Canvass__c, null);
            system.assertNotEquals(newTelcoAccount.ID, null);
            Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
            objTelco.Telco_Code__c = 'Test';
            update objTelco;
            system.assertNotEquals(newTelcoAccount.ID, null);
            Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
            Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
            Division__c objDiv = TestMethodsUtility.createDivision();
            Directory__c objDir = TestMethodsUtility.generateDirectory();
            objDir.Telco_Provider__c = objTelco.Id;
            objDir.Canvass__c = newAccount.Primary_Canvass__c;        
            objDir.Publication_Company__c = newPubAccount.Id;
            objDir.Division__c = objDiv.Id;
            insert objDir;
            
            Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
            Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
            insert objSHM;
            
            Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
            objDirEd.Pub_Date__c=System.today().addMOnths(1);
            insert objDirEd;
            
            Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
            objDirEd1.Pub_Date__c=System.today().addMOnths(2);
            insert objDirEd1;
            
            Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
            objDM.Telco__c = objDir.Telco_Provider__c;
            objDM.Canvass__c = objDir.Canvass__c;
            objDM.Directory__c = objDir.Id;
            insert objDM;
            
            system.assertNotEquals(objDir.ID, null);
            list<Product2> lstProduct = new list<Product2>();
            set<String> setUDAC = new set<String>();   
            for(Integer x=0; x<3;x++){
                Product2 newProduct = TestMethodsUtility.generateproduct();
                newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
                newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
                lstProduct.add(newProduct);
            }
            insert lstProduct;
            
            list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
            list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
            
            for(Product2 iterator : lstProduct) {
                Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
                objDPM.Product2__c = iterator.Id;
                setUDAC.add(iterator.ProductCode);
                lstDPM.add(objDPM);
            }
            insert lstDPM;
            system.assertEquals(lstDPM.size(), 3);
            Test.startTest();            
            system.assertNotEquals(null, lstDPM);
            set<Id> setProdID = new set<Id>();
            set<Id> setDirID = new set<Id>();
            String dirCode = objDir.Directory_Code__c;
            set<String> setStrCombo = new set<String>();
            for(Directory_Product_Mapping__c iterator : lstDPM) {
                setProdID.add(iterator.Product2__c);
                system.assertNotEquals(iterator.Directory__c, null);
                setDirID.add(iterator.Directory__c);
                setStrCombo.add(String.valueOf(iterator.Directory__c).substring(0, 15) +''+ String.valueOf(iterator.Product2__c).substring(0, 15));
            }
            DirectoryProductMappingSOQLMethods.getDirProdMapByProdId(setProdID);
            system.assertNotEquals(dirCode, null);
            DirectoryProductMappingSOQLMethods.getDirProdMapByDirCode(dirCode);
            DirectoryProductMappingSOQLMethods.getDirProdMapByProdIdAndDirId(setProdID, setDirID);
            DirectoryProductMappingSOQLMethods.getDirProdMapByUdacAndDirId(setUDAC, setDirID);
            DirectoryProductMappingSOQLMethods.fetchDPMByDirectoryId(setDirID);
            Test.stopTest();
        }
    }
}