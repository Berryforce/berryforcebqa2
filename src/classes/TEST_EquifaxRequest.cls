/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_EquifaxRequest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Vertex_Berry.Bsure_commonUtilityFunctioncall.customerinfobean objCustInfo=new Vertex_Berry.Bsure_commonUtilityFunctioncall.customerinfobean();
        objCustInfo.strAddressLine1='30 PARK AVE STE 4';
        objCustInfo.strCity='NEW YORK';
        objCustInfo.strState='NY';
        objCustInfo.strPostalCode='100163801';
        objCustInfo.strBusinessName='BOGHEN PHARMACY';
        
        Vertex_Berry.Bsure_commonUtilityFunctioncall.ConsumerInfoBean objConsumerInfo = new Vertex_Berry.Bsure_commonUtilityFunctioncall.ConsumerInfoBean();
        objConsumerInfo.ConSubjectSSN='666131447';
        objConsumerInfo.ConSubjectBirthDate='11/01/1972';
        objConsumerInfo.ConFirstName='TINA MICHELL';
        objConsumerInfo.ConLastName='FXNNXH';
        objConsumerInfo.ConStreetName='OLD BARN';
        objConsumerInfo.ConCity='THURMONT';
        objConsumerInfo.ConState='MD';
        objConsumerInfo.ConPostalCode='21788';
        
        List<Vertex_Berry__BSure_Configuration_Settings__c> lstSett=new List<Vertex_Berry__BSure_Configuration_Settings__c>();
        Vertex_Berry__BSure_Configuration_Settings__c objsett=new Vertex_Berry__BSure_Configuration_Settings__c();
        objsett.Name='BSureC_EquifaxMemberId';
        objsett.Vertex_Berry__Parameter_Key__c='BSureC_EquifaxMemberId';
        objsett.Vertex_Berry__Parameter_Value__c='BSureC_EquifaxMemberId';
        lstSett.add(objsett);
        Vertex_Berry__BSure_Configuration_Settings__c objsett2=new Vertex_Berry__BSure_Configuration_Settings__c();
        objsett2.Name='EquifaxCommercialURL';
        objsett2.Vertex_Berry__Parameter_Key__c='EquifaxCommercialURL';
        objsett2.Vertex_Berry__Parameter_Value__c='EquifaxCommercialURL';
        lstSett.add(objsett2);
        Vertex_Berry__BSure_Configuration_Settings__c objsett3=new Vertex_Berry__BSure_Configuration_Settings__c();
        objsett3.Name='EquifaxConsumerURL';
        objsett3.Vertex_Berry__Parameter_Key__c='EquifaxConsumerURL';
        objsett3.Vertex_Berry__Parameter_Value__c='EquifaxConsumerURL';
        lstSett.add(objsett3);
        insert lstSett;
        
        EquifaxRequest.getConfigurationValues(objsett3.Name);
        
        List<Vertex_Berry__BsureEquifax__c> lstEq=new List<Vertex_Berry__BsureEquifax__c>();
        Vertex_Berry__BsureEquifax__c objEqu=new Vertex_Berry__BsureEquifax__c();
        objEqu.Name='AnnualSalesRange';
        objEqu.Vertex_Berry__AliasName__c='BCS';
        objEqu.Vertex_Berry__NodeName__c='Business Delinquency Score';
        objEqu.Vertex_Berry__ReportCode__c='SCR0063';
        objEqu.Vertex_Berry__ReportName__c='Business Delinquency Score';
        objEqu.Vertex_Berry__RequestType__c='Commercial';
        lstEq.add(objEqu);
        
        System.assertEquals(objEqu.Name, 'AnnualSalesRange');
        Vertex_Berry__BsureEquifax__c objEqui2=new Vertex_Berry__BsureEquifax__c();
        objEqui2.Name='Business Delinquency Score';
        objEqui2.Vertex_Berry__AliasName__c='AnnualSalesRange';
        objEqui2.Vertex_Berry__NodeName__c='AnnualSalesRange';
        objEqui2.Vertex_Berry__RequestType__c='Commercial';
        lstEq.add(objEqui2);
        Vertex_Berry__BsureEquifax__c objEqui3=new Vertex_Berry__BsureEquifax__c();
        objEqui3.Name='EfxId';
        objEqui3.Vertex_Berry__AliasName__c='EfxId';
        objEqui3.Vertex_Berry__NodeName__c='EfxId';
        objEqui3.Vertex_Berry__ReportCode__c='RPT0016';
        objEqui3.Vertex_Berry__ReportName__c='BCIR Plus';
        objEqui3.Vertex_Berry__RequestType__c='Commercial';
        lstEq.add(objEqui3);
        EquifaxRequest.getConfigurationValues(objsett2.Name);
        /*Bsure_Equifax__c objEqui34=new Bsure_Equifax__c();
        objEqui34.Name='AlertCode';
        objEqui34.AliasName__c='AlertCode';
        objEqui34.NodeName__c='AlertCode';
        objEqui34.ReportCode__c='RPT0016';
        objEqui34.ReportName__c='BCIR Plus';
        objEqui34.RequestType__c='Commercial';
        lstEq.add(objEqui34); */
        Vertex_Berry__BsureEquifax__c objEqui4=new Vertex_Berry__BsureEquifax__c();
        objEqui4.Name='AlertCode';
        objEqui4.Vertex_Berry__AliasName__c='AlertCode';
        objEqui4.Vertex_Berry__NodeName__c='AlertCode';
        objEqui4.Vertex_Berry__ReportCode__c='RPT0016';
        objEqui4.Vertex_Berry__ReportName__c='BCIR Plus';
        objEqui4.Vertex_Berry__RequestType__c='Commercial';
        lstEq.add(objEqui4);
        
        Vertex_Berry__BsureEquifax__c objEqui5=new Vertex_Berry__BsureEquifax__c();
        objEqui5.Name='ScoreData';
        objEqui5.Vertex_Berry__AliasName__c='ScoreData';
        objEqui5.Vertex_Berry__NodeName__c='ScoreData';
        objEqui5.Vertex_Berry__ReportCode__c='RPT0016';
        objEqui5.Vertex_Berry__ReportName__c='BCIR Plus';
        objEqui5.Vertex_Berry__RequestType__c='Commercial';
        lstEq.add(objEqui5);
        
        Vertex_Berry__BsureEquifax__c objEqui6=new Vertex_Berry__BsureEquifax__c();
        objEqui6.Name='AlertDescription';
        objEqui6.Vertex_Berry__AliasName__c='AlertDescription';
        objEqui6.Vertex_Berry__NodeName__c='AlertDescription';
        objEqui6.Vertex_Berry__ReportCode__c='RPT0016';
        objEqui6.Vertex_Berry__ReportName__c='BCIR Plus';
        objEqui6.Vertex_Berry__RequestType__c='Commercial';
        lstEq.add(objEqui6);
        
        Vertex_Berry__BsureEquifax__c objEqui7=new Vertex_Berry__BsureEquifax__c();
        objEqui7.Name='ScoreAttribute';
        objEqui7.Vertex_Berry__AliasName__c='ScoreAttribute';
        objEqui7.Vertex_Berry__NodeName__c='ScoreAttribute';
        objEqui7.Vertex_Berry__ReportCode__c='RPT0016';
        objEqui7.Vertex_Berry__ReportName__c='BCIR Plus';
        objEqui7.Vertex_Berry__RequestType__c='Commercial';
        lstEq.add(objEqui7);
        
        insert lstEq;
        
		String testResponse=' <EfxTransmit customerReference="HTTP Test" efxInternalTranId="0145052685" serviceCode="SB1" version="5.0"> ' +
            ' <ProductCode name="COMM" code="0001">Commercial - Hit</ProductCode> ' +
            ' <ProductCode name="RPT" code="0016">Business Credit Industry Report Plus</ProductCode> ' +
            ' <ProductCode name="SCR" code="0064">Business Failure Score</ProductCode> ' +
            ' <ProductCode name="SCR" code="0063">Business Delinquency Score</ProductCode> ' +
            ' <ProductCode name="SCR" code="0069">Suggested Credit Limit, v2.0</ProductCode> ' +
            ' <CustomerSecurityInfo> ' +
            ' <ProductCode name="SCR" code="0064">Business Failure Score</ProductCode> ' +
            ' <ProductCode name="SCR" code="0063">Business Delinquency Score</ProductCode> ' +
            ' <ProductCode name="SCR" code="0069">Suggested Credit Limit</ProductCode> ' +
            ' <ProductCode name="RPT" code="0016">Business Credit Industry Report Plus</ProductCode> ' +
            ' </CustomerSecurityInfo> ' +
            ' <CommercialCreditReport> ' +
            ' <Header> ' +
            ' <CustomerNumber>999PL00087</CustomerNumber> ' +
            ' <DateOfRequest>11/13/2013</DateOfRequest> ' +
            ' </Header> ' +
            ' <Folder> ' +
            ' <EfxId>500000196</EfxId> ' +
            ' <FolderActivity> ' +
            ' <FileActivityDate>07/24/2012</FileActivityDate> ' +
            ' <FileCreationDate>06/11/2002</FileCreationDate> ' +
            ' </FolderActivity> ' +
            ' <Alert> ' +
            ' <AlertCode>500</AlertCode> ' +
            ' <AlertDescription>Insufficient Data Available to Calculate Credit Utilization</AlertDescription> ' +
            ' </Alert> ' +
            ' <ReportAttributes> ' +
            ' <RecentSinceDate>08/01/2013</RecentSinceDate> ' +
            ' <AsOfDate3Mo>08/01/2013</AsOfDate3Mo> ' +
            ' <AsOfDate24Mo>11/01/2011</AsOfDate24Mo> ' +
            ' <AsOfDateToday>11/13/2013</AsOfDateToday> ' +
            ' <FinancialSummary> ' +
            ' <SummaryAttributes> ' +
            ' <NewInquiries>0</NewInquiries> ' +
            ' <CreditActiveSince>08/15/1981</CreditActiveSince> ' +
            ' </SummaryAttributes> ' +
            ' </FinancialSummary> ' +
            ' <NonFinancialSummary> ' +
            ' </NonFinancialSummary> ' +
            ' <PaymentIndexInfo> ' +
            ' <PaymentIndexBusiness>1</PaymentIndexBusiness> ' +
            ' <PaymentIndexIndustry>92</PaymentIndexIndustry> ' +
            ' </PaymentIndexInfo> ' +
            ' </ReportAttributes> ' +
            ' <DecisionTools> ' +
            ' <ScoreData scoreName="Business Failure Score" score="1342" riskLevel="2" seqno="" scoreType="COMMERCIAL" riskDescription=""> ' +
            ' <ScoreAttribute Name="NationalPercentile">39</ScoreAttribute> ' +
            ' <ScoreAttribute Name="RateInClass">1.9</ScoreAttribute> ' +
            ' <ScoreAttribute Name="NationalAve">4.6</ScoreAttribute> ' +
            ' </ScoreData> ' +
            ' <ScoreData scoreName="Business Delinquency Score" score="391" riskLevel="4" seqno="" scoreType="COMMERCIAL" riskDescription=""> ' +
            ' </ScoreData> ' +
            ' </DecisionTools> ' +
            ' </Folder> ' +
            ' </CommercialCreditReport> ' +
            ' </EfxTransmit> ' ;
            XmlStreamReader reader=new XmlStreamReader(testResponse);
            //system.debug(reader);
            
            
            
            map<String,String> mapAlias=new map<string,string>();
             for(Vertex_Berry__BsureEquifax__c objBsureEq:Vertex_Berry__BsureEquifax__c.getAll().values())
             {
                if(objBsureEq!=null && objBsureEq.Vertex_Berry__RequestType__c!=null && objBsureEq.Vertex_Berry__RequestType__c!='' && objBsureEq.Vertex_Berry__RequestType__c=='Commercial')
                {
                    if(objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__AliasName__c!=null && objBsureEq.Vertex_Berry__AliasName__c!='')
                    {
                        mapAlias.put(objBsureEq.Vertex_Berry__NodeName__c,objBsureEq.Vertex_Berry__AliasName__c);
                    }
                }
             }
             
            EquifaxRequest objEqui=new EquifaxRequest();
            objEqui.BSureC_EquifaxURL = 'EquifaxConsumerURL';
            //system.debug('mapAlias==='+mapAlias.keyset());
            objEqui.ConSubjectSSN1='666131447';
			objEqui.ConSubjectBirthDate1='11/01/1972';
			objEqui.ConFirstName1='TINA MICHELL';
			objEqui.ConLastName1='FXNNXH';
			objEqui.ConStreetName1='OLD BARN';
			objEqui.ConCity1='THURMONT';
			objEqui.ConState1='MD';
			objEqui.ConPostalCode1='21788'; 
			
			
			objEqui.strAddressLine11 = '21820 W RIVIERA CT';
			objEqui.strCity1 = 'MUNDELEIN';
			objEqui.strState1 = 'IL';
			objEqui.strPostalCode1 = '600605328';
			objEqui.strBusinessName1 = 'SHORELINE BUILDERS';
			
			objEqui.Reqresultconsumer ='test request';
			objEqui.Resresultconsumer='test request';
			objEqui.ReqresultCommercial='test request';
			objEqui.ResresultCommercial='test request';
			//objEqui.click();
            objEqui.parseBooks(reader, mapAlias);
            try{
                objEqui.getEquifaxCommercialReport(objCustInfo,objConsumerInfo);
                
            }catch(Exception e)
            {}
            try{
                map<string,string> mapConsumerAlias=new map<string,string>();
                for(Vertex_Berry__BsureEquifax__c objBsureEq:Vertex_Berry__BsureEquifax__c.getAll().values())
                 {
                    if(objBsureEq!=null && objBsureEq.Vertex_Berry__RequestType__c!=null && objBsureEq.Vertex_Berry__RequestType__c!='' && objBsureEq.Vertex_Berry__RequestType__c=='Consumer')
                    {
                        if(objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__AliasName__c!=null && objBsureEq.Vertex_Berry__AliasName__c!='')
                            {
                                mapConsumerAlias.put(objBsureEq.Vertex_Berry__NodeName__c,objBsureEq.Vertex_Berry__AliasName__c);
                            }
                    }
                    if(objBsureEq!=null && objBsureEq.Vertex_Berry__RequestType__c!=null && objBsureEq.Vertex_Berry__RequestType__c!='' && objBsureEq.Vertex_Berry__RequestType__c=='Commercial')
                    {
                        if(objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__NodeName__c!='' && objBsureEq.Vertex_Berry__AliasName__c!=null && objBsureEq.Vertex_Berry__AliasName__c!='')
                            {
                                mapConsumerAlias.put(objBsureEq.Vertex_Berry__NodeName__c,objBsureEq.Vertex_Berry__AliasName__c);
                            }
                    }
                 }
                 map<string,string> mapResultBook=new map<string,string>();
                objEqui.getEquifaxConsumerReport(objConsumerInfo,mapConsumerAlias,mapResultBook);
                objEqui.getProductCodeTags(mapResultBook);
                
            }catch(Exception e)
            {
            EquifaxRequest.notifyDevelopersOf(e);
            }
		
    }
}