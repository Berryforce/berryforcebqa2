@IsTest (SeeAllData=true)
public class DFFMultiTestClass {
    static testmethod void dffMultiUndoMultiTest(){
        //Test.startTest();
        /*Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        //insert objCanvass;
        Account objAccount = TestMethodsUtility.createAccount(objCanvass);
        insert objAccount;
        Contact objContact = TestMethodsUtility.createContact(objAccount);
        insert objContact;
        Opportunity objOpportunity = TestMethodsUtility.createOpportunity(objAccount,objContact);
        insert objOpportunity;
        
        Product2 objProd = new Product2();
            objProd.Name = 'Test';
            objProd.Product_Type__c = 'Print';
            objProd.ProductCode = 'WLCSH';
            
            insert objProd;
        
        Product2 objProd1 = new Product2();
            objProd1.Name = 'Test1';
            objProd1.Product_Type__c = 'Print';
            objProd1.ProductCode = 'WLCSH';
            
            insert objProd1;
            
        User u= [Select id, phone, Email from user where isActive=true and phone!=null and Email!=null limit 1];
        
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;
    
    
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id);
        insert objOrderGroup ;
    
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Account_Manager__c=u.id, Billing_Contact__c=objContact.id, Canvass__c=objCanvass.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id);
        insert objOrderLineItem ;
        
         Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Account_Manager__c=u.id, Billing_Contact__c=objContact.id, Canvass__c=objCanvass.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd1.Id);
         insert objOrderLineItem1 ;
        */
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id); 
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;

        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;

        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.Pub_Date__c=System.today().addMOnths(1);
        insert objDirEd;

        Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd1.Pub_Date__c=System.today().addMOnths(2);
        insert objDirEd1;

        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;

        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        lstProduct.add(newProduct);
        }
        insert lstProduct;

        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        for(Product2 iterator : lstProduct) {
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDPM.Product2__c = iterator.Id;
        }
        insert lstDPM;

        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        insert newOpportunity;

        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
        OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
        objOLI.PricebookEntryId = iterator.Id;
        objOLI.Billing_Duration__c = 12;
        objOLI.Directory__c = objDir.Id;
        objOLI.Directory_Edition__c = objDirEd.Id;
        objOLI.Full_Rate__c = 30.00;
        objOLI.UnitPrice = 30.00;
        objOLI.Package_ID__c = '123456';
        objOLI.Billing_Partner__c = objOLI.Id;
        objOLI.OpportunityId = newOpportunity.Id;
        objOLI.Directory_Heading__c = objDH.Id;
        objOLI.Directory_Section__c = objDS.Id;
        lstOLI.add(objOLI);
        mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;

        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        for(OpportunityLineItem iterator : lstOLI) {
        Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
        objOLI.Package_ID__c = iterator.Package_ID__c;
        objOLI.Package_Item_Quantity__c = 2;
        objOLI.Directory__c = iterator.Directory__c;
        objOLI.Directory_Edition__c = iterator.Directory_Edition__c;
        objOLI.UnitPrice__c = iterator.UnitPrice;
        objOLI.Opportunity_line_Item_id__c = iterator.Id;
        objOLI.Media_Type__c = CommonMessages.oliPrintProductType;
        objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
        objOLI.Directory_Section__c = iterator.Directory_Section__c;
        objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
        objOLI.Product_Inventory_Tracking_Group__c = 'YP Leader Ad';
        lstOrderLI.add(objOLI);
        }

        Test.startTest();
        insert lstOrderLI;
        Order_Line_Items__c objOrderLineItem = lstOrderLI[0];
        Order_Line_Items__c objOrderLineItem1 = lstOrderLI[1];
        
        RecordType rtId = CommonMethods.getRecordTypeDetailsByName('Digital_Product_Requirement__c','Print Graphic');
        ID lockRT = CommonMethods.getRedordTypeIdByName(CommonMessages.dffLockRT, CommonMessages.dffObjectName);
        list<Digital_Product_Requirement__c> objDFFLst = new list<Digital_Product_Requirement__c>();
        
        Digital_Product_Requirement__c objDFF = new Digital_Product_Requirement__c();
        objDFF.Account__c = newAccount.Id;
        objDFF.URN_number__c = 4;
        objDFF.OrderLineItemID__c = objOrderLineItem.Id;
        //objDFF.UDAC__c  = 'WRL';
        objDFF.URN_text__c = '12';
        objDFF.Forenames__c = 'Test 1';
        objDFF.Surname__c = 'Test SurName 1';
        //objDFF.cust_area_code__c = '9998881112';
        objDFF.RecordtypeId = rtId.Id ;
        
        try{
        insert objDFF;
        } catch(exception e){}
       
        
        Digital_Product_Requirement__c objDFF1 = new Digital_Product_Requirement__c();
        objDFF1.Account__c = newAccount.Id;
        objDFF1.URN_number__c = 6;
        objDFF.OrderLineItemID__c = objOrderLineItem1.Id;
        objDFF1.Forenames__c = 'Test 2';
        objDFF1.Surname__c = 'Test SurName 2';
        //objDFF1.cust_area_code__c = '9998881114';
        objDFF1.Is_Multied__c = false;
        objDFF1.RecordtypeId = rtId.Id ;
        
        objDFFLst.add(objDFF1);
        //insert objDFF1;
        
        Digital_Product_Requirement__c objDFF2 = new Digital_Product_Requirement__c();
        objDFF2.Account__c = newAccount.Id;
        objDFF2.URN_number__c = 4;
        objDFF.OrderLineItemID__c = objOrderLineItem.Id;
        objDFF2.Forenames__c = 'Test 24';
        objDFF2.Surname__c = 'Test SurName 24';
        //objDFF2.cust_area_code__c = '9998881112';
        objDFF2.Is_Multied__c = true;
        objDFF2.RecordtypeId = lockRT;
        
       // insert objDFF2;
        
        objDFFLst.add(objDFF2);
        
        insert objDFFLst; 
        
        ApexPages.StandardController sc = new ApexPages.standardController(new Digital_Product_Requirement__c());
        ApexPages.currentPage().getParameters().put('Id',objDFF.Id);
        
        DFFMulti multidff = new DFFMulti(sc);
        multidff.dffcompleteLst = objDFFLst;
        multidff.SaveSelectedDFF();
       
        Test.stopTest();
    }
}