@isTest
public class ContactSOQLMethodsTest {
    @isTest static void ContactSOQLMethodsCoverage() {
        User u = TestMethodsUtility.generateStandardUser();        
        System.runAs(u) {
            Account objAccount = TestMethodsUtility.createAccount('customer');
            system.assertNotEquals(null, objAccount);
            system.assert(objAccount != null, 'Account ID : ' + objAccount.Id);
            Contact objContact = TestMethodsUtility.createContact(objAccount.Id);           
            system.assertNotEquals(null, objContact);
            system.assert(objContact != null, 'Contact ID : ' + objContact.Id);
            system.assertNotEquals(null, ContactSOQLMethods.getContactByContactID(new set<Id>{objContact.Id}));
            system.assertNotEquals(null, ContactSOQLMethods.getContactByAccountID(new set<Id>{objAccount.Id}));
            system.assertNotEquals(null, ContactSOQLMethods.getContact_PaymentMethodByContactID(new set<Id>{objContact.Id}));
        }
    }
}