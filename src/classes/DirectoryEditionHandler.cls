public class DirectoryEditionHandler {
  public static void onAfterInsert(List<Directory_Edition__c> listDirectoryEdition) {
    createNationalBillingStatus(listDirectoryEdition);
    //createBudgetRecord(listDirectoryEdition);
  }
  
  public static void onBeforeInsert(List<Directory_Edition__c> lstDE) {
	for(Directory_Edition__c objDE : lstDE) {
	  objDE.Telco__c = objDE.DE_Directory_Telco_Provider__c;
	}
  }
  
  public static void onBeforeUpdate(List<Directory_Edition__c> lstDE, map<Id, Directory_Edition__c> oldMap) {
	for(Directory_Edition__c objDE : lstDE) {
	    Directory_Edition__c objOldDE = oldMap.get(objDE.Id);
	    //if(objDE.Book_Status__c == 'NI' && objDE.Telco__c != objOldDE.Telco__c) {
	    	objDE.Telco__c = objDE.DE_Directory_Telco_Provider__c;
	    //}
	}
  }
  
  //For ATP-36367 
  public static void onAfterUpdate(List<Directory_Edition__c> listDirectoryEdition,Map<id,Directory_Edition__c> oldmap) {
        /*list<ID> listDE= new list<ID> ();
        for(Directory_Edition__c objDE:listDirectoryEdition)
        {       
            if(objDE.year__c!= oldmap.get(objDE.id).year__c)
            {           
                listDE.add(objDE.id);       
            }
            else if(objDE.Edition_Code__c!= oldmap.get(objDE.id).Edition_Code__c)
            {
                listDE.add(objDE.id); 
            }else if(objDE.Name!= oldmap.get(objDE.id).Name)
            {
                listDE.add(objDE.id); 
            }
            setValuesForDirectoryPagination(listDE);    
      }*/
  }
  /*public static void setValuesForDirectoryPagination(list<id> listDE)
  {     
    list<Directory_Pagination__c> listDirectoryPagination=[select id,Order_Line_Item__c,
                                                                    Order_Line_Item__r.Directory_Edition__c,
                                                                    Order_Line_Item__r.Directory_Edition__r.year__c,
                                                                    Order_Line_Item__r.Directory_Edition__r.name,
                                                                    Order_Line_Item__r.Directory_Edition__r.Edition_Code__c
                                                                    from Directory_Pagination__c where Order_Line_Item__r.Directory_Edition__c IN :listDE];
    list<Directory_Pagination__c> listUpdateDP=new list<Directory_Pagination__c> ();
    for(Directory_Pagination__c objDP:listDirectoryPagination)
    {       
        objDP.DP_year__c = objDP.Order_Line_Item__r.Directory_Edition__r.year__c;
        objDP.DP_Directory_Edition_Code__c=objDP.Order_Line_Item__r.Directory_Edition__r.Edition_Code__c;
        objDp.DP_Directory_Edition_Name__c=objDP.Order_Line_Item__r.Directory_Edition__r.Name;
        listUpdateDP.add(objDP);        
    } 
    if(listUpdateDP.size() > 0)
    {
        update listUpdateDP;
    }   
  }*/

  private static void createNationalBillingStatus(List<Directory_Edition__c> listDirectoryEdition) {
    List<National_Billing_Status__c> listBillingStatus = new List<National_Billing_Status__c>();
    List<Directory_Edition__c> listDE = new List<Directory_Edition__c>();
    List<Budget__c> LstBudget = new List<Budget__c>();
        
    Set<Id> dirEdtnIds = new Set<Id>();
    
    for(Directory_Edition__c DE : listDirectoryEdition) {
      dirEdtnIds.add(DE.Id);
    }
    
    listDE = DirectoryEditionSOQLMethods.getDEByIds(dirEdtnIds);

    for(Directory_Edition__c DE : listDE) {
      National_Billing_Status__c NBS = new National_Billing_Status__c();
      NBS.Name = DE.Name;
      NBS.Directory_Edition__c = DE.Id;
      NBS.PUBCO__c = DE.Directory__r.Publication_Company__c;
      NBS.TP_Covers_Spines_And_Indexes__c = true;
      listBillingStatus.add(NBS);
      
        Budget__c objBudget = new Budget__c();
        objBudget.Name = DE.Name; 
        objBudget.Directory_Name__c = DE.Directory__c;
        objBudget.Directory_Edition__c = DE.id;
        objBudget.ExternalID__c = DE.Directory_Code__c + DE.Edition_Code__c;
        LstBudget.add(objBudget);
                
    }
    insert listBillingStatus;
    insert LstBudget;
  }
    /*
    private static void createBudgetRecord(List<Directory_Edition__c> listDirectoryEdition) {
        boolean stopReccur = true;
        List<Budget__c> LstBudget = new List<Budget__c>();
        List<Directory_Edition__c> LstDE = new List<Directory_Edition__c>();
        set<Id> DEIds = new Set<Id>();
        
        for(Directory_Edition__c lstIteratorDE : listDirectoryEdition){
            DEIds.add(lstIteratorDE.Id);
        }
        
        LstDE = DirectoryEditionSOQLMethods.getDEByIds(DEIds);
        system.debug('Lst DE with Ids----' + LstDE);
        
        if(stopReccur){
            for(Directory_Edition__c iteratorDE : LstDE){
                Budget__c objBudget = new Budget__c();
                objBudget.Name = iteratorDE.Name; 
                objBudget.Directory_Name__c = iteratorDE.Directory__c;
                objBudget.Directory_Edition__c = iteratorDE.id;
                objBudget.ExternalID__c = iteratorDE.Directory_Code__c + iteratorDE.Edition_Code__c;
                LstBudget.add(objBudget);
                stopReccur = false;
            }
            insert LstBudget;
            system.debug('Inserted List----' + LstBudget);
        }
    }
    */
}