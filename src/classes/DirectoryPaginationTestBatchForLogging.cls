global class DirectoryPaginationTestBatchForLogging implements Database.Batchable<sObject>{
     
    global String strLabel;
    
    global DirectoryPaginationTestBatchForLogging(String strCond){
        strLabel = strCond;
    }
    
    global DirectoryPaginationTestBatchForLogging(){
    }
    global Database.QueryLocator start(Database.BatchableContext bc){        
        String SOQL = 'SELECT id, Caption_Header__c, DP_Caption_Header_F__c, Continuous_Service_Order_Appearance__c,'+
                      'DP_Continuous_Service_Order_Appearance_F__c, Is_Cross_Reference__c, DP_Is_Cross_Reference_F__c, IBUN_Website_One__c, DP_IBUN_Website_One_F__c,'+
                      'IBUN_Website_Two__c, DP_IBUN_Website_Two_F__c, Scoped_Caption_Header__c, DP_Scoped_Caption_Header_F__c, Year__c, DP_Year_F__c, Directory_Edition_Code__c,'+
                      'DP_Directory_Edition_Code_F__c, Directory_Edition_Name__c, DP_Directory_Edition_Name_F__c,Banner__c,DP_Banner_F__c,DP_Billboard_F__c,Billboard__c,Directory_Heading_Code__c,DP_Directory_Heading_Code_F__c,' +
                      'Directory_Heading__c,DP_Directory_Heading_F__c,IBUN_Type__c,DP_IBUN_Type_F__c,Incorrect_Setup_Excluded_from_XML__c,DP_Incorrect_Setup_Excluded_from_XML_F__c,'+
                      'Internet_Bundle_Ad__c,DP_Internet_Bundle_Ad_F__c,UDAC__c,DP_UDAC_F__c,Scoped_Caption_Member__c,DP_Scoped_Caption_Member_F__c,Section_Heading_Mapping_ID__c,DP_Section_Heading_Mapping_ID_F__c,'+
                      'DP_Display_Ad_F__c,Display_Ad__c,DP_Extra_Line_Type_Product_F__c,Extra_Line_Type_Product__c,DP_Coupon_Ad_F__c,Coupon_Ad__c,DP_Scoped_Indent_Order_F__c,Scoped_Indent_Order__c,DP_Listing_F__c,Listing__c,Sold_Caption_Header__c,DP_Sold_Caption_Header_F__c,DP_Sold_Caption_Member_F__c,Sold_Caption_Member__c,DP_Sold_Indent_Order_F__c,Sold_Indent_Order__c,spacead__c,DP_spacead_F__c,DP_TCAP_Trademark_Line_F__c,TCAP_Trademark_Line__c,DP_Trademark_Sorting_NULL_Identifier_F__c,Trademark_Sorting_NULL_Identifier__c, Under_Caption__c,DP_Under_Caption_F__c,DP_Caption_Member_F__c, Caption_Member__c, Anchor__c, DP_Anchor_F__c, DP_Sequence_Padded_F__c, Sequence_Padded__c, DP_Leader_Ad_F__c, Leader_Ad__c, Digital_Product_Requirement__c, Section_Type__c,DP_Section_Type_F__c,Trademark_Finding_Line__c,DP_Trademark_Finding_Line_F__c,Type__c,DP_Type_F__c, Order_Line_Item__r.Digital_Product_Requirement__c FROM Directory_Pagination__c '+strLabel;
       
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Directory_Pagination__c> scope){
        String strErrorMessage1 = '';
        String strErrorMessage2 = '';
        //List<Directory_Pagination__c> lstUpdt = new List<Directory_Pagination__c>();
        id idDP;
        String strErrorMessage = '';
        List<String> lstError = null;
        Boolean bolError = false;
        for(Directory_Pagination__c objDP : scope) {
            bolError = false;
            idDP = objDP.id;
            lstError = new List<String>();
            //Directory_Pagination__c objUpdtDP = new Directory_Pagination__c(id=objDP.id);
            if(objDP.Order_Line_Item__c != null && objDP.Digital_Product_Requirement__c == null) {
                bolError = true;
                //objUpdtDP.Digital_Product_Requirement__c = objDP.Order_Line_Item__r.Digital_Product_Requirement__c;
                lstError.add('Digital_Product_Requirement__c missing ,');
            }
            if(objDP.Caption_Member__c != objDP.DP_Caption_Member_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('Caption_Member__c\t'+objDP.Caption_Member__c+'\t'+objDP.DP_Caption_Member_F__c+',');
            }
            
            if(objDP.Leader_Ad__c != objDP.DP_Leader_Ad_F__c) {
                bolError = true;
                lstError.add('Leader_Ad__c\t'+objDP.Leader_Ad__c+'\t'+objDP.DP_Leader_Ad_F__c+',');
            }
            if(objDP.Anchor__c != objDP.DP_Anchor_F__c) {
                bolError = true;
                lstError.add('Anchor__c\t'+objDP.Caption_Member__c+'\t'+objDP.DP_Anchor_F__c+',');
            }
            if(objDP.Under_Caption__c != objDP.DP_Under_Caption_F__c) {
                bolError = true;
                lstError.add('Under_Caption__c\t'+objDP.Under_Caption__c+'\t'+objDP.DP_Under_Caption_F__c+',');
            }
            if(objDP.Type__c != objDP.DP_Type_F__c) {
                bolError = true;
                lstError.add('Type__c\t'+objDP.Type__c+'\t'+objDP.DP_Type_F__c+',');
            }
            if(objDP.Trademark_Sorting_NULL_Identifier__c != objDP.DP_Trademark_Sorting_NULL_Identifier_F__c) {
                bolError = true;
                lstError.add('Trademark_Sorting_NULL_Identifier__c\t'+objDP.Trademark_Sorting_NULL_Identifier__c+'\t'+objDP.DP_Trademark_Sorting_NULL_Identifier_F__c+',');
            }
            if(objDP.TCAP_Trademark_Line__c != objDP.DP_TCAP_Trademark_Line_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('TCAP_Trademark_Line__c\t'+objDP.TCAP_Trademark_Line__c+'\t'+objDP.DP_TCAP_Trademark_Line_F__c+',');
            }
            if(objDP.spacead__c != objDP.DP_spacead_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('spacead__c\t'+objDP.spacead__c+'\t'+objDP.DP_spacead_F__c+',');
            }
            if(objDP.Sold_Indent_Order__c != objDP.DP_Sold_Indent_Order_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('Sold_Indent_Order__c\t'+objDP.Sold_Indent_Order__c+'\t'+objDP.DP_Sold_Indent_Order_F__c+',');
            }
            if(objDP.Sold_Caption_Member__c != objDP.DP_Sold_Caption_Member_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('Sold_Caption_Member__c\t'+objDP.Sold_Caption_Member__c+'\t'+objDP.DP_Sold_Caption_Member_F__c+',');
            }
            if(objDP.Sold_Caption_Header__c != objDP.DP_Sold_Caption_Header_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('Sold_Caption_Header__c\t'+objDP.Sold_Caption_Header__c+'\t'+objDP.DP_Sold_Caption_Header_F__c+',');
            }
            if(objDP.Sequence_Padded__c != objDP.DP_Sequence_Padded_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('Sequence_Padded__c\t'+objDP.Sequence_Padded__c+'\t'+objDP.DP_Sequence_Padded_F__c+',');
            }
            if(objDP.Listing__c != objDP.DP_Listing_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('Listing__c\t'+objDP.Listing__c+'\t'+objDP.DP_Listing_F__c+',');
            }
            if(objDP.Scoped_Indent_Order__c != objDP.DP_Scoped_Indent_Order_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('Scoped_Indent_Order__c\t'+objDP.Scoped_Indent_Order__c+'\t'+objDP.DP_Scoped_Indent_Order_F__c+',');
            }
            if(objDP.Coupon_Ad__c != objDP.DP_Coupon_Ad_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('Coupon_Ad__c\t'+objDP.Coupon_Ad__c+'\t'+objDP.DP_Coupon_Ad_F__c+',');
            }
            if(objDP.Extra_Line_Type_Product__c != objDP.DP_Extra_Line_Type_Product_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('Extra_Line_Type_Product__c\t'+objDP.Extra_Line_Type_Product__c+'\t'+objDP.DP_Extra_Line_Type_Product_F__c+',');
            }
            if(objDP.Display_Ad__c != objDP.DP_Display_Ad_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Member__c = objDP.DP_Caption_Member_F__c;
                lstError.add('Display_Ad__c\t'+objDP.Display_Ad__c+'\t'+objDP.DP_Display_Ad_F__c+',');
            }
            if(objDP.Caption_Header__c != objDP.DP_Caption_Header_F__c) {
                bolError = true;
                //objUpdtDP.Caption_Header__c = objDP.DP_Caption_Header_F__c;
                lstError.add('Caption_Header__c\t'+objDP.Caption_Header__c +'\t'+objDP.DP_Caption_Header_F__c+',');
            }
            if(objDP.Continuous_Service_Order_Appearance__c!= objDP.DP_Continuous_Service_Order_Appearance_F__c) {
                bolError = true;
                //objUpdtDP.Continuous_Service_Order_Appearance__c= objDP.DP_Continuous_Service_Order_Appearance_F__c;
                lstError.add('Continuous_Service_Order_Appearance__c\t'+objDP.Continuous_Service_Order_Appearance__c+'\t'+objDP.DP_Continuous_Service_Order_Appearance_F__c+',');
            }
            if(objDP.Is_Cross_Reference__c != objDP.DP_Is_Cross_Reference_F__c) {
                bolError = true;
                //objUpdtDP.Is_Cross_Reference__c = objDP.DP_Is_Cross_Reference_F__c;
                lstError.add('Is_Cross_Reference__c\t'+objDP.Is_Cross_Reference__c +'\t'+objDP.DP_Is_Cross_Reference_F__c+',');
            }
            if(objDP.IBUN_Website_One__c != objDP.DP_IBUN_Website_One_F__c) {
                bolError = true;
                //objUpdtDP.IBUN_Website_One__c = objDP.DP_IBUN_Website_One_F__c;
                lstError.add('IBUN_Website_One__c\t'+objDP.IBUN_Website_One__c +'\t'+objDP.DP_IBUN_Website_One_F__c+',');
            }
            if(objDP.IBUN_Website_Two__c != objDP.DP_IBUN_Website_Two_F__c) {
                bolError = true;
                //objUpdtDP.IBUN_Website_Two__c = objDP.DP_IBUN_Website_Two_F__c;
                lstError.add('IBUN_Website_Two__c\t'+objDP.IBUN_Website_Two__c +'\t'+objDP.DP_IBUN_Website_Two_F__c+',');
            }
            if(objDP.Scoped_Caption_Header__c != objDP.DP_Scoped_Caption_Header_F__c) {
                bolError = true;
                //objUpdtDP.Scoped_Caption_Header__c = objDP.DP_Scoped_Caption_Header_F__c;
                lstError.add('Scoped_Caption_Header__c\t'+objDP.Scoped_Caption_Header__c +'\t'+objDP.DP_Scoped_Caption_Header_F__c+',');
            }
            if(objDP.Year__c != objDP.DP_Year_F__c) {
                bolError = true;
                //objUpdtDP.Year__c = objDP.DP_Year_F__c;
                lstError.add('Year__c\t'+objDP.Year__c +'\t'+objDP.DP_Year_F__c+',');
            }
            if(objDP.Directory_Edition_Code__c != objDP.DP_Directory_Edition_Code_F__c) {
                if(objDP.Directory_Edition_Code__c == null && objDP.DP_Directory_Edition_Code_F__c == '0') {
                }
                else {
                    bolError = true;
                    //objUpdtDP.Directory_Edition_Code__c = objDP.DP_Directory_Edition_Code_F__c;
                    lstError.add('Directory_Edition_Code__c\t'+objDP.Directory_Edition_Code__c +'\t'+objDP.DP_Directory_Edition_Code_F__c+',');
                }
            }
            if(objDP.Directory_Edition_Name__c != objDP.DP_Directory_Edition_Name_F__c) {
                bolError = true;
                //objUpdtDP.Directory_Edition_Name__c = objDP.DP_Directory_Edition_Name_F__c;
                lstError.add('Directory_Edition_Name__c\t'+objDP.Directory_Edition_Name__c +'\t'+objDP.DP_Directory_Edition_Name_F__c+',');
            }
            if(objDP.Banner__c != objDP.DP_Banner_F__c) {
                bolError = true;
                //objUpdtDP.Banner__c = objDP.DP_Banner_F__c;
                lstError.add('Banner__c\t'+objDP.Banner__c +'\t'+objDP.DP_Banner_F__c+',');
            }
            if(objDP.Billboard__c != objDP.DP_Billboard_F__c) {
                bolError = true;
                //objUpdtDP.Billboard__c = objDP.DP_Billboard_F__c;
                lstError.add('Billboard__c\t'+objDP.Billboard__c +'\t'+objDP.DP_Billboard_F__c+',');
            }
            if(objDP.Directory_Heading_Code__c != objDP.DP_Directory_Heading_Code_F__c) {
                bolError = true;
                //objUpdtDP.Directory_Heading_Code__c = objDP.DP_Directory_Heading_Code_F__c;
                lstError.add('Directory_Heading_Code__c\t'+objDP.Directory_Heading_Code__c +'\t'+objDP.DP_Directory_Heading_Code_F__c+',');
            }
            if(objDP.Directory_Heading__c != objDP.DP_Directory_Heading_F__c) {
                bolError = true;
                //objUpdtDP.Directory_Heading__c = objDP.DP_Directory_Heading_F__c;
                lstError.add('Directory_Heading__c\t'+objDP.Directory_Heading__c +'\t'+objDP.DP_Directory_Heading_F__c+',');
            }
            if(objDP.IBUN_Type__c != objDP.DP_IBUN_Type_F__c) {
                bolError = true;
                //objUpdtDP.IBUN_Type__c = objDP.DP_IBUN_Type_F__c;
                lstError.add('IBUN_Type__c\t'+objDP.IBUN_Type__c +'\t'+objDP.DP_IBUN_Type_F__c+',');
            }
            //added newly
            if(objDP.Under_Caption__c != objDP.DP_Under_Caption_F__c) {
                bolError = true;
                //objUpdtDP.Under_Caption__c=objDP.DP_Under_Caption_F__c;
                lstError.add('Under_Caption__c\t'+objDP.Under_Caption__c +'\t'+objDP.DP_Under_Caption_F__c+',');
            }
            
            if(objDP.Incorrect_Setup_Excluded_from_XML__c != objDP.DP_Incorrect_Setup_Excluded_from_XML_F__c) {
                bolError = true;
                //objUpdtDP.Incorrect_Setup_Excluded_from_XML__c = objDP.DP_Incorrect_Setup_Excluded_from_XML_F__c;
               lstError.add('Incorrect_Setup_Excluded_from_XML__c\t'+objDP.Incorrect_Setup_Excluded_from_XML__c +'\t'+objDP.DP_Incorrect_Setup_Excluded_from_XML_F__c+',');
            }
            if(objDP.Internet_Bundle_Ad__c != objDP.DP_Internet_Bundle_Ad_F__c) {
                bolError = true;
                //objUpdtDP.Internet_Bundle_Ad__c = objDP.DP_Internet_Bundle_Ad_F__c;
                lstError.add('Internet_Bundle_Ad__c\t'+objDP.Internet_Bundle_Ad__c +'\t'+objDP.DP_Internet_Bundle_Ad_F__c+',');
            }
            if(objDP.UDAC__c != objDP.DP_UDAC_F__c) {
                bolError = true;
                //objUpdtDP.UDAC__c = objDP.DP_UDAC_F__c;
                lstError.add('UDAC__c\t'+objDP.UDAC__c +'\t'+objDP.DP_UDAC_F__c+',');
            }
            if(objDP.Scoped_Caption_Member__c != objDP.DP_Scoped_Caption_Member_F__c) {
                bolError = true;
               // objUpdtDP.Scoped_Caption_Member__c = objDP.DP_Scoped_Caption_Member_F__c;
                lstError.add('Scoped_Caption_Member__c\t'+objDP.Scoped_Caption_Member__c +'\t'+objDP.DP_Scoped_Caption_Member_F__c+',');
            }
            if(objDP.Section_Heading_Mapping_ID__c != objDP.DP_Section_Heading_Mapping_ID_F__c) {
                bolError = true;
                //objUpdtDP.Section_Heading_Mapping_ID__c = objDP.DP_Section_Heading_Mapping_ID_F__c;
                lstError.add('Section_Heading_Mapping_ID__c\t'+objDP.Section_Heading_Mapping_ID__c +'\t'+objDP.DP_Section_Heading_Mapping_ID_F__c+',');
            }
            if(objDP.Section_Type__c != objDP.DP_Section_Type_F__c) {
                bolError = true;
                //objUpdtDP.Section_Type__c = objDP.DP_Section_Type_F__c;
                lstError.add('Section_Type__c\t'+objDP.Section_Type__c +'\t'+objDP.DP_Section_Type_F__c+',');
            }
            if(objDP.Trademark_Finding_Line__c != objDP.DP_Trademark_Finding_Line_F__c) {
                bolError = true;
                //objUpdtDP.Trademark_Finding_Line__c = objDP.DP_Trademark_Finding_Line_F__c;
                lstError.add('Trademark_Finding_Line__c\t'+objDP.Trademark_Finding_Line__c +'\t'+objDP.DP_Trademark_Finding_Line_F__c+',');
            }
            if(objDP.Type__c != objDP.DP_Type_F__c) {
                bolError = true;
                //objUpdtDP.Type__c = objDP.DP_Type_F__c;
                lstError.add('Type__c\t'+objDP.Type__c +'\t'+objDP.DP_Type_F__c+',');
            }
            
            if(bolError) {
                strErrorMessage += idDP+'\t';
                for(String strMsg : lstError) {
                    strErrorMessage += strMsg; 
                }
                strErrorMessage += '---*---'; 
                //lstUpdt.add(objUpdtDP);
            }
        }
      /*  if(lstUpdt != null && lstUpdt.size() > 0) {
            update lstUpdt;
        } */
        if(String.isNotBlank(strErrorMessage)) {
            if(strErrorMessage.length() > 32767) {
                
                strErrorMessage1 = strErrorMessage.subString(32767,strErrorMessage.length());
                strErrorMessage = strErrorMessage.subString(0,32767);
                if(strErrorMessage1.length() > 32767) {
                    strErrorMessage2 = strErrorMessage1.subString(32767,strErrorMessage1.length());
                    strErrorMessage1 = strErrorMessage1.subString(0,32767);
                    System.debug('strErrorMessage2 length '+strErrorMessage2.length());
                    futureCreateErrorLog.createErrorRecordBatch('Logging only - Mismatching values for formula fields and new fields', strErrorMessage2, 'Logging only - Directory Pagination Field Differences');
                }
                futureCreateErrorLog.createErrorRecordBatch('Logging only - Mismatching values for formula fields and new fields', strErrorMessage1, 'Logging only - Directory Pagination Field Differences');
            }
            futureCreateErrorLog.createErrorRecordBatch('Logging only - Mismatching values for formula fields and new fields', strErrorMessage, 'Logging only - Directory Pagination Field Differences');
        }
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Batch Process Status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures.');
    }
}