@IsTest(SeeAllData=True)
public class PaymentMethodSOQLMethodsTest {
    @isTest static void PaymentMethodSOQLMethodsCoverage() {
        User u = TestMethodsUtility.generateStandardUser();
        System.runAs(u) {
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            insert lstAccount;  
            Account newAccount = new Account();        
            for(Account iterator : lstAccount) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                    newAccount = iterator;
                }
            }
            system.assertNotEquals(newAccount.ID, null);
            system.assertNotEquals(newAccount.Primary_Canvass__c, null);
            Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
            PaymentMethodSOQLMethods.getExpireSoonCreditCardDetails(new set<Id>{newContact.Id}, '10', '12');
            PaymentMethodSOQLMethods.getExpireSoonCreditCardDetailsByExipiryDate(new set<Id>{newContact.Id}, Date.today());
        }
    }
}