global class DFFForFulProfileUpdateBatch implements Database.Batchable<sObject>{
    
     global Database.QueryLocator start(Database.BatchableContext bc) {
        String userId='005K0000002MSSp';
        String start='100135660-321709674-10';
        String endId='102194840-311913407-10';
        String query='Select id,name,Account__c,Core_Opportunity_Line_ID__c from Digital_Product_Requirement__c where CreatedById=:userId and Fulfillment_Profile__c=null and Core_Opportunity_Line_ID__c>=:start and Core_Opportunity_Line_ID__c<=:endId';
        return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext bc, List<Digital_Product_Requirement__c> lstDFF) {
        set<Id> accIdSet=new Set<Id>();
        map<id,Account> reqdAccMap=null;
        for(Digital_Product_Requirement__c dp:lstDFF){
            accIdSet.add(dp.Account__c);
        }
        if(accIdSet.size()>0){
            reqdAccMap=new Map<Id,Account>([select id, (select id from Fulfillment_Profiles__r) from Account where id IN :accIdSet]);
        }
        if(reqdAccMap!=null && reqdAccMap.size()>0){
            for(Digital_Product_Requirement__c dp:lstDFF){
            if(reqdAccMap.get(dp.Account__c).Fulfillment_Profiles__r!=null && reqdAccMap.get(dp.Account__c).Fulfillment_Profiles__r.size()>0){
                dp.Fulfillment_Profile__c=reqdAccMap.get(dp.Account__c).Fulfillment_Profiles__r[0].Id;
                }
            }
        }
        update lstDFF;
     }
     
     global void finish(Database.BatchableContext bc) {
        
    }
}