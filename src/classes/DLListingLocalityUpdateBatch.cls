global class DLListingLocalityUpdateBatch implements Database.Batchable<sObject> {
     global Database.QueryLocator start(Database.BatchableContext bc) {
        string DMId = '005G0000005hhf4';
        String query = 'Select Id,Area_code__c,Exchange__c,Directory__c,Directory_Section__c,Listing_Locality__c FROM Directory_Listing__c where createdbyId =:DMId AND Listing_Locality__c= null';
        system.debug('directorylistingquery'+query);
        return Database.getQueryLocator(query);  
     }
     global void execute(Database.BatchableContext bc, List<Directory_Listing__c> objDLList) {
        set<string> setLeadAssignCombo = new set<string>();
        map<string,Lead_Assignment_Rules__c> mapLAR = new map<string,Lead_Assignment_Rules__c>();
        list<Directory_Listing__c> objDLLst = new list<Directory_Listing__c>();
        if(objDLList.size()> 0) {
            for(Directory_Listing__c iteratorDL : objDLList) {
                system.debug('directory value'+iteratorDL.Directory__c);
                string strcombo = iteratorDL.Area_code__c+iteratorDL.Exchange__c+string.valueof(iteratorDL.Directory__c).left(15)+string.valueof(iteratorDL.Directory_Section__c).left(15);
                system.debug('DL String combo'+strcombo);
                setLeadAssignCombo.add(strcombo);
            }
        }
        if(setLeadAssignCombo.size()> 0) {
            list<Lead_Assignment_Rules__c> lstLeadAssign = new list<Lead_Assignment_Rules__c>();
            lstLeadAssign = [SELECT Area_Code__c,Directory_Section__c,Directory__c,Exchange__c,Id,Name,Telco__c,strLeadAssgnCombo__c FROM Lead_Assignment_Rules__c where strLeadAssgnCombo__c IN : setLeadAssignCombo];
            system.debug('LAR List'+lstLeadAssign.size());
            if(lstLeadAssign.size() >0){
                for(Lead_Assignment_Rules__c iteratorLA : lstLeadAssign) {
                    system.debug('LAR String value'+iteratorLA.strLeadAssgnCombo__c);
                    if(iteratorLA.strLeadAssgnCombo__c != null) {
                        mapLAR.put(iteratorLA.strLeadAssgnCombo__c,iteratorLA);
                    }
                }
            }
        }
        for(Directory_Listing__c iteratorDL : objDLList) {
            string strLARcombo = iteratorDL.Area_code__c+iteratorDL.Exchange__c+string.valueof(iteratorDL.Directory__c).left(15)+string.valueof(iteratorDL.Directory_Section__c).left(15);
            system.debug('String combo'+strLARcombo);
            system.debug('MAP LAR value'+mapLAR.get(strLARcombo));
            if(mapLAR.get(strLARcombo)!= null){
                iteratorDL.Listing_Locality__c = 'Local';
                objDLLst.add(iteratorDL);
            }
            else {
                iteratorDL.Listing_Locality__c = 'Foreign';
                objDLLst.add(iteratorDL);
            }
        }
        system.debug('Updated DL'+objDLLst.size());
        if(objDLLst.size()> 0) {
            update objDLLst;
        }
     }
     global void finish(Database.BatchableContext bc) {
        
     }
}