<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Service Orders &amp; Pagination</label>
    <tab>Listing__c</tab>
    <tab>Directory_Listing__c</tab>
    <tab>Service_Order_Stage__c</tab>
    <tab>Galley_Parameter</tab>
    <tab>standard-report</tab>
    <tab>Service_Order_Annual_Batch</tab>
    <tab>Service_Order_Manual_Batch_Run</tab>
    <tab>Directory__c</tab>
    <tab>Multi_Scoping</tab>
    <tab>Multi_Scoping_Header__c</tab>
    <tab>Multi_Scoping_Child__c</tab>
    <tab>ffps_ct__MatchingWriteOff__c</tab>
</CustomApplication>
