<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Getting_Started_social</tab>
    <tab>Getting_Started_reg</tab>
    <tab>Canvass__c</tab>
    <tab>Payment_History</tab>
    <tab>Directory_Guide__c</tab>
    <tab>ffps_ct__MatchingWriteOff__c</tab>
</CustomApplication>
