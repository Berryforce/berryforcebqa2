trigger BMQuote_AIUD on BigMachines__Quote__c (after insert, after update) {
    if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.quoteBMIObjectName)) {
        if (trigger.isAfter) {
            if (trigger.isInsert) {         
                BMQuoteHandlerController.onAfterInsert(trigger.new);           
            } 
            else if (trigger.isUpdate) {
                BMQuoteHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
            }
        }
    }
}