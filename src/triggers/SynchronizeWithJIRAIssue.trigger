trigger SynchronizeWithJIRAIssue on Case (after update) {
 
 
        {
            // the below lines of code is for getting the Record Type id’s of Case Object and Assigned to below declared variable.
            ID BugRecordType;
            ID EnhancementRecordType;

            for (RecordType rt :[SELECT Id, Name,DeveloperName FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName IN ('Bug', 'Enhancement')])
            {
                if (rt.DeveloperName == 'Bug') BugRecordType = rt.Id;
                if (rt.DeveloperName == 'Enhancement') EnhancementRecordType = rt.Id;
   
            } 
            // Above lines of code is for getting record Type id’s of Case Object.
                       
            //To get current user email id
            String userEmailid='';
            User current_user=[SELECT Email FROM User WHERE Id= :UserInfo.getUserId()] ;
             userEmailid = current_user.Email;
             system.debug('userEmailid======'+userEmailid);
             if(userEmailid <> 'service.rocket@berryforce.com')
             {
                for (Case c : Trigger.new) {                   
                    String objectId = c.id;
                                        
                    // Below condition is for checking if Selected Record Type is “Bug” or “Enhancement”
                    // if selected Record Type id matches with above id’s it will execute the “JIRAConnectorWebserviceCallout. createIssue” method.
                    system.debug('c.RecordTypeId======'+c.RecordTypeId); 
                    system.debug('BugRecordType======='+BugRecordType+'==='+EnhancementRecordType);   
                    if(c.RecordTypeId == BugRecordType || c.RecordTypeId == EnhancementRecordType)
                    {
                        //Execute the call
                        JIRAConnectorWebserviceCalloutSync.synchronizeWithJIRAIssue(objectId);
                    }
                }
             }
    }
}