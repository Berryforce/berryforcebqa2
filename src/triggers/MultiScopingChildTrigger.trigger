trigger MultiScopingChildTrigger on Multi_Scoping_Child__c (before insert, before update) {
	for(Multi_Scoping_Child__c MSC : trigger.New) {
		MSC.MSC_Area_Exchange_Dir_Dir_Sec_Combo__c = MSC.MSC_Area_Code__c + MSC.MSC_Exchange_Code__c + MSC.MSC_Directory__c + MSC.MSC_Directory_Section__c;
	}
}