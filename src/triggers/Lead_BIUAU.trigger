trigger Lead_BIUAU on Lead (before insert,before update, after update) {
	if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.leadObjectName)) { 
	    if(trigger.isBefore) {
	        if(trigger.isInsert) {
	            LeadHandlerController.onBeforeInsert(trigger.new);            
	        }
	        else if(trigger.isUpdate) {
	            LeadHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);      
	        }
	    }
	    
	    if (trigger.isAfter) {if (trigger.isUpdate) {
	            LeadHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
	        }
	    }
	}
}