trigger TrackDFF_ABIUD on Track_DFF_Update__c (after insert, after update, after delete, before insert, before update, before delete) {

    if(trigger.isAfter){
        if(trigger.isUpdate){
        
            TrackDFFHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
        
        }
        if(trigger.isInsert){

        	TrackDFFHandlerController.onAfterInsert(trigger.new);

        }
    
    }
    
    if(trigger.isBefore){
    
    }
}