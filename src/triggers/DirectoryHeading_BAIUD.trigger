trigger DirectoryHeading_BAIUD on Directory_Heading__c (before insert, before update) {
	if(trigger.isInsert){
		DirectoryHeadingHandlerController.onBeforeInsert(trigger.new);
	}
	if(trigger.isUpdate){
		DirectoryHeadingHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);
	}
}