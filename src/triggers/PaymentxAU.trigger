trigger PaymentxAU on pymt__PaymentX__c (after insert,after update) {
    Set<ID> InvIds = new Set<Id>();
    Set<ID> ECheck_InvIds = new Set<ID>();
    
    //Commented Date: 03/04/2014
    //JIRA ISSUE : BFTHREE-1871
    //Commented By : Ankit Nigam
    
    /*
    //For Cash Entry & Cash Entry Line Items
    
    Bank_Account__c BankBNC= Bank_Account__c.getInstance('Berry National Collections');    
    Map<String,Id> MapPeriod =new Map<String,Id>();
    List<c2g__codaInvoiceLineItem__c> InvoiceLIList=new List<c2g__codaInvoiceLineItem__c>();
    
    for(c2g__codaPeriod__c cPeriod :[Select id,Name from c2g__codaPeriod__c where Name =:CommonMethods.getCurrentPeriod()])
    {
        MapPeriod.put(cPeriod.Name,cPeriod.Id);
    }
    List<c2g__codaCashEntry__c> CashEntryInsertList=new List<c2g__codaCashEntry__c>();
    List<c2g__codaCashEntryLineItem__c> CaseEntryLineItemsList=new List<c2g__codaCashEntryLineItem__c>();
    Set<Id> InvoiceIds=new Set<Id>();
    map<Id, c2g__codaInvoice__c> SalesInvoiceMap=new map<Id, c2g__codaInvoice__c>();
    Map<Id, List<c2g__codaInvoiceLineItem__c>> MapAccountILI=new Map<Id, List<c2g__codaInvoiceLineItem__c>>();
    for (pymt__PaymentX__c pymtX: trigger.new) 
    {
        if (pymtX.pymt__Status__c == 'Completed' && pymtX.p2f__FF_Sales_Invoice__c != Null && pymtX.pymt__Amount__c>0) 
        {
          InvoiceIds.add(pymtX.p2f__FF_Sales_Invoice__c);
        //  CashEntryInsertList = lockCloneCommonMethods.generatecashentry(pymtX,BankBNC,MapPeriod);
        }
    }
    
    if(InvoiceIds.size()>0)
          {
            SalesInvoiceMap=SalesInvoiceSOQLMethods.getMapSalesInvoiceLineItemsPaymentsbyInvoiceId(InvoiceIds);
            if(SalesInvoiceMap.size()>0)
            {
               for(c2g__codaInvoice__c SI : SalesInvoiceMap.values())
               {
                  for(c2g__codaInvoiceLineItem__c invoiceLItems : SI.c2g__InvoiceLineItems__r)
                  {
                        //if(invoiceLItems.c2g__NetValue__c>0)
                        //{
                             if(MapAccountILI.get(invoiceLItems.Order_Line_Item__r.Account__c)==null)
                             {
                                 InvoiceLIList=new List<c2g__codaInvoiceLineItem__c>();
                                 InvoiceLIList.add(invoiceLItems);
                                 MapAccountILI.put(invoiceLItems.Order_Line_Item__r.Account__c,InvoiceLIList);
                             }
                             else
                             {
                                  MapAccountILI.get(invoiceLItems.Order_Line_Item__r.Account__c).add(invoiceLItems);
                             }
                       // }
                  }
               }
            
            }
            
             if(CashEntryInsertList.size()>0)
             {
             
             //avalidation error for thie following line 
             // edit this and re save to see the error  https://cs11.salesforce.com/a2LZ0000004APdc
             
             
             
                  insert CashEntryInsertList;
                  for(c2g__codaCashEntry__c ccE : CashEntryInsertList)
                  {
                             System.debug('######CaseEntryLineItemsList######'+ccE.c2g__Account__c+'@@@@@@@@@@@'+MapAccountILI.get(ccE.c2g__Account__c));
                             CaseEntryLineItemsList= lockCloneCommonMethods.generatecashentryItems(ccE.c2g__Account__c,MapAccountILI.get(ccE.c2g__Account__c),ccE);
                  }
                  if(CaseEntryLineItemsList.size()>0)
                  {
                             Database.SaveResult[] srList = Database.insert(CaseEntryLineItemsList, false);
                                for (Database.SaveResult sr : srList) 
                                {
                                        if (sr.isSuccess()) 
                                        {
                                        // Operation was successful, so get the ID of the record that was processed
                                        System.debug('Successfully inserted CaseEntryLineItems. CaseEntryLineItem ID: ' + sr.getId());
                                        }
                                        else 
                                        {
                                        // Operation failed, so get all errors                
                                            for(Database.Error err : sr.getErrors()) 
                                            {
                                            System.debug('The following error has occurred.');                    
                                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                            
                                            }
                                        }
                                }
                  }
             }
    }
      */
      //For Cash Entry & Cash Entry Line Items End Here ..
    
    for (pymt__PaymentX__c PAY: trigger.new) {
        if (PAY.pymt__Status__c == 'Completed' && PAY.pymt__Payment_Type__c == 'Credit Card' && PAY.p2f__FF_Sales_Invoice__c != Null) 
        {
            system.debug(PAY.Name+'**************'+ PAY.pymt__Status__c + '*************' + PAY.pymt__Payment_Type__c + '***********' + PAY.p2f__FF_Sales_Invoice__c);
            if(PAY.Name =='Initial Payment') {
                // other things
            }
            else {
                InvIds.add(PAY.p2f__FF_Sales_Invoice__c);
                system.debug('******InvIds' + InvIds);
            }
        }
        else if (PAY.pymt__Status__c == 'Completed' && PAY.pymt__Payment_Type__c == 'ECheck' && PAY.p2f__FF_Sales_Invoice__c != Null) 
       {
           system.debug(PAY.pymt__Status__c + '*************' + PAY.pymt__Payment_Type__c + '***********' + PAY.p2f__FF_Sales_Invoice__c);
           if(PAY.Name =='Initial Payment') 
           {
                // other things
            }
            else {
                ECheck_InvIds.add(PAY.p2f__FF_Sales_Invoice__c);
                system.debug('******ECheck_InvIds' + ECheck_InvIds);
            }
        }
    }
    PaymentxAUtrigger_v1 paut=new PaymentxAUtrigger_v1();
    
    system.debug('*********InvIds size' + InvIds.size());
    if(InvIds.size() >0) 
    {
        
        paut.makePDF(InvIds);
        //PaymentxAUtrigger.makePDFandSend(InvIds);
    }
    if(ECheck_InvIds.size()>0)
    {
        paut.makePDF(ECheck_InvIds);
       // PaymentxAUtrigger.makePDFandSend(ECheck_InvIds);
    }
    
    PaymentxAUtrigger_v1.declineCCNotification(trigger.new);
}