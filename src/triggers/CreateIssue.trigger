trigger CreateIssue on Case (after insert) {
 

        {
        
        // the below lines of code is for getting the Record Type id’s of Case Object and Assigned to below declared variable.
        ID BugRecordType;
        ID EnhancementRecordType;

        for (RecordType rt :[SELECT Id, Name,DeveloperName FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName IN ('Bug', 'Enhancement')])
        {
                if (rt.DeveloperName == 'Bug') BugRecordType = rt.Id;
                if (rt.DeveloperName == 'Enhancement') EnhancementRecordType = rt.Id;
   
        } 
        // Above lines of code is for getting record Type id’s of Case Object.

        
            for (Case c : Trigger.new) {
 
                    //Define parameters to be used in calling Apex Class
                    String jiraURL = 'https://cscberryteam.atlassian.net';
                    String systemId = '7';
                    String objectType ='Case';
                    String objectId = c.id;
                    String projectKey = 'BA';
                    String issueType = '1';
                    
                    // Below condition is for checking if Selected Record Type is “Bug” or “Enhancement”
                    // if selected Record Type id matches with above id’s it will execute the “JIRAConnectorWebserviceCallout. createIssue” method.
                    system.debug('c.RecordTypeId======'+c.RecordTypeId); 
                    system.debug('BugRecordType======='+BugRecordType+'==='+EnhancementRecordType); 
                    if(c.RecordTypeId == EnhancementRecordType)
                    {
                        issueType = '4';
                    }          
                    if(c.RecordTypeId == BugRecordType || c.RecordTypeId == EnhancementRecordType)
                    {
                        //Execute the trigger
                       // JIRAConnectorWebserviceCallout.createIssue(jiraURL, systemId ,objectType,objectId,projectKey,issueType);
                    }
            }
        }
    
}