trigger DirectoryMapping_BIUAIUD on Directory_Mapping__c(before insert, before update) {
  if(trigger.isBefore) {
      if(trigger.isInsert) {
          DirectoryMappingHandlerController.onBeforeInsert(trigger.new);
      }
      if(trigger.isUpdate) {
         DirectoryMappingHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);
      }
    }
}