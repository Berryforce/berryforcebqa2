trigger SalesCreditNote_AIUD on c2g__codaCreditNote__c ( after insert, after update) {
    set<ID> caseId = new set<ID>();
    if(trigger.isInsert || trigger.isUpdate) {
        /*Commenting the Credit Note amount calculation as per ATP-3910 */
        //SalesCreditNote_AIUDTrigger.creditNoteAmountCalculation(trigger.new);
        caseId = new set<Id>();
        for(c2g__codaCreditNote__c iterator : trigger.new) {
            if(String.isNotEmpty(iterator.Case__c)) {
                caseId.add(iterator.Case__c);
            }
        }
    }
    
    list<Case> lstCase;
    map<String, decimal> caseTotalCredit = new map<String, decimal>();
    if(caseId.size() > 0) {
        lstCase =  CommonFunctions.getCaseByID(caseId);
        list<AggregateResult> lstAggregate = CommonFunctions.getSalesCreditNoteTotalSumByCase(caseId);
        for(AggregateResult iteratorAggre : lstAggregate) {
            string str = String.valueOf(iteratorAggre.get('SumCredit'));
            caseTotalCredit.put(String.valueOf(iteratorAggre.get('Case__c')), Decimal.valueOf(str));
        }
    }
    
    if(lstCase != null) {
        list<Case> updateCase = new list<Case>();
        for(Case iteratorCase : lstCase) {
            if(caseTotalCredit.get(iteratorCase.Id) != null) {
                iteratorCase.Total_Credit_Note__c = caseTotalCredit.get(iteratorCase.Id);
                //updateCase.add(iteratorCase);
            }
            else {
                if(iteratorCase.Total_Credit_Note__c != null) {
                    iteratorCase.Total_Credit_Note__c = null;
                }
            }
            updateCase.add(iteratorCase);
        }
        
        if(updateCase.size() > 0) {
            update updateCase;
        }
        
        /*map<Id, decimal> caseTotalCredit = new map<Id, decimal>();
        for(c2g__codaCreditNote__c iterator : trigger.new) {
            if(!caseTotalCredit.keyContains(iterator.Case__c)) {
                caseTotalCredit.put(iterator.Case__c, iterator.c2g__CreditNoteTotal__c);
            }
            else {
                decimal totalNote  = caseTotalCredit.get(iterator.Case__c);
                totalNote  = totalNote + iterator.c2g__CreditNoteTotal__c;
                caseTotalCredit.put(iterator.Case__c, totalNote);
            }
        }*/
    }
    
    
}