trigger DirectoryEdition_BIUAI on Directory_Edition__c (before Insert,before update, after insert) {
    if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.DEObjectName)) {
        map<string,Directory__c> mapDir = new map<String,Directory__c>();
        map<Id,String> mapTelcos = new map<Id,String>();
        map<Id,String> mapCanvas = new map<Id,String>();
        set<Id> setDE = new set<Id>();
        
        if(trigger.isInsert && trigger.isAfter) {
            DirectoryEditionHandler.onAfterInsert(trigger.new);
        } 
        
        if(trigger.isUpdate && trigger.isBefore) {
            DirectoryEditionHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        } 
        
        if(trigger.isBefore) {
        	
        	if(trigger.isInsert) {
        		DirectoryEditionHandler.onBeforeInsert(trigger.new);
        	}
        	
            for(Directory_Edition__c iterationDE : trigger.new) {
                if(iterationDE.Directory__c != null) {
                    setDE.add(iterationDE.Directory__c);
                }
            }
            
            for(Directory__c objD : [select id,name,Telco_Provider__r.Name,Canvass__r.Name from Directory__c where Id IN :setDE]) {
                mapDir.put(objD.Id,objD);
                mapTelcos.put(objD.Id,objD.Telco_Provider__r.name);
                mapCanvas.put(objD.Id,objD.Canvass__r.Name);
            }
        
            system.debug('***************Iteration Start******************');
            if(trigger.isInsert) {
                DirectoryEditionDatesUpdate.DatesUpdate(trigger.new, null, mapDir, mapTelcos, mapCanvas);
            }
            else if(trigger.isUpdate) {
                system.debug('***************Iteration Update Start******************');
                DirectoryEditionDatesUpdate.DatesUpdateNew(trigger.new, trigger.oldMap);            
                DirectoryEditionDatesUpdate.DatesUpdate(trigger.new, trigger.oldMap, mapDir, mapTelcos, mapCanvas);
                system.debug('***************Iteration Update End******************');
                
            }
        }
        system.debug('***************Iteration End******************');
    }
}