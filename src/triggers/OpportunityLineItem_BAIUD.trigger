trigger OpportunityLineItem_BAIUD on OpportunityLineItem (after insert, after update, before insert, before update) {
if( [SELECT count() from Opportunity_trigger_off__c where name!='True' or name!='true' ] == 1 ){

    Map <String, String> MissingDirId = new Map <String, String>();
    if (trigger.isBefore && trigger.isInsert){
        OpportunityLineItemHandler.onBeforeInsert(trigger.new);
    }
    if(trigger.isAfter && trigger.isInsert){
        //NationalOrderCreation NationOrderC=new NationalOrderCreation ();
        //NationOrderC.OrderLineCreation(trigger.new);
        //check the directory__c
        for(OpportunityLineItem OLI:trigger.new){
            if(OLI.directory__c == Null && OLI.Directory_number__c !=Null){
               //MissingDirId.put(OLI.ID,OLI.Directory_number__c); 
            }   
        }
        OpportunityLineItemHandler.onAfterInsert(trigger.new, trigger.newmap);
    }
    if(trigger.isBefore && trigger.isUpdate) {
        OpportunityLineItemHandler.onBeforeUpdate(trigger.new, trigger.oldmap);
    }
    
    if(trigger.isAfter && trigger.isUpdate) {
        OpportunityLineItemHandler.onAfterUpdate(trigger.new, trigger.oldmap);
    }
    
    if(MissingDirId.size()>0){
      // OpportunityLineItem_BAIUDTrigger.fixDirectory(MissingDirId);
    }   
}
}