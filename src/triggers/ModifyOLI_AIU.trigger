trigger ModifyOLI_AIU on Modification_Order_Line_Item__c (after insert) 
{
    if(CommonMethods.skipTriggerLogic(CommonMessages.OLIObjectName)) {
       Map<id,Order_Line_Items__c> MapOLIforUpdate=new Map<Id,Order_Line_Items__c>();
        if(Trigger.isInsert){
           /* for(Modification_Order_Line_Item__c moli : trigger.new){
            if(moli.Order_Line_Item__c!=null){
                Order_Line_Items__c ParentOli=new Order_Line_Items__c(id=moli.Order_Line_Item__c,OrderLineItem_Status__c = moli.ModificationOrderLineItem_Status__c);
                MapOLIforUpdate.put(moli.Order_Line_Item__c,ParentOli);
            }    
            }
            
            if(MapOLIforUpdate.size()>0)
            {
              update MapOLIforUpdate.values();
            }*/
            
            ModificationOLIHandlerController.onAfterInsert(trigger.New);
        }
    }
}