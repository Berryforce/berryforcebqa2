trigger Case_BIAIU on Case (before insert,after insert,after update){
    if(Trigger.isBefore && Trigger.isInsert) {
        caseHandlerController.onBeforeInsert(trigger.new);
        
    }
    if(Trigger.isAfter) { 
        if(Trigger.isInsert) {            
            caseHandlerController.onAfterInsert(trigger.new);
        }
        if(Trigger.isUpdate) {            
            caseHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }
}