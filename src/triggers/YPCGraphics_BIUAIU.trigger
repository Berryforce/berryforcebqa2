trigger YPCGraphics_BIUAIU on YPC_Graphics__c (before update, after update) {
    if(trigger.isBefore) {
        YPCDFFHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
    } else if(trigger.isAfter) {
        YPCDFFHandler.onAfterUpdate(trigger.new, trigger.oldMap);    
    }
}